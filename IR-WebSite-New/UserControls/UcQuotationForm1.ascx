﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UcQuotationForm1.ascx.cs"
    Inherits="UserControls_UcQuotationForm1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    @media only screen and (max-width: 639px)
    {
        .class-size td
        {
            width: 100%;
            display: -webkit-box;
        }
        .div-select-class
        {
            min-height: 100px;
        }
    }
    .radio-manage
    {
        position:relative !important;
        top:8px !important;
    }
    #MainContent_eurostar_UcQuotationForm1_rdBookingType tr td :first-child input
    {
        color:Green;
    }
</style>
<asp:UpdatePanel ID="updOneWayJourney" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnFilter" runat="server" />
        
        <div class="form_block fullrow" id="divSearch" runat="server">
            <div class="row"> 
                    <label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 subtitle detail-heading">
                        Journey details
                        <br>
                        <small>This is the description of your section break. </small>
                    </label>
                   <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="display:inline-flex;" id="div-journey">
                   <strong class="radio-manage"> One way or return journey? *</strong> <span class="pull-right">
                               <div class="starail-u-cf">
                                <asp:RadioButtonList ID="rdBookingType" runat="server" ValidationGroup="vgs1" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rdBookingType_SelectedIndexChanged">
                                    <asp:ListItem Value="0" Selected="True">One-way</asp:ListItem>
                                    <asp:ListItem Value="1">Return</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                            </span>
                    
                    </div></div>
           
        </div> 
        <div class="form_block fullrow">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="warning" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="row">
                <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                    One way journey
                </label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <div class="form-group">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label class="control-label">
                                From
                            </label>
                            <asp:TextBox ID="txtFrom" runat="server" onkeyup="selectpopup1(this)" onkeydown="Getkeydown1(event,this)"
                                class="starail-Form-input" autocomplete="off" />
                            <asp:RequiredFieldValidator ID="reqvalerror1" runat="server" ValidationGroup="vgs1"
                                ErrorMessage="*" CssClass="starail-Form-required absolute" Display="Static" ControlToValidate="txtFrom" />
                            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtFrom"
                                WatermarkText="Enter a start location" WatermarkCssClass="watermark" />
                            <span id="spantxtFrom1" style="display: none"></span>
                            <asp:HiddenField ID="hdnFrm" runat="server" />
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label class="control-label">
                                To
                            </label>
                            <asp:TextBox ID="txtTo" runat="server" onkeyup="selectpopup1(this)" onkeydown="Getkeydown1(event,this)"
                                class="starail-Form-input" autocomplete="off" />
                            <asp:RequiredFieldValidator ID="reqvalerror2" runat="server" ValidationGroup="vgs1"
                                ErrorMessage="*" CssClass="starail-Form-required absolute" Display="Static" ControlToValidate="txtTo" />
                            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtTo"
                                WatermarkText="Enter a destination" WatermarkCssClass="watermark" />
                            <span id="spantxtTo1" style="display: none"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <p class="margb0">
                                <strong>Date of departure *</strong></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0 ">
                    <div class="form-group">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="starail-Form-datePicker">
                                <asp:TextBox ID="txtDepartureDate" runat="server" class="starail-Form-input" Text="DD/MM/YYYY"
                                    autocomplete="off" onfocus="DatePickerCall1()" />
                                <asp:RequiredFieldValidator ID="reqvalerror11" runat="server" ValidationGroup="vgs1"
                                    ErrorMessage="*" CssClass="starail-Form-required absolute" InitialValue="DD/MM/YYYY"
                                    Display="Static" ControlToValidate="txtDepartureDate" />
                                <i class="starail-Icon-datepicker"></i>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <asp:DropDownList ID="ddldepTime" runat="server" CssClass="starail-Form-input">
                                <asp:ListItem>00:00</asp:ListItem>
                                <asp:ListItem>01:00</asp:ListItem>
                                <asp:ListItem>02:00</asp:ListItem>
                                <asp:ListItem>03:00</asp:ListItem>
                                <asp:ListItem>04:00</asp:ListItem>
                                <asp:ListItem>05:00</asp:ListItem>
                                <asp:ListItem>06:00</asp:ListItem>
                                <asp:ListItem>07:00</asp:ListItem>
                                <asp:ListItem>08:00</asp:ListItem>
                                <asp:ListItem Selected="True">09:00</asp:ListItem>
                                <asp:ListItem>10:00</asp:ListItem>
                                <asp:ListItem>11:00</asp:ListItem>
                                <asp:ListItem>12:00</asp:ListItem>
                                <asp:ListItem>13:00</asp:ListItem>
                                <asp:ListItem>14:00</asp:ListItem>
                                <asp:ListItem>15:00</asp:ListItem>
                                <asp:ListItem>16:00</asp:ListItem>
                                <asp:ListItem>17:00</asp:ListItem>
                                <asp:ListItem>18:00</asp:ListItem>
                                <asp:ListItem>19:00</asp:ListItem>
                                <asp:ListItem>20:00</asp:ListItem>
                                <asp:ListItem>21:00</asp:ListItem>
                                <asp:ListItem>22:00</asp:ListItem>
                                <asp:ListItem>23:00</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0 div_smallheight div-select-class">
                    <div class="form-group">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display: inline-flex;">
                            <p class="p-lineheight" >
                                <strong class="radio-manage">Class *</strong> <span class="pull-right">
                                    <asp:RadioButtonList ID="ddlClass" runat="server" RepeatDirection="Horizontal" CssClass="class-size">
                                        <asp:ListItem Selected="True" Value="Standard">Standard</asp:ListItem>
                                        <asp:ListItem Value="Standard Premier">Standard Premier</asp:ListItem>
                                        <asp:ListItem Value="Business Premier">Business Premier</asp:ListItem>
                                    </asp:RadioButtonList>
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0 div_largeheight">
                    <div class="form-group">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <p>
                                <strong>Train number (if known)</strong>
                                <asp:TextBox ID="txtTrainNo" runat="server" CssClass="starail-Form-input"></asp:TextBox></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="rdBookingType" />
    </Triggers>
</asp:UpdatePanel>
<asp:HiddenField ID="hdnForModelUK" runat="server" />
<asp:HiddenField ID="hdnForModel" runat="server" />
<asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="updOneWayJourney"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="modalBackground progessposition">
        </div>
        <div class="progess-inner2">
            UPDATING RESULTS...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<script type="text/javascript">
    function btnswapstation() {
        var spantxtFrom = '';
        var txtFrom = '';
        spantxtFrom = $("#spantxtFrom1").text();
        $("#spantxtFrom1").text($("#spantxtTo1").text());
        $("#spantxtTo1").text(spantxtFrom);
        txtFrom = $("[id*=UcQuotationForm1_txtFrom]").val();
        $("#txtFromtxtFrom").val($("#txtFromtxtTo").val());
        $("#txtFromtxtTo").val(txtFrom);
    }
</script>
<script type="text/javascript">
    var count = 0;
    var countKey = 1;
    var conditionone = 0;
    var conditiontwo = 0;
    var tabkey = 0;

    $(document).ready(function () {
        callvalerror();
        LoadCal1();
        DatePickerCall1();
        $("[id*=UcQuotationForm1_txtFrom]").focus();
        if ($("[id*=UcQuotationForm1_txtFrom]").val() != '') {
            $('#spantxtTo1').text(localStorage.getItem("spantxtTo1"));
        }
        if ($("[id*=UcQuotationForm1_txtTo]").val() != '') {
            $('#spantxtFrom1').text(localStorage.getItem("spantxtFrom1"));
        }
        $(window).click(function (event) {
            $('#_bindDivData').remove();
        });
        changeSearchStaion();
    });

    function changeSearchStaion() {
        $("input[id*=txtFrom],input[id*=txtto]").keydown(function (event) {
            if (event.keyCode == 13 || (event.keyCode == 9 && tabkey == 1)) {
                //tabkey = 0;
                //event.preventDefault();
                return false;
            }
            else {
                //tabkey = 1;
            }
        });
        $('input[type="text"]').keydown(function (event) {
            if (event.keyCode == 13 || (event.keyCode == 9 && tabkey == 1)) {
                //tabkey = 0;
                //event.preventDefault();
                return false;
            }
            else {
                //tabkey = 1;
            }
        });
    }

    function Getkeydown1(event, obj) {
        var $id = $(obj);
        var maxno = 0;
        count = event.keyCode;
        $(".popupselect").each(function () { maxno++; });
        if (count == 13 || count == 9) {
            $(".popupselect").each(function () {
                if ($(this).attr('style') != undefined) {
                    $(this).trigger('onclick');
                    $id.val($.trim($(this).find('span').text()));
                }
            });
            $('#_bindDivData').remove();
            countKey = 1;
            conditionone = 0;
            conditiontwo = 0;
        }
        else if (count == 40 && maxno > 1) {
            conditionone = 1;
            if (countKey == maxno)
                countKey = 0;
            if (conditiontwo == 1) {
                countKey++;
                conditiontwo = 0;
            }
            $(".popupselect").removeAttr('style');
            $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
            countKey++;
        }
        else if (count == 38 && maxno > 1) {
            conditiontwo = 1;
            if (countKey == 0)
                countKey = maxno;
            if (conditionone == 1) {
                countKey--;
                conditionone = 0;
            }
            countKey--;
            $(".popupselect").removeAttr('style');
            $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
        }
        else {
            countKey = 1;
            conditionone = 0;
            conditiontwo = 0;
        }
    }

    function selectpopup1(e) {
        if (count != 40 && count != 38 && count != 13 && count != 37 && count != 39 && count != 9) {
            var $this = $(e);
            var data = $this.val();

            var station = '';
            var hostName = window.location.host;
            var url = "http://" + hostName;

            if (window.location.toString().indexOf("https:") >= 0)
                url = "https://" + hostName;


            if ($("[id*=UcQuotationForm1_txtFrom]").val() == '' && $("[id*=UcQuotationForm1_txtTo]").val() == '') {
                $('#spantxtFrom1').text('');
                $('#spantxtTo1').text('');
            }

            var filter = $("#span" + $this.attr('id') + "").text();
            if (filter == "" && $this.val() != "")
                filter = $("[id*=UcQuotationForm1_hdnFilter]").val();
            $("[id*=UcQuotationForm1_hdnFilter]").val(filter);

            

            if (/txtTo/i.test($this.attr('id'))){
                station = $("[id*=UcQuotationForm1_txtFrom]").val();
                data = data + "*from";
            }
            else {
                station = $("[id*=UcQuotationForm1_txtTo]").val();
            }

            if (hostName == "localhost") {
                url = url + "/1tracknew";
            }
            var hostUrl = url + "/StationList.asmx/getStationsQuoteXList";

            data = data.replace(/[']/g, "♥");
            var $div = $("<div id='_bindDivData' onmousemove='_removehoverclass(this)'/>");
            $.ajax({
                type: "POST",
                url: hostUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'prefixText':'" + data + "','filter':'" + filter + "','station':'" + station + "'}",
                success: function (msg) {
                    $('#_bindDivData').remove();
                    var lentxt = data.split('*')[0].length;
                    $.each(msg.d, function (key, value) {
                        var splitdata = value.split('ñ');
                        var lenfull = splitdata[0].length; ;
                        var txtupper = splitdata[0].substring(0, lentxt);
                        var txtlover = splitdata[0].substring(lentxt, lenfull);
                        $this.after($div.append("<div class='popupselect' onclick='_Bindthisvalue(this)'><b class='_prifixText'>" + txtupper + "</b>" + txtlover + "<span style='display:none'>" + splitdata[0] + "</span><div style='display:none'>" + splitdata[1] + "</div>"));
                    });
                    $(".popupselect:eq(0)").attr('style', 'background-color: #ccc !important');
                },
                error: function () {
                }
            });
        }
    }

    function _removehoverclass(e) {
        $(".popupselect").hover(function () {
            $(".popupselect").removeAttr('style');
            $(this).attr('style', 'background-color: #ccc !important');
            countKey = $(this).index();
        });
    }

    function _Bindthisvalue(e) {
        var idtxtbox = $('#_bindDivData').prev("input").attr('id');
        $("#" + idtxtbox + "").val($(e).find('span').text());
        if (idtxtbox == 'MainContent_UcQuotationForm1_txtTo') {
            $('#spantxtFrom1').text($(e).find('div').text());
            localStorage.setItem("spantxtFrom1", $(e).find('div').text());
        }
        else {
            $('#spantxtTo1').text($(e).find('div').text());
            localStorage.setItem("spantxtTo1", $(e).find('div').text());
        }
        $('#_bindDivData').remove();
    }

    var unavailableDates = '<%=unavailableDates1 %>';
    function nationalDays(date) {
        dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
        if ($.inArray(dmy, unavailableDates) > -1) {
            return [false, "", "Unavailable"];
        }
        return [true, ""];
    }

    function LoadCal1() {
        $("[id*=UcQuotationForm1_txtDepartureDate]").bind("contextmenu cut copy paste", function (e) {
            return false;
        });
        $("[id*=UcQuotationForm1_txtDepartureDate]").keypress(function (event) { event.preventDefault(); });

        $(".imgCal").click(function () {
            $("[id*=UcQuotationForm1_txtDepartureDate]").datepicker('show');
        });
        DatePickerCall1();
    }

    function calenable() {
        callvalerror();
        LoadCal1();
        DatePickerCall1();
    }

    function caldisable() {
        callvalerror();
        LoadCal1();
        DatePickerCall1();
    }

    function DatePickerCall1() {
        $(document).ready(function () {
            $("[id*=UcQuotationForm1_txtDepartureDate],[id*=UcQuotationForm2_txtDepartureDate]").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: '<%=minDate %>'
            });

            $("#MainContent_ucLoyaltyCart_txtDepartureDate").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: '<%=minDate %>',
                onClose: function (selectedDate) {
                    $("#MainContent_ucLoyaltyCart_txtReturnDate").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#MainContent_ucLoyaltyCart_txtReturnDate").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: $("#MainContent_ucLoyaltyCart_txtDepartureDate").val()
            });
        });
    }
    function LoadCal() { };
</script>
