﻿#region Using
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using Business;
using OneHubServiceRef;
using System.Web.UI.HtmlControls;
using System.Web.Script.Serialization;
#endregion

public partial class UserControls_ucThailandHeader : System.Web.UI.UserControl
{
    private readonly db_1TrackEntities db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    Guid siteId;
    public string AdminSiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    public int BucketItem = 0;
    public string siteURL, SiteLogoUrl, FaviconIcon, AttentionTitle = string.Empty;
    public string AgentName = "Agent Login";
    public bool IsLoggedIn = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                if (Session["siteId"] != null)
                    siteId = Guid.Parse(Session["siteId"].ToString());
                else
                    return;

                if (_oWebsitePage.GetSiteLayoutType(siteId) != 6)
                    return;

                siteURL = _oWebsitePage.GetSiteURLbySiteId(siteId);
                if (AgentuserInfo.UserID != Guid.Empty)
                    AgentPageLoad();
                else
                    AgentName = "Agent Login";

                hour3.Visible = _oWebsitePage.IsVisibleHourOfSite(siteId);
                IrHeaderSection();
                ShowAd75Menu();
            }
            LoadSiteLogo();
            GetNoOfBucketItem();
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    private void GetNoOfBucketItem()
    {
        try
        {
            List<getRailPassData> list = Session["RailPassData"] as List<getRailPassData>;
            if (list != null && list.Count > 0)
                BucketItem = list.Count();
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void LoadSiteLogo()
    {
        try
        {
            var data = db.tblSites.FirstOrDefault(x => x.ID == siteId && x.IsActive == true);
            if (data != null)
            {
                if (!string.IsNullOrEmpty(data.FaviconPath))
                    FaviconIcon = "<link href='" + AdminSiteUrl + data.FaviconPath + "' rel='shortcut icon' type='image/x-icon' />";

                if (!string.IsNullOrEmpty(data.LogoPath))
                    SiteLogoUrl = AdminSiteUrl + data.LogoPath;
                else
                    SiteLogoUrl = siteURL + "assets/img/branding/logo.png";
            }

            var siteDetail = _oWebsitePage.GetSiteDetailsById(siteId);
            if (data != null)
                AttentionTitle = siteDetail.AttentionTitle;
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    private void IrHeaderSection()
    {
        try
        {
            var data = _oWebsitePage.GetSiteDetailsById(siteId);
            if (data != null)
            {
                ltrSitePhoneNumber.Text = data.PhoneNumber;
                SiteHeadingTitle.InnerText = data.SiteHeadingTitle;
            }
            rptCountry.DataSource = _oWebsitePage.GetCountryNavigationList(siteId);
            rptCountry.DataBind();

            rptSpecialTrain.DataSource = _oWebsitePage.GetSpecialTrainList(siteId);
            rptSpecialTrain.DataBind();
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    private void AgentPageLoad()
    {
        try
        {
            string agentUser = AgentuserInfo.Username;
            var rec = db.tblAdminUsers.FirstOrDefault(x => x.UserName == agentUser);
            string name = rec == null ? "" : rec.Forename;

            if (name == string.Empty)
                name = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase((AgentuserInfo.UserID == Guid.Empty ? AgentuserInfo.Username : "Agent").ToString().ToLower());
            IsLoggedIn = true;
            AgentName = "Logout";
            lblIRAgent.Text = name;
            if (rec != null)
            {
                Guid InJob = Guid.Parse("5D3E09AB-AFDB-400D-A7C7-8DBF6F6A5F87");
                IrPrintQueue.Visible = db.aspnet_Roles.FirstOrDefault(x => x.RoleId == rec.RoleID).IsPrintingAllow;
            }
            IrPrintQueue.HRef = siteURL + "Printing/PrintingOrders.aspx";
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void ShowAd75Menu()
    {
        try
        {
            var objsite = db.tblSites.FirstOrDefault(x => x.ID == siteId);
            if (objsite == null)
                return;

            if ((bool)objsite.IsAgent)
                if (AgentuserInfo.UserID != Guid.Empty && !AgentuserInfo.IsAffliate)
                {
                    var AgentId = AgentuserInfo.UserID;
                    IrFoc.Visible = _oWebsitePage.IsFocAD75(AgentId);
                    IrFoc.HRef = siteURL + "?category=FOC-AD75";
                }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}