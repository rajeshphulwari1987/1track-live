﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="JourneyRequestForm.ascx.cs"
    Inherits="UserControls_JourneyRequestForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .row {
        padding-bottom: 10px;
    }

    .singledatatable tr {
        display: inline;
    }

    .doubledatatable label {
        display: inline !important;
    }

    tr label {
        padding-right: 10px;
        padding-left: 5px;
    }

    .addbtn {
        text-decoration: underline;
        font-weight: bold;
    }

    .chkwidth {
        width: 1%;
    }

    .p-full-text {
        text-align: justify;
        margin: 0px;
    }
</style>
<asp:UpdatePanel ID="updjrq" runat="server">
    <ContentTemplate>
        <div class="page-wrap">
            <div class="row warning" id="errorMsg" runat="server" visible="false">
                <p class="starail-Form-required">
                    I'm sorry we don't appear to be able to do that journey online at the moment, the
                    chances are we're able to do it offline though. Please complete the form below and
                    we'll get back to you with a quote.
                </p>
            </div>
            <div class="left-content success" id="sucessMsg" runat="server" visible="false">
                <asp:Label runat="server" ID="succmessage"></asp:Label>
                <asp:LinkButton ID="btnRedirect" runat="server" Text="home" OnClick="btnRedirect_Click"></asp:LinkButton>
            </div>
        </div>
        <asp:Panel ID="pnlJourneyInfo" runat="server">
            <div class="starail-BookingDetails-form">
                <div class="row">
                    <h2>Journey Request Form
                    </h2>
                </div>
                <div class="row">
                    Please complete the form below with as much detail as possible for your required
                    journey and one of our agents will come back to you with the details and a quotation.
                    Should there be more than one FIP card holder on the booking please add this detail
                    in the “Special instructions” field at the bottom.
                    <hr />
                </div>
                <div class="row">
                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                        Your name<span class="starail-Form-required">*</span>
                    </label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <asp:TextBox ID="txtfirst" runat="server" class="starail-Form-input" placeholder="First name"
                                MaxLength="50" />
                            <asp:RequiredFieldValidator ID="reqvalerrorfirst" runat="server" Text="*" ControlToValidate="txtfirst"
                                ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic" />
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <asp:TextBox ID="txtlast" runat="server" class="starail-Form-input" placeholder="Last name"
                                MaxLength="50" />
                            <asp:RequiredFieldValidator ID="reqvalerrorlast" runat="server" Text="*" ControlToValidate="txtlast"
                                ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                        Your email <span class="starail-Form-required">*</span>
                    </label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <asp:TextBox ID="txtemail" runat="server" class="starail-Form-input" placeholder="Email"
                                MaxLength="150" />
                            <asp:RequiredFieldValidator ID="reqvalerroremail" runat="server" Text="*" ControlToValidate="txtemail"
                                ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                        Your phone number <span class="starail-Form-required">*</span>
                    </label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <asp:TextBox ID="txtphone" runat="server" class="starail-Form-input" placeholder="Phone"
                                MaxLength="30" />
                            <asp:RequiredFieldValidator ID="reqvalerrorphone" runat="server" Text="*" ControlToValidate="txtphone"
                                ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic" />
                        </div>
                    </div>
                </div>
                <div class="row" id="div_FipCard" runat="server">
                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                        FIP Card Number (including prefix, if any) <span class="starail-Form-required">*</span>
                    </label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <asp:TextBox ID="txtFIPcardNo" runat="server" class="starail-Form-input" placeholder="FIP Card Number"
                                MaxLength="100" />
                            <asp:RequiredFieldValidator ID="reqvalerrorstore" runat="server" Text="*" ControlToValidate="txtFIPcardNo"
                                ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic" />
                        </div>
                    </div>
                </div>
                <div class="row" id="div_FipClass" runat="server">
                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                        FIP Card Class <span class="starail-Form-required">*</span>
                    </label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <asp:DropDownList ID="ddlFIPClass" runat="server" CssClass="starail-Form-input">
                                <asp:ListItem>1st class FIP card</asp:ListItem>
                                <asp:ListItem>2nd class FIP card</asp:ListItem>
                                <asp:ListItem>1st class coupon</asp:ListItem>
                                <asp:ListItem>2nd class coupon</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                        How would you like the ticket delivered <span class="starail-Form-required">*</span>
                    </label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <asp:RadioButtonList ID="ticketdelivered" runat="server" CssClass="doubledatatable">
                                <asp:ListItem Value="Paper ticket (please add the shipping to address in the additional notes area at the bottom of the page)"></asp:ListItem>
                                <asp:ListItem Value="Ticket on departure (if available)"></asp:ListItem>
                                <asp:ListItem Value="E-ticket (if available)"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="reqvalerrorticketdelivered" runat="server" Text="*"
                                ControlToValidate="ticketdelivered" ForeColor="#ededed" ValidationGroup="vgsTR"
                                Display="Dynamic" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                        Passenger details <span class="starail-Form-required">*</span>
                    </label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            Number of adults
                            <asp:DropDownList ID="ddladult" runat="server" CssClass="starail-Form-input">
                            </asp:DropDownList>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            Number of children<asp:DropDownList ID="ddlchildren" runat="server" CssClass="starail-Form-input">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                    </label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            Number of youths<asp:DropDownList ID="ddlyouths" runat="server" CssClass="starail-Form-input">
                            </asp:DropDownList>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            Number of seniors<asp:DropDownList ID="ddlseniors" runat="server" CssClass="starail-Form-input">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                        Lead passenger name (as per passport) <span class="starail-Form-required">*</span>
                    </label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <asp:DropDownList runat="server" ID="ddltitle" CssClass="starail-Form-input">
                                <asp:ListItem Value="0">Title</asp:ListItem>
                                <asp:ListItem Value="Dr.">Dr.</asp:ListItem>
                                <asp:ListItem Value="Mr.">Mr.</asp:ListItem>
                                <asp:ListItem Value="Miss.">Miss.</asp:ListItem>
                                <asp:ListItem Value="Mrs.">Mrs.</asp:ListItem>
                                <asp:ListItem Value="Ms.">Ms.</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqvalerrorleadtitle" runat="server" Text="*" ControlToValidate="ddltitle"
                                InitialValue="0" ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic" />
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <asp:TextBox runat="server" ID="txtleadfname" class="starail-Form-input" placeholder="First name"
                                MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqvalerrorleadfname" runat="server" Text="*" ControlToValidate="txtleadfname"
                                ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic" />
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <asp:TextBox runat="server" ID="txtleadlast" class="starail-Form-input" placeholder="Last name"
                                MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqvalerrorleadlast" runat="server" Text="*" ControlToValidate="txtleadlast"
                                ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                    </label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="height: 10px;">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                    </label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="height: 10px;">
                        </div>
                    </div>
                </div>
                <asp:Repeater ID="repeterjourneylist" runat="server" OnItemDataBound="repeterjourneylist_ItemDataBound">
                    <ItemTemplate>
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h3>
                                    <%#Eval("textrowno")%>
                                    journey (<a href="http://plannerint.b-rail.be/bin/query.exe/en?L=profi&amp;" target="_blank">European
                                        timetable</a>)</h3>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                <a id="lnkRemovejourney" data-value='<%#Eval("rowno")%>' class="deletebtn" <%#(Eval("rowno").ToString()=="1"?"style='display:none;'":"")%>>
                                    <img src="images/btn-cross.png" /></a>
                                <asp:HiddenField runat="server" ID="hdnrowno" Value='<%#Eval("rowno")%>' />
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                                From<span class="starail-Form-required">*</span>
                            </label>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <asp:TextBox runat="server" ID="txtfrom" Value='<%#Eval("txtfrom")%>' class="starail-Form-input"
                                        MaxLength="100"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqvalerrorfrom" runat="server" Text="*" ControlToValidate="txtfrom"
                                        ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                                To<span class="starail-Form-required">*</span>
                            </label>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <asp:TextBox runat="server" ID="txtto" Value='<%#Eval("txtto")%>' class="starail-Form-input"
                                        MaxLength="100"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqvalerrorto" runat="server" Text="*" ControlToValidate="txtto"
                                        ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                                Via/change
                            </label>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <asp:TextBox runat="server" ID="txtchange" Value='<%#Eval("txtchange")%>' class="starail-Form-input"
                                        MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                                Date of departure<span class="starail-Form-required">*</span>
                            </label>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <asp:TextBox runat="server" ID="txtdeptdate" Value='<%#Eval("txtdeptdate")%>' class="starail-Form-input DepartureDate"
                                        MaxLength="20" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqvalerrordeptdate" runat="server" Text="*" ControlToValidate="txtdeptdate"
                                        ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                                Time of departure (24 hour)<span class="starail-Form-required">*</span>
                            </label>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <asp:HiddenField runat="server" ID="hdnddltimedept" Value='<%#Eval("ddltimedept")%>' />
                                    <asp:DropDownList runat="server" ID="ddltimedept" CssClass="starail-Form-input">
                                        <asp:ListItem Value="00:00">00:00</asp:ListItem>
                                        <asp:ListItem Value="01:00">01:00</asp:ListItem>
                                        <asp:ListItem Value="02:00">02:00</asp:ListItem>
                                        <asp:ListItem Value="03:00">03:00</asp:ListItem>
                                        <asp:ListItem Value="04:00">04:00</asp:ListItem>
                                        <asp:ListItem Value="05:00">05:00</asp:ListItem>
                                        <asp:ListItem Value="06:00">06:00</asp:ListItem>
                                        <asp:ListItem Value="07:00">07:00</asp:ListItem>
                                        <asp:ListItem Value="08:00">08:00</asp:ListItem>
                                        <asp:ListItem Selected="True" Value="09:00">09:00</asp:ListItem>
                                        <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                        <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                        <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                        <asp:ListItem Value="13:00">13:00</asp:ListItem>
                                        <asp:ListItem Value="14:00">14:00</asp:ListItem>
                                        <asp:ListItem Value="15:00">15:00</asp:ListItem>
                                        <asp:ListItem Value="16:00">16:00</asp:ListItem>
                                        <asp:ListItem Value="17:00">17:00</asp:ListItem>
                                        <asp:ListItem Value="18:00">18:00</asp:ListItem>
                                        <asp:ListItem Value="19:00">19:00</asp:ListItem>
                                        <asp:ListItem Value="20:00">20:00</asp:ListItem>
                                        <asp:ListItem Value="21:00">21:00</asp:ListItem>
                                        <asp:ListItem Value="22:00">22:00</asp:ListItem>
                                        <asp:ListItem Value="23:00">23:00</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reqvalerrorddltimedept" runat="server" Text="*" ControlToValidate="ddltimedept"
                                        ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                                Time of arrival (24 hour)
                            </label>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <asp:HiddenField runat="server" ID="hdnddltimearrival" Value='<%#Eval("ddltimearrival")%>' />
                                    <asp:DropDownList runat="server" ID="ddltimearrival" CssClass="starail-Form-input">
                                        <asp:ListItem Value="00:00">00:00</asp:ListItem>
                                        <asp:ListItem Value="01:00">01:00</asp:ListItem>
                                        <asp:ListItem Value="02:00">02:00</asp:ListItem>
                                        <asp:ListItem Value="03:00">03:00</asp:ListItem>
                                        <asp:ListItem Value="04:00">04:00</asp:ListItem>
                                        <asp:ListItem Value="05:00">05:00</asp:ListItem>
                                        <asp:ListItem Value="06:00">06:00</asp:ListItem>
                                        <asp:ListItem Value="07:00">07:00</asp:ListItem>
                                        <asp:ListItem Value="08:00">08:00</asp:ListItem>
                                        <asp:ListItem Value="09:00">09:00</asp:ListItem>
                                        <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                        <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                        <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                        <asp:ListItem Value="13:00">13:00</asp:ListItem>
                                        <asp:ListItem Value="14:00">14:00</asp:ListItem>
                                        <asp:ListItem Value="15:00">15:00</asp:ListItem>
                                        <asp:ListItem Value="16:00">16:00</asp:ListItem>
                                        <asp:ListItem Value="17:00">17:00</asp:ListItem>
                                        <asp:ListItem Value="18:00">18:00</asp:ListItem>
                                        <asp:ListItem Value="19:00">19:00</asp:ListItem>
                                        <asp:ListItem Value="20:00">20:00</asp:ListItem>
                                        <asp:ListItem Value="21:00">21:00</asp:ListItem>
                                        <asp:ListItem Value="22:00">22:00</asp:ListItem>
                                        <asp:ListItem Value="23:00">23:00</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                                Train number (if known)
                            </label>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <asp:TextBox runat="server" ID="txttrainno" Value='<%#Eval("txttrainno")%>' class="starail-Form-input"
                                        MaxLength="20"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                                Class of service
                            </label>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <asp:HiddenField runat="server" ID="hdnddlclass" Value='<%#Eval("ddlclass")%>' />
                                    <asp:DropDownList runat="server" ID="ddlclass" CssClass="starail-Form-input">
                                        <asp:ListItem Value="All"></asp:ListItem>
                                        <asp:ListItem Value="1st Class">1st Class</asp:ListItem>
                                        <asp:ListItem Value="2nd Class">2nd Class</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                                Type of accommodation<span class="starail-Form-required">*</span>
                            </label>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <asp:HiddenField runat="server" ID="hdnddlservicetype" Value='<%#Eval("ddlservicetype")%>' />
                                    <asp:DropDownList runat="server" ID="ddlservicetype" CssClass="starail-Form-input">
                                        <asp:ListItem>Seat</asp:ListItem>
                                        <asp:ListItem>4 Berth Couchette</asp:ListItem>
                                        <asp:ListItem>6 Berth Couchette</asp:ListItem>
                                        <asp:ListItem>Single Sleeper</asp:ListItem>
                                        <asp:ListItem>Single Sleeper Deluxe</asp:ListItem>
                                        <asp:ListItem>Double Sleeper</asp:ListItem>
                                        <asp:ListItem>Double Sleeper Deluxe</asp:ListItem>
                                        <asp:ListItem>3 Berth Sleeper</asp:ListItem>
                                        <asp:ListItem>3 Berth Sleeper Deluxe</asp:ListItem>
                                        <asp:ListItem>4 Berth Sleeper (on certain night trains only)</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reqvalerrorservicetype" runat="server" Text="*" ControlToValidate="ddlservicetype"
                                        InitialValue="0" ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                            </label>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="height: 10px;">
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <div class="row">
                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                    </label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="text-align: right;">
                            <asp:LinkButton ID="btnAddNewJourney" runat="server" Text="Add Journey" CssClass="addbtn"
                                OnClick="btnAddNewJourney_Click"></asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                    </label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="height: 10px;">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-lg-9 col-md-9 col-sm-9 col-xs-12 subtitle">
                        Special instructions eg seat requests, extra pax, extra journeys.
                    </label>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                    </div>
                </div>
                <div class="row">
                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                    </label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <asp:TextBox runat="server" ID="txtnote" TextMode="MultiLine" Width="100%" Height="100px"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                    </label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <asp:Button runat="server" ID="btnsubmit" Text="Submit" ValidationGroup="vgsTR" CssClass="starail-Button starail-Form-button starail-Form-button--primary"
                                OnClick="btnsubmit_Click"></asp:Button>
                            <asp:Button ID="btnDeleteNewJourney" runat="server" Text="Delete Journey" OnClick="btnDeleteNewJourney_Click"
                                Style="display: none;" />
                            <asp:HiddenField ID="hdnclickValue" runat="server" Value="0" />
                        </div>
                    </div>
                </div>
                <div class="row p-desc-mad" id="div_Mandatory" runat="server" visible="false">
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 chkwidth">
                        <asp:CheckBox ID="chkMandatory" runat="server" />
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 chkwidth" style="padding: 0px;">
                        <span class="starail-Form-required">*</span>
                    </div>
                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                        <p class="p-full-text" id="p_MandatoryText" runat="server"></p>
                    </div>
                </div>
                <div class="row p-desc-not-mad" id="div_NotMandatory" runat="server" visible="false">
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 chkwidth">
                        <asp:CheckBox ID="chkNotMandatory" runat="server" />
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 chkwidth" style="padding: 0px;">
                    </div>
                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                        <p class="p-full-text" id="p_NotMandatoryText" runat="server"></p>
                    </div>
                </div>
                <div class="row p-desc-not-mad">
                    <div style="color: red">
                        * Please complete all mandatory fields.
                    </div>
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="updjrq"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="modalBackground progessposition">
        </div>
        <div class="progess-inner2">
            Shovelling coal into the server...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<script type="text/javascript">
    $(document).ready(function () {
        showhideterms();
    });

    var unavailableDates = '<%=unavailableDates1 %>';
    function nationalDays(date) {
        dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
        if ($.inArray(dmy, unavailableDates) > -1) {
            return [false, "", "Unavailable"];
        }
        return [true, ""];
    }

    function LoadCal22() {
        $(".DepartureDate").keypress(function (event) { event.preventDefault(); });
        $(".DepartureDate").datepicker({
            numberOfMonths: 1,
            dateFormat: 'dd/M/yy',////////
            beforeShowDay: nationalDays,
            showButtonPanel: true,
            firstDay: 1,
            minDate: 21,
            maxDate: '+6m'
        });
    }

    function callvalerror22() {
        $('input:submit').on('click', function () {
            callvalerror22();
        });
        $("span[id*=reqvalerror]").each(function (key, value) {
            var style = $(this).css('display');
            var visibility = $(this).css('visibility');
            if (style == 'none' || visibility == 'hidden') {
                $(this).parent().find('table').removeClass('starail-Form-error').css("border", "0px solid red");
            }
            else {
                $(this).parent().find('table').addClass('starail-Form-error').css("border", "1px solid red");
            }
        });
    }

    function pageLoad(sender, args) {
        LoadCal22();
        callvalerror22();
        $(document).ready(function () {
            $('.deletebtn').click(function () {
                $('[id*=hdnclickValue]').val($(this).attr('data-value'));
                $("[id*=btnDeleteNewJourney]").trigger("click");
            });
        });
    }

    function showhideterms() {
        if ('<%=IsStaSite %>' == 'False') {
            var mandatoryText = '<%=MandatoryTextFirst %>';
            if (mandatoryText.length > 130) {
                $('[id*=p_MandatoryText]').text(mandatoryText.substr(0, mandatoryText.lastIndexOf(' ', 127)) + '...');
                mandatoryText = mandatoryText.substr(0, mandatoryText.lastIndexOf(' ', 127)) + '...';
            }
            var notMandatoryText = '<%=MandatoryTextFirstSecond %>';
            if (notMandatoryText.length > 130) {
                $('[id*=p_NotMandatoryText]').text(notMandatoryText.substr(0, notMandatoryText.lastIndexOf(' ', 127)) + '...');
                notMandatoryText = notMandatoryText.substr(0, notMandatoryText.lastIndexOf(' ', 127)) + '...';
            }

            $('.p-desc-mad').mouseover(function () {
                $(this).find('.p-full-text').text('<%=MandatoryTextFirst %>');
            }).mouseout(function () {

                $(this).find('.p-full-text').text(mandatoryText);
            });
            ;
            $('.p-desc-not-mad').mouseover(function () {
                $(this).find('.p-full-text').text('<%=MandatoryTextFirstSecond %>');
            }).mouseout(function () {
                $(this).find('.p-full-text').text(notMandatoryText);
            });
        }
    }

    function checkMandatory() {
        alert('Please accept the privacy statement, by clicking the tick box at the bottom of the page.');
    }
</script>
