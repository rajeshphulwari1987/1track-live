﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;

public partial class UserControls_EurostarRequestQuote : System.Web.UI.UserControl
{
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    ManageTrainDetails _master = new ManageTrainDetails();
    private readonly ManageBooking objBooking = new ManageBooking();
    public string siteURL, currency = string.Empty;
    Guid siteId;
    public long OrderID = 0;
    public Guid currencyID = new Guid();

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            if (Session["siteId"] != null)
            {
                siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
                siteId = Guid.Parse(Session["siteId"].ToString());
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            GetCurrencyCode();
            if (!IsPostBack)
            {
                LoadCountry();
                LoadPassengerList();
                if (Session["DeadlineOrder"] != null)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "form1sendrequest", "confirmRequest();", true);
                    OrderID = Convert.ToInt64(Session["DeadlineOrder"].ToString());
                    GetP2POrderData(OrderID);
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void LoadCountry()
    {
        try
        {
            ddlCountry.DataSource = _master.GetCountryDetail();
            ddlCountry.DataValueField = "CountryID";
            ddlCountry.DataTextField = "CountryName";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("Select Country", "0"));

            ddlShipCountry.DataSource = _master.GetCountryDetail();
            ddlShipCountry.DataValueField = "CountryID";
            ddlShipCountry.DataTextField = "CountryName";
            ddlShipCountry.DataBind();
            ddlShipCountry.Items.Insert(0, new ListItem("Select Country", "0"));

            for (int j = 10; j >= 0; j--)
            {
                ddlAdult.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlChild.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlYouth.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlSenior.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlAdult.SelectedValue = "1";
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void btnCheckout_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlAdult.SelectedValue == "0" && ddlYouth.SelectedValue == "0" && ddlSenior.SelectedValue == "0" && ddlChild.SelectedValue == "0")
            {
                CallScripts();
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "form1alertmsg", "alert('Please select at least 1 adult, senior or junior(youth) passenger.')", true);
                return;
            }
            if (Session["DeadlineOrder"] != null)
                OrderID = Convert.ToInt64(Session["DeadlineOrder"].ToString());
            OrderID = new ManageQuotation().CreateOrder(OrderID, string.Empty, AgentuserInfo.UserID, Guid.Empty, siteId, "P2P", "0");

            //add order billing address
            tblOrderBillingAddress objBilling = new tblOrderBillingAddress();
            objBilling.ID = Guid.NewGuid();
            objBilling.OrderID = OrderID;
            objBilling.Title = ddlMr.SelectedItem.Text;
            objBilling.FirstName = txtFirst.Text.Trim();
            objBilling.LastName = txtLast.Text.Trim();
            objBilling.EmailAddress = txtEmail.Text.Trim();
            objBilling.Phone = txtBillPhone.Text.Trim();
            objBilling.Address1 = txtAdd.Text.Trim();
            objBilling.Address2 = txtAdd2.Text.Trim();
            objBilling.City = txtCity.Text.Trim();
            objBilling.State = txtState.Text.Trim();
            objBilling.Postcode = txtZip.Text.Trim();
            objBilling.Country = ddlCountry.SelectedItem.Text.Trim();
            if (chkShipping.Checked)
            {
                objBilling.TitleShpg = ddlShipMr.SelectedItem.Text;
                objBilling.FirstNameShpg = txtShipFirst.Text.Trim();
                objBilling.LastNameShpg = txtShipLast.Text.Trim();
                objBilling.EmailAddressShpg = txtShipEmail.Text.Trim();
                objBilling.PhoneShpg = txtShipPhone.Text.Trim();
                objBilling.Address1Shpg = txtShipAdd.Text.Trim();
                objBilling.Address2Shpg = txtShipAdd2.Text.Trim();
                objBilling.CityShpg = txtShipCity.Text.Trim();
                objBilling.StateShpg = txtShipState.Text.Trim();
                objBilling.PostcodeShpg = txtShipZip.Text.Trim();
                objBilling.CountryShpg = ddlShipCountry.SelectedItem.Text.Trim();
            }
            else
            {
                objBilling.TitleShpg = ddlMr.SelectedItem.Text;
                objBilling.FirstNameShpg = txtFirst.Text.Trim();
                objBilling.LastNameShpg = txtLast.Text.Trim();
                objBilling.EmailAddressShpg = txtEmail.Text.Trim();
                objBilling.PhoneShpg = txtBillPhone.Text.Trim();
                objBilling.Address1Shpg = txtAdd.Text.Trim();
                objBilling.Address2Shpg = txtAdd2.Text.Trim();
                objBilling.CityShpg = txtCity.Text.Trim();
                objBilling.StateShpg = txtState.Text.Trim();
                objBilling.PostcodeShpg = txtZip.Text.Trim();
                objBilling.CountryShpg = ddlCountry.SelectedItem.Text.Trim();
            }
            new ManageQuotation().AddUpdateOrderBillingAddress(objBilling);

            //delete all existing order
            new ManageQuotation().DeleteAllOrderInfo(OrderID);

            //add one way journey
            RadioButtonList rdBookingType = (RadioButtonList)UcQuotationForm1.FindControl("rdBookingType");
            TextBox txtFrom1 = (TextBox)UcQuotationForm1.FindControl("txtFrom");
            TextBox txtTo1 = (TextBox)UcQuotationForm1.FindControl("txtTo");
            TextBox txtDepartureDate1 = (TextBox)UcQuotationForm1.FindControl("txtDepartureDate");
            DropDownList ddldepTime1 = (DropDownList)UcQuotationForm1.FindControl("ddldepTime");
            RadioButtonList ddlClass1 = (RadioButtonList)UcQuotationForm1.FindControl("ddlClass");
            TextBox txtTrainNo1 = (TextBox)UcQuotationForm1.FindControl("txtTrainNo");

            Guid P2PSaleId = AddP2PPassSale(txtFrom1.Text.Trim(), txtTo1.Text.Trim(), ddlClass1.SelectedValue.ToString(), Convert.ToDateTime(txtDepartureDate1.Text.Trim()), ddldepTime1.SelectedValue.ToString(), GetApiName(txtFrom1.Text.Trim()), txtTrainNo1.Text.Trim());
            if (P2PSaleId != Guid.Empty)
            {
                AddTraveller(OrderID, P2PSaleId);
                new ManageQuotation().UpdateFIPNumber(P2PSaleId, ViewState["FIPNumber"].ToString());
            }

            //add return journey
            if (rdBookingType.SelectedValue == "1")
            {
                TextBox txtFrom2 = (TextBox)UcQuotationForm2.FindControl("txtFrom");
                TextBox txtTo2 = (TextBox)UcQuotationForm2.FindControl("txtTo");
                TextBox txtDepartureDate2 = (TextBox)UcQuotationForm2.FindControl("txtDepartureDate");
                DropDownList ddldepTime2 = (DropDownList)UcQuotationForm2.FindControl("ddldepTime");
                RadioButtonList ddlClass2 = (RadioButtonList)UcQuotationForm2.FindControl("ddlClass");
                TextBox txtTrainNo2 = (TextBox)UcQuotationForm2.FindControl("txtTrainNo");

                Guid P2PSaleId2 = AddP2PPassSale(txtFrom2.Text.Trim(), txtTo2.Text.Trim(), ddlClass2.SelectedValue.ToString(), Convert.ToDateTime(txtDepartureDate2.Text.Trim()), ddldepTime2.SelectedValue.ToString(), GetApiName(txtFrom2.Text.Trim()), txtTrainNo2.Text.Trim());
                if (P2PSaleId2 != Guid.Empty)
                {
                    new ManageQuotation().AddPassP2PSalelookup(OrderID, P2PSaleId2, new ManageQuotation().GetTravellerId(OrderID), ViewState["FIPNumber"].ToString());
                    new ManageQuotation().UpdateFIPNumber(P2PSaleId2, ViewState["FIPNumber"].ToString());
                }
            }
            // add ordermodel lookup
            new ManageQuotation().AddModelLookUp(OrderID);
            //add order note
            tblOrderNote objNotes = new tblOrderNote();
            objNotes.ID = Guid.NewGuid();
            objNotes.OrderID = OrderID;
            objNotes.CreatedOn = DateTime.Now;
            objNotes.Notes = txtParagraph.Text.Trim();
            new ManageQuotation().AddNotes(objNotes);

            if (Session["DeadlineOrder"] != null)
            {
                Session["DeadlineOrder"] = null;
                Session.Remove("DeadlineOrder");
            }

            ResetControls();
            LoadPassengerList();
            ShowMessage(1, "Your request has been sent successfully. Team will contact you shortly.");
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "form1animateToTop", "animateToTop();", true);
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public Guid AddP2PPassSale(string From, string To, string Class, DateTime DeptDate, string DeptTime, string ApiName, string TrainNo)
    {
        try
        {
            tblP2PSale objP2P = new tblP2PSale();
            objP2P.From = From.Trim();
            objP2P.To = To.Trim();
            objP2P.Adult = ddlAdult.SelectedValue.ToString();
            objP2P.Children = ddlChild.SelectedValue.ToString();
            objP2P.Youth = ddlYouth.SelectedValue.ToString();
            objP2P.Senior = ddlSenior.SelectedValue.ToString();
            objP2P.Passenger = ddlAdult.SelectedValue.ToString() + " Adults, " + ddlChild.SelectedValue.ToString() + " Child," + ddlYouth.SelectedValue.ToString() + " Youth," + ddlSenior.SelectedValue.ToString() + " Senior";
            objP2P.CurrencyId = currencyID;
            objP2P.Class = Class;
            objP2P.DateTimeDepature = DeptDate;
            objP2P.DepartureTime = DeptTime;
            objP2P.ApiName = ApiName;
            objP2P.TrainNo = TrainNo;
            objP2P.Price = Convert.ToDecimal("0.00");
            objP2P.NetPrice = Convert.ToDecimal("0.00");
            objP2P.TicketProtection = Convert.ToDecimal("0.00");
            Guid P2PSaleId = new ManageQuotation().AddP2PSale(objP2P);
            return P2PSaleId;
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); return Guid.Empty; }
    }

    protected void ddlSenior_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            LoadPassengerList();
            CallScripts();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "form1filldefaultnameOnChange1", "filldefaultnameOnChange();", true);
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void ddlYouth_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            LoadPassengerList();
            CallScripts();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "form1filldefaultnameOnChange2", "filldefaultnameOnChange();", true);
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void ddlChild_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            LoadPassengerList();
            CallScripts();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "form1filldefaultnameOnChange3", "filldefaultnameOnChange();", true);
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void ddlAdult_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            LoadPassengerList();
            CallScripts();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "form1filldefaultnameOnChange4", "filldefaultnameOnChange();", true);
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void LoadPassengerList()
    {
        try
        {
            var list = new List<Passanger>();
            if (Convert.ToInt32(ddlAdult.SelectedValue) > 0)
            {
                for (int i = 1; i <= Convert.ToInt32(ddlAdult.SelectedValue); i++)
                {
                    list.Add(new Passanger
                    {
                        PassangerType = "Adult " + i.ToString()
                    });
                }
            }
            if (Convert.ToInt32(ddlChild.SelectedValue) > 0)
            {
                for (int i = 1; i <= Convert.ToInt32(ddlChild.SelectedValue); i++)
                {
                    list.Add(new Passanger
                    {
                        PassangerType = "Child " + i.ToString()
                    });
                }
            }
            if (Convert.ToInt32(ddlYouth.SelectedValue) > 0)
            {
                for (int i = 1; i <= Convert.ToInt32(ddlYouth.SelectedValue); i++)
                {
                    list.Add(new Passanger
                    {
                        PassangerType = "Youth " + i.ToString()
                    });
                }
            }
            if (Convert.ToInt32(ddlSenior.SelectedValue) > 0)
            {
                for (int i = 1; i <= Convert.ToInt32(ddlSenior.SelectedValue); i++)
                {
                    list.Add(new Passanger
                    {
                        PassangerType = "Senior " + i.ToString()
                    });
                }
            }

            if (list.Count() > 0)
                dtlPassngerDetails.DataSource = list;
            else
                dtlPassngerDetails.DataSource = null;
            dtlPassngerDetails.DataBind();
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void CallScripts()
    {
        try
        {
            RadioButtonList rdBookingType = (RadioButtonList)UcQuotationForm1.FindControl("rdBookingType");
            if (rdBookingType.SelectedValue == "0")
                ScriptManager.RegisterStartupScript(Page, GetType(), "form11cal2", "caldisable();caldisable2();showhideJourney();", true);
            else
                ScriptManager.RegisterStartupScript(Page, GetType(), "form11cal3", "calenable();calenable2();showhideJourney();", true);
            ScriptManager.RegisterStartupScript(Page, GetType(), "form11return", "LoadCal1();LoadCal2(); changeSearchStaion();changelabel();enableShippingAddress();", true);
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    void GetCurrencyCode()
    {
        try
        {
            if (Session["siteId"] != null)
            {
                siteId = Guid.Parse(Session["siteId"].ToString());
                currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
                currency = oManageClass.GetCurrency(currencyID);
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public string GetApiName(string FromStation)
    {
        try
        {
            GetStationDetailsByStationName objStationDeptDetail = objBooking.GetStationDetailsByStationName(FromStation);
            if (objStationDeptDetail != null)
                return objStationDeptDetail.RailName.Trim();
            else
                return string.Empty;
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); return string.Empty; }
    }

    public void AddTraveller(long OrderNo, Guid P2PSaleId)
    {
        try
        {
            if (dtlPassngerDetails.Items.Count > 0)
            {
                for (int i = 0; i < dtlPassngerDetails.Items.Count; i++)
                {
                    DropDownList Title = (DropDownList)dtlPassngerDetails.Items[i].FindControl("ddlTitle");
                    TextBox FirstName = (TextBox)dtlPassngerDetails.Items[i].FindControl("txtFirstName");
                    TextBox LastName = (TextBox)dtlPassngerDetails.Items[i].FindControl("txtLastName");
                    TextBox FIPNumber = (TextBox)dtlPassngerDetails.Items[i].FindControl("txtFipNumber");
                    DropDownList ddlPsrCountry = (DropDownList)dtlPassngerDetails.Items[i].FindControl("ddlPsrCountry");

                    tblOrderTraveller objTravell = new tblOrderTraveller();
                    objTravell.Title = Title.SelectedItem.Text;
                    objTravell.FirstName = FirstName.Text.Trim();
                    objTravell.LastName = LastName.Text.Trim();
                    objTravell.Country = Guid.Parse(ddlPsrCountry.SelectedValue.ToString());
                    Guid OrderTravellerId = new ManageQuotation().AddOrderTraveller(objTravell);

                    if (i == 0)
                    {
                        ViewState["FIPNumber"] = FIPNumber.Text.Trim();
                        new ManageQuotation().AddPassP2PSalelookup(OrderNo, P2PSaleId, OrderTravellerId, FIPNumber.Text);
                    }
                    if (dtlPassngerDetails.Items.Count > 1)
                        new ManageQuotation().AddP2POtherTravellerLookup(OrderNo, P2PSaleId, OrderTravellerId, FIPNumber.Text);
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void dtlPassngerDetails_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlPsrCountry = (DropDownList)e.Item.FindControl("ddlPsrCountry");
                ddlPsrCountry.DataSource = _master.GetCountryDetail();
                ddlPsrCountry.DataValueField = "CountryID";
                ddlPsrCountry.DataTextField = "CountryName";
                ddlPsrCountry.DataBind();
                ddlPsrCountry.Items.Insert(0, new ListItem("Select Country", "0"));
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    void ResetControls()
    {
        try
        {
            txtAdd.Text = "";
            txtAdd2.Text = "";
            txtBillPhone.Text = "";
            txtCity.Text = "";
            txtEmail.Text = "";
            txtFirst.Text = "";
            txtLast.Text = "";
            txtParagraph.Text = "";
            txtState.Text = "";
            txtZip.Text = "";
            ddlCountry.SelectedIndex = 0;

            txtShipAdd.Text = "";
            txtShipAdd2.Text = "";
            txtShipPhone.Text = "";
            txtShipCity.Text = "";
            txtShipEmail.Text = "";
            txtShipFirst.Text = "";
            txtShipLast.Text = "";
            txtShipState.Text = "";
            txtShipZip.Text = "";
            ddlShipCountry.SelectedIndex = 0;
            ddlShipMr.SelectedIndex = 0;

            ddlAdult.SelectedValue = "1";
            ddlChild.SelectedIndex = 0;
            ddlYouth.SelectedIndex = 0;
            ddlSenior.SelectedIndex = 0;
            txtParagraph.Text = "";
            chkShipping.Checked = false;

            CallScripts();
            RadioButtonList rdBookingType = (RadioButtonList)UcQuotationForm1.FindControl("rdBookingType");
            TextBox txtFrom1 = (TextBox)UcQuotationForm1.FindControl("txtFrom");
            TextBox txtTo1 = (TextBox)UcQuotationForm1.FindControl("txtTo");
            TextBox txtDepartureDate1 = (TextBox)UcQuotationForm1.FindControl("txtDepartureDate");
            DropDownList ddldepTime1 = (DropDownList)UcQuotationForm1.FindControl("ddldepTime");
            RadioButtonList ddlClass1 = (RadioButtonList)UcQuotationForm1.FindControl("ddlClass");
            TextBox txtTrainNo1 = (TextBox)UcQuotationForm1.FindControl("txtTrainNo");
            rdBookingType.SelectedValue = "0";
            txtFrom1.Text = "";
            txtTo1.Text = "";
            txtDepartureDate1.Text = "DD/MM/YYYY";
            ddldepTime1.SelectedValue = "09:00";
            ddlClass1.SelectedValue = "0";
            txtTrainNo1.Text = "";

            TextBox txtFrom2 = (TextBox)UcQuotationForm2.FindControl("txtFrom");
            TextBox txtTo2 = (TextBox)UcQuotationForm2.FindControl("txtTo");
            TextBox txtDepartureDate2 = (TextBox)UcQuotationForm2.FindControl("txtDepartureDate");
            DropDownList ddldepTime2 = (DropDownList)UcQuotationForm2.FindControl("ddldepTime");
            RadioButtonList ddlClass2 = (RadioButtonList)UcQuotationForm2.FindControl("ddlClass");
            TextBox txtTrainNo2 = (TextBox)UcQuotationForm2.FindControl("txtTrainNo");
            txtFrom2.Text = "";
            txtTo2.Text = "";
            txtDepartureDate2.Text = "DD/MM/YYYY";
            ddldepTime2.SelectedValue = "09:00";
            ddlClass2.SelectedValue = "0";
            txtTrainNo2.Text = "";
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    private void GetP2POrderData(long OrderID)
    {
        try
        {
            var lst = new ManageBooking().GetP2PSaleDetail(OrderID).OrderBy(t => t.ShortOrder).ToList();
            if (lst != null && lst.Count() > 0 && lst.FirstOrDefault().TrvType == "P2P")
            {
                ddlAdult.SelectedValue = lst.FirstOrDefault().Adult;
                ddlChild.SelectedValue = lst.FirstOrDefault().Children;
                ddlYouth.SelectedValue = lst.FirstOrDefault().Youth;
                ddlSenior.SelectedValue = lst.FirstOrDefault().Senior;
                LoadPassengerList();

                GetTraveller(OrderID);
                GetBillngAddress(OrderID);

                for (int i = 0; i < lst.Count; i++)
                {
                    if (i == 0)
                    {
                        RadioButtonList rdBookingType = (RadioButtonList)UcQuotationForm1.FindControl("rdBookingType");
                        TextBox txtFrom1 = (TextBox)UcQuotationForm1.FindControl("txtFrom");
                        TextBox txtTo1 = (TextBox)UcQuotationForm1.FindControl("txtTo");
                        TextBox txtDepartureDate1 = (TextBox)UcQuotationForm1.FindControl("txtDepartureDate");
                        DropDownList ddldepTime1 = (DropDownList)UcQuotationForm1.FindControl("ddldepTime");
                        RadioButtonList ddlClass1 = (RadioButtonList)UcQuotationForm1.FindControl("ddlClass");
                        TextBox txtTrainNo1 = (TextBox)UcQuotationForm1.FindControl("txtTrainNo");

                        rdBookingType.SelectedValue = lst.Count == 2 ? "1" : "0";
                        txtFrom1.Text = lst[i].From;
                        txtTo1.Text = lst[i].To;
                        txtDepartureDate1.Text = lst[i].DateTimeDepature.Value.ToString("dd/MMM/yyyy");
                        ddldepTime1.SelectedValue = lst[i].DepartureTime.Split(':')[0];
                        ddlClass1.SelectedValue = lst[i].Class;
                        txtTrainNo1.Text = lst[i].TrainNo;
                    }
                    else
                    {
                        TextBox txtFrom2 = (TextBox)UcQuotationForm2.FindControl("txtFrom");
                        TextBox txtTo2 = (TextBox)UcQuotationForm2.FindControl("txtTo");
                        TextBox txtDepartureDate2 = (TextBox)UcQuotationForm2.FindControl("txtDepartureDate");
                        DropDownList ddldepTime2 = (DropDownList)UcQuotationForm2.FindControl("ddldepTime");
                        RadioButtonList ddlClass2 = (RadioButtonList)UcQuotationForm2.FindControl("ddlClass");
                        TextBox txtTrainNo2 = (TextBox)UcQuotationForm2.FindControl("txtTrainNo");

                        txtFrom2.Text = lst[i].From;
                        txtTo2.Text = lst[i].To;
                        txtDepartureDate2.Text = lst[i].DateTimeDepature.Value.ToString("dd/MMM/yyyy");
                        ddldepTime2.SelectedValue = lst[i].DepartureTime.Split(':')[0];
                        ddlClass2.SelectedValue = lst[i].Class;
                        txtTrainNo2.Text = lst[i].TrainNo;
                    }
                }
            }
            //CallScripts();
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    private void GetTraveller(long OrderID)
    {
        try
        {
            var objTrvellerList = (from TOLU in _db.tblP2POtherTravellerLookup
                                   join TOT in _db.tblOrderTravellers on TOLU.OrderTravellerID equals TOT.ID
                                   where TOLU.OrderID == OrderID
                                   select new { TOT, TOLU }).Select(x => new OrderTrvellers
                                   {
                                       TravellerId = x.TOT.ID,
                                       P2POtherId = x.TOLU.ID,
                                       Title = x.TOT.Title,
                                       FirstName = x.TOT.FirstName,
                                       LastName = x.TOT.LastName,
                                       FipNumber = x.TOLU.FIPNumber,
                                       OrderIdentity = x.TOT.OrderIdentity,
                                       TvCountry = x.TOT.Country.Value
                                   }).OrderBy(x => x.OrderIdentity).ToList();


            if (objTrvellerList != null && objTrvellerList.Count > 0)
            {
                for (int i = 0; i < objTrvellerList.Count; i++)
                {
                    HiddenField hdnTravellerId = (HiddenField)dtlPassngerDetails.Items[i].FindControl("hdnTravellerId");
                    HiddenField hdnP2POtherId = (HiddenField)dtlPassngerDetails.Items[i].FindControl("hdnP2POtherId");
                    DropDownList ddlTitle = (DropDownList)dtlPassngerDetails.Items[i].FindControl("ddlTitle");
                    TextBox txtFirstName = (TextBox)dtlPassngerDetails.Items[i].FindControl("txtFirstName");
                    TextBox txtLastName = (TextBox)dtlPassngerDetails.Items[i].FindControl("txtLastName");
                    TextBox txtFipNumber = (TextBox)dtlPassngerDetails.Items[i].FindControl("txtFipNumber");
                    DropDownList ddlPsrCountry = (DropDownList)dtlPassngerDetails.Items[i].FindControl("ddlPsrCountry");

                    txtFipNumber.Text = !string.IsNullOrEmpty(objTrvellerList[i].FipNumber.ToString()) ? objTrvellerList[i].FipNumber.ToString() : "";
                    ddlTitle.SelectedIndex = ddlTitle.Items.IndexOf(ddlTitle.Items.FindByText(objTrvellerList[i].Title));
                    txtFirstName.Text = objTrvellerList[i].FirstName;
                    txtLastName.Text = objTrvellerList[i].LastName;
                    ddlPsrCountry.SelectedValue = objTrvellerList[i].TvCountry.ToString();
                    hdnTravellerId.Value = objTrvellerList[i].TravellerId.ToString();
                    hdnP2POtherId.Value = objTrvellerList[i].P2POtherId.ToString();
                }
            }
            else
            {
                var objTvList = (from TPSL in _db.tblPassP2PSalelookup
                                 join TOT in _db.tblOrderTravellers on TPSL.OrderTravellerID equals TOT.ID
                                 where TPSL.OrderID == OrderID
                                 select new { TOT, TPSL }).Select(x => new OrderTrvellers
                                 {
                                     TravellerId = x.TOT.ID,
                                     PassP2PId = x.TPSL.ID,
                                     Title = x.TOT.Title,
                                     FirstName = x.TOT.FirstName,
                                     LastName = x.TOT.LastName,
                                     OrderIdentity = x.TOT.OrderIdentity,
                                     TvCountry = x.TOT.Country.Value,
                                     FipNumber = x.TPSL.FIPNumber
                                 }).OrderBy(x => x.OrderIdentity).ToList();

                if (objTvList != null && objTvList.Count > 0)
                {
                    HiddenField hdnTravellerId = (HiddenField)dtlPassngerDetails.Items[0].FindControl("hdnTravellerId");
                    HiddenField hdnPassP2PId = (HiddenField)dtlPassngerDetails.Items[0].FindControl("hdnPassP2PId");
                    DropDownList ddlTitle = (DropDownList)dtlPassngerDetails.Items[0].FindControl("ddlTitle");
                    TextBox txtFirstName = (TextBox)dtlPassngerDetails.Items[0].FindControl("txtFirstName");
                    TextBox txtLastName = (TextBox)dtlPassngerDetails.Items[0].FindControl("txtLastName");
                    TextBox txtFipNumber = (TextBox)dtlPassngerDetails.Items[0].FindControl("txtFipNumber");
                    DropDownList ddlPsrCountry = (DropDownList)dtlPassngerDetails.Items[0].FindControl("ddlPsrCountry");

                    txtFipNumber.Text = !string.IsNullOrEmpty(objTvList.FirstOrDefault().FipNumber) ? objTvList.FirstOrDefault().FipNumber : "";
                    ddlTitle.SelectedIndex = ddlTitle.Items.IndexOf(ddlTitle.Items.FindByText(objTvList.FirstOrDefault().Title));
                    txtFirstName.Text = objTvList.FirstOrDefault().FirstName;
                    txtLastName.Text = objTvList.FirstOrDefault().LastName;
                    ddlPsrCountry.SelectedValue = objTvList.FirstOrDefault().TvCountry.ToString();
                    hdnTravellerId.Value = objTvList.FirstOrDefault().TravellerId.ToString();
                    hdnPassP2PId.Value = objTvList.FirstOrDefault().PassP2PId.ToString();
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    private void GetBillngAddress(long OrderID)
    {
        try
        {
            var objBilling = new ManageQuotation().GetBillingAddress(OrderID);
            if (objBilling != null)
            {
                chkShipping.Checked = true;

                ddlMr.SelectedIndex = ddlMr.Items.IndexOf(ddlMr.Items.FindByText(objBilling.Title));
                txtFirst.Text = objBilling.FirstName;
                txtLast.Text = objBilling.LastName;
                txtEmail.Text = objBilling.EmailAddress;
                txtBillPhone.Text = objBilling.Phone;
                txtAdd.Text = objBilling.Address1;
                txtAdd2.Text = objBilling.Address2;
                txtCity.Text = objBilling.City;
                txtState.Text = objBilling.State;
                txtZip.Text = objBilling.Postcode;
                ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByText(objBilling.Country));

                ddlShipMr.SelectedIndex = ddlShipMr.Items.IndexOf(ddlShipMr.Items.FindByText(objBilling.TitleShpg));
                txtShipFirst.Text = objBilling.FirstNameShpg;
                txtShipLast.Text = objBilling.LastNameShpg;
                txtShipEmail.Text = objBilling.EmailAddressShpg;
                txtShipPhone.Text = objBilling.PhoneShpg;
                txtShipAdd.Text = objBilling.Address1Shpg;
                txtShipAdd2.Text = objBilling.Address2Shpg;
                txtShipCity.Text = objBilling.CityShpg;
                txtShipState.Text = objBilling.StateShpg;
                txtShipZip.Text = objBilling.PostcodeShpg;
                ddlShipCountry.SelectedIndex = ddlShipCountry.Items.IndexOf(ddlShipCountry.Items.FindByText(objBilling.CountryShpg));
            }

            var objNote = _db.tblOrderNotes.FirstOrDefault(x => x.OrderID == OrderID);
            if (objNote != null)
            {
                txtParagraph.Text = objNote.Notes;
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}