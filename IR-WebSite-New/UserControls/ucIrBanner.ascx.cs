﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using Business;
using OneHubServiceRef;
using System.Web.UI.HtmlControls;
using System.Web.Script.Serialization;

public partial class UserControls_ucIrBanner : System.Web.UI.UserControl
{
    private readonly Masters _master = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    Guid pageID, siteId;
    public string siteURLS;
    public string bannerUrls = string.Empty;
    public string frontSiteUrl, ContinentName;
    public string liItems = string.Empty;
    public List<ImageBannerUrl> BannerList = new List<ImageBannerUrl>();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (Session["siteId"] != null)
                    siteId = Guid.Parse(Session["siteId"].ToString());
                else
                    return;

                int SiteType = _oWebsitePage.GetSiteLayoutType(siteId);
                if (SiteType != 2 && SiteType != 6 && SiteType != 9)
                    return;

                if (Page.RouteData.Values["PageId"] != null)
                    pageID = (Guid)Page.RouteData.Values["PageId"];
                else
                {
                    var pid = _db.tblWebMenus.FirstOrDefault(x => x.Name.ToLower() == "home");
                    if (pid != null)
                        pageID = pid.ID;
                }
                PageContent(pageID, siteId);
                BindPrormotion(siteId);
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void PageContent(Guid pageID, Guid siteID)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageID && x.SiteID == siteID);
            if (result != null)
            {
                var url = result.Url;
                var oPage = _master.GetPageDetailsByUrl(url);
                string[] arrListId = oPage.BannerIDs.Split(',');
                var idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _master.GetBannerImgByID(idList);
                foreach (var data in list)
                {
                    BannerList.Add(new ImageBannerUrl { BannerUrl = SiteUrl + "" + data.ImgUrl });
                }

                SearchText.InnerHtml = string.IsNullOrEmpty(result.HomeSearchText) ? "Where do you want to go ?" : result.HomeSearchText;
                CountrySearchText.InnerHtml = string.IsNullOrEmpty(result.HomeCountryText) ? "SEE ALL THE COUNTRIES <br> YOU COULD EXPLORE" : result.HomeCountryText;
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void BindPrormotion(Guid siteID)
    {
        try
        {
            int count = 0, countBanner = 0, promotionCount = 0;
            var result = _master.GetEurailPromotion(siteID);
            var EurailPromotion = result.Where(x => x.IsActive == true).OrderBy(x => x.sortorder).ThenBy(x => x.Title).ToList();
            if ((BannerList != null && BannerList.Count > 0) || (EurailPromotion != null && EurailPromotion.Count > 0))
            {
                countBanner = BannerList != null && BannerList.Count > 0 ? BannerList.Count : 0;
                promotionCount = EurailPromotion != null && EurailPromotion.Count > 0 ? EurailPromotion.Count : 0;
                List<Masters.PromotionList> newEurailPromotion = new List<Masters.PromotionList>();
                List<promotionWithBanner> promotionWithBannerList = new List<promotionWithBanner>();
                if (countBanner == promotionCount)
                {
                    for (int i = 0; i < EurailPromotion.Count; i++)
                    {
                        promotionWithBannerList.Add(new promotionWithBanner
                        {
                            BannerUrl = BannerList[i].BannerUrl,
                            AltText = GetAltText(i),
                            Title = GetTitleText(i),
                            PromotionList = new List<PromotionsList> 
                            {
                                new PromotionsList {ID=EurailPromotion[i].ID, Title=EurailPromotion[i].Title, Description=EurailPromotion[i].Description, NavUrl=EurailPromotion[i].NavUrl}
                            }
                        });
                    }
                    rptEurailPromotion.DataSource = promotionWithBannerList;
                    rptEurailPromotion.DataBind();
                    count = EurailPromotion.Count;
                }
                else if (countBanner > promotionCount)
                {
                    if (promotionCount > 0)
                    {
                        newEurailPromotion.AddRange(EurailPromotion);
                        for (int i = promotionCount; i < countBanner; i++)
                        {
                            newEurailPromotion.Add(new Masters.PromotionList { Title = newEurailPromotion[0].Title, Description = newEurailPromotion[0].Description, NavUrl = newEurailPromotion[0].NavUrl, ID = newEurailPromotion[0].ID });
                        }
                        for (int i = 0; i < newEurailPromotion.Count; i++)
                        {
                            promotionWithBannerList.Add(new promotionWithBanner
                            {
                                BannerUrl = BannerList[i].BannerUrl,
                                AltText = GetAltText(i),
                                Title = GetTitleText(i),
                                PromotionList = new List<PromotionsList> 
                                {
                                    new PromotionsList {ID=newEurailPromotion[i].ID,Title=newEurailPromotion[i].Title,Description=newEurailPromotion[i].Description,NavUrl=newEurailPromotion[i].NavUrl}
                                }
                            });
                        }
                    }
                    else
                    {
                        for (int i = 0; i < BannerList.Count; i++)
                        {
                            promotionWithBannerList.Add(new promotionWithBanner { BannerUrl = BannerList[i].BannerUrl, AltText = GetAltText(i), Title = GetTitleText(i), PromotionList = null });
                        }
                    }
                    rptEurailPromotion.DataSource = promotionWithBannerList;
                    rptEurailPromotion.DataBind();
                    count = countBanner;
                }
                else if (countBanner < promotionCount)
                {
                    for (int i = 0; i < EurailPromotion.Count; i++)
                    {
                        promotionWithBannerList.Add(new promotionWithBanner
                        {
                            PromotionList = new List<PromotionsList> 
                                {
                                    new PromotionsList {ID=EurailPromotion[i].ID,Title=EurailPromotion[i].Title,Description=EurailPromotion[i].Description,NavUrl=EurailPromotion[i].NavUrl}
                                }
                        });
                    }
                    if (countBanner > 0)
                    {
                        for (int i = countBanner; i < promotionCount; i++)
                        {
                            BannerList.Add(new ImageBannerUrl { BannerUrl = BannerList[0].BannerUrl });
                        }
                        for (int i = 0; i < promotionCount; i++)
                        {
                            promotionWithBannerList[i].Title = GetTitleText(i);
                            promotionWithBannerList[i].AltText = GetAltText(i);
                            promotionWithBannerList[i].BannerUrl = BannerList[i].BannerUrl;
                        }
                    }
                    else
                    {
                        for (int i = 0; i < promotionCount; i++)
                        {
                            promotionWithBannerList[i].Title = GetTitleText(i);
                            promotionWithBannerList[i].AltText = GetAltText(i);
                            promotionWithBannerList[i].BannerUrl = "https://1track.internationalrail.net/CMSImages/Homepage-slider-train-3.jpg";
                        }
                    }
                    rptEurailPromotion.DataSource = promotionWithBannerList;
                    rptEurailPromotion.DataBind();
                    count = promotionCount;
                }
            }
            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    if (i == 0)
                        liItems += "<li data-target='#carousel-example-generic' data-slide-to='" + i + "' class='active'></li>";
                    else
                        liItems += "<li data-target='#carousel-example-generic' data-slide-to='" + i + "' class=''></li>";
                }
                listItem.InnerHtml = liItems;
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public string GetAltText(int count)
    {
        switch (count)
        {
            case 0:
                return "Book Train Tickets";
            case 1:
                return "Buy Train Tickets";
            case 2:
                return "International Train Tickets";
            case 3:
                return "Train Reservations, Train Tickets";
            case 4:
                return "Book Your Train Tickets";
            case 5:
                return "Train Tickets";
            case 6:
                return "Train Reservations";
            case 7:
                return "Buy Train Tickets";
        }
        return "";
    }

    public string GetTitleText(int count)
    {
        switch (count)
        {
            case 0:
                return "Book Train Tickets – International Rail";
            case 1:
                return "Buy Train Tickets – International Rail";
            case 2:
                return "International Train Tickets";
            case 3:
                return "Train Reservations Buy International Rail";
            case 4:
                return "Book Your Train Tickets";
            case 5:
                return "Book Your Train Tickets";
            case 6:
                return "Buy Train Tickets";
            case 7:
                return "Buy Train Tickets";
        }
        return "";
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    public class ImageBannerUrl
    {
        public string BannerUrl { get; set; }
    }

    public class PromotionsList
    {
        public Guid ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string NavUrl { get; set; }
    }

    public class promotionWithBanner
    {
        public string BannerUrl { get; set; }
        public List<PromotionsList> PromotionList { get; set; }
        public string AltText { get; set; }
        public string Title { get; set; }
    }
}