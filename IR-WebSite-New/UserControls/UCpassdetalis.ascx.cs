﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;
using System.Threading;
using System.Web.UI.HtmlControls;

public partial class UserControls_passdetalis : System.Web.UI.UserControl
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    string con = System.Configuration.ConfigurationManager.ConnectionStrings["db_Entities"].ToString();
    ManageTrainDetails _master = new ManageTrainDetails();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    ManageBooking _masterBooking = new ManageBooking();
    readonly Masters _masterPage = new Masters();
    public List<getRailPassData> list = new List<getRailPassData>();
    public string currency = "$";
    CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
    Guid siteId;
    Guid countryID;
    public string TkpAmount = "0";
    public string TicketProtection = "style='display:none;'";
    readonly ManageUser _ManageUser = new ManageUser();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());

    }

    public void PageconditionBind()
    {
        try
        {
            var data = _db.tblTicketProtections.FirstOrDefault(t => t.SiteID == siteId && t.IsActive);
            if (data != null)
            {
                TkpAmount = Convert.ToString(data.Amount);
                var istckProt = _masterPage.IsTicketProtection(siteId);
                if (istckProt != null && istckProt)
                    TicketProtection = "style='display:block'";
            }
            var list = FrontEndManagePass.GetTicketProtectionPrice(siteId);
            if (list != null)
                divpopupdata.InnerHtml = list.Description;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        PageconditionBind();
        long POrderID = Convert.ToInt64(Session["OrderID"]);
        GetCurrencyCode();
        if (!IsPostBack)
        {
            FetchRailPassData();
            FillAgentDetails();
        }
        SwapDropdown();
    }

    public bool getvalidcountryid(Guid pid, Guid cid)
    {
        try
        {
            return _db.tblProductPermittedLookups.Any(ty => ty.ProductID == pid && ty.CountryID == cid);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void FetchRailPassData()
    {
        try
        {
            if (Session["RailPassData"] != null)
            {
                list = Session["RailPassData"] as List<getRailPassData>;
                rptPassDetail.DataSource = list;
                rptPassDetail.DataBind();
            }
            else
            {
                Response.Redirect("home");
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void rptPassDetail_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var txtStartDate = e.Item.FindControl("txtStartDate") as TextBox;
            var ddlTitle = e.Item.FindControl("ddlTitle") as DropDownList;
            var txtFirstName = e.Item.FindControl("txtFirstName") as TextBox;
            var txtMiddleName = e.Item.FindControl("txtMiddleName") as TextBox;
            var txtLastName = e.Item.FindControl("txtLastName") as TextBox;
            var txtPassportNumber = e.Item.FindControl("txtPassportNumber") as TextBox;
            var hdnproductid = e.Item.FindControl("hdnproductid") as HiddenField;
            var ddlCountry = e.Item.FindControl("ddlCountry") as DropDownList;
            var ddlNationality = e.Item.FindControl("ddlNationality") as DropDownList;
            var hdnfromdate = e.Item.FindControl("hdnfromdate") as HiddenField;
            var hdntodate = e.Item.FindControl("hdntodate") as HiddenField;
            var hdnmsg = e.Item.FindControl("hdnmsg") as HiddenField;
            var passno = e.Item.FindControl("passno") as Label;
            var lblLID = e.Item.FindControl("lblLID") as HiddenField;
            var hdnproductname = e.Item.FindControl("hdnproductname") as HiddenField;
            var hdnCountryLevelIDs = e.Item.FindControl("hdnCountryLevelIDs") as HiddenField;
            var lblCountryName = e.Item.FindControl("lblCountryName") as Label;
            var ddlDay = e.Item.FindControl("ddlDay") as DropDownList;
            var ddlMonth = e.Item.FindControl("ddlMonth") as DropDownList;
            var ddlYear = e.Item.FindControl("ddlYear") as DropDownList;
            var lnkRemovePass = e.Item.FindControl("lnkRemovePass") as LinkButton;
            var hdnSaver = e.Item.FindControl("hdnSaver") as HiddenField;
            var hdnbookingfee = e.Item.FindControl("hdnbookingfee") as HiddenField;
            int startYear = DateTime.Now.Year;
            var hdnCategoryID = e.Item.FindControl("hdnCategoryID") as HiddenField;
            var alertboxstartday = e.Item.FindControl("alertboxstartday") as HtmlGenericControl;
            var alertboxstartdaymobile = e.Item.FindControl("alertboxstartdaymobile") as HtmlGenericControl;
            var Lblalertboxstartday = e.Item.FindControl("Lblalertboxstartday") as HtmlGenericControl;
            var LblalertboxstartdayM = e.Item.FindControl("LblalertboxstartdayM") as HtmlGenericControl;
            var lblHeaderNote = e.Item.FindControl("lblHeaderNote") as Label;
            var divMiddleName = e.Item.FindControl("divMiddleName") as HtmlGenericControl;
            var divLastName = e.Item.FindControl("divLastName") as HtmlGenericControl;

            Guid ProductId = Guid.Parse(hdnproductid.Value);
            bool AllowMiddleName = _db.tblProducts.Any(x => x.IsAllowMiddleName == true && x.ID == ProductId);
            if (lblHeaderNote != null && AllowMiddleName)// hdnproductid.Value.ToUpper().Contains("621785C4-FF2E-4D19-A6F9-7CAFB7BA6F0F")
            {
                lblHeaderNote.Text = "NAMES MUST BE ENTERED EXACTLY AS PER PASSPORT INCLUDING ALL MIDDLE NAMES";
                divMiddleName.Visible = true;
            }
            else
            {
                lblHeaderNote.Text = "SPELLING OF FIRST AND LAST NAME MUST BE AS PER PASSPORT";
                divMiddleName.Visible = false;
                divLastName.Attributes.Add("style", "width: 100%;");
            }
            Guid CategoryId = Guid.Parse(hdnCategoryID.Value);
            var CatData = _db.tblCategories.FirstOrDefault(t => t.ID == CategoryId);
            bool Britrail = (CatData != null ? CatData.IsBritRailPass : false);
            if (hdnSaver.Value.Contains("Saver"))
            {
                if (Britrail)
                    lnkRemovePass.OnClientClick = "if(!saverhas('3')) return false;";
                else
                    lnkRemovePass.OnClientClick = "if(!saverhas('2')) return false;";
            }
            else
                lnkRemovePass.OnClientClick = "if(!savernothas()) return false;";

            txtStartDate.Attributes.Add("readonly", "readonly");
            passno.Text = (e.Item.ItemIndex + 1) + "";

            for (int i = startYear; i >= startYear - 100; i--)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlYear.Items.Insert(0, new ListItem("YYYY", "YYYY"));
            /*Daterange for startdate*/
            Guid product = Guid.Empty;
            if (!string.IsNullOrEmpty(hdnproductid.Value))
            {
                product = Guid.Parse(hdnproductid.Value);
                var data = _db.tblProducts.FirstOrDefault(x => x.ID == product);
                if (data != null && Lblalertboxstartday != null && LblalertboxstartdayM != null)
                {
                    if (string.IsNullOrEmpty(data.StartDayText))
                        LblalertboxstartdayM.Visible = Lblalertboxstartday.Visible = false;
                    else
                    {
                        alertboxstartday.InnerHtml = data.StartDayText.Replace(Environment.NewLine, "<br />");
                        alertboxstartdaymobile.InnerHtml = data.StartDayText.Replace(Environment.NewLine, "<br />");
                    }

                    hdnfromdate.Value = data.ProductValidFromDate.ToString("MM/dd/yyyy");
                    hdntodate.Value = data.ProductValidToDate.ToString("MM/dd/yyyy");
                    hdnmsg.Value = "You can only purchase this pass for travel dates from " + data.ProductValidFromDate.ToString("dd/MMM/yyyy") + " to " + data.ProductValidToDate.ToString("dd/MMM/yyyy") + ".";
                }
            }
            /*end startdate*/

            ddlNationality.DataSource = _master.GetNationality();
            ddlNationality.DataValueField = "Adjective";
            ddlNationality.DataTextField = "Adjective";
            ddlNationality.DataBind();
            ddlNationality.Items.Insert(0, new ListItem("Choose a country", "0"));

            var Countrydata = new List<tblCountriesMst>();
            if (_db.tblProductCategoriesLookUps.FirstOrDefault(x => x.ProductID == ProductId).tblCategory.IsInterRailPass || hdnproductname.Value.ToLower().Contains("interrail"))
                Countrydata = _master.GetCountryDetail().Where(x => x.IsInterrail).ToList();
            else
                Countrydata = _master.GetCountryDetail().Where(x => x.IsEurail).ToList();

            ddlCountry.DataSource = Countrydata;
            ddlCountry.DataValueField = "CountryID";
            ddlCountry.DataTextField = "CountryName";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("Choose a country", "0"));

            var productid = string.IsNullOrEmpty(hdnproductid.Value) ? new Guid() : Guid.Parse(hdnproductid.Value);
            bool result = getvalidcountryid(productid, countryID);
            if (!String.IsNullOrEmpty(txtLastName.Text))
            {
                Guid IdSession = Guid.Parse(lblLID.Value);
                if (list != null && list.Count > 0)
                {
                    ddlCountry.SelectedValue = list.FirstOrDefault(t => t.Id == IdSession).Country.ToString();
                }
            }
            else if (result && String.IsNullOrEmpty(txtLastName.Text))
                ddlCountry.SelectedValue = countryID.ToString();
            else if (String.IsNullOrEmpty(txtLastName.Text))
                ddlCountry.SelectedIndex = 0;

            if (Session["DetailRailPass"] != null)
            {
                var list2 = Session["DetailRailPass"] as List<getpreviousShoppingData>;
                Guid ID = Guid.Parse(lnkRemovePass.CommandArgument);
                var data2 = list2 == null ? null : list2.FirstOrDefault(ty => ty.Id == ID);
                if (data2 != null)
                {
                    string dob = data2.DOB;
                    string[] bdob = dob.Split('/');
                    ddlDay.SelectedValue = bdob[0];
                    ddlMonth.SelectedValue = bdob[1];
                    ddlYear.SelectedValue = bdob[2];
                    txtStartDate.Text = data2.Date;
                    ddlTitle.SelectedValue = data2.Title;
                    txtFirstName.Text = data2.FirstName;
                    txtMiddleName.Text = data2.MiddleName;
                    txtLastName.Text = data2.LastName;
                    ddlCountry.SelectedValue = data2.Country;
                    txtPassportNumber.Text = data2.Passoprt;
                    ddlNationality.SelectedValue = data2.Nationality;
                    chkTicketProtection.Checked = data2.TicketProtection;
                }
            }
            string counrty = _master.GetEurailCountryNames(hdnCountryLevelIDs.Value);
            if (!string.IsNullOrEmpty(counrty))
                lblCountryName.Text = "(" + counrty + ")";
            var Div_Eurail = e.Item.FindControl("Div_IsEurailPass") as HtmlGenericControl;
            var reqvalerrorDay = e.Item.FindControl("reqvalerrorDay") as RequiredFieldValidator;
            var reqvalerrorMonth = e.Item.FindControl("reqvalerrorMonth") as RequiredFieldValidator;
            var reqvalerrorYear = e.Item.FindControl("reqvalerrorYear") as RequiredFieldValidator;
            bool DOBVisible = !_masterBooking.IsDOB(Guid.Parse(hdnproductid.Value));
            if (DOBVisible)
            {
                Div_Eurail.Attributes.Add("style", "display:none;");
                reqvalerrorDay.Enabled = reqvalerrorMonth.Enabled = reqvalerrorYear.Enabled = false;
            }
        }
    }

    protected void rptPassDetail_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "RemovePass")
        {
            int no = 2;
            ManageProduct _Product = new ManageProduct();
            ManageBooking _masterBookingx = new ManageBooking();
            var DeleteList = new List<ProductPass>();
            var list = new List<getRailPassData>();
            var detaillst = new List<getpreviousShoppingData>();
            Guid Id = Guid.Parse(e.CommandArgument.ToString());
            detaillst = HoldDataDetail();
            if (HttpContext.Current.Session["RailPassData"] != null)
                list = HttpContext.Current.Session["RailPassData"] as List<getRailPassData>;
            var objRPD = list.FirstOrDefault(a => a.Id == Id);
            if (objRPD != null && objRPD.TravellerName.ToLower().Contains("saver"))
            {
                Guid CategoryId = Guid.Parse(objRPD.CategoryID);
                var CatData = _db.tblCategories.FirstOrDefault(t => t.ID == CategoryId);
                bool Britrail = (CatData != null ? CatData.IsBritRailPass : false);
                if (Britrail)
                    no = 3;
                var SaverList = (from a in list
                                 where a.PrdtName == objRPD.PrdtName && a.ValidityName == objRPD.ValidityName
                                 && a.TravellerName.ToLower().Contains("saver")
                                 select a).ToList();
                if (SaverList.Count > no)
                {
                    DeleteList = list.Where(t => t.Id == Id).Select(item => new ProductPass
                    {
                        Price = item.Price,
                        ProductCode = item.PassTypeCode.ToString(),
                        CategoryName = _Product.GetCategoriesNameById(Guid.Parse(item.CategoryID)).FirstOrDefault().Name,
                        Name = item.PrdtName,
                    }).ToList();

                    list.RemoveAll(ty => ty.Id == Id);
                    detaillst.RemoveAll(ty => ty.Id == Id);
                    _masterBookingx.DelpasssaleData(Id);

                    HttpContext.Current.Session["GaTaggingProductlist"] = list.GroupBy(t => new { t.PassTypeCode, t.PrdtName, _Product.GetCategoriesNameById(Guid.Parse(t.CategoryID)).FirstOrDefault().Name, t.Price }).
                    Select(t => new ProductPass
                    {
                        ProductCode = t.Key.PassTypeCode.ToString(),
                        Name = t.Key.PrdtName,
                        CategoryName = t.Key.Name,
                        Price = t.Key.Price,
                        Count = t.Count()
                    }).ToList();
                }
                else
                {
                    foreach (getRailPassData a in SaverList)
                    {
                        var dellst = list.Where(ty => ty.Id == a.Id).Select(item => new ProductPass
                        {
                            Price = item.Price,
                            ProductCode = item.PassTypeCode.ToString(),
                            CategoryName = _Product.GetCategoriesNameById(Guid.Parse(item.CategoryID)).FirstOrDefault().Name,
                            Name = item.PrdtName,
                        }).FirstOrDefault();
                        DeleteList.Add(new ProductPass
                        {
                            Price = dellst.Price,
                            ProductCode = dellst.ProductCode,
                            CategoryName = dellst.CategoryName,
                            Name = dellst.Name,
                        });
                        list.RemoveAll(ty => ty.Id == a.Id);
                        detaillst.RemoveAll(ty => ty.Id == Id);
                        _masterBookingx.DelpasssaleData(a.Id);
                    }

                    HttpContext.Current.Session["GaTaggingProductlist"] = list.GroupBy(t => new
                    {
                        t.PassTypeCode,
                        t.PrdtName,
                        _Product.GetCategoriesNameById(Guid.Parse(t.CategoryID)).FirstOrDefault().Name,
                        t.Price
                    }).Select(t => new ProductPass
                    {
                        ProductCode = t.Key.PassTypeCode.ToString(),
                        Name = t.Key.PrdtName,
                        CategoryName = t.Key.Name,
                        Price = t.Key.Price,
                        Count = t.Count()
                    }).ToList();
                }
            }
            else
            {
                DeleteList = list.Where(t => t.Id == Id).Select(item => new ProductPass
                {
                    Price = item.Price,
                    ProductCode = item.PassTypeCode.ToString(),
                    CategoryName = _Product.GetCategoriesNameById(Guid.Parse(item.CategoryID)).FirstOrDefault().Name,
                    Name = item.PrdtName,
                }).ToList();

                list.RemoveAll(ty => ty.Id == Id);
                detaillst.RemoveAll(ty => ty.Id == Id);
                _masterBookingx.DelpasssaleData(Id);

                HttpContext.Current.Session["GaTaggingProductlist"] = list.GroupBy(t => new
                {
                    t.PassTypeCode,
                    t.PrdtName,
                    _Product.GetCategoriesNameById(Guid.Parse(t.CategoryID)).FirstOrDefault().Name,
                    t.Price
                }).Select(t => new ProductPass
                {
                    ProductCode = t.Key.PassTypeCode.ToString(),
                    Name = t.Key.PrdtName,
                    CategoryName = t.Key.Name,
                    Price = t.Key.Price,
                    Count = t.Count()
                }).ToList();
            }

            #region/*Tagging Code*/
            ManageAnalyticTag _masterAnalyticTag = new ManageAnalyticTag();
            Guid siteId = Guid.Parse(HttpContext.Current.Session["siteId"].ToString());
            var data = _masterAnalyticTag.GetTagBySiteId(siteId);
            if (data != null)
            {
                if (DeleteList != null)
                {
                    string obj = "";
                    string[] SplitData = data.RmvActions.Split(';');
                    string RmvActions = "";
                    foreach (var item in DeleteList)
                    {
                        RmvActions = SplitData[0].Replace("<Product Code>", "" + item.ProductCode + "");
                        RmvActions = RmvActions.Replace("<Product Name>", "" + item.Name.Replace("'", "’") + "");
                        RmvActions = RmvActions.Replace("<Product Category>", "" + item.CategoryName.Replace("'", "’") + "");
                        RmvActions = RmvActions.Replace("<Price>", "" + item.Price + "");
                        RmvActions = RmvActions.Replace("<Quantity>", "1");
                        obj += RmvActions + ";";
                        RmvActions = "";
                    }
                    if (SplitData.Count() > 1)
                        foreach (var item in DeleteList)
                        {
                            RmvActions = SplitData[1].Replace("<Product Code>", "" + item.ProductCode + "");
                            RmvActions = RmvActions.Replace("<Product Name>", "" + item.Name.Replace("'", "’") + "");
                            RmvActions = RmvActions.Replace("<Product Category>", "" + item.CategoryName.Replace("'", "’") + "");
                            RmvActions = RmvActions.Replace("<Price>", "" + item.Price + "");
                            RmvActions = RmvActions.Replace("<Quantity>", "1");
                            obj += RmvActions + ";";
                            RmvActions = "";
                        }
                    HttpContext.Current.Session["RemoveProductTags"] = "<script type='text/javascript'>/*ActionRemoveProduct*/" + obj + data.RmvSetActions + "</script>";
                }
            }
            #endregion

            Session.Add("RailPassData", list);
            Session.Add("DetailRailPass", detaillst);
            if (list != null && list.Count == 0)
                Response.Redirect("/");
            rptPassDetail.DataSource = list;
            rptPassDetail.DataBind();
            FillAgentDetails();
            ScriptManager.RegisterStartupScript(Page, GetType(), "getdatacall", "shipingchk();pophide();updateCartBucket(" + list.Count + ");callvalerror();", true);
        }
    }

    public List<getpreviousShoppingData> HoldDataDetail()
    {
        var detaillst = new List<getpreviousShoppingData>();
        foreach (RepeaterItem item in rptPassDetail.Items)
        {
            var ddlTitle = item.FindControl("ddlTitle") as DropDownList;
            var ddlCountry = item.FindControl("ddlCountry") as DropDownList;
            var ddlNationality = item.FindControl("ddlNationality") as DropDownList;
            var ddlDay = item.FindControl("ddlDay") as DropDownList;
            var ddlMonth = item.FindControl("ddlMonth") as DropDownList;
            var ddlYear = item.FindControl("ddlYear") as DropDownList;
            var txtFirstName = item.FindControl("txtFirstName") as TextBox;
            var txtMiddleName = item.FindControl("txtMiddleName") as TextBox;
            var txtLastName = item.FindControl("txtLastName") as TextBox;
            var txtPassportNumber = item.FindControl("txtPassportNumber") as TextBox;
            var txtStartDate = item.FindControl("txtStartDate") as TextBox;
            var ID = item.FindControl("lblLID") as HiddenField;
            var TicketProtection = item.FindControl("chkTicketProtection") as CheckBox;

            Guid Ids = Guid.Parse(ID.Value);

            getpreviousShoppingData obj = new getpreviousShoppingData();
            obj.Id = Ids;
            obj.Title = ddlTitle.SelectedValue;
            obj.Country = ddlCountry.SelectedValue;
            obj.Nationality = ddlNationality.SelectedValue;
            obj.FirstName = txtFirstName.Text;
            obj.MiddleName = txtMiddleName.Text;
            obj.LastName = txtLastName.Text;
            obj.Passoprt = txtPassportNumber.Text;
            obj.DOB = ddlDay.SelectedValue + "/" + ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue;
            obj.Date = txtStartDate.Text;
            obj.TicketProtection = TicketProtection.Checked;
            detaillst.Add(obj);
        }
        return detaillst;
    }

    public void GetCurrencyCode()
    {

        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            countryID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCountryID;
            currency = oManageClass.GetCurrency((Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID);
        }
    }

    public bool btnChkOut_Click()
    {
        bool returnresult = true;
        try
        {
            Guid SaverCountry = Guid.Empty;
            var textInfo = cultureInfo.TextInfo;
            bool flagInvalidDt = false;
            if (rptPassDetail.Items.Count > 0)
            {
                long POrderID = 0;
                string AffiliateCode = string.Empty;
                var AgentID = Guid.Empty;
                var UserID = Guid.Empty;

                if (Session["AffCode"] != null)
                    AffiliateCode = Session["AffCode"].ToString();
                if (AgentuserInfo.UserID != Guid.Empty && AgentuserInfo.IsAffliate)
                    AffiliateCode = _masterBooking.GetAffiliateCode(AgentuserInfo.UserID);
                else if (AgentuserInfo.UserID != Guid.Empty && !AgentuserInfo.IsAffliate)
                    AgentID = AgentuserInfo.UserID;
                else if (USERuserInfo.ID != Guid.Empty)
                    UserID = USERuserInfo.ID;

                if (Session["OrderID"] == null)
                {
                    POrderID = _masterBooking.CreateOrder(AffiliateCode, AgentID, UserID, siteId, "UCPass", GetBrowserData());
                    Session["OrderID"] = POrderID;
                }
                else
                {
                    POrderID = Convert.ToInt64(Session["OrderID"]);
                    var result = _masterBooking.DeleteOldOrderPassSales(POrderID);
                    POrderID = _masterBooking.updateExistsOrder(AffiliateCode, AgentID, UserID, siteId, POrderID, GetBrowserData());
                }

                var lstRPData = new List<getRailPassData>();
                list = Session["RailPassData"] as List<getRailPassData>;
                #region foreach
                foreach (RepeaterItem it in rptPassDetail.Items)
                {
                    Guid productid = Guid.Empty;
                    decimal ticketprotectonFee = 0;
                    var IdInsert = Guid.NewGuid();
                    var HdnOriginalPrice = it.FindControl("HdnOriginalPrice") as HiddenField;
                    var hdnPrdtId = it.FindControl("hdnPrdtId") as HiddenField;
                    var hdnPassSale = it.FindControl("hdnPassSale") as HiddenField;
                    var hdnSalePrice = it.FindControl("hdnSalePrice") as HiddenField;
                    string[] str = hdnPassSale.Value.Split(',');
                    var LID = it.FindControl("lblLID") as HiddenField;
                    var txtStartDate = it.FindControl("txtStartDate") as TextBox;
                    var chkTicketProtection = it.FindControl("chkTicketProtection") as CheckBox;
                    //var txtDob = it.FindControl("txtDob") as TextBox;
                    var ddlTitle = it.FindControl("ddlTitle") as DropDownList;
                    var txtFirstName = it.FindControl("txtFirstName") as TextBox;
                    var txtMiddleName = it.FindControl("txtMiddleName") as TextBox;
                    var txtLastName = it.FindControl("txtLastName") as TextBox;
                    var ddlCountry = it.FindControl("ddlCountry") as DropDownList;
                    var ddlNationality = it.FindControl("ddlNationality") as DropDownList;
                    var txtPassportNumber = it.FindControl("txtPassportNumber") as TextBox;
                    var hdnproductid = it.FindControl("hdnproductid") as HiddenField;
                    /*mobile view*/
                    var ddlDay = it.FindControl("ddlDay") as DropDownList;
                    var ddlMonth = it.FindControl("ddlMonth") as DropDownList;
                    var ddlYear = it.FindControl("ddlYear") as DropDownList;
                    var lblInvalidDate1 = it.FindControl("lblInvalidDate1") as Label;
                    if (chkTicketProtection.Checked)
                        ticketprotectonFee = Convert.ToDecimal(TkpAmount);
                    /*Validate Age*/
                    var hdnCategoryID = it.FindControl("hdnCategoryID") as HiddenField;
                    var hdnSaver = it.FindControl("hdnSaver") as HiddenField;
                    var travellerID = Guid.Parse(str[2]);
                    var validateAge = _db.tblTravelerMsts.FirstOrDefault(x => x.ID == travellerID && x.IsActive);
                    Guid CategoryId = Guid.Parse(hdnCategoryID.Value);
                    var CatData = _db.tblCategories.FirstOrDefault(t => t.ID == CategoryId);
                    bool Britrail = (CatData != null ? CatData.IsBritRailPass : false);
                    if (validateAge != null)
                    {
                        if (hdnSaver.Value.Contains("Saver") && Britrail)
                            validateAge.FromAge = 17;
                        var bdob = ddlDay.SelectedValue + "/" + ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue;
                        if (!string.IsNullOrEmpty(bdob) && bdob != "DD/MM/YYYY")
                        {
                            var dob = Convert.ToDateTime(bdob);
                            var tm = (DateTime.Now - dob);
                            var age = (tm.Days / 365);
                            if (age >= validateAge.FromAge && age <= validateAge.ToAge || validateAge.ToAge == 0)
                            {
                                lblInvalidDate1.Style.Add("display", "none");
                            }
                            else
                            {
                                GetCurrencyCode();
                                flagInvalidDt = true;
                                lblInvalidDate1.Style.Add("display", "block");
                                lblInvalidDate1.Text = "Please make sure the passenger date of birth is correct for the pass you have selected.";
                                returnresult = false;
                                Session.Add("addpos", ddlDay.ClientID);
                                break;
                            }
                        }
                    }
                    /*end dob*/
                    #region update data details
                    var objTraveller = new tblOrderTraveller();
                    if (LID.Value.Trim() == string.Empty)
                        objTraveller.ID = IdInsert;
                    else
                        objTraveller.ID = Guid.Parse(LID.Value);
                    objTraveller.Title = ddlTitle.SelectedItem.Text;
                    objTraveller.LastName = textInfo.ToTitleCase(txtLastName.Text);
                    objTraveller.FirstName = textInfo.ToTitleCase(txtFirstName.Text);
                    objTraveller.MiddleName = textInfo.ToTitleCase(txtMiddleName.Text);
                    objTraveller.Country = Guid.Parse(ddlCountry.SelectedValue);
                    objTraveller.PassportNo = txtPassportNumber.Text;
                    objTraveller.Nationality = ddlNationality.SelectedValue;
                    objTraveller.PassStartDate = Convert.ToDateTime(txtStartDate.Text);
                    bool DOBVisible = _masterBooking.IsDOB(Guid.Parse(hdnproductid.Value));
                    if (DOBVisible)
                        objTraveller.DOB = Convert.ToDateTime(ddlDay.SelectedValue + "/" + ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue);
                    else
                        objTraveller.DOB = null;
                    var Gtps = new tblPassSale();
                    Gtps.ID = IdInsert;
                    Gtps.OrderID = POrderID;
                    Gtps.ProductID = Guid.Parse(str[0]);
                    Gtps.Price = Convert.ToDecimal(str[1]);
                    Gtps.TravellerID = Guid.Parse(str[2]);
                    Gtps.ClassID = Guid.Parse(str[3]);
                    Gtps.ValidityID = Guid.Parse(str[4]);
                    Gtps.CategoryID = Guid.Parse(str[5]);
                    Gtps.Commition = Convert.ToDecimal(str[6]);
                    Gtps.MarkUp = Convert.ToDecimal(str[7]);
                    Gtps.TicketProtection = ticketprotectonFee;
                    Gtps.CPBookingFee = Convert.ToDecimal(hdnbookingfee.Value);
                    Gtps.CountryStartCode = Convert.ToInt32((!string.IsNullOrEmpty(str[8]) ? str[8] : null));
                    Gtps.CountryEndCode = Convert.ToInt32((!string.IsNullOrEmpty(str[9]) ? str[9] : null));
                    Gtps.Country1 = (!string.IsNullOrEmpty(str[10]) ? Guid.Parse(str[10]) : Guid.Empty);
                    Gtps.Country2 = (!string.IsNullOrEmpty(str[11]) ? Guid.Parse(str[11]) : Guid.Empty);
                    Gtps.Country3 = (!string.IsNullOrEmpty(str[12]) ? Guid.Parse(str[12]) : Guid.Empty);
                    Gtps.Country4 = (!string.IsNullOrEmpty(str[13]) ? Guid.Parse(str[13]) : Guid.Empty);
                    Gtps.Country5 = (!string.IsNullOrEmpty(str[14]) ? Guid.Parse(str[14]) : Guid.Empty);
                    //product currency//
                    Gtps.Site_MP = _masterBooking.GetCurrencyMultiplier("SITE", Guid.Parse(str[0]), POrderID);
                    Gtps.USD_MP = _masterBooking.GetCurrencyMultiplier("USD", Guid.Parse(str[0]), POrderID);
                    Gtps.SEU_MP = _masterBooking.GetCurrencyMultiplier("SEU", Guid.Parse(str[0]), POrderID);
                    Gtps.SBD_MP = _masterBooking.GetCurrencyMultiplier("SBD", Guid.Parse(str[0]), POrderID);
                    Gtps.GBP_MP = _masterBooking.GetCurrencyMultiplier("GBP", Guid.Parse(str[0]), POrderID);
                    Gtps.EUR_MP = _masterBooking.GetCurrencyMultiplier("EUR", Guid.Parse(str[0]), POrderID);
                    Gtps.INR_MP = _masterBooking.GetCurrencyMultiplier("INR", Guid.Parse(str[0]), POrderID);
                    Gtps.SEK_MP = _masterBooking.GetCurrencyMultiplier("SEK", Guid.Parse(str[0]), POrderID);
                    Gtps.NZD_MP = _masterBooking.GetCurrencyMultiplier("NZD", Guid.Parse(str[0]), POrderID);
                    Gtps.CAD_MP = _masterBooking.GetCurrencyMultiplier("CAD", Guid.Parse(str[0]), POrderID);
                    Gtps.JPY_MP = _masterBooking.GetCurrencyMultiplier("JPY", Guid.Parse(str[0]), POrderID);
                    Gtps.AUD_MP = _masterBooking.GetCurrencyMultiplier("AUD", Guid.Parse(str[0]), POrderID);
                    Gtps.CHF_MP = _masterBooking.GetCurrencyMultiplier("CHF", Guid.Parse(str[0]), POrderID);
                    Gtps.EUB_MP = _masterBooking.GetCurrencyMultiplier("EUB", Guid.Parse(str[0]), POrderID);
                    Gtps.EUT_MP = _masterBooking.GetCurrencyMultiplier("EUT", Guid.Parse(str[0]), POrderID);
                    Gtps.GBB_MP = _masterBooking.GetCurrencyMultiplier("GBB", Guid.Parse(str[0]), POrderID);
                    Gtps.THB_MP = _masterBooking.GetCurrencyMultiplier("THB", Guid.Parse(str[0]), POrderID);
                    Gtps.SGD_MP = _masterBooking.GetCurrencyMultiplier("SGD", Guid.Parse(str[0]), POrderID);
                    //site currency//
                    Gtps.Site_USD = _masterBooking.GetCurrencyMultiplier("SUSD", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_SEU = _masterBooking.GetCurrencyMultiplier("SSEU", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_SBD = _masterBooking.GetCurrencyMultiplier("SSBD", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_GBP = _masterBooking.GetCurrencyMultiplier("SGBP", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_EUR = _masterBooking.GetCurrencyMultiplier("SEUR", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_INR = _masterBooking.GetCurrencyMultiplier("SINR", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_SEK = _masterBooking.GetCurrencyMultiplier("SSEK", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_NZD = _masterBooking.GetCurrencyMultiplier("SNZD", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_CAD = _masterBooking.GetCurrencyMultiplier("SCAD", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_JPY = _masterBooking.GetCurrencyMultiplier("SJPY", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_AUD = _masterBooking.GetCurrencyMultiplier("SAUD", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_CHF = _masterBooking.GetCurrencyMultiplier("SCHF", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_EUB = _masterBooking.GetCurrencyMultiplier("SEUB", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_EUT = _masterBooking.GetCurrencyMultiplier("SEUT", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_GBB = _masterBooking.GetCurrencyMultiplier("SGBB", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_THB = _masterBooking.GetCurrencyMultiplier("STHB", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_SGD = _masterBooking.GetCurrencyMultiplier("SSGD", Guid.Parse(str[0]), POrderID);

                    Guid PassSaleId = _masterBooking.AddPassSale(Gtps, IdInsert, hdnSalePrice.Value, HdnOriginalPrice.Value);
                    //if (_masterBooking.GetCategoryIsInterrailById(Gtps.CategoryID.Value))
                    //{
                    //    decimal OriginalPrice = !string.IsNullOrEmpty(HdnOriginalPrice.Value) ? Convert.ToDecimal(HdnOriginalPrice.Value) : 0;
                    //    int InterrailPassType = (Gtps.CountryStartCode == 9999 ? 1 : (Gtps.CountryStartCode > 0 && Gtps.CountryStartCode < 245 ? 2 : 0));
                    //    _masterBooking.AddInterrailCoefficients(PassSaleId, InterrailPassType, OriginalPrice, Gtps.ClassID.Value, Gtps.CountryStartCode.Value);
                    //}
                    _masterBooking.AddOrderTraveller(objTraveller, PassSaleId);

                    var objUpdates = new getRailPassData();
                    objUpdates = list != null ? list.Where(a => a.Id == objTraveller.ID).FirstOrDefault() : null;
                    if (objUpdates != null)
                    {
                        objUpdates.Title = ddlTitle.SelectedItem.Text;
                        objUpdates.FirstName = textInfo.ToTitleCase(txtFirstName.Text);
                        objUpdates.MiddleName = textInfo.ToTitleCase(txtMiddleName.Text);
                        objUpdates.LastName = textInfo.ToTitleCase(txtLastName.Text);
                        objUpdates.Country = Guid.Parse(ddlCountry.SelectedValue);
                        objUpdates.PassportNo = txtPassportNumber.Text;
                        if (DOBVisible)
                            objUpdates.DOB = ddlDay.SelectedValue + "/" + ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue;
                        else
                            objUpdates.DOB = null;
                        objUpdates.PassStartDate = Convert.ToDateTime(txtStartDate.Text);
                        lstRPData.Add(objUpdates);
                    }
                    #endregion
                }
                #endregion
                if (!flagInvalidDt)
                    Session["RailPassData"] = lstRPData;
            }
            return returnresult;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public string GetBrowserData()
    {
        System.Web.HttpBrowserCapabilities browser = Request.Browser;
        string BrowserData = "Browser Capabilities;\n"
            + "Type = " + browser.Type + ";\n"
            + "Name = " + browser.Browser + ";\n"
            + "Version = " + browser.Version + ";\n"
            + "Major Version = " + browser.MajorVersion + ";\n"
            + "Minor Version = " + browser.MinorVersion + ";\n"
            + "Platform = " + browser.Platform + ";\n"
            + "Is Beta = " + browser.Beta + ";\n"
            + "Is Crawler = " + browser.Crawler + ";\n"
            + "Is AOL = " + browser.AOL + ";\n"
            + "Is Win16 = " + browser.Win16 + ";\n"
            + "Is Win32 = " + browser.Win32 + ";\n"
            + "Supports Frames = " + browser.Frames + ";\n"
            + "Supports Tables = " + browser.Tables + ";\n"
            + "Supports Cookies = " + browser.Cookies + ";\n"
            + "Supports VBScript = " + browser.VBScript + ";\n"
            + "Supports JavaScript = " +
                browser.EcmaScriptVersion.ToString() + ";\n"
            + "Supports Java Applets = " + browser.JavaApplets + ";\n"
            + "Supports ActiveX Controls = " + browser.ActiveXControls
                  + ";\n"
            + "Supports JavaScript Version = " +
                browser["JavaScriptVersion"] + ";\n";
        return BrowserData;
    }

    public void SwapDropdown()
    {
        var data = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
        if (data != null)
        {
            if (data.DateFormat == 1)
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ddl", "WapppingDdl();", true);
        }
    }

    public class getpreviousShoppingData
    {
        public Guid Id { get; set; }
        public String DOB { get; set; }
        public String Date { get; set; }
        public String Title { get; set; }
        public String FirstName { get; set; }
        public String MiddleName { get; set; }
        public String LastName { get; set; }
        public String Country { get; set; }
        public String Passoprt { get; set; }
        public String Nationality { get; set; }
        public bool TicketProtection { get; set; }
    }

    public void FillAgentDetails()
    {
        try
        {
            if (Session["AgentUserID"] != null)
            {
                var IDuser = Guid.Parse(Session["AgentUserID"].ToString());
                var Agentlist = _ManageUser.AgentDetailsById(IDuser);

                //--Vesa has stop that functionality.

                //var AgentNameAndEmail = _ManageUser.AgentNameEmailById(IDuser);
                if (Agentlist != null && rptPassDetail.Items.Count > 0)
                {
                    TextBox txtAFirstName = rptPassDetail.Items[0].FindControl("txtFirstName") as TextBox;
                    TextBox txtMiddleName = rptPassDetail.Items[0].FindControl("txtMiddleName") as TextBox;
                    TextBox txtALastName = rptPassDetail.Items[0].FindControl("txtLastName") as TextBox;
                    DropDownList ddlATitle = rptPassDetail.Items[0].FindControl("ddlTitle") as DropDownList;

                    //ddlATitle.SelectedValue = Convert.ToInt32(AgentNameAndEmail.Salutation).ToString();
                    //txtAFirstName.Text = AgentNameAndEmail.Forename;
                    //txtALastName.Text = AgentNameAndEmail.Surname;

                    for (int i = 0; i < rptPassDetail.Items.Count; i++)
                    {
                        DropDownList ddlACountry = rptPassDetail.Items[i].FindControl("ddlCountry") as DropDownList;
                        ddlACountry.SelectedValue = Convert.ToString(Agentlist.Country);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}