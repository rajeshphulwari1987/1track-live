﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Configuration;

public partial class UserControls_UCGeneralInformation : System.Web.UI.UserControl
{
    readonly Masters _master = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private Guid siteId;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var list = _db.tblGeneralInformation_.FirstOrDefault(ty => ty.SiteId == siteId);
        if (list != null)
        {
            if(!string.IsNullOrEmpty(list.Imagepath))
            imagetag.Src = ConfigurationManager.AppSettings["HttpAdminHost"]+list.Imagepath;
            if(list.Keywords.Length>85)
            PKeyword.InnerHtml = (list.Keywords).Substring(0, 85) + "»";
            else
                PKeyword.InnerHtml = list.Keywords + "»";
        }
    }
}