﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucIrBanner.ascx.cs" Inherits="UserControls_ucIrBanner" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .imgarrow
    {
        background-image: url('assets/img/icon_arrow.png');
        background-repeat: no-repeat;
        padding: 0 16px 0px 10px;
        margin-left: 5px;
    }
    .seeall i
    {
        padding-top: 3px !important;
    }
</style>
<asp:UpdatePanel ID="updUcIrBanner" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" CssClass="starail-Form-required" />
            </div>
        </asp:Panel>
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators" id="listItem" runat="server" clientidmode="Static">
            </ol>
            <div class="carousel-inner">
                <asp:Repeater ID="rptEurailPromotion" runat="server">
                    <ItemTemplate>
                        <div class="item">
                            <img src='<%#Eval("BannerUrl")%>' alt='<%#Eval("AltText")%>' title='<%#Eval("Title")%>'
                                width="960" height="253" />
                            <asp:Repeater ID="rptInnerEurailPromotion" runat="server" DataSource='<%#Eval("PromotionList") %>'>
                                <ItemTemplate>
                                    <div class="carousel-caption">
                                        <h3 style="font-size: 17px">
                                            <%#Eval("Title")%>
                                        </h3>
                                        <p>
                                            <%# (Server.HtmlDecode(Eval("Description").ToString()).Length > 100) ? Server.HtmlDecode(Eval("Description").ToString()).Substring(0, 100) + "..." : Server.HtmlDecode(Eval("Description").ToString())%></p>
                                        <a class="linkbtn" href='<%# (Eval("NavUrl") != "" && Eval("NavUrl") != null) ? Eval("NavUrl") : frontSiteUrl + "PromotionDetails.aspx?id=" + Eval("ID") %>'>
                                            view more detail</a>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
        <div class="searchbar">
            <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
                <h4 id="SearchText" runat="server">
                    Where do you want to go ?
                </h4>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <div class="input-group">
                    <asp:TextBox ID="txtSearch" runat="server" autocomplete="off" CssClass="form-control"
                        placeholder="Enter a continent, country or pass name" />
                    <asp:RequiredFieldValidator ID="reqvalerrortxtSearch" runat="server" ControlToValidate="txtSearch"
                        ValidationGroup="search" />
                    <span class="input-group-btn">
                        <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-default IR-GaCode" Text="Go"
                            ValidationGroup="search"></asp:Button></span>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 seeall">
                <span class="cornertop">&nbsp;</span> <a href="countries"><span id="CountrySearchText"
                    runat="server">SEE ALL THE COUNTRIES YOU COULD EXPLORE </span><i class="imgarrow">
                    </i></a>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<script type="text/javascript">
    function initcallSearch(isreload) {
        $("#MainContent_IrBanner_txtSearch").CustomSearch({
            border: "1px solid #C0C0C0",
            font_size: "14px",
            ul_background: "rgb(237, 239, 241)",
            getTextFromTitle: false,
            modeStyle: 'manual',
            manual_B_color: '#941e34',
            manual_B_Size: '13px',
            animationDuration: 500,
            recordno: 10,
            NoRecord: "Record Not Found",
            Data_list: countryList,
            reload: isreload
        });
        $("#MainContent_IrBanner_txtSearch").on('keyup', function (event) {
            $("#MainContent_IrBanner_txtSearch").next('div').find("ul").find('li').click(function () {
                $("#txtSearch").val($("#MainContent_IrBanner_txtSearch").val());
                $("#hdnSearchText").val($("#MainContent_IrBanner_txtSearch").val());
                $("#MainContent_btnSearch").trigger('click');
            });
            if (event.keyCode == 13 && $(this).val().length > 0) {
                $("#txtSearch").val($("#MainContent_IrBanner_txtSearch").val());
                $("#hdnSearchText").val($("#MainContent_IrBanner_txtSearch").val());
                $("#MainContent_btnSearch").trigger('click');
            }
        });
    }
</script>
