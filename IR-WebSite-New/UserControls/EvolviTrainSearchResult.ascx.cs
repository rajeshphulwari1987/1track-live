﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using OneHubServiceRef;
using Business;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Web.UI;

public partial class UserControls_EvolviTrainSearchResult : System.Web.UI.UserControl
{
    #region Global Variable
    public string pricePoup = "";
    public string journyTag = "";
    public string outBoundTag = "";
    public string inBoundTag = "";

    private int _index, newindex;
    List<TrainInformation> outboundList = null;
    List<TrainInformation> inboundList = null;

    List<OutTrainTimeDateBENEList> OutTrainTimeList = null;
    List<OutTrainTimeDateBENEList> OutTrainTimeList1 = null;
    List<InTrainTimeDateBENEList> InTrainTimeList = null;
    List<InTrainTimeDateBENEList> InTrainTimeList1 = null;
    List<EvolviReservationRef> EvReservationRefList = new List<EvolviReservationRef>();

    BookingRequestUserControl objBRUC;
    public string currency;
    public Guid currencyID = new Guid();
    Guid siteId;
    ManageBooking _masterBooking = new ManageBooking();
    ManageTrainDetails _master = new ManageTrainDetails();
    readonly Masters _objMaster = new Masters();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public static DateTime curentDateTime;
    public static List<getStationList_Result> list = new List<getStationList_Result>();
    public string _FromDate;
    public string _ToDate;
    List<P2PReservationIDInfo> lstP2PIdInfo = new List<P2PReservationIDInfo>();
    List<BookingRequestInfo> listBookingReqInfo = new List<BookingRequestInfo>();
    List<TrainPrice> listTrainPriceInfo = new List<TrainPrice>();
    Guid id = Guid.NewGuid();
    public string FromLatitude = string.Empty;
    public string FromLongitude = string.Empty;
    public string ToLatitude = string.Empty;
    public string ToLongitude = string.Empty;
    List<ReservationInfo> ReservationInfoList = new List<ReservationInfo>();
    private readonly db_1TrackEntities db = new db_1TrackEntities();
    public string siteURL = string.Empty;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private List<EvFareInformation> EvFareInformationList = new List<EvFareInformation>();
    ManageOneHub _ManageOneHub = new ManageOneHub();
    #endregion

    #region PageEvents
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            hdnReq.Value = Request.QueryString["req"] != null ? Request.QueryString["req"].Trim() : "";
            if (hdnReq.Value == "BE" || hdnReq.Value == "TI" || hdnReq.Value == "NTV")
                return;

            ShowMessage(0, null);
            GetCurrencyCode();
            NotIsPostBack();

            if (Session["ErrorMessage"] != null)
            {
                if (Session["ErrorMessage"].ToString().Trim() != "ErrorMaxDate")
                {
                    ShowMessage(2, Session["ErrorMessage"].ToString());
                    Session.Remove("ErrorMessage");
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }
    #endregion

    void NotIsPostBack()
    {
        if (!IsPostBack)
        {
            try
            {
                hdnsiteURL.Value = new ManageFrontWebsitePage().GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
                Session["SHOPPINGCART"] = null;
                Session["BOOKING-REQUEST"] = null;

                if (Session["BookingUCRerq"] == null)
                    return;
                objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];

                #region Hide rail card option it will be used in future.
                //if ((objBRUC.Seniors > 0 || objBRUC.Youths > 0) && objBRUC.isRailCard == false)
                ////    div_youthSnrMessage.Attributes.Add("style", "display:block;");
                #endregion

                hdnDateDepart.Value = objBRUC.depdt.ToString("dd MMM yyyy");
                hdnDateArr.Value = objBRUC.ReturnDate.Trim();

                GetTraveller();
                BindResult();
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }
    }

    void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
            currency = oManageClass.GetCurrency(currencyID);
            hdnCurrID.Value = currency;
        }
    }

    #region Earlier & Previous Train

    protected void lnkEarlier_Click(object sender, EventArgs e)
    {
        Session["HoldJourneyDetailsList"] = null;
        Session["HoldReserVationRequest"] = null;
        OutBoundSearch(-1);
    }
    protected void lnkLater_Click(object sender, EventArgs e)
    {
        Session["HoldJourneyDetailsList"] = null;
        Session["HoldReserVationRequest"] = null;
        OutBoundSearch(1);
    }
    void OutBoundSearch(int day)
    {
        if (Session["BookingUCRerq"] != null)
        {
            var objBruc = Session["BookingUCRerq"] as BookingRequestUserControl;
            string DeptDate = objBruc.depdt.ToString("dd/MMM/yyyy");
            string DeptTime = objBruc.depTime.ToString("HH");

            DropDownList ddldepTime = ucTrainSearch.FindControl("ddldepTime") as DropDownList;
            TextBox txtDepartureDate = ucTrainSearch.FindControl("txtDepartureDate") as TextBox;
            OutTrainTimeList = Session["OutTrainTimeDateEvolviList"] as List<OutTrainTimeDateBENEList>;
            if (OutTrainTimeList != null && OutTrainTimeList.Count > 0)
            {
                if (day == 1)
                {
                    #region Later Train
                    int time = Convert.ToInt32(OutTrainTimeList.FirstOrDefault(x => x.Type == 1).DeptTme.Substring(0, 2)) + 1;
                    string lastResulDate = OutTrainTimeList.FirstOrDefault(x => x.Type == 1).DeptDate.ToString();
                    objBruc.depdt = DateTime.ParseExact(Convert.ToDateTime(lastResulDate).ToString("dd/MMM/yyyy"), "dd/MMM/yyyy", null);
                    if (time == 24)
                    {
                        time = 1;
                        DateTime dt = Convert.ToDateTime(DeptDate);
                        dt = dt.AddDays(day);
                        DeptDate = dt.ToString("dd/MMM/yyyy");
                        txtDepartureDate.Text = DeptDate;
                        objBruc.depdt = DateTime.ParseExact(DeptDate, "dd/MMM/yyyy", null);
                    }

                    DeptTime = time.ToString().Length == 1 ? "0" + time.ToString() + ":00" : time.ToString() + ":00";
                    ddldepTime.SelectedValue = DeptTime;
                    objBruc.depTime = Convert.ToDateTime(DeptTime);
                    Session["BookingUCRerq"] = objBruc;
                    SearchTrainInfo();
                    BindResult();
                    var isreturn = ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])) != null ? ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.ToList().FirstOrDefault(x => x.IsReturn == false) : null;
                    if (isreturn == null && objBruc != null)
                    {
                        if (!string.IsNullOrEmpty(DeptDate) && !DeptDate.Contains("DD/MM/YYYY"))
                        {
                            DateTime dt = Convert.ToDateTime(DeptDate);
                            dt = DeptTime == "01:00" ? dt.AddDays(0) : dt.AddDays(day);
                            DeptDate = dt.ToString("dd/MMM/yyyy");
                            txtDepartureDate.Text = DeptDate;
                            objBruc.depdt = DateTime.ParseExact(DeptDate, "dd/MMM/yyyy", null);

                            DeptTime = day == 1 ? "02:00" : "20:00";
                            ddldepTime.SelectedValue = DeptTime;
                            objBruc.depTime = Convert.ToDateTime(DeptTime);

                            Session["BookingUCRerq"] = objBruc;
                            SearchTrainInfo();
                            BindResult();
                        }
                    }
                    else
                    {
                        if (((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])) != null)
                        {
                            if (((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.Count() > 0)
                            {
                                var firstOutList = ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.Where(x => x.IsReturn == false).OrderBy(x => Convert.ToDateTime(x.DepartureDate)).ThenBy(x => Convert.ToDateTime(x.DepartureTime)).ToList().Take(1).FirstOrDefault();
                                if (firstOutList != null)
                                {
                                    txtDepartureDate.Text = (Convert.ToDateTime(firstOutList.DepartureDate)).ToString("dd/MMM/yyyy");
                                    objBruc.depdt = DateTime.ParseExact((Convert.ToDateTime(firstOutList.DepartureDate)).ToString("dd/MMM/yyyy"), "dd/MMM/yyyy", null);

                                    ddldepTime.SelectedValue = firstOutList.DepartureTime.Substring(0, 2) + ":00";
                                    objBruc.depTime = Convert.ToDateTime(firstOutList.DepartureTime.Substring(0, 2) + ":00");
                                    Session["BookingUCRerq"] = objBruc;
                                }
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Earlier Train
                    int time = 0;
                    int LessTime = 3;
                    int LastTime = Convert.ToInt32(OutTrainTimeList.FirstOrDefault(x => x.Type == 2).DeptTme.Substring(0, 2));
                    ViewState["OldTime"] = OutTrainTimeList.FirstOrDefault(x => x.Type == 2).DeptTme.Substring(0, 5);
                NotFoundTrian:
                    time = LastTime - LessTime;
                    string lastResulDate = OutTrainTimeList.FirstOrDefault(x => x.Type == 1).DeptDate.ToString();
                    objBruc.depdt = DateTime.ParseExact(Convert.ToDateTime(lastResulDate).ToString("dd/MMM/yyyy"), "dd/MMM/yyyy", null);
                    if (time < 0)
                    {
                        time = LastTime = 20;
                        LessTime = 0;
                        if (!string.IsNullOrEmpty(DeptDate) && !DeptDate.Contains("DD/MM/YYYY"))
                        {
                            DateTime dt = Convert.ToDateTime(DeptDate);
                            dt = dt.AddDays(day);
                            DeptDate = dt.ToString("dd/MMM/yyyy");
                            txtDepartureDate.Text = DeptDate;
                            objBruc.depdt = DateTime.ParseExact(DeptDate, "dd/MMM/yyyy", null);
                        }
                    }
                    DeptTime = time.ToString().Length == 1 ? "0" + time.ToString() + ":00" : time.ToString() + ":00";
                    ddldepTime.SelectedValue = DeptTime;
                    objBruc.depTime = Convert.ToDateTime(DeptTime);
                    Session["BookingUCRerq"] = objBruc;
                    SearchTrainInfo();
                    BindResult();
                    OutTrainTimeList1 = Session["OutTrainTimeDateEvolviList"] as List<OutTrainTimeDateBENEList>;
                    var isreturn = ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])) != null ? ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.ToList().FirstOrDefault(x => x.IsReturn == false) : null;
                    if (OutTrainTimeList1 != null && OutTrainTimeList1.Count > 0 && ViewState["OldTime"] != null)
                    {
                        string newtime = OutTrainTimeList1.FirstOrDefault(x => x.Type == 2).DeptTme.Substring(0, 5);
                        if (newtime == ViewState["OldTime"].ToString())
                        {
                            LessTime = LessTime + 3;
                            goto NotFoundTrian;
                        }
                        else if (isreturn == null && objBruc != null)
                        {
                            DeptTime = day == 1 ? "02:00" : "20:00";
                            ddldepTime.SelectedValue = DeptTime;
                            objBruc.depTime = Convert.ToDateTime(DeptTime);
                            Session["BookingUCRerq"] = objBruc;
                            SearchTrainInfo();
                            BindResult();
                            var isreturnnew = ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.ToList().FirstOrDefault(x => x.IsReturn == false);
                            if ((isreturnnew == null || ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.Count() == 0) && objBruc != null)
                            {
                                LastTime = 20;
                                LessTime = LessTime + 3;
                                goto NotFoundTrian;
                            }
                        }
                    }
                    #endregion
                }
            }
            Response.Redirect("TrainResults.aspx?req=" + Request.QueryString["req"] + "");
        }
    }

    protected void lnkInEarlier_Click(object sender, EventArgs e)
    {
        Session["HoldJourneyDetailsList"] = null;
        Session["HoldReserVationRequest"] = null;
        InBoundSearch(-1);
    }
    protected void lnkInLater_Click(object sender, EventArgs e)
    {
        Session["HoldJourneyDetailsList"] = null;
        Session["HoldReserVationRequest"] = null;
        InBoundSearch(1);
    }
    void InBoundSearch(int day)
    {
        if (Session["BookingUCRerq"] != null)
        {
            var objBruc = Session["BookingUCRerq"] as BookingRequestUserControl;
            string ReturnDate = objBruc.ReturnDate;
            string ReturnTime = objBruc.ReturnTime.Substring(0, 2);

            DropDownList ddlReturnTime = ucTrainSearch.FindControl("ddlReturnTime") as DropDownList;
            TextBox txtReturnDate = ucTrainSearch.FindControl("txtReturnDate") as TextBox;
            InTrainTimeList = Session["InTrainTimeDateEvolviList"] as List<InTrainTimeDateBENEList>;
            if (InTrainTimeList != null && InTrainTimeList.Count > 0)
            {
                if (day == 1)
                {
                    #region Later Train
                    int time = Convert.ToInt32(InTrainTimeList.FirstOrDefault(x => x.Type == 1).ReturnTme.Substring(0, 2)) + 1;
                    string lastResulDate = InTrainTimeList.FirstOrDefault(x => x.Type == 1).ReturnDate.ToString();
                    objBruc.ReturnDate = (DateTime.Parse(lastResulDate)).ToString("dd/MMM/yyyy");
                    if (time == 24)
                    {
                        time = 1;
                        DateTime dt = Convert.ToDateTime(ReturnDate);
                        dt = dt.AddDays(day);
                        ReturnDate = dt.ToString("dd/MMM/yyyy");
                        txtReturnDate.Text = ReturnDate;
                        objBruc.ReturnDate = String.IsNullOrEmpty(ReturnDate) || ReturnDate.Trim() == "DD/MM/YYYY" ? string.Empty : ReturnDate;
                    }
                    ReturnTime = time.ToString().Length == 1 ? "0" + time.ToString() + ":00" : time.ToString() + ":00";
                    ddlReturnTime.SelectedValue = ReturnTime;
                    objBruc.ReturnTime = ReturnTime;
                    Session["BookingUCRerq"] = objBruc;
                    SearchTrainInfo();
                    BindResult();
                    var isreturn = ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])) != null ? ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.ToList().FirstOrDefault(x => x.IsReturn == true) : null;
                    if (objBruc != null && isreturn == null)
                    {
                        if (!string.IsNullOrEmpty(ReturnDate) && !ReturnDate.Contains("DD/MM/YYYY"))
                        {
                            DateTime dt = Convert.ToDateTime(ReturnDate);
                            dt = ReturnTime == "01:00" ? dt.AddDays(0) : dt.AddDays(day);
                            ReturnDate = dt.ToString("dd/MMM/yyyy");
                            txtReturnDate.Text = ReturnDate;
                            objBruc.ReturnDate = String.IsNullOrEmpty(ReturnDate) || ReturnDate.Trim() == "DD/MM/YYYY" ? string.Empty : ReturnDate;

                            ReturnTime = day == 1 ? "02:00" : "20:00";
                            ddlReturnTime.SelectedValue = ReturnTime;
                            objBruc.ReturnTime = ReturnTime;

                            Session["BookingUCRerq"] = objBruc;
                            SearchTrainInfo();
                            BindResult();
                        }
                    }
                    else
                    {
                        if (((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])) != null)
                        {
                            if (((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.Count() > 0)
                            {
                                var firstOutList = ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.Where(x => x.IsReturn == true).OrderBy(x => Convert.ToDateTime(x.DepartureDate)).ThenBy(x => Convert.ToDateTime(x.DepartureTime)).ToList().Take(1).FirstOrDefault();
                                if (firstOutList != null)
                                {
                                    txtReturnDate.Text = Convert.ToDateTime(firstOutList.DepartureDate).ToString("dd/MMM/yyyy");
                                    objBruc.ReturnDate = Convert.ToDateTime(firstOutList.DepartureDate).ToString("dd/MMM/yyyy");

                                    ddlReturnTime.SelectedValue = firstOutList.DepartureTime.Substring(0, 2) + ":00";
                                    objBruc.ReturnTime = firstOutList.DepartureTime.Substring(0, 2) + ":00";
                                    Session["BookingUCRerq"] = objBruc;
                                }
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Earlier Train
                    int time = 0;
                    int LessTime = 3;
                    int LastTime = Convert.ToInt32(InTrainTimeList.FirstOrDefault(x => x.Type == 2).ReturnTme.Substring(0, 2));
                    ViewState["OldTime"] = InTrainTimeList.FirstOrDefault(x => x.Type == 2).ReturnTme.Substring(0, 5);
                NotFoundTrian:
                    time = LastTime - LessTime;
                    string lastResulDate = InTrainTimeList.FirstOrDefault(x => x.Type == 1).ReturnDate.ToString();
                    objBruc.ReturnDate = DateTime.Parse(lastResulDate).ToString("dd/MMM/yyyy");
                    if (time < 0)
                    {
                        time = LastTime = 20;
                        LessTime = 0;
                        if (!string.IsNullOrEmpty(ReturnDate) && !ReturnDate.Contains("DD/MM/YYYY"))
                        {
                            DateTime dt = Convert.ToDateTime(ReturnDate);
                            dt = dt.AddDays(day);
                            ReturnDate = dt.ToString("dd/MMM/yyyy");
                            txtReturnDate.Text = ReturnDate;
                            objBruc.ReturnDate = String.IsNullOrEmpty(ReturnDate) || ReturnDate.Trim() == "DD/MM/YYYY" ? string.Empty : ReturnDate;
                        }
                    }
                    ReturnTime = time.ToString().Length == 1 ? "0" + time.ToString() + ":00" : time.ToString() + ":00";
                    ddlReturnTime.SelectedValue = ReturnTime;
                    objBruc.ReturnTime = ReturnTime;
                    Session["BookingUCRerq"] = objBruc;
                    SearchTrainInfo();
                    BindResult();
                    InTrainTimeList1 = Session["InTrainTimeDateEvolviList"] as List<InTrainTimeDateBENEList>;
                    var isreturn = ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])) != null ? ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.ToList().FirstOrDefault(x => x.IsReturn == true) : null;
                    if (InTrainTimeList1 != null && InTrainTimeList1.Count > 0 && ViewState["OldTime"] != null)
                    {
                        string newtime = InTrainTimeList1.FirstOrDefault(x => x.Type == 2).ReturnTme.Substring(0, 5);
                        if (newtime == ViewState["OldTime"].ToString())
                        {
                            LessTime = LessTime + 3;
                            goto NotFoundTrian;
                        }
                        else if (objBruc != null && isreturn == null)
                        {
                            ReturnTime = day == 1 ? "02:00" : "20:00";
                            ddlReturnTime.SelectedValue = ReturnTime;
                            objBruc.ReturnTime = ReturnTime;
                            Session["BookingUCRerq"] = objBruc;
                            SearchTrainInfo();
                            BindResult();
                            var isreturnnew = ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.ToList().FirstOrDefault(x => x.IsReturn == true);
                            if (((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.Count() > 0 && objBruc != null && isreturnnew == null)
                            {
                                LastTime = 20;
                                LessTime = LessTime + 3;
                                goto NotFoundTrian;
                            }
                        }
                    }
                    #endregion
                }
            }
            Response.Redirect("TrainResults.aspx?req=" + Request.QueryString["req"] + "");
        }
    }

    #endregion

    public void BindResult()
    {
        try
        {
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
            if (pInfoSolutionsResponse.TrainInformationList == null)
                return;
            //Longitude and Latitude
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            FromLatitude = string.IsNullOrEmpty(_objMaster.GetLongitudeLatitude(objBRUC.depstCode.Trim(), "EVOLVI").Latitude) ? "" : _objMaster.GetLongitudeLatitude(objBRUC.depstCode.Trim(), "EVOLVI").Latitude;
            FromLongitude = string.IsNullOrEmpty(_objMaster.GetLongitudeLatitude(objBRUC.depstCode.Trim(), "EVOLVI").Longitude) ? "" : _objMaster.GetLongitudeLatitude(objBRUC.depstCode.Trim(), "EVOLVI").Longitude;
            ToLatitude = string.IsNullOrEmpty(_objMaster.GetLongitudeLatitude(objBRUC.arrstCode.Trim(), "EVOLVI").Latitude) ? "" : _objMaster.GetLongitudeLatitude(objBRUC.arrstCode.Trim(), "EVOLVI").Latitude;
            ToLongitude = string.IsNullOrEmpty(_objMaster.GetLongitudeLatitude(objBRUC.arrstCode.Trim(), "EVOLVI").Longitude) ? "" : _objMaster.GetLongitudeLatitude(objBRUC.arrstCode.Trim(), "EVOLVI").Longitude;

            //Search JourneyBlock-header
            ltrinTo.Text = ltroutFrom.Text = objBRUC.FromDetail;
            ltrinFrom.Text = ltroutTo.Text = objBRUC.ToDetail;
            ltrinDate.Text = string.IsNullOrEmpty(objBRUC.ReturnDate) ? objBRUC.depdt.ToString("dd-MM-yyyy") : Convert.ToDateTime(objBRUC.ReturnDate).ToString("dd-MM-yyyy");
            ltroutDate.Text = objBRUC.depdt.ToString("dd-MM-yyyy");
            ltrPassenger.Text = objBRUC.Adults.ToString() + " Adult, " + objBRUC.Boys.ToString() + " Child, " + objBRUC.Youths.ToString() + " Youth and " + objBRUC.Seniors.ToString() + " Senior";

            // Outbound JourneyBlock-header 
            outboundList = pInfoSolutionsResponse.TrainInformationList.Where(x => x.IsReturn == false).OrderBy(x => Convert.ToDateTime(x.DepartureDate)).ThenBy(x => Convert.ToDateTime(x.DepartureTime)).ToList();
            outboundList = outboundList != null && outboundList.Count > 0 ? outboundList.Where(x => x.PriceInfo.Any(y => y.TrainPriceList != null && y.TrainPriceList.Length > 0)).ToList() : null;
            rptoutEvolvi.DataSource = outboundList;
            rptoutEvolvi.DataBind();

            inboundList = pInfoSolutionsResponse.TrainInformationList.Where(x => x.IsReturn).OrderBy(x => Convert.ToDateTime(x.DepartureDate)).ThenBy(x => Convert.ToDateTime(x.DepartureTime)).ToList();
            inboundList = inboundList != null && inboundList.Count > 0 ? inboundList.Where(x => x.PriceInfo.Any(y => y.TrainPriceList != null && y.TrainPriceList.Length > 0)).ToList() : null;
            ShowInboud.Visible = (inboundList != null && inboundList.Count > 0);

            outBoundTag = (inboundList != null && inboundList.Count > 0 && outboundList != null && outboundList.Count > 0) ? "Inbound Journey" : "Booking Details";
            inBoundTag = (inboundList != null && inboundList.Count > 0) ? "Outbound Journey" : "Booking Details";

            rptinEvolvi.DataSource = inboundList;
            rptinEvolvi.DataBind();


            #region OutBound Earlier later train Serach
            if (outboundList != null && outboundList.Count > 0)
            {
                var LastItemList = outboundList.OrderByDescending(x => Convert.ToDateTime(x.DepartureDate)).ThenByDescending(x => Convert.ToDateTime(x.DepartureTime)).Take(1).ToList();
                var FirstItemList = outboundList.Take(1).ToList();
                OutTrainTimeList = new List<OutTrainTimeDateBENEList>();
                Session["OutTrainTimeDateEvolviList"] = null;
                if (LastItemList != null && LastItemList.Count > 0)
                {
                    foreach (var item in LastItemList)
                    {
                        OutTrainTimeList.Add(new OutTrainTimeDateBENEList()
                        {
                            DeptDate = DateTime.Parse(item.DepartureDate).ToString("dd/MMMM/yyyy"),
                            DeptTme = item.DepartureTime,
                            Type = 1
                        });
                    }
                }
                if (FirstItemList != null && FirstItemList.Count > 0)
                {
                    foreach (var item in FirstItemList)
                    {
                        OutTrainTimeList.Add(new OutTrainTimeDateBENEList()
                        {
                            DeptDate = DateTime.Parse(item.DepartureDate).ToString("dd/MMMM/yyyy"),
                            DeptTme = item.DepartureTime,
                            Type = 2
                        });
                    }
                }
                Session["OutTrainTimeDateEvolviList"] = OutTrainTimeList;
            }
            #endregion

            #region InBound Earlier later train Serach
            if (inboundList != null && inboundList.Count > 0)
            {
                var LastItemList = inboundList.OrderByDescending(x => Convert.ToDateTime(x.DepartureDate)).ThenByDescending(x => Convert.ToDateTime(x.DepartureTime)).Take(1).ToList();
                var FirstItemList = inboundList.Take(1).ToList();
                InTrainTimeList = new List<InTrainTimeDateBENEList>();
                Session["InTrainTimeDateEvolviList"] = null;
                if (LastItemList != null && LastItemList.Count > 0)
                {
                    foreach (var item in LastItemList)
                    {
                        InTrainTimeList.Add(new InTrainTimeDateBENEList()
                        {
                            ReturnDate = DateTime.Parse(item.DepartureDate).ToString("dd/MMM/yyyy"),
                            ReturnTme = item.DepartureTime,
                            Type = 1
                        });
                    }
                }
                if (FirstItemList != null && FirstItemList.Count > 0)
                {
                    foreach (var item in FirstItemList)
                    {
                        InTrainTimeList.Add(new InTrainTimeDateBENEList()
                        {
                            ReturnDate = DateTime.Parse(item.DepartureDate).ToString("dd/MMM/yyyy"),
                            ReturnTme = item.DepartureTime,
                            Type = 2
                        });
                    }
                }
                Session["InTrainTimeDateEvolviList"] = InTrainTimeList;
            }
            #endregion

        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        try
        {
            Session["IsFullPrice"] = null;
            Session["IsCheckout"] = null;
            Session["BOOKING-REQUEST"] = null;
            if (Session["P2POrderID"] != null)
            {
                long orderID = Convert.ToInt64(Session["P2POrderID"]);
                _ManageOneHub.EmptyEvolviBasket(orderID, siteId, true);
            }

            if (Session["P2POrderID"] != null)
            {
                new ManageUser().DeleteExistingP2PSale(Convert.ToInt64(Session["P2POrderID"].ToString()));
                new ManageUser().DeleteExistingP2PSleeperEvolvi(Convert.ToInt64(Session["P2POrderID"].ToString()));
            }
            else
                Session["P2POrderID"] = new ManageBooking().CreateOrder(string.Empty, AgentuserInfo.UserID, Guid.Empty, siteId, "P2P", "0", "EvolviTR");
            Session["ProductType"] = "P2P";

            Boolean res = false;
            SaveTravellers();
            Session["EvolviReservationRef"] = null;

            if (!String.IsNullOrEmpty(hdnFareIdFrom.Value))
                res = BookingRequestEvolvi(hdnFareIdFrom.Value, hdnTrainNoFrom.Value, hdnFareClassFrom.Value, hdnSingleOrDefaultFrom.Value, hdnRestrictionCodeFrom.Value, hdnClassCodeFrom.Value, hdnpriceFrom.Value, hdnIdFrom.Value, hdnTicketTypeCodeFrom.Value, hdnValidityCodeFrom.Value, hdnFareSetterCodeFrom.Value, hdnRefundAndCancellationsFrom.Value, hdnChangesToTravelPlansFrom.Value, "Inbound");
            if (!String.IsNullOrEmpty(hdnFareIdTo.Value))
                res = BookingRequestEvolvi(hdnFareIdTo.Value, hdnTrainNoTo.Value, hdnFareClassTo.Value, hdnSingleOrDefaultTo.Value, hdnRestrictionCodeTo.Value, hdnClassCodeTo.Value, hdnpriceTo.Value, hdnIdTo.Value, hdnTicketTypeCodeTo.Value, hdnValidityCodeTo.Value, hdnFareSetterCodeTo.Value, hdnRefundAndCancellationsTo.Value, hdnChangesToTravelPlansTo.Value, "Outbound");

            SaveTravellerInDB(Convert.ToInt64(Session["P2POrderID"].ToString()));
            Session["P2PIdInfo"] = lstP2PIdInfo;
            Session["JourneyDetauls"] = null;
            if (res == true)
            {
                Session["HoldOldJourneyResponse"] = null;
                Response.Redirect("Reservation.aspx?req=EV");
            }
            else
                Response.Redirect("TrainResults.aspx?req=EV", true);
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public Boolean BookingRequestEvolvi(string jFareId, string jTrainNo, string jFareClass, string jSingleOrDefault, string jRestrictionCode, string jClassCode, string jprice, string jId, string jTicketTypeCode, string jvalidityCode, string jfareSetterCode, string jRefundAndCancellations, string jChangesToTravelPlans, string TypeOfJ)
    {
        try
        {
            string evpricevalue = "0";
            var objTravellLists = Session["TravellerList"] as List<TravellersInfo>;
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
            if (TypeOfJ == "Inbound")
                ReservationInfoList = new List<ReservationInfo>();
            else if (Session["ReservationInfoList"] != null)
                ReservationInfoList = Session["ReservationInfoList"] as List<ReservationInfo>;

            if (pInfoSolutionsResponse != null)
            {
                TrainInformation trainInfo = pInfoSolutionsResponse.TrainInformationList.FirstOrDefault(tp => tp.TrainNumber == jTrainNo);
                if (trainInfo != null)
                {
                    var priceInfo = trainInfo.PriceInfo != null ? trainInfo.PriceInfo.SelectMany(x => x.TrainPriceList != null ? x.TrainPriceList.Where(y => y.FareId == jFareId && y.FareClass == jFareClass && y.SingleOrReturn == jSingleOrDefault && y.RestrictionCode == jRestrictionCode && y.TicketTypeCode == jTicketTypeCode) : null).FirstOrDefault() : null;
                    if (priceInfo != null)
                    {
                        var fareInfo = priceInfo.FarePricesInfo != null ? priceInfo.FarePricesInfo.FirstOrDefault(x => x.ClassCode == jClassCode) : null;
                        if (fareInfo != null)
                        {
                            var srcCurId = FrontEndManagePass.GetCurrencyID("GBP");
                            ReservationInfoList.Add(new ReservationInfo
                            {
                                JourneyIdentifier = trainInfo.TrainNumber,
                                FareId = priceInfo.FareId,
                                JourneyType = trainInfo.IsReturn ? "Inbound" : "Outbound",
                                Price = trainInfo.IsReturn && priceInfo.SingleOrReturn == "R" ? evpricevalue : jprice,
                                ReservationStatus = priceInfo.ReservableStatus,
                                FareSetterCode = priceInfo.FareSetterCode
                            });

                            #region Cart Information
                            List<ShoppingCartDetails> listCart = trainInfo != null && objTravellLists != null && objTravellLists.Count > 0 ? new List<ShoppingCartDetails> {new ShoppingCartDetails
                            {
                                Id = id,
                                DepartureStation = trainInfo.DepartureStationName,
                                DepartureDate = trainInfo.DepartureDate,
                                DepartureTime = trainInfo.DepartureTime,

                                ArrivalStation = trainInfo.ArrivalStationName,
                                ArrivalDate = trainInfo.ArrivalDate,
                                ArrivalTime = trainInfo.ArrivalTime,

                                TrainNo = trainInfo.TrainNumber,
                                Title = objTravellLists.FirstOrDefault(x=>x.Lead).Title ,
                                FirstName = objTravellLists.FirstOrDefault(x=>x.Lead).FName ,
                                LastName= objTravellLists.FirstOrDefault(x=>x.Lead).LName,
                                Passenger = priceInfo != null ? priceInfo.NumAdults + " Adults, " + priceInfo.NumBoys + " Child," + priceInfo.NumYouths + " Youths," + priceInfo.NumSeniors + " Seniors":"",
                                ServiceName = fareInfo != null ?  fareInfo.ClassCode :"",
                                Fare = priceInfo != null ? priceInfo.FareClass :"",
                                Price =(trainInfo.IsReturn && priceInfo.SingleOrReturn=="R")?evpricevalue: fareInfo!=null? FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(fareInfo.TotalFarePricesInfo.Amount), siteId, srcCurId, currencyID).ToString("F") :"N/A",  
                                netPrice =(trainInfo.IsReturn && priceInfo.SingleOrReturn=="R")?Convert.ToDecimal(evpricevalue): fareInfo!=null?  Convert.ToDecimal(FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(fareInfo.TotalFarePricesInfo.Amount), siteId, srcCurId, currencyID).ToString("F")):0       
                            }} : null;

                            long POrderID = Convert.ToInt64(Session["P2POrderID"]);
                            tblP2PSale objP2P = new tblP2PSale();
                            objP2P.From = trainInfo.DepartureStationName;
                            objP2P.DateTimeDepature = Convert.ToDateTime(trainInfo.DepartureDate);
                            objP2P.To = trainInfo.ArrivalStationName;
                            objP2P.DateTimeArrival = Convert.ToDateTime(trainInfo.ArrivalDate);
                            objP2P.TrainNo = trainInfo.TrainNumber;
                            objP2P.Passenger = priceInfo != null ? objBRUC.Adults + " Adults, " + objBRUC.Boys + " Child," + objBRUC.Youths + " Youth," + objBRUC.Seniors + " Senior" : "";
                            objP2P.Adult = objBRUC.Adults.ToString();
                            objP2P.Children = objBRUC.Boys.ToString();
                            objP2P.Youth = objBRUC.Youths.ToString();
                            objP2P.Senior = objBRUC.Seniors.ToString();
                            objP2P.SeviceName = fareInfo != null ? fareInfo.ClassCode : "";
                            objP2P.FareName = priceInfo != null ? priceInfo.FareClass : "";
                            objP2P.Price = (trainInfo.IsReturn && priceInfo.SingleOrReturn == "R") ? Convert.ToDecimal(evpricevalue) : fareInfo != null ? FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(fareInfo.TotalFarePricesInfo.Amount), siteId, srcCurId, currencyID) : 0;
                            objP2P.DepartureTime = trainInfo.DepartureTime;
                            objP2P.ArrivalTime = trainInfo.ArrivalTime;
                            objP2P.NetPrice = (trainInfo.IsReturn && priceInfo.SingleOrReturn == "R") ? Convert.ToDecimal(evpricevalue) : Convert.ToDecimal(objP2P.Price.ToString());
                            objP2P.CurrencyId = currencyID;
                            objP2P.ApiPrice = (trainInfo.IsReturn && priceInfo.SingleOrReturn == "R") ? Convert.ToDecimal(evpricevalue) : BusinessOneHub.IsNumeric(fareInfo.TotalFarePricesInfo.Amount) ? Convert.ToDecimal(fareInfo.TotalFarePricesInfo.Amount) : 0;
                            objP2P.ApiName = "Evolvi";
                            objP2P.FromStation = objBRUC.FromDetail;
                            objP2P.ToStation = objBRUC.ToDetail;

                            if (jFareClass == "First")
                                objP2P.Class = "1st Class";
                            else if (jFareClass == "Standard")
                                objP2P.Class = "2nd Class";
                            else
                                objP2P.Class = jFareClass;

                            string evolviTerms = string.Empty;
                            evolviTerms += "<p><strong>Refunds & Cancellations: </strong>" + jRefundAndCancellations.Replace("$", ",") + "</p>";
                            evolviTerms += "<p><strong>Changes to travel plans: </strong>" + jChangesToTravelPlans.Replace("$", ",") + "</p>";
                            objP2P.Terms = evolviTerms;

                            //site currency//
                            objP2P.Site_USD = _masterBooking.GetCurrencyMultiplier("SUSD", Guid.Empty, POrderID);
                            objP2P.Site_SEU = _masterBooking.GetCurrencyMultiplier("SSEU", Guid.Empty, POrderID);
                            objP2P.Site_SBD = _masterBooking.GetCurrencyMultiplier("SSBD", Guid.Empty, POrderID);
                            objP2P.Site_GBP = _masterBooking.GetCurrencyMultiplier("SGBP", Guid.Empty, POrderID);
                            objP2P.Site_EUR = _masterBooking.GetCurrencyMultiplier("SEUR", Guid.Empty, POrderID);
                            objP2P.Site_INR = _masterBooking.GetCurrencyMultiplier("SINR", Guid.Empty, POrderID);
                            objP2P.Site_SEK = _masterBooking.GetCurrencyMultiplier("SSEK", Guid.Empty, POrderID);
                            objP2P.Site_NZD = _masterBooking.GetCurrencyMultiplier("SNZD", Guid.Empty, POrderID);
                            objP2P.Site_CAD = _masterBooking.GetCurrencyMultiplier("SCAD", Guid.Empty, POrderID);
                            objP2P.Site_JPY = _masterBooking.GetCurrencyMultiplier("SJPY", Guid.Empty, POrderID);
                            objP2P.Site_AUD = _masterBooking.GetCurrencyMultiplier("SAUD", Guid.Empty, POrderID);
                            objP2P.Site_CHF = _masterBooking.GetCurrencyMultiplier("SCHF", Guid.Empty, POrderID);
                            objP2P.Site_EUB = _masterBooking.GetCurrencyMultiplier("SEUB", Guid.Empty, POrderID);
                            objP2P.Site_EUT = _masterBooking.GetCurrencyMultiplier("SEUT", Guid.Empty, POrderID);
                            objP2P.Site_GBB = _masterBooking.GetCurrencyMultiplier("SGBB", Guid.Empty, POrderID);
                            objP2P.Site_THB = _masterBooking.GetCurrencyMultiplier("STHB", Guid.Empty, POrderID);
                            objP2P.Site_SGD = _masterBooking.GetCurrencyMultiplier("SSGD", Guid.Empty, POrderID);

                            objP2P.EUR_EUT = _masterBooking.GetCurrencyMultiplier("E_EUT", Guid.Empty, POrderID);
                            objP2P.EUR_EUB = _masterBooking.GetCurrencyMultiplier("E_EUB", Guid.Empty, POrderID);
                            if (objP2P.Price == 0 && objBRUC.OneHubServiceName != "Evolvi")
                                return false;

                            Guid P2PID;
                            Guid LookupID = new ManageBooking().AddP2PBookingOrder(objP2P, Convert.ToInt64(Session["P2POrderID"]), out P2PID);
                            tblP2PSale objP2PRow = new ManageBooking().getP2PSingleRowData(P2PID);

                            tblEvolviSleeperPriceLookUp objtblEvolviSleeperPriceLookUp = new tblEvolviSleeperPriceLookUp();
                            objtblEvolviSleeperPriceLookUp.Price = objP2P.Price.Value;
                            objtblEvolviSleeperPriceLookUp.OrderId = Convert.ToInt64(Session["P2POrderID"]);
                            objtblEvolviSleeperPriceLookUp.PassSaleId = P2PID;
                            objtblEvolviSleeperPriceLookUp.SiteID = siteId;
                            new ManageBooking().AddEvolviSleeper(objtblEvolviSleeperPriceLookUp);

                            P2PReservationIDInfo objP2PIdInfo = new P2PReservationIDInfo();
                            objP2PIdInfo.ID = P2PID;
                            objP2PIdInfo.P2PID = objP2PRow.P2PId;
                            objP2PIdInfo.JourneyType = TypeOfJ;
                            lstP2PIdInfo.Add(objP2PIdInfo);

                            if (Session["EvolviReservationRef"] == null)
                                EvReservationRefList.Add(new EvolviReservationRef { P2PId = P2PID, TrainNo = trainInfo.TrainNumber });
                            else
                            {
                                EvReservationRefList = Session["EvolviReservationRef"] as List<EvolviReservationRef>;
                                EvReservationRefList.Add(new EvolviReservationRef { P2PId = P2PID, TrainNo = trainInfo.TrainNumber });
                            }
                            Session["EvolviReservationRef"] = EvReservationRefList;

                            if (Session["SHOPPINGCART"] != null)
                            {
                                List<ShoppingCartDetails> oldList = Session["SHOPPINGCART"] as List<ShoppingCartDetails>;
                                if (oldList != null) if (listCart != null) listCart.AddRange(oldList);
                            }
                            Session["SHOPPINGCART"] = listCart;
                            #endregion
                        }
                    }
                }
            }
            Session["ReservationInfoList"] = ReservationInfoList;
            return true;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
            return false;
        }
    }

    protected void rptoutEvolvi_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            TrainInformationResponse pInfoSolutionsResponse = null;
            if (outboundList != null)
                pInfoSolutionsResponse = new TrainInformationResponse
                {
                    TrainInformationList = outboundList.ToArray()
                };

            if (outboundList != null && _index == outboundList.Count())
                return;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (pInfoSolutionsResponse != null)
                {
                    var listTrainInfo = pInfoSolutionsResponse.TrainInformationList[_index];
                    DropDownList ddlPrice = e.Item.FindControl("ddlFromPrice") as DropDownList;
                    HyperLink hlnkContinue = e.Item.FindControl("hlnkContinue") as HyperLink;
                    Label lblCheapPrice = e.Item.FindControl("lblCheapPrice") as Label;
                    HtmlGenericControl divTrn = e.Item.FindControl("DivTr") as HtmlGenericControl;
                    Literal ltrFrom = e.Item.FindControl("ltrFrom") as Literal;
                    Literal ltrTo = e.Item.FindControl("ltrTo") as Literal;
                    ltrFrom.Text = listTrainInfo.TrainInfoSegment.FirstOrDefault().DepartureStationName;
                    ltrTo.Text = listTrainInfo.TrainInfoSegment.LastOrDefault().ArrivalStationName;

                    var trainPriceList = pInfoSolutionsResponse.TrainInformationList[_index].PriceInfo[0].TrainPriceList;
                    List<TrainPrice> pricelist = new List<TrainPrice>();
                    List<FaresBreakdownInfo> FaresBreakdownList = new List<FaresBreakdownInfo>();

                    foreach (var item in pInfoSolutionsResponse.TrainInformationList[_index].PriceInfo.ToList())
                    {
                        if (item.TrainPriceList != null)
                            pricelist.AddRange(item.TrainPriceList);
                        if (item.FaresBreakdownList != null)
                            FaresBreakdownList.AddRange(item.FaresBreakdownList);
                    }
                    pricelist.Where(x => x != null).ToList();
                    FaresBreakdownList.Where(x => x != null).ToList();

                    if (pricelist.Count > 0)
                    {
                        var cheapPrice = pricelist.Min(x => x.FarePricesInfo.Min(y => y.Amount));
                        var srcCurId = FrontEndManagePass.GetCurrencyID("GBP");
                        var cheapestPrice = FrontEndManagePass.GetPriceAfterConversion(cheapPrice, siteId, srcCurId, currencyID).ToString("F2");
                        lblCheapPrice.Text = currency.ToString() + cheapestPrice.ToString();
                    }

                    objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];

                    var returnFaresList = pricelist != null && pricelist.Count > 0 ? objBRUC.IsReturnJurney == false ? pricelist.Where(x => x.SingleOrReturn == "S").ToList() : pricelist.Where(x => x.SingleOrReturn == "R").ToList() : null;
                    var singleFaresList = pricelist != null && pricelist.Count > 0 ? objBRUC.IsReturnJurney == true ? pricelist.Where(x => x.SingleOrReturn == "S").ToList() : null : null;

                    #region Return fares
                    string classDiv = "", priceDiv = "", classDiv2 = "", priceDiv2 = "";
                    if (returnFaresList != null && returnFaresList.Count > 0)
                    {
                        #region First Class
                        var lstClass = returnFaresList.Where(x => x.FareClass == "First").ToList();
                        if (lstClass != null && (lstClass.Count() > 0))
                        {
                            classDiv = "<div class='starail-JourneyBlock-class'><p>1st Class</p>";
                            foreach (var item in synchroniseFareClass(lstClass))
                            {
                                var srcCurId = FrontEndManagePass.GetCurrencyID("GBP");
                                foreach (var itemPrice in item.FarePricesInfo.ToList())
                                {
                                    if (itemPrice != null)
                                    {
                                        Guid id = Guid.NewGuid();
                                        string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(itemPrice.TotalFarePricesInfo.Amount), siteId, srcCurId, currencyID).ToString("F2");
                                        string Text = itemPrice.ClassCode + " " + currency.ToString() + price;
                                        string Value = item.FareId + "," + item.TrainNumber + "," + item.FareClass + "," + item.SingleOrReturn + "," + item.RestrictionCode + "," + itemPrice.ClassCode + "," + price + "," + id + "," + item.TicketTypeCode + "," + item.ValidityCode + "," + item.FareSetterCode + "," + GetRefundAndCancellations(item.TicketTypeCode, item.RestrictionDescription) + "," + GetChangesToTravelPlans(item.TicketTypeCode, item.RestrictionDescription);
                                        string GroupDiscoutCode = FaresBreakdownList.Count > 0 ? FaresBreakdownList.FirstOrDefault(x => x.FareId == item.FareId).RailcardCode : "";

                                        priceDiv += "<div class='starail-JourneyBlock-radioRow return'><label> <span class='starail-Form-fancyRadioGroup outbound return' fareid='" + item.FareId + "'>";
                                        priceDiv += "<input type='radio' id='rdoFromEVOLVI' name='starail-ticket-outbound' value='" + Value + "'> ";
                                        priceDiv += Text;
                                        priceDiv += "<span><i></i></span></span> &nbsp;</label><a href='#' class='js-lightboxOpen' data-lightbox-id='starail-ticket-info" + id + "'><i class='starail-Icon-question'></i></a> <input type='hidden' id='hdnGroupDiscountCode' value='" + GroupDiscoutCode + "'/></div> ";

                                        #region Get Fare Information
                                        pricePoup += "<div class='starail-Lightbox'id='starail-ticket-info" + id + "'><div class='starail-Lightbox-content'><div class='starail-Lightbox-closeContainer'><a href='#' class='starail-Lightbox-close js-lightboxClose'><i class='starail-Icon starail-Icon-close'></i></a>";
                                        pricePoup += "</div><div class='starail-Lightbox-content-inner'> <div class='starail-Lightbox-text'> <h3 class='starail-Lightbox-textTitle'>Ticket Info</h3>";
                                        pricePoup += "<span class='tresult'>" + (!string.IsNullOrEmpty(item.ValidityCode) ? "<div class='farecode-parent'><span class='click-able' onclick=\"getValidityCode(\'" + item.ValidityCode + "\');\" data-validity-code=" + item.ValidityCode + ">Fare Code :" + item.ValidityCode + "</span></div>" : "") + "<p><strong>Fare Setter Code: </strong>" + item.FareSetterCode + "</p>" + GetFareInfoDetails(item.TicketTypeCode, item.RestrictionDescription) + "</span>";
                                        pricePoup += "</div></div></div></div>";
                                        #endregion

                                        hlnkContinue.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + Value + ",outbound" + "');");
                                        anchor_Next.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + Value + ",outbound" + "');");
                                        ddlPrice.Items.Add(new ListItem(Text, Value));
                                    }
                                }
                            }
                        }
                        else if (pricelist.Count() == 0)
                            priceDiv = "<div class='starail-JourneyBlock-radioRow return'><label><span class='starail-Form-fancyRadioGroup'>Sold out<span><i></i></span></span></label></div> ";
                        if (lstClass != null && lstClass.Count > 0)
                        {
                            classDiv += priceDiv;
                            classDiv += "</div>";
                        }
                        #endregion
                        #region Second Class
                        var secondClass = returnFaresList.Where(x => x.FareClass == "Standard").ToList();
                        if (secondClass != null && (secondClass.Count() > 0))
                        {
                            classDiv2 = "<div class='starail-JourneyBlock-class'><p>2nd Class</p>";
                            foreach (var item in synchroniseFareClass(secondClass))
                            {
                                var srcCurId = FrontEndManagePass.GetCurrencyID("GBP");
                                foreach (var itemPrice in item.FarePricesInfo.ToList())
                                {
                                    if (itemPrice != null)
                                    {
                                        Guid id = Guid.NewGuid();
                                        string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(itemPrice.TotalFarePricesInfo.Amount), siteId, srcCurId, currencyID).ToString("F2");
                                        string Text = itemPrice.ClassCode + " " + currency.ToString() + price;
                                        string Value = item.FareId + "," + item.TrainNumber + "," + item.FareClass + "," + item.SingleOrReturn + "," + item.RestrictionCode + "," + itemPrice.ClassCode + "," + price + "," + id + "," + item.TicketTypeCode + "," + item.ValidityCode + "," + item.FareSetterCode + "," + GetRefundAndCancellations(item.TicketTypeCode, item.RestrictionDescription) + "," + GetChangesToTravelPlans(item.TicketTypeCode, item.RestrictionDescription);
                                        string GroupDiscoutCode = FaresBreakdownList.Count > 0 ? FaresBreakdownList.FirstOrDefault(x => x.FareId == item.FareId).RailcardCode : "";

                                        priceDiv2 += "<div class='starail-JourneyBlock-radioRow return'><label> <span class='starail-Form-fancyRadioGroup outbound return' fareid='" + item.FareId + "'>";
                                        priceDiv2 += "<input type='radio' id='rdoFromEVOLVI' name='starail-ticket-outbound' value='" + Value + "'> ";
                                        priceDiv2 += Text;
                                        priceDiv2 += "<span><i></i></span></span> &nbsp;</label><a href='#' class='js-lightboxOpen' data-lightbox-id='starail-ticket-info" + id + "'><i class='starail-Icon-question'></i></a><input type='hidden' id='hdnGroupDiscountCode' value='" + GroupDiscoutCode + "'/> </div> ";

                                        #region Get Fare Information
                                        pricePoup += "<div class='starail-Lightbox'id='starail-ticket-info" + id + "'><div class='starail-Lightbox-content'><div class='starail-Lightbox-closeContainer'><a href='#' class='starail-Lightbox-close js-lightboxClose'><i class='starail-Icon starail-Icon-close'></i></a>";
                                        pricePoup += "</div><div class='starail-Lightbox-content-inner'> <div class='starail-Lightbox-text'> <h3 class='starail-Lightbox-textTitle'>Ticket Info</h3>";
                                        pricePoup += "<span class='tresult'>" + (!string.IsNullOrEmpty(item.ValidityCode) ? "<div class='farecode-parent'><span class='click-able' onclick=\"getValidityCode(\'" + item.ValidityCode + "\');\" data-validity-code=" + item.ValidityCode + ">Fare Code :" + item.ValidityCode + "</span></div>" : "") + "<p><strong>Fare Setter Code: </strong>" + item.FareSetterCode + "</p>" + GetFareInfoDetails(item.TicketTypeCode, item.RestrictionDescription) + "</span>";
                                        pricePoup += "</div></div></div></div>";
                                        #endregion

                                        hlnkContinue.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + Value + ",outbound" + "');");
                                        anchor_Next.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + Value + ",outbound" + "');");
                                        ddlPrice.Items.Add(new ListItem(Text, Value));
                                    }
                                }
                            }
                        }
                        else if (pricelist.Count() == 0)
                            priceDiv2 = "<div class='starail-JourneyBlock-radioRow return'><label><span class='starail-Form-fancyRadioGroup'>Sold out<span><i></i></span></span></label></div> ";
                        if (secondClass != null && secondClass.Count > 0)
                        {
                            classDiv2 += priceDiv2;
                            classDiv2 += "</div>";
                        }
                        #endregion
                    }
                    #endregion

                    #region Single Fares
                    string classDiv3 = "", priceDiv3 = "", classDiv4 = "", priceDiv4 = "";
                    if (singleFaresList != null && singleFaresList.Count > 0)
                    {
                        #region First Class
                        var lstClass2 = singleFaresList.Where(x => x.FareClass == "First").ToList();
                        if (lstClass2 != null && (lstClass2.Count() > 0))
                        {
                            classDiv3 = "<div class='starail-JourneyBlock-class'><p>1st Class</p>";
                            foreach (var item in synchroniseFareClass(lstClass2))
                            {
                                var srcCurId = FrontEndManagePass.GetCurrencyID("GBP");
                                foreach (var itemPrice in item.FarePricesInfo.ToList())
                                {
                                    if (itemPrice != null)
                                    {
                                        Guid id = Guid.NewGuid();
                                        string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(itemPrice.TotalFarePricesInfo.Amount), siteId, srcCurId, currencyID).ToString("F2");
                                        string Text = itemPrice.ClassCode + " " + currency.ToString() + price;
                                        string Value = item.FareId + "," + item.TrainNumber + "," + item.FareClass + "," + item.SingleOrReturn + "," + item.RestrictionCode + "," + itemPrice.ClassCode + "," + price + "," + id + "," + item.TicketTypeCode + "," + item.ValidityCode + "," + item.FareSetterCode + "," + GetRefundAndCancellations(item.TicketTypeCode, item.RestrictionDescription) + "," + GetChangesToTravelPlans(item.TicketTypeCode, item.RestrictionDescription);
                                        string GroupDiscoutCode = FaresBreakdownList.Count > 0 ? FaresBreakdownList.FirstOrDefault(x => x.FareId == item.FareId).RailcardCode : "";

                                        priceDiv3 += "<div class='starail-JourneyBlock-radioRow single'><label> <span class='starail-Form-fancyRadioGroup outbound single' fareid='" + item.FareId + "'>";
                                        priceDiv3 += "<input type='radio' id='rdoFromEVOLVI' name='starail-ticket-outbound' value='" + Value + "'> ";
                                        priceDiv3 += Text;
                                        priceDiv3 += "<span><i></i></span></span> &nbsp;</label><a href='#' class='js-lightboxOpen' data-lightbox-id='starail-ticket-info" + id + "'><i class='starail-Icon-question'></i></a><input type='hidden' id='hdnGroupDiscountCode' value='" + GroupDiscoutCode + "'/> </div> ";

                                        #region Get Fare Information
                                        pricePoup += "<div class='starail-Lightbox'id='starail-ticket-info" + id + "'><div class='starail-Lightbox-content'><div class='starail-Lightbox-closeContainer'><a href='#' class='starail-Lightbox-close js-lightboxClose'><i class='starail-Icon starail-Icon-close'></i></a>";
                                        pricePoup += "</div><div class='starail-Lightbox-content-inner'> <div class='starail-Lightbox-text'> <h3 class='starail-Lightbox-textTitle'>Ticket Info</h3>";
                                        pricePoup += "<span class='tresult'>" + (!string.IsNullOrEmpty(item.ValidityCode) ? "<div class='farecode-parent'><span class='click-able' onclick=\"getValidityCode(\'" + item.ValidityCode + "\');\" data-validity-code=" + item.ValidityCode + ">Fare Code :" + item.ValidityCode + "</span></div>" : "") + "<p><strong>Fare Setter Code: </strong>" + item.FareSetterCode + "</p>" + GetFareInfoDetails(item.TicketTypeCode, item.RestrictionDescription) + "</span>";
                                        pricePoup += "</div></div></div></div>";
                                        #endregion

                                        hlnkContinue.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + Value + ",outbound" + "');");
                                        anchor_Next.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + Value + ",outbound" + "');");
                                        ddlPrice.Items.Add(new ListItem(Text, Value));
                                    }
                                }
                            }
                        }
                        else if (pricelist.Count() == 0)
                            priceDiv3 = "<div class='starail-JourneyBlock-radioRow single'><label><span class='starail-Form-fancyRadioGroup'>Sold out<span><i></i></span></span></label></div> ";
                        if (lstClass2 != null && lstClass2.Count > 0)
                        {
                            classDiv3 += priceDiv3;
                            classDiv3 += "</div>";
                        }
                        #endregion
                        #region Second Class
                        var secondClass2 = singleFaresList.Where(x => x.FareClass == "Standard").ToList();
                        if (secondClass2 != null && (secondClass2.Count() > 0))
                        {
                            classDiv4 = "<div class='starail-JourneyBlock-class'><p>2nd Class</p>";
                            foreach (var item in synchroniseFareClass(secondClass2))
                            {
                                var srcCurId = FrontEndManagePass.GetCurrencyID("GBP");
                                foreach (var itemPrice in item.FarePricesInfo.ToList())
                                {
                                    if (itemPrice != null)
                                    {
                                        Guid id = Guid.NewGuid();
                                        string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(itemPrice.TotalFarePricesInfo.Amount), siteId, srcCurId, currencyID).ToString("F2");
                                        string Text = itemPrice.ClassCode + " " + currency.ToString() + price;
                                        string Value = item.FareId + "," + item.TrainNumber + "," + item.FareClass + "," + item.SingleOrReturn + "," + item.RestrictionCode + "," + itemPrice.ClassCode + "," + price + "," + id + "," + item.TicketTypeCode + "," + item.ValidityCode + "," + item.FareSetterCode + "," + GetRefundAndCancellations(item.TicketTypeCode, item.RestrictionDescription) + "," + GetChangesToTravelPlans(item.TicketTypeCode, item.RestrictionDescription);
                                        string GroupDiscoutCode = FaresBreakdownList.Count > 0 ? FaresBreakdownList.FirstOrDefault(x => x.FareId == item.FareId).RailcardCode : "";

                                        priceDiv4 += "<div class='starail-JourneyBlock-radioRow single'><label> <span class='starail-Form-fancyRadioGroup outbound single' fareid='" + item.FareId + "'>";
                                        priceDiv4 += "<input type='radio' id='rdoFromEVOLVI' name='starail-ticket-outbound' value='" + Value + "'> ";
                                        priceDiv4 += Text;
                                        priceDiv4 += "<span><i></i></span></span> &nbsp;</label><a href='#' class='js-lightboxOpen' data-lightbox-id='starail-ticket-info" + id + "'><i class='starail-Icon-question'></i></a> <input type='hidden' id='hdnGroupDiscountCode' value='" + GroupDiscoutCode + "'/></div> ";

                                        #region Get Fare Information
                                        pricePoup += "<div class='starail-Lightbox'id='starail-ticket-info" + id + "'><div class='starail-Lightbox-content'><div class='starail-Lightbox-closeContainer'><a href='#' class='starail-Lightbox-close js-lightboxClose'><i class='starail-Icon starail-Icon-close'></i></a>";
                                        pricePoup += "</div><div class='starail-Lightbox-content-inner'> <div class='starail-Lightbox-text'> <h3 class='starail-Lightbox-textTitle'>Ticket Info</h3>";
                                        pricePoup += "<span class='tresult'>" + (!string.IsNullOrEmpty(item.ValidityCode) ? "<div class='farecode-parent'><span class='click-able' onclick=\"getValidityCode(\'" + item.ValidityCode + "\');\" data-validity-code=" + item.ValidityCode + ">Fare Code :" + item.ValidityCode + "</span></div>" : "") + "<p><strong>Fare Setter Code: </strong>" + item.FareSetterCode + "</p>" + GetFareInfoDetails(item.TicketTypeCode, item.RestrictionDescription) + "</span>";
                                        pricePoup += "</div></div></div></div>";
                                        #endregion

                                        hlnkContinue.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + Value + ",outbound" + "');");
                                        anchor_Next.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + Value + ",outbound" + "');");
                                        ddlPrice.Items.Add(new ListItem(Text, Value));
                                    }
                                }
                            }
                        }
                        else if (pricelist.Count() == 0)
                            priceDiv4 = "<div class='starail-JourneyBlock-radioRow single'><label><span class='starail-Form-fancyRadioGroup'>Sold out<span><i></i></span></span></label></div> ";
                        if (secondClass2 != null && secondClass2.Count > 0)
                        {
                            classDiv4 += priceDiv4;
                            classDiv4 += "</div>";
                        }
                        #endregion
                    }
                    #endregion

                    if (objBRUC.IsReturnJurney == false)
                        divTrn.InnerHtml = returnFaresList != null && returnFaresList.Count > 0 ? ("<div class='starail-JourneyBlock-class full-fare-width'><p>Single Fares</p></div>" + classDiv2 + classDiv) : string.Empty;
                    else
                        divTrn.InnerHtml = (returnFaresList != null && returnFaresList.Count > 0 ? ("<div class='starail-JourneyBlock-class full-fare-width'><p>Return Fares</p></div>" + classDiv2 + classDiv) : string.Empty) + (singleFaresList != null && singleFaresList.Count > 0 ? ("<div class='starail-JourneyBlock-class full-fare-width'><p>Dual Single Fares</p></div>" + classDiv4 + classDiv3) : string.Empty);

                    #region Journey Details Request
                    List<TrainFareSegment> journeyResponse = new List<TrainFareSegment>();
                    if (Session["HoldJourneyDetailsList"] == null)
                        journeyResponse = JourneyDetailsRequest(listTrainInfo).TrainFareSegmentList.ToList();
                    else
                    {
                        var journeyHoldList = Session["HoldJourneyDetailsList"] as List<TrainFareSegment>;
                        if (journeyHoldList != null && journeyHoldList.Count > 0 && journeyHoldList.Any(x => x.JourneyIdentifier == listTrainInfo.TrainNumber))
                            journeyResponse = journeyHoldList.Where(x => x.JourneyIdentifier == listTrainInfo.TrainNumber).ToList();
                        else
                            journeyResponse = JourneyDetailsRequest(listTrainInfo).TrainFareSegmentList.ToList();
                    }
                    if (journeyResponse != null && journeyResponse.Count > 0)
                    {
                        Repeater GrdRouteInfo = e.Item.FindControl("GrdRouteInfo") as Repeater;
                        GrdRouteInfo.DataSource = journeyResponse.ToList();
                        GrdRouteInfo.DataBind();
                    }
                    #endregion

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ddloutpricevalue", "getOutBoundmobilevalue();", true);
                }
                _index++;
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void rptinEvolvi_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            TrainInformationResponse pInfoSolutionsResponse = null;
            if (inboundList != null && inboundList.Count > 0)
                pInfoSolutionsResponse = new TrainInformationResponse
                {
                    TrainInformationList = inboundList.ToArray()
                };

            if (inboundList != null && newindex == inboundList.Count())
                return;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (pInfoSolutionsResponse != null)
                {
                    var listTrainInfo = pInfoSolutionsResponse.TrainInformationList[newindex];
                    DropDownList ddlPrice = e.Item.FindControl("ddlFromPrice") as DropDownList;
                    HyperLink hlnkContinue = e.Item.FindControl("hlnkContinue") as HyperLink;
                    Label lblCheapPrice = e.Item.FindControl("lblCheapPrice") as Label;
                    HtmlGenericControl divTrn = e.Item.FindControl("DivTr") as HtmlGenericControl;
                    Literal ltrFrom = e.Item.FindControl("ltrFrom") as Literal;
                    Literal ltrTo = e.Item.FindControl("ltrTo") as Literal;
                    ltrFrom.Text = listTrainInfo.TrainInfoSegment.FirstOrDefault().DepartureStationName;
                    ltrTo.Text = listTrainInfo.TrainInfoSegment.LastOrDefault().ArrivalStationName;
                    var trainPriceList = pInfoSolutionsResponse.TrainInformationList[newindex].PriceInfo[0].TrainPriceList;
                    List<TrainPrice> pricelist = new List<TrainPrice>();
                    List<FaresBreakdownInfo> FaresBreakdownList = new List<FaresBreakdownInfo>();

                    foreach (var item in pInfoSolutionsResponse.TrainInformationList[newindex].PriceInfo.ToList())
                    {
                        if (item.TrainPriceList != null)
                            pricelist.AddRange(item.TrainPriceList);
                        if (item.FaresBreakdownList != null)
                            FaresBreakdownList.AddRange(item.FaresBreakdownList);
                    }
                    pricelist.Where(x => x != null).ToList();
                    FaresBreakdownList.Where(x => x != null).ToList();

                    if (pricelist.Count > 0)
                    {
                        var cheapPrice = pricelist.Min(x => x.FarePricesInfo.Min(y => y.Amount));
                        var srcCurId = FrontEndManagePass.GetCurrencyID("GBP");
                        var cheapestPrice = FrontEndManagePass.GetPriceAfterConversion(cheapPrice, siteId, srcCurId, currencyID).ToString("F2");
                        lblCheapPrice.Text = currency.ToString() + cheapestPrice.ToString();
                    }

                    objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
                    var returnFaresList = pricelist != null && pricelist.Count > 0 ? pricelist.Where(x => x.SingleOrReturn == "R").ToList() : null;
                    var singleFaresList = pricelist != null && pricelist.Count > 0 ? pricelist.Where(x => x.SingleOrReturn == "S").ToList() : null;

                    #region Return fares
                    string classDiv = "", priceDiv = "", classDiv2 = "", priceDiv2 = "";
                    if (returnFaresList != null && returnFaresList.Count > 0)
                    {
                        #region First Class
                        var lstClass = returnFaresList.Where(x => x.FareClass == "First").ToList();
                        if (lstClass != null && (lstClass.Count() > 0))
                        {
                            classDiv = "<div class='starail-JourneyBlock-class'><p>1st Class</p>";
                            foreach (var item in synchroniseFareClass(lstClass))
                            {
                                var srcCurId = FrontEndManagePass.GetCurrencyID("GBP");
                                foreach (var itemPrice in item.FarePricesInfo.ToList())
                                {
                                    if (itemPrice != null)
                                    {
                                        Guid id = Guid.NewGuid();
                                        string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(itemPrice.TotalFarePricesInfo.Amount), siteId, srcCurId, currencyID).ToString("F2");
                                        string Text = itemPrice.ClassCode + " " + currency.ToString() + price;
                                        string Value = item.FareId + "," + item.TrainNumber + "," + item.FareClass + "," + item.SingleOrReturn + "," + item.RestrictionCode + "," + itemPrice.ClassCode + "," + price + "," + id + "," + item.TicketTypeCode + "," + item.ValidityCode + "," + item.FareSetterCode + "," + GetRefundAndCancellations(item.TicketTypeCode, item.RestrictionDescription) + "," + GetChangesToTravelPlans(item.TicketTypeCode, item.RestrictionDescription);
                                        string GroupDiscoutCode = FaresBreakdownList.Count > 0 ? FaresBreakdownList.FirstOrDefault(x => x.FareId == item.FareId).RailcardCode : "";

                                        priceDiv += "<div class='starail-JourneyBlock-radioRow return'><label> <span class='starail-Form-fancyRadioGroup inbound return' fareid='" + item.FareId + "'>";
                                        priceDiv += "<input type='radio' id='rdoToEVOLVI' name='starail-ticket-inbound' value='" + Value + "'> ";
                                        priceDiv += Text;
                                        priceDiv += "<span><i></i></span></span> &nbsp;</label><a href='#' class='js-lightboxOpen' data-lightbox-id='starail-ticket-info" + id + "'><i class='starail-Icon-question'></i></a><input type='hidden' id='hdnGroupDiscountCode' value='" + GroupDiscoutCode + "'/> </div> ";

                                        #region Get Fare Information
                                        pricePoup += "<div class='starail-Lightbox'id='starail-ticket-info" + id + "'><div class='starail-Lightbox-content'><div class='starail-Lightbox-closeContainer'><a href='#' class='starail-Lightbox-close js-lightboxClose'><i class='starail-Icon starail-Icon-close'></i></a>";
                                        pricePoup += "</div><div class='starail-Lightbox-content-inner'> <div class='starail-Lightbox-text'> <h3 class='starail-Lightbox-textTitle'>Ticket Info</h3>";
                                        pricePoup += "<span class='tresult'>" + (!string.IsNullOrEmpty(item.ValidityCode) ? "<div class='farecode-parent'><span class='click-able' onclick=\"getValidityCode(\'" + item.ValidityCode + "\');\" data-validity-code=" + item.ValidityCode + ">Fare Code :" + item.ValidityCode + "</span></div>" : "") + "<p><strong>Fare Setter Code: </strong>" + item.FareSetterCode + "</p>" + GetFareInfoDetails(item.TicketTypeCode, item.RestrictionDescription) + "</span>";
                                        pricePoup += "</div></div></div></div>";
                                        #endregion

                                        hlnkContinue.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + Value + ",inbound" + "');");
                                        anchor_Next.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + Value + ",inbound" + "');");
                                        ddlPrice.Items.Add(new ListItem(Text, Value));
                                    }
                                }
                            }
                        }
                        else if (pricelist.Count() == 0)
                            priceDiv = "<div class='starail-JourneyBlock-radioRow return'><label><span class='starail-Form-fancyRadioGroup'>Sold out<span><i></i></span></span></label></div> ";
                        if (lstClass != null && lstClass.Count > 0)
                        {
                            classDiv += priceDiv;
                            classDiv += "</div>";
                        }
                        #endregion
                        #region Second Class
                        var secondClass = returnFaresList.Where(x => x.FareClass == "Standard").ToList();
                        if (secondClass != null && (secondClass.Count() > 0))
                        {
                            classDiv2 = "<div class='starail-JourneyBlock-class'><p>2nd Class</p>";
                            foreach (var item in synchroniseFareClass(secondClass))
                            {
                                var srcCurId = FrontEndManagePass.GetCurrencyID("GBP");
                                foreach (var itemPrice in item.FarePricesInfo.ToList())
                                {
                                    if (itemPrice != null)
                                    {
                                        Guid id = Guid.NewGuid();
                                        string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(itemPrice.TotalFarePricesInfo.Amount), siteId, srcCurId, currencyID).ToString("F2");
                                        string Text = itemPrice.ClassCode + " " + currency.ToString() + price;
                                        string Value = item.FareId + "," + item.TrainNumber + "," + item.FareClass + "," + item.SingleOrReturn + "," + item.RestrictionCode + "," + itemPrice.ClassCode + "," + price + "," + id + "," + item.TicketTypeCode + "," + item.ValidityCode + "," + item.FareSetterCode + "," + GetRefundAndCancellations(item.TicketTypeCode, item.RestrictionDescription) + "," + GetChangesToTravelPlans(item.TicketTypeCode, item.RestrictionDescription);
                                        string GroupDiscoutCode = FaresBreakdownList.Count > 0 ? FaresBreakdownList.FirstOrDefault(x => x.FareId == item.FareId).RailcardCode : "";

                                        priceDiv2 += "<div class='starail-JourneyBlock-radioRow return'><label> <span class='starail-Form-fancyRadioGroup inbound return' fareid='" + item.FareId + "'>";
                                        priceDiv2 += "<input type='radio' id='rdoToEVOLVI' name='starail-ticket-inbound' value='" + Value + "'> ";
                                        priceDiv2 += Text;
                                        priceDiv2 += "<span><i></i></span></span> &nbsp;</label><a href='#' class='js-lightboxOpen' data-lightbox-id='starail-ticket-info" + id + "'><i class='starail-Icon-question'></i></a> <input type='hidden' id='hdnGroupDiscountCode' value='" + GroupDiscoutCode + "'/></div> ";

                                        #region Get Fare Information
                                        pricePoup += "<div class='starail-Lightbox'id='starail-ticket-info" + id + "'><div class='starail-Lightbox-content'><div class='starail-Lightbox-closeContainer'><a href='#' class='starail-Lightbox-close js-lightboxClose'><i class='starail-Icon starail-Icon-close'></i></a>";
                                        pricePoup += "</div><div class='starail-Lightbox-content-inner'> <div class='starail-Lightbox-text'> <h3 class='starail-Lightbox-textTitle'>Ticket Info</h3>";
                                        pricePoup += "<span class='tresult'>" + (!string.IsNullOrEmpty(item.ValidityCode) ? "<div class='farecode-parent'><span class='click-able' onclick=\"getValidityCode(\'" + item.ValidityCode + "\');\" data-validity-code=" + item.ValidityCode + ">Fare Code :" + item.ValidityCode + "</span></div>" : "") + "<p><strong>Fare Setter Code: </strong>" + item.FareSetterCode + "</p>" + GetFareInfoDetails(item.TicketTypeCode, item.RestrictionDescription) + "</span>";
                                        pricePoup += "</div></div></div></div>";
                                        #endregion

                                        hlnkContinue.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + Value + ",inbound" + "');");
                                        anchor_Next.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + Value + ",inbound" + "');");
                                        ddlPrice.Items.Add(new ListItem(Text, Value));
                                    }
                                }
                            }
                        }
                        else if (pricelist.Count() == 0)
                            priceDiv2 = "<div class='starail-JourneyBlock-radioRow return'><label><span class='starail-Form-fancyRadioGroup'>Sold out<span><i></i></span></span></label></div> ";
                        if (secondClass != null && secondClass.Count > 0)
                        {
                            classDiv2 += priceDiv2;
                            classDiv2 += "</div>";
                        }
                        #endregion
                    }
                    #endregion

                    #region Single Fares
                    string classDiv3 = "", priceDiv3 = "", classDiv4 = "", priceDiv4 = "";
                    if (singleFaresList != null && singleFaresList.Count > 0)
                    {
                        #region First Class
                        var lstClass2 = singleFaresList.Where(x => x.FareClass == "First").ToList();
                        if (lstClass2 != null && (lstClass2.Count() > 0))
                        {
                            classDiv3 = "<div class='starail-JourneyBlock-class'><p>1st Class</p>";
                            foreach (var item in synchroniseFareClass(lstClass2))
                            {
                                var srcCurId = FrontEndManagePass.GetCurrencyID("GBP");
                                foreach (var itemPrice in item.FarePricesInfo.ToList())
                                {
                                    if (itemPrice != null)
                                    {
                                        Guid id = Guid.NewGuid();
                                        string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(itemPrice.TotalFarePricesInfo.Amount), siteId, srcCurId, currencyID).ToString("F2");
                                        string Text = itemPrice.ClassCode + " " + currency.ToString() + price;
                                        string Value = item.FareId + "," + item.TrainNumber + "," + item.FareClass + "," + item.SingleOrReturn + "," + item.RestrictionCode + "," + itemPrice.ClassCode + "," + price + "," + id + "," + item.TicketTypeCode + "," + item.ValidityCode + "," + item.FareSetterCode + "," + GetRefundAndCancellations(item.TicketTypeCode, item.RestrictionDescription) + "," + GetChangesToTravelPlans(item.TicketTypeCode, item.RestrictionDescription);
                                        string GroupDiscoutCode = FaresBreakdownList.Count > 0 ? FaresBreakdownList.FirstOrDefault(x => x.FareId == item.FareId).RailcardCode : "";

                                        priceDiv3 += "<div class='starail-JourneyBlock-radioRow single'><label> <span class='starail-Form-fancyRadioGroup inbound single' fareid='" + item.FareId + "'>";
                                        priceDiv3 += "<input type='radio' id='rdoToEVOLVI' name='starail-ticket-inbound' value='" + Value + "'> ";
                                        priceDiv3 += Text;
                                        priceDiv3 += "<span><i></i></span></span> &nbsp;</label><a href='#' class='js-lightboxOpen' data-lightbox-id='starail-ticket-info" + id + "'><i class='starail-Icon-question'></i></a><input type='hidden' id='hdnGroupDiscountCode' value='" + GroupDiscoutCode + "'/></div> ";

                                        #region Get Fare Information
                                        pricePoup += "<div class='starail-Lightbox'id='starail-ticket-info" + id + "'><div class='starail-Lightbox-content'><div class='starail-Lightbox-closeContainer'><a href='#' class='starail-Lightbox-close js-lightboxClose'><i class='starail-Icon starail-Icon-close'></i></a>";
                                        pricePoup += "</div><div class='starail-Lightbox-content-inner'> <div class='starail-Lightbox-text'> <h3 class='starail-Lightbox-textTitle'>Ticket Info</h3>";
                                        pricePoup += "<span class='tresult'>" + (!string.IsNullOrEmpty(item.ValidityCode) ? "<div class='farecode-parent'><span class='click-able' onclick=\"getValidityCode(\'" + item.ValidityCode + "\');\" data-validity-code=" + item.ValidityCode + ">Fare Code :" + item.ValidityCode + "</span></div>" : "") + "<p><strong>Fare Setter Code: </strong>" + item.FareSetterCode + "</p>" + GetFareInfoDetails(item.TicketTypeCode, item.RestrictionDescription) + "</span>";
                                        pricePoup += "</div></div></div></div>";
                                        #endregion

                                        hlnkContinue.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + Value + ",inbound" + "');");
                                        anchor_Next.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + Value + ",inbound" + "');");
                                        ddlPrice.Items.Add(new ListItem(Text, Value));
                                    }
                                }
                            }
                        }
                        else if (pricelist.Count() == 0)
                            priceDiv3 = "<div class='starail-JourneyBlock-radioRow single'><label><span class='starail-Form-fancyRadioGroup'>Sold out<span><i></i></span></span></label></div> ";
                        if (lstClass2 != null && lstClass2.Count > 0)
                        {
                            classDiv3 += priceDiv3;
                            classDiv3 += "</div>";
                        }
                        #endregion
                        #region Second Class
                        var secondClass2 = singleFaresList.Where(x => x.FareClass == "Standard").ToList();
                        if (secondClass2 != null && (secondClass2.Count() > 0))
                        {
                            classDiv4 = "<div class='starail-JourneyBlock-class'><p>2nd Class</p>";
                            foreach (var item in synchroniseFareClass(secondClass2))
                            {
                                var srcCurId = FrontEndManagePass.GetCurrencyID("GBP");
                                foreach (var itemPrice in item.FarePricesInfo.ToList())
                                {
                                    if (itemPrice != null)
                                    {
                                        Guid id = Guid.NewGuid();
                                        string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(itemPrice.TotalFarePricesInfo.Amount), siteId, srcCurId, currencyID).ToString("F2");
                                        string Text = itemPrice.ClassCode + " " + currency.ToString() + price;
                                        string Value = item.FareId + "," + item.TrainNumber + "," + item.FareClass + "," + item.SingleOrReturn + "," + item.RestrictionCode + "," + itemPrice.ClassCode + "," + price + "," + id + "," + item.TicketTypeCode + "," + item.ValidityCode + "," + item.FareSetterCode + "," + GetRefundAndCancellations(item.TicketTypeCode, item.RestrictionDescription) + "," + GetChangesToTravelPlans(item.TicketTypeCode, item.RestrictionDescription);
                                        string GroupDiscoutCode = FaresBreakdownList.Count > 0 ? FaresBreakdownList.FirstOrDefault(x => x.FareId == item.FareId).RailcardCode : "";

                                        priceDiv4 += "<div class='starail-JourneyBlock-radioRow single'><label> <span class='starail-Form-fancyRadioGroup inbound single' fareid='" + item.FareId + "'>";
                                        priceDiv4 += "<input type='radio' id='rdoToEVOLVI' name='starail-ticket-inbound' value='" + Value + "'> ";
                                        priceDiv4 += Text;
                                        priceDiv4 += "<span><i></i></span></span> &nbsp;</label><a href='#' class='js-lightboxOpen' data-lightbox-id='starail-ticket-info" + id + "'><i class='starail-Icon-question'></i></a><input type='hidden' id='hdnGroupDiscountCode' value='" + GroupDiscoutCode + "'/> </div> ";

                                        #region Get Fare Information
                                        pricePoup += "<div class='starail-Lightbox'id='starail-ticket-info" + id + "'><div class='starail-Lightbox-content'><div class='starail-Lightbox-closeContainer'><a href='#' class='starail-Lightbox-close js-lightboxClose'><i class='starail-Icon starail-Icon-close'></i></a>";
                                        pricePoup += "</div><div class='starail-Lightbox-content-inner'> <div class='starail-Lightbox-text'> <h3 class='starail-Lightbox-textTitle'>Ticket Info</h3>";
                                        pricePoup += "<span class='tresult'>" + (!string.IsNullOrEmpty(item.ValidityCode) ? "<div class='farecode-parent'><span class='click-able' onclick=\"getValidityCode(\'" + item.ValidityCode + "\');\" data-validity-code=" + item.ValidityCode + ">Fare Code :" + item.ValidityCode + "</span></div>" : "") + "<p><strong>Fare Setter Code: </strong>" + item.FareSetterCode + "</p>" + GetFareInfoDetails(item.TicketTypeCode, item.RestrictionDescription) + "</span>";
                                        pricePoup += "</div></div></div></div>";
                                        #endregion

                                        hlnkContinue.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + Value + ",inbound" + "');");
                                        anchor_Next.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + Value + ",inbound" + "');");
                                        ddlPrice.Items.Add(new ListItem(Text, Value));
                                    }
                                }
                            }
                        }
                        else if (pricelist.Count() == 0)
                            priceDiv4 = "<div class='starail-JourneyBlock-radioRow single'><label><span class='starail-Form-fancyRadioGroup'>Sold out<span><i></i></span></span></label></div> ";
                        if (secondClass2 != null && secondClass2.Count > 0)
                        {
                            classDiv4 += priceDiv4;
                            classDiv4 += "</div>";
                        }
                        #endregion
                    }
                    #endregion

                    divTrn.InnerHtml = (returnFaresList != null && returnFaresList.Count > 0 ? ("<div class='starail-JourneyBlock-class full-fare-width'><p>Return Fares</p></div>" + classDiv2 + classDiv) : string.Empty) + (singleFaresList != null && singleFaresList.Count > 0 ? ("<div class='starail-JourneyBlock-class full-fare-width'><p>Dual Single Fares</p></div>" + classDiv4 + classDiv3) : string.Empty);

                    #region Journey Details Request
                    List<TrainFareSegment> journeyResponse = new List<TrainFareSegment>();
                    if (Session["HoldJourneyDetailsList"] == null)
                        journeyResponse = JourneyDetailsRequest(listTrainInfo).TrainFareSegmentList.ToList();
                    else
                    {
                        var journeyHoldList = Session["HoldJourneyDetailsList"] as List<TrainFareSegment>;
                        if (journeyHoldList != null && journeyHoldList.Count > 0 && journeyHoldList.Any(x => x.JourneyIdentifier == listTrainInfo.TrainNumber))
                            journeyResponse = journeyHoldList.Where(x => x.JourneyIdentifier == listTrainInfo.TrainNumber).ToList();
                        else
                            journeyResponse = JourneyDetailsRequest(listTrainInfo).TrainFareSegmentList.ToList();
                    }
                    if (journeyResponse != null && journeyResponse.Count > 0)
                    {
                        Repeater GrdRouteInfo = e.Item.FindControl("GrdRouteInfo") as Repeater;
                        GrdRouteInfo.DataSource = journeyResponse.ToList();
                        GrdRouteInfo.DataBind();
                    }
                    #endregion
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ddlinpricevalue", "getInBoundmobilevalue();", true);
                }
                newindex++;
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    string GetRoundPrice(string price)
    {
        try
        {
            string[] strPrice = price.ToString().Split('.');

            if (strPrice[1] != null && Convert.ToDecimal(strPrice[1]) > 50)
                return (Convert.ToDecimal(strPrice[0]) + 1).ToString() + ".00";
            else if (strPrice[1] != null && Convert.ToDecimal(strPrice[1]) > 0 && Convert.ToDecimal(strPrice[1]) <= 50)
                return strPrice[0].Trim() + ".50";
            else
                return strPrice[0].Trim() + ".00";
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); return ""; }
    }

    public JourneyDetailsResponse JourneyDetailsRequest(TrainInformation objTrainInfo)
    {
        try
        {
            OneHubRailOneHubClient client = new OneHubRailOneHubClient();
            JourneyDetailsRequest request = new JourneyDetailsRequest();
            JourneyDetailsResponse response = new JourneyDetailsResponse();
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;

            if (objTrainInfo != null)
            {
                string TrainNo = objTrainInfo.TrainNumber.ToString();
                var FareIdList = objTrainInfo.PriceInfo != null ? objTrainInfo.PriceInfo.SelectMany(x => x.TrainPriceList != null ? x.TrainPriceList.Select(y => y.FareId) : null) : null;
                if (pInfoSolutionsResponse.EvolviPayloadAttributesInfo != null)
                {
                    request = new JourneyDetailsRequest
                    {
                        EchoToken = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.EchoToken,
                        Version = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.Version,
                        TransactionIdentifier = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.TransactionIdentifier,
                        TimeStamp = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.TimeStamp,
                        SequenceNmbr = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.SequenceNmbr,
                        TargetType = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.TargetType,
                        ISOCountry = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.ISOCountry,
                        ISOCurrency = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.ISOCurrency,

                        EvJourneyInfo = new List<EvJourneyInfo> 
                        {
                            new EvJourneyInfo
                            {
                                JourneyIdentifier=TrainNo,
                                JourneyFareInfo=objTrainInfo.PriceInfo != null ? objTrainInfo.PriceInfo.SelectMany(x => x.TrainPriceList != null ? x.TrainPriceList.Select(y =>new JourneyFareInfo
                                {
                                    FareId=y.FareId
                                }) : null).ToArray() : null
                            }
                        }.ToArray()
                    };
                    _ManageOneHub.ApiLogin(request, siteId);
                    response = client.EvolviJourneyDetails(request);
                    if (response.TrainFareSegmentList != null && response.TrainFareSegmentList.Length > 0)
                    {
                        var journeyHoldList = new List<TrainFareSegment>();
                        if (Session["HoldJourneyDetailsList"] != null)
                        {
                            journeyHoldList = Session["HoldJourneyDetailsList"] as List<TrainFareSegment>;
                            if (journeyHoldList != null && journeyHoldList.Count > 0)
                                journeyHoldList.AddRange(response.TrainFareSegmentList.ToList());
                        }
                        else
                            journeyHoldList.AddRange(response.TrainFareSegmentList.ToList());
                        Session["HoldJourneyDetailsList"] = journeyHoldList;
                    }
                    return response.TrainFareSegmentList != null ? response : null;
                }
                return null;
            }
            return null;
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); return null; }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    public void SearchTrainInfo()
    {
        try
        {
            if (Session["BookingUCRerq"] != null)
            {
                var objBruc = Session["BookingUCRerq"] as BookingRequestUserControl;
                var client = new OneHubRailOneHubClient();
                TrainInformationRequest request = _ManageOneHub.TrainInformation(objBruc, 1, objBruc.depdt.ToString("dd/MMM/yyyy"), objBruc.depTime.ToString("HH:mm"), !string.IsNullOrEmpty(objBruc.ReturnDate) ? (DateTime.Parse(objBruc.ReturnDate)).ToString("dd/MMM/yyyy") : string.Empty, objBruc.ReturnTime);

                if (objBruc.OneHubServiceName == "Evolvi")
                {
                    request.SequenceNo = System.Web.Configuration.WebConfigurationManager.AppSettings["SequenceNo"].ToString();
                    request.MaxResponse = System.Web.Configuration.WebConfigurationManager.AppSettings["MaxResponse"].ToString();
                    request.ISOCountry = new ManageBooking().GetCountryISOCode(siteId);
                    request.ISOCurrency = new ManageBooking().GetCurrencyISOCode(siteId);
                }

                if (objBruc.IsReturnJurney)
                    request.IsReturnJourney = true;
                else
                    request.IsReturnJourney = false;

                _ManageOneHub.ApiLogin(request, siteId);
                TrainInformationResponse pInfoSolutionsResponse = client.TrainInformation(request);
                Session["TrainSearch"] = pInfoSolutionsResponse;
            }
        }
        catch (Exception ex)
        {
            if (!ex.Message.Contains("Thread"))
            {
                Session["ErrorMessage"] = ex.Message;
                Response.Redirect("TrainResults.aspx");
            }
        }
    }

    protected void rptPassengerDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            try
            {
                DropDownList ddlRailCard = e.Item.FindControl("ddlRailCard") as DropDownList;
                ddlRailCard.DataSource = _master.GetRailCardList();
                ddlRailCard.DataTextField = "RailCardName";
                ddlRailCard.DataValueField = "RailCardCode";
                ddlRailCard.DataBind();
                ddlRailCard.Items.Insert(0, new ListItem("Select RailCard", "0"));

                //use for radio button group name
                RadioButton rdo = (RadioButton)e.Item.FindControl("rdnLeadPassenger");
                string script = "SetUniqueRadioButton('rptPassengerDetails.*lead',this)";
                rdo.Attributes.Add("onclick", script);
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }
    }

    private void GetTraveller()
    {
        try
        {
            if (Session["BookingUCRerq"] != null && Session["TrainSearch"] != null)
            {
                objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
                List<TravellersInfo> TravellerList = new List<TravellersInfo>();

                if (objBRUC.Adults > 0)
                    for (int i = 1; i <= objBRUC.Adults; i++)
                        TravellerList.Add(new TravellersInfo() { Type = objBRUC.Adults == 1 ? "Adult" : "Adult " + i });
                if (objBRUC.Seniors > 0)
                    for (int i = 1; i <= objBRUC.Seniors; i++)
                        TravellerList.Add(new TravellersInfo() { Type = objBRUC.Seniors == 1 ? "Senior" : "Senior " + i });
                if (objBRUC.Youths > 0)
                    for (int i = 1; i <= objBRUC.Youths; i++)
                        TravellerList.Add(new TravellersInfo() { Type = objBRUC.Youths == 1 ? "Youth" : "Youth " + i });
                if (objBRUC.Boys > 0)
                    for (int i = 1; i <= objBRUC.Boys; i++)
                        TravellerList.Add(new TravellersInfo() { Type = objBRUC.Boys == 1 ? "Child" : "Child " + i });

                rptPassengerDetails.DataSource = TravellerList;
                rptPassengerDetails.DataBind();
                if (TravellerList != null)
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "checkFirstRadioButton", "checkFirstRadioButton();", true);
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    private void SaveTravellers()
    {
        try
        {
            List<TravellersInfo> TravellerList = new List<TravellersInfo>();
            for (int i = 0; i < rptPassengerDetails.Items.Count; i++)
            {
                Label lblType = rptPassengerDetails.Items[i].FindControl("lblType") as Label;
                DropDownList ddlTitle = rptPassengerDetails.Items[i].FindControl("ddlTitle") as DropDownList;
                TextBox txtFirstname = rptPassengerDetails.Items[i].FindControl("txtFirstname") as TextBox;
                TextBox txtLastname = rptPassengerDetails.Items[i].FindControl("txtLastname") as TextBox;
                DropDownList ddlRailCard = rptPassengerDetails.Items[i].FindControl("ddlRailCard") as DropDownList;
                RadioButton rdnLeadPassenger = rptPassengerDetails.Items[i].FindControl("rdnLeadPassenger") as RadioButton;

                TravellerList.Add(new TravellersInfo
                {
                    Type = lblType.Text.Trim(),
                    Title = ddlTitle.SelectedItem.Text,
                    Country = string.Empty,
                    FName = txtFirstname.Text.Trim(),
                    LName = txtLastname.Text.Trim(),
                    RailCard = ddlRailCard.SelectedValue == "0" ? "" : ddlRailCard.SelectedValue,
                    Lead = rdnLeadPassenger.Checked ? true : false,
                    TitleValue = ddlTitle.SelectedValue
                });
            }
            Session["TravellerList"] = TravellerList;
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    private void SaveTravellerInDB(long OrderId)
    {
        try
        {
            var objTravellLists = Session["TravellerList"] as List<TravellersInfo>;
            if (objTravellLists != null && objTravellLists.Count > 0)
            {
                for (int i = 0; i < objTravellLists.Count; i++)
                {
                    tblOrderTraveller objTraveller = new tblOrderTraveller();
                    objTraveller.ID = Guid.NewGuid();
                    objTraveller.Title = objTravellLists[i].Title;
                    objTraveller.FirstName = objTravellLists[i].FName;
                    objTraveller.LastName = objTravellLists[i].LName;
                    objTraveller.LeadPassenger = objTravellLists[i].Lead;
                    Guid P2PTravellerId = new ManageBooking().AddP2POrderTraveller(objTraveller);

                    if (objTravellLists[i].Lead)
                        new ManageBooking().UpdateP2POrderTraveller(OrderId, P2PTravellerId);
                    if (objTravellLists.Count > 1)
                        new ManageBooking().AddP2POtherTravellerLookup(OrderId, P2PTravellerId);
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    private List<TrainPrice> synchroniseFareClass(List<TrainPrice> pricelistnew)
    {
        try
        {
            var FirstRowPrice = pricelistnew.GroupBy(q => q.FarePricesInfo.FirstOrDefault().ClassCode.Replace(" ", string.Empty).Replace("-", string.Empty)).Select(t =>
                new
                {
                    ClassCode = t.Key,
                    Amount = t.Min(x => x.FarePricesInfo.FirstOrDefault().Amount)
                }).ToList();

            var dataFirstRowPrice = (from a in pricelistnew
                                     join b in FirstRowPrice on
                                     new
                                     {
                                         x = a.FarePricesInfo.FirstOrDefault().Amount,
                                         y = a.FarePricesInfo.FirstOrDefault().ClassCode.Replace(" ", string.Empty).Replace("-", string.Empty)
                                     }
                                     equals
                                     new
                                     {
                                         x = b.Amount,
                                         y = b.ClassCode
                                     }
                                     select a).ToList();

            decimal AmountFirst = 0; string ClassFrist = string.Empty;
            pricelistnew = new List<TrainPrice>();
            foreach (var item in dataFirstRowPrice)
            {
                if (item.FarePricesInfo.FirstOrDefault().Amount != AmountFirst && item.FarePricesInfo.FirstOrDefault().ClassCode != ClassFrist)
                {
                    AmountFirst = item.FarePricesInfo.FirstOrDefault().Amount;
                    ClassFrist = item.FarePricesInfo.FirstOrDefault().ClassCode;
                    pricelistnew.Add(item);
                }
            }
            return pricelistnew;
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); return null; }
    }

    public string GetFareInfoDetails(string TicketTypeCode, string RestrictionDescription)
    {
        string TicketDetails = string.Empty;
        EvFareInformationList = new StationList().EvolviFareInformationList();
        if (EvFareInformationList != null && EvFareInformationList.Count > 0)
        {
            var fareInfoList = EvFareInformationList.Where(x => x.TicketTypeCode == TicketTypeCode).Count() > 1 ? EvFareInformationList.Where(x => x.TicketTypeCode == TicketTypeCode).Skip(1).ToList() : EvFareInformationList.Where(x => x.TicketTypeCode == TicketTypeCode).ToList();
            if (fareInfoList != null && fareInfoList.Count > 0)
            {
                string tod = fareInfoList.FirstOrDefault().TodAvailable == "1" ? "Available" : "Not Available";
                TicketDetails = "<h4>" + fareInfoList.FirstOrDefault().TicketTypeAltName + "</h4>";
                TicketDetails += "<p><strong>Ticket Code: </strong>" + fareInfoList.FirstOrDefault().TicketTypeCode + "</p>";
                TicketDetails += "<p><strong>Class: </strong>" + fareInfoList.FirstOrDefault().TicketTypeClass + "</p>";
                TicketDetails += "<p><strong>Reservation: </strong>" + fareInfoList.FirstOrDefault().ReservationRule + "</p>";
                TicketDetails += "<p><strong>Journey: </strong>" + fareInfoList.FirstOrDefault().JourneyType + "</p>";
                TicketDetails += "<p><strong>Routes: </strong>" + RestrictionDescription + "</p>";

                TicketDetails += "<p><strong>Refunds & Cancellations: </strong>" + fareInfoList.FirstOrDefault().RefundCancellationRule + "</p>";
                TicketDetails += "<p><strong>Changes to travel plans: </strong>" + fareInfoList.FirstOrDefault().ChangeRule + "</p>";
                TicketDetails += "<p><strong>Ticket Conditions: </strong>" + fareInfoList.FirstOrDefault().TicketTypeConditionsDescription + "</p>";
                TicketDetails += "<p><strong>Break of Journey: </strong>" + fareInfoList.FirstOrDefault().JourneyBreakRule + "</p>";
                TicketDetails += "<p><strong>Availability: </strong>" + fareInfoList.FirstOrDefault().AvailabilityRule + "</p>";
                TicketDetails += "<p><strong>Ticket on Departure: </strong>" + tod + "</p>";
                TicketDetails += "<p><strong>Pre-Booking requirement: </strong>" + fareInfoList.FirstOrDefault().PreBookingRequirement + "</p>";
                TicketDetails += "<p><strong>Valid out: </strong>" + fareInfoList.FirstOrDefault().ValidOutRule + "</p>";
                TicketDetails += "<p><strong>Valid return: </strong>" + fareInfoList.FirstOrDefault().ValidReturnRule + "</p>";
                TicketDetails += "<p><strong>Child discount: </strong>" + fareInfoList.FirstOrDefault().ChildDiscount + "</p>";
                TicketDetails += "<p><strong>Railcard discounts: </strong>" + fareInfoList.FirstOrDefault().RailcardDiscount + "</p>";
            }
        }
        return TicketDetails;
    }

    public string GetValidityCodeHtml(string validityCode)
    {
        try
        {
            EvolviValidityCodeRequest request = new EvolviValidityCodeRequest();
            EvolviValidityCodeResponse response = new EvolviValidityCodeResponse();
            OneHubRailOneHubClient client = new OneHubRailOneHubClient();
            if (!string.IsNullOrEmpty(validityCode))
            {
                request.ValidityCode = validityCode;
                response = client.EvolviValidityCodeInformation(request);
                if (response != null)
                    if (response.ErrorMessage == null)
                        return response.Details;
            }
            return "";
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
            return "";
        }
    }

    public string GetRefundAndCancellations(string TicketTypeCode, string RestrictionDescription)
    {
        string TicketDetails = string.Empty;
        EvFareInformationList = new StationList().EvolviFareInformationList();
        if (EvFareInformationList != null && EvFareInformationList.Count > 0)
        {
            var fareInfoList = EvFareInformationList.Where(x => x.TicketTypeCode == TicketTypeCode).Count() > 1 ? EvFareInformationList.Where(x => x.TicketTypeCode == TicketTypeCode).Skip(1).ToList() : EvFareInformationList.Where(x => x.TicketTypeCode == TicketTypeCode).ToList();
            if (fareInfoList != null && fareInfoList.Count > 0)
            {
                string tod = fareInfoList.FirstOrDefault().TodAvailable == "1" ? "Available" : "Not Available";
                TicketDetails += fareInfoList.FirstOrDefault().RefundCancellationRule.Replace(",", "$");
            }
        }
        return TicketDetails;
    }

    public string GetChangesToTravelPlans(string TicketTypeCode, string RestrictionDescription)
    {
        string TicketDetails = string.Empty;
        EvFareInformationList = new StationList().EvolviFareInformationList();
        if (EvFareInformationList != null && EvFareInformationList.Count > 0)
        {
            var fareInfoList = EvFareInformationList.Where(x => x.TicketTypeCode == TicketTypeCode).Count() > 1 ? EvFareInformationList.Where(x => x.TicketTypeCode == TicketTypeCode).Skip(1).ToList() : EvFareInformationList.Where(x => x.TicketTypeCode == TicketTypeCode).ToList();
            if (fareInfoList != null && fareInfoList.Count > 0)
            {
                string tod = fareInfoList.FirstOrDefault().TodAvailable == "1" ? "Available" : "Not Available";
                TicketDetails += fareInfoList.FirstOrDefault().ChangeRule.Replace(",", "$");
            }
        }
        return TicketDetails;
    }
}