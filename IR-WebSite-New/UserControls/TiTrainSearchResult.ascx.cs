﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using OneHubServiceRef;
using Business;
using System.Web.UI.HtmlControls;


public partial class UserControls_TiTrainSearchResult : System.Web.UI.UserControl
{
    #region Global Variable
    public string pricePoup = "";
    public string journyTag = "";

    private int _index;

    BookingRequestUserControl objBRUC;
    public string currency;
    public Guid currencyID = new Guid();
    Guid siteId;
    ManageBooking _masterBooking = new ManageBooking();
    ManageTrainDetails _master = new ManageTrainDetails();
    readonly Masters _objMaster = new Masters();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public static DateTime curentDateTime;
    public static List<getStationList_Result> list = new List<getStationList_Result>();
    public string _FromDate;
    public string _ToDate;
    List<P2PReservationIDInfo> lstP2PIdInfo = new List<P2PReservationIDInfo>();
    List<BookingRequestInfo> listBookingReqInfo = new List<BookingRequestInfo>();
    List<TrainPrice> listTrainPriceInfo = new List<TrainPrice>();
    Guid id = Guid.NewGuid();
    public string siteURL = string.Empty;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    #endregion

    #region PageEvents
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            hdnReq.Value = Request.QueryString["req"] != null ? Request.QueryString["req"].Trim() : "";
            if (hdnReq.Value == "BE" || hdnReq.Value == "NTV" || hdnReq.Value == "EV")
                return;

            ShowMessage(0, null);
            GetCurrencyCode();
            NotIsPostBack();

            if (Session["ErrorMessage"] != null)
            {
                if (Session["ErrorMessage"].ToString().Trim() != "ErrorMaxDate")
                {
                    ShowMessage(2, Session["ErrorMessage"].ToString());
                    Session.Remove("ErrorMessage");
                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    #endregion

    void NotIsPostBack()
    {
        if (!IsPostBack)
        {
            try
            {
                if (Session["TiTimer"] == null)
                    new StationList().GetTrenitaliaTime();

                hdnsiteURL.Value = new ManageFrontWebsitePage().GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
                Session["SHOPPINGCART"] = null;
                Session["BOOKING-REQUEST"] = null;
                if (Session["BookingUCRerq"] == null)
                    return;
                objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];

                hdnDateDepart.Value = objBRUC.depdt.ToString("dd MMM yyyy");
                hdnDateArr.Value = objBRUC.ReturnDate.Trim();

                ddlCountry.DataSource = _master.GetCountryDetail();
                ddlCountry.DataValueField = "CountryID";
                ddlCountry.DataTextField = "CountryName";
                ddlCountry.DataBind();

                if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
                {
                    var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
                    Guid CoutryID = Guid.Parse(cookie.Values["_cuntryId"]);
                    ddlCountry.SelectedValue = CoutryID.ToString().Trim();
                }

                BindResult();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
            currency = oManageClass.GetCurrency(currencyID);
            hdnCurrID.Value = currency;
        }
    }

    int GetEurostarBookingDays(BookingRequestUserControl request)
    {
        List<string> listStCode = new List<string> { "GBSPX", "BEBMI", "FRPNO", "FRPAR", "GBASI", "FRLIL", "FRLLE", "FRMLV", "GBEBF" };
        if (listStCode.Contains(request.depstCode) && listStCode.Contains(request.arrstCode))
            return 180;
        else
            return 0;
    }

    #region Earlier & Previous Train

    protected void lnkEarlier_Click(object sender, EventArgs e)
    {
        OutBoundSearch(-1);
    }
    protected void lnkLater_Click(object sender, EventArgs e)
    {
        OutBoundSearch(1);
    }
    void OutBoundSearch(int day)
    {

        DropDownList ddldepTime = ucTrainSearch.FindControl("ddldepTime") as DropDownList;
        int time = (Convert.ToInt32(ddldepTime.SelectedValue.Replace(":00", "")) + (day == -1 ? -3 : 3));
        if (time > 23 || time < 2)
        {
            TextBox txtDepartureDate = ucTrainSearch.FindControl("txtDepartureDate") as TextBox;
            if (!string.IsNullOrEmpty(txtDepartureDate.Text) && !txtDepartureDate.Text.Contains("DD/MM/YYYY"))
            {
                DateTime dt = Convert.ToDateTime(txtDepartureDate.Text);
                dt = dt.AddDays(day);
                txtDepartureDate.Text = dt.ToString("dd/MMM/yyyy");

            }
        }
        else
        {
            ddldepTime.SelectedValue = time.ToString().Length == 1 ? "0" + time.ToString() + ":00" : time.ToString() + ":00";
        }
        ucTrainSearch.btnSubmit_Click(new object(), new EventArgs());
    }

    protected void lnkInEarlier_Click(object sender, EventArgs e)
    {
        InBoundSearch(-1);
    }
    protected void lnkInLater_Click(object sender, EventArgs e)
    {
        InBoundSearch(1);
    }
    void InBoundSearch(int day)
    {
        DropDownList ddlReturnTime = ucTrainSearch.FindControl("ddlReturnTime") as DropDownList;
        int time = (Convert.ToInt32(ddlReturnTime.SelectedValue.Replace(":00", "")) + (day == -1 ? -3 : 3));
        if (time > 23 || time < 3)
        {
            TextBox txtReturnDate = ucTrainSearch.FindControl("txtReturnDate") as TextBox;
            if (!string.IsNullOrEmpty(txtReturnDate.Text) && !txtReturnDate.Text.Contains("DD/MM/YYYY"))
            {
                DateTime dt = Convert.ToDateTime(txtReturnDate.Text);
                dt = dt.AddDays(day);
                txtReturnDate.Text = dt.ToString("dd/MMM/yyyy");
            }
        }
        else
        {
            ddlReturnTime.SelectedValue = time.ToString().Length == 1 ? "0" + time.ToString() + ":00" : time.ToString() + ":00";
        }

        ucTrainSearch.btnSubmit_Click(new object(), new EventArgs());
    }

    #endregion

    public void BindResult()
    {
        TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
        if (pInfoSolutionsResponse.TrainInformationList == null)
            return;

        //Search JourneyBlock-header
        ltrinTo.Text = ltroutFrom.Text = objBRUC.FromDetail;
        ltrinFrom.Text = ltroutTo.Text = objBRUC.ToDetail;
        ltrinDate.Text = string.IsNullOrEmpty(objBRUC.ReturnDate) ? objBRUC.depdt.ToString("dd-MM-yyyy") : Convert.ToDateTime(objBRUC.ReturnDate).ToString("dd-MM-yyyy");
        ltroutDate.Text = objBRUC.depdt.ToString("dd-MM-yyyy");
        ltrPassenger.Text = objBRUC.Adults.ToString() + " Adult, " + objBRUC.Boys.ToString() + " Child, " + objBRUC.Youths.ToString() + " Youth and " + objBRUC.Seniors.ToString() + " Senior";

        // Outbound JourneyBlock-header 
        List<TrainInformation> outboundList = pInfoSolutionsResponse.TrainInformationList.Where(x => x.IsReturn == false).OrderBy(x => Convert.ToDateTime(x.DepartureDate)).ThenBy(x => Convert.ToDateTime(x.DepartureTime)).ToList();
        rptoutTi.DataSource = outboundList.Count > 3 ? outboundList.Take(3).ToList() : outboundList;
        rptoutTi.DataBind();

        List<TrainInformation> inboundList = pInfoSolutionsResponse.TrainInformationList.Where(x => x.IsReturn).OrderBy(x => Convert.ToDateTime(x.DepartureDate)).ThenBy(x => Convert.ToDateTime(x.DepartureTime)).ToList();
        ShowInboud.Visible = (inboundList.Count > 0);
        journyTag = (inboundList.Count > 0) ? "Return Journey" : "Booking Details";

        _index = outboundList.Count();
        rptinTi.DataSource = inboundList.Count > 3 ? inboundList.Take(3).ToList() : inboundList;
        rptinTi.DataBind();

    }

    #region Make Booking Request

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        try
        {
            if (new StationList().GetTrenitaliaTime() && Session["TrainSearch"] == null)
                Response.Redirect("~/");
            else
                Session["TiTimer"] = null;

            Session["IsFullPrice"] = null;
            Session["IsCheckout"] = null;
            Session["BOOKING-REQUEST"] = null;
            if (Session["P2POrderID"] != null)
            {
                new ManageUser().DeleteExistingP2PSale(Convert.ToInt64(Session["P2POrderID"].ToString()));
            }
            else
                Session["P2POrderID"] = new ManageBooking().CreateOrder(string.Empty, AgentuserInfo.UserID, Guid.Empty, siteId, "P2P", "0", "ItaliaTR");
            Session["ProductType"] = "P2P";

            Boolean res = false;
            if (!String.IsNullOrEmpty(hdnFromjsCode.Value))
                res = BookingRequestTi(hdnFromjsCode.Value, hdnFromoCd.Value, hdnFromoTypCd.Value, hdnFromoSubCd.Value, hdnFromsvCode.Value, hdnFromsvTypCode.Value, hdnFromtrainNo.Value, hdnFromtripType.Value, hdnFromagre.Value, hdnFromClass.Value, "Inbound");
            if (!String.IsNullOrEmpty(hdnTojsCode.Value))
                res = BookingRequestTi(hdnTojsCode.Value, hdnTooCd.Value, hdnTooTypCd.Value, hdnTooSubCd.Value, hdnTosvCode.Value, hdnTosvTypCode.Value, hdnTotrainNo.Value, hdnTotripType.Value, hdnToagre.Value, hdnToClass.Value, "Outbound");

            Session["P2PIdInfo"] = lstP2PIdInfo;
            Session["TiApiTimer"] = null;
            if (res == true)
                Response.Redirect("P2PBookingCart.aspx?req=IT");
            else
                Response.Redirect("TrainResults.aspx?req=TI", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public Boolean BookingRequestTi(string jsCode, string oCd, string oTypCd, string oSubCd, string svCode, string svTypCode, string trainNo, string tripType, string agre, string clas, string TypeOfJ)
    {
        try
        {
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
            if (pInfoSolutionsResponse != null)
            {
                TrainInformation trainInfo = pInfoSolutionsResponse.TrainInformationList.FirstOrDefault(tp => tp.JourneySolutionCode == jsCode);
                if (trainInfo != null)
                {
                    TrainInfoSegment[] trainSeglist = trainInfo.TrainInfoSegment.Where(x => x.JourneySolutionCode == jsCode).ToArray();
                    TrainInfoSegment trainSegInfo = trainInfo.TrainInfoSegment.FirstOrDefault(x => x.JourneySolutionCode == jsCode);
                    TrainInfoSegment trainSegInfolast = trainInfo.TrainInfoSegment.LastOrDefault(x => x.JourneySolutionCode == jsCode);
                    List<TrainPrice> trainPriceInfoList = trainInfo.PriceInfo.SelectMany(x => x.TrainPriceList.Select(t => t)).ToList();
                    TrainPrice trainPriceInfo = trainPriceInfoList.FirstOrDefault(o => o.Offer.OfferCode == Convert.ToInt32(oCd) && o.Offer.OfferTypeCode == Convert.ToInt32(oTypCd) && o.Offer.OfferSubgroupCode == Convert.ToInt32(oSubCd) && o.ServiceCode == svCode && o.ServiceTypeCode == svTypCode);
                    Guid id = Guid.NewGuid();

                    var srcCurId = FrontEndManagePass.GetCurrencyID("EUT");
                    #region Cart Information
                    List<ShoppingCartDetails> listCart = trainSegInfo != null ? new List<ShoppingCartDetails> {new ShoppingCartDetails
                            {
                                Id = id,
                                DepartureStation = trainSegInfo.DepartureStationName,
                                DepartureDate = trainSegInfo.DepartureDate,
                                DepartureTime = trainSegInfo.DepartureTime,

                                ArrivalStation = trainSegInfo.ArrivalStationName,
                                ArrivalDate = trainSegInfo.ArrivalDate,
                                ArrivalTime = trainSegInfo.ArrivalTime,

                                TrainNo = trainSegInfo.TrainNumber,
                                Title = ddlTitle.SelectedItem.Text ,
                                FirstName = txtFirstname.Text ,
                                LastName= txtLastname.Text,
                                Passenger = trainPriceInfo != null ? trainPriceInfo.NumAdults + " Adults, " + trainPriceInfo.NumBoys + " Child," + trainPriceInfo.NumYouths + " Youths," + trainPriceInfo.NumSeniors + " Seniors":"",
                                ServiceName = trainPriceInfo != null ?  trainPriceInfo.ServiceName :"",
                                Fare = trainPriceInfo != null ? trainPriceInfo.TravelType :"",
                                Price =  trainPriceInfo!=null ?( BusinessOneHub.IsNumeric(trainPriceInfo.MinPrice) ? FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(trainPriceInfo.MinPrice), siteId, srcCurId, currencyID).ToString("F") : trainPriceInfo.MinPrice):"N/A",  
                                netPrice = trainPriceInfo!=null?  Convert.ToDecimal(FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(trainPriceInfo.MinPrice), siteId, srcCurId, currencyID).ToString("F")):0       
                                
                            }} : null;

                    long POrderID = Convert.ToInt64(Session["P2POrderID"]);


                    tblP2PSale objP2P = new tblP2PSale();

                    objP2P.From = trainSegInfo.DepartureStationName;
                    objP2P.DateTimeDepature = Convert.ToDateTime(trainSegInfo.DepartureDate);

                    objP2P.To = trainSegInfolast.ArrivalStationName;
                    objP2P.DateTimeArrival = Convert.ToDateTime(trainSegInfolast.ArrivalDate);
                    objP2P.TrainNo = trainSegInfo.TrainNumber;

                    objP2P.Passenger = trainPriceInfo != null ? objBRUC.Adults + " Adults, " + objBRUC.Boys + " Child," + objBRUC.Youths + " Youth," + objBRUC.Seniors + " Senior" : "";

                    objP2P.Adult = objBRUC.Adults.ToString();
                    objP2P.Children = objBRUC.Boys.ToString();
                    objP2P.Youth = objBRUC.Youths.ToString();
                    objP2P.Senior = objBRUC.Seniors.ToString();

                    objP2P.SeviceName = trainPriceInfo != null ? trainPriceInfo.ServiceName : "";
                    objP2P.FareName = trainPriceInfo != null ? trainPriceInfo.TravelType : "";
                    objP2P.Price = trainPriceInfo != null ? (BusinessOneHub.IsNumeric(trainPriceInfo.MinPrice) ? FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(trainPriceInfo.MinPrice), siteId, srcCurId, currencyID) : 0) : 0;
                    objP2P.DepartureTime = trainSegInfo.DepartureTime;
                    objP2P.ArrivalTime = trainSegInfo.ArrivalTime;
                    objP2P.NetPrice = Convert.ToDecimal(GetRoundPrice(objP2P.Price.ToString()));
                    objP2P.Class = clas;
                    objP2P.CurrencyId = currencyID;
                    objP2P.Terms = trainPriceInfo.FareDescription;
                    objP2P.ApiPrice = BusinessOneHub.IsNumeric(trainPriceInfo.MinPrice) ? Convert.ToDecimal(trainPriceInfo.MinPrice) : 0;
                    objP2P.ApiName = "ITALIA";
                    objP2P.FromStation = objBRUC.FromDetail;
                    objP2P.ToStation = objBRUC.ToDetail;

                    //site currency//
                    objP2P.Site_USD = _masterBooking.GetCurrencyMultiplier("SUSD", Guid.Empty, POrderID);
                    objP2P.Site_SEU = _masterBooking.GetCurrencyMultiplier("SSEU", Guid.Empty, POrderID);
                    objP2P.Site_SBD = _masterBooking.GetCurrencyMultiplier("SSBD", Guid.Empty, POrderID);
                    objP2P.Site_GBP = _masterBooking.GetCurrencyMultiplier("SGBP", Guid.Empty, POrderID);
                    objP2P.Site_EUR = _masterBooking.GetCurrencyMultiplier("SEUR", Guid.Empty, POrderID);
                    objP2P.Site_INR = _masterBooking.GetCurrencyMultiplier("SINR", Guid.Empty, POrderID);
                    objP2P.Site_SEK = _masterBooking.GetCurrencyMultiplier("SSEK", Guid.Empty, POrderID);
                    objP2P.Site_NZD = _masterBooking.GetCurrencyMultiplier("SNZD", Guid.Empty, POrderID);
                    objP2P.Site_CAD = _masterBooking.GetCurrencyMultiplier("SCAD", Guid.Empty, POrderID);
                    objP2P.Site_JPY = _masterBooking.GetCurrencyMultiplier("SJPY", Guid.Empty, POrderID);
                    objP2P.Site_AUD = _masterBooking.GetCurrencyMultiplier("SAUD", Guid.Empty, POrderID);
                    objP2P.Site_CHF = _masterBooking.GetCurrencyMultiplier("SCHF", Guid.Empty, POrderID);
                    objP2P.Site_EUB = _masterBooking.GetCurrencyMultiplier("SEUB", Guid.Empty, POrderID);
                    objP2P.Site_EUT = _masterBooking.GetCurrencyMultiplier("SEUT", Guid.Empty, POrderID);
                    objP2P.Site_GBB = _masterBooking.GetCurrencyMultiplier("SGBB", Guid.Empty, POrderID);
                    objP2P.Site_THB = _masterBooking.GetCurrencyMultiplier("STHB", Guid.Empty, POrderID);
                    objP2P.Site_SGD = _masterBooking.GetCurrencyMultiplier("SSGD", Guid.Empty, POrderID);

                    objP2P.EUR_EUT = _masterBooking.GetCurrencyMultiplier("E_EUT", Guid.Empty, POrderID);
                    objP2P.EUR_EUB = _masterBooking.GetCurrencyMultiplier("E_EUB", Guid.Empty, POrderID);
                    if (objP2P.Price == 0)
                        return false;


                    Guid P2PID;
                    Guid LookupID = new ManageBooking().AddP2PBookingOrder(objP2P, Convert.ToInt64(Session["P2POrderID"]), out P2PID);
                    tblP2PSale objP2PRow = new ManageBooking().getP2PSingleRowData(P2PID);

                    P2PReservationIDInfo objP2PIdInfo = new P2PReservationIDInfo();
                    objP2PIdInfo.ID = P2PID;
                    objP2PIdInfo.P2PID = objP2PRow.P2PId;
                    objP2PIdInfo.JourneyType = TypeOfJ;
                    lstP2PIdInfo.Add(objP2PIdInfo);

                    Session["P2PID"] = P2PID;
                    tblOrderTraveller objTraveller = new tblOrderTraveller();
                    objTraveller.ID = Guid.NewGuid();
                    objTraveller.Title = ddlTitle.SelectedItem.Text;
                    objTraveller.FirstName = txtFirstname.Text;
                    objTraveller.LastName = txtLastname.Text;
                    objTraveller.Country = (ddlCountry.SelectedValue != "0" ? Guid.Parse(ddlCountry.SelectedValue) : Guid.Empty);
                    Session["objTravellerID"] = objTraveller.ID;
                    new ManageBooking().AddTraveller(objTraveller, LookupID);

                    if (Session["SHOPPINGCART"] != null)
                    {
                        List<ShoppingCartDetails> oldList = Session["SHOPPINGCART"] as List<ShoppingCartDetails>;
                        if (oldList != null) if (listCart != null) listCart.AddRange(oldList);
                    }
                    Session["SHOPPINGCART"] = listCart;
                    #endregion

                    #region Request Information

                    if (trainPriceInfo == null)
                        return false;

                    List<BookingRequest> listBookingRequest = new List<BookingRequest>();
                    BookingRequest bkrequest = new BookingRequest();
                    bkrequest.DepartureStationName = trainInfo.DepartureStationName;
                    bkrequest.IsInternational = pInfoSolutionsResponse.IsInternationl;

                    bool isTicketLess = trainInfo.TrainInfoSegment.Any(x => x.JourneySolutionCode == jsCode && x.IsTicketLessSale);
                    Session["SegmentType"] = isTicketLess ? "M" : trainInfo.SegmentType;

                    Owner[] owners = new Owner[1];
                    owners[0] = new Owner
                    {
                        Lastname = txtLastname.Text,
                        FirstName = txtFirstname.Text,
                        DocumentNumber = "",
                        DocumentType = DocumentType.None,
                        OwnerType = objBRUC.Adults > 0 || objBRUC.Seniors > 0 || objBRUC.Youths > 0 ? OwnerType.Adult : OwnerType.Boy
                    };

                    PurchasingForServiceOwnerRequest request = new PurchasingForServiceOwnerRequest
                    {
                        Header = new Header
                        {
                            onehubusername = "#@dots!squares",
                            onehubpassword = "#@dots!squares",
                            unitofwork = 0,
                            language = Language.nl_BE,
                        },

                        IssuanceMode = isTicketLess ? IssuanceMode.T : IssuanceMode.D,

                        //S= Standard , T=Ticketless, P=Postoclick ticketless , Q=Postoclick con ritiro alla SS, D= Differed Print , O= Hold on, C=change
                        //IssuanceMode = svTypCode=="1" ? IssuanceMode.D : IssuanceMode.T,
                        PurchaseOption = false,

                        PurchasingForServiceOwnerGroup = new PurchasingForServiceOwner[] 
                            { 
                                new PurchasingForServiceOwner
                                    { 
                                    Class = trainPriceInfo.Class,
                                    NumAdults = objBRUC.Adults,
                                    NumBoys = objBRUC.Boys,
                                    NumSeniors = objBRUC.Seniors,
                                    NumYouths = objBRUC.Youths,
                                    Owners=  owners,
                                    SeparatedTicketsInUse = true,

                                    ServicePreferences =trainPriceInfo.ServicePreferenceList.Select(x=>new ServicePreference
                                    {
                                        Bed=x.Bed,
                                        Service=x.Service,
                                        TrainNumber=x.TrainNumber                                    
                                    }).ToArray(),                                  
                                   
                                    ServiceSetting = new ServiceSetting
                                    {
                                        transportadultpassengersnumber = 0,
                                        transportboypassengersnumber = 0,
                                        traveltype = tripType
                                    },
                                    SolutionRate = new SolutionRate
                                    {
                                        JourneySolutionCode = jsCode.Replace("-",","),
                                        Offer = new Offer
                                        {
                                            Agreement = agre,
                                            OfferCode = int.Parse(oCd),
                                            OfferSubgroupCode = int.Parse(oSubCd),
                                            OfferTypeCode = int.Parse(oTypCd),
                                        }
                                    }
                                 }
                            }
                    };
                    bkrequest.PurchasingForServiceOwnerRequest = request;
                    bkrequest.Id = id;
                    listBookingRequest.Add(bkrequest);

                    if (Session["BOOKING-REQUEST"] != null)
                    {
                        List<BookingRequest> oldList = Session["BOOKING-REQUEST"] as List<BookingRequest>;
                        if (oldList != null) listBookingRequest.AddRange(oldList);
                    }
                    Session["BOOKING-REQUEST"] = listBookingRequest;

                    #endregion
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
            return false;
        }
    }

    #endregion

    protected void rptoutTi_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
            if (pInfoSolutionsResponse != null && pInfoSolutionsResponse.TrainInformationList != null)
                pInfoSolutionsResponse = new TrainInformationResponse
                {
                    ErrorMessage = pInfoSolutionsResponse.ErrorMessage,
                    IsInternationl = pInfoSolutionsResponse.IsInternationl,
                    TrainInformationList = pInfoSolutionsResponse.TrainInformationList.ToArray()
                };
            if (pInfoSolutionsResponse != null && _index == pInfoSolutionsResponse.TrainInformationList.Count())
                return;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (pInfoSolutionsResponse != null)
                {
                    Label lblTrainChanges = e.Item.FindControl("lblTrainChanges") as Label;
                    Label lblTrainChangesMobile = e.Item.FindControl("lblTrainChangesMobile") as Label;
                    Label lblCheapPrice = e.Item.FindControl("lblCheapPrice") as Label;
                    DropDownList ddlPrice = e.Item.FindControl("ddlFromPrice") as DropDownList;
                    HyperLink hlnkContinue = e.Item.FindControl("hlnkContinue") as HyperLink;
                    Literal ltrFrom = e.Item.FindControl("ltrFrom") as Literal;
                    Literal ltrTo = e.Item.FindControl("ltrTo") as Literal;
                    Repeater GrdRouteInfo = e.Item.FindControl("GrdRouteInfo") as Repeater;

                    var listTrainInfo = pInfoSolutionsResponse.TrainInformationList[_index];
                    if (GrdRouteInfo != null)
                    {
                        ltrFrom.Text = listTrainInfo.TrainInfoSegment.FirstOrDefault().DepartureStationName;
                        ltrTo.Text = listTrainInfo.TrainInfoSegment.LastOrDefault().ArrivalStationName;
                        GrdRouteInfo.DataSource = listTrainInfo.TrainInfoSegment.ToList();
                        GrdRouteInfo.DataBind();
                    }
                    if (listTrainInfo.TrainInfoSegment.ToList().Count() > 0)
                        lblTrainChangesMobile.Text = lblTrainChanges.Text = listTrainInfo.TrainInfoSegment.ToList().Count() == 1 ? "Direct" : ((listTrainInfo.TrainInfoSegment.ToList().Count() - 1).ToString() + " Change");

                    HtmlGenericControl divTrn = e.Item.FindControl("DivTr") as HtmlGenericControl;
                    string classDiv = "";
                    string jsCode = listTrainInfo.JourneySolutionCode.Trim().ToUpper();
                    string tripType = listTrainInfo.TripType;
                    if (listTrainInfo.PriceInfo != null && !listTrainInfo.TrainInfoSegment.Any(x => x.TrainDescr == "RV" || x.TrainDescr == "RE"))
                    {
                        var lPrice = listTrainInfo.PriceInfo.Where(a => a.TrainPriceList != null).ToList();
                        List<TrainPrice> list = listTrainInfo.PriceInfo.SelectMany(x => x.TrainPriceList.Select(y => y)).ToList();
                        list = list.OrderByDescending(t => Convert.ToDecimal(t.MaxPrice)).Select(tt => tt).ToList();
                        if (list.Count > 0)
                        {
                            var cheapPrice = Convert.ToDecimal(list.OrderBy(x => Convert.ToDecimal(x.MinPrice)).FirstOrDefault().MinPrice);
                            var srcCurId = FrontEndManagePass.GetCurrencyID("EUT");
                            var cheapestPrice = GetRoundPrice(FrontEndManagePass.GetPriceAfterConversion(cheapPrice, siteId, srcCurId, currencyID).ToString("F"));
                            lblCheapPrice.Text = currency.ToString() + cheapestPrice.ToString();
                        }
                        if (lPrice.Count() > 0 && list.Count > 0)
                        {
                            string trainNo = string.Empty;
                            var trainInfoSegment = listTrainInfo.TrainInfoSegment.FirstOrDefault(x => x.JourneySolutionCode == jsCode);
                            if (trainInfoSegment != null)
                                trainNo = trainInfoSegment.TrainNumber;
                            List<string> offer = list.Select(x => x.TravelType).Distinct().ToList();
                            List<string> serviceName = list.Select(x => x.ServiceName).Distinct().ToList();

                            foreach (var sItem in serviceName)
                            {
                                classDiv += "<div class='starail-JourneyBlock-class'><p>" + sItem + "</p>";
                                string priceDiv = "";
                                foreach (var item in offer)
                                {
                                    Guid id = Guid.NewGuid();
                                    var firstOrDefault = list.FirstOrDefault(x => x.TravelType == item && x.ServiceName == sItem);
                                    if (firstOrDefault != null)
                                    {
                                        var srcCurId = FrontEndManagePass.GetCurrencyID("EUT");
                                        if (BusinessOneHub.IsNumeric(firstOrDefault.MinPrice))
                                        {
                                            string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(firstOrDefault.MinPrice), siteId, srcCurId, currencyID).ToString("F");

                                            string text = sItem + "  " + item + "  " + currency + GetRoundPrice(price);
                                            string value = jsCode + "," + trainNo + "," + tripType + "," + firstOrDefault.ServiceCode + "," + firstOrDefault.ServiceTypeCode + "," +
                                                         firstOrDefault.Offer.OfferCode + "," + firstOrDefault.Offer.OfferTypeCode + "," + firstOrDefault.Offer.OfferSubgroupCode + "," +
                                                         firstOrDefault.Offer.Agreement.Trim() + "," + firstOrDefault.Class + "," + id + "," + GetRoundPrice(price);

                                            ddlPrice.Items.Add(new ListItem(text, value));
                                            hlnkContinue.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + value + ",outbound" + "');");
                                            anchor_Next.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + value + ",outbound" + "');");

                                            priceDiv += "<div class='starail-JourneyBlock-radioRow'><label> <span class='starail-Form-fancyRadioGroup'>";
                                            priceDiv += "<input type='radio' name='starail-ticket-outbound' id='rdoFromTI' value='" + value + "' />";
                                            priceDiv += item + "  " + currency + GetRoundPrice(price);
                                            priceDiv += "<span><i></i></span></span> &nbsp;</label><a href='#' class='js-lightboxOpen' data-lightbox-id='starail-ticket-info" + id + "'><i class='starail-Icon-question'></i></a> </div> ";

                                            pricePoup += "<div class='starail-Lightbox'id='starail-ticket-info" + id + "'><div class='starail-Lightbox-content'><div class='starail-Lightbox-closeContainer'><a href='#' class='starail-Lightbox-close js-lightboxClose'><i class='starail-Icon starail-Icon-close'></i></a>";
                                            pricePoup += "</div>  <div class='starail-Lightbox-content-inner'> <div class='starail-Lightbox-text'> <h3 class='starail-Lightbox-textTitle'>   Ticket Info</h3>";
                                            pricePoup += "<span  class='tresult'><h4>Class &amp; Seats  </h4>" + Server.HtmlDecode(firstOrDefault.FareDescription) + "</span>";
                                            pricePoup += "</div></div></div></div>";
                                        }
                                    }
                                }
                                classDiv += priceDiv;
                                classDiv += "</div>";
                            }
                        }
                        divTrn.InnerHtml = classDiv;
                        _index++;

                    }
                    else if (listTrainInfo.TrainInfoSegment.Any(x => x.TrainDescr == "RV" || x.TrainDescr == "RE"))
                    {
                        Button btnGetPrice = e.Item.FindControl("btnGetPrice") as Button;
                        Button btnGetPriceMobile = e.Item.FindControl("btnGetPriceMobile") as Button;
                        HyperLink btnContinueBooking = e.Item.FindControl("btnContinueBooking") as HyperLink;
                        HyperLink hlnkConfirm = e.Item.FindControl("hlnkConfirm") as HyperLink;
                        DropDownList ddlFromPrice = e.Item.FindControl("ddlFromPrice") as DropDownList;
                        hlnkConfirm.Visible = divTrn.Visible = ddlFromPrice.Visible = btnContinueBooking.Visible = false;
                        btnGetPriceMobile.Visible = btnGetPrice.Visible = true;
                        _index++;
                    }

                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }

    }

    protected void rptinTi_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
            if (pInfoSolutionsResponse != null && pInfoSolutionsResponse.TrainInformationList != null)
                pInfoSolutionsResponse = new TrainInformationResponse
                {
                    ErrorMessage = pInfoSolutionsResponse.ErrorMessage,
                    IsInternationl = pInfoSolutionsResponse.IsInternationl,
                    TrainInformationList = pInfoSolutionsResponse.TrainInformationList.ToArray()
                };
            if (pInfoSolutionsResponse != null && _index == pInfoSolutionsResponse.TrainInformationList.Count())
                return;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (pInfoSolutionsResponse != null)
                {
                    Literal ltrFrom = e.Item.FindControl("ltrFrom") as Literal;
                    Literal ltrTo = e.Item.FindControl("ltrTo") as Literal;
                    Label lblTrainChanges = e.Item.FindControl("lblTrainChanges") as Label;
                    Label lblTrainChangesMobile = e.Item.FindControl("lblTrainChangesMobile") as Label;
                    Label lblCheapPrice = e.Item.FindControl("lblCheapPrice") as Label;
                    DropDownList ddlPrice = e.Item.FindControl("ddlFromPrice") as DropDownList;
                    HyperLink hlnkContinue = e.Item.FindControl("hlnkContinue") as HyperLink;
                    Repeater GrdRouteInfo = e.Item.FindControl("GrdRouteInfo") as Repeater;

                    var listTrainInfo = pInfoSolutionsResponse.TrainInformationList[_index];
                    if (GrdRouteInfo != null)
                    {
                        ltrFrom.Text = listTrainInfo.TrainInfoSegment.FirstOrDefault().DepartureStationName;
                        ltrTo.Text = listTrainInfo.TrainInfoSegment.LastOrDefault().ArrivalStationName;
                        GrdRouteInfo.DataSource = listTrainInfo.TrainInfoSegment.ToList();
                        GrdRouteInfo.DataBind();
                    }
                    if (listTrainInfo.TrainInfoSegment.ToList().Count() > 0)
                        lblTrainChangesMobile.Text = lblTrainChanges.Text = listTrainInfo.TrainInfoSegment.ToList().Count() == 1 ? "Direct" : ((listTrainInfo.TrainInfoSegment.ToList().Count() - 1).ToString() + " Change");

                    HtmlGenericControl divTrn = e.Item.FindControl("DivTr") as HtmlGenericControl;
                    string classDiv = "";
                    string jsCode = listTrainInfo.JourneySolutionCode.Trim().ToUpper();
                    string tripType = listTrainInfo.TripType;
                    if (listTrainInfo.PriceInfo != null && !listTrainInfo.TrainInfoSegment.Any(x => x.TrainDescr == "RV" || x.TrainDescr == "RE"))
                    {
                        var lPrice = listTrainInfo.PriceInfo.Where(a => a.TrainPriceList != null).ToList();
                        List<TrainPrice> list = listTrainInfo.PriceInfo.SelectMany(x => x.TrainPriceList.Select(y => y)).ToList();
                        list = list.OrderByDescending(t => Convert.ToDecimal(t.MaxPrice)).Select(tt => tt).ToList();
                        if (list.Count > 0)
                        {
                            var cheapPrice = Convert.ToDecimal(list.OrderBy(x => Convert.ToDecimal(x.MinPrice)).FirstOrDefault().MinPrice);
                            var srcCurId = FrontEndManagePass.GetCurrencyID("EUT");
                            var cheapestPrice = GetRoundPrice(FrontEndManagePass.GetPriceAfterConversion(cheapPrice, siteId, srcCurId, currencyID).ToString("F"));
                            lblCheapPrice.Text = currency.ToString() + cheapestPrice.ToString();
                        }
                        if (lPrice.Count() > 0 && list.Count > 0)
                        {
                            string trainNo = string.Empty;
                            var trainInfoSegment = listTrainInfo.TrainInfoSegment.FirstOrDefault(x => x.JourneySolutionCode == jsCode);
                            if (trainInfoSegment != null)
                                trainNo = trainInfoSegment.TrainNumber;

                            List<string> offer = list.Select(x => x.TravelType).Distinct().ToList();
                            List<string> serviceName = list.Select(x => x.ServiceName).Distinct().ToList();

                            foreach (var sItem in serviceName)
                            {
                                classDiv += "<div class='starail-JourneyBlock-class'><p>" + sItem + "</p>";
                                string priceDiv = "";
                                foreach (var item in offer)
                                {
                                    Guid id = Guid.NewGuid();
                                    var firstOrDefault = list.FirstOrDefault(x => x.TravelType == item && x.ServiceName == sItem);
                                    if (firstOrDefault != null)
                                    {
                                        var srcCurId = FrontEndManagePass.GetCurrencyID("EUT");
                                        if (BusinessOneHub.IsNumeric(firstOrDefault.MinPrice))
                                        {
                                            string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(firstOrDefault.MinPrice), siteId, srcCurId, currencyID).ToString("F");

                                            string text = sItem + "  " + item + "  " + currency + GetRoundPrice(price);
                                            string value = jsCode + "," + trainNo + "," + tripType + "," + firstOrDefault.ServiceCode + "," + firstOrDefault.ServiceTypeCode + "," +
                                                         firstOrDefault.Offer.OfferCode + "," + firstOrDefault.Offer.OfferTypeCode + "," + firstOrDefault.Offer.OfferSubgroupCode + "," +
                                                         firstOrDefault.Offer.Agreement.Trim() + "," + firstOrDefault.Class + "," + id + "," + GetRoundPrice(price);

                                            ddlPrice.Items.Add(new ListItem(text, value));
                                            hlnkContinue.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + value + ",inbound" + "');");
                                            anchor_Next.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + value + ",inbound" + "');");

                                            priceDiv += "<div class='starail-JourneyBlock-radioRow'><label> <span class='starail-Form-fancyRadioGroup'>";
                                            priceDiv += "<input type='radio' name='starail-ticket-inbound' id='rdoToTI' value='" + value + "' />";
                                            priceDiv += item + "  " + currency + GetRoundPrice(price);
                                            priceDiv += "<span><i></i></span></span> &nbsp;</label><a href='#' class='js-lightboxOpen' data-lightbox-id='starail-ticket-info" + id + "'><i class='starail-Icon-question'></i></a> </div> ";

                                            pricePoup += "<div class='starail-Lightbox'id='starail-ticket-info" + id + "'><div class='starail-Lightbox-content'><div class='starail-Lightbox-closeContainer'><a href='#' class='starail-Lightbox-close js-lightboxClose'><i class='starail-Icon starail-Icon-close'></i></a>";
                                            pricePoup += "</div>  <div class='starail-Lightbox-content-inner'> <div class='starail-Lightbox-text'> <h3 class='starail-Lightbox-textTitle'>   Ticket Info</h3>";
                                            pricePoup += "<span  class='tresult'><h4>Class &amp; Seats  </h4>" + Server.HtmlDecode(firstOrDefault.FareDescription) + "</span>";
                                            pricePoup += "</div></div></div></div>";
                                        }
                                    }
                                }
                                classDiv += priceDiv;
                                classDiv += "</div>";
                            }
                        }
                        divTrn.InnerHtml = classDiv;
                        _index++;

                    }
                    else if (listTrainInfo.TrainInfoSegment.Any(x => x.TrainDescr == "RV" || x.TrainDescr == "RE"))
                    {

                        Button btnGetPrice = e.Item.FindControl("btnGetPrice") as Button;
                        Button btnGetPriceMobile = e.Item.FindControl("btnGetPriceMobile") as Button;
                        HyperLink btnContinueBooking = e.Item.FindControl("btnContinueBooking") as HyperLink;
                        HyperLink hlnkConfirm = e.Item.FindControl("hlnkConfirm") as HyperLink;
                        DropDownList ddlFromPrice = e.Item.FindControl("ddlFromPrice") as DropDownList;
                        hlnkConfirm.Visible = divTrn.Visible = ddlFromPrice.Visible = btnContinueBooking.Visible = false;
                        btnGetPriceMobile.Visible = btnGetPrice.Visible = true;
                        _index++;
                    }

                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void rptoutTi_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "GetPrice")
            {

                HiddenField hdnJournyTypeReturn = e.Item.FindControl("hdnJournyTypeReturn") as HiddenField;
                Session["InbountOrOutBound"] = Convert.ToBoolean(hdnJournyTypeReturn.Value.Trim()) ? "Inbound" : "Outbound";
                string[] str = e.CommandArgument.ToString().Split(',');
                string JsCode = str[0];
                string TripType = str[1];

                if (Session["TrainSearch"] == null) return;
                TrainInformationResponse response = Session["TrainSearch"] as TrainInformationResponse;
                RegionalTrainPrice RegionalTrainPrice = response.TrainInformationList.FirstOrDefault(x => x.JourneySolutionCode == JsCode).RegionalTrainPrice;

                Session["PriceInfoResponse"] = RegionalTrainPrice;
                Response.Redirect("TrainSegmentSearchResult.aspx");
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void rptinTi_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "GetPrice")
            {

                HiddenField hdnJournyTypeReturn = e.Item.FindControl("hdnJournyTypeReturn") as HiddenField;
                Session["InbountOrOutBound"] = Convert.ToBoolean(hdnJournyTypeReturn.Value.Trim()) ? "Inbound" : "Outbound";
                string[] str = e.CommandArgument.ToString().Split(',');
                string JsCode = str[0];
                string TripType = str[1];

                if (Session["TrainSearch"] == null) return;
                TrainInformationResponse response = Session["TrainSearch"] as TrainInformationResponse;
                RegionalTrainPrice RegionalTrainPrice = response.TrainInformationList.FirstOrDefault(x => x.JourneySolutionCode == JsCode).RegionalTrainPrice;

                Session["PriceInfoResponse"] = RegionalTrainPrice;
                Response.Redirect("TrainSegmentSearchResult.aspx");
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    #region Price Calculation
    public string GetPriceTable(TrainPrice tp, bool isTrenItalia)
    {
        var srcCurId = FrontEndManagePass.GetCurrencyID(isTrenItalia ? "EUT" : "EUB");
        string priceTable = "<table width='100%' class='tariffPriceTable' cellpadding='0' cellspacing='0'> <tbody><tr> <th colspan='3'>Price Table</th> </tr>";
        if (Convert.ToDecimal(tp.PerUnitAdultPrice) > 0)
        {
            string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitAdultPrice), siteId, srcCurId, currencyID).ToString("F");
            priceTable += "<tr class='cellWhite'><td>Adults </td> <td> <span class='pound'> " + currency + " " + GetRoundPrice(price) + " each </span>   </td> </tr>";
        }

        if (Convert.ToDecimal(tp.PerUnitChildPrice) > 0)
        {
            string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitChildPrice), siteId, srcCurId, currencyID).ToString("F");
            priceTable += "<tr class='cellWhite'><td>Childs </td> <td> <span class='pound'> " + currency + " " + GetRoundPrice(price) + " each </span>   </td> </tr>";
        }

        if (Convert.ToDecimal(tp.PerUnitYouthPrice) > 0)
        {
            string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitYouthPrice), siteId, srcCurId, currencyID).ToString("F");
            priceTable += "<tr class='cellWhite'><td>Youths </td> <td> <span class='pound'> " + currency + " " + GetRoundPrice(price) + " each </span>   </td> </tr>";
        }
        if (Convert.ToDecimal(tp.PerUnitSeniorPrice) > 0)
        {
            string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitSeniorPrice), siteId, srcCurId, currencyID).ToString("F");
            priceTable += "<tr class='cellWhite'><td>Seniors </td> <td> <span class='pound'> " + currency + " " + GetRoundPrice(price) + " each </span>   </td> </tr>";
        }
        priceTable += "</tbody></table>";
        return priceTable;
    }

    public Decimal GetTotalPrice(TrainPrice tp, bool isTrenItalia)
    {
        var srcCurId = FrontEndManagePass.GetCurrencyID(isTrenItalia ? "EUT" : "EUB");
        Decimal price = 0;
        if (Convert.ToDecimal(tp.PerUnitAdultPrice) > 0)
        {
            string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitAdultPrice), siteId, srcCurId, currencyID).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumAdults;
        }
        if (Convert.ToDecimal(tp.PerUnitChildPrice) > 0)
        {
            string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitChildPrice), siteId, srcCurId, currencyID).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumBoys;
        }
        if (Convert.ToDecimal(tp.PerUnitSeniorPrice) > 0)
        {
            string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitSeniorPrice), siteId, srcCurId, currencyID).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumSeniors;
        }
        if (Convert.ToDecimal(tp.PerUnitYouthPrice) > 0)
        {
            string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitYouthPrice), siteId, srcCurId, currencyID).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumYouths;
        }
        return price;
    }

    public Decimal GetTotalOriginalPrice(TrainPrice tp)
    {
        Decimal price = 0;
        if (Convert.ToDecimal(tp.PerUnitAdultPrice) > 0)
        {
            string Aprice = Convert.ToDecimal(tp.PerUnitAdultPrice).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumAdults;
        }
        if (Convert.ToDecimal(tp.PerUnitChildPrice) > 0)
        {
            string Aprice = Convert.ToDecimal(tp.PerUnitChildPrice).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumBoys;
        }
        if (Convert.ToDecimal(tp.PerUnitSeniorPrice) > 0)
        {
            string Aprice = Convert.ToDecimal(tp.PerUnitSeniorPrice).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumSeniors;
        }
        if (Convert.ToDecimal(tp.PerUnitYouthPrice) > 0)
        {
            string Aprice = Convert.ToDecimal(tp.PerUnitYouthPrice).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumYouths;
        }
        return price;
    }

    public Decimal GetTotalPriceBeNe(TrainPrice tp, bool isTrenItalia)
    {
        var srcCurId = FrontEndManagePass.GetCurrencyID(isTrenItalia ? "EUT" : "EUB");
        Decimal price = 0;
        if (Convert.ToDecimal(tp.MinPrice) > 0)
        {
            string MinPrice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.MinPrice), siteId, srcCurId, currencyID).ToString("F");
            price = Convert.ToDecimal(GetRoundPrice(MinPrice));
        }
        return price;
    }

    string GetRoundPrice(string price)
    {
        //Please round up the prices to nearest above 0.50
        //If he price is 100.23, mark it as 100.50, 
        //If the price is 100.65 mark it as 102.00

        string[] strPrice = price.ToString().Split('.');

        if (strPrice[1] != null && Convert.ToDecimal(strPrice[1]) > 50)
            return (Convert.ToDecimal(strPrice[0]) + 1).ToString() + ".00";
        else if (strPrice[1] != null && Convert.ToDecimal(strPrice[1]) > 0 && Convert.ToDecimal(strPrice[1]) <= 50)
            return strPrice[0].Trim() + ".50";
        else
            return strPrice[0].Trim() + ".00";

    }
    #endregion

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}