﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucTravellCutsHeader.ascx.cs"
    Inherits="UserControls_ucTravellCutsHeader" %>
<style type="text/css">
    .seasional-info
    {
        width: 262px !important;
        float: right;
        top: -50px;
    }
    .starail-Switcher-tab
    {
        z-index: 1;
    }
    @media (max-width: 640px)
    {
        .seasional-info
        {
            position: absolute;
            right: -275px;
            top: 10px;
        }
    }
    @media (max-width: 360px)
    {
        .seasional-info
        {
            position: absolute;
            right: -250px;
            top: 60px;
        }
    }
</style>
<div class="starail-Nav-mobile starail-u-hideDesktop">
</div>
<div class="starail-Outer-wrap">
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" CssClass="starail-Form-required" />
        </div>
    </asp:Panel>
    <div class="js-starail-nav-trigger starail-u-hideDesktop starail-Nav-mobileClose">
    </div>
    <div class="starail-Header-mobile starail-u-hideDesktop">
        <div class="seasional-info" id="hour1" runat="server">
            <%=Session["AttentionBody"] != null ? Newtonsoft.Json.JsonConvert.DeserializeObject(Session["AttentionBody"].ToString()) : string.Empty%>
        </div>
        <a href="#" class="js-starail-nav-trigger starail-Header-mobileMenu"><i class="starail-Icon-bars">
        </i><span class="starail-u-visuallyHidden">Menu</span></a> <a class="starail-Header-mobileLogo"
            href="<%=siteURL%>">
            <img src="<%=SiteLogoUrl %>" alt="TravellCuts" />
        </a>
    </div>
    <div class='starail-Full-wrap'>
        <div class='starail-Page-wrap'>
            <header class="starail-clearfix starail-Header starail-u-hideMobile" role="banner">
                <div class="starail-u-hideMobile starail-u-cf starail-Header-topRow" itemscope itemtype="http://schema.org/Organization">
                    <div class="starail-Header-logo">
                        <a itemprop="url" href="<%=siteURL%>">
                               <img src="<%=SiteLogoUrl %>" alt="TravellCuts" /> 
                            </a>
                    </div>
                                <%=Session["AttentionBody"] != null ? Newtonsoft.Json.JsonConvert.DeserializeObject(Session["AttentionBody"].ToString()) : string.Empty%>
                    <div class="starail-Header-contact">
                        <p class="starail-Header-contact-title">
                            <%=b2bSiteHeading%>
                        </p>
                        <h2 class="starail-Header-contact-phone" itemprop="telephone"><%=b2bSitePhone%></h2>
                        <p class="starail-Header-contact-disclaimer">
                            <%=b2bSiteTitle%>
                        </p>
                        <div class="seasional-info" style="width:2px;"> 
                             <div id="hour4" runat="server"  clientidmode="Static">
                        </div> 
                        </div>                           
                        </div>
                    </div>
       <nav class="starail-u-cf starail-Nav-wrap" id="sta-nav-wrap">
                        <ul class="starail-u-cf starail-Nav" role="navigation" id="starail-nav">
                            <li class="starail-u-hideDesktop">
                            <div>
                                    <div class="starail-Header-contact-title">   
   
    <div class="seasional-info"> 
    <%=b2bSiteHeading%>  </div> </div>
                                    <h2 class="starail-Header-contact-phone" itemprop="telephone"><%=b2bSitePhone%></h2>
                                </div>
                            </li> 
                            <li class="starail-Nav-right starail-Nav-pinToBottom" style='<%=IsTravelAgentLi ? "" : "display: none" %>'> 
                                <a href="<%=siteURL%>agent"> <%=AgentName %> </a>
                            </li>
                            <li class="starail-Nav-right" style='<%=IsTravelAgentLi ? "" : "display: none" %>'> 
                              <%=MyCart%>
                            </li>
                            <li class="starail-Nav-right" style='<%=IsTravelAgentLi ? "" : "display: none" %>'>
                                <div id="DivAgentGereet" runat="server">Welcome,  <asp:Label ID="lblAgent" runat="server"/> !</div>
                            </li> 
                            <li>
                                <a href='<%=siteURL%>' style='<%=IsTravelAgentLi ? "" : "display: none" %>'> Rail Home   </a> 
                            </li>
                            <li>
                            <a id="printingAgent" runat="server" visible="false">Print Queue </a> </li>
                            <li>
                             <a id="aFoclink" runat="server" visible="false">FOC / AD75</a></li>
                            <li>
                                <a href='<%=siteURL%>Faq' style='<%=IsTravelAgentLi ? "" : "display: none" %>'> FAQ   </a> 
                            </li>
                            <li>
                                 <a href='<%=siteURL%>feedback' style='<%=IsTravelAgentLi ? "" : "display: none" %>'> Feedback </a>
                            </li>
                            <li>
                            <a href='<%=siteURL%>security' style='<%=IsTravelAgentLi ? "" : "display: none" %>'> Security  </a></li>
                        </ul>
                    </nav>
    </header>
