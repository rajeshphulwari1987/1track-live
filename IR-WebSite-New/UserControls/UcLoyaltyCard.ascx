﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UcLoyaltyCard.ascx.cs"
    Inherits="UserControls_UcLoyaltyCard" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .pnlTravellerPopUPNewIR
    {
        width: 26% !important;
        left: 35% !important;
        z-index: 10 !important;
        position: absolute;
        border: 3px solid #941e34;
        margin-top: -76px;
        box-shadow: 10px 10px 5px #888888;
    }
    
    .pnlTravellerPopUPIR
    {
        width: 26% !important;
        left: 35% !important;
        z-index: 10 !important;
        position: absolute;
        border: 3px solid #941e34;
        margin-top: -76px;
        box-shadow: 10px 10px 5px #888888;
    }
    
    .pnlTravellerPopUPNewIR .btnclose_popup, .pnlTravellerPopUPIR .btnclose_popup
    {
        background-color: #941e34;
    }
    
    .pnlTravellerPopUPSTA
    {
        width: 26% !important;
        left: 59% !important;
        z-index: 10 !important;
        position: absolute;
        border: 3px solid #0c6ab8;
        margin-top: -76px;
        box-shadow: 10px 10px 5px #888888;
    }
    .pnlTravellerPopUPSTA .btnclose_popup
    {
        background-color: #0c6ab8;
    }
    .pnlTravellerPopUP td, .pnlTravellerPopUP th
    {
        width: 25% !important;
    }
    .pnlTravellerPopUP th
    {
        padding-left: 10px;
    }
    .pnlTravellerPopUP .starail-Form-row
    {
        height: 180px;
        overflow-y: auto;
    }
    .pnlTravellerPopUP .starail-Form-row table tr td
    {
        padding-left: 10px;
    }
    .travellpop
    {
        padding: 18px 1px 18px 5px !important;
    }
    .ddl-width-100
    {
        width: 50% !important;
    }
    .pnlpopup
    {
        z-index: 111 !important;
        position: fixed !important;
        left: 23%;
        top: 25%;
    }
    .modalBackgroundnew
    {
        position: fixed !important;
        left: 0px !important;
        top: 0px !important;
        z-index: 110 !important;
        width: 100% !important;
        height: 100% !important;
    }
    @media only screen and (max-width: 480px)
    {
        .ddl-width-100
        {
            width: 100% !important;
        }
        .popup-inner
        {
            width: 90% !important;
            left: 7% !important;
            top: 10% !important;
        }
        .popup-inner .title
        {
            width: 100% !important;
            left: 0 !important;
        }
    
        .pnlTravellerPopUPSTA
        {
            width: 95% !important;
            left: 2% !important;
            margin-top: -205px !important;
        }
    
        .pnlTravellerPopUPIR
        {
            left: 2% !important;
            width: 95% !important;
            margin-top: -205px !important;
        }
    
        .pnlTravellerPopUPNewIR
        {
            left: 2% !important;
            width: 95% !important;
            margin-top: -205px !important;
        }
        .pnlTravellerPopUP th, .pnlTravellerPopUP td
        {
            width: 0% !important;
        }
    }
</style>
<asp:UpdatePanel ID="updLoyalty" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnNewOrderID" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnFilter" runat="server" ClientIDMode="Static" />
        <div class="starail-Form starail-Form--onBoxNarrow starail-SearchTickets-form" id="divSearch"
            runat="server">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="warning" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="starail-Form-row starail-SearchTickets-destination hideoverflow">
                <div class="starail-SearchTickets-destination-row starail-SearchTickets-destination-row--start hideoverflow">
                    <label for="starail-startlocation" class="starail-Form-label starail-u-visuallyHiddenMobile">
                        From<span class="val-error">*</span>
                    </label>
                    <div class="starail-SearchTickets-destinationInput hideoverflow loysetposition">
                        <asp:RequiredFieldValidator ID="reqvalerror1" runat="server" ValidationGroup="vgs"
                            Display="Dynamic" ControlToValidate="txtFrom" />
                        <asp:TextBox ID="txtFrom" runat="server" ClientIDMode="Static" onkeyup="selectpopup(this)"
                            placeholder="Enter a start location" onkeydown="Getkeydown(event,this)" class="starail-Form-input"
                            autocomplete="off" />
                        <span id="spantxtFrom" style="display: none"></span>
                        <asp:HiddenField ID="hdnFrm" runat="server" />
                    </div>
                </div>
                <div class="starail-DestinationIcon">
                    <div class="starail-DestinationIcon-line">
                    </div>
                    <div class="starail-DestinationIcon-circle starail-DestinationIcon-circle--filled">
                    </div>
                    <div class="starail-DestinationIcon-circle">
                    </div>
                </div>
                <div class="starail-SearchTickets-destination-row starail-SearchTickets-destination-row--end hideoverflow">
                    <label for="starail-endlocation" class="starail-Form-label starail-u-visuallyHiddenMobile">
                        To<span class="val-error">*</span>
                    </label>
                    <div class="starail-SearchTickets-destinationInput hideoverflow loysetposition">
                        <asp:RequiredFieldValidator ID="reqvalerror2" runat="server" ValidationGroup="vgs"
                            Display="Dynamic" ControlToValidate="txtTo" />
                        <asp:TextBox ID="txtTo" runat="server" onkeyup="selectpopup(this)" onkeydown="Getkeydown(event,this)"
                            placeholder="Enter a destination" ClientIDMode="Static" class="starail-Form-input"
                            autocomplete="off" />
                        <span id="spantxtTo" style="display: none"></span>
                    </div>
                </div>
                <a href="#" class="starail-SearchTickets-switch-trigger"><i class="starail-Icon starail-Icon-reverse">
                </i><span class="starail-u-visuallyHidden">Switch direction</span></a>
            </div>
            <div class="starail-Form-row">
                <label for="" class="starail-Form-label starail-u-invisible starail-u-visuallyHiddenMobile journeylbl">
                    Journey Type</label>
                <div class="starail-u-cf starail-Form-switchRadioGroup">
                    <asp:RadioButtonList ID="rdBookingType" runat="server" ValidationGroup="vg" RepeatDirection="Horizontal"
                        CssClass="switchRadioGroup" Width="100%" AutoPostBack="True" OnSelectedIndexChanged="rdBookingType_SelectedIndexChanged">
                        <asp:ListItem Value="0" Selected="True">One-way</asp:ListItem>
                        <asp:ListItem Value="1">Return</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="starail-Form-row starail-u-cf">
                <label for="leaving" class="starail-Form-label">
                    Leaving<span class="val-error">*</span>
                </label>
                <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                    <div class="starail-Form-datePicker">
                        <asp:TextBox ID="txtDepartureDate" runat="server" class="starail-Form-input" Text="DD/MM/YYYY"
                            autocomplete="off" onfocus="DatePickerCall()" />
                        <asp:RequiredFieldValidator ID="reqvalerror11" runat="server" ValidationGroup="vgs"
                            InitialValue="DD/MM/YYYY" Display="Dynamic" ControlToValidate="txtDepartureDate" />
                        <i class="starail-Icon-datepicker"></i>
                    </div>
                    <asp:DropDownList ID="ddldepTime" runat="server" CssClass="starail-Form-select starail-Form-inputContainer--time starail-Form-inputContainer--last">
                        <asp:ListItem>00:00</asp:ListItem>
                        <asp:ListItem>01:00</asp:ListItem>
                        <asp:ListItem>02:00</asp:ListItem>
                        <asp:ListItem>03:00</asp:ListItem>
                        <asp:ListItem>04:00</asp:ListItem>
                        <asp:ListItem>05:00</asp:ListItem>
                        <asp:ListItem>06:00</asp:ListItem>
                        <asp:ListItem>07:00</asp:ListItem>
                        <asp:ListItem>08:00</asp:ListItem>
                        <asp:ListItem Selected="True">09:00</asp:ListItem>
                        <asp:ListItem>10:00</asp:ListItem>
                        <asp:ListItem>11:00</asp:ListItem>
                        <asp:ListItem>12:00</asp:ListItem>
                        <asp:ListItem>13:00</asp:ListItem>
                        <asp:ListItem>14:00</asp:ListItem>
                        <asp:ListItem>15:00</asp:ListItem>
                        <asp:ListItem>16:00</asp:ListItem>
                        <asp:ListItem>17:00</asp:ListItem>
                        <asp:ListItem>18:00</asp:ListItem>
                        <asp:ListItem>19:00</asp:ListItem>
                        <asp:ListItem>20:00</asp:ListItem>
                        <asp:ListItem>21:00</asp:ListItem>
                        <asp:ListItem>22:00</asp:ListItem>
                        <asp:ListItem>23:00</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="starail-Form-row starail-u-cf" runat="server" id="returndiv" visible="false">
                <label for="returning" class="starail-Form-label">
                    Returning<asp:Label runat="server" ID="lblrtnerror" CssClass="val-error" Text="*"
                        Visible="false"></asp:Label>
                </label>
                <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                    <div class="starail-Form-datePicker loyreturntxt">
                        <asp:TextBox ID="txtReturnDate" runat="server" class="starail-Form-input" Text="DD/MM/YYYY"
                            autocomplete="off" Enabled="False" onfocus="DatePickerCall()" />
                        <asp:RequiredFieldValidator ID="reqvalerror3" runat="server" ValidationGroup="vgs"
                            InitialValue="DD/MM/YYYY" Enabled="false" Display="Dynamic" ControlToValidate="txtReturnDate" />
                        <i class="starail-Icon-datepicker"></i>
                    </div>
                    <asp:DropDownList ID="ddlReturnTime" runat="server" CssClass="loyreturnselect" Enabled="false">
                        <asp:ListItem>00:00</asp:ListItem>
                        <asp:ListItem>01:00</asp:ListItem>
                        <asp:ListItem>02:00</asp:ListItem>
                        <asp:ListItem>03:00</asp:ListItem>
                        <asp:ListItem>04:00</asp:ListItem>
                        <asp:ListItem>05:00</asp:ListItem>
                        <asp:ListItem>06:00</asp:ListItem>
                        <asp:ListItem>07:00</asp:ListItem>
                        <asp:ListItem>08:00</asp:ListItem>
                        <asp:ListItem Selected="True">09:00</asp:ListItem>
                        <asp:ListItem>10:00</asp:ListItem>
                        <asp:ListItem>11:00</asp:ListItem>
                        <asp:ListItem>12:00</asp:ListItem>
                        <asp:ListItem>13:00</asp:ListItem>
                        <asp:ListItem>14:00</asp:ListItem>
                        <asp:ListItem>15:00</asp:ListItem>
                        <asp:ListItem>16:00</asp:ListItem>
                        <asp:ListItem>17:00</asp:ListItem>
                        <asp:ListItem>18:00</asp:ListItem>
                        <asp:ListItem>19:00</asp:ListItem>
                        <asp:ListItem>20:00</asp:ListItem>
                        <asp:ListItem>21:00</asp:ListItem>
                        <asp:ListItem>22:00</asp:ListItem>
                        <asp:ListItem>23:00</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="starail-Form-row starail-u-cf starail-SearchTickets-quantity">
                <div id="pnlTravellerPopUP" runat="server" style="display: none;" class="pnlTravellerPopUP">
                    <div class="starail-popup">
                        <div class="travellpop">
                            <a id="close_popup" class="btnclose_popup"><i class="starail-Icon starail-Icon-close"
                                style="line-height: 31px;"></i></a>
                            <div class="starail-Form-row">
                                <table>
                                    <tbody>
                                        <tr>
                                            <th>
                                                Country
                                            </th>
                                            <th>
                                                Child
                                            </th>
                                            <th>
                                                Youth
                                            </th>
                                            <th>
                                                Senior
                                            </th>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 10px; padding-bottom: 10px;">
                                                Eurostar
                                            </td>
                                            <td>
                                                4-11y
                                            </td>
                                            <td>
                                                12-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Austria
                                            </td>
                                            <td>
                                                6-15y
                                            </td>
                                            <td>
                                                16-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Belgium
                                            </td>
                                            <td>
                                                6-15y
                                            </td>
                                            <td>
                                                16-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Bosnia
                                            </td>
                                            <td>
                                                4-11y
                                            </td>
                                            <td>
                                                12-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Bulgaria
                                            </td>
                                            <td>
                                                6-11y
                                            </td>
                                            <td>
                                                12-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Croatia
                                            </td>
                                            <td>
                                                4-11y
                                            </td>
                                            <td>
                                                12-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Denmark
                                            </td>
                                            <td>
                                                6-15y
                                            </td>
                                            <td>
                                                16-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Finland
                                            </td>
                                            <td>
                                                6-16y
                                            </td>
                                            <td>
                                                17-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                France
                                            </td>
                                            <td>
                                                4-11y
                                            </td>
                                            <td>
                                                12-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Germany
                                            </td>
                                            <td>
                                                6-14y
                                            </td>
                                            <td>
                                                15-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Greece
                                            </td>
                                            <td>
                                                4-11y
                                            </td>
                                            <td>
                                                12-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Hungary
                                            </td>
                                            <td>
                                                6-13y
                                            </td>
                                            <td>
                                                14-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Italy
                                            </td>
                                            <td>
                                                4-11y
                                            </td>
                                            <td>
                                                12-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Luxemburg
                                            </td>
                                            <td>
                                                6-11y
                                            </td>
                                            <td>
                                                12-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Macedonia
                                            </td>
                                            <td>
                                                4-11y
                                            </td>
                                            <td>
                                                12-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Netherlands
                                            </td>
                                            <td>
                                                4-11y
                                            </td>
                                            <td>
                                                12-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Poland
                                            </td>
                                            <td>
                                                4-11y
                                            </td>
                                            <td>
                                                12-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Portugal
                                            </td>
                                            <td>
                                                4-11y
                                            </td>
                                            <td>
                                                12-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Romania
                                            </td>
                                            <td>
                                                4-11y
                                            </td>
                                            <td>
                                                12-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Serbia
                                            </td>
                                            <td>
                                                4-11y
                                            </td>
                                            <td>
                                                12-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Slovakia
                                            </td>
                                            <td>
                                                6-11y
                                            </td>
                                            <td>
                                                12-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Slovenia
                                            </td>
                                            <td>
                                                6-11y
                                            </td>
                                            <td>
                                                12-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Spain
                                            </td>
                                            <td>
                                                4-13y
                                            </td>
                                            <td>
                                                14-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Switzerland
                                            </td>
                                            <td>
                                                6-15y
                                            </td>
                                            <td>
                                                16-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                UK
                                            </td>
                                            <td>
                                                6 - 16y
                                            </td>
                                            <td>
                                                17-25y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Australia
                                            </td>
                                            <td>
                                                4-15y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                New Zaeland
                                            </td>
                                            <td>
                                                2-14y
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                USA
                                            </td>
                                            <td>
                                                2-12y
                                            </td>
                                            <td>
                                                62+
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Canada
                                            </td>
                                            <td>
                                                2-11y
                                            </td>
                                            <td>
                                                60+
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <label for="" class="starail-Form-label">
                    Who's going?</label>
                <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                    <div class="starail-Form-inputContainer-col irtextchange">
                        <asp:DropDownList ID="ddlAdult" runat="server" CssClass="starail-Form-select starail-Form-select--narrow ddltextchange"
                            Style="padding-right: 0px;">
                        </asp:DropDownList>
                        <label for="starail-adult" id="lblAdult" runat="server">
                            Adults (26-65 at time of travel)</label>
                        <label for="" id="lblIRAdult" runat="server" style="display: none;">
                            Adult (26-65yr)
                        </label>
                    </div>
                    <div class="starail-Form-inputContainer-col irtextchange">
                        <asp:DropDownList ID="ddlChild" runat="server" CssClass="starail-Form-select starail-Form-select--narrow ddltextchange"
                            Style="padding-right: 0px;">
                        </asp:DropDownList>
                        <label for="starail-children" id="lblChild" runat="server">
                            Children</label>
                        <label for="" id="lblIRChild" runat="server" style="display: none;">
                            Children
                        </label>
                    </div>
                    <div class="starail-Form-inputContainer-col irtextchange">
                        <asp:DropDownList ID="ddlYouth" runat="server" CssClass="starail-Form-select starail-Form-select--narrow ddltextchange"
                            Style="padding-right: 0px;">
                        </asp:DropDownList>
                        <label for="starail-adult" id="lblYouth" runat="server">
                            Youths</label>
                        <label for="" id="lblIRYouth" runat="server" style="display: none;">
                            Youth
                        </label>
                    </div>
                    <div class="starail-Form-inputContainer-col irtextchange">
                        <asp:DropDownList ID="ddlSenior" runat="server" CssClass="starail-Form-select starail-Form-select--narrow ddltextchange"
                            Style="padding-right: 0px;">
                        </asp:DropDownList>
                        <label for="starail-adult" id="lblSenior" runat="server">
                            Seniors (over 66 at time of travel)</label>
                        <label for="" id="lblIRSenior" runat="server" style="display: none;">
                            Senior (66+yr)
                        </label>
                    </div>
                </div>
                <!-- starail-Form-selectGroup -->
            </div>
            <div class="starail-Form-row starail-u-cf" id="div_Class" runat="server">
                <label for="leaving" class="starail-Form-label">
                    Class
                </label>
                <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                    <asp:RadioButtonList ID="ddlClass" CssClass="loyclassradiotbl" runat="server" RepeatDirection="Horizontal"
                        Width="100%">
                        <asp:ListItem class="starail-Form-fancyRadioGroup loyspanradio" Selected="True" Value="0">All</asp:ListItem>
                        <asp:ListItem class="starail-Form-fancyRadioGroup loyspanradio" Value="1">1st</asp:ListItem>
                        <asp:ListItem class="starail-Form-fancyRadioGroup loyspanradio" Value="2">2nd</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="starail-Form-row starail-u-cf" id="div_Transfer" runat="server">
                <label for="returning" class="starail-Form-label">
                    Max Transfers</label>
                <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                    <asp:DropDownList ID="ddlTransfer" runat="server" class="starail-Form-select starail-Form-inputContainer--time starail-Form-inputContainer--last ddl-width-100">
                        <asp:ListItem Value="0">Direct Trains only</asp:ListItem>
                        <asp:ListItem Value="1">Max. 1 transfer</asp:ListItem>
                        <asp:ListItem Value="2">Max. 2 transfers</asp:ListItem>
                        <asp:ListItem Value="3" Selected="True">Show all</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="starail-Form-row">
                <asp:Button ID="btnCheckout" runat="server" Text="Search Tickets" CssClass="starail-Button starail-Form-button starail-Form-button--primary IR-GaCode"
                    ValidationGroup="vgs" OnClick="btnCheckout_Click" OnClientClick="Focusonerrorcontrol();checkPassenger(event);EvolviMaxPassenger(event);" />
                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="vgs" ShowMessageBox="false"
                    ShowSummary="false" runat="server" />
            </div>
        </div>
        <div class="starail-Form starail-Form--onBoxNarrow starail-SearchTickets-form" id="divSearchMessage"
            runat="server" style="text-align: center; color: Red;">
            Sorry! Ticket booking is not enabled for this site.
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ddlAdult" />
        <asp:AsyncPostBackTrigger ControlID="ddlChild" />
        <asp:AsyncPostBackTrigger ControlID="ddlYouth" />
        <asp:AsyncPostBackTrigger ControlID="ddlSenior" />
        <asp:AsyncPostBackTrigger ControlID="btnCheckout" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
<asp:HiddenField ID="hdnForModelUK" runat="server" />
<asp:HiddenField ID="hdnForModel" runat="server" />
<div class="clear">
</div>
<asp:Panel ID="pnlPassenger" runat="server" CssClass="modal-popup pnlpopupbox" Style="display: none">
    <div class="modalBackground modalBackgroundnew">
    </div>
    <div class="popup-inner pnlpopup" style="z-index: 111; position: fixed; left: 23%;
        top: 25%;">
        <div class="title">
            Please select passenger type
        </div>
        <p>
            Please enter at least 1 adult, senior or junior(youth) passenger.</p>
        <p>
            Children cannot travel unattended.</p>
        <p>
            There is a limitation for accompanied children:</p>
        <p>
            - Max. 4 children per passenger allowed.</p>
        <div>
            <button type="button" class="starail-Button btn-black newbutton closex" style="float: right">
                Cancel</button>
        </div>
    </div>
</asp:Panel>
<input id="hdntxtFromRailName" type="hidden" runat="server" clientidmode="Static" />
<input id="hdntxtToRailName" type="hidden" runat="server" clientidmode="Static" />
<asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="updLoyalty"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="modalBackground progessposition">
        </div>
        <div class="progess-inner2">
            UPDATING RESULTS...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<script type="text/javascript">
    function checkPassenger(event) {
        if ($('[id*=txtReturnDate]').val() != '' && $('[id*=txtReturnDate]').val() != 'DD/MM/YYYY' && $('[id*=txtReturnDate]').val() != undefined) {
            if (Date.parse($('[id*=txtReturnDate]').val()) < Date.parse($('[id*=txtDepartureDate]').val())) {
                alert('Return Date should be greater than the departure Date');
                event.preventDefault();
            }
        }

        if ($('[id*=ddlAdult]').val() == '0' && $('[id*=ddlChild]').val() == '0' && $('[id*=ddlYouth]').val() == '0' && $('[id*=ddlSenior]').val() == '0') {
            alert('Please enter at least 1 adult, senior or junior(youth) passenger.');
            event.preventDefault();
        }
        else if ($('[id*=ddlAdult]').val() == '0' && $('[id*=ddlChild]').val() != '0' && $('[id*=ddlYouth]').val() == '0' && $('[id*=ddlSenior]').val() == '0') {
            $('[id*=pnlPassenger]').show();
            event.preventDefault();
        }

        var totalAdult = parseInt($('[id*=ddlAdult]').val()) * 4;
        var totalYouth = parseInt($('[id*=ddlYouth]').val()) * 4;
        var totalSenior = parseInt($('[id*=ddlSenior]').val()) * 4;
        var totalChilden = totalAdult + totalYouth + totalSenior;

        if (parseInt($('[id*=ddlChild]').val()) > totalChilden) {
            $('[id*=pnlPassenger]').show();
            event.preventDefault();
        }
    }

    function EvolviMaxPassenger(event) {
        if ($("#hdntxtToRailName").val() == "EVOLVI" && $("#hdntxtFromRailName").val() == "EVOLVI") {
            var count = parseInt($('[id*=ddlAdult]').val()) + parseInt($('[id*=ddlChild]').val()) + parseInt($('[id*=ddlYouth]').val()) + parseInt($('[id*=ddlSenior]').val());
            if (count > 9) {
                alert('Please select at most 9 passenger.');
                event.preventDefault();
            }
        }
    }

    function ChangeChildAgeForEvolvi() {
        if ($("#hdntxtToRailName").val() == "EVOLVI" && $("#hdntxtFromRailName").val() == "EVOLVI") {
            $('[id*=lblChild]').text('Children');
            $('[id*=lblYouth]').text('Youths');
            $('[id*=lblIRChild]').text('Children');
            $('[id*=lblIRYouth]').text('Youth');
        }
        else {
            $('[id*=lblChild]').text('Children');
            $('[id*=lblYouth]').text('Youths');
            $('[id*=lblIRChild]').text('Children');
            $('[id*=lblIRYouth]').text('Youth');
        }
    }

    function btnswapstation() {
        var spantxtFrom = '';
        var txtFrom = '';
        spantxtFrom = $("#spantxtFrom").text();
        $("#spantxtFrom").text($("#spantxtTo").text());
        $("#spantxtTo").text(spantxtFrom);
        txtFrom = $("#txtFrom").val();
        $("#txtFromtxtFrom").val($("#txtFromtxtTo").val());
        $("#txtFromtxtTo").val(txtFrom);
    }
    function Focusonerrorcontrol() {
        callvalerror();
        $(window).on('click', function () {
            var Toperror = $(".starail-Form-error").eq(0).offset();
            if (Toperror != undefined) {
                $('body,html').animate({ scrollTop: Toperror.top }, 1000);
                $(window).off('click');
            }
        });
    }
    function IrTextChanges() {
        $('html').on('click', function (e) {
            if ($(e.target).closest('.pnlTravellerPopUP').attr('id') == undefined) {
                $('#MainContent_ucLoyaltyCart_pnlTravellerPopUP').hide();
            }
        });

        $('.ddltextchange').mouseover(function () {
            $('#MainContent_ucLoyaltyCart_pnlTravellerPopUP').show();
        });
        $('.ddltextchange,#close_popup').click(function () {
            $('#MainContent_ucLoyaltyCart_pnlTravellerPopUP').hide();
        });
    }
    function removeIrClasses() {
        //$('#MainContent_ucLoyaltyCart_ddlAdult,#MainContent_ucLoyaltyCart_ddlChild,#MainContent_ucLoyaltyCart_ddlYouth,#MainContent_ucLoyaltyCart_ddlSenior').removeClass('ddltextchange');
    }

    function addNewIRClass() {
        $('[id*=pnlTravellerPopUP]').addClass('pnlTravellerPopUPNewIR');
    }

    function addIRClass() {
        $('[id*=pnlTravellerPopUP]').addClass('pnlTravellerPopUPIR');
    }

    function addSTAClass() {
        $('[id*=pnlTravellerPopUP]').addClass('pnlTravellerPopUPSTA');
    }
</script>
<script type="text/javascript">
    var count = 0;
    var countKey = 1;
    var conditionone = 0;
    var conditiontwo = 0;
    var tabkey = 0;

    $(document).ready(function () {
        callvalerror();
        LoadCal();
        DatePickerCall();
        IrTextChanges();
        ChangeChildAgeForEvolvi();
        $(".closex").click(function () {
            $(".pnlpopupbox").hide();
        });

        $("#MainContent_ucLoyaltyCart_txtReturnDate").val("DD/MM/YYYY");
        $("#txtFrom").focus();
        if ($("#txtFrom").val() != '') {
            //                alert(localStorage.getItem("spantxtTo"));
            $('#spantxtTo').text(localStorage.getItem("spantxtTo"));
        }
        if ($("#txtTo").val() != '') {
            //                alert(localStorage.getItem("spantxtFrom"));
            $('#spantxtFrom').text(localStorage.getItem("spantxtFrom"));
        }
        $(window).click(function (event) {
            $('#_bindDivData').remove();
        });

        if (window.location.href.indexOf("1tracknew") >= 0 || window.location.href.indexOf("bookmyrst.co.uk") >= 0) {
            //alert();
        }
        else {
            $(window).keydown(function (event) {
                if (event.keyCode == 13 || (event.keyCode == 9 && tabkey == 1)) {
                    tabkey = 0;
                    event.preventDefault();
                    return false;
                }
                tabkey = 1;
            });
        }
    });
    function Getkeydown(event, obj) {
        var $id = $(obj);
        var maxno = 0;
        count = event.keyCode;
        $(".popupselect").each(function () { maxno++; });
        if (count == 13 || count == 9) {
            $(".popupselect").each(function () {
                if ($(this).attr('style') != undefined) {
                    $(this).trigger('onclick');
                    $id.val($.trim($(this).find('span').text()));
                }
            });
            $('#_bindDivData').remove();
            countKey = 1;
            conditionone = 0;
            conditiontwo = 0;
        }
        else if (count == 40 && maxno > 1) {
            conditionone = 1;
            if (countKey == maxno)
                countKey = 0;
            if (conditiontwo == 1) {
                countKey++;
                conditiontwo = 0;
            }
            $(".popupselect").removeAttr('style');
            $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
            countKey++;
        }
        else if (count == 38 && maxno > 1) {
            conditiontwo = 1;
            if (countKey == 0)
                countKey = maxno;
            if (conditionone == 1) {
                countKey--;
                conditionone = 0;
            }
            countKey--;
            $(".popupselect").removeAttr('style');
            $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
        }
        else {
            countKey = 1;
            conditionone = 0;
            conditiontwo = 0;
        }
    }
    function selectpopup(e) {
        if (count != 40 && count != 38 && count != 13 && count != 37 && count != 39 && count != 9) {

            var $this = $(e);
            var data = $this.val();

            var station = '';
            var hostName = window.location.host;
            var url = "http://" + hostName;

            if (window.location.toString().indexOf("https:") >= 0)
                url = "https://" + hostName;


            if ($("#txtFrom").val() == '' && $("#txtTo").val() == '') {
                $('#spantxtFrom').text('');
                $('#spantxtTo').text('');
            }

            var filter = $("#span" + $this.attr('id') + "").text();
            if (filter == "" && $this.val() != "")
                filter = $("#hdnFilter").val();
            $("#hdnFilter").val(filter);

            if ($this.attr('id') == 'txtTo') {
                station = $("#txtFrom").val();
            }
            else {
                station = $("#txtTo").val();
            }

            if (hostName == "localhost") {
                url = url + "/1tracknew";
            }
            var hostUrl = url + "/StationList.asmx/getStationsXList";

            data = data.replace(/[']/g, "♥");
            var $div = $("<div id='_bindDivData' onmousemove='_removehoverclass(this)'/>");
            $.ajax({
                type: "POST",
                url: hostUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'prefixText':'" + data + "','filter':'" + filter + "','station':'" + station + "'}",
                success: function (msg) {
                    $('#_bindDivData').remove();
                    var lentxt = data.length;
                    $.each(msg.d, function (key, value) {
                        var splitdata = value.split('ñ');
                        var lenfull = splitdata[0].length; ;
                        var txtupper = splitdata[0].substring(0, lentxt);
                        var txtlover = splitdata[0].substring(lentxt, lenfull);
                        $this.after($div.append("<div class='popupselect' onclick='_Bindthisvalue_x(this)'><b class='_prifixText'>" + txtupper + "</b>" + txtlover + "<span style='display:none'>" + splitdata[0] + "</span><div style='display:none'>" + splitdata[1] + "</div><p style='display:none'>" + splitdata[2] + "</p></div>"));
                    });
                    $(".popupselect:eq(0)").attr('style', 'background-color: #ccc !important');
                },
                error: function () {
                    //                    alert("Wait...");
                }
            });
        }
    }
    function _removehoverclass(e) {
        $(".popupselect").hover(function () {
            $(".popupselect").removeAttr('style');
            $(this).attr('style', 'background-color: #ccc !important');
            countKey = $(this).index();
        });
    }
    function _Bindthisvalue_x(e) {
        var idtxtbox = $('#_bindDivData').prev("input").attr('id');
        $("#" + idtxtbox + "").val($(e).find('span').text());
        if (idtxtbox == 'txtTo') {
            $('#spantxtFrom').text($(e).find('div').text());
            localStorage.setItem("spantxtFrom", $(e).find('div').text());
            $("#hdntxtToRailName").val($(e).find('p').text());
        }
        else {
            $('#spantxtTo').text($(e).find('div').text());
            localStorage.setItem("spantxtTo", $(e).find('div').text());
            $("#hdntxtFromRailName").val($(e).find('p').text());
        }
        $('#_bindDivData').remove();
        ChangeChildAgeForEvolvi();
    }

    var unavailableDates = '<%=unavailableDates1 %>';
    function nationalDays(date) {
        dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
        if ($.inArray(dmy, unavailableDates) > -1) {
            return [false, "", "Unavailable"];
        }
        return [true, ""];
    }

    function LoadCal() {
        $(".loycheckboxSpan").remove();
        $(".loycheckbox").find('label').before("<span class='loycheckboxSpan'><i class='starail-Icon-tick'></i></span>");
        $(".loyradioSpan").remove();
        $(".loyspanradio").find('label').before("<span class='loyradioSpan' style='float:left;'><i class='radioSpancenter'></i></span>");

        $("#MainContent_ucLoyaltyCart_txtDepartureDate, #MainContent_ucLoyaltyCart_txtReturnDate").bind("contextmenu cut copy paste", function (e) {
            return false;
        });
        $("#MainContent_ucLoyaltyCart_txtDepartureDate, #MainContent_ucLoyaltyCart_txtReturnDate").keypress(function (event) { event.preventDefault(); });
    }

    function calenable() {
        callvalerror();
        LoadCal();
        DatePickerCall();
    }
    function caldisable() {
        callvalerror();
        LoadCal();
        DatePickerCall();
    }
    function DatePickerCall() {
        $("#MainContent_ucLoyaltyCart_txtDepartureDate").datepicker(
        {
            numberOfMonths: 1,
            dateFormat: 'dd/M/yy',
            beforeShowDay: nationalDays,
            showButtonPanel: true,
            firstDay: 1,
            minDate: '<%=minDate %>',
            onClose: function (selectedDate) {
                $("#MainContent_ucLoyaltyCart_txtReturnDate").datepicker("option", "minDate", selectedDate);
            }
        });

        $("#MainContent_ucLoyaltyCart_txtReturnDate").datepicker({
            numberOfMonths: 1,
            dateFormat: 'dd/M/yy',
            beforeShowDay: nationalDays,
            showButtonPanel: true,
            firstDay: 1,
            minDate: $("#MainContent_ucLoyaltyCart_txtDepartureDate").val()
        });
        $("[id*=UcQuotationForm1_txtDepartureDate],[id*=UcQuotationForm2_txtDepartureDate]").datepicker(
        {
            numberOfMonths: 1,
            dateFormat: 'dd/M/yy',
            beforeShowDay: nationalDays,
            showButtonPanel: true,
            firstDay: 1,
            minDate: '<%=minDate %>'
        });
    }
</script>
<style type="text/css">
    #MainContent_ucLoyaltyCart_rdBookingType tbody tr td
    {
        width: 50%;
    }
    .btnclose_popup
    {
        position: absolute;
        top: -11.2px;
        right: -11.2px;
        width: 32px;
        height: 32px;
        border-radius: 16px;
        box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.15);
        display: block;
        color: #FFF;
        text-decoration: none;
        text-align: center;
        transition: background-color 0.3s ease-in-out;
    }
    .travellpop
    {
        background-color: #fff !important;
        padding: 1rem;
    }
    .travellpop p
    {
        margin: 0;
        padding: 0;
    }
</style>
