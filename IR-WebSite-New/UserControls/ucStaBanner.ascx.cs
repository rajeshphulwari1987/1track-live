﻿#region Using
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using Business;
using OneHubServiceRef;
using System.Web.UI.HtmlControls;
using System.Web.Script.Serialization;
#endregion

public partial class UserControls_ucStaBanner : System.Web.UI.UserControl
{
    private readonly Masters _master = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    Guid pageID, siteId;
    public string frontSiteUrl, ContinentName;
    private readonly ManageInterRailNew _ManageIRNew = new ManageInterRailNew();
    public string siteURLS;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (Session["siteId"] != null)
                    siteId = Guid.Parse(Session["siteId"].ToString());
                else
                    return;

                int SiteType = _oWebsitePage.GetSiteLayoutType(siteId);
                if (SiteType != 1 && SiteType != 4 && SiteType != 5 && SiteType != 7 && SiteType != 8)
                    return;

                if (Page.RouteData.Values["PageId"] != null)
                    pageID = (Guid)Page.RouteData.Values["PageId"];
                else
                {
                    var pid = _db.tblWebMenus.FirstOrDefault(x => x.Name.ToLower() == "home");
                    if (pid != null)
                        pageID = pid.ID;
                }
                PageContent(pageID, siteId);
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void PageContent(Guid pageID, Guid siteID)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageID && x.SiteID == siteID);
            if (result != null)
            {
                var url = result.Url;
                var oPage = _master.GetPageDetailsByUrl(url);
                string[] arrListId = oPage.BannerIDs.Split(',');
                var idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _master.GetBannerImgByID(idList);
                int countdata = list.Count();
                int index = new Random().Next(countdata);
                var newlist = list.Skip(index).FirstOrDefault();
                if (newlist != null && newlist.ImgUrl != null)
                    img_Banner.Src = SiteUrl + "" + newlist.ImgUrl;
                else
                    img_Banner.Visible = false;

                SearchText.InnerHtml = string.IsNullOrEmpty(result.HomeSearchText) ? "<h2>Where do you want to go ?</h2>" : result.HomeSearchText;
                CountrySearchText.InnerHtml = string.IsNullOrEmpty(result.HomeCountryText) ? "SEE ALL THE COUNTRIES YOU COULD EXPLORE" : result.HomeCountryText;
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}