﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucStaBanner.ascx.cs" Inherits="UserControls_ucStaBanner" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="updUcStaBanner" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" />
            </div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" CssClass="starail-Form-required" />
            </div>
        </asp:Panel>
        <div class="starail-Grid--mobileFull">
            <div class="starail-Grid-col starail-Grid-col--nopadding">
                <div class="starail-Section starail-HomeHero starail-Section--nopadding" style="overflow: visible;">
                    <img class="starail-HomeHero-img dots-header" alt="." id="img_Banner" runat="server" />
                    <div class="starail-HomeHero-search">
                        <div class="starail-HomeHero-title" style="font-size: 28px;" id="SearchText" runat="server">
                            <h2>Where do you want to go?</h2>
                        </div>
                        <div class="starail-HomeHero-form" id="search">
                            <asp:TextBox ID="txtSearch" runat="server" autocomplete="off" CssClass="starail-Form-input starail-HomeHero-formInput nw-pd"
                                placeholder="Enter a continent, country or pass name" />
                            <asp:RequiredFieldValidator ID="reqvalerrortxtSearch" runat="server" ControlToValidate="txtSearch" ValidationGroup="search" />
                            <asp:Button ID="btnSearch" runat="server" CssClass="starail-Button starail-HomeHero-formSubmit IR-GaCode" ValidationGroup="search"
                                ClientIDMode="Static" Text="Go"></asp:Button>
                        </div>
                        <p class="starail-HomeHero-allLink">
                            <a href="countries"><span class="starail-u-hideMobile" id="CountrySearchText"
                                runat="server">See all the Countries you could explore</span></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<script type="text/javascript">
    function initcallSearch(isreload) {
        $("#MainContent_StaBanner_txtSearch").CustomSearch({
            border: "1px solid #C0C0C0",
            font_size: "14px",
            ul_background: "rgb(237, 239, 241)",
            getTextFromTitle: false,
            modeStyle: 'manual',
            manual_B_color: '#0c6ab8',
            manual_B_Size: '13px',
            animationDuration: 500,
            recordno: 10,
            NoRecord: "Record Not Found",
            Data_list: countryList,
            reload: isreload
        });
        $("#MainContent_StaBanner_txtSearch").on('keyup', function (event) {
            $("#MainContent_StaBanner_txtSearch").next('div').find("ul").find('li').click(function () {
                $("#txtSearch").val($("#MainContent_StaBanner_txtSearch").val());
                $("#hdnSearchText").val($("#MainContent_StaBanner_txtSearch").val());
                $("#MainContent_btnSearch").trigger('click');
            });
            if (event.keyCode == 13 && $(this).val().length > 0) {
                $("#txtSearch").val($("#MainContent_StaBanner_txtSearch").val());
                $("#hdnSearchText").val($("#MainContent_StaBanner_txtSearch").val());
                $("#MainContent_btnSearch").trigger('click');
            }
        });
    }
</script>
