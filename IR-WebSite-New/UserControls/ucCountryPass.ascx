﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucCountryPass.ascx.cs"
    Inherits="UserControls_ucCountryPass" %>
<asp:UpdatePanel ID="upCULVL" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" name="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="starail-BookingDetails-form starail-BookingDetails--four-country">
            <div class="starail-Form-row">
                <label for="starail-class" class="starail-Form-label">
                    What class do you want to travel?</label>
                <div class="starail-Form-inputContainer">
                    <div class="starail-Form-inputContainer-col">
                        <asp:DropDownList class="starail-Form-select customval" ID="ddlClass" runat="server"
                            AutoPostBack="True" OnSelectedIndexChanged="ddlClass_SelectedIndexChanged" />
                    </div>
                </div>
            </div>
            <div class="starail-Form-row">
                <label for="starail-travel" class="starail-Form-label">
                    How many days do you want to travel?</label>
                <div class="starail-Form-inputContainer">
                    <div class="starail-Form-inputContainer-col">
                        <asp:DropDownList class="starail-Form-select customval travell-font-size" ID="ddlValidityDays" runat="server"
                            AutoPostBack="True" OnSelectedIndexChanged="ddlValidityDays_SelectedIndexChanged" />
                    </div>
                </div>
            </div>
            <div class="starail-Form-row starail-u-cf starail-BookingDetails-quantity" id="divtravellers"
                runat="server" visible="false">
                <label for="" class="starail-Form-label">
                    Who's going?</label>
                <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                    <asp:Repeater runat="server" ID="rpttravellers" OnItemDataBound="rpttravellers_ItemDataBound">
                        <ItemTemplate>
                            <div class="starail-Form-inputContainer-col">
                                <asp:DropDownList ID="ddlSelectno" runat="server" class="starail-Form-select customvaltravellers starail-Form-select--narrow travell-margin-right">
                                    <asp:ListItem>0</asp:ListItem>
                                    <asp:ListItem>1</asp:ListItem>
                                    <asp:ListItem>2</asp:ListItem>
                                    <asp:ListItem>3</asp:ListItem>
                                    <asp:ListItem>4</asp:ListItem>
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>6</asp:ListItem>
                                    <asp:ListItem>7</asp:ListItem>
                                    <asp:ListItem>8</asp:ListItem>
                                    <asp:ListItem>9</asp:ListItem>
                                    <asp:ListItem>10</asp:ListItem>
                                </asp:DropDownList>
                                <asp:HiddenField runat="server" Value='<%#Eval("ID") %>' ID="hdntravellerId" />
                                <asp:HiddenField runat="server" Value='<%#Eval("Name") %>' ID="hdntravellerName" />
                                <%#Eval("Name") + "("+ Eval("Age") +")"%>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <!-- starail-Form-selectGroup -->
            </div>
            <div class="starail-Form-row">
                <label for="starail-country" class="starail-Form-label">
                    Choose your first country</label>
                <div class="starail-Form-inputContainer">
                    <div class="starail-Form-inputContainer-col">
                        <asp:DropDownList class="starail-Form-select customval" ID="ddlCunt1" runat="server"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlCunt1_SelectedIndexChanged" />
                    </div>
                </div>
            </div>
            <div id="divcunt2" runat="server" class="starail-Form-row starail-Form-row--disabled">
                <label for="starail-country" class="starail-Form-label">
                    Choose your second country</label>
                <div class="starail-Form-inputContainer">
                    <div class="starail-Form-inputContainer-col">
                        <asp:DropDownList class="starail-Form-select customval" ID="ddlCunt2" runat="server"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlCunt2_SelectedIndexChanged" />
                    </div>
                </div>
            </div>
            <div id="divcunt3" runat="server" class="starail-Form-row starail-Form-row--disabled">
                <label for="starail-country" class="starail-Form-label">
                    Choose your third country</label>
                <div class="starail-Form-inputContainer">
                    <div class="starail-Form-inputContainer-col">
                        <asp:DropDownList class="starail-Form-select customval" ID="ddlCunt3" runat="server"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlCunt3_SelectedIndexChanged" />
                    </div>
                </div>
            </div>
            <div id="divcunt4" runat="server" class="starail-Form-row starail-Form-row--disabled">
                <label for="starail-country" class="starail-Form-label">
                    Choose your fourth country</label>
                <div class="starail-Form-inputContainer">
                    <div class="starail-Form-inputContainer-col">
                        <asp:DropDownList class="starail-Form-select customval" ID="ddlCunt4" runat="server"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlCunt4_SelectedIndexChanged" />
                    </div>
                </div>
            </div>
            <div id="divcunt5" runat="server" class="starail-Form-row starail-Form-row--disabled">
                <label for="starail-country" class="starail-Form-label">
                    Choose your fifth country</label>
                <div class="starail-Form-inputContainer">
                    <div class="starail-Form-inputContainer-col">
                        <asp:DropDownList class="starail-Form-select customval" ID="ddlCunt5" runat="server" />
                    </div>
                </div>
            </div>
            <div class="starail-Form-row">
                <div class="starail-Form-spacer starail-u-hideMobile">
                    &nbsp;</div>
                <div class="starail-Form-inputContainer">
                    <div class="starail-Form-inputContainer-col">
                        <asp:Button ID="lnlAdd" Text="Show Me Prices" class="starail-Button starail-Button--full starail-Button--blue"
                            runat="server" OnClientClick="return SelectCal()" OnClick="lnlAdd_Click" />
                    </div>
                </div>
            </div>
        </div>
        <div id="divGrd" runat="server" style="display: none;" class="starail-YourBooking starail-YourBooking--passPrices">
            <h2 class="starail-u-hideMobile">
                Your Booking</h2>
            <div class="starail-YourBooking-table">
                <asp:GridView ID="grdTravellerinfo" runat="server" GridLines="None" CssClass="countrylvlgrd"
                    AutoGenerateColumns="False" OnRowCommand="grdTravellerinfo_RowCommand">
                    <HeaderStyle CssClass="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--header starail-u-hideMobile" />
                    <RowStyle CssClass="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--four-pass" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <%#Eval("TravellerName")%>:
                                <%#Eval("ClassName")%>
                            </ItemTemplate>
                            <ItemStyle CssClass="starail-YourBooking-mobileHeader starail-u-hideDesktop" />
                            <HeaderStyle CssClass="displaynone" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pass">
                            <ItemTemplate>
                                <p>
                                    <strong>
                                        <%#Eval("TravellerName")%>,</strong>
                                    <%#Eval("ClassName")%></p>
                            </ItemTemplate>
                            <ItemStyle CssClass="starail-u-hideMobile" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Countries">
                            <ItemTemplate>
                                <p class="starail-u-hideDesktop">
                                    Countries</p>
                                <p>
                                    <%#Eval("CountryName")%></p>
                            </ItemTemplate>
                            <ItemStyle CssClass="starail-YourBooking-col  starail-YourBooking-col--mobileFullWidth" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Validity">
                            <ItemTemplate>
                                <p class="starail-u-hideDesktop">
                                    Validity:</p>
                                <p>
                                    <%#Eval("ValidityName")%></p>
                            </ItemTemplate>
                            <ItemStyle CssClass="starail-YourBooking-col  starail-YourBooking-col--mobileFullWidth" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Price">
                            <ItemTemplate>
                                <p>
                                    <span class="starail-YourBooking-col-price-title" style="margin-right: 0px;">
                                        <%=currency %><%#Eval("Price")%></span> x
                                    <%#Eval("Qty")%></p>
                            </ItemTemplate>
                            <ItemStyle CssClass="starail-YourBooking-col-price-wInput starail-YourBooking-col" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                    CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/DeleteRed.png"
                                    OnClientClick="return confirm('Are you sure you want to delete this item?')"><i class="starail-Icon-delete"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                            <ItemStyle CssClass="starail-YourBooking-col-delete starail-YourBooking-col deletepass">
                            </ItemStyle>
                            <HeaderStyle CssClass="starail-YourBooking-col-delete" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <p class="starail-YourBooking-totalPrice" style="overflow: hidden; text-align: center;">
                Total Price for
                <asp:Label runat="server" ID="lbltxttraveller"></asp:Label>
                : <span class="starail-YourBooking-totalPrice-amount">
                    <%=currency %><asp:Label ID="lblPrice" runat="server" Text="0.00" /></span>
            </p>
            <div class="starail-Section starail-Section--nopadding starail-Section-nextButtonSection textcenter">
                <asp:Button CssClass="starail-Button IR-GaCode" ID="btnBookNow" runat="server" Text="NEXT: Booking details"
                    OnClick="btnBookNow_Click" />
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnBookNow" />
    </Triggers>
</asp:UpdatePanel>
<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upCULVL"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="modalBackground progessposition">
        </div>
        <div class="progess-inner2">
            UPDATING RESULTS...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<script type="text/javascript">
    function SelectCal() {
        var result = true;
        var travellerselect = true;
        $('.customval').each(function () {
            var data = $(this).val();
            if (data == '0' && $(this).attr('disabled') == undefined) {
                $(this).addClass("starail-Form-error");
                result = false;
            }
            else {
                $(this).removeClass("starail-Form-error");
            }
        });
        $('.customvaltravellers').each(function () {
            var data = parseInt($(this).val());
            if (data > 0 && result) {
                travellerselect = false;
                var travellername = $(this).parent().find("[id*=hdntravellerName]").val();
                if (travellername.match(/saver/i)) {
                    if (data < 2 || data > 5) {
                        $(this).val('0');
                        result = false;
                        alert('You can select minimum 2 or maximum 5 number of people for ' + travellername + ' ticket on same travelling class.');
                    }
                }
            }
        });
        if (travellerselect && result) {
            showmsg();
            result = false;
        }
        return result;
    }
    function showmeprice() {
        var stop = $("#MainContent_ucCountryLevel_divGrd").offset().top;
        var delay = 1000;
        $('body,html').animate({ scrollTop: stop }, delay);
    }
</script>
