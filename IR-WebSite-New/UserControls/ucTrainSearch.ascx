﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucTrainSearch.ascx.cs"
    Inherits="OtherSiteP2PBooking_ucTrainSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="updSearchUC" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnFilter" runat="server" ClientIDMode="Static" />
        <div id="divSearch" runat="server" class="bottompadding starail-Tabs-content">
            <div class="starail-SearchTickets starail-SearchTickets--mini">
                <div class="starail-Box starail-Box--whiteMobile">
                    <div class="starail-Sidebar-title">
                        <h4>
                            Find Alternative Tickets</h4>
                    </div>
                    <div class="starail-Form starail-Form--onBoxNarrow starail-SearchTickets-form">
                        <div class="starail-Form-row starail-SearchTickets-destination hideoverflow">
                            <div class="starail-SearchTickets-destination-row starail-SearchTickets-destination-row--start hideoverflow">
                                <label for="starail-startlocation" class="starail-Form-label starail-u-visuallyHiddenMobile">
                                    From<span class="val-error">*</span>
                                </label>
                                <div class="starail-SearchTickets-destinationInput hideoverflow setposition">
                                    <asp:RequiredFieldValidator ID="reqvalerror1" runat="server" ValidationGroup="vgs"
                                        Display="Dynamic" ControlToValidate="txtFrom" />
                                    <asp:TextBox ID="txtFrom" runat="server" ClientIDMode="Static" onkeyup="selectpopup(this)"
                                        onkeydown="Getkeydown(event,this)" class="starail-Form-input" placeholder="Enter a start location"
                                        autocomplete="off" />
                                    <span id="spantxtFrom" style="display: none"></span>
                                    <asp:HiddenField ID="hdnFrm" runat="server" />
                                </div>
                            </div>
                            <div class="starail-DestinationIcon">
                                <div class="starail-DestinationIcon-line">
                                </div>
                                <div class="starail-DestinationIcon-circle starail-DestinationIcon-circle--filled">
                                </div>
                                <div class="starail-DestinationIcon-circle">
                                </div>
                            </div>
                            <div class="starail-SearchTickets-destination-row starail-SearchTickets-destination-row--end hideoverflow">
                                <label for="starail-endlocation" class="starail-Form-label starail-u-visuallyHiddenMobile">
                                    To<span class="val-error">*</span>
                                </label>
                                <div class="starail-SearchTickets-destinationInput hideoverflow setposition">
                                    <asp:RequiredFieldValidator ID="reqvalerror2" runat="server" ValidationGroup="vgs"
                                        Display="Dynamic" ControlToValidate="txtTo" />
                                    <asp:TextBox ID="txtTo" runat="server" onkeyup="selectpopup(this)" onkeydown="Getkeydown(event,this)"
                                        ClientIDMode="Static" class="starail-Form-input" placeholder="Enter a destination"
                                        autocomplete="off" />
                                    <span id="spantxtTo" style="display: none"></span>
                                </div>
                            </div>
                            <a href="#" class="starail-SearchTickets-switch-trigger"><i class="starail-Icon starail-Icon-reverse">
                            </i><span class="starail-u-visuallyHidden">Switch direction</span></a>
                        </div>
                        <div class="starail-Form-row">
                            <asp:RadioButtonList ID="rdBookingType" runat="server" RepeatDirection="Horizontal"
                                ClientIDMode="Static" Style="display: none;">
                                <asp:ListItem Value="0" Selected="True">One-way</asp:ListItem>
                                <asp:ListItem Value="1">Return</asp:ListItem>
                            </asp:RadioButtonList>
                            <label for="" class="starail-Form-label starail-u-invisible starail-u-visuallyHiddenMobile journeylbl">
                                Journey Type</label>
                            <div class="starail-u-cf starail-Form-switchRadioGroup">
                                <table id="BookingType" class="switchRadioGroup" style="width: 100%;">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input id="rdBookingType0" type="radio" name="rdBookingType" value="0" checked="checked">
                                                <label for="rdBookingType0" onclick="clickaa('0')">
                                                    One-way</label>
                                            </td>
                                            <td>
                                                <input id="rdBookingType1" type="radio" name="rdBookingType" value="1">
                                                <label for="rdBookingType1" onclick="clickaa('1')">
                                                    Return</label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="starail-Form-row starail-u-cf">
                            <label for="leaving" class="starail-Form-label">
                                Depart<span class="val-error">*</span>
                            </label>
                            <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                                <div class="starail-Form-datePicker">
                                    <asp:RequiredFieldValidator ID="reqvalerror3" runat="server" ValidationGroup="vgs"
                                        Display="Dynamic" ControlToValidate="txtDepartureDate" />
                                    <asp:TextBox ID="txtDepartureDate" ClientIDMode="Static" placeholder="DD/MM/YYYY"
                                        runat="server" autocomplete="off" class="starail-Form-input" />
                                </div>
                                <asp:DropDownList ID="ddldepTime" runat="server" class="starail-Form-select starail-Form-inputContainer--time starail-Form-inputContainer--last">
                                    <asp:ListItem>00:00</asp:ListItem>
                                    <asp:ListItem>01:00</asp:ListItem>
                                    <asp:ListItem>02:00</asp:ListItem>
                                    <asp:ListItem>03:00</asp:ListItem>
                                    <asp:ListItem>04:00</asp:ListItem>
                                    <asp:ListItem>05:00</asp:ListItem>
                                    <asp:ListItem>06:00</asp:ListItem>
                                    <asp:ListItem>07:00</asp:ListItem>
                                    <asp:ListItem>08:00</asp:ListItem>
                                    <asp:ListItem Selected="True">09:00</asp:ListItem>
                                    <asp:ListItem>10:00</asp:ListItem>
                                    <asp:ListItem>11:00</asp:ListItem>
                                    <asp:ListItem>12:00</asp:ListItem>
                                    <asp:ListItem>13:00</asp:ListItem>
                                    <asp:ListItem>14:00</asp:ListItem>
                                    <asp:ListItem>15:00</asp:ListItem>
                                    <asp:ListItem>16:00</asp:ListItem>
                                    <asp:ListItem>17:00</asp:ListItem>
                                    <asp:ListItem>18:00</asp:ListItem>
                                    <asp:ListItem>19:00</asp:ListItem>
                                    <asp:ListItem>20:00</asp:ListItem>
                                    <asp:ListItem>21:00</asp:ListItem>
                                    <asp:ListItem>22:00</asp:ListItem>
                                    <asp:ListItem>23:00</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="starail-Form-row starail-u-cf">
                            <label for="returning" class="starail-Form-label">
                                Return<asp:Label runat="server" ID="lblrtnerror" CssClass="val-error" Text="*" Style="display: none;"
                                    ClientIDMode="Static"></asp:Label>
                            </label>
                            <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                                <div class="starail-Form-datePicker journeyreturntxt">
                                    <asp:RequiredFieldValidator ID="reqvalerror4" runat="server" ValidationGroup="vgs"
                                        Display="Dynamic" ControlToValidate="txtReturnDate" disabled="disabled" />
                                    <asp:TextBox ID="txtReturnDate" runat="server" placeholder="DD/MM/YYYY" class="starail-Form-input"
                                        Width="100%" autocomplete="off" ClientIDMode="Static" Enabled="false" />
                                </div>
                                <asp:DropDownList ID="ddlReturnTime" runat="server" disabled="disabled" CssClass="jurnyreturnselect"
                                    ClientIDMode="Static">
                                    <asp:ListItem>00:00</asp:ListItem>
                                    <asp:ListItem>01:00</asp:ListItem>
                                    <asp:ListItem>02:00</asp:ListItem>
                                    <asp:ListItem>03:00</asp:ListItem>
                                    <asp:ListItem>04:00</asp:ListItem>
                                    <asp:ListItem>05:00</asp:ListItem>
                                    <asp:ListItem>06:00</asp:ListItem>
                                    <asp:ListItem>07:00</asp:ListItem>
                                    <asp:ListItem>08:00</asp:ListItem>
                                    <asp:ListItem Selected="True">09:00</asp:ListItem>
                                    <asp:ListItem>10:00</asp:ListItem>
                                    <asp:ListItem>11:00</asp:ListItem>
                                    <asp:ListItem>12:00</asp:ListItem>
                                    <asp:ListItem>13:00</asp:ListItem>
                                    <asp:ListItem>14:00</asp:ListItem>
                                    <asp:ListItem>15:00</asp:ListItem>
                                    <asp:ListItem>16:00</asp:ListItem>
                                    <asp:ListItem>17:00</asp:ListItem>
                                    <asp:ListItem>18:00</asp:ListItem>
                                    <asp:ListItem>19:00</asp:ListItem>
                                    <asp:ListItem>20:00</asp:ListItem>
                                    <asp:ListItem>21:00</asp:ListItem>
                                    <asp:ListItem>22:00</asp:ListItem>
                                    <asp:ListItem>23:00</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="starail-Form-row starail-u-cf starail-SearchTickets-quantity" style="margin: 0px;">
                            <label for="" class="starail-Form-label" style="padding-top: 0px;">
                                Passengers</label>
                            <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                                <div class="starail-Form-inputContainer-col">
                                    <asp:DropDownList CssClass="starail-Form-select starail-Form-select--narrow" ID="ddlAdult"
                                        runat="server">
                                    </asp:DropDownList>
                                    <label for="starail-adult">
                                        Adults</label>
                                </div>
                                <div class="starail-Form-inputContainer-col">
                                    <asp:DropDownList CssClass="starail-Form-select starail-Form-select--narrow" ID="ddlChild"
                                        runat="server">
                                    </asp:DropDownList>
                                    <label for="starail-children">
                                        Children</label>
                                </div>
                                <div class="starail-Form-inputContainer-col">
                                    <asp:DropDownList CssClass="starail-Form-select starail-Form-select--narrow" ID="ddlYouth"
                                        runat="server">
                                    </asp:DropDownList>
                                    <label for="starail-adult">
                                        Youths</label>
                                </div>
                                <div class="starail-Form-inputContainer-col">
                                    <asp:DropDownList CssClass="starail-Form-select starail-Form-select--narrow" ID="ddlSenior"
                                        runat="server">
                                    </asp:DropDownList>
                                    <label for="starail-adult">
                                        Seniors</label>
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row starail-u-cf">
                            <label for="leaving" class="starail-Form-label">
                                Class
                            </label>
                            <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                                <asp:RadioButtonList ID="ddlClass" CssClass="classradiotbl" runat="server" RepeatDirection="Horizontal"
                                    Width="100%">
                                    <asp:ListItem class="starail-Form-fancyRadioGroup spanradio" Selected="True" Value="0">All</asp:ListItem>
                                    <asp:ListItem class="starail-Form-fancyRadioGroup spanradio" Value="1">1st</asp:ListItem>
                                    <asp:ListItem class="starail-Form-fancyRadioGroup spanradio" Value="2">2nd</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div class="starail-Form-row starail-u-cf">
                            <label for="leaving" class="starail-Form-label">
                                Transfers
                            </label>
                            <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                                <asp:DropDownList ID="ddlTransfer" runat="server" class="starail-Form-select starail-Form-inputContainer--time starail-Form-inputContainer--last"
                                    Width="61%">
                                    <asp:ListItem Value="0">Direct Trains only</asp:ListItem>
                                    <asp:ListItem Value="1">Max. 1 transfer</asp:ListItem>
                                    <asp:ListItem Value="2">Max. 2 transfers</asp:ListItem>
                                    <asp:ListItem Value="3" Selected="True">Show all</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <%-- Hide rail card option it will be used in future--%>
                        <div class="starail-Form-row starail-u-cf" id="div_chkAddRailCard" runat="server"
                            style="display: none;">
                            <label for="returning" class="starail-Form-label">
                            </label>
                            <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                                <label class="starail-BookingDetails-form-fancyCheckbox">
                                    +
                                    <asp:CheckBox ID="chkAddRailCard" CssClass="starail-sendtoaddress" runat="server"
                                        Style="display: none;" />
                                    <a class="addcardtext" style="text-decoration: underline; color: Black !important;">
                                        Add rail card</a>
                                </label>
                            </div>
                        </div>
                        <%-- End rail card option --%>
                        <div class="starail-Form-row">
                            <asp:Button ID="btnSubmit" runat="server" class="starail-Button starail-Form-button starail-Button--blue IR-GaCode"
                                OnClientClick="checkPassenger(event);EvolviMaxPassenger(event);" ValidationGroup="vgs"
                                Text="Search Tickets" OnClick="btnSubmit_Click" Style="margin-bottom: 10px" />
                            <asp:ValidationSummary ID="vsm" ValidationGroup="vgs" ShowMessageBox="false" ShowSummary="false"
                                runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <asp:Panel ID="pnlPassenger" runat="server" CssClass="modal-popup pnlpopupbox" Style="display: none">
            <div class="modalBackground modalBackgroundnew">
            </div>
            <div class="popup-inner pnlpopup" style="z-index: 111; position: fixed; left: 23%;
                top: 25%;">
                <div class="title">
                    Please select passenger type
                </div>
                <p>
                    Please enter at least 1 adult, senior or junior(youth) passenger.</p>
                <p>
                    Children cannot travel unattended.</p>
                <p>
                    There is a limitation for accompanied children:</p>
                <p>
                    - Max. 4 children per passenger allowed.</p>
                <div>
                    <button type="button" class="starail-Button btn-black newbutton closex" style="width: 20%!important;
                        float: right">
                        Cancel</button>
                </div>
            </div>
        </asp:Panel>
        <%-- Model Popup--%>
        <asp:Panel ID="pnlRailCardQuckLoad" runat="server" CssClass="pnlpopupbox pnl-pop"
            Style="display: none;">
            <div class="modalBackground modalBackgroundnew">
            </div>
            <div class="starail-popup pnlpopup">
                <div class="starail-YourBooking">
                    <h2>
                        Passenger information / Select Railcard</h2>
                    <div class="starail-YourBooking-table">
                    </div>
                    <a id="btnRailCardContinue" class="starail-Button starail-Form-button submitbuttons">
                        Save</a>
                    <div class="cancelBtn">
                        <a id="btnPopUpCloseWin" class="cancelbutton">Cancel</a>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <%-- Model Popup Close--%>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
<input id="hdntxtFromRailName" type="hidden" runat="server" clientidmode="Static" />
<input id="hdntxtToRailName" type="hidden" runat="server" clientidmode="Static" />
<asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="updSearchUC"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="modalBackground progessposition">
        </div>
        <div class="progess-inner2">
            UPDATING RESULTS...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<script type="text/jscript">
    $(document).ready(function () {
        fillRailCardDynamic();
        bindRailcard('load');
        $("#MainContent_ucTrainSearch_chkIsLoyaltyActive").change(function () {
            if ($("#MainContent_ucTrainSearch_chkIsLoyaltyActive").prop("checked")) {
                $("#MainContent_ucTrainSearch_chkIhaveRailPass").prop("checked", false);
                $("#FtpCardsDescriptionContent").slideDown();
            }
            else {
                $("#FtpCardsDescriptionContent").slideUp();
            }
        });

        $("#MainContent_ucTrainSearch_chkIhaveRailPass").change(function () {
            if ($("#MainContent_ucTrainSearch_chkIhaveRailPass").prop("checked")) {
                $("#FtpCardsDescriptionContent").slideUp();
                $("#MainContent_ucTrainSearch_chkIsLoyaltyActive").prop("checked", false);
            }
        });

        $(".closex").click(function () {
            $(".pnlpopupbox").hide();
        });

        var widthx = $(window).width();
        var heightx = $(window).height();
        var topx = (heightx - (heightx / 2)) - 250;
        var leftx = (widthx - (widthx / 2)) - 250;
        $(".pnlpopup").css({ "left": leftx + "px", "top": topx + "px" })

    });
    function ShowWarrningPopup() {
        $(document).ready(function () {
            var txtfrom = $("#MainContent_ucTrainSearch_ddldepTime option:selected").text();
            var txtTo = $("#MainContent_ucTrainSearch_ddlReturnTime option:selected").text();
            var datefrm = $("txtDepartureDate").val();
            var datearray = datefrm.split('/');
            var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
            var FromDate = new Date(newdate);
            $("#MainContent_ucTrainSearch_lblFrom").text("Departure " + txtfrom);
            $("#MainContent_ucTrainSearch_lblTo").text("Arrival " + txtTo);
            $("#MainContent_ucTrainSearch_lblDateTime").text(FromDate.customFormat("#DDDD# #MMMM# #DD#, #YYYY#"));
        });
    }

    Date.prototype.customFormat = function (formatString) {
        var YYYY, YY, MMMM, MMM, MM, M, DDDD, DDD, DD, D, hhh, hh, h, mm, m, ss, s, ampm, AMPM, dMod, th;
        var dateObject = this;
        YY = ((YYYY = dateObject.getFullYear()) + "").slice(-2);
        MM = (M = dateObject.getMonth() + 1) < 10 ? ('0' + M) : M;
        MMM = (MMMM = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][M - 1]).substring(0, 3);
        DD = (D = dateObject.getDate()) < 10 ? ('0' + D) : D;
        DDD = (DDDD = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][dateObject.getDay()]).substring(0, 3);
        th = (D >= 10 && D <= 20) ? 'th' : ((dMod = D % 10) == 1) ? 'st' : (dMod == 2) ? 'nd' : (dMod == 3) ? 'rd' : 'th';
        formatString = formatString.replace("#YYYY#", YYYY).replace("#YY#", YY).replace("#MMMM#", MMMM).replace("#MMM#", MMM).replace("#MM#", MM).replace("#M#", M).replace("#DDDD#", DDDD).replace("#DDD#", DDD).replace("#DD#", DD).replace("#D#", D).replace("#th#", th);

        h = (hhh = dateObject.getHours());
        if (h == 0) h = 24;
        if (h > 12) h -= 12;
        hh = h < 10 ? ('0' + h) : h;
        AMPM = (ampm = hhh < 12 ? 'am' : 'pm').toUpperCase();
        mm = (m = dateObject.getMinutes()) < 10 ? ('0' + m) : m;
        ss = (s = dateObject.getSeconds()) < 10 ? ('0' + s) : s;
        return formatString.replace("#hhh#", hhh).replace("#hh#", hh).replace("#h#", h).replace("#mm#", mm).replace("#m#", m).replace("#ss#", ss).replace("#s#", s).replace("#ampm#", ampm).replace("#AMPM#", AMPM);
    }

    function checkPassenger(event) {
        if ($('[id*=txtReturnDate]').val() != '' && $('[id*=txtReturnDate]').val() != 'DD/MM/YYYY' && $('[id*=txtReturnDate]').val() != undefined) {
            if (Date.parse($('[id*=txtReturnDate]').val()) < Date.parse($('[id*=txtDepartureDate]').val())) {
                alert('Return Date should be greater than the departure Date');
                event.preventDefault();
            }
        }

        if ($('[id*=ddlAdult]').val() == '0' && $('[id*=ddlChild]').val() == '0' && $('[id*=ddlYouth]').val() == '0' && $('[id*=ddlSenior]').val() == '0') {
            alert('Please enter at least 1 adult, senior or junior(youth) passenger.');
            event.preventDefault();
        }
        else if ($('[id*=ddlAdult]').val() == '0' && $('[id*=ddlChild]').val() != '0' && $('[id*=ddlYouth]').val() == '0' && $('[id*=ddlSenior]').val() == '0') {
            $('[id*=pnlPassenger]').show();
            event.preventDefault();
        }

        var totalAdult = parseInt($('[id*=ddlAdult]').val()) * 4;
        var totalYouth = parseInt($('[id*=ddlYouth]').val()) * 4;
        var totalSenior = parseInt($('[id*=ddlSenior]').val()) * 4;
        var totalChilden = totalAdult + totalYouth + totalSenior;

        if (parseInt($('[id*=ddlChild]').val()) > totalChilden) {
            $('[id*=pnlPassenger]').show();
            event.preventDefault();
        }
    }

    function EvolviMaxPassenger(event) {
        if ($("#hdntxtToRailName").val() == "EVOLVI" && $("#hdntxtFromRailName").val() == "EVOLVI") {
            var count = parseInt($('[id*=ddlAdult]').val()) + parseInt($('[id*=ddlChild]').val()) + parseInt($('[id*=ddlYouth]').val()) + parseInt($('[id*=ddlSenior]').val());
            if (count > 9) {
                alert('Please select at most 9 passenger.');
                event.preventDefault();
            }
        }
    }

    function isEvolviStation() {
        //Hide rail card option it will be used in future
        //        if ($("#hdntxtToRailName").val() == "EVOLVI" && $("#hdntxtFromRailName").val() == "EVOLVI") {
        //            $('[id*=div_chkAddRailCard]').show();
        //        }
        //        else {
        //            $('[id*=div_chkAddRailCard]').hide();
        //        }
    }
    function fillRailCardDynamic() {
        //Hide rail card option it will be used in future
        //        $('[id*=chkAddRailCard]').click(function () {
        //            bindRailcard('click');
        //        });

        //        $('#btnPopUpCloseWin').click(function () {
        //            $('[id*=pnlRailCardQuckLoad]').hide();
        //        });

        //        $('#btnRailCardContinue').click(function () {
        //            fillRailCardText();
        //            $('[id*=pnlRailCardQuckLoad]').hide();
        //        });
    }

    function fillRailCardText() {
        //Hide rail card option it will be used in future
        //        var ddltext = '';
        //        var cardVal = 0;
        //        $('[id*=pnlRailCardQuckLoad]').find('.starail-YourBooking-ticketDetails').each(function (key, value) {
        //            if ($(value).find('.selectlist2') != undefined) {
        //                if ($(value).find('.selectlist2').val() != '0') {
        //                    ddltext += $(value).find('.selectlist2').val() + ',';
        //                }
        //                cardVal++;
        //            }
        //        });
        //        $('[id*=hdnSelectedRailcard]').val(ddltext.length > 0 ? ddltext.substring(0, ddltext.length - 1) : ddltext);

        //        if ($('[id*=hdnSelectedRailcard]').val() != '') {
        //            var arrlist = $('[id*=hdnSelectedRailcard]').val().split(',');
        //            var txt = '';
        //            if (arrlist.length > 0) {
        //                $.each(arrlist, function (key, value) {
        //                    if (value != '') {
        //                        txt += '<span>' + railpassArray[value] + '</span></br>';
        //                    }
        //                });
        //            }
        //            $('.addcardtext').html(txt);
        //            //hire error msg when all railcard selected
        //            console.log(cardVal);
        //            console.log(arrlist.length);
        //            if (cardVal == arrlist.length)
        //                $('[id*=div_youthSnrMessage]').hide();
        //        }
        //        else {
        //            $('.addcardtext').text('Add rail card');
        //        }
    }

    function bindRailcard(type) {
        //Hide rail card option it will be used in future
        //        $.ajax({
        //            type: "POST",
        //            url: "StationList.asmx/GetRailCardList",
        //            contentType: "application/json; charset=utf-8",
        //            dataType: "json",
        //            data: '{}',
        //            success: function (data) {
        //                var result = data.d;
        //                var innerDiv = "";
        //                var ddl = '';
        //                ddl += '<option value="0">Not Applicable</option>';
        //                $(result).each(function () {
        //                    ddl += '<option value=' + this.RailCardCode + '>' + this.RailCardName + '</option>';
        //                    railpassArray[this.RailCardCode] = this.RailCardName;
        //                });
        //                ddl += '</select>';

        //                if (type == 'click') {
        //                    var countAdult = parseInt($('[id*=ddlAdult]').val());
        //                    var countChild = parseInt($('[id*=ddlChild]').val());
        //                    var countYouth = parseInt($('[id*=ddlYouth]').val());  //
        //                    var countSenior = parseInt($('[id*=ddlSenior]').val()); //
        //                    var total = countAdult + countChild + countYouth + countSenior;
        //                    if (total > 0) {
        //                        var counter = 1;
        //                        if (countAdult > 0) {
        //                            for (var i = 1; i <= countAdult; i++) {
        //                                var headerDDl = '<select class="starail-Form-select selectlist2" name="ddlRailCard' + counter + '" id="ddlRailCard' + counter + '">';
        //                                headerDDl += ddl;
        //                                innerDiv += '<div class="starail-YourBooking-ticketDetails"><div class="starail-YourBooking-col header-width"><label>Adult ' + (countAdult == 1 ? '' : i) + '</label></div>';
        //                                innerDiv += '<div class="starail-YourBooking-col row-width">' + headerDDl + '</div></div>';
        //                                counter++;
        //                            }
        //                        }
        //                        if (countChild > 0) {
        //                            for (var i = 1; i <= countChild; i++) {
        //                                var headerDDl = '<select class="starail-Form-select selectlist2" name="ddlRailCard' + counter + '" id="ddlRailCard' + counter + '">';
        //                                headerDDl += ddl;
        //                                innerDiv += '<div class="starail-YourBooking-ticketDetails"><div class="starail-YourBooking-col header-width"><label>Child ' + (countChild == 1 ? '' : i) + '</label></div>';
        //                                innerDiv += '<div class="starail-YourBooking-col row-width">' + headerDDl + '</div></div>';
        //                                counter++;
        //                            }
        //                        }
        //                        if (countYouth > 0) {
        //                            for (var i = 1; i <= countYouth; i++) {
        //                                var headerDDl = '<select class="starail-Form-select selectlist2" name="ddlRailCard' + counter + '" id="ddlRailCard' + counter + '">';
        //                                headerDDl += ddl;
        //                                innerDiv += '<div class="starail-YourBooking-ticketDetails"><div class="starail-YourBooking-col header-width"><label>Youth ' + (countYouth == 1 ? '' : i) + '</label></div>';
        //                                innerDiv += '<div class="starail-YourBooking-col row-width">' + headerDDl + '</div></div>';
        //                                counter++;
        //                            }
        //                        }
        //                        if (countSenior > 0) {
        //                            for (var i = 1; i <= countSenior; i++) {
        //                                var headerDDl = '<select class="starail-Form-select selectlist2" name="ddlRailCard' + counter + '" id="ddlRailCard' + counter + '">';
        //                                headerDDl += ddl;
        //                                innerDiv += '<div class="starail-YourBooking-ticketDetails"><div class="starail-YourBooking-col header-width"><label>Senior ' + (countSenior == 1 ? '' : i) + '</label></div>';
        //                                innerDiv += '<div class="starail-YourBooking-col row-width">' + headerDDl + '</div></div>';
        //                                counter++;
        //                            }
        //                        }
        //                        $('[id*=pnlRailCardQuckLoad]').show();
        //                    }
        //                    $('[id*=pnlRailCardQuckLoad]').find('.starail-YourBooking-table').html(innerDiv);
        //                    //show selecetd ddl
        //                    if ($('[id*=hdnSelectedRailcard]').val() != '') {
        //                        $('[id*=pnlRailCardQuckLoad]').find('.starail-YourBooking-ticketDetails').each(function (key, value) {
        //                            if ($(value).find('.selectlist2') != undefined) {
        //                                var cardlist = $('[id*=hdnSelectedRailcard]').val().split(',')[key];
        //                                $(value).find('.selectlist2 option[value="' + cardlist + '"]').prop('selected', true);
        //                            }
        //                        });
        //                    }
        //                    //end code
        //                }
        //                else if (type == 'load') {
        //                    var list = '<%=railCardCodeList %>';
        //                    list = list.length > 0 ? $.parseJSON(list) : '';
        //                    if (list.length > 0) {
        //                        $.each(list, function (key, value) {
        //                            var headerDDl = '<select class="starail-Form-select selectlist2" name="ddlRailCard' + (key + 1) + '" id="ddlRailCard' + (key + 1) + '">';
        //                            headerDDl += ddl;
        //                            innerDiv += '<div class="starail-YourBooking-ticketDetails"><div class="starail-YourBooking-col header-width"><label>Adult</label></div>';
        //                            innerDiv += '<div class="starail-YourBooking-col row-width">' + headerDDl + '</div></div>';
        //                        });
        //                        $('[id*=pnlRailCardQuckLoad]').find('.starail-YourBooking-table').html(innerDiv);
        //                        $.each(list, function (key, value) {
        //                            $('#ddlRailCard' + (key + 1) + ' option[value="' + value + '"]').prop('selected', true);
        //                        });
        //                        fillRailCardText();
        //                    }
        //                }
        //            },
        //            error: function (err) {
        //                console.log(err.d);
        //            }
        //        });
    }
</script>
<script type="text/javascript">

    var count = 0;
    var countKey = 1;
    var conditionone = 0;
    var conditiontwo = 0;
    var tabkey = 0;
    $(document).ready(function () {
        $("#txtFrom").focus();
        if ($("#txtFrom").val() != '') {
            $('#spantxtTo').text(localStorage.getItem("spantxtTo"));
        }
        if ($("#txtTo").val() != '') {
            $('#spantxtFrom').text(localStorage.getItem("spantxtFrom"));
        }
        $(window).click(function (event) {
            $('#_bindDivData').remove();
        });
        $(window).keydown(function (event) {
            if ($(event.target).attr('id').match(/txtFrom/i) || $(event.target).attr('id').match(/txtTo/i)) {
                if (event.keyCode == 13 || (event.keyCode == 9 && tabkey == 1)) {
                    tabkey = 0;
                    event.preventDefault();
                    return false;
                }
                tabkey = 1;
            }
        });
    });
    function Getkeydown(event, obj) {
        var $id = $(obj);
        var maxno = 0;
        count = event.keyCode;
        $(".popupselect").each(function () { maxno++; });
        if (count == 13 || count == 9) {
            $(".popupselect").each(function () {
                if ($(this).attr('style') != undefined) {
                    $(this).trigger('onclick');
                    $id.val($.trim($(this).find('span').text()));
                }
            });
            $('#_bindDivData').remove();
            countKey = 1;
            conditionone = 0;
            conditiontwo = 0;
        }
        else if (count == 40 && maxno > 1) {
            conditionone = 1;
            if (countKey == maxno)
                countKey = 0;
            if (conditiontwo == 1) {
                countKey++;
                conditiontwo = 0;
            }
            $(".popupselect").removeAttr('style');
            $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
            countKey++;
        }
        else if (count == 38 && maxno > 1) {
            conditiontwo = 1;
            if (countKey == 0)
                countKey = maxno;
            if (conditionone == 1) {
                countKey--;
                conditionone = 0;
            }
            countKey--;
            $(".popupselect").removeAttr('style');
            $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
        }
        else {
            countKey = 1;
            conditionone = 0;
            conditiontwo = 0;
        }
    }
    function selectpopup(e) {
        if (count != 40 && count != 38 && count != 13 && count != 37 && count != 39 && count != 9) {
            var $this = $(e);
            var data = $this.val();
            var station = '';
            var hostName = window.location.host;
            var url = "http://" + hostName;
            if (window.location.toString().match(/https:/i))
                url = "https://" + hostName;
            if ($("#txtFrom").val() == '' && $("#txtTo").val() == '') {
                $('#spantxtFrom').text('');
                $('#spantxtTo').text('');
            }
            var filter = $("#span" + $this.attr('id') + "").text();
            if (filter == "" && $this.val() != "")
                filter = $("#hdnFilter").val();
            $("#hdnFilter").val(filter);
            if ($this.attr('id') == 'txtTo') {
                station = $("#txtFrom").val();
            }
            else {
                station = $("#txtTo").val();
            }
            if (hostName == "localhost") {
                url = url + "/1TrackNew";
            }
            var hostUrl = url + "/StationList.asmx/getStationsXList";
            data = data.replace(/[']/g, "♥");
            var $div = $("<div id='_bindDivData' onmousemove='_removehoverclass(this)'/>");
            $.ajax({
                type: "POST",
                url: hostUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'prefixText':'" + data + "','filter':'" + filter + "','station':'" + station + "'}",
                success: function (msg) {
                    $('#_bindDivData').remove();
                    var lentxt = data.length;
                    $.each(msg.d, function (key, value) {
                        var splitdata = value.split('ñ');
                        var lenfull = splitdata[0].length; ;
                        var txtupper = splitdata[0].substring(0, lentxt);
                        var txtlover = splitdata[0].substring(lentxt, lenfull);
                        $this.after($div.append("<div class='popupselect' onclick='_Bindthisvalue(this)'><b class='_prifixText'>" + txtupper + "</b>" + txtlover + "<span style='display:none'>" + splitdata[0] + "</span><div style='display:none'>" + splitdata[1] + "</div><p style='display:none'>" + splitdata[2] + "</p></div>"));
                    });
                    $(".popupselect:eq(0)").attr('style', 'background-color: #ccc !important');
                },
                error: function () {
                    //                    alert("Wait...");
                }
            });
        }
    }
    function _removehoverclass(e) {
        $(".popupselect").hover(function () {
            $(".popupselect").removeAttr('style');
            $(this).attr('style', 'background-color: #ccc !important');
            countKey = $(this).index();
        });
    }
    function _Bindthisvalue(e) {
        var idtxtbox = $('#_bindDivData').prev("input").attr('id');
        $("#" + idtxtbox + "").val($(e).find('span').text());
        if (idtxtbox == 'txtTo') {
            $('#spantxtFrom').text($(e).find('div').text());
            localStorage.setItem("spantxtFrom", $(e).find('div').text());
            $("#hdntxtToRailName").val($(e).find('p').text());
        }
        else {
            $('#spantxtTo').text($(e).find('div').text());
            localStorage.setItem("spantxtTo", $(e).find('div').text());
            $("#hdntxtFromRailName").val($(e).find('p').text());
        }
        $('#_bindDivData').remove();
        isEvolviStation();
    }
    function checkDate(sender) {
        var selectedDate = new Date(sender._selectedDate);
        var today = new Date();
        today.setHours(0, 0, 0, 0);

        if (selectedDate < today) {
            alert('Select a date sometime in the future!');
            sender._selectedDate = new Date();
            sender._textbox.set_Value(sender._selectedDate.format(sender._format));
        }
    }
    function CheckDateVal(sender, args) {

        if ($.trim(document.getElementById("txtDepartureDate").value).toString() != "DD/MM/YYYY") {
            var startDate = new Date(document.getElementById("txtDepartureDate").value);
            var finishDate = new Date(document.getElementById("txtReturnDate").value);

            if (startDate > finishDate) {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }
        else {
            args.IsValid = true;
        }
    }
    function OnClientPopulating(sender, e) {
        sender._element.className = "loading";
    }
    var unavailableDates = '<%=unavailableDates1 %>';
    function nationalDays(date) {
        dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();

        if ($.inArray(dmy, unavailableDates) > -1) {
            return [false, "", "Unavailable"];
        }
        return [true, ""];
    }
    $(document).ready(function () {
        LoadCal();
        if ($("#rdBookingType_0").prop("checked")) {
            clickaa($("#rdBookingType_0").val());
        }
        else
            if ($("#rdBookingType_1").prop("checked")) {
                clickaa($("#rdBookingType_1").val());
            }
    });
    function LoadCal() {
        callvalerror();
        $(".journeycheckboxSpan").remove();
        $(".classcheckbox").find('label').before("<span class='journeycheckboxSpan'><i class='starail-Icon-tick'></i></span>");
        $(".radioSpan").remove();
        $(".spanradio").find('label').before("<span class='radioSpan' style='float:left;'><i class='radioSpancenter'></i></span>");
        $("#txtDepartureDate, #txtReturnDate").bind("contextmenu cut copy paste", function (e) {
            return false;
        });
        $("#txtDepartureDate").datepicker(
        {
            numberOfMonths: 1,
            dateFormat: 'dd/M/yy',
            beforeShowDay: nationalDays,
            showButtonPanel: true,
            firstDay: 1,
            minDate: '<%=minDate %>',
            onClose: function (selectedDate) {
                $("#txtReturnDate").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#txtReturnDate").datepicker({
            numberOfMonths: 1,
            dateFormat: 'dd/M/yy',
            beforeShowDay: nationalDays,
            showButtonPanel: true,
            firstDay: 1,
            minDate: $("#txtDepartureDate").val()
        });
        $("#txtDepartureDate, #txtReturnDate").keypress(function (event) { event.preventDefault(); });
    }
    function clickaa(value) {
        if (value == "0") {
            $("#rdBookingType0, #rdBookingType_0").trigger('click');
            $("#lblrtnerror").hide();
            $("#txtReturnDate").val("");
            $("#txtReturnDate,#ddlReturnTime").attr("disabled", "disabled");
            ValidatorEnable($('[id*=reqvalerror4]')[0], false);
        }
        else {
            $("#rdBookingType1, #rdBookingType_1").trigger('click');
            $("#lblrtnerror,#txtReturnDate").show();
            $("#txtReturnDate,#ddlReturnTime").removeAttr("disabled");
            ValidatorEnable($('[id*=reqvalerror4]')[0], true);
        }
    }
</script>
<style type="text/css">
    .pnlpopup
    {
        z-index: 111 !important;
        position: fixed !important;
        left: 23%;
        top: 25%;
    }
    .modalBackgroundnew
    {
        position: fixed !important;
        left: 0px !important;
        top: 0px !important;
        z-index: 110 !important;
        width: 100% !important;
        height: 100% !important;
    }
    .pnl-pop .starail-YourBooking-table
    {
        display: inline-block;
        max-height: 250px;
        overflow-x: auto;
    }
    .cancelBtn
    {
        text-align: center;
    }
    .starail-Icon-tick-yes
    {
        color: #0c6ab8;
        padding-left: 3px;
        position: absolute;
        z-index: 1;
    }
    .addcardtext span
    {
        display: table-row;
    }
    @media only screen and (max-width: 639px)
    {
        .pnl-pop .starail-YourBooking-ticketDetails
        {
            padding: 5px 11px 10px 5px;
        }
        .pnl-pop .header-width
        {
            width: 30%;
        }
        .pnl-pop .row-width
        {
            width: 70%;
        }
        .pnl-pop .starail-YourBooking-ticketDetails
        {
            margin-bottom: 5px;
        }
    }
</style>