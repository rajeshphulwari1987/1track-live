﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UCpassdetalis.ascx.cs"
    Inherits="UserControls_passdetalis" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<script type="text/javascript">
    $(document).ready(function () {
        $(".bookingcheckboxSpan").remove();
        $(".bookingcheckbox").find('label').before("<span class='bookingcheckboxSpan'><i class='starail-Icon-tick'></i></span>");
        //        delpassselected();
        $("[id*='txtFirstName'],[id*='txtLastName']").keypress(function (e) {
            var regex = new RegExp("^[a-zA-Z0-9]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            var code = e.which || e.charCode || e.keyCode;
            if (code == 32 || code == 8 || code == 46 || code == 9 || code == 37 || code == 39)
                return true;
            e.preventDefault();
            return false;
        });
    });
    function delpassselected() {
        $("[id*=lnkDelete]").off('click');
        $("[id*=lnkDelete]").on('click', function () {
            var countsaver = 0;
            var result = false;
            var saver = $(this).attr('saver');
            if (saver.match(/saver/i)) {
                result = confirm('You are now deleting the whole saverpass booking, as min 2 pax is required.');
                saver = 'yes';
            }
            else
                result = confirm('Are you sure? Do you want to delete this item?');
            if (result) {
                var Id = $(this).attr('commandargument');
                $("a[saver]").each(function (key, value) {
                    if ($(this).attr('saver').match(/saver/i)) {
                        countsaver++;
                    }
                });
                console.log(countsaver);
                JSONcall2(Id, $(this), saver, countsaver);
            }
        });
    }
    function JSONcall2(Id, obj, hassaver, countsaver) {
        var hostUrl = window.location + '.aspx/del_pass';
        $(document).ready(function () {
            $.ajax({
                url: hostUrl,
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                data: "{'Id':'" + Id + "'}",
                success: function (Pdata) {
                    if (Pdata.d == 'BookingCart') {
                        if (hassaver == 'yes' && countsaver < 3) {
                            $("a[saver]").each(function (key, value) {
                                if ($(this).attr('saver').match(/saver/i)) {
                                    $(this).parent().parent().remove();
                                }
                            });
                        }
                        else
                            obj.parent().parent().remove();
                        $(".customheader").each(function (key, value) {
                            $(this).html('Lead Passenger Information: Pass ' + (key + 1));
                        });
                    }
                    else
                        window.location = Pdata.d;
                    console.log(Pdata.d);
                    getdata();
                },
                error: function () {
                }
            });
        });
    }
    /**Error Focus**/
    function Focusonerrorcontrol() {
        $(window).on('click', function () {
            var Toperror = $(".starail-Form-error").eq(0).offset();
            if (Toperror != undefined) {
                $('body,html').animate({ scrollTop: Toperror.top }, 1000);
                $(window).off('click');
            }
        });
        delpassselected();
    }
    function validDOBpos(id) {
        var toppos = $("#" + id).eq(0).offset().top;
        $('body,html').animate({ scrollTop: toppos }, 1000);
        delpassselected();
    }
    /*end*/

    function logs(xx) {
        console.log(xx);
    }

    function chkVal(id, e) {
        if (e == 0)
            e = 3;
        var txtStartDateId = '#' + $(id).find('input[type=text]').attr('id');
        /*interrail date section*/
        /*var interraildatestring = '16,May,2016';
        var validinterraildate = (new Date(interraildatestring));*/
        var validinterraildate = (new Date());
        var categoryid = $(id).attr('categoryid');
        var isinterrailpass = false;
        /*Live block interrail*/
        /*if (categoryid == '171caff2-85e9-44e9-bbec-d6e07b1918cb' || categoryid == 'd470e66b-555d-4884-81bc-daede593507d') {
        isinterrailpass = true;
        }
        else
        validinterraildate = new Date();*/
        /*interrail date section*/
        $(txtStartDateId).datepicker({
            numberOfMonths: 1,
            dateFormat: 'dd/M/yy',
            showButtonPanel: true,
            firstDay: 1,
            minDate: 0,
            maxDate: isinterrailpass ? validinterraildate : '+' + e + 'm-1d'
        });
        $(txtStartDateId).datepicker('show');
    }

    function Productval(e) {
        var d = $(e).val().split('/');
        var startdate = new Date(d[1] + "," + d[0] + "," + d[2]);
        if (startdate == "Invalid Date") {
            alert("Pass Start Date is not valid format");
        }
        else {
            $("#MainContent_txtDateOfDepature").val($(e).val());
            var sdate = new Date($(e).parent().find("#hdnfromdate").val());
            var edate = new Date($(e).parent().find("#hdntodate").val());
            var msg = $(e).parent().find("#hdnmsg").val();
            var valdationmsg = "#" + $(e).attr('id').replace("txtStartDate", "lblInvalidDate");
            if (sdate <= startdate && edate >= startdate) {
                $(valdationmsg).text('').hide();
            }
            else {
                $(valdationmsg).text(msg).show();
                $(e).val("DD/MM/YYYY");
            }
        }
    }
    function chkDate(event) {
        var key = event.keyCode;
        var trgid = event.target.id ? event.target.id : event.srcElement.id;
        var id = "#" + trgid;
        var value = $(id).val();
        var strmon = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var m = value.split("/");
        var mon = parseInt(m[1]) - 1;
        if ($.isNumeric(mon))
            $(id).val(m[0] + "/" + strmon[mon] + "/" + m[2]);
    }

    function WapppingDdl() {
        $(".DivMobileView").each(function (key, value) {
            $("#MainContent_UcPassDetail_rptPassDetail_ddlMonth_" + key).insertBefore("#MainContent_UcPassDetail_rptPassDetail_ddlDay_" + key);
        });
    }
</script>
<style type="text/css">
    .absolute {
        color: rgb(237, 239, 241);
    }

    @media only screen and (max-width: 639px) {
        .inlinenew {
            display: inline !important;
        }
    }

    .pass-middel-name {
        width: 49%;
        float: left;
    }

    .pass-last-name {
        width: 49%;
        float: right;
    }
</style>
<script type="text/javascript">
    function startdayalertshow(obj, event) {
        $(obj).next('div').addClass('is-open');
    }

    function startdayopen(obj, event) {
        $(obj).next().addClass("starail-u-hideDesktop");
    }

    function startdayclose(obj, event) {
        $(obj).parent().parent().removeClass("starail-u-hideDesktop");
    }

</script>
<asp:HiddenField ID="hdnScreenSize" runat="server" />
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:Repeater ID="rptPassDetail" runat="server" OnItemDataBound="rptPassDetail_ItemDataBound"
            OnItemCommand="rptPassDetail_ItemCommand">
            <ItemTemplate>
                <div class="starail-BookingDetails-form">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <div class="starail-Lightbox-closeContainer" style="float: right; cursor: pointer;">
                                <asp:HiddenField runat="server" ID="hdnbookingfee" Value='<%#Eval("BookingFee")%>' />
                                <asp:HiddenField runat="server" ID="hdnSaver" Value='<%#Eval("TravellerName")%>' />
                                <asp:HiddenField runat="server" ID="hdnCategoryID" Value='<%#Eval("CategoryID")%>' />
                                <asp:LinkButton ID="lnkRemovePass" runat="server" CommandName="RemovePass" CommandArgument='<%#Eval("Id") %>'
                                    CausesValidation="false" Style="position: absolute; top: -11.2px; right: -11.2px; width: 32px; height: 32px; border-radius: 16px; box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.15); display: block; color: #FFF; text-decoration: none; background-color: #0c6ab8; text-align: center; transition: background-color 0.3s ease-in-out; }">
                                <i class="starail-Icon starail-Icon-close" style="line-height: 31px;"></i> </asp:LinkButton>
                            </div>
                            <h2>
                                <div class="customheader">
                                    Lead Passenger Information: Pass
                                    <asp:Label ID="passno" runat="server"></asp:Label>
                                </div>
                                <p class="bkpassdetails">
                                    <asp:HiddenField ID="hdnPassSale" runat="server" Value='<%#Eval("PrdtId")+","+Eval("Price")+","+Eval("TravellerID")+","+Eval("ClassID")+","+Eval("ValidityID")+","+Eval("CategoryID")+","+Eval("commission")+","+Eval("MarkUp")+","+Eval("CountryStartCode")+","+Eval("CountryEndCode")+","+Eval("CountryLevelIDs")%>' />
                                    <asp:HiddenField ID="HdnOriginalPrice" runat="server" Value='<%#Eval("OriginalPrice")%>' />
                                    <asp:HiddenField ID="hdnPrdtId" runat="server" Value='<%#Eval("PrdtId")%>' />
                                    <asp:HiddenField ID="hdnSalePrice" runat="server" Value='<%#Eval("SalePrice")%>' />
                                    <asp:HiddenField ID="hdnCountryLevelIDs" runat="server" Value='<%#Eval("CountryLevelIDs")%>' />
                                    <asp:HiddenField ID="lblLID" runat="server" Value='<%#Eval("Id")%>' />
                                    <asp:HiddenField ID="hdnproductname" runat="server" Value='<%#Eval("PrdtName")%>' />
                                    <%#Eval("PrdtName")%>
                                    <asp:Label ID="lblCountryName" runat="server" />,
                                    <%#Eval("ValidityName")%>,
                                    <%#Eval("TravellerName")%>,
                                    <%#Eval("ClassName")%>
                                    -
                                    <%=currency.ToString()%>
                                    <%#Eval("Price")%>
                                    <br />
                                </p>
                                <p class="bkpassdetails">
                                    <span>Your age must be correct at time of travel for the pass chosen</span>
                                    <asp:Label Style="display: table; padding-top: 10px;" runat="server" ID="lblHeaderNote"
                                        Text="Names must be entered exactly as per passport including all middle names"></asp:Label>
                                </p>
                            </h2>
                            <div class="starail-Form-row">
                                <label for="starail-firstname" class="starail-Form-label">
                                    Passenger name <span class="starail-Form-required">*</span></label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col starail-Form-inputContainer-col--ddInput">
                                        <div>
                                            <asp:DropDownList ID="ddlTitle" CssClass="starail-Form-select selectTitlewidth" runat="server">
                                                <asp:ListItem Text="Title" Value="0" />
                                                <asp:ListItem Text="Dr." Value="1" />
                                                <asp:ListItem Text="Mr." Value="2" />
                                                <asp:ListItem Text="Miss" Value="3" />
                                                <asp:ListItem Text="Mrs." Value="4" />
                                                <asp:ListItem Text="Ms" Value="5" />
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqvalerrorTitle" runat="server" CssClass="absolute"
                                                Display="Static" Text="*" ErrorMessage="Please select title." ValidationGroup="vgs1"
                                                ControlToValidate="ddlTitle" InitialValue="0" />
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtFirstName" runat="server" class="starail-Form-input required"
                                                Text='<%#Eval("FirstName") %>' placeholder="First name" />
                                            <asp:RequiredFieldValidator ID="reqvalerrorFname" runat="server" CssClass="absolute"
                                                Display="Static" Text="*" ErrorMessage="Please enter pass first name." ValidationGroup="vgs1"
                                                ControlToValidate="txtFirstName" />
                                            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtFirstName"
                                                WatermarkCssClass="watermark" WatermarkText="First name" />
                                            <asp:FilteredTextBoxExtender ID="ftr1" runat="server" TargetControlID="txtFirstName"
                                                InvalidChars="0123456789" FilterMode="InvalidChars" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label for="starail-dob-day" class="starail-Form-label">
                                    <span class="starail-Form-required"></span>
                                </label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <div class="pass-middel-name" runat="server" id="divMiddleName">
                                            <asp:TextBox ID="txtMiddleName" runat="server" class="starail-Form-input" Text='<%#Eval("MiddleName") %>' />
                                            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtMiddleName"
                                                WatermarkCssClass="watermark" WatermarkText="Middle names" />
                                            <br />
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtMiddleName"
                                                InvalidChars="0123456789" FilterMode="InvalidChars" />
                                        </div>
                                        <div class="pass-last-name" runat="server" id="divLastName">
                                            <asp:TextBox ID="txtLastName" runat="server" class="starail-Form-input required"
                                                Text='<%#Eval("LastName") %>' />
                                            <asp:RequiredFieldValidator ID="reqvalerrorLname" runat="server" CssClass="absolute"
                                                Display="Static" Text="*" ErrorMessage="Please enter pass last name." ValidationGroup="vgs1"
                                                ControlToValidate="txtLastName" />
                                            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtLastName"
                                                WatermarkCssClass="watermark" WatermarkText="Last name" />
                                            <br />
                                            <asp:FilteredTextBoxExtender ID="ftr2" runat="server" TargetControlID="txtLastName"
                                                InvalidChars="0123456789" FilterMode="InvalidChars" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row" runat="server" id="Div_IsEurailPass">
                                <label for="starail-dob-day" class="starail-Form-label">
                                    Date of birth <span class="starail-Form-required">*</span></label>
                                <div class="starail-Form-inputContainer DivMobileView">
                                    <div class="starail-Form-inputContainer-col starail-Form-inputContainer-col--autoWidth">
                                        <asp:DropDownList ID="ddlDay" runat="server" class="starail-Form-select">
                                            <asp:ListItem Value="DD" Text="DD"></asp:ListItem>
                                            <asp:ListItem Value="01" Text="1"></asp:ListItem>
                                            <asp:ListItem Value="02" Text="2"></asp:ListItem>
                                            <asp:ListItem Value="03" Text="3"></asp:ListItem>
                                            <asp:ListItem Value="04" Text="4"></asp:ListItem>
                                            <asp:ListItem Value="05" Text="5"></asp:ListItem>
                                            <asp:ListItem Value="06" Text="6"></asp:ListItem>
                                            <asp:ListItem Value="07" Text="7"></asp:ListItem>
                                            <asp:ListItem Value="08" Text="8"></asp:ListItem>
                                            <asp:ListItem Value="09" Text="9"></asp:ListItem>
                                            <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                            <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                            <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                            <asp:ListItem Value="13" Text="13"></asp:ListItem>
                                            <asp:ListItem Value="14" Text="14"></asp:ListItem>
                                            <asp:ListItem Value="15" Text="15"></asp:ListItem>
                                            <asp:ListItem Value="16" Text="16"></asp:ListItem>
                                            <asp:ListItem Value="17" Text="17"></asp:ListItem>
                                            <asp:ListItem Value="18" Text="18"></asp:ListItem>
                                            <asp:ListItem Value="19" Text="19"></asp:ListItem>
                                            <asp:ListItem Value="20" Text="20"></asp:ListItem>
                                            <asp:ListItem Value="21" Text="21"></asp:ListItem>
                                            <asp:ListItem Value="22" Text="22"></asp:ListItem>
                                            <asp:ListItem Value="23" Text="23"></asp:ListItem>
                                            <asp:ListItem Value="24" Text="24"></asp:ListItem>
                                            <asp:ListItem Value="25" Text="25"></asp:ListItem>
                                            <asp:ListItem Value="26" Text="26"></asp:ListItem>
                                            <asp:ListItem Value="27" Text="27"></asp:ListItem>
                                            <asp:ListItem Value="28" Text="28"></asp:ListItem>
                                            <asp:ListItem Value="29" Text="29"></asp:ListItem>
                                            <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                            <asp:ListItem Value="31" Text="31"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqvalerrorDay" runat="server" CssClass="absolute"
                                            Display="Static" Text="*" ErrorMessage="Please select dob day." ValidationGroup="vgs1"
                                            ControlToValidate="ddlDay" InitialValue="DD" />
                                        <asp:DropDownList ID="ddlMonth" runat="server" class="starail-Form-select" placeholder="your age must be correct at time of travel">
                                            <asp:ListItem Value="MM" Text="MM"></asp:ListItem>
                                            <asp:ListItem Value="Jan" Text="Jan"></asp:ListItem>
                                            <asp:ListItem Value="Feb" Text="Feb"></asp:ListItem>
                                            <asp:ListItem Value="Mar" Text="Mar"></asp:ListItem>
                                            <asp:ListItem Value="Apr" Text="Apr"></asp:ListItem>
                                            <asp:ListItem Value="May" Text="May"></asp:ListItem>
                                            <asp:ListItem Value="Jun" Text="Jun"></asp:ListItem>
                                            <asp:ListItem Value="Jul" Text="Jul"></asp:ListItem>
                                            <asp:ListItem Value="Aug" Text="Aug"></asp:ListItem>
                                            <asp:ListItem Value="Sep" Text="Sep"></asp:ListItem>
                                            <asp:ListItem Value="Oct" Text="Oct"></asp:ListItem>
                                            <asp:ListItem Value="Nov" Text="Nov"></asp:ListItem>
                                            <asp:ListItem Value="Dec" Text="Dec"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqvalerrorMonth" runat="server" CssClass="absolute"
                                            Display="Static" Text="*" ErrorMessage="Please select dob month." ValidationGroup="vgs1"
                                            ControlToValidate="ddlMonth" InitialValue="MM" />
                                        <asp:DropDownList ID="ddlYear" runat="server" class="starail-Form-select" placeholder="your age must be correct at time of travel">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqvalerrorYear" runat="server" CssClass="absolute"
                                            Display="Static" Text="*" ErrorMessage="Please select dob year." ValidationGroup="vgs1"
                                            ControlToValidate="ddlYear" InitialValue="YYYY" />
                                        <asp:Label ID="lblInvalidDate1" Style="display: none;" runat="server" CssClass="starail-Form-required" />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label for="starail-country" class="starail-Form-label">
                                    Country of residence <span class="starail-Form-required">*</span></label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:HiddenField ID="hdnproductid" runat="server" Value='<%#Eval("PrdtId") %>' />
                                        <asp:DropDownList ID="ddlCountry" runat="server" class="starail-Form-select" onchange="GetValidCountry(this)" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorCountry" runat="server" CssClass="absolute"
                                            Display="Static" Text="*" ErrorMessage="Please select pass country." ValidationGroup="vgs1"
                                            ControlToValidate="ddlCountry" InitialValue="0" />
                                        <asp:Label runat="server" Style="color: Red;" ID="errormsg"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row" runat="server" visible='<%#Eval("PassportIsVisible")%>'>
                                <label for="starail-passportno" class="starail-Form-label">
                                    Passport number <span class="starail-Form-required">*</span></label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtPassportNumber" Text='<%#Eval("PassportNo") %>' class="starail-Form-input"
                                            runat="server" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorPP" ValidationGroup="vgs1" runat="server"
                                            ErrorMessage="Please enter pass passport number." CssClass="absolute" Display="Static"
                                            Text="*" ControlToValidate="txtPassportNumber" />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label for="starail-dob-day" class="starail-Form-label forpopup">
                                    When do you want to start using this pass? <span class="starail-Form-required">*</span>
                                    <div class="starail-u-hideMobile" runat="server" id="Lblalertboxstartday" style="display: inline; margin-top: -3px;"
                                        onclick="startdayalertshow(this,event)">
                                        <a href="#" class="js-lightboxOpen" data-lightbox-id="starail-ticket-infooffer_9"><i
                                            class="starail-Icon-question"></i></a>
                                    </div>
                                    <div class="starail-Lightbox starail-u-hideMobile">
                                        <div class="starail-Lightbox-content">
                                            <div class="starail-Lightbox-closeContainer">
                                                <a href="#" class="starail-Lightbox-close js-lightboxClose"><i class="starail-Icon starail-Icon-close"></i></a>
                                            </div>
                                            <div class="starail-Lightbox-content-inner">
                                                <div class="starail-Lightbox-text">
                                                    <h3 class="starail-Lightbox-textTitle">Start Day Info.</h3>
                                                    <div runat="server" class="" id="alertboxstartday">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="starail-u-hideDesktop inlinenew" runat="server" id="LblalertboxstartdayM"
                                        onclick="startdayopen(this,event)" style="margin-top: -3px;">
                                        <i class="starail-Icon-question"></i>
                                    </div>
                                    <div class="starail-Form-row " style="padding: 1px; background: #ccc; width: 100%; display: none">
                                        <div class="starail-Lightbox-closeContainer">
                                            <i class="starail-Icon starail-Icon-close  starail-Lightbox-close " onclick="startdayclose(this,event)"
                                                style="padding: 7px;"></i>
                                        </div>
                                        <div class="starail-Lightbox-content-inner">
                                            <div class="starail-Lightbox-text">
                                                <h3 class="starail-Lightbox-textTitle">Start Day Info.</h3>
                                                <div runat="server" class="" id="alertboxstartdaymobile">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col starail-Form-inputContainer-col--autoWidth starail-Form-datePicker"
                                        onclick="chkVal(this,'<%#Eval("MonthValidity")%>')" categoryid='<%#Eval("CategoryID")%>'>
                                        <asp:TextBox ID="txtStartDate" MaxLength="11" runat="server" Text='<%#(Eval("PassStartDate") != null ? Eval("PassStartDate","{0:dd/MMM/yyyy}") : "DD/MM/YYYY") %>'
                                            class="starail-Form-input" onfocusout="chkDate(event)" onchange="Productval(this)"
                                            autocomplete="off" />
                                        <asp:Label ID="lblInvalidDate" Style="display: none;" runat="server" CssClass="starail-Form-required" />
                                        <div style="display: none;">
                                            <asp:HiddenField ID="hdnfromdate" ClientIDMode="Static" runat="server" />
                                            <asp:HiddenField ID="hdntodate" ClientIDMode="Static" runat="server" />
                                            <asp:HiddenField ID="hdnmsg" ClientIDMode="Static" runat="server" />
                                        </div>
                                        <asp:RequiredFieldValidator ID="reqvalerrorPSD" ValidationGroup="vgs1" runat="server"
                                            InitialValue="DD/MM/YYYY" ErrorMessage="Please enter pass start date." CssClass="absolute"
                                            Display="Static" Text="*" ControlToValidate="txtStartDate" />
                                        <i class="starail-Icon-datepicker" style="padding-right: 20px;"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row" runat="server" visible='<%#Eval("NationalityIsVisible")%>'>
                                <label for="starail-dob-day" class="starail-Form-label">
                                    Nationality <span class="starail-Form-required">*</span></label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:DropDownList ID="ddlNationality" runat="server" class="starail-Form-select" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorNty" ValidationGroup="vgs1" runat="server"
                                            ErrorMessage="Please enter pass nationality." CssClass="absolute" Display="Static"
                                            Text="*" ControlToValidate="ddlNationality" InitialValue="0" />
                                    </div>
                                </div>
                            </div>
                            <p class="starail-BookingDetails-protectPassDisclaimer" style="height: 1px; position: relative; padding: 0px; margin: 0px; color: transparent;">
                                Worry about losing stuff? Ticket protection entitles you to a refund on the unused
                                portion of your rail pass or ticket if it gets lost or stolen while you're travelling.
                            </p>
                            <div <%=TicketProtection %>>
                                <h2>
                                    <i class="starail-Icon-shield"></i>Protect your pass</h2>
                                <p class="starail-BookingDetails-protectPassDisclaimer">
                                    Worry about losing stuff? Ticket protection entitles you to a refund on the unused
                                    portion of your rail pass or ticket if it gets lost or stolen while you're travelling.
                                    <a href="#" class="js-lightboxOpen" data-lightbox-id="starail-ticket-info">Terms and
                                        conditions apply</a>.
                                </p>
                                <div class="starail-Form-row">
                                    <div class="starail-Form-spacer starail-u-hideMobile">
                                        Protect your pass for just <strong>
                                            <%=currency+TkpAmount %></strong>
                                    </div>
                                    <div class="starail-Form-inputContainer">
                                        <div class="starail-Form-inputContainer-col">
                                            <label class="starail-BookingDetails-form-fancyCheckboxx">
                                                <asp:CheckBox ID="chkTicketProtection" runat="server" onclick="getdata()" Text=" Protect my pass"
                                                    CssClass="starail-Form-fancyCheckbox bookingcheckbox calculateTotal" />
                                                <span class="starail-u-hideDesktopInline starail-Form-fancyCheckbox-trailingMobileText">for just <strong>
                                                    <%=currency+TkpAmount %>
                                                </strong>.</span>
                                                <input type="hidden" class="hdntkprs" value='<%=TkpAmount %>' />
                                                <input type="hidden" class="hdnprs" value='<%#Eval("Price")%>' />
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="lnkRemovePass" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="starail-Lightbox" id="starail-ticket-info" data-scroll="1600">
    <div class="starail-Lightbox-content">
        <div class="starail-Lightbox-closeContainer">
            <a href="#" class="starail-Lightbox-close js-lightboxClose"><i class="starail-Icon starail-Icon-close"></i></a>
        </div>
        <div class="starail-Lightbox-content-inner">
            <div class="starail-Lightbox-text">
                <div id="divpopupdata" runat="server">
                </div>
            </div>
        </div>
    </div>
</div>
<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="modalBackground progessposition">
        </div>
        <div class="progess-inner2">
            UPDATING RESULTS...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<script type="text/javascript">
    function saverhas(no) {
        var result = confirm('You are now deleting the whole saverpass booking, as min ' + no + ' pax is required.');
        if (result) {
            $("#MainContent_UcPassDetail_UpdateProgress1").show();
        }
        return result;
    }
    function savernothas() {
        var result = confirm('Are you sure? Do you want to delete this item?'); if (result) {
            $("#MainContent_UcPassDetail_UpdateProgress1").show();
        }
        return result;
    }
    function pophide() {
        $(".bookingcheckboxSpan").remove();
        $(".bookingcheckbox").find('label').before("<span class='bookingcheckboxSpan'><i class='starail-Icon-tick'></i></span>");
        $("#MainContent_UcPassDetail_UpdateProgress1").hide();
    }
</script>
<script type="text/javascript">
    function GetValidCountry(obj) {
        var hostUrl = window.location.origin + window.location.pathname + '.aspx/AjaxCheckCountry';
        var productid = $(obj).parent().find("[id*=hdnproductid]").val();
        var countryid = $(obj).val();
        var errormsg = $(obj).parent().find("[id*=errormsg]");
        //        console.log(productid + "; " + countryid + "; " + errormsg + "; " + hostUrl + "; ");
        $.ajax({
            url: hostUrl,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            data: "{'productid':'" + productid + "','countryid':'" + countryid + "'}",
            success: function (Pdata) {
                if (Pdata.d == "yes") {
                    $(errormsg).text('');
                }
                else {
                    $(errormsg).text(Pdata.d);
                    $(obj).val("0");
                }
                shipingchk(); callvalerror(); pophide();
            },
            error: function () {
                console.log("error on request method");
            }
        });
    }
</script>
