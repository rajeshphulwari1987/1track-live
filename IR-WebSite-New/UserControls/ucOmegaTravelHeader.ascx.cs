﻿#region Using
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using Business;
using OneHubServiceRef;
using System.Web.UI.HtmlControls;
using System.Web.Script.Serialization;
using System.Globalization;
#endregion

public partial class UserControls_ucOmegaTravelHeader : System.Web.UI.UserControl
{
    private readonly db_1TrackEntities db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    Guid siteId;
    public bool IsHourVisible = false;
    public string MyCart, siteURL, SiteLogoUrl, FaviconIcon, b2bSitePhone, b2bSiteHeading, b2bSiteTitle, AttentionTitle = string.Empty;
    public string AdminSiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    public bool IsOmegaTravelLi = false;
    public string AgentName = "Agent Login";

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                if (Session["siteId"] != null)
                    siteId = Guid.Parse(Session["siteId"].ToString());
                else
                    return;

                if (_oWebsitePage.GetSiteLayoutType(siteId) != 8)
                    return;

                siteURL = _oWebsitePage.GetSiteURLbySiteId(siteId);
                if (AgentuserInfo.UserID != Guid.Empty)
                    AgentPageLoad();
                else
                    AgentName = "Agent Login";

                hour1.Visible = hour4.Visible = IsHourVisible = _oWebsitePage.IsVisibleHourOfSite(siteId);

                if (db.tblSites.Any(x => x.ID == siteId && (bool)x.IsAgent && x.LayoutType == 8))
                {
                    hour4.Attributes.Add("class", "agentcut");
                    IsOmegaTravelLi = printingAgent.Visible = aFoclink.Visible = true;
                }

                ShowAd75Menu();
            }
            LoadSiteLogo();
            SetItemOrderCount();
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void LoadSiteLogo()
    {
        try
        {
            var data = db.tblSites.FirstOrDefault(x => x.ID == siteId && x.IsActive == true);
            if (data != null)
            {
                if (!string.IsNullOrEmpty(data.FaviconPath))
                    FaviconIcon = "<link href='" + AdminSiteUrl + data.FaviconPath + "' rel='shortcut icon' type='image/x-icon' />";

                if (!string.IsNullOrEmpty(data.LogoPath))
                    SiteLogoUrl = AdminSiteUrl + data.LogoPath;
                else
                    SiteLogoUrl = siteURL + "assets/img/branding/logo.png";
            }

            var siteDetail = _oWebsitePage.GetSiteDetailsById(siteId);
            if (data != null)
            {
                b2bSitePhone = siteDetail.PhoneNumber;
                b2bSiteTitle = siteDetail.SiteHeadingTitle;
                b2bSiteHeading = siteDetail.SiteHeading;
                //AttentionTitle = siteDetail.AttentionTitle;
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void SetItemOrderCount()
    {
        try
        {
            if (Session["RailPassData"] == null && AgentuserInfo.UserID != Guid.Empty && !AgentuserInfo.IsAffliate)
                GetOrderinBookingCart();
            string linkURL = siteURL;
            int countpass = 0;
            if (Session["RailPassData"] != null)
            {
                var lstRP = Session["RailPassData"] as List<getRailPassData>;
                countpass = lstRP.Count;
                linkURL = siteURL + "BookingCart";
            }
            if (countpass < 1)
                MyCart = "<a href='" + linkURL + "' style='padding: 2px 17px;padding-left:39px;'><lable style='background:url(" + siteURL + "images/STACartEmp.png)no-repeat;background-size: 25px 22px;position: absolute;width: 25px;height: 25px;margin-left: -25px;'></lable>&nbsp;Cart (" + countpass + ")</a>";
            else
                MyCart = "<a href='" + linkURL + "' style='padding: 2px 17px;padding-left:39px;'><lable style='background:url(" + siteURL + "images/STACart.jpg)no-repeat;background-size: 25px 25px;position: absolute;width: 25px;height: 25px;margin-left: -25px;'></lable>&nbsp;Cart (" + countpass + ")</a>";
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void GetOrderinBookingCart()
    {
        try
        {
            if (Session["RailPassData"] == null && AgentuserInfo.UserID != Guid.Empty && Session["OrderID"] == null)
            {
                var list = new ManageBooking().GetLastAgentOrderInCreateStatus(AgentuserInfo.UserID);
                if (list != null && list.Count() > 0)
                {
                    Session.Add("RailPassData", list);
                    Session["OrderID"] = list.FirstOrDefault().OrderIdentity;
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    private void AgentPageLoad()
    {
        try
        {
            string agentUser = AgentuserInfo.Username;
            var rec = db.tblAdminUsers.FirstOrDefault(x => x.UserName == agentUser);
            string name = rec == null ? "" : rec.Forename;

            if (name == string.Empty)
                name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase((AgentuserInfo.UserID == Guid.Empty ? AgentuserInfo.Username : "Agent").ToString().ToLower());
            AgentName = "Logout";
            lblAgent.Text = name;
            if (rec != null)
            {
                Guid InJob = Guid.Parse("5D3E09AB-AFDB-400D-A7C7-8DBF6F6A5F87");
                printingAgent.Visible = db.aspnet_Roles.FirstOrDefault(x => x.RoleId == rec.RoleID).IsPrintingAllow;
            }
            printingAgent.HRef = siteURL + "Printing/PrintingOrders.aspx";
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void ShowAd75Menu()
    {
        try
        {
            var objsite = db.tblSites.FirstOrDefault(x => x.ID == siteId);
            if (objsite == null)
                return;

            if ((bool)objsite.IsAgent)
                if (AgentuserInfo.UserID != Guid.Empty && !AgentuserInfo.IsAffliate)
                {
                    var AgentId = AgentuserInfo.UserID;
                    aFoclink.Visible = _oWebsitePage.IsFocAD75(AgentId);
                    aFoclink.HRef = siteURL + "?category=FOC-AD75";
                }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}