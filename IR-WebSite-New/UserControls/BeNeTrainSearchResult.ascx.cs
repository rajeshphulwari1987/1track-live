﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using OneHubServiceRef;
using Business;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using ExtensionMethods;
public partial class UserControls_BeNeTrainSearchResult : System.Web.UI.UserControl
{
    #region Global Variable
    public string pricePoup = "";
    public string journyTag = "";

    private int _index;
    List<TrainInformation> outboundList = null;
    List<TrainInformation> inboundList = null;
    List<OutTrainTimeDateBENEList> OutTrainTimeList = null;
    List<OutTrainTimeDateBENEList> OutTrainTimeList1 = null;

    List<InTrainTimeDateBENEList> InTrainTimeList = null;
    List<InTrainTimeDateBENEList> InTrainTimeList1 = null;
    bool IsOutTime;

    BookingRequestUserControl objBRUC;
    public string currency;
    public Guid currencyID = new Guid();
    Guid siteId;
    ManageBooking _masterBooking = new ManageBooking();
    ManageTrainDetails _master = new ManageTrainDetails();
    readonly Masters _objMaster = new Masters();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public static DateTime curentDateTime;
    public static List<getStationList_Result> list = new List<getStationList_Result>();
    public string _FromDate;
    public string _ToDate;
    List<P2PReservationIDInfo> lstP2PIdInfo = new List<P2PReservationIDInfo>();
    List<BookingRequestInfo> listBookingReqInfo = new List<BookingRequestInfo>();
    List<TrainPrice> listTrainPriceInfo = new List<TrainPrice>();
    Guid id = Guid.NewGuid();
    public string FromLatitude = string.Empty;
    public string FromLongitude = string.Empty;
    public string ToLatitude = string.Empty;
    public string ToLongitude = string.Empty;
    public string siteURL = string.Empty;
    ManageOneHub _ManageOneHub = new ManageOneHub();
    #endregion

    #region PageEvents
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            hdnReq.Value = Request.QueryString["req"] != null ? Request.QueryString["req"].Trim() : "";
            if (hdnReq.Value == "TI" || hdnReq.Value == "NTV" || hdnReq.Value == "EV")
                return;

            ShowMessage(0, null);
            GetCurrencyCode();
            NotIsPostBack();

            if (Session["ErrorMessage"] != null)
            {
                if (Session["ErrorMessage"].ToString().Trim() != "ErrorMaxDate")
                {
                    ShowMessage(2, Session["ErrorMessage"].ToString());
                    Session.Remove("ErrorMessage");
                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }
    #endregion

    void NotIsPostBack()
    {
        if (!IsPostBack)
        {
            try
            {
                hdnsiteURL.Value = new ManageFrontWebsitePage().GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
                Session["SHOPPINGCART"] = null;
                Session["BOOKING-REQUEST"] = null;
                if (Session["BookingUCRerq"] == null)
                    return;
                objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];

                hdnDateDepart.Value = objBRUC.depdt.ToString("dd/MMM/yyyy");
                hdnDateArr.Value = objBRUC.ReturnDate.Trim();

                ddlCountry.DataSource = _master.GetCountryDetail();
                ddlCountry.DataValueField = "CountryID";
                ddlCountry.DataTextField = "CountryName";
                ddlCountry.DataBind();


                if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
                {
                    var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
                    Guid CoutryID = Guid.Parse(cookie.Values["_cuntryId"]);
                    ddlCountry.SelectedValue = CoutryID.ToString().Trim();
                }

                BindResult();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
            currency = oManageClass.GetCurrency(currencyID);
            hdnCurrID.Value = currency;
        }
    }

    int GetEurostarBookingDays(BookingRequestUserControl request)
    {
        List<string> listStCode = new List<string> { "GBSPX", "BEBMI", "FRPNO", "FRPAR", "GBASI", "FRLIL", "FRLLE", "FRMLV", "GBEBF" };
        if (listStCode.Contains(request.depstCode) && listStCode.Contains(request.arrstCode))
            return 180;
        else
            return 0;
    }

    #region Earlier & Previous Train

    protected void lnkEarlier_Click(object sender, EventArgs e)
    {
        OutBoundSearch(-1);
    }

    protected void lnkLater_Click(object sender, EventArgs e)
    {
        OutBoundSearch(1);
    }

    void OutBoundSearch(int day)
    {
        if (Session["BookingUCRerq"] != null)
        {
            var objBruc = Session["BookingUCRerq"] as BookingRequestUserControl;
            string DeptDate = objBruc.depdt.ToString("dd/MMM/yyyy");
            string DeptTime = objBruc.depTime.ToString("HH");

            DropDownList ddldepTime = ucTrainSearch.FindControl("ddldepTime") as DropDownList;
            TextBox txtDepartureDate = ucTrainSearch.FindControl("txtDepartureDate") as TextBox;
            OutTrainTimeList = Session["OutTrainTimeDateBENEList"] as List<OutTrainTimeDateBENEList>;
            if (OutTrainTimeList != null && OutTrainTimeList.Count > 0)
            {
                if (day == 1)
                {
                    #region Later Train
                    int time = Convert.ToInt32(OutTrainTimeList.FirstOrDefault(x => x.Type == 1).DeptTme.Substring(0, 2)) + 1;
                    DeptTime = time.ToString().Length == 1 ? "0" + time.ToString() + ":00" : time.ToString() + ":00";
                    ddldepTime.SelectedValue = DeptTime;
                    objBruc.depTime = Convert.ToDateTime(DeptTime);
                    Session["BookingUCRerq"] = objBruc;
                    SearchTrainInfo();
                    BindResult();
                    var isreturn = ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.ToList().FirstOrDefault(x => x.IsReturn == false);
                    if ((isreturn == null || ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.Count() == 0) && objBruc != null)
                    {
                        if (!string.IsNullOrEmpty(DeptDate) && !DeptDate.Contains("DD/MM/YYYY"))
                        {
                            DateTime dt = Convert.ToDateTime(DeptDate);
                            dt = dt.AddDays(day);
                            DeptDate = dt.ToString("dd/MMM/yyyy");
                            txtDepartureDate.Text = DeptDate;
                            objBruc.depdt = DateTime.ParseExact(DeptDate, "dd/MMM/yyyy", null);

                            DeptTime = day == 1 ? "01:00" : "22:00";
                            ddldepTime.SelectedValue = DeptTime;
                            objBruc.depTime = Convert.ToDateTime(DeptTime);

                            Session["BookingUCRerq"] = objBruc;
                            SearchTrainInfo();
                            BindResult();
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Earlier Train
                    int time = 0;
                    int LessTime = 3;
                    int LastTime = Convert.ToInt32(OutTrainTimeList.FirstOrDefault(x => x.Type == 2).DeptTme.Substring(0, 2));
                    ViewState["OldTime"] = OutTrainTimeList.FirstOrDefault(x => x.Type == 2).DeptTme.Substring(0, 5);
                NotFoundTrian:
                    time = LastTime - LessTime;
                    if (time < 0)
                    {
                        time = LastTime = 23;
                        LessTime = 0;
                        if (!string.IsNullOrEmpty(DeptDate) && !DeptDate.Contains("DD/MM/YYYY"))
                        {
                            DateTime dt = Convert.ToDateTime(DeptDate);
                            dt = dt.AddDays(day);
                            DeptDate = dt.ToString("dd/MMM/yyyy");
                            txtDepartureDate.Text = DeptDate;
                            objBruc.depdt = DateTime.ParseExact(DeptDate, "dd/MMM/yyyy", null);
                        }
                    }
                    DeptTime = time.ToString().Length == 1 ? "0" + time.ToString() + ":00" : time.ToString() + ":00";
                    ddldepTime.SelectedValue = DeptTime;
                    objBruc.depTime = Convert.ToDateTime(DeptTime);
                    Session["BookingUCRerq"] = objBruc;
                    SearchTrainInfo();
                    BindResult();
                    OutTrainTimeList1 = Session["OutTrainTimeDateBENEList"] as List<OutTrainTimeDateBENEList>;
                    var isreturn = ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.ToList().FirstOrDefault(x => x.IsReturn == false);
                    if (OutTrainTimeList1 != null && OutTrainTimeList1.Count > 0 && ViewState["OldTime"] != null)
                    {
                        string newtime = OutTrainTimeList1.FirstOrDefault(x => x.Type == 2).DeptTme.Substring(0, 5);
                        if (newtime == ViewState["OldTime"].ToString())
                        {
                            LessTime = LessTime + 3;
                            goto NotFoundTrian;
                        }
                        else if ((isreturn == null || ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.Count() == 0) && objBruc != null)
                        {
                            DeptTime = day == 1 ? "01:00" : "22:00";
                            ddldepTime.SelectedValue = DeptTime;
                            objBruc.depTime = Convert.ToDateTime(DeptTime);
                            Session["BookingUCRerq"] = objBruc;
                            SearchTrainInfo();
                            BindResult();
                            var isreturnnew = ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.ToList().FirstOrDefault(x => x.IsReturn == false);
                            if ((isreturnnew == null || ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.Count() == 0) && objBruc != null)
                            {
                                LastTime = 22;
                                LessTime = LessTime + 3;
                                goto NotFoundTrian;
                            }
                        }
                    }
                    #endregion
                }
            }
            Response.Redirect("TrainResults.aspx?req=" + Request.QueryString["req"] + "");
        }
    }

    protected void lnkInEarlier_Click(object sender, EventArgs e)
    {
        InBoundSearch(-1);
    }

    protected void lnkInLater_Click(object sender, EventArgs e)
    {
        InBoundSearch(1);
    }

    void InBoundSearch(int day)
    {
        if (Session["BookingUCRerq"] != null)
        {
            var objBruc = Session["BookingUCRerq"] as BookingRequestUserControl;
            string ReturnDate = objBruc.ReturnDate;
            string ReturnTime = objBruc.ReturnTime.Substring(0, 2);

            DropDownList ddlReturnTime = ucTrainSearch.FindControl("ddlReturnTime") as DropDownList;
            TextBox txtReturnDate = ucTrainSearch.FindControl("txtReturnDate") as TextBox;
            InTrainTimeList = Session["InTrainTimeDateBENEList"] as List<InTrainTimeDateBENEList>;
            if (InTrainTimeList != null && InTrainTimeList.Count > 0)
            {
                if (day == 1)
                {
                    #region Later Train
                    int time = Convert.ToInt32(InTrainTimeList.FirstOrDefault(x => x.Type == 1).ReturnTme.Substring(0, 2)) + 1;
                    ReturnTime = time.ToString().Length == 1 ? "0" + time.ToString() + ":00" : time.ToString() + ":00";
                    ddlReturnTime.SelectedValue = ReturnTime;
                    objBruc.ReturnTime = ReturnTime;
                    Session["BookingUCRerq"] = objBruc;
                    SearchTrainInfo();
                    BindResult();
                    var isreturn = ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.ToList().FirstOrDefault(x => x.IsReturn == true);
                    if (((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.Count() > 0 && objBruc != null && isreturn == null)
                    {
                        if (!string.IsNullOrEmpty(ReturnDate) && !ReturnDate.Contains("DD/MM/YYYY"))
                        {
                            DateTime dt = Convert.ToDateTime(ReturnDate);
                            dt = dt.AddDays(day);
                            ReturnDate = dt.ToString("dd/MMM/yyyy");
                            txtReturnDate.Text = ReturnDate;
                            objBruc.ReturnDate = String.IsNullOrEmpty(ReturnDate) || ReturnDate.Trim() == "DD/MM/YYYY" ? string.Empty : ReturnDate;

                            ReturnTime = day == 1 ? "01:00" : "22:00";
                            ddlReturnTime.SelectedValue = ReturnTime;
                            objBruc.ReturnTime = ReturnTime;

                            Session["BookingUCRerq"] = objBruc;
                            SearchTrainInfo();
                            BindResult();
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Earlier Train
                    int time = 0;
                    int LessTime = 3;
                    int LastTime = Convert.ToInt32(InTrainTimeList.FirstOrDefault(x => x.Type == 2).ReturnTme.Substring(0, 2));
                    ViewState["OldTime"] = InTrainTimeList.FirstOrDefault(x => x.Type == 2).ReturnTme.Substring(0, 5);
                NotFoundTrian:
                    time = LastTime - LessTime;
                    if (time < 0)
                    {
                        time = LastTime = 23;
                        LessTime = 0;
                        if (!string.IsNullOrEmpty(ReturnDate) && !ReturnDate.Contains("DD/MM/YYYY"))
                        {
                            DateTime dt = Convert.ToDateTime(ReturnDate);
                            dt = dt.AddDays(day);
                            ReturnDate = dt.ToString("dd/MMM/yyyy");
                            txtReturnDate.Text = ReturnDate;
                            objBruc.ReturnDate = String.IsNullOrEmpty(ReturnDate) || ReturnDate.Trim() == "DD/MM/YYYY" ? string.Empty : ReturnDate;
                        }
                    }
                    ReturnTime = time.ToString().Length == 1 ? "0" + time.ToString() + ":00" : time.ToString() + ":00";
                    ddlReturnTime.SelectedValue = ReturnTime;
                    objBruc.ReturnTime = ReturnTime;
                    Session["BookingUCRerq"] = objBruc;
                    SearchTrainInfo();
                    BindResult();
                    InTrainTimeList1 = Session["InTrainTimeDateBENEList"] as List<InTrainTimeDateBENEList>;
                    var isreturn = ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.ToList().FirstOrDefault(x => x.IsReturn == true);
                    if (InTrainTimeList1 != null && InTrainTimeList1.Count > 0 && ViewState["OldTime"] != null)
                    {
                        string newtime = InTrainTimeList1.FirstOrDefault(x => x.Type == 2).ReturnTme.Substring(0, 5);
                        if (newtime == ViewState["OldTime"].ToString())
                        {
                            LessTime = LessTime + 3;
                            goto NotFoundTrian;
                        }
                        else if (((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.Count() > 0 && objBruc != null && isreturn == null)
                        {
                            ReturnTime = day == 1 ? "01:00" : "22:00";
                            ddlReturnTime.SelectedValue = ReturnTime;
                            objBruc.ReturnTime = ReturnTime;
                            Session["BookingUCRerq"] = objBruc;
                            SearchTrainInfo();
                            BindResult();
                            var isreturnnew = ((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.ToList().FirstOrDefault(x => x.IsReturn == true);
                            if (((OneHubServiceRef.TrainInformationResponse)(Session["TrainSearch"])).TrainInformationList.Count() > 0 && objBruc != null && isreturnnew == null)
                            {
                                LastTime = 22;
                                LessTime = LessTime + 3;
                                goto NotFoundTrian;
                            }
                        }
                    }
                    #endregion
                }
            }
            Response.Redirect("TrainResults.aspx?req=" + Request.QueryString["req"] + "");
        }
    }

    #endregion

    #region Make Booking Request

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        try
        {
            Session["IsFullPrice"] = null;
            Session["IsCheckout"] = null;
            Session["BOOKING-REQUEST"] = null;
            if (Session["P2POrderID"] != null)
            {
                new ManageUser().DeleteExistingP2PSale(Convert.ToInt64(Session["P2POrderID"].ToString()));
            }
            else
                Session["P2POrderID"] = new ManageBooking().CreateOrder(string.Empty, AgentuserInfo.UserID, Guid.Empty, siteId, "P2P", "0", "BeneTR");
            Session["ProductType"] = "P2P";

            Boolean res = false;

            if (!String.IsNullOrEmpty(hdnFromRid.Value))
                res = BookingRequestBeNe(hdnFromRid.Value, hdnFromsvcTyp.Value, hdnFromPriceId.Value, hdnFromBeNeClass.Value, "Inbound");
            if (!String.IsNullOrEmpty(hdnToRid.Value))
                res = BookingRequestBeNe(hdnToRid.Value, hdnTosvcTyp.Value, hdnToPriceId.Value, hdnToBeNeClass.Value, "Outbound");
            BookingRequestBeNeInboundOutbound();


            Session["P2PIdInfo"] = lstP2PIdInfo;

            if (res == true)
                Response.Redirect("P2PBookingCart.aspx?req=BE");
            else
                Response.Redirect("~/TrainResults", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public Boolean BookingRequestBeNe(string rid, string serviceTyoeId, string ProductGroup, string clas, string TypeOfJ)
    {
        try
        {
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
            List<BookingRequest> listBookingRequest = new List<BookingRequest>();

            if (pInfoSolutionsResponse != null)
            {
                TrainInformation trainInfo = pInfoSolutionsResponse.TrainInformationList.FirstOrDefault(x => x.Routesummaryid == rid);
                if (trainInfo != null)
                {
                    listBookingReqInfo.AddRange(trainInfo.BookingRequestInfo.Where(x => x.Productfeature.ProductGroup.Contains(ProductGroup)).Select(y => y).ToList());
                    List<PriceResponse> priceRes = trainInfo.PriceInfo.ToList();

                    if (priceRes != null)
                    {
                        List<TrainPrice> trainPricelist = new List<TrainPrice>();

                        foreach (var item in priceRes)
                        {
                            if (item.TrainPriceList != null)
                                trainPricelist.AddRange(item.TrainPriceList);
                        }
                        TrainPrice trainPriceInfo = trainPricelist.FirstOrDefault(x => x.ServiceTypeCode == serviceTyoeId && x.ProductGroup == ProductGroup);
                        listTrainPriceInfo.Add(trainPriceInfo);
                        var srcCurId = FrontEndManagePass.GetCurrencyID("EUB");

                        if (!trainPriceInfo.IsFullPrice && Session["IsFullPrice"] == null)
                            Session["IsFullPrice"] = trainPriceInfo.IsFullPrice;


                        #region Cart Information
                        List<ShoppingCartDetails> listCart = new List<ShoppingCartDetails> {new ShoppingCartDetails
                                {
                                    Id = id,
                                    DepartureStation = trainInfo.DepartureStationName,
                                    DepartureDate = trainInfo.DepartureDate,
                                    DepartureTime = trainInfo.DepartureTime,
                                    ArrivalStation = trainInfo.ArrivalStationName,
                                    ArrivalDate = trainInfo.ArrivalDate,
                                    ArrivalTime = trainInfo.ArrivalTime,
                                    TrainNo = trainInfo.TrainNumber,
                                    
                                    Title = ddlTitle.SelectedItem.Text ,
                                    FirstName = txtFirstname.Text ,
                                    LastName = txtLastname.Text,
                                    Passenger = trainPriceInfo!=null? trainPriceInfo.NumAdults + " Adults, " + trainPriceInfo.NumBoys + " Child," + trainPriceInfo.NumYouths + " Youth," + trainPriceInfo.NumSeniors + " Senior" :"",
                                    ServiceName = trainPriceInfo!=null?  trainPriceInfo.ServiceName :"",
                                    Fare = trainPriceInfo!=null? trainPriceInfo.TravelType :"",
                                    Price =  trainPriceInfo!=null ?( BusinessOneHub.IsNumeric(trainPriceInfo.MinPrice) ? GetTotalPriceBeNe(trainPriceInfo,false).ToString("F") : trainPriceInfo.MinPrice):"N/A",  
                                    netPrice = trainPriceInfo!=null?  Convert.ToDecimal(trainPriceInfo.MinPrice):0                                    
                                }};

                        long POrderID = Convert.ToInt64(Session["P2POrderID"]);
                        tblP2PSale objP2P = new tblP2PSale();
                        objP2P.From = trainInfo.DepartureStationName;
                        objP2P.DateTimeDepature = Convert.ToDateTime(trainInfo.DepartureDate);
                        objP2P.To = trainInfo.ArrivalStationName;
                        objP2P.DateTimeArrival = Convert.ToDateTime(trainInfo.ArrivalDate);
                        objP2P.TrainNo = trainInfo.TrainNumber;
                        objP2P.Passenger = trainPriceInfo != null ? trainPriceInfo.NumAdults + " Adults, " + trainPriceInfo.NumBoys + " Child," + trainPriceInfo.NumYouths + " Youth," + trainPriceInfo.NumSeniors + " Senior" : "";

                        objP2P.Adult = objBRUC.Adults.ToString();
                        objP2P.Children = objBRUC.Boys.ToString();
                        objP2P.Youth = objBRUC.Youths.ToString();
                        objP2P.Senior = objBRUC.Seniors.ToString();

                        objP2P.SeviceName = trainPriceInfo != null ? trainPriceInfo.ServiceName : "";
                        objP2P.FareName = trainPriceInfo != null ? trainPriceInfo.TravelType : "";
                        objP2P.Price = trainPriceInfo != null ? (BusinessOneHub.IsNumeric(trainPriceInfo.MinPrice) ? GetTotalPriceBeNe(trainPriceInfo, false) : 0) : 0;
                        objP2P.DepartureTime = trainInfo.DepartureTime;
                        objP2P.ArrivalTime = trainInfo.ArrivalTime;
                        objP2P.NetPrice = Convert.ToDecimal(GetRoundPrice(objP2P.Price.ToString()));
                        objP2P.Class = clas;
                        objP2P.CurrencyId = currencyID;
                        objP2P.Terms = trainPriceInfo.FareDescription;
                        objP2P.ApiPrice = BusinessOneHub.IsNumeric(trainPriceInfo.MinPrice) ? GetTotalOriginalPrice(trainPriceInfo) : 0;
                        objP2P.ApiName = "BENE";
                        objP2P.FromStation = objBRUC.FromDetail;
                        objP2P.ToStation = objBRUC.ToDetail;

                        if (clas == "1")
                            objP2P.Class = "1st Class";
                        else if (clas == "2")
                            objP2P.Class = "2nd Class";
                        else
                            objP2P.Class = clas;

                        if (Session["fareresponse"] != null)
                        {
                            var fareResponseList = System.Web.HttpContext.Current.Session["fareresponse"] as FareDetailsBene[];
                            if (fareResponseList != null && fareResponseList.Length > 0)
                            {
                                if (fareResponseList.Any(x => x.ServiceTypeCode == serviceTyoeId))
                                {
                                    string fareHtml = fareResponseList.FirstOrDefault(x => x.ServiceTypeCode == serviceTyoeId).fareCondition.ToString();
                                    string[] stringSeparators = new string[] { "<p>" };
                                    var resultHtml = fareHtml.Split(stringSeparators, StringSplitOptions.None);
                                    if (resultHtml.Length > 2)
                                        objP2P.Terms = "<p>" + resultHtml[1] + "<p>" + resultHtml[2];
                                }
                            }
                        }

                        //site currency//
                        objP2P.Site_USD = _masterBooking.GetCurrencyMultiplier("SUSD", Guid.Empty, POrderID);
                        objP2P.Site_SEU = _masterBooking.GetCurrencyMultiplier("SSEU", Guid.Empty, POrderID);
                        objP2P.Site_SBD = _masterBooking.GetCurrencyMultiplier("SSBD", Guid.Empty, POrderID);
                        objP2P.Site_GBP = _masterBooking.GetCurrencyMultiplier("SGBP", Guid.Empty, POrderID);
                        objP2P.Site_EUR = _masterBooking.GetCurrencyMultiplier("SEUR", Guid.Empty, POrderID);
                        objP2P.Site_INR = _masterBooking.GetCurrencyMultiplier("SINR", Guid.Empty, POrderID);
                        objP2P.Site_SEK = _masterBooking.GetCurrencyMultiplier("SSEK", Guid.Empty, POrderID);
                        objP2P.Site_NZD = _masterBooking.GetCurrencyMultiplier("SNZD", Guid.Empty, POrderID);
                        objP2P.Site_CAD = _masterBooking.GetCurrencyMultiplier("SCAD", Guid.Empty, POrderID);
                        objP2P.Site_JPY = _masterBooking.GetCurrencyMultiplier("SJPY", Guid.Empty, POrderID);
                        objP2P.Site_AUD = _masterBooking.GetCurrencyMultiplier("SAUD", Guid.Empty, POrderID);
                        objP2P.Site_CHF = _masterBooking.GetCurrencyMultiplier("SCHF", Guid.Empty, POrderID);
                        objP2P.Site_EUB = _masterBooking.GetCurrencyMultiplier("SEUB", Guid.Empty, POrderID);
                        objP2P.Site_EUT = _masterBooking.GetCurrencyMultiplier("SEUT", Guid.Empty, POrderID);
                        objP2P.Site_GBB = _masterBooking.GetCurrencyMultiplier("SGBB", Guid.Empty, POrderID);
                        objP2P.Site_THB = _masterBooking.GetCurrencyMultiplier("STHB", Guid.Empty, POrderID);
                        objP2P.Site_SGD = _masterBooking.GetCurrencyMultiplier("SSGD", Guid.Empty, POrderID);

                        objP2P.EUR_EUT = _masterBooking.GetCurrencyMultiplier("E_EUT", Guid.Empty, POrderID);
                        objP2P.EUR_EUB = _masterBooking.GetCurrencyMultiplier("E_EUB", Guid.Empty, POrderID);

                        Guid P2PID;
                        Guid LookupID = new ManageBooking().AddP2PBookingOrder(objP2P, Convert.ToInt64(Session["P2POrderID"]), out P2PID);
                        tblP2PSale objP2PRow = new ManageBooking().getP2PSingleRowData(P2PID);

                        P2PReservationIDInfo objP2PIdInfo = new P2PReservationIDInfo();
                        objP2PIdInfo.ID = P2PID;
                        objP2PIdInfo.P2PID = objP2PRow.P2PId;
                        objP2PIdInfo.JourneyType = TypeOfJ;
                        lstP2PIdInfo.Add(objP2PIdInfo);

                        Session["P2PID"] = P2PID;
                        tblOrderTraveller objTraveller = new tblOrderTraveller();
                        objTraveller.ID = Guid.NewGuid();
                        objTraveller.Title = ddlTitle.SelectedItem.Text;
                        objTraveller.FirstName = txtFirstname.Text;
                        objTraveller.LastName = txtLastname.Text;
                        objTraveller.Country = (ddlCountry.SelectedValue != "0" ? Guid.Parse(ddlCountry.SelectedValue) : Guid.Empty);
                        Session["objTravellerID"] = objTraveller.ID;
                        new ManageBooking().AddTraveller(objTraveller, LookupID);

                        if (Session["SHOPPINGCART"] != null)
                        {
                            List<ShoppingCartDetails> oldList = Session["SHOPPINGCART"] as List<ShoppingCartDetails>;
                            if (oldList != null) listCart.AddRange(oldList);
                        }
                        Session["SHOPPINGCART"] = listCart;
                        #endregion
                    }
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BookingRequestBeNeInboundOutbound()
    {
        List<Proposedprice> listPrpose = listBookingReqInfo.Select(item => new Proposedprice
        {
            ViaStations = item.ViaStations,
            DepartureStationCode = item.DepartureStationCode,
            ArrivalStationCode = item.ArrivalStationCode,
            DepartureStationName = item.DepartureStationName,
            ArrivalStationName = item.ArrivalStationName,
            Departuredate = Convert.ToDateTime(item.DepartureDate),
            Departuretime = Convert.ToDateTime(item.DepartureTime),
            Availability = item.Availability,
            Oneticketindicator = item.Oneticketindicator,
            TravelTimeDuration = item.TravelTimeDuration,
            Productfeature = new Productfeature
            {
                Comfortclass = item.Productfeature.Comfortclass,
                Ebs = item.Productfeature.Ebs,
                Esn = item.Productfeature.Esn,
                Ischangeable = item.Productfeature.Ischangeable,
                Isrefundable = item.Productfeature.Isrefundable,
                ProductGroup = item.Productfeature.ProductGroup,
                Singleindication = item.Productfeature.Singleindication,
                Supplier = item.Productfeature.Supplier,
                Value = item.Productfeature.Value
            },
            PossibleReservation = item.PossibleReservation,
            Proposedpriceid = item.Proposedpriceid,
            Routesummaryid = item.Routesummaryid,
            Segmentid = item.Segmentid,
            Segmenttype = item.Segmenttype,
            TrainPrice = listTrainPriceInfo != null ? listTrainPriceInfo.Select(trainPriceInfo => new TrainPrice
            {
                NumAdults = trainPriceInfo.NumAdults,
                NumBoys = trainPriceInfo.NumBoys,
                NumSeniors = trainPriceInfo.NumSeniors,
                NumYouths = trainPriceInfo.NumYouths,
                CurrencyCode = trainPriceInfo.CurrencyCode,
                Domain = trainPriceInfo.Domain,
                MinPrice = trainPriceInfo.MinPrice,
                ServiceCode = trainPriceInfo.ServiceCode,
                ServiceName = trainPriceInfo.ServiceName,
                ServiceTypeCode = trainPriceInfo.ServiceTypeCode,
                ProductionModeList = trainPriceInfo.ProductionModeList
            }).ToArray() : null,
            Trainnumber = item.TrainNumber,
            PriceOffer = item.PriceOffer
        }).ToList();

        List<Passengerreply> listPreply = new List<Passengerreply>();
        foreach (var item2 in listBookingReqInfo.FirstOrDefault().PassengerListReply)
        {
            listPreply.Add(new Passengerreply
            {
                Loyaltycard = objBRUC.lstLoyalty != null ? objBRUC.lstLoyalty.ToArray() : null,
                Passengerid = item2.Passengerid,
                SeatPreference = SeatPreference.S,
                FirstName = txtFirstname.Text,
                LastName = txtLastname.Text,
            });
        }
        var request = new PurchasingForServiceRequest
        {
            Header = new Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = 0,
                language = Language.nl_BE,
            },
            PassengerId = listTrainPriceInfo != null ? listTrainPriceInfo.FirstOrDefault().PassengerId : null,
            PassengerListReply = listPreply.Distinct().ToArray(),
            Ticketlanguage = new BookinginfoTicketlanguage
            {
                Language = Language.nl_BE
            },
            BookingRequestList = listPrpose.ToArray()
        };

        BookingRequest bkrequest = new BookingRequest { PurchasingForServiceRequest = request, Id = id };

        List<BookingRequest> listBookingRequest = new List<BookingRequest>();
        listBookingRequest.Add(bkrequest);
        Session["BOOKING-REQUEST"] = listBookingRequest;

    }

    #endregion

    public void BindResult()
    {
        TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
        if (pInfoSolutionsResponse.TrainInformationList == null)
            return;
        objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];

        //Longitude and Latitude
        FromLatitude = string.IsNullOrEmpty(_objMaster.GetLongitudeLatitude(objBRUC.depstCode.Trim(), "BENE").Latitude) ? "" : _objMaster.GetLongitudeLatitude(objBRUC.depstCode.Trim(), "BENE").Latitude;
        FromLongitude = string.IsNullOrEmpty(_objMaster.GetLongitudeLatitude(objBRUC.depstCode.Trim(), "BENE").Longitude) ? "" : _objMaster.GetLongitudeLatitude(objBRUC.depstCode.Trim(), "BENE").Longitude;
        ToLatitude = string.IsNullOrEmpty(_objMaster.GetLongitudeLatitude(objBRUC.arrstCode.Trim(), "BENE").Latitude) ? "" : _objMaster.GetLongitudeLatitude(objBRUC.arrstCode.Trim(), "BENE").Latitude;
        ToLongitude = string.IsNullOrEmpty(_objMaster.GetLongitudeLatitude(objBRUC.arrstCode.Trim(), "BENE").Longitude) ? "" : _objMaster.GetLongitudeLatitude(objBRUC.arrstCode.Trim(), "BENE").Longitude;

        //Search JourneyBlock-header
        ltrinTo.Text = ltroutFrom.Text = objBRUC.FromDetail;
        ltrinFrom.Text = ltroutTo.Text = objBRUC.ToDetail;
        ltrinDate.Text = string.IsNullOrEmpty(objBRUC.ReturnDate) ? objBRUC.depdt.ToString("dd-MM-yyyy") : Convert.ToDateTime(objBRUC.ReturnDate).ToString("dd-MM-yyyy");
        ltroutDate.Text = objBRUC.depdt.ToString("dd-MM-yyyy");
        ltrPassenger.Text = objBRUC.Adults.ToString() + " Adult, " + objBRUC.Boys.ToString() + " Child, " + objBRUC.Youths.ToString() + " Youth and " + objBRUC.Seniors.ToString() + " Senior";


        if (pInfoSolutionsResponse.TrainInformationList.ToList().Any(x => x.IsReturn))
        {
            int ReturnJorneyStartIndex = pInfoSolutionsResponse.TrainInformationList.ToList().FindIndex(x => x.IsReturn);
            outboundList = pInfoSolutionsResponse.TrainInformationList.ToList().GetRange(0, ReturnJorneyStartIndex).Where(x => x.PriceInfo.Any(y => y.TrainPriceList.Any())).OrderBy(x => Convert.ToDateTime(x.DepartureDate)).ThenBy(x => Convert.ToDateTime(x.DepartureTime)).ToList();
            inboundList = pInfoSolutionsResponse.TrainInformationList.ToList().GetRange(ReturnJorneyStartIndex, pInfoSolutionsResponse.TrainInformationList.ToList().Count - ReturnJorneyStartIndex).Where(x => x.PriceInfo.Any(y => y.TrainPriceList.Any())).OrderBy(x => Convert.ToDateTime(x.DepartureDate)).ThenBy(x => Convert.ToDateTime(x.DepartureTime)).ToList();
        }
        else
            outboundList = pInfoSolutionsResponse.TrainInformationList.Where(x => x.PriceInfo.Any(y => y.TrainPriceList.Any())).OrderBy(x => Convert.ToDateTime(x.DepartureDate)).ThenBy(x => Convert.ToDateTime(x.DepartureTime)).ToList();

        ShowInboud.Visible = false;
        if (inboundList != null)
        {
            ShowInboud.Visible = (inboundList.Count > 0);
            journyTag = (inboundList.Count > 0) ? "Return Journey" : "Booking Details";
        }
        else
            journyTag = "Booking Details";

        List<Taco> listtaco = pInfoSolutionsResponse.TrainInformationList.SelectMany(a => a.PriceInfo.SelectMany(x => x.TrainPriceList.Select(t => new Taco { Domain = t.Domain, Tariffgroup = t.ServiceCode, Tic = t.ServiceTypeCode, Value = t.ServiceName }).ToList())).ToList();
        Session["farerequest"] = GetFareRequest(listtaco);
        ScriptManager.RegisterStartupScript(Page, GetType(), "getfare", " getfare();", true);

        _index = 0;
        rptoutBeNe.DataSource = outboundList;
        rptoutBeNe.DataBind();

        _index = 0;
        rptinBeNe.DataSource = inboundList;
        rptinBeNe.DataBind();

        #region OutBound Earlier later train Serach
        if (outboundList != null && outboundList.Count > 0)
        {
            var LastItemList = outboundList.OrderByDescending(x => Convert.ToDateTime(x.DepartureDate)).ThenByDescending(x => Convert.ToDateTime(x.DepartureTime)).Take(1).ToList();
            var FirstItemList = outboundList.Take(1).ToList();
            OutTrainTimeList = new List<OutTrainTimeDateBENEList>();
            Session["OutTrainTimeDateBENEList"] = null;
            if (LastItemList != null && LastItemList.Count > 0)
            {
                foreach (var item in LastItemList)
                {
                    OutTrainTimeList.Add(new OutTrainTimeDateBENEList()
                    {
                        DeptDate = item.DepartureDate,
                        DeptTme = item.DepartureTime,
                        Type = 1
                    });
                }
            }
            if (FirstItemList != null && FirstItemList.Count > 0)
            {
                foreach (var item in FirstItemList)
                {
                    OutTrainTimeList.Add(new OutTrainTimeDateBENEList()
                    {
                        DeptDate = item.DepartureDate,
                        DeptTme = item.DepartureTime,
                        Type = 2
                    });
                }
            }
            Session["OutTrainTimeDateBENEList"] = OutTrainTimeList;
        }
        #endregion

        #region InBound Earlier later train Serach
        if (inboundList != null && inboundList.Count > 0)
        {
            var LastItemList = inboundList.OrderByDescending(x => Convert.ToDateTime(x.DepartureDate)).ThenByDescending(x => Convert.ToDateTime(x.DepartureTime)).Take(1).ToList();
            var FirstItemList = inboundList.Take(1).ToList();
            InTrainTimeList = new List<InTrainTimeDateBENEList>();
            Session["InTrainTimeDateBENEList"] = null;
            if (LastItemList != null && LastItemList.Count > 0)
            {
                foreach (var item in LastItemList)
                {
                    InTrainTimeList.Add(new InTrainTimeDateBENEList()
                    {
                        ReturnDate = item.DepartureDate,
                        ReturnTme = item.DepartureTime,
                        Type = 1
                    });
                }
            }
            if (FirstItemList != null && FirstItemList.Count > 0)
            {
                foreach (var item in FirstItemList)
                {
                    InTrainTimeList.Add(new InTrainTimeDateBENEList()
                    {
                        ReturnDate = item.DepartureDate,
                        ReturnTme = item.DepartureTime,
                        Type = 2
                    });
                }
            }
            Session["InTrainTimeDateBENEList"] = InTrainTimeList;
        }
        #endregion
    }

    protected void rptoutBeNe_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        TrainInformationResponse pInfoSolutionsResponse = null;
        if (outboundList != null)
            pInfoSolutionsResponse = new TrainInformationResponse
            {
                TrainInformationList = outboundList.ToArray()
            };

        if (outboundList != null && _index == outboundList.Count())
            return;

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (pInfoSolutionsResponse != null)
            {
                Repeater GrdRouteInfo = e.Item.FindControl("GrdRouteInfo") as Repeater;
                Label lblTrainChanges = e.Item.FindControl("lblTrainChanges") as Label;
                Label lblTrainChangesMobile = e.Item.FindControl("lblTrainChangesMobile") as Label;
                Label lblCheapPrice = e.Item.FindControl("lblCheapPrice") as Label;
                DropDownList ddlPrice = e.Item.FindControl("ddlFromPrice") as DropDownList;
                HyperLink hlnkContinue = e.Item.FindControl("hlnkContinue") as HyperLink;
                Literal ltrFrom = e.Item.FindControl("ltrFrom") as Literal;
                Literal ltrTo = e.Item.FindControl("ltrTo") as Literal;

                var listTrainInfo = pInfoSolutionsResponse.TrainInformationList[_index];
                if (GrdRouteInfo != null)
                {
                    ltrFrom.Text = listTrainInfo.TrainInfoSegment.FirstOrDefault().DepartureStationName.ToMyLowerCase();
                    ltrTo.Text = listTrainInfo.TrainInfoSegment.LastOrDefault().ArrivalStationName.ToMyLowerCase();
                    GrdRouteInfo.DataSource = listTrainInfo.TrainInfoSegment.ToList();
                    GrdRouteInfo.DataBind();
                }
                if (listTrainInfo.TrainInfoSegment.ToList().Count() > 0)
                    lblTrainChangesMobile.Text = lblTrainChanges.Text = listTrainInfo.TrainInfoSegment.ToList().Count() == 1 ? "Direct" : ((listTrainInfo.TrainInfoSegment.ToList().Count() - 1).ToString() + " Change");

                List<TrainPrice> pricelist = new List<TrainPrice>();
                foreach (var item in pInfoSolutionsResponse.TrainInformationList[_index].PriceInfo.ToList())
                    if (item.TrainPriceList != null)
                        pricelist.AddRange(item.TrainPriceList);

                HtmlGenericControl divTrn = e.Item.FindControl("DivTr") as HtmlGenericControl;
                string classDiv = "";
                string priceDiv = "";
                pricelist.Where(x => x != null).ToList();
                if (pricelist.Count > 0)
                {
                    var cheapPrice = Convert.ToDecimal(pricelist.FirstOrDefault().MinPrice);
                    decimal cheapestPrice = GetCheapestPriceBeNe(cheapPrice, false);
                    lblCheapPrice.Text = currency.ToString() + cheapestPrice.ToString();
                }
                var lstnewCls = pricelist.Where(a => a.Class == "1");
                if (lstnewCls != null && (lstnewCls.Count() > 0))
                {
                    classDiv = "<div class='starail-JourneyBlock-class'><p>1st Class</p>";
                    foreach (var itemp in lstnewCls)
                    {
                        TrainPrice trainPrice = lstnewCls.FirstOrDefault(x => x.ProductGroup == itemp.ProductGroup);
                        if (trainPrice != null)
                        {
                            var srcCurId = FrontEndManagePass.GetCurrencyID("EUB");
                            if (trainPrice.MinPrice.Contains("Sold"))
                                priceDiv += "<div class='starail-JourneyBlock-radioRow'><label><span class='starail-Form-fancyRadioGroup'> Sold out <span><i></i></span></span></label></div> ";

                            decimal totalprice = GetTotalPriceBeNe(trainPrice, false);

                            string text = "1st Class " + itemp.ServiceName + " : " + currency.ToString() + totalprice.ToString();
                            string value = itemp.Routesummaryid + "," + itemp.ServiceTypeCode + "," + itemp.ProductGroup + "," + itemp.Class + "," + totalprice;
                            ddlPrice.Items.Add(new ListItem(text, value));
                            hlnkContinue.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + value + ",outbound" + "');");
                            anchor_Next.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + value + ",outbound" + "');");

                            priceDiv += "<div class='starail-JourneyBlock-radioRow'><label> <span class='starail-Form-fancyRadioGroup'>";
                            priceDiv += "<input type='radio' id='rdoFrom' name='starail-ticket-outbound' value='" + itemp.Routesummaryid + "," + itemp.ServiceTypeCode + "," + itemp.ProductGroup + "," + itemp.Class + "," + totalprice + "'> ";
                            priceDiv += itemp.ServiceName + currency + totalprice;
                            priceDiv += "<span><i></i></span></span> &nbsp;</label><a href='#' class='js-lightboxOpen' data-lightbox-id='starail-ticket-info" + itemp.ProductGroup + "'><i class='starail-Icon-question'></i></a> </div> ";

                            pricePoup += "<div class='starail-Lightbox'id='starail-ticket-info" + itemp.ProductGroup + "'><div class='starail-Lightbox-content'><div class='starail-Lightbox-closeContainer'><a href='#' class='starail-Lightbox-close js-lightboxClose'><i class='starail-Icon starail-Icon-close'></i></a>";
                            pricePoup += "</div>  <div class='starail-Lightbox-content-inner'> <div class='starail-Lightbox-text'> <h3 class='starail-Lightbox-textTitle'>   Ticket Info</h3>";
                            pricePoup += "<span  class='tresult'>" + trainPrice.FareDescription + "<p class='" + itemp.ServiceTypeCode + "'><img src='images/fareloading.gif' style='position: static;padding-left: 32%'/></p>" + GetPriceTable(trainPrice, false) + "</span>";
                            pricePoup += "<span  class='tresult' style='color:red;'>" + (trainPrice.IsFullPrice == true ? "" : "<b>Note: </b>This is not valid price, the valid price will only be shown at the next step.") + "</span>";
                            pricePoup += "</div></div></div></div>";

                        }
                    }
                }
                else if (pricelist.Count() == 0)
                    priceDiv = "<div class='starail-JourneyBlock-radioRow'><label><span class='starail-Form-fancyRadioGroup'>Sold out<span><i></i></span></span></label></div> ";
                if (lstnewCls != null && (lstnewCls.Count() > 0))
                {
                    classDiv += priceDiv;
                    classDiv += "</div>";
                }

                //---2nd class
                string classDiv2 = "";
                string priceDiv2 = "";
                pricelist.Where(x => x != null).ToList();
                lstnewCls = pricelist.Where(a => a.Class == "2");

                if (lstnewCls != null && (lstnewCls.Count() > 0))
                {
                    classDiv2 = "<div class='starail-JourneyBlock-class'><p>2nd Class</p>";
                    foreach (var itemp in lstnewCls)
                    {
                        TrainPrice trainPrice = lstnewCls.FirstOrDefault(x => x.ProductGroup == itemp.ProductGroup);
                        if (trainPrice != null)
                        {
                            var srcCurId = FrontEndManagePass.GetCurrencyID("EUB");
                            if (trainPrice.MinPrice.Contains("Sold"))
                                priceDiv2 += "<div class='starail-JourneyBlock-radioRow'><label><span class='starail-Form-fancyRadioGroup'> Sold out <span><i></i></span></span></label></div> ";

                            decimal totalprice = GetTotalPriceBeNe(trainPrice, false);

                            string text = "2nd Class " + itemp.ServiceName + " : " + currency.ToString() + totalprice.ToString();
                            string value = itemp.Routesummaryid + "," + itemp.ServiceTypeCode + "," + itemp.ProductGroup + "," + itemp.Class + "," + totalprice;
                            ddlPrice.Items.Add(new ListItem(text, value));
                            hlnkContinue.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + value + ",outbound" + "');");
                            anchor_Next.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + value + ",outbound" + "');");
                            priceDiv2 += "<div class='starail-JourneyBlock-radioRow'><label> <span class='starail-Form-fancyRadioGroup'>";
                            priceDiv2 += "<input type='radio' id='rdoFrom' name='starail-ticket-outbound' value='" + itemp.Routesummaryid + "," + itemp.ServiceTypeCode + "," + itemp.ProductGroup + "," + itemp.Class + "," + totalprice + "'> ";
                            priceDiv2 += itemp.ServiceName + currency + totalprice;
                            priceDiv2 += "<span><i></i></span></span> &nbsp;</label><a href='#' class='js-lightboxOpen' data-lightbox-id='starail-ticket-info" + itemp.ProductGroup + "'><i class='starail-Icon-question'></i></a> </div> ";

                            pricePoup += "<div class='starail-Lightbox'id='starail-ticket-info" + itemp.ProductGroup + "'><div class='starail-Lightbox-content'><div class='starail-Lightbox-closeContainer'><a href='#' class='starail-Lightbox-close js-lightboxClose'><i class='starail-Icon starail-Icon-close'></i></a>";
                            pricePoup += "</div>  <div class='starail-Lightbox-content-inner'> <div class='starail-Lightbox-text'> <h3 class='starail-Lightbox-textTitle'>   Ticket Info</h3>";
                            pricePoup += "<span  class='tresult'>" + trainPrice.FareDescription + "<p class='" + itemp.ServiceTypeCode + "'><img src='images/fareloading.gif' style='position: static;padding-left: 32%'/></p>" + GetPriceTable(trainPrice, false) + "</span>";
                            pricePoup += "<span  class='tresult' style='color:red;'>" + (trainPrice.IsFullPrice == true ? "" : "<b>Note: </b>This is not valid price, the valid price will only be shown at the next step.") + "</span>";
                            pricePoup += "</div></div></div></div>";

                        }
                    }
                }
                else if (pricelist.Count() == 0)
                    priceDiv2 = "<div class='starail-JourneyBlock-radioRow'><label><span class='starail-Form-fancyRadioGroup'>Sold out<span><i></i></span></span></label></div> ";

                if (lstnewCls != null && (lstnewCls.Count() > 0))
                {
                    classDiv2 += priceDiv2;
                    classDiv2 += "</div>";
                }

                divTrn.InnerHtml = classDiv2 + classDiv;
            }
            _index++;
        }
    }

    protected void rptinBeNe_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {


        TrainInformationResponse pInfoSolutionsResponse = null;
        if (inboundList != null)
            pInfoSolutionsResponse = new TrainInformationResponse
            {
                TrainInformationList = inboundList.ToArray()
            };

        if (inboundList != null && _index == inboundList.Count())
            return;

        if (pInfoSolutionsResponse != null && _index == pInfoSolutionsResponse.TrainInformationList.Count())
            return;

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (pInfoSolutionsResponse != null)
            {
                Label lblTrainChanges = e.Item.FindControl("lblTrainChanges") as Label;
                Label lblTrainChangesMobile = e.Item.FindControl("lblTrainChangesMobile") as Label;
                Label lblCheapPrice = e.Item.FindControl("lblCheapPrice") as Label;
                HyperLink hlnkContinue = e.Item.FindControl("hlnkContinue") as HyperLink;
                DropDownList ddlPrice = e.Item.FindControl("ddlToPrice") as DropDownList;
                Repeater GrdRouteInfo = e.Item.FindControl("GrdRouteInfo") as Repeater;
                Literal ltrFrom = e.Item.FindControl("ltrFrom") as Literal;
                Literal ltrTo = e.Item.FindControl("ltrTo") as Literal;

                var listTrainInfo = pInfoSolutionsResponse.TrainInformationList[_index];
                if (GrdRouteInfo != null)
                {
                    ltrFrom.Text = listTrainInfo.TrainInfoSegment.FirstOrDefault().DepartureStationName.ToMyLowerCase(); ;
                    ltrTo.Text = listTrainInfo.TrainInfoSegment.LastOrDefault().ArrivalStationName.ToMyLowerCase();
                    GrdRouteInfo.DataSource = listTrainInfo.TrainInfoSegment.ToList();
                    GrdRouteInfo.DataBind();
                }

                if (listTrainInfo.TrainInfoSegment.ToList().Count() > 0)
                    lblTrainChangesMobile.Text = lblTrainChanges.Text = listTrainInfo.TrainInfoSegment.ToList().Count() == 1 ? "Direct" : ((listTrainInfo.TrainInfoSegment.ToList().Count() - 1).ToString() + " Change");


                List<TrainPrice> pricelist = new List<TrainPrice>();
                foreach (var item in pInfoSolutionsResponse.TrainInformationList[_index].PriceInfo.ToList())
                    if (item.TrainPriceList != null)
                        pricelist.AddRange(item.TrainPriceList);

                HtmlGenericControl divTrn = e.Item.FindControl("DivTr") as HtmlGenericControl;
                string classDiv = "";
                string priceDiv = "";
                pricelist.Where(x => x != null).ToList();
                if (pricelist.Count > 0)
                {
                    var cheapPrice = Convert.ToDecimal(pricelist.FirstOrDefault().MinPrice);
                    decimal cheapestPrice = GetCheapestPriceBeNe(cheapPrice, false);
                    lblCheapPrice.Text = currency.ToString() + cheapestPrice.ToString();
                }
                var lstnewCls = pricelist.Where(a => a.Class == "1");
                if (lstnewCls != null && (lstnewCls.Count() > 0))
                {
                    classDiv = "<div class='starail-JourneyBlock-class'><p>1st Class</p>";
                    foreach (var itemp in lstnewCls)
                    {
                        TrainPrice trainPrice = lstnewCls.FirstOrDefault(x => x.ProductGroup == itemp.ProductGroup);
                        if (trainPrice != null)
                        {
                            var srcCurId = FrontEndManagePass.GetCurrencyID("EUB");
                            if (trainPrice.MinPrice.Contains("Sold"))
                                priceDiv += "<div class='starail-JourneyBlock-radioRow'><label><span class='starail-Form-fancyRadioGroup'> Sold out <span><i></i></span></span></label></div> ";

                            decimal totalprice = GetTotalPriceBeNe(trainPrice, false);

                            string text = "1st Class " + itemp.ServiceName + " : " + currency.ToString() + totalprice.ToString();
                            string value = itemp.Routesummaryid + "," + itemp.ServiceTypeCode + "," + itemp.ProductGroup + "," + itemp.Class + "," + totalprice;
                            ddlPrice.Items.Add(new ListItem(text, value));
                            hlnkContinue.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + value + ",inbound" + "');");
                            anchor_Next.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + value + ",inbound" + "');");
                            priceDiv += "<div class='starail-JourneyBlock-radioRow'><label> <span class='starail-Form-fancyRadioGroup'>";
                            priceDiv += "<input type='radio' id='rdoTo' name='starail-ticket-inbound' value='" + itemp.Routesummaryid + "," + itemp.ServiceTypeCode + "," + itemp.ProductGroup + "," + itemp.Class + "," + totalprice + "'> ";
                            priceDiv += itemp.ServiceName + currency + totalprice;
                            priceDiv += "<span><i></i></span></span> &nbsp;</label><a href='#' class='js-lightboxOpen' data-lightbox-id='starail-ticket-info" + itemp.ProductGroup + "'><i class='starail-Icon-question'></i></a> </div> ";

                            priceDiv += "<div class='starail-Lightbox'id='starail-ticket-info" + itemp.ProductGroup + "'><div class='starail-Lightbox-content'><div class='starail-Lightbox-closeContainer'><a href='#' class='starail-Lightbox-close js-lightboxClose'><i class='starail-Icon starail-Icon-close'></i></a>";
                            priceDiv += "</div>  <div class='starail-Lightbox-content-inner'> <div class='starail-Lightbox-text'> <h3 class='starail-Lightbox-textTitle'>   Ticket Info</h3>";
                            priceDiv += "<span  class='tresult'>" + trainPrice.FareDescription + "<p class='" + itemp.ServiceTypeCode + "'><img src='images/fareloading.gif' style='position: static;padding-left: 32%'/></p>" + GetPriceTable(trainPrice, false) + "</span>";
                            pricePoup += "<span  class='tresult' style='color:red;'>" + (trainPrice.IsFullPrice == true ? "" : "<b>Note: </b>This is not valid price, the valid price will only be shown at the next step.") + "</span>";
                            priceDiv += "</div></div></div></div>";

                        }
                    }
                }
                else if (pricelist.Count() == 0)
                    priceDiv = "<div class='starail-JourneyBlock-radioRow'><label><span class='starail-Form-fancyRadioGroup'>Sold out<span><i></i></span></span></label></div> ";
                if (lstnewCls != null && (lstnewCls.Count() > 0))
                {
                    classDiv += priceDiv;
                    classDiv += "</div>";
                }

                //---2nd class
                string classDiv2 = "";
                string priceDiv2 = "";
                pricelist.Where(x => x != null).ToList();
                lstnewCls = pricelist.Where(a => a.Class == "2");

                if (lstnewCls != null && (lstnewCls.Count() > 0))
                {
                    classDiv2 = "<div class='starail-JourneyBlock-class'><p>2nd Class</p>";
                    foreach (var itemp in lstnewCls)
                    {
                        TrainPrice trainPrice = lstnewCls.FirstOrDefault(x => x.ProductGroup == itemp.ProductGroup);
                        if (trainPrice != null)
                        {
                            var srcCurId = FrontEndManagePass.GetCurrencyID("EUB");
                            if (trainPrice.MinPrice.Contains("Sold"))
                                priceDiv2 += "<div class='starail-JourneyBlock-radioRow'><label><span class='starail-Form-fancyRadioGroup'> Sold out <span><i></i></span></span></label></div> ";

                            decimal totalprice = GetTotalPriceBeNe(trainPrice, false);

                            string text = "2nd Class " + itemp.ServiceName + " : " + currency.ToString() + totalprice.ToString();
                            string value = itemp.Routesummaryid + "," + itemp.ServiceTypeCode + "," + itemp.ProductGroup + "," + itemp.Class + "," + totalprice;
                            ddlPrice.Items.Add(new ListItem(text, value));
                            hlnkContinue.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + value + ",inbound" + "');");
                            anchor_Next.Attributes.Add("onclick", "return SendJCodeAndServiceIDMobile('" + value + ",inbound" + "');");

                            priceDiv2 += "<div class='starail-JourneyBlock-radioRow'><label> <span class='starail-Form-fancyRadioGroup'>";
                            priceDiv2 += "<input type='radio' id='rdoTo' name='starail-ticket-inbound' value='" + itemp.Routesummaryid + "," + itemp.ServiceTypeCode + "," + itemp.ProductGroup + "," + itemp.Class + "," + totalprice + "'> ";
                            priceDiv2 += itemp.ServiceName + currency + totalprice;

                            priceDiv2 += "<span><i></i></span></span> &nbsp;</label><a href='#' class='js-lightboxOpen' data-lightbox-id='starail-ticket-info" + itemp.ProductGroup + "'><i class='starail-Icon-question'></i></a> </div> ";
                            priceDiv2 += "<div class='starail-Lightbox'id='starail-ticket-info" + itemp.ProductGroup + "'><div class='starail-Lightbox-content'><div class='starail-Lightbox-closeContainer'><a href='#' class='starail-Lightbox-close js-lightboxClose'><i class='starail-Icon starail-Icon-close'></i></a>";
                            priceDiv2 += "</div>  <div class='starail-Lightbox-content-inner'> <div class='starail-Lightbox-text'> <h3 class='starail-Lightbox-textTitle'>   Ticket Info</h3>";
                            priceDiv2 += "<span  class='tresult'>" + trainPrice.FareDescription + "<p class='" + itemp.ServiceTypeCode + "'><img src='images/fareloading.gif' style='position: static;padding-left: 32%'/></p>" + GetPriceTable(trainPrice, false) + "</span>";
                            pricePoup += "<span  class='tresult' style='color:red;'>" + (trainPrice.IsFullPrice == true ? "" : "<b>Note: </b>This is not valid price, the valid price will only be shown at the next step.") + "</span>";
                            priceDiv2 += "</div></div></div></div>";

                        }
                    }
                }
                else if (pricelist.Count() == 0)
                    priceDiv2 = "<div class='starail-JourneyBlock-radioRow'><label><span class='starail-Form-fancyRadioGroup'>Sold out<span><i></i></span></span></label></div> ";

                if (lstnewCls != null && (lstnewCls.Count() > 0))
                {
                    classDiv2 += priceDiv2;
                    classDiv2 += "</div>";
                }

                divTrn.InnerHtml = classDiv2 + classDiv;
            }
            _index++;
        }
    }

    #region Price Calculation
    public string GetPriceTable(TrainPrice tp, bool isTrenItalia)
    {
        var srcCurId = FrontEndManagePass.GetCurrencyID(isTrenItalia ? "EUT" : "EUB");
        string priceTable = "<table width='100%' class='tariffPriceTable' cellpadding='0' cellspacing='0'> <tbody><tr> <th colspan='3'>Price Table</th> </tr>";
        if (Convert.ToDecimal(tp.PerUnitAdultPrice) > 0)
        {
            string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitAdultPrice), siteId, srcCurId, currencyID).ToString("F");
            priceTable += "<tr class='cellWhite'><td>Adults </td> <td> <span class='pound'> " + currency + " " + GetRoundPrice(price) + " each </span>   </td> </tr>";
        }

        if (Convert.ToDecimal(tp.PerUnitChildPrice) > 0)
        {
            string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitChildPrice), siteId, srcCurId, currencyID).ToString("F");
            priceTable += "<tr class='cellWhite'><td>Childs </td> <td> <span class='pound'> " + currency + " " + GetRoundPrice(price) + " each </span>   </td> </tr>";
        }

        if (Convert.ToDecimal(tp.PerUnitYouthPrice) > 0)
        {
            string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitYouthPrice), siteId, srcCurId, currencyID).ToString("F");
            priceTable += "<tr class='cellWhite'><td>Youths </td> <td> <span class='pound'> " + currency + " " + GetRoundPrice(price) + " each </span>   </td> </tr>";
        }
        if (Convert.ToDecimal(tp.PerUnitSeniorPrice) > 0)
        {
            string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitSeniorPrice), siteId, srcCurId, currencyID).ToString("F");
            priceTable += "<tr class='cellWhite'><td>Seniors </td> <td> <span class='pound'> " + currency + " " + GetRoundPrice(price) + " each </span>   </td> </tr>";
        }

        if (!isTrenItalia)
            foreach (var item in tp.SeatReservationDetail)
            {
                priceTable += "<tr class='cellWhite' style='background:#dfdfdf'><td><b>" + item.Supplier + "</b>  " + item.Journey + " </td> <td> <span class='pound'>" + item.ReservationStatus + "  </span>   </td> </tr>";
            }
        priceTable += "</tbody></table>";
        return priceTable;
    }

    public Decimal GetTotalPriceBeNe(TrainPrice tp, bool isTrenItalia)
    {
        var srcCurId = FrontEndManagePass.GetCurrencyID(isTrenItalia ? "EUT" : "EUB");
        Decimal price = 0;
        if (Convert.ToDecimal(tp.MinPrice) > 0)
        {
            string MinPrice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.MinPrice), siteId, srcCurId, currencyID).ToString("F");
            price = Convert.ToDecimal(GetRoundPrice(MinPrice));
        }
        return price;
    }

    public Decimal GetCheapestPriceBeNe(decimal CheapPrice, bool isTrenItalia)
    {
        var srcCurId = FrontEndManagePass.GetCurrencyID(isTrenItalia ? "EUT" : "EUB");
        if (CheapPrice > 0)
        {
            string MinPrice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(CheapPrice), siteId, srcCurId, currencyID).ToString("F");
            CheapPrice = Convert.ToDecimal(GetRoundPrice(MinPrice));
        }
        return CheapPrice;
    }

    public Decimal GetTotalPrice(TrainPrice tp, bool isTrenItalia)
    {
        var srcCurId = FrontEndManagePass.GetCurrencyID(isTrenItalia ? "EUT" : "EUB");
        Decimal price = 0;
        if (Convert.ToDecimal(tp.PerUnitAdultPrice) > 0)
        {
            string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitAdultPrice), siteId, srcCurId, currencyID).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumAdults;
        }
        if (Convert.ToDecimal(tp.PerUnitChildPrice) > 0)
        {
            string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitChildPrice), siteId, srcCurId, currencyID).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumBoys;
        }
        if (Convert.ToDecimal(tp.PerUnitSeniorPrice) > 0)
        {
            string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitSeniorPrice), siteId, srcCurId, currencyID).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumSeniors;
        }
        if (Convert.ToDecimal(tp.PerUnitYouthPrice) > 0)
        {
            string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitYouthPrice), siteId, srcCurId, currencyID).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumYouths;
        }
        return price;
    }

    public Decimal GetTotalOriginalPrice(TrainPrice tp)
    {
        Decimal price = 0;
        if (Convert.ToDecimal(tp.PerUnitAdultPrice) > 0)
        {
            string Aprice = Convert.ToDecimal(tp.PerUnitAdultPrice).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumAdults;
        }
        if (Convert.ToDecimal(tp.PerUnitChildPrice) > 0)
        {
            string Aprice = Convert.ToDecimal(tp.PerUnitChildPrice).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumBoys;
        }
        if (Convert.ToDecimal(tp.PerUnitSeniorPrice) > 0)
        {
            string Aprice = Convert.ToDecimal(tp.PerUnitSeniorPrice).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumSeniors;
        }
        if (Convert.ToDecimal(tp.PerUnitYouthPrice) > 0)
        {
            string Aprice = Convert.ToDecimal(tp.PerUnitYouthPrice).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumYouths;
        }
        return price;
    }

    public string GetBeneUserPrice(TrainPrice tp, int Type)
    {
        var srcCurId = FrontEndManagePass.GetCurrencyID("EUB");
        Decimal price = 0;
        switch (Type)
        {
            case 1:
                if (Convert.ToDecimal(tp.PerUnitAdultPrice) > 0)
                {
                    string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitAdultPrice), siteId, srcCurId, currencyID).ToString("F");
                    price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumAdults;
                }
                break;
            case 2:
                if (Convert.ToDecimal(tp.PerUnitChildPrice) > 0)
                {
                    string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitChildPrice), siteId, srcCurId, currencyID).ToString("F");
                    price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumBoys;
                }
                break;
            case 3:
                if (Convert.ToDecimal(tp.PerUnitSeniorPrice) > 0)
                {
                    string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitSeniorPrice), siteId, srcCurId, currencyID).ToString("F");
                    price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumSeniors;
                }
                break;
            case 4:
                if (Convert.ToDecimal(tp.PerUnitYouthPrice) > 0)
                {
                    string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitYouthPrice), siteId, srcCurId, currencyID).ToString("F");
                    price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumYouths;
                }
                break;
        }
        return price.ToString();
    }

    string GetRoundPrice(string price)
    {
        //Please round up the prices to nearest above 0.50
        //If he price is 100.23, mark it as 100.50, 
        //If the price is 100.65 mark it as 102.00

        string[] strPrice = price.ToString().Split('.');

        if (strPrice[1] != null && Convert.ToDecimal(strPrice[1]) > 50)
            return (Convert.ToDecimal(strPrice[0]) + 1).ToString() + ".00";
        else if (strPrice[1] != null && Convert.ToDecimal(strPrice[1]) > 0 && Convert.ToDecimal(strPrice[1]) <= 50)
            return strPrice[0].Trim() + ".50";
        else
            return strPrice[0].Trim() + ".00";

    }
    #endregion

    private FareRequest GetFareRequest(List<Taco> tacos)
    {
        var objrequest = new FareRequest
        {
            Header = new Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = 0,
                language = Language.nl_BE,
            },
            taco = tacos.ToArray()
        };
        _ManageOneHub.ApiLogin(objrequest, siteId);
        var client = new OneHubRailOneHubClient();
        return objrequest;
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    public void SearchTrainInfo()
    {
        try
        {
            if (Session["BookingUCRerq"] != null)
            {
                var objBruc = Session["BookingUCRerq"] as BookingRequestUserControl;
                int daysLimit = GetEurostarBookingDays(objBruc);
                if (daysLimit == 0)
                    daysLimit = new ManageBooking().getOneHubServiceDayCount(objBruc.OneHubServiceName);
                var currDate = DateTime.Now;
                var maxDate = currDate.AddDays(daysLimit - 1);

                if (objBruc.depdt > maxDate)
                {
                    Session["ErrorMessage"] = "ErrorMaxDate";
                    Session["TrainSearch"] = null;
                    Response.Redirect("TrainResults.aspx");
                }
                else
                    Session["ErrorMessage"] = null;

                var client = new OneHubRailOneHubClient();
                TrainInformationRequest request = _ManageOneHub.TrainInformation(objBruc, 1, objBruc.depdt.ToString("dd/MMM/yyyy"), objBruc.depTime.ToString("HH:mm"), !string.IsNullOrEmpty(objBruc.ReturnDate) ? (DateTime.Parse(objBruc.ReturnDate)).ToString("dd/MMM/yyyy") : string.Empty, objBruc.ReturnTime);

                if (Convert.ToInt32(objBruc.Journeytype) > 0 && Convert.ToInt32(objBruc.depRCode) > 0)
                    request.IsReturnJourney = false;
                else if (Convert.ToInt32(objBruc.Journeytype) > 0)
                    request.IsReturnJourney = true;

                _ManageOneHub.ApiLogin(request, siteId);
                TrainInformationResponse pInfoSolutionsResponse = client.TrainInformation(request);

                if (pInfoSolutionsResponse != null && pInfoSolutionsResponse.TrainInformationList != null)
                {
                    //--TreniItalia Search return request                
                    if (objBruc.ReturnDate != string.Empty && objBruc.OneHubServiceName == "Trenitalia")
                    {
                        List<TrainInfoSegment> list = pInfoSolutionsResponse.TrainInformationList.SelectMany(x => x.TrainInfoSegment.Select(y => y)).ToList();
                        request = _ManageOneHub.TrainInformation(objBruc, 2, objBruc.depdt.ToString("dd/MMM/yyyy"), objBruc.depTime.ToString("HH:mm"), !string.IsNullOrEmpty(objBruc.ReturnDate) ? (DateTime.Parse(objBruc.ReturnDate)).ToString("dd/MMM/yyyy") : string.Empty, objBruc.ReturnTime);
                        request.IsReturnJourney = true;
                        _ManageOneHub.ApiLogin(request, siteId);
                        TrainInformationResponse pInfoSolutionsResponseReturn = client.TrainInformation(request);
                        List<TrainInfoSegment> listReturn = pInfoSolutionsResponseReturn.TrainInformationList.SelectMany(x => x.TrainInfoSegment.Select(y => y)).ToList();
                        list.AddRange(listReturn);
                        if (pInfoSolutionsResponseReturn.TrainInformationList != null)
                        {
                            List<TrainInformation> listResp = pInfoSolutionsResponse.TrainInformationList.ToList();
                            List<TrainInformation> listRespReturn = pInfoSolutionsResponseReturn.TrainInformationList.ToList();
                            List<TrainInformation> resultList = listResp.Concat(listRespReturn).ToList();
                            pInfoSolutionsResponse.TrainInformationList = resultList.ToArray();
                        }
                    }
                }
                Session["TrainSearch"] = pInfoSolutionsResponse;
            }
        }
        catch (Exception ex)
        {
            if (!ex.Message.Contains("Thread"))
            {
                Session["ErrorMessage"] = ex.Message;
                Response.Redirect("TrainResults.aspx");
            }
        }
    }
}



