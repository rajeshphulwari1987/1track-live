﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EurostarRequestQuote.ascx.cs"
    Inherits="UserControls_EurostarRequestQuote" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/UserControls/UcQuotationForm1.ascx" TagName="UcQuotationForm1"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/UcQuotationForm2.ascx" TagName="UcQuotationForm2"
    TagPrefix="uc1" %>
<style type="text/css">
    .starail-Wrapper
    {
        margin-top: 20px;
        margin-left: -15px;
    }
    .labelclass
    {
        font-size: 13px;
        font-weight: 500;
    }
    .starail-Form-required
    {
        color: #EEE;
    }
    .ui-datepicker-calendar
    {
        margin-left: 4px;
    }
    #sta-full-wrap
    {
        font-size: 14px;
    }
    .starail-Full-wrap
    {
        font-size: 14px;
    }
    .dots-starail-Wrapper
    {
        margin: 10px auto !important;
    }
    #MainContent_eurostar_chkShipping
    {
        margin-right: 10px;
    }
    #MainContent_eurostar_UcQuotationForm1_txtTrainNo
    {
        width: 50%;
    }
    #MainContent_eurostar_UcQuotationForm2_txtTrainNo
    {
        width: 50%;
    }
    .div_height
    {
        height: 40px;
    }
    .div_smallheight
    {
        height: 50px;
    }
    .div_largeheight
    {
        height: 70px;
    }
    @media (max-width: 690px)
    {
        .col-xs-12
        {
            margin-bottom: 5px;
        }
        .dots-starail-Wrapper
        {
            margin: 10px auto !important;
            width: 100% !important;
        }
        .progess-inner2
        {
            left: -20% !important;
        }
        #MainContent_eurostar_UcQuotationForm1_txtTrainNo
        {
            width: 100%;
        }
        #MainContent_eurostar_UcQuotationForm2_txtTrainNo
        {
            width: 100%;
        }
    }
    @media (max-width: 390px)
    {
        .col-xs-12
        {
            margin-bottom: 5px;
        }
        #div-journey
        {
            display: inline !important;
        }
        .dots-starail-Wrapper
        {
            margin: 10px auto !important;
            width: 100% !important;
        }
        .progess-inner2
        {
            left: 7% !important;
            width: 82% !important;
        }
        #MainContent_eurostar_UcQuotationForm1_txtTrainNo
        {
            width: 100%;
        }
        #MainContent_eurostar_UcQuotationForm2_txtTrainNo
        {
            width: 100%;
        }
        .div_height
        {
            height: 70px;
        }
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        filldefaultname();
        animateToTop();
        showhideJourney();
        changelabel();
        enableShippingAddress();
        callvalerror();
    });
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    function showhideJourney() {
        if ($('#MainContent_eurostar_UcQuotationForm1_rdBookingType_1').is(':checked')) {
            $('#MainContent_eurostar_UcQuotationForm2_updReturnJourney').show();
            ValidatorEnable($('[id*=reqvalerrorFrom2]')[0], true);
            ValidatorEnable($('[id*=reqvalerrorTo2]')[0], true);
            ValidatorEnable($('[id*=reqvalerrorDate2]')[0], true);
        }
        else {
            $('#MainContent_eurostar_UcQuotationForm2_updReturnJourney').hide();
            ValidatorEnable($('[id*=reqvalerrorFrom2]')[0], false);
            ValidatorEnable($('[id*=reqvalerrorTo2]')[0], false);
            ValidatorEnable($('[id*=reqvalerrorDate2]')[0], false);
        }
    }
    function Focusonerrorcontrol() {
        showhideJourney();
        callvalerror();
        $(window).on('click', function () {
            var Toperror = $(".starail-Form-error").eq(0).offset();
            if (Toperror != undefined) {
                $('body,html').animate({ scrollTop: Toperror.top }, 1000);
                $(window).off('click');
            }
        });
    }
    function changelabel() {
        $("table tbody tr td").each(function () {
            var radiotext = $(this).html();
            var index = radiotext.indexOf('label');
            radiotext = radiotext.substr(0, parseInt(index) - 1);
            $(this).find("input[type='radio']").remove();
            $(this).find("label").prepend(radiotext).addClass('checkbox-inline');
        });
    }
    function animateToTop() {
        $('body,html').animate({ scrollTop: top }, 1000);
    }
    function enableShippingAddress() {
        if ($("#MainContent_eurostar_chkShipping").is(':checked')) {
            $("#div_ShippingAddress").show();
            ValidatorEnable($('[id*=reqvalerrorShipMr]')[0], true);
            ValidatorEnable($('[id*=reqvalerrorShipFirst]')[0], true);
            ValidatorEnable($('[id*=reqvalerrorShipLast]')[0], true);
            ValidatorEnable($('[id*=reqvalerrorShipEmail]')[0], true);
            ValidatorEnable($('[id*=reqvalerrorShipPhone]')[0], true);
            ValidatorEnable($('[id*=reqvalerrorShipAdd]')[0], true);
            ValidatorEnable($('[id*=reqvalerrorShipCity]')[0], true);
            ValidatorEnable($('[id*=reqvalerrorShipZip]')[0], true);
            ValidatorEnable($('[id*=reqvalerrorShipCountry]')[0], true);
            if ($("#MainContent_eurostar_ddlShipMr").val() == '0') {
                $("#MainContent_eurostar_ddlShipMr").prop("selectedIndex", $("#MainContent_eurostar_ddlMr").prop("selectedIndex"));
            }
            if ($("#txtShipFirst").val() == '') {
                $("#txtShipFirst").val($("#txtFirst").val());
            }
            if ($("#MainContent_eurostar_txtShipLast").val() == '') {
                $("#MainContent_eurostar_txtShipLast").val($("#MainContent_eurostar_txtLast").val());
            }
            if ($("#MainContent_eurostar_txtShipEmail").val() == '') {
                $("#MainContent_eurostar_txtShipEmail").val($("#MainContent_eurostar_txtEmail").val());
            }
            if ($("#MainContent_eurostar_txtShipPhone").val() == '') {
                $("#MainContent_eurostar_txtShipPhone").val($("#MainContent_eurostar_txtBillPhone").val());
            }
            if ($("#MainContent_eurostar_txtShipAdd").val() == '') {
                $("#MainContent_eurostar_txtShipAdd").val($("#MainContent_eurostar_txtAdd").val());
            }
            if ($("#MainContent_eurostar_txtShipAdd2").val() == '') {
                $("#MainContent_eurostar_txtShipAdd2").val($("#MainContent_eurostar_txtAdd2").val());
            }
            if ($("#MainContent_eurostar_txtShipCity").val() == '') {
                $("#MainContent_eurostar_txtShipCity").val($("#MainContent_eurostar_txtCity").val());
            }
            if ($("#MainContent_eurostar_txtShipState").val() == '') {
                $("#MainContent_eurostar_txtShipState").val($("#MainContent_eurostar_txtState").val());
            }
            if ($("#MainContent_eurostar_txtShipZip").val() == '') {
                $("#MainContent_eurostar_txtShipZip").val($("#MainContent_eurostar_txtZip").val());
            }
            if ($("#MainContent_eurostar_ddlShipCountry").val() == '0') {
                $("#MainContent_eurostar_ddlShipCountry").prop("selectedIndex", $("#MainContent_eurostar_ddlCountry").prop("selectedIndex"));
            }
        }
        else {
            $("#div_ShippingAddress").hide();
            ValidatorEnable($('[id*=reqvalerrorShipMr]')[0], false);
            ValidatorEnable($('[id*=reqvalerrorShipFirst]')[0], false);
            ValidatorEnable($('[id*=reqvalerrorShipLast]')[0], false);
            ValidatorEnable($('[id*=reqvalerrorShipEmail]')[0], false);
            ValidatorEnable($('[id*=reqvalerrorShipPhone]')[0], false);
            ValidatorEnable($('[id*=reqvalerrorShipAdd]')[0], false);
            ValidatorEnable($('[id*=reqvalerrorShipCity]')[0], false);
            ValidatorEnable($('[id*=reqvalerrorShipZip]')[0], false);
            ValidatorEnable($('[id*=reqvalerrorShipCountry]')[0], false);
        }
    }

    function filldefaultname() {
        $("#txtFirst").keyup(function () {
            $("#MainContent_eurostar_dtlPassngerDetails_txtFirstName_0").val($("#txtFirst").val());
        });
        $("#MainContent_eurostar_txtLast").keyup(function () {
            $("#MainContent_eurostar_dtlPassngerDetails_txtLastName_0").val($("#MainContent_eurostar_txtLast").val());
        });
        $("#MainContent_eurostar_ddlMr").change(function () {
            $("#MainContent_eurostar_dtlPassngerDetails_ddlTitle_0").prop("selectedIndex", $("#MainContent_eurostar_ddlMr").prop("selectedIndex"));
        });
    }

    function filldefaultnameOnChange() {
        if ($("#txtFirst").val() != '' && $("#txtFirst").val() != undefined) {
            $("#MainContent_eurostar_dtlPassngerDetails_txtFirstName_0").val($("#txtFirst").val());
        }
        if ($("#MainContent_eurostar_txtLast").val() != '' && $("#MainContent_eurostar_txtLast").val() != undefined) {
            $("#MainContent_eurostar_dtlPassngerDetails_txtLastName_0").val($("#MainContent_eurostar_txtLast").val());
        } if ($("#MainContent_eurostar_ddlMr").prop("selectedIndex") != '0' && $("#MainContent_eurostar_ddlMr").prop("selectedIndex") != undefined) {
            $("#MainContent_eurostar_dtlPassngerDetails_ddlTitle_0").prop("selectedIndex", $("#MainContent_eurostar_ddlMr").prop("selectedIndex"));
        }
    }

    function confirmRequest() {
        if (confirm('"Do you want to resubmit your quote request" and "if needed amend your request and click Send Request"')) {
            return true;
        }
        else {
            window.location.href = 'home';
        }
    }
</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="page-wrap">
            <div class="container starail-Wrapper dots-starail-Wrapper">
                <div class="starail-SearchTickets">
                    <h1 class="starail-SearchTickets-title starail-u-alpha">
                        Tell us <span class="starail-u-hideMobile">more </span>about your eurostar trip</h1>
                    <div class="starail-BookingDetails-form q_requet_form form-horizontal">
                        <div class="starail-Form-row">
                            <p>
                                If you'd like to order a eurostar journey only then fillout this form and we'll
                                call you back for payment within then hour (during working times).</p>
                            <asp:Panel ID="pnlErrSuccess" runat="server">
                                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                                <div id="DivError" runat="server" class="error" style="display: none;">
                                    <asp:Label ID="lblErrorMsg" runat="server" />
                                </div>
                            </asp:Panel>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                                Billing Address
                            </label>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="form-group">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <asp:DropDownList ID="ddlMr" runat="server" CssClass="starail-Form-input">
                                            <asp:ListItem Value="0" Text="Title"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="Dr."></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Mr."></asp:ListItem>
                                            <asp:ListItem Value="3" Text="Miss"></asp:ListItem>
                                            <asp:ListItem Value="4" Text="Mrs."></asp:ListItem>
                                            <asp:ListItem Value="5" Text="Ms."></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqvalerrorTitle" runat="server" ControlToValidate="ddlMr"
                                            CssClass="starail-Form-required absolute" InitialValue="0" Display="Static" ValidationGroup="vgs1"
                                            ErrorMessage="*" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <asp:TextBox ID="txtFirst" runat="server" CssClass="starail-Form-input" ClientIDMode="Static"
                                            placeholder="First name" autocomplete="off" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorFirstName" runat="server" ControlToValidate="txtFirst"
                                            ErrorMessage="*" CssClass="starail-Form-required absolute" Display="Static" Text="*"
                                            ValidationGroup="vgs1" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <asp:TextBox ID="txtLast" runat="server" CssClass="starail-Form-input" placeholder="Last name"
                                            autocomplete="off" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorLastName" runat="server" CssClass="absolute starail-Form-required"
                                            ValidationGroup="vgs1" ErrorMessage="*" Display="Static" Text="*" ControlToValidate="txtLast" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0">
                                <div class="form-group">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="starail-Form-input" placeholder="Email" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorEmailAddress" runat="server" ControlToValidate="txtEmail"
                                            ErrorMessage="*" CssClass="starail-Form-required absolute" Display="Static" Text="*"
                                            ValidationGroup="vgs1" />
                                        <asp:RegularExpressionValidator ID="reqcustvalerrorEmailAddress" runat="server" CssClass="starail-Form-required absolute"
                                            ErrorMessage="*" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            Display="Static" Text="*" ValidationGroup="vgs1" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtBillPhone" runat="server" CssClass="starail-Form-input" MaxLength="20"
                                            placeholder="Phone No" onkeypress="return isNumberKey(event)" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorPhoneNo" runat="server" ControlToValidate="txtBillPhone"
                                            ErrorMessage="*" CssClass="starail-Form-required absolute" ValidationGroup="vgs1"
                                            Display="Static" Text="*" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0">
                                <div class="form-group">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:TextBox ID="txtAdd" runat="server" CssClass="starail-Form-input" MaxLength="50"
                                            placeholder="Address1" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorAddress1" runat="server" ControlToValidate="txtAdd"
                                            ErrorMessage="*" CssClass="starail-Form-required absolute" ValidationGroup="vgs1"
                                            Display="Static" Text="*" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0">
                                <div class="form-group">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:TextBox ID="txtAdd2" runat="server" CssClass="starail-Form-input" MaxLength="50"
                                            placeholder="Address2" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0">
                                <div class="form-group">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtCity" runat="server" CssClass="starail-Form-input" placeholder="City" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorCity" runat="server" ControlToValidate="txtCity"
                                            ErrorMessage="*" CssClass="starail-Form-required absolute" Display="Static" ValidationGroup="vgs1"
                                            Text="*" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtState" runat="server" CssClass="starail-Form-input" placeholder="State" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0">
                                <div class="form-group">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtZip" runat="server" CssClass="starail-Form-input" MaxLength="7"
                                            placeholder="Postal/Zip Code" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorZipCode" runat="server" ControlToValidate="txtZip"
                                            ErrorMessage="*" CssClass="starail-Form-required absolute" ValidationGroup="vgs1"
                                            Display="Static" Text="*" />
                                        <asp:Label ID="lblpmsg" runat="server" Visible="false" CssClass="starail-Form-required"
                                            Text="Please enter maximum 7 character postal/zip code." />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:DropDownList ID="ddlCountry" runat="server" class="starail-Form-input" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorCountry" runat="server" CssClass="starail-Form-required absolute"
                                            ValidationGroup="vgs1" Display="Static" Text="*" ControlToValidate="ddlCountry"
                                            InitialValue="0" ErrorMessage="*" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0">
                                <div class="form-group">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShipping" runat="server" Text="Send my tickets to a different address"
                                            onchange="enableShippingAddress();" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form_block fullrow" id="div_ShippingAddress" style="display: none;">
                            <div class="row">
                                <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                                    Shipping Address
                                </label>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    <div class="form-group">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <asp:DropDownList ID="ddlShipMr" runat="server" CssClass="starail-Form-input">
                                                <asp:ListItem Value="0" Text="Title"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Dr."></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Mr."></asp:ListItem>
                                                <asp:ListItem Value="3" Text="Miss"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="Mrs."></asp:ListItem>
                                                <asp:ListItem Value="5" Text="Ms."></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqvalerrorShipMr" runat="server" ControlToValidate="ddlShipMr"
                                                CssClass="starail-Form-required absolute" InitialValue="0" Display="Static" ValidationGroup="vgs1"
                                                ErrorMessage="*" />
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <asp:TextBox ID="txtShipFirst" runat="server" CssClass="starail-Form-input" ClientIDMode="Static"
                                                placeholder="First name" />
                                            <asp:RequiredFieldValidator ID="reqvalerrorShipFirst" runat="server" ControlToValidate="txtShipFirst"
                                                ErrorMessage="*" CssClass="starail-Form-required absolute" Display="Static" Text="*"
                                                ValidationGroup="vgs1" />
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <asp:TextBox ID="txtShipLast" runat="server" CssClass="starail-Form-input" placeholder="Last name" />
                                            <asp:RequiredFieldValidator ID="reqvalerrorShipLast" runat="server" CssClass="absolute starail-Form-required"
                                                ValidationGroup="vgs1" ErrorMessage="*" Display="Static" Text="*" ControlToValidate="txtShipLast" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0">
                                    <div class="form-group">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <asp:TextBox ID="txtShipEmail" runat="server" CssClass="starail-Form-input" placeholder="Email" />
                                            <asp:RequiredFieldValidator ID="reqvalerrorShipEmail" runat="server" ControlToValidate="txtShipEmail"
                                                ErrorMessage="*" CssClass="starail-Form-required absolute" Display="Static" Text="*"
                                                ValidationGroup="vgs1" />
                                            <asp:RegularExpressionValidator ID="reqcustvalerrorShipEmail" runat="server" CssClass="starail-Form-required absolute"
                                                ErrorMessage="*" ControlToValidate="txtShipEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                Display="Static" Text="*" ValidationGroup="vgs1" />
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <asp:TextBox ID="txtShipPhone" runat="server" CssClass="starail-Form-input" MaxLength="20"
                                                placeholder="Phone No" onkeypress="return isNumberKey(event)" />
                                            <asp:RequiredFieldValidator ID="reqvalerrorShipPhone" runat="server" ControlToValidate="txtShipPhone"
                                                ErrorMessage="*" CssClass="starail-Form-required absolute" ValidationGroup="vgs1"
                                                Display="Static" Text="*" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0">
                                    <div class="form-group">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:TextBox ID="txtShipAdd" runat="server" CssClass="starail-Form-input" MaxLength="50"
                                                placeholder="Address1" />
                                            <asp:RequiredFieldValidator ID="reqvalerrorShipAdd" runat="server" ControlToValidate="txtShipAdd"
                                                ErrorMessage="*" CssClass="starail-Form-required absolute" ValidationGroup="vgs1"
                                                Display="Static" Text="*" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0">
                                    <div class="form-group">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:TextBox ID="txtShipAdd2" runat="server" CssClass="starail-Form-input" MaxLength="50"
                                                placeholder="Address2" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0">
                                    <div class="form-group">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <asp:TextBox ID="txtShipCity" runat="server" CssClass="starail-Form-input" placeholder="City" />
                                            <asp:RequiredFieldValidator ID="reqvalerrorShipCity" runat="server" ControlToValidate="txtShipCity"
                                                ErrorMessage="*" CssClass="starail-Form-required absolute" Display="Static" ValidationGroup="vgs1"
                                                Text="*" />
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <asp:TextBox ID="txtShipState" runat="server" CssClass="starail-Form-input" placeholder="State" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0">
                                    <div class="form-group">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <asp:TextBox ID="txtShipZip" runat="server" CssClass="starail-Form-input" MaxLength="7"
                                                placeholder="Postal/Zip Code" />
                                            <asp:RequiredFieldValidator ID="reqvalerrorShipZip" runat="server" ControlToValidate="txtShipZip"
                                                ErrorMessage="*" CssClass="starail-Form-required absolute" ValidationGroup="vgs1"
                                                Display="Static" Text="*" />
                                            <asp:Label ID="lblshipmsg" runat="server" Visible="false" CssClass="starail-Form-required"
                                                Text="Please enter maximum 7 character postal/zip code." />
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <asp:DropDownList ID="ddlShipCountry" runat="server" class="starail-Form-input" />
                                            <asp:RequiredFieldValidator ID="reqvalerrorShipCountry" runat="server" CssClass="starail-Form-required absolute"
                                                ValidationGroup="vgs1" Display="Static" Text="*" ControlToValidate="ddlShipCountry"
                                                InitialValue="0" ErrorMessage="*" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form_block fullrow">
                            <div class="row">
                                <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                                    Passenger details
                                    <br>
                                    <small>Who's travelling? </small>
                                </label>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    <div class="form-group">
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                            <asp:DropDownList ID="ddlAdult" runat="server" CssClass="starail-Form-input" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlAdult_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label class="labelclass">
                                                Adults(26-65 at time of travel)</label>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                            <asp:DropDownList ID="ddlChild" runat="server" CssClass="starail-Form-input" Style="padding-right: 0px;"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlChild_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label class="labelclass">
                                                Children(under 17 at time of travel)</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0">
                                    <div class="form-group">
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                            <asp:DropDownList ID="ddlYouth" runat="server" CssClass="starail-Form-input" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlYouth_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label class="labelclass">
                                                Youths (17-25 at time of travel)</label>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                            <asp:DropDownList ID="ddlSenior" runat="server" CssClass="starail-Form-input" Style="padding-right: 0px;"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlSenior_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label class="labelclass">
                                                Seniors (over 66 at time of travel)</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:DataList ID="dtlPassngerDetails" runat="server" RepeatColumns="1" RepeatDirection="Horizontal"
                            RepeatLayout="Flow" OnItemDataBound="dtlPassngerDetails_ItemDataBound">
                            <ItemTemplate>
                                <div class="form_block fullrow">
                                    <div class="row">
                                        <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                                            <%#Eval("PassangerType")%>
                                        </label>
                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                            <div class="form-group">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <asp:HiddenField ID="hdnTravellerId" runat="server" />
                                                    <asp:HiddenField ID="hdnP2POtherId" runat="server" />
                                                    <asp:HiddenField ID="hdnPassP2PId" runat="server" />
                                                    <asp:DropDownList ID="ddlTitle" runat="server" CssClass="starail-Form-input">
                                                        <asp:ListItem Value="0">Title</asp:ListItem>
                                                        <asp:ListItem Value="1">Dr.</asp:ListItem>
                                                        <asp:ListItem Value="2">Mr.</asp:ListItem>
                                                        <asp:ListItem Value="3">Miss</asp:ListItem>
                                                        <asp:ListItem Value="4">Mrs.</asp:ListItem>
                                                        <asp:ListItem Value="5">Ms.</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="reqvalerrorPassengerTitle" runat="server" ControlToValidate="ddlTitle"
                                                        CssClass="starail-Form-required  absolute" InitialValue="0" Display="Static"
                                                        ValidationGroup="vgs1" ErrorMessage="*" />
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <asp:TextBox ID="txtFirstName" runat="server" MaxLength="15" CssClass="starail-Form-input"
                                                        placeholder="First Name" />
                                                    <asp:RequiredFieldValidator ID="reqreqvalerrorPFirstName" runat="server" ErrorMessage="*"
                                                        CssClass="starail-Form-required  absolute" Display="Static" Text="*" ControlToValidate="txtFirstName"
                                                        ValidationGroup="vgs1" />
                                                    <asp:FilteredTextBoxExtender ID="ftr1" runat="server" TargetControlID="txtFirstName"
                                                        ValidChars="-qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="25" CssClass="starail-Form-input"
                                                        placeholder="Last Name" />
                                                    <asp:RequiredFieldValidator ID="reqreqvalerrorPLastName" runat="server" ErrorMessage="*"
                                                        CssClass="starail-Form-required  absolute" Display="Static" Text="*" ControlToValidate="txtLastName"
                                                        ValidationGroup="vgs1" />
                                                    <asp:FilteredTextBoxExtender ID="ftex" runat="server" TargetControlID="txtLastName"
                                                        ValidChars="-qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0">
                                            <div class="form-group">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <asp:TextBox ID="txtFipNumber" runat="server" MaxLength="25" CssClass="starail-Form-input" />
                                                    <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtFipNumber"
                                                        WatermarkText="FIP Number" WatermarkCssClass="watermark" />
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <asp:DropDownList ID="ddlPsrCountry" runat="server" CssClass="starail-Form-input">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="reqvalerrorPsrCountry" runat="server" CssClass="starail-Form-required absolute"
                                                        ValidationGroup="vgs1" Display="Static" Text="*" ControlToValidate="ddlPsrCountry"
                                                        InitialValue="0" ErrorMessage="*" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                        <uc1:UcQuotationForm1 ID="UcQuotationForm1" runat="server" />
                        <uc1:UcQuotationForm2 ID="UcQuotationForm2" runat="server" />
                        <div class="form_block fullrow">
                            <div class="row">
                                <label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 subtitle">
                                    Additional comments
                                    <br>
                                    <small>Who's travelling? </small>
                                </label>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    <div class="form-group">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:TextBox ID="txtParagraph" runat="server" CssClass="starail-Form-input" TextMode="MultiLine"
                                                Style="height: 120px;" Columns="5" Rows="20" />
                                            <asp:RequiredFieldValidator ID="reqreqvalerrorParagraph" runat="server" ControlToValidate="txtParagraph"
                                                ErrorMessage="*" CssClass="starail-Form-required absolute" Display="Static" ValidationGroup="vgs1"
                                                Text="*" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-0 ">
                                    <div class="form-group">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Button ID="btnCheckout" ClientIDMode="Static" runat="server" Text="Send Request"
                                                CssClass="starail-Button starail-Form-button starail-Form-button--primary pull-left"
                                                Style="margin: 0px; width: 170px" ValidationGroup="vgs1" OnClick="btnCheckout_Click"
                                                OnClientClick="Focusonerrorcontrol();" />
                                            <asp:ValidationSummary ID="vs1" runat="server" ShowSummary="false" DisplayMode="List"
                                                ValidationGroup="vgs1" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnCheckout" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
