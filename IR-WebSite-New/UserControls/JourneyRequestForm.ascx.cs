﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Net;
using System.Net.Mail;
using System.Xml;

public partial class UserControls_JourneyRequestForm : System.Web.UI.UserControl
{
    private Guid _siteId;
    public string script = "";
    public static string unavailableDates1 = "";
    public List<SnoClass> list = new List<SnoClass>();
    private ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string siteURLS;
    public Guid IrAgentSiteID = Guid.Parse("57ab589f-8549-46ca-82e8-4d835c2ff0ac"); // Guid.Parse("98bb7831-068a-4963-9eb8-d7ccab265b4e");
    db_1TrackEntities db = new db_1TrackEntities();
    public bool IsStaSite = false;
    public string MandatoryTextFirst = "", MandatoryTextFirstSecond = "";

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURLS = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            if (!Page.IsPostBack)
            {
                if (Page.RouteData.Values["PageId"] != null)
                {
                    var pageID = (Guid)Page.RouteData.Values["PageId"];
                    Page.Header.DataBind();
                }
                var siteDDates = new ManageHolidays().GetAllHolydaysBySite(_siteId);
                unavailableDates1 = "[";
                if (siteDDates.Any())
                {
                    foreach (var it in siteDDates)
                    {
                        unavailableDates1 += "\"" + Convert.ToDateTime(it.DateofHoliday).ToString("dd-MM-yyyy") + "\"" + ",";
                    }
                    unavailableDates1 = unavailableDates1.Substring(0, unavailableDates1.Length - 1);
                }
                unavailableDates1 += "]";
                list.Add(new SnoClass { textrowno = "1st", rowno = "1" });
                repeterjourneylist.DataSource = list;
                repeterjourneylist.DataBind();

                if (_siteId == IrAgentSiteID)
                {
                    div_FipCard.Visible = div_FipClass.Visible = false;
                    fillAgentInfo();
                }
                ShowHideTermsMandatoryText(_siteId);
                FillPassengerList();
            }
        }
        catch (Exception ex) { errorMsg.Visible = true; }
    }

    protected void repeterjourneylist_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var hdnddltimedept = e.Item.FindControl("hdnddltimedept") as HiddenField;
                var hdnddltimearrival = e.Item.FindControl("hdnddltimearrival") as HiddenField;
                var hdnddlclass = e.Item.FindControl("hdnddlclass") as HiddenField;
                var hdnddlservicetype = e.Item.FindControl("hdnddlservicetype") as HiddenField;

                var ddltimedept = e.Item.FindControl("ddltimedept") as DropDownList;
                var ddltimearrival = e.Item.FindControl("ddltimearrival") as DropDownList;
                var ddlclass = e.Item.FindControl("ddlclass") as DropDownList;
                var ddlservicetype = e.Item.FindControl("ddlservicetype") as DropDownList;

                if (!string.IsNullOrEmpty(hdnddlclass.Value))
                    ddlclass.SelectedValue = hdnddlclass.Value;

                if (!string.IsNullOrEmpty(hdnddlservicetype.Value))
                    ddlservicetype.SelectedValue = hdnddlservicetype.Value;

                if (!string.IsNullOrEmpty(hdnddltimearrival.Value))
                    ddltimearrival.SelectedValue = hdnddltimearrival.Value;

                if (!string.IsNullOrEmpty(hdnddltimedept.Value))
                    ddltimedept.SelectedValue = hdnddltimedept.Value;
            }
        }
        catch (Exception ex)
        {
            errorMsg.Visible = true;
        }
    }

    public void fillrepeter()
    {
        try
        {
            int i = 1;
            foreach (RepeaterItem it in repeterjourneylist.Items)
            {
                string textrowno = "";
                if (i == 1)
                    textrowno = "1st";
                if (i == 2)
                    textrowno = "2nd";
                if (i == 3)
                    textrowno = "3rd";
                if (i > 3)
                    textrowno = (i) + "th";

                var hdnrowno = it.FindControl("hdnrowno") as HiddenField;
                var txtfrom = it.FindControl("txtfrom") as TextBox;
                var txtto = it.FindControl("txtto") as TextBox;
                var txtchange = it.FindControl("txtchange") as TextBox;
                var txtdeptdate = it.FindControl("txtdeptdate") as TextBox;
                var txttrainno = it.FindControl("txttrainno") as TextBox;
                var ddltimedept = it.FindControl("ddltimedept") as DropDownList;
                var ddltimearrival = it.FindControl("ddltimearrival") as DropDownList;
                var ddlclass = it.FindControl("ddlclass") as DropDownList;
                var ddlservicetype = it.FindControl("ddlservicetype") as DropDownList;

                var obj = new SnoClass
                {
                    ddlclass = ddlclass.SelectedValue,
                    ddlservicetype = ddlservicetype.SelectedValue,
                    ddltimearrival = ddltimearrival.SelectedValue,
                    ddltimedept = ddltimedept.SelectedValue,
                    rowno = i.ToString(),
                    textrowno = textrowno,
                    txtchange = txtchange.Text,
                    txtdeptdate = txtdeptdate.Text,
                    txtfrom = txtfrom.Text,
                    txtto = txtto.Text,
                    txttrainno = txttrainno.Text,
                };
                list.Add(obj);
                i++;
            }
        }
        catch (Exception ex)
        {
            errorMsg.Visible = true;
        }
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (!chkMandatory.Checked)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "checkMandatory", "checkMandatory();showhideterms();", true);
                return;
            }
            string subject = "";
            tblOrder objOrder = new tblOrder();
            objOrder.CreatedOn = DateTime.Now;
            objOrder.Status = 21;
            objOrder.SiteID = _siteId;
            objOrder.UserID = USERuserInfo.ID;
            objOrder.AgentID = AgentuserInfo.UserID;
            objOrder.TrvType = "P2P";
            objOrder.IpAddress = Request.ServerVariables["REMOTE_ADDR"];
            objOrder.page_accept = "created";
            objOrder.AffiliateCode = "";
            objOrder.Note = txtnote.Text;
            objOrder.AgentReferenceNo = "0";
            objOrder.IsFutureCommunication = chkNotMandatory.Checked;
            db.tblOrders.AddObject(objOrder);

            tblOrderTraveller objtot = new tblOrderTraveller();
            objtot.ID = Guid.NewGuid();
            objtot.Title = ddltitle.SelectedValue;
            objtot.FirstName = txtleadfname.Text;
            objtot.LastName = txtleadlast.Text;
            objtot.Country = Guid.Empty;
            db.tblOrderTravellers.AddObject(objtot);
            db.SaveChanges();

            int count = 0;
            foreach (RepeaterItem it in repeterjourneylist.Items)
            {
                var txtfrom = it.FindControl("txtfrom") as TextBox;
                var txtto = it.FindControl("txtto") as TextBox;
                var txtchange = it.FindControl("txtchange") as TextBox;
                var txtdeptdate = it.FindControl("txtdeptdate") as TextBox;
                var txttrainno = it.FindControl("txttrainno") as TextBox;
                var ddltimedept = it.FindControl("ddltimedept") as DropDownList;
                var ddltimearrival = it.FindControl("ddltimearrival") as DropDownList;
                var ddlclass = it.FindControl("ddlclass") as DropDownList;
                var ddlservicetype = it.FindControl("ddlservicetype") as DropDownList;

                tblP2PSale objtps = new tblP2PSale();
                objtps.ID = Guid.NewGuid();
                objtps.From = txtfrom.Text;
                objtps.To = txtto.Text;
                objtps.Via = txtchange.Text;
                objtps.DateTimeArrival = objtps.DateTimeDepature = Convert.ToDateTime(txtdeptdate.Text);
                objtps.TrainNo = txttrainno.Text;
                objtps.Passenger = ddladult.SelectedValue + " Adults, " + ddlchildren.SelectedValue + " Child, " + ddlyouths.SelectedValue + " Youth, " + ddlseniors.SelectedValue + " Senior";
                objtps.CurrencyId = db.tblSites.FirstOrDefault(x => x.ID == _siteId).DefaultCurrencyID;
                objtps.Class = ddlclass.SelectedValue;
                objtps.SeviceName = ddlservicetype.SelectedValue;
                objtps.DepartureTime = ddltimedept.SelectedValue;
                objtps.ArrivalTime = ddltimearrival.SelectedValue;
                objtps.Senior = ddlseniors.SelectedValue;
                objtps.Adult = ddladult.SelectedValue;
                objtps.Children = ddlchildren.SelectedValue;
                objtps.Youth = ddlyouths.SelectedValue;
                objtps.DeliveryOption = ticketdelivered.SelectedValue;
                if (_siteId != IrAgentSiteID)
                {
                    objtps.FIPNumber = txtFIPcardNo.Text;
                    objtps.FIPClass = ddlFIPClass.SelectedValue;
                }
                db.tblP2PSale.AddObject(objtps);

                tblPassP2PSalelookup objtpsl = new tblPassP2PSalelookup();
                objtpsl.ID = Guid.NewGuid();
                objtpsl.OrderID = objOrder.OrderID;
                objtpsl.PassSaleID = objtps.ID;
                objtpsl.OrderTravellerID = objtot.ID;
                objtpsl.ProductType = "P2P";
                db.tblPassP2PSalelookup.AddObject(objtpsl);
                db.SaveChanges();
                if (count == 0)
                    subject = "Quote - Dep: " + txtdeptdate.Text + " Pax: " + ddltitle.SelectedValue + " " + txtleadfname.Text + " " + txtleadlast.Text;
                count++;
            }

            tblOrderBillingAddress objbilling = new tblOrderBillingAddress();
            objbilling.ID = Guid.NewGuid();
            objbilling.OrderID = objOrder.OrderID;
            objbilling.TitleShpg = ddltitle.SelectedValue;
            objbilling.FirstNameShpg = objbilling.FirstName = txtfirst.Text;
            objbilling.LastNameShpg = objbilling.LastName = txtlast.Text;
            objbilling.EmailAddressShpg = objbilling.EmailAddress = txtemail.Text;
            objbilling.PhoneShpg = objbilling.Phone = txtphone.Text;
            db.tblOrderBillingAddresses.AddObject(objbilling);
            db.SaveChanges();

            string body = GetEmailBody(objOrder.OrderID);
            string CCEmailAddress = _oWebsitePage.GetSiteDatabySiteId(_siteId).JourneyEmail;
            if (_siteId == IrAgentSiteID)
            {
                //SendMail(_siteId, subject, body, "dipu.bharti@dotsquares.com", txtemail.Text, CCEmailAddress);     //used for IR agent
                SendMail(_siteId, subject, body, CCEmailAddress, txtemail.Text, CCEmailAddress);   //used for IR agent
            }
            else
            {
                //SendMail(_siteId, subject, body, "dipu.bharti@dotsquares.com", txtemail.Text, CCEmailAddress);   //used for bookmyrst.co.uk
                SendMail(_siteId, subject, body, "sales@bookmyrst.co.uk", txtemail.Text, CCEmailAddress);      //used for bookmyrst.co.uk
            }
            succmessage.Text = "Thank you, your request ID is " + objOrder.OrderID + ", please make a note of this and quote it whenever you're in contact with us. One of our rail experts will be in touch soon! To submit another request please click here ";
            pnlJourneyInfo.Visible = errorMsg.Visible = false;
            sucessMsg.Visible = true;
        }
        catch (Exception ex) { errorMsg.Visible = true; }
    }

    public bool SendMail(Guid siteId, string Subject, string Body, string FromEmail, string ToMail, string CCEmailAddress)
    {
        try
        {
            var masterPage = new Masters();
            var message = new MailMessage();
            var smtpClient = new SmtpClient();
            var result = masterPage.GetEmailSettingDetail(siteId);
            if (result != null)
            {
                smtpClient.Host = result.SmtpHost;
                smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);

                message.From = new MailAddress(FromEmail, FromEmail);
                message.To.Add(ToMail);
                message.Subject = Subject;
                message.Bcc.Add(CCEmailAddress);
                message.Bcc.Add("dipu.bharti@dotsquares.com");
                message.IsBodyHtml = true;
                message.Body = Body;
                smtpClient.Send(message);
                return true;
            }
            else
                return false;
        }
        catch (Exception ee) { return false; }
    }

    public string GetEmailBody(long OrderID)
    {
        try
        {
            string body = string.Empty; string htmfile = string.Empty;
            ManageEmailTemplate objMailTemp = new ManageEmailTemplate();
            htmfile = Server.MapPath("~/MailTemplate/NewBookMyRSTTemplate.html");
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(htmfile);
            var list = xmlDoc.SelectNodes("html");
            string bodyHtml = list[0].InnerXml.ToString();
            bodyHtml = bodyHtml.Replace("##QuoteID##", OrderID.ToString());
            bodyHtml = bodyHtml.Replace("##QuoteRequestedBy##", txtfirst.Text + " " + txtlast.Text);
            bodyHtml = _siteId == IrAgentSiteID ? bodyHtml.Replace("##FIPCardNumber##", "") : bodyHtml.Replace("##FIPCardNumber##", "<tr style='background-color: #eeeeee; font-size: 14px;'><td width='30%' style='padding: 5px 0px 5px 10px;'>FIP card number:</td><td>" + txtFIPcardNo.Text + "</td></tr>");
            bodyHtml = _siteId == IrAgentSiteID ? bodyHtml.Replace("##FIPCardClass##", "") : bodyHtml.Replace("##FIPCardClass##", "<tr style='background-color: #eeeeee; font-size: 14px;'><td width='30%' style='padding: 5px 0px 5px 10px;'>FIP card class:</td><td>" + ddlFIPClass.SelectedValue + "</td></tr>");
            bodyHtml = bodyHtml.Replace("##DeliveryMethod##", ticketdelivered.SelectedValue);
            bodyHtml = bodyHtml.Replace("##NoOfAdult##", ddladult.SelectedValue);
            bodyHtml = bodyHtml.Replace("##NoOfSenior##", ddlseniors.SelectedValue);
            bodyHtml = bodyHtml.Replace("##NoOfYouth##", ddlyouths.SelectedValue);
            bodyHtml = bodyHtml.Replace("##NoOfChild##", ddlchildren.SelectedValue);
            bodyHtml = bodyHtml.Replace("##LeadPassengerName##", ddltitle.SelectedValue + " " + txtleadfname.Text + " " + txtleadlast.Text);
            bodyHtml = bodyHtml.Replace("##EmailAddress##", txtemail.Text);
            bodyHtml = bodyHtml.Replace("##PhoneNumber##", txtphone.Text);

            for (int i = 0; i < repeterjourneylist.Items.Count; i++)
            {
                var txtfrom = repeterjourneylist.Items[i].FindControl("txtfrom") as TextBox;
                var txtto = repeterjourneylist.Items[i].FindControl("txtto") as TextBox;
                var txtchange = repeterjourneylist.Items[i].FindControl("txtchange") as TextBox;
                var txtdeptdate = repeterjourneylist.Items[i].FindControl("txtdeptdate") as TextBox;
                var txttrainno = repeterjourneylist.Items[i].FindControl("txttrainno") as TextBox;
                var ddltimedept = repeterjourneylist.Items[i].FindControl("ddltimedept") as DropDownList;
                var ddltimearrival = repeterjourneylist.Items[i].FindControl("ddltimearrival") as DropDownList;
                var ddlclass = repeterjourneylist.Items[i].FindControl("ddlclass") as DropDownList;
                var ddlservicetype = repeterjourneylist.Items[i].FindControl("ddlservicetype") as DropDownList;
                int rowCount = repeterjourneylist.Items.Count;

                body += "<tr style='background-color: #eeeeee; font-size: 14px;'><td colspan='2' style='padding: 5px 0px 5px 10px;'><strong>" + txtfrom.Text + " - " + txtto.Text + "</strong></td></tr>";
                body += "<tr style='background-color: #eeeeee; font-size: 14px;'><td width='30%' style='padding: 5px 0px 5px 10px;'>Departs:</td><td>" + ddltimedept.Text + " " + txtdeptdate.Text + "</td></tr>";
                body += "<tr style='background-color: #eeeeee; font-size: 14px;'><td width='30%' style='padding: 5px 0px 5px 10px;'>Arrives:</td><td>" + ddltimearrival.Text + " " + txtdeptdate.Text + "</td></tr>";
                body += "<tr style='background-color: #eeeeee; font-size: 14px;'><td width='30%' style='padding: 5px 0px 5px 10px;'>Via/change:</td><td>" + txtchange.Text + "</td></tr>";
                body += "<tr style='background-color: #eeeeee; font-size: 14px;'><td width='30%' style='padding: 5px 0px 5px 10px;'>Train number:</td><td>" + txttrainno.Text + "</td></tr>";
                body += "<tr style='background-color: #eeeeee; font-size: 14px;'><td width='30%' style='padding: 5px 0px 5px 10px;'>Class of travel:</td><td>" + ddlclass.SelectedValue + "</td></tr>";
                body += "<tr style='background-color: #eeeeee; font-size: 14px;'><td width='30%' style='padding: 5px 0px 5px 10px;'>Type of accommodation:</td><td>" + ddlservicetype.SelectedValue + "</td></tr>";
            }
            bodyHtml = bodyHtml.Replace("##JourneyDetails##", body);
            bodyHtml = bodyHtml.Replace("##SpecialInstruction##", txtnote.Text);
            bodyHtml = bodyHtml.Replace("##SiteLogoLeft##", siteURLS + "images/newbookmyrst-logo.png");
            bodyHtml = _siteId == IrAgentSiteID ? bodyHtml.Replace("##SiteLogoRight##", "") : bodyHtml.Replace("##SiteLogoRight##", siteURLS + "images/newbookmyrst-logo-right.png");
            return bodyHtml;
        }
        catch (Exception ex) { throw ex; }
    }

    protected void btnDeleteNewJourney_Click(object sender, EventArgs e)
    {
        try
        {
            fillrepeter();
            if (!string.IsNullOrEmpty(hdnclickValue.Value) && hdnclickValue.Value != "0")
            {
                list.Remove(list.FirstOrDefault(x => x.rowno == hdnclickValue.Value));
                repeterjourneylist.DataSource = list;
                repeterjourneylist.DataBind();
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "callvalerror22", "callvalerror();callvalerror22();LoadCal22();", true);
        }
        catch (Exception ex) { errorMsg.Visible = true; }
    }

    protected void btnAddNewJourney_Click(object sender, EventArgs e)
    {
        try
        {
            fillrepeter();
            int item = repeterjourneylist.Items.Count;
            if (item == 0)
                list.Add(new SnoClass { textrowno = "1st", rowno = "1" });
            if (item == 1)
                list.Add(new SnoClass { textrowno = "2nd", rowno = "2" });
            if (item == 2)
                list.Add(new SnoClass { textrowno = "3rd", rowno = "3" });
            if (item >= 3)
                list.Add(new SnoClass { textrowno = (item + 1) + "th", rowno = (item + 1).ToString() });
            repeterjourneylist.DataSource = list;
            repeterjourneylist.DataBind();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "LoadCal12", "callvalerror();callvalerror22();LoadCal22();", true);
        }
        catch (Exception ex) { errorMsg.Visible = true; }
    }

    protected void btnRedirect_Click(object sender, EventArgs e)
    {
        Response.Redirect(siteURLS);
    }

    public void fillAgentInfo()
    {
        if (_siteId == IrAgentSiteID && AgentuserInfo.UserID != Guid.Empty)
        {
            var agentInfo = db.tblAdminUsers.FirstOrDefault(x => x.ID == AgentuserInfo.UserID);
            if (agentInfo != null)
            {
                txtfirst.Text = agentInfo.Forename;
                txtlast.Text = agentInfo.Surname;
                txtemail.Text = agentInfo.EmailAddress;
            }
        }
    }

    public void ShowHideTermsMandatoryText(Guid SiteId)
    {
        try
        {
            var data = _oWebsitePage.GetSiteDatabySiteId(SiteId);
            if (data != null)
            {
                IsStaSite = data.IsSTA.HasValue ? (data.IsSTA.Value == true ? true : false) : false;
                div_Mandatory.Visible = data.IsMandatoryTerm;
                div_NotMandatory.Visible = data.IsMandatoryTermSecond;
                p_MandatoryText.InnerHtml = MandatoryTextFirst = !string.IsNullOrEmpty(data.MandatoryTermText) ? data.MandatoryTermText : "";
                p_NotMandatoryText.InnerHtml = MandatoryTextFirstSecond = !string.IsNullOrEmpty(data.MandatoryTermTextSecond) ? data.MandatoryTermTextSecond : "";
            }
        }
        catch (Exception ex) { }
    }

    public void FillPassengerList()
    {
        if (_siteId == IrAgentSiteID)
        {
            for (int i = 0; i <= 50; i++)
            {
                ddladult.Items.Add(new ListItem(i.ToString(), i.ToString()));
                ddlyouths.Items.Add(new ListItem(i.ToString(), i.ToString()));
                ddlseniors.Items.Add(new ListItem(i.ToString(), i.ToString()));
                ddlchildren.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddladult.SelectedIndex = 1;
        }
        else
        {
            for (int i = 0; i <= 10; i++)
            {
                ddladult.Items.Add(new ListItem(i.ToString(), i.ToString()));
                ddlyouths.Items.Add(new ListItem(i.ToString(), i.ToString()));
                ddlseniors.Items.Add(new ListItem(i.ToString(), i.ToString()));
                ddlchildren.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddladult.SelectedIndex = 1;
        }
    }
}

public class SnoClass
{
    public string rowno { get; set; }
    public string textrowno { get; set; }
    public string txtfrom { get; set; }
    public string txtto { get; set; }
    public string txtchange { get; set; }
    public string txtdeptdate { get; set; }
    public string txttrainno { get; set; }
    public string ddltimedept { get; set; }
    public string ddltimearrival { get; set; }
    public string ddlclass { get; set; }
    public string ddlservicetype { get; set; }
}