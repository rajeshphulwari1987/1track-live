﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using OneHubServiceRef;
using Business;
using System.Configuration;

public partial class UserControls_UcLoyaltyCard : System.Web.UI.UserControl
{
    public int minDate = 0;
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    BookingRequestUserControl objBRUC;
    public static string unavailableDates1 = "";
    private Guid siteId;
    public string siteURL;
    public string script;
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    ManageOneHub _ManageOneHub = new ManageOneHub();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            ShowHaveRailPass(siteId);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            var siteDDates = new ManageHolidays().GetAllHolydaysBySite(siteId);
            unavailableDates1 = "[";
            if (siteDDates.Count() > 0)
            {
                foreach (var it in siteDDates)
                {
                    unavailableDates1 += "\"" + Convert.ToDateTime(it.DateofHoliday).ToString("dd-MM-yyyy") + "\"" + ",";
                }
                unavailableDates1 = unavailableDates1.Substring(0, unavailableDates1.Length - 1);
            }
            unavailableDates1 += "]";
        }

        LoadPopForSites();
        if (!IsPostBack)
        {
            bool isUseSite = (bool)_oWebsitePage.IsUsSite(siteId);
            divSearch.Visible = (bool)_oWebsitePage.IsVisibleP2PWidget(siteId);
            divSearchMessage.Visible = !(bool)_oWebsitePage.IsVisibleP2PWidget(siteId);
            minDate = _oWebsitePage.GetBookingDayLimitBySiteId(siteId);
            rdBookingType.Items[1].Text = isUseSite ? "Round Trip" : "Return";
            script = new Masters().GetQubitScriptBySId(siteId);
            for (int j = 10; j >= 0; j--)
            {
                ddlAdult.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlChild.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlYouth.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlSenior.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlAdult.SelectedValue = "1";
            }
            //For ir site
            if (_db.tblSites.Any(x => x.ID == siteId && x.LayoutType == 2) && !Request.Url.AbsoluteUri.ToLower().Contains("1tracknew") && !Request.Url.AbsoluteUri.ToLower().Contains("bookmyrst"))
            {
                lblAdult.Attributes.Add("style", "display:none;");
                lblChild.Attributes.Add("style", "display:none;");
                lblYouth.Attributes.Add("style", "display:none;");
                lblSenior.Attributes.Add("style", "display:none;");

                lblIRAdult.Attributes.Add("style", "display:block;");
                lblIRChild.Attributes.Add("style", "display:block;");
                lblIRYouth.Attributes.Add("style", "display:block;");
                lblIRSenior.Attributes.Add("style", "display:block;");

                div_Class.Attributes.Add("style", "display:none;");
                div_Transfer.Attributes.Add("style", "display:none;");
            }
            else
            {
                pnlTravellerPopUP.Attributes.Add("style", "display:none;");
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "removeIrClasses", "removeIrClasses();", true);
            }
            if (Session["P2POrderID"] != null)
                GetExistingUserOrder();
            else if (Session["BookingUCRerq"] != null)
                FillPageInfo();
        }
    }

    void ShowHaveRailPass(Guid siteID)
    {
        var railPass = _oWebsitePage.HavRailPass(siteID);
        //divRailPass.Visible = railPass;
    }

    public void FillPageInfo()
    {
        if (Session["BookingUCRerq"] != null)
        {
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            txtFrom.Text = objBRUC.FromDetail;
            txtTo.Text = objBRUC.ToDetail;
            txtDepartureDate.Text = objBRUC.depdt.ToString("dd/MMM/yyyy");
            ddldepTime.SelectedValue = objBRUC.depTime.ToString("HH:mm");
            if (objBRUC.ReturnDate != string.Empty)
            {
                rdBookingType.SelectedValue = "1";
                returndiv.Visible = true;
                txtReturnDate.Enabled = true;
                lblrtnerror.Visible = true;
                reqvalerror3.Enabled = true;
                ddlReturnTime.Enabled = true;
                txtReturnDate.Text = objBRUC.ReturnDate;
                ddlReturnTime.SelectedValue = objBRUC.ReturnTime;
            }
            else
            {
                rdBookingType.SelectedValue = "0";
                returndiv.Visible = false;
                lblrtnerror.Visible = false;
                txtReturnDate.Enabled = false;
                ddlReturnTime.Enabled = false;
                txtReturnDate.Text = string.Empty;
                ddlReturnTime.SelectedValue = objBRUC.ReturnTime;
                reqvalerror3.Enabled = false;
            }
            ddlAdult.SelectedValue = objBRUC.Adults.ToString();
            ddlChild.SelectedValue = objBRUC.Boys.ToString();
            ddlClass.SelectedValue = objBRUC.ClassValue.ToString();
            ddlTransfer.SelectedValue = objBRUC.Transfare.ToString();
            hdntxtFromRailName.Value = hdntxtToRailName.Value = !string.IsNullOrEmpty(objBRUC.OneHubServiceName) ? objBRUC.OneHubServiceName.ToUpper() : "";

            #region Hide rail card option it will be used in future
            //if (objBRUC.OneHubServiceName == "Evolvi")
            //{
            //    div_chkAddRailCard.Attributes.Add("style", "display: block;");
            //    hdntxtFromRailName.Value = "EVOLVI";
            //    hdntxtToRailName.Value = "EVOLVI";
            //}
            #endregion
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected void rdBookingType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rdBookingType.SelectedValue == "0")
        {
            returndiv.Visible = false;
            reqvalerror3.Enabled = false;
            lblrtnerror.Visible = false;
            txtReturnDate.Enabled = false;
            ddlReturnTime.Enabled = false;
            ScriptManager.RegisterStartupScript(Page, GetType(), "cal", "caldisable();ChangeChildAgeForEvolvi();", true);
        }
        else
        {
            returndiv.Visible = true;
            lblrtnerror.Visible = true;
            txtReturnDate.Enabled = true;
            ddlReturnTime.Enabled = true;
            reqvalerror3.Enabled = true;
            txtReturnDate.Text = string.Empty;
            ScriptManager.RegisterStartupScript(Page, GetType(), "cal", "calenable();ChangeChildAgeForEvolvi();", true);
        }
        if (_db.tblSites.Any(x => x.ID == siteId && x.LayoutType == 2) && !Request.Url.AbsoluteUri.ToLower().Contains("1tracknew") && !Request.Url.AbsoluteUri.ToLower().Contains("bookmyrst"))
        {
            //
        }
        else
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "removeIrClasses2", "removeIrClasses();", true);

        ScriptManager.RegisterStartupScript(Page, GetType(), "return", "TabRailTicket();LoadCal();initcallSearch('yes');", true);
    }

    protected void btnCheckout_Click(object sender, EventArgs e)
    {
        try
        {
            #region If P2P Widget is not allowed for this site then searching will not happing
            bool IsVisibleP2PWidget = (bool)_oWebsitePage.IsVisibleP2PWidget(siteId);
            if (!IsVisibleP2PWidget)
                return;
            #endregion
            Search();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void Search()
    {
        try
        {
            BookingRequestUserControl objBruc = new BookingRequestUserControl();
            ManageBooking objBooking = new ManageBooking();

            objBruc.FromDetail = txtFrom.Text.Trim();
            objBruc.ToDetail = txtTo.Text.Trim();
            objBruc.depdt = DateTime.ParseExact(txtDepartureDate.Text, "dd/MMM/yyyy", null);
            objBruc.depTime = Convert.ToDateTime(ddldepTime.SelectedValue);

            GetStationDetailsByStationName objStationDeptDetail = objBooking.GetStationDetailsByStationName(txtFrom.Text.Trim());
            objBruc.depstCode = objStationDeptDetail != null ? objStationDeptDetail.StationCode : string.Empty;
            objBruc.depRCode = objStationDeptDetail != null ? objStationDeptDetail.RailwayCode : "0";
            ViewState["DeptrailName"] = objStationDeptDetail != null ? objStationDeptDetail.RailName : "";

            GetStationDetailsByStationName objStationArrDetail = objBooking.GetStationDetailsByStationName(txtTo.Text.Trim());
            objBruc.arrstCode = objStationArrDetail != null ? objStationArrDetail.StationCode : string.Empty;
            objBruc.arrRCode = objStationArrDetail != null ? objStationArrDetail.RailwayCode : "0";
            ViewState["ArvtrailName"] = objStationArrDetail != null ? objStationArrDetail.RailName : "";

            List<EvolviRailCardRequestInfo> objRailCardList = new List<EvolviRailCardRequestInfo>();
            if (objStationDeptDetail != null)
            {
                if (objStationDeptDetail.RailName.Trim() == "BENE")
                    objBruc.OneHubServiceName = "BeNe";
                else if (objStationDeptDetail.RailName.Trim() == "NTV")
                    objBruc.OneHubServiceName = "NTV";
                else if (objStationDeptDetail.RailName.Trim() == "ITALIA")
                    objBruc.OneHubServiceName = "Trenitalia";
                else if (objStationDeptDetail.RailName.Trim() == "EVOLVI")
                {
                    objBruc.OneHubServiceName = "Evolvi";
                    objBruc.SequenceNo = System.Web.Configuration.WebConfigurationManager.AppSettings["SequenceNo"].ToString();
                    objBruc.MaxResponse = System.Web.Configuration.WebConfigurationManager.AppSettings["MaxResponse"].ToString();
                    objBruc.ISOCountry = new ManageBooking().GetCountryISOCode(siteId);
                    objBruc.ISOCurrency = new ManageBooking().GetCurrencyISOCode(siteId);
                    objBruc.isRailCard = false;

                    #region Hide rail card option it will be used in future.
                    //for (int i = 0; i < rptPassengerDetails.Items.Count; i++)
                    //{
                    //    Label lblType = rptPassengerDetails.Items[i].FindControl("lblType") as Label;
                    //    DropDownList ddlRailCard = rptPassengerDetails.Items[i].FindControl("ddlRailCard") as DropDownList;
                    //    if (Convert.ToInt32(ddlYouth.SelectedValue) == 0 && Convert.ToInt32(ddlSenior.SelectedValue) == 0)
                    //    {
                    //        objRailCardList.Add(new EvolviRailCardRequestInfo
                    //        {
                    //            RailCardType = ddlRailCard.SelectedValue == "0" ? "" : "RailCard",
                    //            RailCardCode = ddlRailCard.SelectedValue == "0" ? "" : ddlRailCard.SelectedValue
                    //        });
                    //    }
                    //    objBruc.isRailCard = true;
                    //}
                    #endregion
                    objBruc.EvolviRailCardRequestInfoList = objRailCardList;
                }
            }

            objBruc.ClassValue = Convert.ToInt32(ddlClass.SelectedValue);
            objBruc.Adults = Convert.ToInt32(ddlAdult.SelectedValue);
            objBruc.Boys = Convert.ToInt32(ddlChild.SelectedValue);
            objBruc.Seniors = Convert.ToInt32(ddlSenior.SelectedValue);
            objBruc.Youths = Convert.ToInt32(ddlYouth.SelectedValue);
            objBruc.Transfare = Convert.ToInt32(ddlTransfer.SelectedValue);
            objBruc.ReturnDate = String.IsNullOrEmpty(txtReturnDate.Text) || txtReturnDate.Text.Trim() == "DD/MM/YYYY" ? string.Empty : txtReturnDate.Text;
            objBruc.ReturnTime = ddlReturnTime.SelectedValue;
            objBruc.Journeytype = rdBookingType.SelectedValue;

            if (rdBookingType.SelectedValue == "1")
                objBruc.IsReturnJurney = true;

            Session["BookingUCRerq"] = objBruc;
            var currDate = DateTime.Now;

            if (objBruc.OneHubServiceName == "BeNe" || objBruc.OneHubServiceName == "Trenitalia")
            {
                int daysLimit = _ManageOneHub.GetEurostarBookingDays(objBruc);
                if (daysLimit == 0)
                    daysLimit = new ManageBooking().getOneHubServiceDayCount(objBruc.OneHubServiceName);

                var maxDate = currDate.AddDays(daysLimit - 1);
                if (objBruc.depdt > maxDate && objStationDeptDetail != null)
                {
                    Session["ErrorMessage"] = "ErrorMaxDate";
                    Session["TrainSearch"] = null;
                    if (ViewState["DeptrailName"] != null && ViewState["ArvtrailName"] != null)
                    {
                        if (ViewState["DeptrailName"].ToString() == "BENE" && ViewState["ArvtrailName"].ToString() == "BENE")
                            Response.Redirect("TrainResults.aspx?req=" + "BE");
                        else if (ViewState["DeptrailName"].ToString() == "ITALIA" && ViewState["ArvtrailName"].ToString() == "ITALIA")
                            Response.Redirect("TrainResults.aspx?req=" + "TI");
                        else if (ViewState["DeptrailName"].ToString() == "NTV" && ViewState["ArvtrailName"].ToString() == "NTV")
                            Response.Redirect("TrainResults.aspx?req=" + "NTV");
                        else if (ViewState["DeptrailName"].ToString() == "EVOLVI" && ViewState["ArvtrailName"].ToString() == "EVOLVI")
                            Response.Redirect("TrainResults.aspx?req=" + "EV");
                    }
                }
            }

            var client = new OneHubRailOneHubClient();
            TrainInformationRequest request = _ManageOneHub.TrainInformation(objBruc, 1, txtDepartureDate.Text, ddldepTime.SelectedValue, txtReturnDate.Text, ddlReturnTime.SelectedValue);

            if (objBruc.OneHubServiceName == "Evolvi")
            {
                request.SequenceNo = System.Web.Configuration.WebConfigurationManager.AppSettings["SequenceNo"].ToString();
                request.MaxResponse = System.Web.Configuration.WebConfigurationManager.AppSettings["MaxResponse"].ToString();
                request.ISOCountry = new ManageBooking().GetCountryISOCode(siteId);
                request.ISOCurrency = new ManageBooking().GetCurrencyISOCode(siteId);
                request.EvolviRailCardRequestInfo = objRailCardList.ToArray();
            }

            if (Convert.ToInt32(rdBookingType.SelectedValue) > 0 && objBruc.OneHubServiceName == "Trenitalia")
                request.IsReturnJourney = false;
            else if (Convert.ToInt32(rdBookingType.SelectedValue) > 0)
                request.IsReturnJourney = true;

            _ManageOneHub.ApiLogin(request, siteId);
            TrainInformationResponse pInfoSolutionsResponse = client.TrainInformation(request);
            if (pInfoSolutionsResponse != null && pInfoSolutionsResponse.TrainInformationList != null)
            {
                //--TreniItalia Search return request                
                if (objBruc.ReturnDate != string.Empty && objBruc.OneHubServiceName == "Trenitalia")
                {
                    List<TrainInfoSegment> list = pInfoSolutionsResponse.TrainInformationList.SelectMany(x => x.TrainInfoSegment.Select(y => y)).ToList();
                    request = _ManageOneHub.TrainInformation(objBruc, 2, txtDepartureDate.Text, ddldepTime.SelectedValue, txtReturnDate.Text, ddlReturnTime.SelectedValue);
                    request.IsReturnJourney = true;
                    _ManageOneHub.ApiLogin(request, siteId);
                    TrainInformationResponse pInfoSolutionsResponseReturn = client.TrainInformation(request);
                    List<TrainInfoSegment> listReturn = pInfoSolutionsResponseReturn.TrainInformationList.SelectMany(x => x.TrainInfoSegment.Select(y => y)).ToList();
                    list.AddRange(listReturn);
                    if (pInfoSolutionsResponseReturn.TrainInformationList != null)
                    {
                        List<TrainInformation> listResp = pInfoSolutionsResponse.TrainInformationList.ToList();
                        List<TrainInformation> listRespReturn = pInfoSolutionsResponseReturn.TrainInformationList.ToList();
                        List<TrainInformation> resultList = listResp.Concat(listRespReturn).ToList();
                        pInfoSolutionsResponse.TrainInformationList = resultList.ToArray();
                    }
                }
            }
            Session["TrainSearch"] = pInfoSolutionsResponse;
            Session["HoldJourneyDetailsList"] = null;
            Session["HoldReserVationRequest"] = null;
            string stri = "BE";
            if (ViewState["DeptrailName"] != null && ViewState["ArvtrailName"] != null)
            {
                if (ViewState["DeptrailName"].ToString() == "BENE" && ViewState["ArvtrailName"].ToString() == "BENE")
                    stri = "BE";
                else if (ViewState["DeptrailName"].ToString() == "ITALIA" && ViewState["ArvtrailName"].ToString() == "ITALIA")
                {
                    stri = "TI";
                    Session["TiTimer"] = null;
                    new StationList().GetTrenitaliaTime();
                }
                else if (ViewState["DeptrailName"].ToString() == "NTV" && ViewState["ArvtrailName"].ToString() == "NTV")
                    stri = "NTV";
                else if (ViewState["DeptrailName"].ToString() == "EVOLVI" && ViewState["ArvtrailName"].ToString() == "EVOLVI")
                    stri = "EV";
            }
            Response.Redirect("TrainResults.aspx?req=" + stri);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void GetExistingUserOrder()
    {
        try
        {
            if (Session["P2POrderID"] != null)
            {
                long OrderID = Convert.ToInt64(Session["P2POrderID"].ToString());
                hdnNewOrderID.Value = OrderID.ToString();
                var result = new ManageUser().GetExistingUserOrderInfo(OrderID);
                if (result != null && result.Count > 0)
                {
                    txtFrom.Text = result.OrderBy(x => x.P2PId).FirstOrDefault().FromStation;
                    txtTo.Text = result.OrderBy(x => x.P2PId).FirstOrDefault().ToStation;
                    txtDepartureDate.Text = string.Format("{0:dd/MMM/yyyy}", result.OrderBy(x => x.P2PId).FirstOrDefault().DateTimeDepature.Value);
                    ddldepTime.SelectedValue = result.OrderBy(x => x.P2PId).FirstOrDefault().DepartureTime.Substring(0, 2) + ":00";
                    ddlAdult.SelectedValue = result.FirstOrDefault().Adult;
                    ddlChild.SelectedValue = result.FirstOrDefault().Children;
                    ddlYouth.SelectedValue = result.FirstOrDefault().Youth;
                    ddlSenior.SelectedValue = result.FirstOrDefault().Senior;
                    if (result.Count > 1)
                    {
                        returndiv.Visible = true;
                        lblrtnerror.Visible = true;
                        txtReturnDate.Enabled = true;
                        ddlReturnTime.Enabled = true;
                        reqvalerror3.Enabled = true;
                        rdBookingType.SelectedValue = "1";
                        txtReturnDate.Text = string.Format("{0:dd/MMM/yyyy}", result.OrderByDescending(x => x.P2PId).FirstOrDefault().DateTimeDepature.Value);
                        ddlReturnTime.SelectedValue = result.OrderByDescending(x => x.P2PId).FirstOrDefault().DepartureTime.Substring(0, 2) + ":00";
                    }
                    //ScriptManager.RegisterStartupScript(Page, GetType(), "TabRailTicket", "TabRailTicket();", true);
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void LoadPopForSites()
    {
        if (Request.Url.AbsoluteUri.ToLower().Contains("1tracknew") || Request.Url.AbsoluteUri.ToLower().Contains("bookmyrst"))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "addNewIRClass", "addNewIRClass();", true);
        }
        else if (_db.tblSites.Any(x => x.ID == siteId && x.LayoutType == 2))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "addIRClass", "addIRClass();", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "addSTAClass", "addSTAClass();", true);
        }
    }
}