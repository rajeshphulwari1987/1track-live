﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BeNeTrainSearchResult.ascx.cs"
    Inherits="UserControls_BeNeTrainSearchResult" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="ucTrainSearch.ascx" TagName="ucTrainSearch" TagPrefix="uc1" %>
<style type="text/css">
    #MainContent_ucSResult_pnlQuckLoad
    {
        z-index: 101 !important;
    }
    
    #MainContent_ucSResult_mdpexQuickLoad_backgroundElement
    {
        z-index: 100 !important;
    }
    .selectlist
    {
        width: 50%;
    }
    .starail-Form-inputContainer-col
    {
        width: 100% !important;
    }
    .selectlistmargin
    {
        margin-bottom: 10px;
    }
    .starail-BookingDetails-form
    {
        padding: 25px !important;
    }
    .top-arrow, .bottom-arrow
    {
        width: 25px;
        float: right;
        top: 20px;
        position: absolute;
        right: -26px;
    }
    .from-price
    {
        width: 56px;
    }
    a.bottom-arrow
    {
        margin-right: 18px;
    }
    a.top-arrow
    {
        margin-right: 18px;
    }
    
    .p-price-margin
    {
        margin-left: 5px;
        width: 100%;
    }
    .price-font
    {
        font-size: 20px !important;
        margin-left: 20px;
    }
    .pricemain-box
    {
        position: relative;
    }
    .pricebox-tag
    {
        position: absolute;
        width: 170px;
        text-align: center;
        right: -10px;
        top: 5px;
        height: 50px;
    }
    .pricebox-tag span
    {
        padding-left: 0px !important;
        width: 100%;
        display: inline-block;
        text-align: center !important;
        font-weight: bold;
        float: none !important;
        margin-left: 0;
    }
    .pricebox-tag img
    {
        margin: -16px auto 0;
        display: inline-block;
    }
    @media only screen and (max-width: 480px)
    {
        #MainContent_ucSResult_pnlQuckLoad
        {
            left: auto !important;
            width: 100% !important;
        }
        .starail-BookingDetails-form
        {
            padding: 5px !important;
        }
        .starail-Form-input, .starail-Form-textarea, .starail-Form-select
        {
            width: 100%;
        }
    }
</style>
<script type="text/javascript">
    function getfare() {
        var hostName = window.location.host;
        var url = "http://" + hostName;
        if (window.location.toString().indexOf("https:") >= 0)
            url = "https://" + hostName;
        if (hostName == "localhost") {
            url = url + "/1TrackNew";
        }
        var hostUrl = url + "/StationList.asmx/GetFare";
        $.ajax({
            type: "POST",
            url: hostUrl,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $.each(data.d, function (key, value) {
                    var scode = $.trim(value.ServiceTypeCode)
                    $('.' + scode).html(value.fareCondition);

                });
            },
            error: function () {
                console.log('An error occurred');
            }
        });
    }
</script>
<asp:UpdatePanel ID="upnlTrainSearch" runat="server">
    <ContentTemplate>
        <div class="starail-Grid starail-Grid--mobileFull">
            <div class="starail-Grid-col starail-Grid-col--nopadding">
                <div class="starail-ProgressBar starail-ProgressBar-stage--stage1 starail-u-hideMobile">
                    <div class="starail-ProgressBar-line">
                        <div class="starail-ProgressBar-progress">
                        </div>
                    </div>
                    <div class="starail-ProgressBar-stage starail-ProgressBar-stage1">
                        <div class="starail-ProgressBar-circle">
                        </div>
                        <p class="starail-ProgressBar-label">
                            Ticket Selection</p>
                        <div class="starail-ProgressBar-subStage">
                        </div>
                    </div>
                    <div class="starail-ProgressBar-stage starail-ProgressBar-stage2">
                        <div class="starail-ProgressBar-circle">
                        </div>
                        <p class="starail-ProgressBar-label">
                            Booking Details</p>
                        <div class="starail-ProgressBar-subStage">
                        </div>
                    </div>
                    <div class="starail-ProgressBar-stage starail-ProgressBar-stage3">
                        <div class="starail-ProgressBar-circle">
                        </div>
                        <p class="starail-ProgressBar-label">
                            Checkout</p>
                    </div>
                </div>
                <asp:HiddenField ID="hdnsiteURL" runat="server" Value="" />
                <asp:HiddenField ID="hdnDateDepart" runat="server" Value="" />
                <asp:HiddenField ID="hdnDateArr" runat="server" Value="" />
                <asp:HiddenField ID="hdnCurrID" runat="server" Value="" />
                <asp:HiddenField ID="hdnReq" runat="server" Value="" />
                <asp:Panel ID="pnlErrSuccess" runat="server">
                    <div id="DivSuccess" runat="server" class="success" style="display: none;">
                        <asp:Label ID="lblSuccessMsg" runat="server" />
                    </div>
                    <div id="DivError" runat="server" class="error" style="display: none;">
                        <asp:Label ID="lblErrorMsg" runat="server" />
                    </div>
                </asp:Panel>
                <div class="starail-Section starail-Section--nopadding">
                    <div class="starail-TrainOptions-mobileNav starail-u-hideDesktop">
                        <div class="starail-TrainOptions-mobileNav-stage1">
                            <a href="home" class="starail-Button starail-Button--blue"><i class="starail-Icon-chevron-left">
                            </i>Edit Search</a> <a class="starail-Button starail-TrainOptions-mobileNav-rtnJourney starail-rtnJourney-trigger starail-Button--blue is-disabled"
                                id="anchor_Next" runat="server">Next: Return <i class="starail-Icon-chevron-right">
                                </i></a>
                        </div>
                        <div class="starail-TrainOptions-mobileNav-stage2 starail-u-hide">
                            <a class="starail-Button starail-Button--blue starail-TrainOptions-outbound-trigger">
                                <i class="starail-Icon-chevron-left"></i>Outbound</a> <a class="starail-Button starail-TrainOptions-mobileNav-booking starail-booking-trigger starail-Button--blue is-disabled">
                                    Your Details <i class="starail-Icon-chevron-right"></i></a>
                            <!-- will submit form -->
                        </div>
                    </div>
                    <div class="starail-Tabs starail-TrainOptions-mainTabs">
                        <ul class="starail-Tabs-tabs">
                            <li class="starail-Tabs-tab starail-Tabs-tab--trainOptions starail-Tabs-tab--active starail-TrainOptions-outbound csscount">
                                <a href="#outbound" class="js-starail-Tabs-trigger">
                                    <div class="starail-TrainOptions-tab-container">
                                        <p class="starail-u-hideDesktop starail-TrainOptions-mobiletab-title">
                                            Your outbound journey</p>
                                        <p class="starail-TrainOptions-tab-cost starail-u-hideMobile">
                                            <%=currency %>
                                            <asp:Label ID="lbloutPrice" runat="server" Text="0.00" />
                                        </p>
                                        <p class="starail-TrainOptions-tab-title">
                                            <asp:Literal ID="ltroutFrom" runat="server" />
                                            <i class="starail-Icon-direction-arrow"><span class="starail-u-visuallyHidden">to </span>
                                            </i>
                                            <asp:Literal ID="ltroutTo" runat="server" />
                                        </p>
                                        <p class="starail-TrainOptions-tab-details">
                                            <span class="starail-u-hideMobile">Outbound </span><span class="starail-u-hideDesktopInline">
                                                Date: </span><span class="starail-TrainOptions-tab-details-date">
                                                    <asp:Literal ID="ltroutDate" runat="server" />
                                                </span><span class="starail-u-hideMobile">| </span><span class="starail-TrainOptions-tab-details-time starail-TrainOptions-tab-details-time--init starail-u-hideMobile">
                                                    (Choose time)</span>
                                        </p>
                                    </div>
                                </a></li>
                            <li class="starail-Tabs-tab starail-Tabs-tab--trainOptions starail-TrainOptions-inbound csscount"
                                runat="server" id="ShowInboud"><a href="#inbound" class="js-starail-Tabs-trigger">
                                    <div class="starail-TrainOptions-tab-container">
                                        <p class="starail-u-hideDesktop starail-TrainOptions-mobiletab-title">
                                            Your inbound journey</p>
                                        <p class="starail-TrainOptions-tab-cost starail-u-hideMobile">
                                            <%=currency %>
                                            <asp:Label ID="lblinPrice" runat="server" Text="0.00" />
                                        </p>
                                        <p class="starail-TrainOptions-tab-title">
                                            <asp:Literal ID="ltrinFrom" runat="server" />
                                            <i class="starail-Icon-direction-arrow"><span class="starail-u-visuallyHidden">to </span>
                                            </i>
                                            <asp:Literal ID="ltrinTo" runat="server" />
                                        </p>
                                        <p class="starail-TrainOptions-tab-details">
                                            <span class="starail-u-hideMobile">Inbound </span><span class="starail-u-hideDesktopInline">
                                                Date: </span><span class="starail-TrainOptions-tab-details-date">
                                                    <asp:Literal ID="ltrinDate" runat="server" />
                                                </span><span class="starail-u-hideMobile">| </span><span class="starail-TrainOptions-tab-details-time starail-TrainOptions-tab-details-time--init starail-u-hideMobile">
                                                    (Choose time)</span>
                                        </p>
                                    </div>
                                </a></li>
                            <li class="starail-Tabs-tab starail-Tabs-tab--trainOptions starail-TrainOptions-subtotal">
                                <div class="starail-TrainOptions-subtotal-details">
                                    <p class="starail-u-hideDesktopInline">
                                        Price:
                                    </p>
                                    <p class="starail-TrainOptions-subtotal-title starail-u-hideMobile" data-starail-subtotal="Subtotal"
                                        data-starail-total="Total Price">
                                        Subtotal</p>
                                    <p class="starail-TrainOptions-subtotal-cost">
                                        <%=currency %>
                                        <asp:Label ID="lblTotalPrice" runat="server" Text="0.00" />
                                    </p>
                                </div>
                                <p class="starail-TrainOptions-subtotal-passengers">
                                    For
                                    <asp:Literal ID="ltrPassenger" runat="server" />
                                </p>
                            </li>
                        </ul>
                        <div class="starail-Tabs-wrapper">
                            <div class="starail-Grid starail-Grid--nopadding starail-Grid--mobileFull">
                                <div class="starail-Grid-col starail-Grid-col--8of12">
                                    <div id="outbound" class="starail-Tabs-content journey-type">
                                        <div class="starail-TrainOptions-earlierLaterBtns">
                                            <asp:LinkButton runat="server" ID="lnkOutTopEarlier" CssClass="starail-Button starail-Button--blue IR-GaCodee IR-GaCode"
                                                OnClick="lnkEarlier_Click"> <i class="starail-Icon-chevron-left"></i><i class="starail-Icon-double-chevron-left"></i>Earlier trains</asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkOutTopLater" CssClass="starail-Button starail-Button--blue starail-TrainOptions-later IR-GaCode"
                                                OnClick="lnkLater_Click">Later trains <i class="starail-Icon-chevron-right"></i><i class="starail-Icon-double-chevron-right"></i></asp:LinkButton>
                                        </div>
                                        <div class="starail-TrainOptions-outboundError starail-Alert starail-Alert--error starail-u-hide">
                                            <p>
                                                Please select an outbound journey.</p>
                                        </div>
                                        <asp:Repeater ID="rptoutBeNe" runat="server" OnItemDataBound="rptoutBeNe_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="starail-JourneyBlock">
                                                    <div class="starail-JourneyBlock-form">
                                                        <div class="starail-JourneyBlock-header">
                                                            <div class="starail-JourneyBlock-summary" style="width: 100%">
                                                                <div class="starail-DestinationIcon" style="height: 5px">
                                                                    <div class="starail-DestinationIcon-circle">
                                                                    </div>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow" style="min-width: 35%; float: left;
                                                                    padding-right: 1%;">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        From:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs" style="text-transform: none">
                                                                        <asp:Literal runat="server" ID="ltrFrom" />
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        To:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details" style="text-transform: none">
                                                                        <asp:Literal runat="server" ID="ltrTo" />
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <p class="starail-JourneyBlock-mobile-title starail-u-hideDesktop">
                                                                <%#Eval("TrainNumber") != null ? Eval("TrainNumber").ToString() : string.Empty%>
                                                                <%# Eval("TrainCategory") != null ? Eval("TrainCategory").ToString() : string.Empty%>
                                                            </p>
                                                            <div class="starail-JourneyBlock-summary">
                                                                <div class="starail-DestinationIcon">
                                                                    <div class="starail-DestinationIcon-line">
                                                                    </div>
                                                                    <div class="starail-DestinationIcon-circle starail-DestinationIcon-circle--filled">
                                                                    </div>
                                                                    <div class="starail-DestinationIcon-circle">
                                                                    </div>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Departs:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%# Convert.ToDateTime(Eval("DepartureTime")).ToString("HH:mm")%></span> <span class="starail-JourneyBlock-divide starail-u-hideMobile">
                                                                                | </span><span class="starail-JourneyBlock-date starail-u-hideMobile">
                                                                                    <asp:Literal ID="ltrDepDate" runat="server" Text='<%#Eval("DepartureDate")%>' />
                                                                                </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Arrives:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%#Convert.ToDateTime(Eval("ArrivalTime")).ToString("HH:mm")%></span> <span class="starail-JourneyBlock-divide starail-u-hideMobile">
                                                                                | </span><span class="starail-JourneyBlock-date starail-u-hideMobile">
                                                                                    <asp:Literal ID="ltrArrDate" runat="server" Text='<%#Eval("ArrivalDate")%>' />
                                                                                </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow starail-u-hideDesktop">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Changes:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <asp:Label ID="lblTrainChanges" runat="server" Text="" />
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow starail-u-hideDesktop">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Duration:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%#Convert.ToDateTime(Eval("TotalJourneyTime")).ToString("HH:mm")%></span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="starail-JourneyBlock-info starail-u-hideMobile pricemain-box">
                                                                <p class="starail-JourneyBlock-infoRow p-price-margin">
                                                                    <a class="starail-JourneyBlock-more-trigger" style="line-height: 0px" href="#"><i
                                                                        class="starail-Icon-changes"></i>
                                                                        <asp:Label ID="lblTrainChangesMobile" runat="server" CssClass="pricecheap" />
                                                                    </a>
                                                                </p>
                                                                <div class="pricebox-tag">
                                                                    <img src="<%=siteURL %>images/from.png" class="from-price" alt="" />
                                                                    <asp:Label ID="lblCheapPrice" runat="server" CssClass="price-font"></asp:Label>
                                                                </div>
                                                                <p class="starail-JourneyBlock-infoRow p-price-margin">
                                                                    <i class="starail-Icon-time"></i><span class="starail-infoRow-time">
                                                                        <%#Convert.ToDateTime(Eval("TravelTimeDuration")).ToString("HH:mm")%></span>
                                                                    <a style="display: block;" class="bottom-arrow">
                                                                        <img src="<%=siteURL %>images/arrow-bottom.png" alt="" /></a> <a style="display: none;"
                                                                            class="top-arrow">
                                                                            <img src="<%=siteURL %>images/arrow-top.png" alt="" /></a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="starail-JourneyBlock-journeyDetails">
                                                            <div class="starail-JourneyBlock-content">
                                                                <asp:Repeater ID="GrdRouteInfo" runat="server">
                                                                    <ItemTemplate>
                                                                        <div class="starail-JourneyBlock-journeyDetails-row">
                                                                            <div class="starail-JourneyBlock-journeyDetails-provider">
                                                                                <p class="starail-JourneyBlock-journeyDetails-title">
                                                                                    <%# Eval("TrainNumber") != null ? Eval("TrainNumber").ToString() : string.Empty%>
                                                                                    <%# Eval("TrainCategory") != null ? Eval("TrainCategory").ToString() : string.Empty%></p>
                                                                            </div>
                                                                            <div class="starail-JourneyBlock-journeyDetails-details">
                                                                                <p class="starail-JourneyBlock-journeyDetails-title">
                                                                                    Departs:</p>
                                                                                <p>
                                                                                    <span>
                                                                                        <%#Convert.ToDateTime(Eval("DepartureTime")).ToString("HH:mm")%></span><span class="starail-u-hideMobile">
                                                                                            | </span><span>
                                                                                                <%#Eval("DepartureDate")%></span></p>
                                                                                <p>
                                                                                    <%#Eval("DepartureStationName")%></p>
                                                                            </div>
                                                                            <div class="starail-JourneyBlock-journeyDetails-details">
                                                                                <p class="starail-JourneyBlock-journeyDetails-title">
                                                                                    Arrives:</p>
                                                                                <p>
                                                                                    <span>
                                                                                        <%#Convert.ToDateTime(Eval("ArrivalTime")).ToString("HH:mm")%></span><span class="starail-u-hideMobile">
                                                                                            | </span><span>
                                                                                                <%#Eval("ArrivalDate")%></span></p>
                                                                                <p>
                                                                                    <%#Eval("ArrivalStationName")%></p>
                                                                            </div>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>
                                                        <%--Mobile--%>
                                                        <div class="starail-u-hideDesktop starail-JourneyBlock-mobileTriggers">
                                                            <a class="starail-JourneyBlock-more-trigger" href="#"><i class="starail-Icon starail-Icon-pluscircle">
                                                            </i>View journey details</a> <a class="starail-JourneyBlock-less-trigger" href="#"><i
                                                                class="starail-Icon starail-Icon-minuscircle"></i>Hide journey details</a>
                                                        </div>
                                                        <div class="starail-JourneyBlock-content starail-u-hideMobile collapsiblediv" id="DivTr"
                                                            runat="server" style="display: none;">
                                                        </div>
                                                        <%--Mobile--%>
                                                        <div class="starail-JourneyBlock-mobileTickets">
                                                            <asp:DropDownList ID="ddlFromPrice" runat="server" CssClass="starail-Form-select starail-JourneyBlock-select" />
                                                            <a href="#" class="starail-Button starail-Button--fullMobile starail-Button--blue starail-JourneyBlock-confirm-trigger">
                                                                Confirm Ticket</a>
                                                        </div>
                                                        <div class="starail-JourneyBlock-footer starail-u-hideMobile collapsiblediv" style="display: none;">
                                                            <a class="starail-JourneyBlock-more-trigger" href="#"><i class="starail-Icon starail-Icon-pluscircle">
                                                            </i>More info</a> <a class="starail-JourneyBlock-less-trigger" href="#"><i class="starail-Icon starail-Icon-minuscircle">
                                                            </i>Hide journey details</a> <a href="#" class="starail-JourneyBlock-button starail-Button starail-rtnJourney-trigger is-disabled IR-GaCode"
                                                                onclick="return SendJCodeAndServiceID();">Next:
                                                                <%=journyTag%></a>
                                                        </div>
                                                        <div class="starail-JourneyBlock-footer starail-JourneyBlock-mobileFooter starail-u-hide">
                                                            <p class="starail-JourneyBlock-footer-ticketSelection">
                                                                <span>Class not found</span> <a href="#" class="js-lightboxOpen" data-lightbox-id="starail-ticket-info">
                                                                    More info<i class="starail-Icon-question"></i></a>
                                                            </p>
                                                            <div class="starail-JourneyBlock-mobileFooter-btns">
                                                                <a href="#" class="starail-Button starail-undoTicket-trigger">Undo</a>
                                                            </div>
                                                            <div class="starail-JourneyBlock-mobileFooter-btns">
                                                                <asp:HyperLink runat="server" ID="hlnkContinue" class="starail-Button starail-rtnJourney-trigger IR-GaCode"
                                                                    Text="Continue" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <div class="starail-TrainOptions-earlierLaterBtns">
                                            <asp:LinkButton runat="server" ID="lnkOutBottomEarlier" CssClass="starail-Button starail-Button--blue IR-GaCode"
                                                OnClick="lnkEarlier_Click"> <i class="starail-Icon-chevron-left"></i><i class="starail-Icon-double-chevron-left"></i>Earlier trains</asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkOutBottomLater" CssClass="starail-Button starail-Button--blue starail-TrainOptions-later IR-GaCode"
                                                OnClick="lnkLater_Click">Later trains <i class="starail-Icon-chevron-right"></i><i class="starail-Icon-double-chevron-right"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                    <div id="inbound" class="starail-Tabs-content starail-Tabs-content--hidden journey-type">
                                        <div class="starail-TrainOptions-earlierLaterBtns">
                                            <asp:LinkButton runat="server" ID="lnkInTopEarlier" CssClass="starail-Button starail-Button--blue IR-GaCode"
                                                OnClick="lnkInEarlier_Click"> <i class="starail-Icon-chevron-left"></i><i class="starail-Icon-double-chevron-left"></i>Earlier trains</asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkInToptomLater" CssClass="starail-Button starail-Button--blue starail-TrainOptions-later IR-GaCode"
                                                OnClick="lnkInLater_Click">Later trains <i class="starail-Icon-chevron-right"></i><i class="starail-Icon-double-chevron-right"></i></asp:LinkButton>
                                        </div>
                                        <asp:Repeater ID="rptinBeNe" runat="server" OnItemDataBound="rptinBeNe_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="starail-JourneyBlock">
                                                    <div class="starail-JourneyBlock-form">
                                                        <div class="starail-JourneyBlock-header">
                                                            <div class="starail-JourneyBlock-summary" style="width: 100%">
                                                                <div class="starail-DestinationIcon" style="height: 5px">
                                                                    <div class="starail-DestinationIcon-circle">
                                                                    </div>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow" style="min-width: 35%; float: left;
                                                                    padding-right: 1%;">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        From:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs" style="text-transform: none">
                                                                        <asp:Literal runat="server" ID="ltrFrom" />
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        To:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details" style="text-transform: none">
                                                                        <asp:Literal runat="server" ID="ltrTo" />
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <p class="starail-JourneyBlock-mobile-title starail-u-hideDesktop">
                                                                <%#Eval("TrainNumber") != null ? Eval("TrainNumber").ToString() : string.Empty%>
                                                                <%# Eval("TrainCategory") != null ? Eval("TrainCategory").ToString() : string.Empty%>
                                                            </p>
                                                            <div class="starail-JourneyBlock-summary">
                                                                <div class="starail-DestinationIcon">
                                                                    <div class="starail-DestinationIcon-line">
                                                                    </div>
                                                                    <div class="starail-DestinationIcon-circle starail-DestinationIcon-circle--filled">
                                                                    </div>
                                                                    <div class="starail-DestinationIcon-circle">
                                                                    </div>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Departs:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%# Convert.ToDateTime(Eval("DepartureTime")).ToString("HH:mm")%></span> <span class="starail-JourneyBlock-divide starail-u-hideMobile">
                                                                                | </span><span class="starail-JourneyBlock-date starail-u-hideMobile">
                                                                                    <asp:Literal ID="ltrDepDate" runat="server" Text='<%#Eval("DepartureDate")%>' />
                                                                                </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Arrives:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%#Convert.ToDateTime(Eval("ArrivalTime")).ToString("HH:mm")%></span> <span class="starail-JourneyBlock-divide starail-u-hideMobile">
                                                                                | </span><span class="starail-JourneyBlock-date starail-u-hideMobile">
                                                                                    <asp:Literal ID="ltrArrDate" runat="server" Text='<%#Eval("ArrivalDate")%>' />
                                                                                </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow starail-u-hideDesktop">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Changes:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <asp:Label ID="lblTrainChanges" runat="server" Text="" />
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow starail-u-hideDesktop">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Duration:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%#Convert.ToDateTime(Eval("TotalJourneyTime")).ToString("HH:mm")%></span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="starail-JourneyBlock-info starail-u-hideMobile pricemain-box">
                                                                <p class="starail-JourneyBlock-infoRow p-price-margin">
                                                                    <a class="starail-JourneyBlock-more-trigger" style="line-height: 0px" href="#"><i
                                                                        class="starail-Icon-changes"></i>
                                                                        <asp:Label ID="lblTrainChangesMobile" runat="server" CssClass="pricecheap" />
                                                                    </a>
                                                                </p>
                                                                <div class="pricebox-tag">
                                                                    <img src="<%=siteURL %>images/from.png" class="from-price" alt="" />
                                                                    <asp:Label ID="lblCheapPrice" runat="server" CssClass="price-font"></asp:Label>
                                                                </div>
                                                                <p class="starail-JourneyBlock-infoRow p-price-margin">
                                                                    <i class="starail-Icon-time"></i><span class="starail-infoRow-time">
                                                                        <%#Convert.ToDateTime(Eval("TravelTimeDuration")).ToString("HH:mm")%></span>
                                                                    <a style="display: block;" class="bottom-arrow">
                                                                        <img src="<%=siteURL %>images/arrow-bottom.png" alt="" /></a> <a style="display: none;"
                                                                            class="top-arrow">
                                                                            <img src="<%=siteURL %>images/arrow-top.png" alt="" /></a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="starail-JourneyBlock-journeyDetails">
                                                            <div class="starail-JourneyBlock-content">
                                                                <asp:Repeater ID="GrdRouteInfo" runat="server">
                                                                    <ItemTemplate>
                                                                        <div class="starail-JourneyBlock-journeyDetails-row">
                                                                            <div class="starail-JourneyBlock-journeyDetails-provider">
                                                                                <p class="starail-JourneyBlock-journeyDetails-title">
                                                                                    <%#Eval("TrainNumber") != null ? Eval("TrainNumber").ToString() : string.Empty%>
                                                                                    <%# Eval("TrainCategory") != null ? Eval("TrainCategory").ToString() : string.Empty%></p>
                                                                            </div>
                                                                            <div class="starail-JourneyBlock-journeyDetails-details">
                                                                                <p class="starail-JourneyBlock-journeyDetails-title">
                                                                                    Departs:</p>
                                                                                <p>
                                                                                    <span>
                                                                                        <%#Convert.ToDateTime(Eval("DepartureTime")).ToString("HH:mm")%></span><span class="starail-u-hideMobile">
                                                                                            | </span><span>
                                                                                                <%#Eval("DepartureDate")%></span></p>
                                                                                <p>
                                                                                    <%#Eval("DepartureStationName")%></p>
                                                                            </div>
                                                                            <div class="starail-JourneyBlock-journeyDetails-details">
                                                                                <p class="starail-JourneyBlock-journeyDetails-title">
                                                                                    Arrives:</p>
                                                                                <p>
                                                                                    <span>
                                                                                        <%#Convert.ToDateTime(Eval("ArrivalTime")).ToString("HH:mm")%></span><span class="starail-u-hideMobile">
                                                                                            | </span><span>
                                                                                                <%#Eval("ArrivalDate")%></span></p>
                                                                                <p>
                                                                                    <%#Eval("ArrivalStationName")%></p>
                                                                            </div>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>
                                                        <div class="starail-JourneyBlock-content starail-u-hideMobile collapsiblediv" id="DivTr"
                                                            runat="server" style="display: none;">
                                                        </div>
                                                        <div class="starail-JourneyBlock-mobileTickets">
                                                            <asp:DropDownList ID="ddlToPrice" runat="server" CssClass="starail-Form-select starail-JourneyBlock-select" />
                                                            <a href="#" class="starail-Button starail-Button--fullMobile starail-Button--blue starail-JourneyBlock-confirm-trigger">
                                                                Confirm Ticket</a>
                                                        </div>
                                                        <div class="starail-JourneyBlock-footer starail-u-hideMobile collapsiblediv" style="display: none;">
                                                            <a class="starail-JourneyBlock-more-trigger" href="#"><i class="starail-Icon starail-Icon-pluscircle">
                                                            </i>More info</a> <a class="starail-JourneyBlock-less-trigger" href="#"><i class="starail-Icon starail-Icon-minuscircle">
                                                            </i>Hide journey details</a> <a href="#" class="starail-JourneyBlock-button starail-Button starail-rtnJourney-trigger is-disabled IR-GaCode"
                                                                onclick="return SendJCodeAndServiceID();">Next: Booking Details</a>
                                                        </div>
                                                        <div class="starail-JourneyBlock-footer starail-JourneyBlock-mobileFooter starail-u-hide">
                                                            <p class="starail-JourneyBlock-footer-ticketSelection">
                                                                <span>Class not found</span> <a href="#" class="js-lightboxOpen" data-lightbox-id="starail-ticket-info">
                                                                    More info<i class="starail-Icon-question"></i></a>
                                                            </p>
                                                            <div class="starail-JourneyBlock-mobileFooter-btns">
                                                                <a href="#" class="starail-Button starail-undoTicket-trigger">Undo</a>
                                                            </div>
                                                            <div class="starail-JourneyBlock-mobileFooter-btns">
                                                                <asp:HyperLink runat="server" ID="hlnkContinue" class="starail-Button starail-rtnJourney-trigger IR-GaCode"
                                                                    Text="Continue" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <div class="starail-TrainOptions-earlierLaterBtns">
                                            <asp:LinkButton runat="server" ID="lnkInBottomEarlier" CssClass="starail-Button starail-Button--blue IR-GaCode"
                                                OnClick="lnkInEarlier_Click"> <i class="starail-Icon-chevron-left"></i><i class="starail-Icon-double-chevron-left"></i>Earlier trains</asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkInBottomLater" CssClass="starail-Button starail-Button--blue starail-TrainOptions-later IR-GaCode"
                                                OnClick="lnkInLater_Click">Later trains <i class="starail-Icon-chevron-right"></i><i class="starail-Icon-double-chevron-right"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="starail-Grid-col starail-Grid-col--4of12 starail-u-hideMobile starail-Sidebar">
                                    <a href="#" class="starail-Button starail-Button--full starail-Sidebar-rtnJourney starail-rtnJourney-trigger is-disabled IR-GaCode"
                                        onclick="return SendJCodeAndServiceID();">Next:
                                        <%=journyTag%></a> <a href="#" class="starail-Button starail-Button--full starail-Sidebar-booking starail-booking-trigger starail-u-hide is-disabled IR-GaCode"
                                            onclick="return SendJCodeAndServiceID();">Next: Booking Details</a>
                                    <div class="starail-Tabs starail-Sidebar-tabs">
                                        <ul class="starail-Tabs-tabs">
                                            <li class="starail-Tabs-tab starail-Tabs-tab--active"><a href="#starail-routemap"
                                                class="js-starail-Tabs-trigger">
                                                <div>
                                                    Route Map
                                                </div>
                                            </a></li>
                                            <li class="starail-Tabs-tab"><a href="#starail-edittickets" class="js-starail-Tabs-trigger IR-GaCode">
                                                <div>
                                                    Edit Search
                                                </div>
                                            </a></li>
                                        </ul>
                                        <div id="starail-routemap" class="starail-Tabs-content">
                                            <div class="results-map">
                                                <div id="mymap" class="results-map" style="height: 300px; width: 100%;">
                                                </div>
                                            </div>
                                        </div>
                                        <div id="starail-edittickets" class="starail-Tabs-content starail-Tabs-content--hidden">
                                            <div class="starail-SearchTickets starail-SearchTickets--mini">
                                                <div class="starail-Box starail-Box--whiteMobile" runat="server" id="ucTrainSearchDiv">
                                                    <uc1:ucTrainSearch ID="ucTrainSearch" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    &nbsp;
                                    <div class="starail-Sidebar-bottomBtns">
                                        <a href="#" class="starail-Button starail-Button--full starail-Sidebar-rtnJourney starail-rtnJourney-trigger is-disabled IR-GaCode"
                                            onclick="return SendJCodeAndServiceID();">Next:
                                            <%=journyTag%></a> <a href="#" class="starail-Button starail-Button--full starail-Sidebar-booking starail-booking-trigger starail-u-hide is-disabled IR-GaCode"
                                                onclick="return SendJCodeAndServiceID();">Next: Booking Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="starail-TrainOptions-mobileNav starail-u-hideDesktop">
                        <div class="starail-TrainOptions-mobileNav-stage1">
                            <a href="default.aspx#rail-tickets" class="starail-Button starail-Button--blue IR-GaCode">
                                <i class="starail-Icon-chevron-left"></i>Edit Search</a> <a href="#" class="starail-Button starail-TrainOptions-mobileNav-rtnJourney starail-rtnJourney-trigger starail-Button--blue is-disabled">
                                    Next: Return <i class="starail-Icon-chevron-right"></i></a>
                        </div>
                        <div class="starail-TrainOptions-mobileNav-stage2 starail-u-hide">
                            <a href="#" class="starail-Button starail-Button--blue starail-TrainOptions-outbound-trigger">
                                <i class="starail-Icon-chevron-left"></i>Outbound</a> <a href="#" class="starail-Button starail-TrainOptions-mobileNav-booking starail-booking-trigger starail-Button--blue is-disabled">
                                    Your Details <i class="starail-Icon-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%=pricePoup%>
        <%--   Model Popup--%>
        <div style="display: none;">
            <asp:HyperLink ID="HyperLink2" runat="server" />
        </div>
        <asp:ModalPopupExtender ID="mdpexQuickLoad" runat="server" CancelControlID="btnCloseWin"
            PopupControlID="pnlQuckLoad" BackgroundCssClass="modalBackground" TargetControlID="HyperLink2" />
        <asp:Panel ID="pnlQuckLoad" runat="server">
            <div class="starail-popup">
                <div class="starail-BookingDetails-form">
                    <h2>
                        Lead Passenger Information</h2>
                    <div class="starail-Form-row selectlistmargin">
                        <label class="starail-Form-label" for="starail-country">
                            Lead passenger name <span class="starail-Form-required">*</span></label>
                        <div class="starail-Form-inputContainer">
                            <div class="starail-Form-inputContainer-col">
                                <asp:DropDownList ID="ddlTitle" runat="server" CssClass="starail-Form-select selectlist"
                                    TabIndex="95">
                                    <asp:ListItem Value="0">Title</asp:ListItem>
                                    <asp:ListItem Value="1">Dr.</asp:ListItem>
                                    <asp:ListItem Value="2">Mr.</asp:ListItem>
                                    <asp:ListItem Value="3">Miss.</asp:ListItem>
                                    <asp:ListItem Value="4">Mrs.</asp:ListItem>
                                    <asp:ListItem Value="5">Ms.</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqvalerror0" runat="server" ControlToValidate="ddlTitle"
                                    InitialValue="0" Display="Dynamic" ValidationGroup="cntnu" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row selectlistmargin">
                        <label class="starail-Form-label" for="starail-country">
                        </label>
                        <div class="starail-Form-inputContainer selectlistwidth">
                            <div class="starail-Form-inputContainer-col">
                                <asp:TextBox ID="txtFirstname" runat="server" MaxLength="15" CssClass="starail-Form-input"
                                    TabIndex="96" placeholder="First Name" />
                                <asp:RequiredFieldValidator ID="reqvalerror1" runat="server" ControlToValidate="txtFirstname"
                                    Display="Dynamic" ValidationGroup="cntnu" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row selectlistmargin">
                        <label class="starail-Form-label" for="starail-country">
                        </label>
                        <div class="starail-Form-inputContainer selectlistwidth">
                            <div class="starail-Form-inputContainer-col">
                                <asp:TextBox ID="txtLastname" runat="server" MaxLength="25" CssClass="starail-Form-input"
                                    TabIndex="97" placeholder="Last Name" />
                                <asp:RequiredFieldValidator ID="reqvalerror2" runat="server" ControlToValidate="txtLastname"
                                    Display="Dynamic" ValidationGroup="cntnu" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                        <label class="starail-Form-label" for="starail-country">
                            Country of residence <span class="starail-Form-required">*</span></label>
                        <div class="starail-Form-inputContainer selectlistwidth">
                            <div class="starail-Form-inputContainer-col">
                                <asp:DropDownList ID="ddlCountry" runat="server" CssClass="starail-Form-select" TabIndex="98">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqvalerror3" runat="server" ErrorMessage="*" ControlToValidate="ddlCountry"
                                    InitialValue="0" Display="Dynamic" CssClass="errMsg" ValidationGroup="cntnu" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row selectlistmargin">
                        <div id="divShowFareRules" class="fare-rules-list">
                        </div>
                        <div id="ShowCurrentDetail" style="display: none">
                        </div>
                        <hr style="margin: 0px" />
                    </div>
                    <div class="starail-Form-row selectlistmargin" style="text-align: center;">
                        <asp:Button ID="btnContinue" runat="server" CssClass="stapopup-btn starail-Button starail-Form-button IR-GaCode"
                            TabIndex="99" Text="Continue" ValidationGroup="cntnu" OnClick="btnContinue_Click" />
                        <asp:LinkButton ID="btnCloseWin" CssClass="stapopup-btn starail-Form-button IR-GaCode"
                            runat="server" Text="Cancel"></asp:LinkButton></div>
                </div>
        </asp:Panel>
        <%--   Bene--%>
        <asp:HiddenField ID="hdnFromRid" runat="server" />
        <asp:HiddenField ID="hdnFromsvcTyp" runat="server" />
        <asp:HiddenField ID="hdnFromPriceId" runat="server" />
        <asp:HiddenField ID="hdnFromBeNeClass" runat="server" />
        <asp:HiddenField ID="hdnToRid" runat="server" />
        <asp:HiddenField ID="hdnTosvcTyp" runat="server" />
        <asp:HiddenField ID="hdnToPriceId" runat="server" />
        <asp:HiddenField ID="hdnToBeNeClass" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="upnlTrainSearch"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="modalBackground progessposition">
        </div>
        <div class="progess-inner2">
            UPDATING RESULTS...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<script type="text/javascript">
    $(document).ready(function () {
        $(".tresult").each(function () {
            $(this).find(".taco :eq(0)").find("div").show();
        });
        $(".taco").click(function () {
            $(this).find("div").slideToggle();
        });
        $('input[type=radio]').click(function () {
            var totalprice = 0;
            if ($('input[id=rdoFrom]:checked').val() != undefined) {
                var strf = $('input[id=rdoFrom]:checked').val();
                var arrayf = strf.split(",");
                $('#MainContent_ucSResult_lbloutPrice').text(parseFloat(arrayf[4]).toFixed(2));
                totalprice = parseFloat(arrayf[4]).toFixed(2);
            }
            if ($('input[id=rdoTo]:checked').val() != undefined) {
                var strf = $('input[id=rdoTo]:checked').val();
                var arrayf = strf.split(",");
                $('#MainContent_ucSResult_lblinPrice').text(parseFloat(arrayf[4]).toFixed(2));
                totalprice = parseFloat(totalprice) + parseFloat(arrayf[4]);
            }
            $('#MainContent_ucSResult_lblTotalPrice').text(parseFloat(totalprice).toFixed(2));
        });

        //change ddl price value in mobile 
        getOutBoundmobilevalue();
        getInBoundmobilevalue();
        $('.starail-JourneyBlock-mobileTickets').change(function () {
            getOutBoundmobilevalue();
            getInBoundmobilevalue();
        });
    });
</script>
<script type="text/javascript">
    function getOutBoundmobilevalue() {
        $("#outbound").find(".starail-JourneyBlock").each(function (key, value) {
            if ($(this).find("#MainContent_ucSResult_rptoutBeNe_ddlFromPrice_" + key + " option:selected").length > 0) {
                var pricevalue = $(this).find("#MainContent_ucSResult_rptoutBeNe_ddlFromPrice_" + key + " option:selected").val();
                pricevalue = pricevalue + ",outbound";
                $(this).find("#MainContent_ucSResult_rptoutBeNe_hlnkContinue_" + key).attr("onclick", "return SendJCodeAndServiceIDMobile('" + pricevalue + "')");
            }
        });
    }

    function getInBoundmobilevalue() {
        $("#inbound").find(".starail-JourneyBlock").each(function (key, value) {
            if ($(this).find("#MainContent_ucSResult_rptinBeNe_ddlToPrice_" + key + " option:selected").length > 0) {
                var pricevalue = $(this).find("#MainContent_ucSResult_rptinBeNe_ddlToPrice_" + key + " option:selected").val();
                pricevalue = pricevalue + ",inbound";
                $(this).find("#MainContent_ucSResult_rptinBeNe_hlnkContinue_" + key).attr("onclick", "return SendJCodeAndServiceIDMobile('" + pricevalue + "')");
            }
        });
    }

    function SendJCodeAndServiceID() {
        console.log("yes");
        if (typeof ($('input[id=rdoFrom]:checked').val()) != "undefined") {
            var strf = $('input[id=rdoFrom]:checked').val();
            var arrayf = strf.split(",");
            $("#MainContent_ucSResult_hdnFromRid").val(arrayf[0]);
            $("#MainContent_ucSResult_hdnFromsvcTyp").val(arrayf[1]);
            $("#MainContent_ucSResult_hdnFromPriceId").val(arrayf[2]);
            $("#MainContent_ucSResult_hdnFromBeNeClass").val(arrayf[3]);
            var fare = $('#starail-ticket-info' + arrayf[2]).find('.toogle').html();
            $('#divShowFareRules').html(fare);
        }

        if (typeof ($('input[id=rdoTo]:checked').val()) != "undefined") {
            var strT = $('input[id=rdoTo]:checked').val();
            var arrayT = strT.split(",");
            $("#MainContent_ucSResult_hdnToRid").val(arrayT[0]);
            $("#MainContent_ucSResult_hdnTosvcTyp").val(arrayT[1]);
            $("#MainContent_ucSResult_hdnToPriceId").val(arrayT[2]);
            $("#MainContent_ucSResult_hdnToBeNeClass").val(arrayT[3]);
            var fare = $('#starail-ticket-info' + arrayf[2]).find('.toogle').html();
            $('#divShowFareRules').html(fare);
        }
        checkSelectedPrice();
        return false;
    }

    function checkSelectedPrice() {
        var count = 0;
        $('.csscount').each(function () {
            count++;
        });
        if (count == 2 && $('input[id=rdoTo]:checked').val()) {
            $find('MainContent_ucSResult_mdpexQuickLoad').show();
        }
        if (count == 1 && $('input[id=rdoFrom]:checked').val()) {
            $find('MainContent_ucSResult_mdpexQuickLoad').show();
        }
        if ($('input[id=rdoFrom]:checked').val() == false && $('input[id=rdoTo]:checked').val() == false) {
            alert("Pelase select at least one price");
        }
    }

    function SendJCodeAndServiceIDMobile(strf) {
        var arrayf = strf.split(",");
        if (arrayf[5] == 'outbound') {
            $("#MainContent_ucSResult_hdnFromRid").val(arrayf[0]);
            $("#MainContent_ucSResult_hdnFromsvcTyp").val(arrayf[1]);
            $("#MainContent_ucSResult_hdnFromPriceId").val(arrayf[2]);
            $("#MainContent_ucSResult_hdnFromBeNeClass").val(arrayf[3]);
        }
        if (arrayf[5] == 'inbound') {
            $("#MainContent_ucSResult_hdnToRid").val(arrayf[0]);
            $("#MainContent_ucSResult_hdnTosvcTyp").val(arrayf[1]);
            $("#MainContent_ucSResult_hdnToPriceId").val(arrayf[2]);
            $("#MainContent_ucSResult_hdnToBeNeClass").val(arrayf[3]);
        }
        checkSelectedPrice();
        return false;
    } 
</script>
<%--gmap--%>
<script src="Scripts/gmap.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&language=en" type="text/javascript"></script>
<script type="text/javascript">
    var geocoder;
    var maps;
    $(window).load(function () {
        LoadMap();
    });
    function LoadMap() {
        var start = document.getElementById('txtFrom').value.replace('TGV', '');
        var matchSt = null;
        if (start.indexOf('(') > "-1") {
            var regExp1 = /\(([^)]+)\)/;
            matchSt = regExp1.exec(start);
        }
        if (((start.toLowerCase()).indexOf('st ') > "-1") || ((start.toLowerCase()).indexOf('le ') > "-1") || ((start.toLowerCase()).indexOf('la ') > "-1") || ((start.toLowerCase()).indexOf('s.') > "-1")) {
            start = (start.replace(" ", "-"));
            start = (start.replace(".", ""));
            start = (start.replace(/'/g, ""));
        } else {
            if (start.indexOf(' ') > "-1") {
                start = (start.substring(0, start.indexOf(' ')));
            }
        }
        if ((start.toLowerCase().indexOf('naples') > "-1") || (start.toLowerCase().indexOf('alba') > "-1")) {
            start = start + " italy";
        }

        if (matchSt != null)
            start = start + " " + matchSt[1];
        /***************************************************/
        var end = document.getElementById('txtTo').value.replace('TGV', '');
        var matchEnd = null;
        if (end.indexOf('(') > "-1") {
            var regExp = /\(([^)]+)\)/;
            matchEnd = regExp.exec(end);
        }
        if (((end.toLowerCase()).indexOf('st ') > "-1") || ((end.toLowerCase()).indexOf('le ') > "-1") || ((end.toLowerCase()).indexOf('la ') > "-1") || ((end.toLowerCase()).indexOf('s.') > "-1")) {
            end = (end.replace(" ", "-"));
            end = (end.replace(".", ""));
            end = (end.replace(/'/g, ""));
        } else {
            if (end.indexOf(' ') > "-1") {
                end = (end.substring(0, end.indexOf(' ')));
            }
        }
        if ((end.toLowerCase().indexOf('naples') > "-1") || (end.toLowerCase().indexOf('alba') > "-1")) {
            end = end + " italy";
        }
        var geocoder = new window.google.maps.Geocoder();
        initializeGMap();


        var FromLatitude = '<%=FromLatitude%>';
        var FromLongitude = '<%=FromLongitude%>';
        var ToLatitude = '<%=ToLatitude%>';
        var ToLongitude = '<%=ToLongitude%>';

        console.log("start: " + start + " :: end: " + end);
        console.log("FromLatitude: " + FromLatitude + " :: FromLongitude: " + FromLongitude + " :: ToLatitude: " + ToLatitude + " :: ToLongitude: " + ToLongitude);

        if (FromLatitude != ' ' && FromLongitude != ' ') {
            var fromstart = new google.maps.LatLng(parseFloat(FromLatitude), parseFloat(FromLongitude));
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': fromstart }, function (results, status) {
                if (status == window.google.maps.GeocoderStatus.OK) {
                    setFromMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), start);
                    drawPolyline();
                }
            });
        }
        else {
            geocoder.geocode({ 'address': start }, function (results, status) {
                if (status == window.google.maps.GeocoderStatus.OK) {
                    setFromMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), start);
                    drawPolyline();
                }
            });
        }

        if (ToLatitude != ' ' && ToLongitude != ' ') {
            var toend = new google.maps.LatLng(parseFloat(ToLatitude), parseFloat(ToLongitude));
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': toend }, function (results, status) {
                if (status == window.google.maps.GeocoderStatus.OK) {
                    setToMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), end);
                    drawPolyline();
                }
            });
        }
        else {
            geocoder.geocode({ 'address': end }, function (results, status) {
                if (status == window.google.maps.GeocoderStatus.OK) {
                    setToMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), end);
                    drawPolyline();
                }
            });
        }
    }
</script>
