﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Gadget.aspx.cs" Inherits="Gadget" %>

<%@ Register Src="~/UserControls/ucTrainSearch.ascx" TagName="TrainSearch" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
          <%--<uc1:TrainSearch ID="TrainSearch1" runat="server" /> --%>


   <div class="starail-Tabs starail-Sidebar-tabs" style="width:300px;"> 
    <ul class="starail-Tabs-tabs">
        <li class="starail-Tabs-tab starail-Tabs-tab--active">
            <a href="#starail-routemap" class="js-starail-Tabs-trigger">
                <div>
                    Route Map
                </div>
            </a>
        </li>
        <li class="starail-Tabs-tab">
            <a href="#starail-edittickets" class="js-starail-Tabs-trigger">
                <div>
                    Edit Search
                </div>
            </a>
        </li>
    </ul> 
    <div id="starail-routemap" class="starail-Tabs-content">
       <div class="results-map">
        <div id="mymap" class="results-map" style="height: 300px; width: 100%;">
        </div>
    </div>
    </div> 
    <div id="starail-edittickets" class="starail-Tabs-content starail-Tabs-content--hidden">
        <div class="starail-SearchTickets starail-SearchTickets--mini"> 
        <div class="starail-Box starail-Box--whiteMobile"> 
          <uc1:TrainSearch ID="ucTrainSearch" runat="server" /> 
</div> 
        </div>
    </div>
</div>
  <script src="Scripts/gmap.js" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&language=en"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            LoadMap();
            setTimeout(LoadMap(), 500);
        });
        function LoadMap() {
            var start = document.getElementById('txtFrom').value;
            var matchSt = null;
            if (start.indexOf('(') > "-1") {
                var regExp1 = /\(([^)]+)\)/;
                matchSt = regExp1.exec(start);
            }
            if (((start.toLowerCase()).indexOf('st ') > "-1") || ((start.toLowerCase()).indexOf('le ') > "-1") || ((start.toLowerCase()).indexOf('la ') > "-1") || ((start.toLowerCase()).indexOf('s.') > "-1")) {
                start = (start.replace(" ", "-"));
                start = (start.replace(".", ""));
                start = (start.replace(/'/g, ""));
            } else {
                if (start.indexOf(' ') > "-1") {
                    start = (start.substring(0, start.indexOf(' ')));
                }
            }
            if ((start.toLowerCase().indexOf('naples') > "-1") || (start.toLowerCase().indexOf('alba') > "-1")) {
                start = start + " italy";
            }
            if (matchSt != null)
                start = start + " " + matchSt[1];
            /***************************************************/
            var end = document.getElementById('txtTo').value;
            var matchEnd = null;
            if (end.indexOf('(') > "-1") {
                var regExp = /\(([^)]+)\)/;
                matchEnd = regExp.exec(end);
            }
            if (((end.toLowerCase()).indexOf('st ') > "-1") || ((end.toLowerCase()).indexOf('le ') > "-1") || ((end.toLowerCase()).indexOf('la ') > "-1") || ((end.toLowerCase()).indexOf('s.') > "-1")) {
                end = (end.replace(" ", "-"));
                end = (end.replace(".", ""));
                end = (end.replace(/'/g, ""));
            } else {
                if (end.indexOf(' ') > "-1") {
                    end = (end.substring(0, end.indexOf(' ')));
                }
            }
            if ((end.toLowerCase().indexOf('naples') > "-1") || (end.toLowerCase().indexOf('alba') > "-1")) {
                end = end + " italy";
            }
            if (matchEnd != null)
                end = end + " " + matchEnd[1];
            var geocoder = new window.google.maps.Geocoder();
            initializeGMap();
            geocoder.geocode({ 'address': start }, function (results, status) {
                if (status == window.google.maps.GeocoderStatus.OK) {
                    setFromMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), start);
                    drawPolyline();
                }
            });
            geocoder.geocode({ 'address': end }, function (results, status) {
                if (status == window.google.maps.GeocoderStatus.OK) {
                    setToMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), end);
                    drawPolyline();
                }
            });
        }

    </script>
</asp:Content>
