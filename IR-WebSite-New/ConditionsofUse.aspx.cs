﻿using System;
using Business;
using System.Web.UI;
using System.Linq;
using System.Configuration;

public partial class ConditionsofUse : Page
{
    readonly ManageBooking _master = new ManageBooking();
    readonly Masters _masterPage = new Masters();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    readonly db_1TrackEntities _db = new db_1TrackEntities();
    private Guid _siteId;
    private Guid pageID;
    public string siteURL;
    public string script;
    public string PageTitle = "";
    public string PageDescription = "";
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(_siteId);
            if (Page.RouteData.Values["PageId"] != null)
            {
                pageID = (Guid)Page.RouteData.Values["PageId"];
                BindConditions(_siteId);

                #region Seobreadcrumbs
                Guid id = (Guid)Page.RouteData.Values["PageId"];
                var dataSeobreadcrumbs = new ManageSeo().Getlinkbody(id, _siteId);
                if (dataSeobreadcrumbs != null && dataSeobreadcrumbs.IsActive)
                    litSeobreadcrumbs.Text = dataSeobreadcrumbs.SourceCode;
                #endregion
            }

            var pid = _db.tblWebMenus.FirstOrDefault(x => x.Name.ToLower() == "home");
            if (pid != null)
                PageContent(pid.ID, _siteId);

        }
    }
    public void PageContent(Guid pageID, Guid siteID)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageID && x.SiteID == siteID);
            if (result != null)
            {
                var url = result.Url;
                var oPage = _masterPage.GetPageDetailsByUrl(url);

                //Banner
                string[] arrListId = oPage.BannerIDs.Split(',');
                var idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _masterPage.GetBannerImgByID(idList);
                int countdata = list.Count();
                int index = new Random().Next(countdata);
                var newlist = list.Skip(index).FirstOrDefault();
                if (newlist != null && newlist.ImgUrl != null)
                    img_Banner.Src = adminSiteUrl + "" + newlist.ImgUrl;
                else
                    img_Banner.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void BindConditions(Guid siteId)
    {
        var result = _master.GetConditionsBySiteid(siteId).Where(x => x.IsActive == true);
        if (result != null)
        {
            PageTitle = result.Select(x => x.Title).FirstOrDefault();
            PageDescription = result.Select(x => x.Description).FirstOrDefault();
        }
        rptConditions.DataSource = result;
        rptConditions.DataBind();
    }
}