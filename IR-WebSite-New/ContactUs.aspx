﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeFile="ContactUs.aspx.cs"
    Inherits="ContactUs" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .address-block
        {
            width: 100% !important;
        }
        .clsAbs
        {
            display: none;
        }
        .download
        {
            padding: 6px 9px;
            background: #e7f7d3;
            border: 1px solid #6c3;
            font-size: 13px !important;
        }
        .col-lg-8, .col-lg-4
        {
            float: left;
        }
        .col-lg-8
        {
            width: 60%;
        }
        .col-lg-4
        {
            width: 40%;
        }
        .pl10
        {
            padding-left: 10px;
        }
        .address-block .fullrow .imgblock
        {
            padding: 0px !important;
        }
        .address-block .fullrow span
        {
            line-height: 0px !important;
        }
        #contactHomeTxt
        {
            padding-left: 36px;
            width: 300px !important;
        }
        #contactCallTxt
        {
            padding-left: 36px;
            width: 300px !important;
        }
        #contactEmailTxt
        {
            padding-left: 36px;
            width: 300px !important;
        }
        .address-block .fullrow
        {
            float: none;
        }
        .con-txt .clsAbs
        {
            left: 50px;
            position: absolute;
            top: -10px;
        }
        @media only screen and (min-width: 639px)
        {
            .starail-u-size8of12, .starail-u-size4of12
            {
                float: left;
            }
            #contactHomeTxt
            {
                width: 100% !important;
            }
            #contactCallTxt
            {
                width: 100% !important;
            }
            #contactEmailTxt
            {
                width: 100% !important;
            }
        }
        @media only screen and (max-width: 639px)
        {
            .m-hidden
            {
                display: none !important;
            }
            #contactHomeTxt
            {
                width: 100% !important;
            }
            #contactCallTxt
            {
                width: 100% !important;
            }
            #contactEmailTxt
            {
                width: 100% !important;
            }
        }
    </style>
    <script type="text/javascript">
        $(function () {
            setMenuActive("CU");
            $('input[type="text"]').focusout(function () {
                if ($(this).hasClass('starail-Form-error') || $(this).val() != "") {
                    $(this).removeClass('starail-Form-error');
                } else { $(this).addClass('starail-Form-error'); }
                if ($('#MainContent_reEmail').css('display') == 'none') {
                    $('#MainContent_txtEmail').removeClass('starail-Form-error');
                }
            });
            $("#aContact").addClass("active");

            var eMail = ($('#contactEmailTxt').html());
            function extractEmails(text) {
                return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
            }

            if (eMail != null) {
                if (eMail.indexOf('@') > "-1") {
                    var emailAddressTo = extractEmails(eMail);
                    document.getElementById("MainContent_hdnEmail").value = emailAddressTo.toString().split(',')[0];
                }
            }
        });
    </script>
    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function SaveContactUs() {
            var returnback = true;
            $('input.required').each(function () {
                if ($(this).val().trim() != '') {
                    $(this).removeClass('starail-Form-error');
                } else {
                    $(this).addClass('starail-Form-error');
                    returnback = false;
                }
            });
            if ($('#MainContent_reEmail').css('display') != 'none') {
                $('#MainContent_txtEmail').addClass('starail-Form-error');
                returnback = false;
            }
            return returnback;
        }     
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
    <div class="starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Section starail-HomeHero starail-Section--nopadding" style="overflow: visible;">
                <img class="starail-HomeHero-img dots-header" alt="." id="img_Banner" runat="server" />
            </div>
        </div>
    </div>
    <br />
            <div class="Breadcrumb"><asp:Literal runat="server" ID="litSeobreadcrumbs"></asp:Literal></div>
    <div class="starail-Box starail-Box--whiteMobile starail-ContactForm">
        <h3>
            <span id="ContentHead" runat="server"></span>
        </h3>
        <p>
            <span id="ContentText" runat="server"></span>
        </p>
        <div class="starail-Form starail-Form--onBoxNarrow starail-ContactForm-form">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" CssClass="starail-Form-required" />
                </div>
            </asp:Panel>
            <div class="floatt starail-u-size8of12">
                <div class="starail-Form-row">
                    <label class="starail-Form-label m-hidden" for="starail-firstname">
                        Name <span class="starail-Form-required">*</span></label>
                    <div class="starail-Form-inputContainer">
                        <asp:TextBox ID="txtName" class="required starail-Form-input" runat="server" placeholder="Name"
                            ValidationGroup="vs" />
                        <asp:RequiredFieldValidator ID="rfNm" CssClass="required" Display="Dynamic" runat="server"
                            ControlToValidate="txtName" ValidationGroup="vs"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="starail-Form-row">
                    <label class="starail-Form-label m-hidden" for="starail-firstname">
                        Email <span class="starail-Form-required">*</span>
                    </label>
                    <div class="starail-Form-inputContainer">
                        <asp:TextBox ID="txtEmail" class="required starail-Form-input" runat="server" placeholder="Email Address"
                            ValidationGroup="vs" />
                        <asp:RequiredFieldValidator ID="reqEmailAddress" CssClass="required" Display="Dynamic"
                            runat="server" ControlToValidate="txtEmail" ValidationGroup="vs"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator CssClass="required" ID="reEmail" Display="Dynamic"
                            runat="server" ControlToValidate="txtEmail" ValidationExpression="^(\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)|(Email Address)$"
                            ValidationGroup="vs"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="starail-Form-row">
                    <label class="starail-Form-label m-hidden" for="starail-firstname">
                        Phone no. <span class="starail-Form-required">*</span></label>
                    <div class="starail-Form-inputContainer">
                        <asp:TextBox ID="txtPhn" class="required starail-Form-input" runat="server" placeholder="Phone No"
                            ValidationGroup="vs" MaxLength="15" onkeypress="return isNumberKey(event)" />
                        <asp:RequiredFieldValidator CssClass="required" ID="rfPhone" Display="Dynamic" runat="server"
                            ControlToValidate="txtPhn" ValidationGroup="vs"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="starail-Form-row" >
                    <label class="starail-Form-label m-hidden" for="starail-firstname">
                        Order no. <span class="starail-Form-required"></span>
                    </label>
                    <div class="starail-Form-inputContainer">
                        <asp:TextBox ID="txtOrderNo" class="starail-Form-input" runat="server" placeholder="Order number (if applicable)"
                            onkeypress="return isNumberKey(event)" />
                    </div>
                </div>
                <div class="starail-Form-row">
                    <label class="starail-Form-label m-hidden" for="starail-firstname">
                        Message <span class="starail-Form-required"></span>
                    </label>
                    <div class="starail-Form-inputContainer">
                        <asp:TextBox ID="txtDesp" class="starail-Form-input" runat="server" TextMode="MultiLine" />
                        <asp:HiddenField ID="hdnEmail" runat="server" />
                    </div>
                </div>
            </div>
            <div class="floatt starail-u-size4of12 pl10">
                <div id="ContactPanel" class="address-block" runat="server">
                </div>
            </div>
            <div class="clear">
            </div>
            <hr />
            <div class="starail-Form-row">
                <asp:Button ID="btnSubmit" class="starail-Button starail-Form-button IR-GaCode" runat="server"
                    Text="Submit" ValidationGroup="vs" OnClick="btnSubmit_Click" OnClientClick="return SaveContactUs();" />
            </div>
        </div>
    </div>
    <div id="callBlock" class="map-block" style="float: right; display: none;" runat="server">
    </div>
</asp:Content>
