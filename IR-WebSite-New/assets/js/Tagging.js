﻿(function ($) {
    $.fn.TaggScripts = function (IsIR, IsSTA) {

        var path = window.location.href.split("?")[0].split("#")[0];
        /*********************/
       /* console.log(IsIR + " :: " + IsSTA);*/
        /*Start STA*/
        if (IsSTA=="True") {
            $('input[type="submit"]').click(function () {
                dataLayer.push({
                    'event': 'click',
                    'variable_name': "'" + path + "'"
                });
            });

            $(".starail-Icon-datepicker").click(function () {
                var id = $(this).parent().find("input[type=text]").attr('id');
                $("#" + id).datepicker('show');
            });
        }
        else if (IsIR) {
            /***********************************************************************/
            /*******************WORKING ONLY FOR LIVE ENVIRONMENT*******************/
            /***********************************************************************/
            
            $(".switchRadioGroup :radio").each(function (key, value) {
            var oldAttr = $(value).attr("onclick");
            oldAttr = oldAttr != undefined ? $.trim(oldAttr) : '';
            var text = $.trim($(value).next('label').text());
            text = text.replace(/[\t\n  ]+/g, ' ');
            $(value).attr("onclick", "ga('send', 'event', 'Booking Engine', '" + text + "', '" + path + "');" + oldAttr + "");
            });

            $('.IR-GaCode').each(function (key, value) {
            $(value).removeAttr("data-ga-category data-ga-action data-ga-label");
            var oldAttr = $(value).attr("onclick");
            oldAttr = oldAttr != undefined ? $.trim(oldAttr) : '';
            if (oldAttr.match(/Booking Engine/i) == null) {
            var text = value["value"] == undefined ? $.trim($(value).text()) : $.trim(value["value"]);
            text = text.replace(/[\t\n  ]+/g, ' ');
            pagename = 'Booking Engine';
            if (text == 'Go')
            pagename = 'Home';
            else if (path.match(/contact-us/i) && text == 'Submit')
            pagename = 'Contact Us';
            else if ($(value).children().hasClass('starail-ImageLink-overlay')) {
            pagename = 'Home';
            text = $(value).attr("irgacodetext");
            }
            $(value).attr("onclick", "ga('send', 'event', '" + pagename + "', '" + text + "', '" + path + "');" + oldAttr + "");
            }
            });
             
        }
        /*End IR*/
        /*********************/
    }
} (jQuery));