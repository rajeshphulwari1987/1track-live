(function($) {
    $.fn.CustomSearch = function(options) {
        var defaults = {
            overflow_y: "hidden",
            overflow_x: "hidden",
            textColor: "#000",
            width: "auto",
            height: "400px",
            backgroundColor: "#fff",
            z_index: 100,
            position: 'absolute',
            border: "1px solid #111",
            font_size: "1em",
            ul_li_padding: "5px",
            ul_margin: "0px",
            li_cursor: "pointer",
            ul_background: "#CBCBCB",
            getTextFromTitle: true,
            modeStyle: 'auto', //'manual || atuo'
            manual_B_color: '#0051FF',
            manual_B_Size: '13px',
            animationDuration: 500,
            recordno: 10,
            NoRecord: "Record Not Found",
            NoRecordcolor: "Red",
            reload: "no",
            Data_list: null
        };
        var settings = $.extend({}, defaults, options);
        var maxCount = 0;
        var t_txt = "";
        var t_length = 0;
        var tagthis = $(this);
        var tagdiv = $("<div/>");
        var tagul = $("<ul/>");
        var ix_Show = true;
        tagthis.after(tagdiv);
        tagdiv.prepend(tagul);
        tagthis.focus();
        if (settings.width == 'auto') {
            settings.width = tagthis.css('width');
        }
        tagdiv.css({
            "overflow-y": settings.overflow_y,
            "overflow-x": settings.overflow_x,
            "font-size": settings.font_size,
            "position": settings.position,
            "z-index": settings.z_index,
            "background": settings.backgroundColor,
            "width": settings.width,
            "height": settings.overflow_y == 'scroll' ? settings.height : 'auto',
            "border": settings.border
        });
        tagul.css({
            "list-style-type": "none",
            "padding": settings.ul_li_padding,
            "margin": settings.ul_margin
        });
        init();

        function init() {
            t_txt = tagthis.val();
            t_length = t_txt.length;
            var newarray = new Array();
            if (settings.Data_list == null || t_length == 0) {
                tagdiv.css("display", "none");
                ix_Show = true;
                return;
            }
            var matchfound = false;
            $.each(settings.Data_list, function(key, value) {
                if (value.substring(0, t_length).match(new RegExp(t_txt, 'i'))) {
                    newarray.push(value);
                    matchfound = true;
                }
            });
            if (!matchfound) {
                //newarray.push(settings.NoRecord);
                tagdiv.css("display", "none");
                tagdiv.hide();
            }
            if (matchfound) {
                if (newarray != null && newarray.length > 0) {
                    if (settings.recordno == '0') {
                        maxCount = newarray.length;
                    } else {
                        maxCount = newarray.length > settings.recordno ? settings.recordno : newarray.length;
                    }
                } else {
                    maxCount = 0;
                }
                tagul.find('li').remove();
                if (maxCount == 0) {
                    tagdiv.css("display", "none");
                } else {
                    tagdiv.css("display", "block");
                }

                tagdiv.hide();
                for (var i = 0; i < maxCount; i++) {
                    var Datavalue = newarray[i];
                    var tagli = $("<li/>");
                    tagul.prepend(tagli);
                    tagli.css({
                        "padding-top": settings.ul_li_padding,
                        "padding-bottom": settings.ul_li_padding,
                        "cursor": settings.li_cursor,
                    });
                    if (settings.getTextFromTitle) {
                        tagli.attr('title', Datavalue);
                    }
                    if (settings.modeStyle == "auto") {
                        var tagspantxt = $("<span/>");
                        tagli.prepend(tagspantxt);
                        tagli.append(tagspantxt);
                        tagspantxt.css("color", settings.textColor);
                        //                    if (Datavalue == settings.NoRecord) {
                        //                        tagspantxt.css('color', settings.NoRecordcolor);
                        //                        tagspantxt.append(Datavalue); //data
                        //                    } else {
                        tagspantxt.text(Datavalue); //data
                        //                    }
                    } else if (settings.modeStyle == "manual") {
                        var tagspan = $("<span/>");
                        var tagb = $("<b/>");
                        tagli.prepend(tagspan);
                        tagspan.prepend(tagb);
                        tagspan.css("color", settings.textColor);
                        tagb.css("color", settings.manual_B_color);
                        //                    if (Datavalue == settings.NoRecord) {
                        //                        tagspan.css('color', settings.NoRecordcolor);
                        //                        tagspan.append(Datavalue); //data
                        //                    } else {
                        tagb.text(Datavalue.substring(0, t_length)); //data
                        tagspan.append(Datavalue.substring(t_length, Datavalue.length)); //data
                        //                    }
                        tagli.append(tagspan);
                    }
                    tagli.mouseover(function() {
                        tagul.find('li').css("background", "#fff");
                        tagul.find('li').removeAttr('selected');
                        $(this).attr('selected', 'selected');
                        $(this).css("background", settings.ul_background);
                    });
                    tagli.click(function() {
                        var click_val = $(this).text();
                        if (click_val != settings.NoRecord) {
                            tagthis.val(click_val);
                            tagdiv.css("display", "none");
                            ix_Show = true;
                        }
                        tagthis.focus();
                    });
                    tagul.find('li:eq(0)').trigger('mouseover');
                }
                if (ix_Show) {
                    tagdiv.slideDown(settings.animationDuration);
                    ix_Show = false;
                } else {
                    tagdiv.show();
                }
                if (settings.reload == "yes") {
                    tagdiv.hide()
                    settings.reload = "no";
                }
            }
        }
        tagthis.on("keyup", function(e) {
            if (e.keyCode == 13 || e.keyCode == 9) {
                t_length = tagthis.val().length;
            }
            if (t_length != tagthis.val().length) {
                init();
            }
            if (settings.overflow_y == "scroll") {
                var pos_c = tagul.find("li[selected='selected']").get(0).scrollHeight * (tagul.find("li[selected='selected']").index());
                var Pos_x = tagul.find("li").height() + tagul.find("li[selected='selected']").position().top;
                var Pos_y = tagdiv.height() - tagdiv.position().top;
                if (Pos_x > Pos_y || pos_c == 0 || Pos_x < 0)
                    tagdiv.scrollTop(pos_c);
            }

        });
        tagthis.on("keydown", function(e) {
            if (e.shiftKey || e.ctrlKey || e.altKey) {
                return;
            }
            switch (e.keyCode) {
                case 9:
                    Get_selected_value(tagul, e);
                    break;
                case 13:
                    Get_selected_value(tagul, e);
                    break;
                case 40: //down
                    DownArrowKey_value(tagul);
                    break;
                case 38: //up
                    UpArrowKey_value(tagul);
                    break;
            }
        });

        function UpArrowKey_value(ulTag) {
            var active_index = ulTag.find("li[selected='selected']").index();
            var max_index = ulTag.find("li").size() - 1;
            if (max_index == 0) {
                ulTag.find('li:eq(' + max_index + ')').trigger('mouseover');
            } else {
                ulTag.find('li:eq(' + (active_index - 1) + ')').trigger('mouseover');
            }
        }

        function DownArrowKey_value(ulTag) {
            var active_index = ulTag.find("li[selected='selected']").index();
            var max_index = ulTag.find("li").size() - 1;
            if (max_index == active_index) {
                ulTag.find('li:eq(0)').trigger('mouseover');
            } else {
                ulTag.find('li:eq(' + (active_index + 1) + ')').trigger('mouseover');
            }
        }

        function Get_selected_value(ulTag, e) {
            if (matchfound) {
                $.each(ulTag.find('li'), function() {
                    var keydown_val = $(this).attr('selected') == 'selected';
                    if (keydown_val) {
                        var currentval = $(this).text();
                        if (currentval != settings.NoRecord) {
                            tagthis.val(currentval);
                            tagdiv.css("display", "none");
                            ix_Show = true;
                            e.preventDefault();
                        }
                        tagthis.focusin();
                    }
                });
            }
        }
    }
})(jQuery);