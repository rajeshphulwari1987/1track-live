﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Pages.aspx.cs" Inherits="Pages" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators" id="listItem" runat="server" clientidmode="Static">
        </ol>
        <div class="carousel-inner">
            <asp:Repeater ID="rptBanner" runat="server">
                <ItemTemplate>
                    <div class="item">
                        <img src='<%=adminSiteUrl%><%#Eval("ImgUrl")%>' class="scale-with-grid" alt='<%#Eval("AltTag")%>' title='<%#Eval("ImageTitle")%>' border="0"
                            width="100%" style="height: 253px" /></div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
    <div class="starail-Grid-col starail-Grid-col--nopadding" style="display: block;">
        <h1>
            <asp:Literal runat="server" ID="headerTitle" />
        </h1>
        <div style="float: right; width: 236px; padding-left: 10px;" id="dvContent" runat="server"
            visible="false">
            <img runat="server" id="imgGinfo" />
        </div>
        <div runat="server" id="divDescription" style="padding: 3px; font-size: 13px;">
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('div.item').eq(0).addClass('active').end();
        });
        function activeCrousal() {
            $(".carousel-inner div:first-child").addClass("active");
        }
    </script>
</asp:Content>
