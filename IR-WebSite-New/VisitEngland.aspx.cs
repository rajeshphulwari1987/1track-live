﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business;
using System.Web.UI;

public partial class VisitEngland : Page
{
    readonly Masters _master = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string adminSiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string siteURL;
    private Guid siteId;
    public string script;
    public static string currency;
    public string currId;
    public List<GetAllProductORSpecialTrain> listnull = new List<GetAllProductORSpecialTrain>();
    private FrontEndManagePass oManageClass = new FrontEndManagePass();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            script = new Masters().GetQubitScriptBySId(siteId);
            GetCurrency();
            if (Page.RouteData.Values["PageId"] != null)
            {
                var pageId = (Guid)Page.RouteData.Values["PageId"];
                PageContent(pageId, siteId);

                #region Seobreadcrumbs
                var dataSeobreadcrumbs = new ManageSeo().Getlinkbody(pageId, siteId);
                if (dataSeobreadcrumbs != null && dataSeobreadcrumbs.IsActive)
                    litSeobreadcrumbs.Text = dataSeobreadcrumbs.SourceCode;
                #endregion
            }
        }
    }

    public void PageContent(Guid pageId, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageId && x.SiteID == siteId);
            if (result != null)
            {
                if (!string.IsNullOrEmpty(result.VEbanner))
                {
                    var data = _master.GetImageListEdit(Convert.ToInt32(result.VEbanner));
                    imgbanner.Attributes.Add("src", adminSiteUrl + data.ImagePath);
                }
                if (!string.IsNullOrEmpty(result.VEptpimg))
                {
                    var data = _master.GetImageListEdit(Convert.ToInt32(result.VEptpimg));
                    imgptp.Attributes.Add("src", adminSiteUrl + data.ImagePath);
                }
                if (!string.IsNullOrEmpty(result.VEmap))
                {
                    var data = _master.GetImageListEdit(Convert.ToInt32(result.VEmap));
                    imgmap.Attributes.Add("src", adminSiteUrl + data.ImagePath);
                }
                if (!string.IsNullOrEmpty(result.VEmaps1))
                {
                    var data = _master.GetImageListEdit(Convert.ToInt32(result.VEmaps1));
                    imgmaps1.Attributes.Add("src", adminSiteUrl + data.ImagePath);
                }
                if (!string.IsNullOrEmpty(result.VEmaps2))
                {
                    var data = _master.GetImageListEdit(Convert.ToInt32(result.VEmaps2));
                    imgmaps2.Attributes.Add("src", adminSiteUrl + data.ImagePath);
                }

                if (!string.IsNullOrEmpty(result.VEmaplink))
                    VEmaplink.Attributes.Add("href", result.VEmaplink);
                if (!string.IsNullOrEmpty(result.VEmaps1link))
                    VEmaps1link.Attributes.Add("href", result.VEmaps1link);
                if (!string.IsNullOrEmpty(result.VEmaps2link))
                    VEmaps2link.Attributes.Add("href", result.VEmaps2link);

                if (!string.IsNullOrEmpty(result.VEbtntext1))
                    sbtn1.InnerHtml = result.VEbtntext1;
                if (!string.IsNullOrEmpty(result.VEbtntext2))
                    sbtn2.InnerHtml = result.VEbtntext2;
                if (!string.IsNullOrEmpty(result.VEbtntext3))
                    sbtn3.InnerHtml = result.VEbtntext3;
                if (!string.IsNullOrEmpty(result.VEbtntext4))
                    sbtn4.InnerHtml = result.VEbtntext4;
                if (!string.IsNullOrEmpty(result.VEbtntext5))
                    sbtn5.InnerHtml = result.VEbtntext5;
                if (!string.IsNullOrEmpty(result.VEbtntext6))
                    sbtn6.InnerHtml = result.VEbtntext6;
                if (!string.IsNullOrEmpty(result.VEbtntext7))
                    sbtn7.InnerHtml = result.VEbtntext7;
                if (!string.IsNullOrEmpty(result.VEbtntext8))
                    sbtn8.InnerHtml = result.VEbtntext8;

                if (!string.IsNullOrEmpty(result.VEbtncontactcall))
                    stacall.InnerHtml = result.VEbtncontactcall;
                if (!string.IsNullOrEmpty(result.VEbtncontactbook))
                    stabook.InnerHtml = result.VEbtncontactbook;
                if (!string.IsNullOrEmpty(result.VEbtncontactlive))
                    stalive.InnerHtml = result.VEbtncontactlive;
                if (!string.IsNullOrEmpty(result.VEbtncontactemail))
                    staemail.InnerHtml = result.VEbtncontactemail;


                if (!string.IsNullOrEmpty(result.VEnewadventure) && result.VEnewadventure.Length > 500)
                    pnlnewadventure.InnerHtml = result.VEnewadventure.Replace("col-xs-6 col-sm-4 col-md-4", "col-xs-12 col-sm-4 col-md-4");

                if (!string.IsNullOrEmpty(result.VEpass))
                {
                    var currencyId = _db.tblSites.FirstOrDefault(x => x.ID == siteId).DefaultCurrencyID;
                    string[] arrListId = result.VEpass.Split(',');
                    List<Guid> passList = (from item in arrListId select Guid.Parse(item)).ToList();
                    var list = _db.GetAllProductORSpecialTrain(siteId, 0).Where(x => passList.Contains(x.ID)).Select(x => new GetAllProductORSpecialTrain
                    {
                        ID = x.ID,
                        BESTSELLER = x.BESTSELLER,
                        IsFOCAD75 = x.IsFOCAD75,
                        ISSPECIALOFFER = x.ISSPECIALOFFER,
                        NAME = x.NAME,
                        PPRICE = Math.Ceiling(FrontEndManagePass.GetPriceAfterConversion((Decimal)x.PPRICE, siteId, x.CURRENCYID, currencyId.Value)),
                        PRODUCTIMAGE = x.PRODUCTIMAGE,
                        SHORTORDER = x.SHORTORDER,
                        URL = x.URL,
                    }).ToList();
                    rptproduct.DataSource = list;
                    rptproduct.DataBind();
                }
                else
                {
                    GetNullPassList();
                    rptproduct.DataSource = listnull;
                    rptproduct.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    public void GetNullPassList()
    {
        for (int i = 1; i <= 3; i++)
            listnull.Add(new GetAllProductORSpecialTrain { BESTSELLER = 1, IsFOCAD75 = false, ID = Guid.Empty, ISSPECIALOFFER = 0, NAME = "Britrail England Flexipass", PRODUCTIMAGE = "images/tour-package-img.jpg" });

    }

    public void GetNullTrainList()
    {
        if (listnull.Count == 3)
            listnull.RemoveRange(0, 3);
        listnull.Add(new GetAllProductORSpecialTrain { NAME = "Asia", PRODUCTIMAGE = "images/adventure-toure-img-1.jpg" });
        listnull.Add(new GetAllProductORSpecialTrain { NAME = "Asia", PRODUCTIMAGE = "images/adventure-toure-img-2.jpg" });
        listnull.Add(new GetAllProductORSpecialTrain { NAME = "Asia", PRODUCTIMAGE = "images/adventure-toure-img-3.jpg" });
    }

    public void GetCurrency()
    {
        var result = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
        if (result != null)
        {
            currId = result.DefaultCurrencyID.ToString();
            if (currId != null)
                currency = oManageClass.GetCurrency(Guid.Parse(currId));
            else
                currency = "£";
        }
        else
            currency = "£";
    }
}
