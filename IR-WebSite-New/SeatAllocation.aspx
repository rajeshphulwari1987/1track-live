﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="SeatAllocation.aspx.cs" Inherits="SeatAllocation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .grddatabind
        {
            padding: 0px !important;
            width: 100%;
        }
        .grddatabind tr td
        {
            line-height: 0px !important;
        }
        .grddatabind tr
        {
            line-height: 0px !important;
            padding-left: 5px;
        }
        .additional label
        {
            line-height: 30px !important;
            padding-left: 5px;
        }
        .grddataparent
        {
            padding: 5px 5px 10px 5px;
        }
        .grddataparent .starail-Form-row
        {
            border-bottom: solid 1px #CCCCC8;
            padding-bottom: 8px;
            margin-bottom: 5px;
            margin-top: 30px;
        }
        .journey-header
        {
            background: #fff;
            padding: 17px 5px 1px 5px;
            width: 100%;
        }
        .parent-div .table > tbody > tr > td
        {
            border: none;
        }
        .spane-space
        {
            padding-left: 0px !important;
        }
        .starail-BookingDetails-form .starail-Form-label
        {
            width: 330px;
        }
        #MainContent_lblTODAvailable, #MainContent_lblBookingOffice, #MainContent_lblTVM
        {
            width: 100%;
        }
        .table
        {
            margin-bottom: 1px !important;
        }
        .table .span-last
        {
            margin-left: 14%;
        }
    </style>
    <script type="text/javascript">
        function removeMultiText() {
            $('.parent-div').each(function () {
                if ($(this).find('.child-div').size() > 1) {
                    $(this).find('tbody').each(function (key, value) {
                        if (key > 0) {
                            $(value).find('td span:first').html('');
                            $(value).find('td span:first').prop('style', 'display:inline-block;');
                        }
                    });
                }
            })
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdatePanel ID="updReservation" runat="server">
        <ContentTemplate>
            <asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server" />
            <br />
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" />
                </div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="starail-BookingDetails-form" id="div_seatAllocation" runat="server" style="display: none;">
                <h2>
                    Seat Allocation</h2>
                <p>
                    Please check your reservation allocation below.To make any changes, please use the
                    Amend button.</p>
                <div class="grddataparent" id="div_Outbound" runat="server" style="display: none;">
                    <asp:Repeater ID="rptOutBound" runat="server">
                        <HeaderTemplate>
                            <div class="journey-header">
                                <h3>
                                    Outbound</h3>
                            </div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="starail-Form-row">
                                <b>Leg
                                    <%# Eval("Index")%></b> -
                                <%#Eval("DepartureStation")%>
                                to
                                <%#Eval("ArrivalStation")%>, departing at
                                <%# DateTime.ParseExact(Eval("DepartureDateTime").ToString(), "dd-MM-yyyy HH:mm", null).ToString("HH:mm")%>
                                on
                                <%# DateTime.ParseExact(Eval("DepartureDate").ToString(), "dd-MM-yyyy", null).ToString("dd MMM yyyy")%>
                                <%# Eval("TravellBy")%></div>
                            <asp:HiddenField ID="hdnSleeper" runat="server" Value='<%#Eval("IsSleeper") %>' />
                            <div class="parent-div">
                                <asp:Repeater ID="rptSeatAllocation" runat="server" DataSource='<%#Eval("EvSeatAllocations") %>'>
                                    <ItemTemplate>
                                        <table class="table child-div">
                                            <tbody>
                                                <tr>
                                                    <td class="spane-space" scope="row">
                                                        <%#Eval("SeatDescription")%>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="grddataparent" id="div_Inbound" runat="server" style="display: none;">
                    <asp:Repeater ID="rptInBound" runat="server">
                        <HeaderTemplate>
                            <div class="journey-header">
                                <h3>
                                    Inbound</h3>
                            </div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="starail-Form-row">
                                <b>Leg
                                    <%# Eval("Index")%></b> -
                                <%#Eval("DepartureStation")%>
                                to
                                <%#Eval("ArrivalStation")%>, departing at
                                <%# DateTime.ParseExact(Eval("DepartureDateTime").ToString(), "dd-MM-yyyy HH:mm", null).ToString("HH:mm")%>
                                on
                                <%# DateTime.ParseExact(Eval("DepartureDate").ToString(), "dd-MM-yyyy", null).ToString("dd MMM yyyy")%>
                                <%# Eval("TravellBy")%></div>
                                <asp:HiddenField ID="hdnSleeper" runat="server" Value='<%#Eval("IsSleeper") %>' />
                            <div class="parent-div">
                                <asp:Repeater ID="rptSeatAllocation" runat="server" DataSource='<%#Eval("EvSeatAllocations") %>'>
                                    <ItemTemplate>
                                        <table class="table child-div">
                                            <tbody>
                                                <tr>
                                                    <td class="spane-space" scope="row">
                                                        <%#Eval("SeatDescription")%>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div class="starail-BookingDetails-form">
                <h2 style="text-transform: none;">
                    TICKET ON DEPARTURE (ToD)</h2>
                <h3 id="h3_StationName" runat="server">
                </h3>
                <div class="starail-Form-row">
                    <label class="starail-Form-label" for="starail-billing-country">
                        Please select the ToD Ticketing Location: <span class="starail-Form-required">*</span></label>
                    <div class="starail-Form-inputContainer">
                        <div class="starail-Form-inputContainer-col">
                            <asp:HiddenField ID="hdnStationCode" runat="server" />
                            <asp:DropDownList ID="ddlStation" runat="server" class="starail-Form-select" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlStation_SelectedIndexChanged">
                                <asp:ListItem Value="0">Select TOD Location</asp:ListItem>
                                <asp:ListItem Value="Abbey Wood">Abbey Wood</asp:ListItem>
                                <asp:ListItem Value="Aber">Aber</asp:ListItem>
                                <asp:ListItem Value="Abercynon">Abercynon</asp:ListItem>
                                <asp:ListItem Value="Aberdare">Aberdare</asp:ListItem>
                                <asp:ListItem Value="Aberdeen">Aberdeen</asp:ListItem>
                                <asp:ListItem Value="Aberdour">Aberdour</asp:ListItem>
                                <asp:ListItem Value="Abergavenny">Abergavenny</asp:ListItem>
                                <asp:ListItem Value="Abergele &amp; Pensarn">Abergele &amp; Pensarn</asp:ListItem>
                                <asp:ListItem Value="Aberystwyth">Aberystwyth</asp:ListItem>
                                <asp:ListItem Value="Accrington">Accrington</asp:ListItem>
                                <asp:ListItem Value="Acocks Green">Acocks Green</asp:ListItem>
                                <asp:ListItem Value="Acton Central">Acton Central</asp:ListItem>
                                <asp:ListItem Value="Acton Main Line">Acton Main Line</asp:ListItem>
                                <asp:ListItem Value="Addlestone">Addlestone</asp:ListItem>
                                <asp:ListItem Value="Adwick">Adwick</asp:ListItem>
                                <asp:ListItem Value="Airdrie">Airdrie</asp:ListItem>
                                <asp:ListItem Value="Albany Park">Albany Park</asp:ListItem>
                                <asp:ListItem Value="Aldershot">Aldershot</asp:ListItem>
                                <asp:ListItem Value="Aldrington">Aldrington</asp:ListItem>
                                <asp:ListItem Value="Alexandra Palace">Alexandra Palace</asp:ListItem>
                                <asp:ListItem Value="Alexandria">Alexandria</asp:ListItem>
                                <asp:ListItem Value="Alfreton">Alfreton</asp:ListItem>
                                <asp:ListItem Value="Alloa">Alloa</asp:ListItem>
                                <asp:ListItem Value="Alnmouth">Alnmouth</asp:ListItem>
                                <asp:ListItem Value="Alresford (Essex)">Alresford (Essex)</asp:ListItem>
                                <asp:ListItem Value="Alton">Alton</asp:ListItem>
                                <asp:ListItem Value="Alvechurch">Alvechurch</asp:ListItem>
                                <asp:ListItem Value="Ambergate">Ambergate</asp:ListItem>
                                <asp:ListItem Value="Amberley">Amberley</asp:ListItem>
                                <asp:ListItem Value="Anderston">Anderston</asp:ListItem>
                                <asp:ListItem Value="Andover">Andover</asp:ListItem>
                                <asp:ListItem Value="Anerley">Anerley</asp:ListItem>
                                <asp:ListItem Value="Angmering">Angmering</asp:ListItem>
                                <asp:ListItem Value="Anniesland">Anniesland</asp:ListItem>
                                <asp:ListItem Value="Apperley Bridge">Apperley Bridge</asp:ListItem>
                                <asp:ListItem Value="Appley Bridge">Appley Bridge</asp:ListItem>
                                <asp:ListItem Value="Apsley">Apsley</asp:ListItem>
                                <asp:ListItem Value="Arbroath">Arbroath</asp:ListItem>
                                <asp:ListItem Value="Ardrossan South Beach">Ardrossan South Beach</asp:ListItem>
                                <asp:ListItem Value="Argyle Street">Argyle Street</asp:ListItem>
                                <asp:ListItem Value="Arlesey">Arlesey</asp:ListItem>
                                <asp:ListItem Value="Armadale">Armadale</asp:ListItem>
                                <asp:ListItem Value="Arundel">Arundel</asp:ListItem>
                                <asp:ListItem Value="Ascot (Berks)">Ascot (Berks)</asp:ListItem>
                                <asp:ListItem Value="Ash">Ash</asp:ListItem>
                                <asp:ListItem Value="Ash Vale">Ash Vale</asp:ListItem>
                                <asp:ListItem Value="Ashford (Surrey)">Ashford (Surrey)</asp:ListItem>
                                <asp:ListItem Value="Ashford International">Ashford International</asp:ListItem>
                                <asp:ListItem Value="Ashtead">Ashtead</asp:ListItem>
                                <asp:ListItem Value="Ashton-under-Lyne">Ashton-under-Lyne</asp:ListItem>
                                <asp:ListItem Value="Ashurst (New Forest)">Ashurst (New Forest)</asp:ListItem>
                                <asp:ListItem Value="Ashwell &amp; Morden">Ashwell &amp; Morden</asp:ListItem>
                                <asp:ListItem Value="Aston">Aston</asp:ListItem>
                                <asp:ListItem Value="Atherstone (Warks)">Atherstone (Warks)</asp:ListItem>
                                <asp:ListItem Value="Atherton (Manchester)">Atherton (Manchester)</asp:ListItem>
                                <asp:ListItem Value="Attenborough">Attenborough</asp:ListItem>
                                <asp:ListItem Value="Audley End">Audley End</asp:ListItem>
                                <asp:ListItem Value="Aviemore">Aviemore</asp:ListItem>
                                <asp:ListItem Value="Axminster">Axminster</asp:ListItem>
                                <asp:ListItem Value="Aylesbury">Aylesbury</asp:ListItem>
                                <asp:ListItem Value="Aylesbury Vale Parkway">Aylesbury Vale Parkway</asp:ListItem>
                                <asp:ListItem Value="Aylesham">Aylesham</asp:ListItem>
                                <asp:ListItem Value="Ayr">Ayr</asp:ListItem>
                                <asp:ListItem Value="Bagshot">Bagshot</asp:ListItem>
                                <asp:ListItem Value="Balcombe">Balcombe</asp:ListItem>
                                <asp:ListItem Value="Baldock">Baldock</asp:ListItem>
                                <asp:ListItem Value="Balham">Balham</asp:ListItem>
                                <asp:ListItem Value="Balloch Central">Balloch Central</asp:ListItem>
                                <asp:ListItem Value="Banbury">Banbury</asp:ListItem>
                                <asp:ListItem Value="Bangor (Gwynedd)">Bangor (Gwynedd)</asp:ListItem>
                                <asp:ListItem Value="Banstead">Banstead</asp:ListItem>
                                <asp:ListItem Value="Barassie">Barassie</asp:ListItem>
                                <asp:ListItem Value="Bargoed">Bargoed</asp:ListItem>
                                <asp:ListItem Value="Barking">Barking</asp:ListItem>
                                <asp:ListItem Value="Barming">Barming</asp:ListItem>
                                <asp:ListItem Value="Barmouth">Barmouth</asp:ListItem>
                                <asp:ListItem Value="Barnehurst">Barnehurst</asp:ListItem>
                                <asp:ListItem Value="Barnes">Barnes</asp:ListItem>
                                <asp:ListItem Value="Barnes Bridge">Barnes Bridge</asp:ListItem>
                                <asp:ListItem Value="Barnham">Barnham</asp:ListItem>
                                <asp:ListItem Value="Barnsley">Barnsley</asp:ListItem>
                                <asp:ListItem Value="Barnstaple">Barnstaple</asp:ListItem>
                                <asp:ListItem Value="Barnt Green">Barnt Green</asp:ListItem>
                                <asp:ListItem Value="Barrhead">Barrhead</asp:ListItem>
                                <asp:ListItem Value="Barrow in Furness">Barrow in Furness</asp:ListItem>
                                <asp:ListItem Value="Barry">Barry</asp:ListItem>
                                <asp:ListItem Value="Barry Docks">Barry Docks</asp:ListItem>
                                <asp:ListItem Value="Barry Island">Barry Island</asp:ListItem>
                                <asp:ListItem Value="Basildon">Basildon</asp:ListItem>
                                <asp:ListItem Value="Basingstoke">Basingstoke</asp:ListItem>
                                <asp:ListItem Value="Bath Spa">Bath Spa</asp:ListItem>
                                <asp:ListItem Value="Bathgate">Bathgate</asp:ListItem>
                                <asp:ListItem Value="Batley">Batley</asp:ListItem>
                                <asp:ListItem Value="Battersea Park">Battersea Park</asp:ListItem>
                                <asp:ListItem Value="Battle">Battle</asp:ListItem>
                                <asp:ListItem Value="Bayford">Bayford</asp:ListItem>
                                <asp:ListItem Value="Beaconsfield">Beaconsfield</asp:ListItem>
                                <asp:ListItem Value="Bearsden">Bearsden</asp:ListItem>
                                <asp:ListItem Value="Bearsted">Bearsted</asp:ListItem>
                                <asp:ListItem Value="Beccles">Beccles</asp:ListItem>
                                <asp:ListItem Value="Beckenham Hill">Beckenham Hill</asp:ListItem>
                                <asp:ListItem Value="Beckenham Junction">Beckenham Junction</asp:ListItem>
                                <asp:ListItem Value="Bedford">Bedford</asp:ListItem>
                                <asp:ListItem Value="Bedhampton">Bedhampton</asp:ListItem>
                                <asp:ListItem Value="Bedwyn">Bedwyn</asp:ListItem>
                                <asp:ListItem Value="Beeston">Beeston</asp:ListItem>
                                <asp:ListItem Value="Bellgrove">Bellgrove</asp:ListItem>
                                <asp:ListItem Value="Bellingham (London)">Bellingham (London)</asp:ListItem>
                                <asp:ListItem Value="Bellshill">Bellshill</asp:ListItem>
                                <asp:ListItem Value="Belmont">Belmont</asp:ListItem>
                                <asp:ListItem Value="Belper">Belper</asp:ListItem>
                                <asp:ListItem Value="Belvedere">Belvedere</asp:ListItem>
                                <asp:ListItem Value="Ben Rhydding">Ben Rhydding</asp:ListItem>
                                <asp:ListItem Value="Benfleet">Benfleet</asp:ListItem>
                                <asp:ListItem Value="Bentley (Hants)">Bentley (Hants)</asp:ListItem>
                                <asp:ListItem Value="Berkhamsted">Berkhamsted</asp:ListItem>
                                <asp:ListItem Value="Berkswell">Berkswell</asp:ListItem>
                                <asp:ListItem Value="Bermuda Park">Bermuda Park</asp:ListItem>
                                <asp:ListItem Value="Berrylands">Berrylands</asp:ListItem>
                                <asp:ListItem Value="Berwick (Sussex)">Berwick (Sussex)</asp:ListItem>
                                <asp:ListItem Value="Berwick-upon-Tweed">Berwick-upon-Tweed</asp:ListItem>
                                <asp:ListItem Value="Bescot Stadium">Bescot Stadium</asp:ListItem>
                                <asp:ListItem Value="Bethnal Green">Bethnal Green</asp:ListItem>
                                <asp:ListItem Value="Beverley">Beverley</asp:ListItem>
                                <asp:ListItem Value="Bexhill">Bexhill</asp:ListItem>
                                <asp:ListItem Value="Bexley">Bexley</asp:ListItem>
                                <asp:ListItem Value="Bexleyheath">Bexleyheath</asp:ListItem>
                                <asp:ListItem Value="Bicester North">Bicester North</asp:ListItem>
                                <asp:ListItem Value="Bicester Village">Bicester Village</asp:ListItem>
                                <asp:ListItem Value="Bickley">Bickley</asp:ListItem>
                                <asp:ListItem Value="Biggleswade">Biggleswade</asp:ListItem>
                                <asp:ListItem Value="Billericay">Billericay</asp:ListItem>
                                <asp:ListItem Value="Billingshurst">Billingshurst</asp:ListItem>
                                <asp:ListItem Value="Bingley">Bingley</asp:ListItem>
                                <asp:ListItem Value="Birchgrove">Birchgrove</asp:ListItem>
                                <asp:ListItem Value="Birchington">Birchington</asp:ListItem>
                                <asp:ListItem Value="Birchwood">Birchwood</asp:ListItem>
                                <asp:ListItem Value="Birkbeck">Birkbeck</asp:ListItem>
                                <asp:ListItem Value="Birmingham International">Birmingham International</asp:ListItem>
                                <asp:ListItem Value="Birmingham Moor Street">Birmingham Moor Street</asp:ListItem>
                                <asp:ListItem Value="Birmingham New Street">Birmingham New Street</asp:ListItem>
                                <asp:ListItem Value="Birmingham Snow Hill">Birmingham Snow Hill</asp:ListItem>
                                <asp:ListItem Value="Bishopbriggs">Bishopbriggs</asp:ListItem>
                                <asp:ListItem Value="Bishops Stortford">Bishops Stortford</asp:ListItem>
                                <asp:ListItem Value="Bishopton (Strathclyde)">Bishopton (Strathclyde)</asp:ListItem>
                                <asp:ListItem Value="Bitterne">Bitterne</asp:ListItem>
                                <asp:ListItem Value="Blackburn">Blackburn</asp:ListItem>
                                <asp:ListItem Value="Blackheath">Blackheath</asp:ListItem>
                                <asp:ListItem Value="Blackpool North">Blackpool North</asp:ListItem>
                                <asp:ListItem Value="Blackridge">Blackridge</asp:ListItem>
                                <asp:ListItem Value="Blackrod">Blackrod</asp:ListItem>
                                <asp:ListItem Value="Blackwater">Blackwater</asp:ListItem>
                                <asp:ListItem Value="Blairhill">Blairhill</asp:ListItem>
                                <asp:ListItem Value="Blake Street">Blake Street</asp:ListItem>
                                <asp:ListItem Value="Blantyre">Blantyre</asp:ListItem>
                                <asp:ListItem Value="Bletchley">Bletchley</asp:ListItem>
                                <asp:ListItem Value="Bloxwich">Bloxwich</asp:ListItem>
                                <asp:ListItem Value="Bloxwich North">Bloxwich North</asp:ListItem>
                                <asp:ListItem Value="Bodmin Parkway">Bodmin Parkway</asp:ListItem>
                                <asp:ListItem Value="Bognor Regis">Bognor Regis</asp:ListItem>
                                <asp:ListItem Value="Bolton">Bolton</asp:ListItem>
                                <asp:ListItem Value="Bookham">Bookham</asp:ListItem>
                                <asp:ListItem Value="Borough Green &amp; Wrotham">Borough Green &amp; Wrotham</asp:ListItem>
                                <asp:ListItem Value="Bosham">Bosham</asp:ListItem>
                                <asp:ListItem Value="Boston">Boston</asp:ListItem>
                                <asp:ListItem Value="Botley">Botley</asp:ListItem>
                                <asp:ListItem Value="Bourne End">Bourne End</asp:ListItem>
                                <asp:ListItem Value="Bournemouth">Bournemouth</asp:ListItem>
                                <asp:ListItem Value="Bournville">Bournville</asp:ListItem>
                                <asp:ListItem Value="Bowes Park">Bowes Park</asp:ListItem>
                                <asp:ListItem Value="Boxhill &amp; Westhumble">Boxhill &amp; Westhumble</asp:ListItem>
                                <asp:ListItem Value="Bracknell">Bracknell</asp:ListItem>
                                <asp:ListItem Value="Bradford Forster Square">Bradford Forster Square</asp:ListItem>
                                <asp:ListItem Value="Bradford Interchange">Bradford Interchange</asp:ListItem>
                                <asp:ListItem Value="Bradford on Avon">Bradford on Avon</asp:ListItem>
                                <asp:ListItem Value="Braintree">Braintree</asp:ListItem>
                                <asp:ListItem Value="Braintree Freeport">Braintree Freeport</asp:ListItem>
                                <asp:ListItem Value="Bramley (Hants)">Bramley (Hants)</asp:ListItem>
                                <asp:ListItem Value="Branksome">Branksome</asp:ListItem>
                                <asp:ListItem Value="Brentford">Brentford</asp:ListItem>
                                <asp:ListItem Value="Brentwood">Brentwood</asp:ListItem>
                                <asp:ListItem Value="Bridge of Allan">Bridge of Allan</asp:ListItem>
                                <asp:ListItem Value="Bridgend">Bridgend</asp:ListItem>
                                <asp:ListItem Value="Bridgeton">Bridgeton</asp:ListItem>
                                <asp:ListItem Value="Bridgwater">Bridgwater</asp:ListItem>
                                <asp:ListItem Value="Bridlington">Bridlington</asp:ListItem>
                                <asp:ListItem Value="Brighton">Brighton</asp:ListItem>
                                <asp:ListItem Value="Brimsdown">Brimsdown</asp:ListItem>
                                <asp:ListItem Value="Bristol Parkway">Bristol Parkway</asp:ListItem>
                                <asp:ListItem Value="Bristol Temple Meads">Bristol Temple Meads</asp:ListItem>
                                <asp:ListItem Value="Brixton">Brixton</asp:ListItem>
                                <asp:ListItem Value="Broadstairs">Broadstairs</asp:ListItem>
                                <asp:ListItem Value="Brockenhurst">Brockenhurst</asp:ListItem>
                                <asp:ListItem Value="Brockley">Brockley</asp:ListItem>
                                <asp:ListItem Value="Bromley Cross">Bromley Cross</asp:ListItem>
                                <asp:ListItem Value="Bromley North">Bromley North</asp:ListItem>
                                <asp:ListItem Value="Bromley South">Bromley South</asp:ListItem>
                                <asp:ListItem Value="Bromsgrove">Bromsgrove</asp:ListItem>
                                <asp:ListItem Value="Brondesbury">Brondesbury</asp:ListItem>
                                <asp:ListItem Value="Brondesbury Park">Brondesbury Park</asp:ListItem>
                                <asp:ListItem Value="Brookmans Park">Brookmans Park</asp:ListItem>
                                <asp:ListItem Value="Brookwood">Brookwood</asp:ListItem>
                                <asp:ListItem Value="Brough">Brough</asp:ListItem>
                                <asp:ListItem Value="Broxbourne">Broxbourne</asp:ListItem>
                                <asp:ListItem Value="Bruce Grove">Bruce Grove</asp:ListItem>
                                <asp:ListItem Value="Brunstane">Brunstane</asp:ListItem>
                                <asp:ListItem Value="Buckshaw Parkway">Buckshaw Parkway</asp:ListItem>
                                <asp:ListItem Value="Burgess Hill">Burgess Hill</asp:ListItem>
                                <asp:ListItem Value="Burley Park">Burley Park</asp:ListItem>
                                <asp:ListItem Value="Burley-in-Wharfedale">Burley-in-Wharfedale</asp:ListItem>
                                <asp:ListItem Value="Burnham (Bucks)">Burnham (Bucks)</asp:ListItem>
                                <asp:ListItem Value="Burnham-on-Crouch">Burnham-on-Crouch</asp:ListItem>
                                <asp:ListItem Value="Burnley Manchester Rd">Burnley Manchester Rd</asp:ListItem>
                                <asp:ListItem Value="Burnside (Strathclyde)">Burnside (Strathclyde)</asp:ListItem>
                                <asp:ListItem Value="Burntisland">Burntisland</asp:ListItem>
                                <asp:ListItem Value="Bursledon">Bursledon</asp:ListItem>
                                <asp:ListItem Value="Burton on Trent">Burton on Trent</asp:ListItem>
                                <asp:ListItem Value="Bury St Edmunds">Bury St Edmunds</asp:ListItem>
                                <asp:ListItem Value="Busby">Busby</asp:ListItem>
                                <asp:ListItem Value="Bush Hill Park">Bush Hill Park</asp:ListItem>
                                <asp:ListItem Value="Bushey">Bushey</asp:ListItem>
                                <asp:ListItem Value="Butlers Lane">Butlers Lane</asp:ListItem>
                                <asp:ListItem Value="Buxted">Buxted</asp:ListItem>
                                <asp:ListItem Value="Buxton">Buxton</asp:ListItem>
                                <asp:ListItem Value="Byfleet &amp; New Haw">Byfleet &amp; New Haw</asp:ListItem>
                                <asp:ListItem Value="Cadoxton">Cadoxton</asp:ListItem>
                                <asp:ListItem Value="Caerphilly">Caerphilly</asp:ListItem>
                                <asp:ListItem Value="Caersws">Caersws</asp:ListItem>
                                <asp:ListItem Value="Caldercruix">Caldercruix</asp:ListItem>
                                <asp:ListItem Value="Caledonian Road &amp; Barnsbury">Caledonian Road &amp; Barnsbury</asp:ListItem>
                                <asp:ListItem Value="Camberley">Camberley</asp:ListItem>
                                <asp:ListItem Value="Cambridge">Cambridge</asp:ListItem>
                                <asp:ListItem Value="Cambridge Heath">Cambridge Heath</asp:ListItem>
                                <asp:ListItem Value="Cambuslang">Cambuslang</asp:ListItem>
                                <asp:ListItem Value="Camden Road">Camden Road</asp:ListItem>
                                <asp:ListItem Value="Camelon">Camelon</asp:ListItem>
                                <asp:ListItem Value="Canley">Canley</asp:ListItem>
                                <asp:ListItem Value="Cannock">Cannock</asp:ListItem>
                                <asp:ListItem Value="Canonbury">Canonbury</asp:ListItem>
                                <asp:ListItem Value="Canterbury East">Canterbury East</asp:ListItem>
                                <asp:ListItem Value="Canterbury West">Canterbury West</asp:ListItem>
                                <asp:ListItem Value="Cardiff Bay">Cardiff Bay</asp:ListItem>
                                <asp:ListItem Value="Cardiff Central">Cardiff Central</asp:ListItem>
                                <asp:ListItem Value="Cardiff Queen Street">Cardiff Queen Street</asp:ListItem>
                                <asp:ListItem Value="Cardonald">Cardonald</asp:ListItem>
                                <asp:ListItem Value="Cardross">Cardross</asp:ListItem>
                                <asp:ListItem Value="Carlisle">Carlisle</asp:ListItem>
                                <asp:ListItem Value="Carluke">Carluke</asp:ListItem>
                                <asp:ListItem Value="Carmarthen">Carmarthen</asp:ListItem>
                                <asp:ListItem Value="Carnforth">Carnforth</asp:ListItem>
                                <asp:ListItem Value="Carnoustie">Carnoustie</asp:ListItem>
                                <asp:ListItem Value="Carntyne">Carntyne</asp:ListItem>
                                <asp:ListItem Value="Carpenders Park">Carpenders Park</asp:ListItem>
                                <asp:ListItem Value="Carshalton">Carshalton</asp:ListItem>
                                <asp:ListItem Value="Carshalton Beech">Carshalton Beech</asp:ListItem>
                                <asp:ListItem Value="Carstairs">Carstairs</asp:ListItem>
                                <asp:ListItem Value="Cartsdyke">Cartsdyke</asp:ListItem>
                                <asp:ListItem Value="Castle Cary">Castle Cary</asp:ListItem>
                                <asp:ListItem Value="Castleford">Castleford</asp:ListItem>
                                <asp:ListItem Value="Castleton (Manchester)">Castleton (Manchester)</asp:ListItem>
                                <asp:ListItem Value="Caterham">Caterham</asp:ListItem>
                                <asp:ListItem Value="Catford">Catford</asp:ListItem>
                                <asp:ListItem Value="Catford Bridge">Catford Bridge</asp:ListItem>
                                <asp:ListItem Value="Cathays">Cathays</asp:ListItem>
                                <asp:ListItem Value="Cathcart">Cathcart</asp:ListItem>
                                <asp:ListItem Value="Chadwell Heath">Chadwell Heath</asp:ListItem>
                                <asp:ListItem Value="Chafford Hundred">Chafford Hundred</asp:ListItem>
                                <asp:ListItem Value="Chalkwell">Chalkwell</asp:ListItem>
                                <asp:ListItem Value="Chandlers Ford">Chandlers Ford</asp:ListItem>
                                <asp:ListItem Value="Chapeltown (Yorks)">Chapeltown (Yorks)</asp:ListItem>
                                <asp:ListItem Value="Charing (Kent)">Charing (Kent)</asp:ListItem>
                                <asp:ListItem Value="Charing Cross (Glasgow)">Charing Cross (Glasgow)</asp:ListItem>
                                <asp:ListItem Value="Charlbury">Charlbury</asp:ListItem>
                                <asp:ListItem Value="Charlton">Charlton</asp:ListItem>
                                <asp:ListItem Value="Chatham">Chatham</asp:ListItem>
                                <asp:ListItem Value="Cheadle Hulme">Cheadle Hulme</asp:ListItem>
                                <asp:ListItem Value="Cheam">Cheam</asp:ListItem>
                                <asp:ListItem Value="Cheddington">Cheddington</asp:ListItem>
                                <asp:ListItem Value="Chelmsford (Essex)">Chelmsford (Essex)</asp:ListItem>
                                <asp:ListItem Value="Chelsfield">Chelsfield</asp:ListItem>
                                <asp:ListItem Value="Cheltenham Spa">Cheltenham Spa</asp:ListItem>
                                <asp:ListItem Value="Chepstow">Chepstow</asp:ListItem>
                                <asp:ListItem Value="Chertsey">Chertsey</asp:ListItem>
                                <asp:ListItem Value="Cheshunt">Cheshunt</asp:ListItem>
                                <asp:ListItem Value="Chessington North">Chessington North</asp:ListItem>
                                <asp:ListItem Value="Chessington South">Chessington South</asp:ListItem>
                                <asp:ListItem Value="Chester">Chester</asp:ListItem>
                                <asp:ListItem Value="Chester Road">Chester Road</asp:ListItem>
                                <asp:ListItem Value="Chesterfield">Chesterfield</asp:ListItem>
                                <asp:ListItem Value="Chestfield &amp; Swalecliffe">Chestfield &amp; Swalecliffe</asp:ListItem>
                                <asp:ListItem Value="Chichester">Chichester</asp:ListItem>
                                <asp:ListItem Value="Chingford">Chingford</asp:ListItem>
                                <asp:ListItem Value="Chippenham">Chippenham</asp:ListItem>
                                <asp:ListItem Value="Chipstead">Chipstead</asp:ListItem>
                                <asp:ListItem Value="Chislehurst">Chislehurst</asp:ListItem>
                                <asp:ListItem Value="Chiswick">Chiswick</asp:ListItem>
                                <asp:ListItem Value="Cholsey">Cholsey</asp:ListItem>
                                <asp:ListItem Value="Chorley">Chorley</asp:ListItem>
                                <asp:ListItem Value="Christchurch">Christchurch</asp:ListItem>
                                <asp:ListItem Value="Christs Hospital">Christs Hospital</asp:ListItem>
                                <asp:ListItem Value="Church Stretton">Church Stretton</asp:ListItem>
                                <asp:ListItem Value="City Thameslink">City Thameslink</asp:ListItem>
                                <asp:ListItem Value="Clacton-on-Sea">Clacton-on-Sea</asp:ListItem>
                                <asp:ListItem Value="Clandon">Clandon</asp:ListItem>
                                <asp:ListItem Value="Clapham High Street">Clapham High Street</asp:ListItem>
                                <asp:ListItem Value="Clapham Junction">Clapham Junction</asp:ListItem>
                                <asp:ListItem Value="Clapton">Clapton</asp:ListItem>
                                <asp:ListItem Value="Clarkston">Clarkston</asp:ListItem>
                                <asp:ListItem Value="Claygate">Claygate</asp:ListItem>
                                <asp:ListItem Value="Cleethorpes">Cleethorpes</asp:ListItem>
                                <asp:ListItem Value="Clock House">Clock House</asp:ListItem>
                                <asp:ListItem Value="Clydebank">Clydebank</asp:ListItem>
                                <asp:ListItem Value="Coatbridge Sunnyside">Coatbridge Sunnyside</asp:ListItem>
                                <asp:ListItem Value="Coatdyke">Coatdyke</asp:ListItem>
                                <asp:ListItem Value="Cobham &amp; Stoke D'Abernon">Cobham &amp; Stoke D'Abernon</asp:ListItem>
                                <asp:ListItem Value="Cogan">Cogan</asp:ListItem>
                                <asp:ListItem Value="Colchester">Colchester</asp:ListItem>
                                <asp:ListItem Value="Colchester Town">Colchester Town</asp:ListItem>
                                <asp:ListItem Value="Coleshill Parkway">Coleshill Parkway</asp:ListItem>
                                <asp:ListItem Value="Collington">Collington</asp:ListItem>
                                <asp:ListItem Value="Colne">Colne</asp:ListItem>
                                <asp:ListItem Value="Colwyn Bay">Colwyn Bay</asp:ListItem>
                                <asp:ListItem Value="Congleton">Congleton</asp:ListItem>
                                <asp:ListItem Value="Cononley">Cononley</asp:ListItem>
                                <asp:ListItem Value="Cooden Beach">Cooden Beach</asp:ListItem>
                                <asp:ListItem Value="Cooksbridge">Cooksbridge</asp:ListItem>
                                <asp:ListItem Value="Corby">Corby</asp:ListItem>
                                <asp:ListItem Value="Coryton">Coryton</asp:ListItem>
                                <asp:ListItem Value="Coseley">Coseley</asp:ListItem>
                                <asp:ListItem Value="Cosham">Cosham</asp:ListItem>
                                <asp:ListItem Value="Cottingham">Cottingham</asp:ListItem>
                                <asp:ListItem Value="Coulsdon South">Coulsdon South</asp:ListItem>
                                <asp:ListItem Value="Coventry">Coventry</asp:ListItem>
                                <asp:ListItem Value="Coventry Arena">Coventry Arena</asp:ListItem>
                                <asp:ListItem Value="Cowden (Kent)">Cowden (Kent)</asp:ListItem>
                                <asp:ListItem Value="Cowdenbeath">Cowdenbeath</asp:ListItem>
                                <asp:ListItem Value="Cowes East (Red Funnel Ship)">Cowes East (Red Funnel Ship)</asp:ListItem>
                                <asp:ListItem Value="Cowes West (Redjet)">Cowes West (Redjet)</asp:ListItem>
                                <asp:ListItem Value="Cradley Heath">Cradley Heath</asp:ListItem>
                                <asp:ListItem Value="Cranbrook (Devon)">Cranbrook (Devon)</asp:ListItem>
                                <asp:ListItem Value="Craven Arms">Craven Arms</asp:ListItem>
                                <asp:ListItem Value="Crawley">Crawley</asp:ListItem>
                                <asp:ListItem Value="Crayford">Crayford</asp:ListItem>
                                <asp:ListItem Value="Crewe">Crewe</asp:ListItem>
                                <asp:ListItem Value="Crewkerne">Crewkerne</asp:ListItem>
                                <asp:ListItem Value="Crews Hill">Crews Hill</asp:ListItem>
                                <asp:ListItem Value="Cricklewood">Cricklewood</asp:ListItem>
                                <asp:ListItem Value="Croftfoot">Croftfoot</asp:ListItem>
                                <asp:ListItem Value="Crofton Park">Crofton Park</asp:ListItem>
                                <asp:ListItem Value="Cromer">Cromer</asp:ListItem>
                                <asp:ListItem Value="Cromford">Cromford</asp:ListItem>
                                <asp:ListItem Value="Crookston">Crookston</asp:ListItem>
                                <asp:ListItem Value="Cross Gates">Cross Gates</asp:ListItem>
                                <asp:ListItem Value="Crossflatts">Crossflatts</asp:ListItem>
                                <asp:ListItem Value="Crosshill">Crosshill</asp:ListItem>
                                <asp:ListItem Value="Crosskeys">Crosskeys</asp:ListItem>
                                <asp:ListItem Value="Crossmyloof">Crossmyloof</asp:ListItem>
                                <asp:ListItem Value="Crouch Hill">Crouch Hill</asp:ListItem>
                                <asp:ListItem Value="Crowborough">Crowborough</asp:ListItem>
                                <asp:ListItem Value="Crowhurst">Crowhurst</asp:ListItem>
                                <asp:ListItem Value="Croy">Croy</asp:ListItem>
                                <asp:ListItem Value="Crystal Palace">Crystal Palace</asp:ListItem>
                                <asp:ListItem Value="Cuffley">Cuffley</asp:ListItem>
                                <asp:ListItem Value="Cumbernauld">Cumbernauld</asp:ListItem>
                                <asp:ListItem Value="Cupar">Cupar</asp:ListItem>
                                <asp:ListItem Value="Cwmbran">Cwmbran</asp:ListItem>
                                <asp:ListItem Value="Dagenham Dock">Dagenham Dock</asp:ListItem>
                                <asp:ListItem Value="Dalgety Bay">Dalgety Bay</asp:ListItem>
                                <asp:ListItem Value="Dalmarnock">Dalmarnock</asp:ListItem>
                                <asp:ListItem Value="Dalmeny">Dalmeny</asp:ListItem>
                                <asp:ListItem Value="Dalmuir">Dalmuir</asp:ListItem>
                                <asp:ListItem Value="Dalreoch">Dalreoch</asp:ListItem>
                                <asp:ListItem Value="Dalry">Dalry</asp:ListItem>
                                <asp:ListItem Value="Dalston Junction">Dalston Junction</asp:ListItem>
                                <asp:ListItem Value="Dalston Kingsland">Dalston Kingsland</asp:ListItem>
                                <asp:ListItem Value="Danes Court">Danes Court</asp:ListItem>
                                <asp:ListItem Value="Darlington">Darlington</asp:ListItem>
                                <asp:ListItem Value="Dartford">Dartford</asp:ListItem>
                                <asp:ListItem Value="Darwen">Darwen</asp:ListItem>
                                <asp:ListItem Value="Datchet">Datchet</asp:ListItem>
                                <asp:ListItem Value="Dawlish">Dawlish</asp:ListItem>
                                <asp:ListItem Value="Deal">Deal</asp:ListItem>
                                <asp:ListItem Value="Deansgate G-Mex">Deansgate G-Mex</asp:ListItem>
                                <asp:ListItem Value="Denham">Denham</asp:ListItem>
                                <asp:ListItem Value="Denmark Hill">Denmark Hill</asp:ListItem>
                                <asp:ListItem Value="Deptford">Deptford</asp:ListItem>
                                <asp:ListItem Value="Derby">Derby</asp:ListItem>
                                <asp:ListItem Value="Dewsbury">Dewsbury</asp:ListItem>
                                <asp:ListItem Value="Didcot Parkway">Didcot Parkway</asp:ListItem>
                                <asp:ListItem Value="Dinas Powys">Dinas Powys</asp:ListItem>
                                <asp:ListItem Value="Dingle Road">Dingle Road</asp:ListItem>
                                <asp:ListItem Value="Dingwall">Dingwall</asp:ListItem>
                                <asp:ListItem Value="Diss">Diss</asp:ListItem>
                                <asp:ListItem Value="Doncaster">Doncaster</asp:ListItem>
                                <asp:ListItem Value="Dorchester South">Dorchester South</asp:ListItem>
                                <asp:ListItem Value="Dore">Dore</asp:ListItem>
                                <asp:ListItem Value="Dorking">Dorking</asp:ListItem>
                                <asp:ListItem Value="Dorking Deepdene">Dorking Deepdene</asp:ListItem>
                                <asp:ListItem Value="Dormans">Dormans</asp:ListItem>
                                <asp:ListItem Value="Dorridge">Dorridge</asp:ListItem>
                                <asp:ListItem Value="Dover Priory">Dover Priory</asp:ListItem>
                                <asp:ListItem Value="Dovercourt">Dovercourt</asp:ListItem>
                                <asp:ListItem Value="Downham Market">Downham Market</asp:ListItem>
                                <asp:ListItem Value="Drayton Park">Drayton Park</asp:ListItem>
                                <asp:ListItem Value="Drem">Drem</asp:ListItem>
                                <asp:ListItem Value="Driffield">Driffield</asp:ListItem>
                                <asp:ListItem Value="Droitwich Spa">Droitwich Spa</asp:ListItem>
                                <asp:ListItem Value="Drumchapel">Drumchapel</asp:ListItem>
                                <asp:ListItem Value="Drumgelloch">Drumgelloch</asp:ListItem>
                                <asp:ListItem Value="Drumry">Drumry</asp:ListItem>
                                <asp:ListItem Value="Duffield">Duffield</asp:ListItem>
                                <asp:ListItem Value="Dumbarton Central">Dumbarton Central</asp:ListItem>
                                <asp:ListItem Value="Dumbarton East">Dumbarton East</asp:ListItem>
                                <asp:ListItem Value="Dumbreck">Dumbreck</asp:ListItem>
                                <asp:ListItem Value="Dumfries">Dumfries</asp:ListItem>
                                <asp:ListItem Value="Dunbar">Dunbar</asp:ListItem>
                                <asp:ListItem Value="Dunblane">Dunblane</asp:ListItem>
                                <asp:ListItem Value="Dundee">Dundee</asp:ListItem>
                                <asp:ListItem Value="Dunfermline Queen Margaret">Dunfermline Queen Margaret</asp:ListItem>
                                <asp:ListItem Value="Dunfermline Town">Dunfermline Town</asp:ListItem>
                                <asp:ListItem Value="Dunlop">Dunlop</asp:ListItem>
                                <asp:ListItem Value="Durham">Durham</asp:ListItem>
                                <asp:ListItem Value="Durrington">Durrington</asp:ListItem>
                                <asp:ListItem Value="Dyce">Dyce</asp:ListItem>
                                <asp:ListItem Value="Ealing Broadway">Ealing Broadway</asp:ListItem>
                                <asp:ListItem Value="Earley">Earley</asp:ListItem>
                                <asp:ListItem Value="Earlsfield">Earlsfield</asp:ListItem>
                                <asp:ListItem Value="Earlswood (Surrey)">Earlswood (Surrey)</asp:ListItem>
                                <asp:ListItem Value="East Croydon">East Croydon</asp:ListItem>
                                <asp:ListItem Value="East Dulwich">East Dulwich</asp:ListItem>
                                <asp:ListItem Value="East Garforth">East Garforth</asp:ListItem>
                                <asp:ListItem Value="East Grinstead">East Grinstead</asp:ListItem>
                                <asp:ListItem Value="East Kilbride">East Kilbride</asp:ListItem>
                                <asp:ListItem Value="East Midlands Parkway">East Midlands Parkway</asp:ListItem>
                                <asp:ListItem Value="East Tilbury">East Tilbury</asp:ListItem>
                                <asp:ListItem Value="East Worthing">East Worthing</asp:ListItem>
                                <asp:ListItem Value="Eastbourne">Eastbourne</asp:ListItem>
                                <asp:ListItem Value="Eastbrook">Eastbrook</asp:ListItem>
                                <asp:ListItem Value="Easterhouse">Easterhouse</asp:ListItem>
                                <asp:ListItem Value="Eastleigh">Eastleigh</asp:ListItem>
                                <asp:ListItem Value="Ebbsfleet International">Ebbsfleet International</asp:ListItem>
                                <asp:ListItem Value="Ebbw Vale Parkway">Ebbw Vale Parkway</asp:ListItem>
                                <asp:ListItem Value="Eden Park">Eden Park</asp:ListItem>
                                <asp:ListItem Value="Edenbridge">Edenbridge</asp:ListItem>
                                <asp:ListItem Value="Edenbridge Town">Edenbridge Town</asp:ListItem>
                                <asp:ListItem Value="Edinburgh">Edinburgh</asp:ListItem>
                                <asp:ListItem Value="Edinburgh Park">Edinburgh Park</asp:ListItem>
                                <asp:ListItem Value="Edmonton Green">Edmonton Green</asp:ListItem>
                                <asp:ListItem Value="Effingham Junction">Effingham Junction</asp:ListItem>
                                <asp:ListItem Value="Egham">Egham</asp:ListItem>
                                <asp:ListItem Value="Elephant &amp; Castle">Elephant &amp; Castle</asp:ListItem>
                                <asp:ListItem Value="Elgin">Elgin</asp:ListItem>
                                <asp:ListItem Value="Elmers End">Elmers End</asp:ListItem>
                                <asp:ListItem Value="Elmstead Woods">Elmstead Woods</asp:ListItem>
                                <asp:ListItem Value="Elsenham Essex">Elsenham Essex</asp:ListItem>
                                <asp:ListItem Value="Elstree &amp; Borehamwood">Elstree &amp; Borehamwood</asp:ListItem>
                                <asp:ListItem Value="Eltham">Eltham</asp:ListItem>
                                <asp:ListItem Value="Ely">Ely</asp:ListItem>
                                <asp:ListItem Value="Emerson Park">Emerson Park</asp:ListItem>
                                <asp:ListItem Value="Emsworth">Emsworth</asp:ListItem>
                                <asp:ListItem Value="Enfield Chase">Enfield Chase</asp:ListItem>
                                <asp:ListItem Value="Enfield Lock">Enfield Lock</asp:ListItem>
                                <asp:ListItem Value="Enfield Town">Enfield Town</asp:ListItem>
                                <asp:ListItem Value="Epsom">Epsom</asp:ListItem>
                                <asp:ListItem Value="Epsom Downs">Epsom Downs</asp:ListItem>
                                <asp:ListItem Value="Erdington">Erdington</asp:ListItem>
                                <asp:ListItem Value="Eridge">Eridge</asp:ListItem>
                                <asp:ListItem Value="Erith">Erith</asp:ListItem>
                                <asp:ListItem Value="Esher">Esher</asp:ListItem>
                                <asp:ListItem Value="Eskbank">Eskbank</asp:ListItem>
                                <asp:ListItem Value="Etchingham">Etchingham</asp:ListItem>
                                <asp:ListItem Value="Evesham">Evesham</asp:ListItem>
                                <asp:ListItem Value="Ewell East">Ewell East</asp:ListItem>
                                <asp:ListItem Value="Ewell West">Ewell West</asp:ListItem>
                                <asp:ListItem Value="Exeter Central">Exeter Central</asp:ListItem>
                                <asp:ListItem Value="Exeter St Davids">Exeter St Davids</asp:ListItem>
                                <asp:ListItem Value="Exhibition Centre (Glasgow)">Exhibition Centre (Glasgow)</asp:ListItem>
                                <asp:ListItem Value="Exmouth">Exmouth</asp:ListItem>
                                <asp:ListItem Value="Eynsford">Eynsford</asp:ListItem>
                                <asp:ListItem Value="Fairwater">Fairwater</asp:ListItem>
                                <asp:ListItem Value="Falconwood">Falconwood</asp:ListItem>
                                <asp:ListItem Value="Falkirk Grahamston">Falkirk Grahamston</asp:ListItem>
                                <asp:ListItem Value="Falkirk High">Falkirk High</asp:ListItem>
                                <asp:ListItem Value="Falmer">Falmer</asp:ListItem>
                                <asp:ListItem Value="Fareham">Fareham</asp:ListItem>
                                <asp:ListItem Value="Farnborough Main">Farnborough Main</asp:ListItem>
                                <asp:ListItem Value="Farncombe">Farncombe</asp:ListItem>
                                <asp:ListItem Value="Farnham">Farnham</asp:ListItem>
                                <asp:ListItem Value="Farningham Road">Farningham Road</asp:ListItem>
                                <asp:ListItem Value="Farringdon">Farringdon</asp:ListItem>
                                <asp:ListItem Value="Faversham">Faversham</asp:ListItem>
                                <asp:ListItem Value="Faygate">Faygate</asp:ListItem>
                                <asp:ListItem Value="Felixstowe">Felixstowe</asp:ListItem>
                                <asp:ListItem Value="Feltham">Feltham</asp:ListItem>
                                <asp:ListItem Value="Feniton">Feniton</asp:ListItem>
                                <asp:ListItem Value="Finchley Road &amp; Frognal">Finchley Road &amp; Frognal</asp:ListItem>
                                <asp:ListItem Value="Finsbury Park">Finsbury Park</asp:ListItem>
                                <asp:ListItem Value="Fishbourne (Sussex)">Fishbourne (Sussex)</asp:ListItem>
                                <asp:ListItem Value="Fishersgate">Fishersgate</asp:ListItem>
                                <asp:ListItem Value="Fitzwilliam">Fitzwilliam</asp:ListItem>
                                <asp:ListItem Value="Five Ways">Five Ways</asp:ListItem>
                                <asp:ListItem Value="Fleet">Fleet</asp:ListItem>
                                <asp:ListItem Value="Flint">Flint</asp:ListItem>
                                <asp:ListItem Value="Flitwick">Flitwick</asp:ListItem>
                                <asp:ListItem Value="Folkestone Central">Folkestone Central</asp:ListItem>
                                <asp:ListItem Value="Folkestone West">Folkestone West</asp:ListItem>
                                <asp:ListItem Value="Ford">Ford</asp:ListItem>
                                <asp:ListItem Value="Forest Gate">Forest Gate</asp:ListItem>
                                <asp:ListItem Value="Forest Hill">Forest Hill</asp:ListItem>
                                <asp:ListItem Value="Forres">Forres</asp:ListItem>
                                <asp:ListItem Value="Fort William">Fort William</asp:ListItem>
                                <asp:ListItem Value="Four Oaks">Four Oaks</asp:ListItem>
                                <asp:ListItem Value="Foxton">Foxton</asp:ListItem>
                                <asp:ListItem Value="Frant">Frant</asp:ListItem>
                                <asp:ListItem Value="Fratton">Fratton</asp:ListItem>
                                <asp:ListItem Value="Frimley">Frimley</asp:ListItem>
                                <asp:ListItem Value="Frinton-on-Sea">Frinton-on-Sea</asp:ListItem>
                                <asp:ListItem Value="Frizinghall">Frizinghall</asp:ListItem>
                                <asp:ListItem Value="Frodsham">Frodsham</asp:ListItem>
                                <asp:ListItem Value="Frome">Frome</asp:ListItem>
                                <asp:ListItem Value="Fulwell">Fulwell</asp:ListItem>
                                <asp:ListItem Value="Galashiels">Galashiels</asp:ListItem>
                                <asp:ListItem Value="Garforth">Garforth</asp:ListItem>
                                <asp:ListItem Value="Garrowhill">Garrowhill</asp:ListItem>
                                <asp:ListItem Value="Garscadden">Garscadden</asp:ListItem>
                                <asp:ListItem Value="Gatwick Airport">Gatwick Airport</asp:ListItem>
                                <asp:ListItem Value="Gerrards Cross">Gerrards Cross</asp:ListItem>
                                <asp:ListItem Value="Gidea Park">Gidea Park</asp:ListItem>
                                <asp:ListItem Value="Giffnock">Giffnock</asp:ListItem>
                                <asp:ListItem Value="Gillingham (Dorset)">Gillingham (Dorset)</asp:ListItem>
                                <asp:ListItem Value="Gillingham (Kent)">Gillingham (Kent)</asp:ListItem>
                                <asp:ListItem Value="Gipsy Hill">Gipsy Hill</asp:ListItem>
                                <asp:ListItem Value="Girvan">Girvan</asp:ListItem>
                                <asp:ListItem Value="Glasgow Airport">Glasgow Airport</asp:ListItem>
                                <asp:ListItem Value="Glasgow Central">Glasgow Central</asp:ListItem>
                                <asp:ListItem Value="Glasgow Queen Street">Glasgow Queen Street</asp:ListItem>
                                <asp:ListItem Value="Glengarnock">Glengarnock</asp:ListItem>
                                <asp:ListItem Value="Glossop">Glossop</asp:ListItem>
                                <asp:ListItem Value="Gloucester">Gloucester</asp:ListItem>
                                <asp:ListItem Value="Glynde">Glynde</asp:ListItem>
                                <asp:ListItem Value="Gobowen">Gobowen</asp:ListItem>
                                <asp:ListItem Value="Godalming">Godalming</asp:ListItem>
                                <asp:ListItem Value="Godstone">Godstone</asp:ListItem>
                                <asp:ListItem Value="Goodmayes">Goodmayes</asp:ListItem>
                                <asp:ListItem Value="Goole">Goole</asp:ListItem>
                                <asp:ListItem Value="Gordon Hill">Gordon Hill</asp:ListItem>
                                <asp:ListItem Value="Gorebridge">Gorebridge</asp:ListItem>
                                <asp:ListItem Value="Goring &amp; Streatley">Goring &amp; Streatley</asp:ListItem>
                                <asp:ListItem Value="Goring by Sea">Goring by Sea</asp:ListItem>
                                <asp:ListItem Value="Gospel Oak">Gospel Oak</asp:ListItem>
                                <asp:ListItem Value="Gourock">Gourock</asp:ListItem>
                                <asp:ListItem Value="Gowerton">Gowerton</asp:ListItem>
                                <asp:ListItem Value="Grange Over Sands">Grange Over Sands</asp:ListItem>
                                <asp:ListItem Value="Grange Park">Grange Park</asp:ListItem>
                                <asp:ListItem Value="Grangetown (Cardiff)">Grangetown (Cardiff)</asp:ListItem>
                                <asp:ListItem Value="Grantham">Grantham</asp:ListItem>
                                <asp:ListItem Value="Grateley">Grateley</asp:ListItem>
                                <asp:ListItem Value="Gravesend">Gravesend</asp:ListItem>
                                <asp:ListItem Value="Grays">Grays</asp:ListItem>
                                <asp:ListItem Value="Great Bentley">Great Bentley</asp:ListItem>
                                <asp:ListItem Value="Great Chesterford">Great Chesterford</asp:ListItem>
                                <asp:ListItem Value="Great Malvern">Great Malvern</asp:ListItem>
                                <asp:ListItem Value="Great Missenden">Great Missenden</asp:ListItem>
                                <asp:ListItem Value="Great Yarmouth">Great Yarmouth</asp:ListItem>
                                <asp:ListItem Value="Greenbank">Greenbank</asp:ListItem>
                                <asp:ListItem Value="Greenfield">Greenfield</asp:ListItem>
                                <asp:ListItem Value="Greenhithe">Greenhithe</asp:ListItem>
                                <asp:ListItem Value="Greenock Central">Greenock Central</asp:ListItem>
                                <asp:ListItem Value="Greenock West">Greenock West</asp:ListItem>
                                <asp:ListItem Value="Greenwich">Greenwich</asp:ListItem>
                                <asp:ListItem Value="Grimsby Town">Grimsby Town</asp:ListItem>
                                <asp:ListItem Value="Grove Park">Grove Park</asp:ListItem>
                                <asp:ListItem Value="Guildford">Guildford</asp:ListItem>
                                <asp:ListItem Value="Guiseley">Guiseley</asp:ListItem>
                                <asp:ListItem Value="Gunnersbury">Gunnersbury</asp:ListItem>
                                <asp:ListItem Value="Hackbridge">Hackbridge</asp:ListItem>
                                <asp:ListItem Value="Hackney Central">Hackney Central</asp:ListItem>
                                <asp:ListItem Value="Hackney Downs">Hackney Downs</asp:ListItem>
                                <asp:ListItem Value="Hackney Wick">Hackney Wick</asp:ListItem>
                                <asp:ListItem Value="Haddenham &amp; Thame Parkway">Haddenham &amp; Thame Parkway</asp:ListItem>
                                <asp:ListItem Value="Hadfield">Hadfield</asp:ListItem>
                                <asp:ListItem Value="Hadley Wood">Hadley Wood</asp:ListItem>
                                <asp:ListItem Value="Haggerston">Haggerston</asp:ListItem>
                                <asp:ListItem Value="Hagley">Hagley</asp:ListItem>
                                <asp:ListItem Value="Hairmyres">Hairmyres</asp:ListItem>
                                <asp:ListItem Value="Halesworth">Halesworth</asp:ListItem>
                                <asp:ListItem Value="Halifax">Halifax</asp:ListItem>
                                <asp:ListItem Value="Hall Green">Hall Green</asp:ListItem>
                                <asp:ListItem Value="Haltwhistle">Haltwhistle</asp:ListItem>
                                <asp:ListItem Value="Ham Street">Ham Street</asp:ListItem>
                                <asp:ListItem Value="Hamble">Hamble</asp:ListItem>
                                <asp:ListItem Value="Hamilton Central">Hamilton Central</asp:ListItem>
                                <asp:ListItem Value="Hamilton West">Hamilton West</asp:ListItem>
                                <asp:ListItem Value="Hampden Park (Sussex)">Hampden Park (Sussex)</asp:ListItem>
                                <asp:ListItem Value="Hampstead Heath">Hampstead Heath</asp:ListItem>
                                <asp:ListItem Value="Hampton (London)">Hampton (London)</asp:ListItem>
                                <asp:ListItem Value="Hampton Court">Hampton Court</asp:ListItem>
                                <asp:ListItem Value="Hampton Wick">Hampton Wick</asp:ListItem>
                                <asp:ListItem Value="Hamstead (Birmingham)">Hamstead (Birmingham)</asp:ListItem>
                                <asp:ListItem Value="Hamworthy">Hamworthy</asp:ListItem>
                                <asp:ListItem Value="Hanborough">Hanborough</asp:ListItem>
                                <asp:ListItem Value="Hanwell">Hanwell</asp:ListItem>
                                <asp:ListItem Value="Harlesden">Harlesden</asp:ListItem>
                                <asp:ListItem Value="Harlington (Beds)">Harlington (Beds)</asp:ListItem>
                                <asp:ListItem Value="Harlow Mill">Harlow Mill</asp:ListItem>
                                <asp:ListItem Value="Harlow Town">Harlow Town</asp:ListItem>
                                <asp:ListItem Value="Harold Wood">Harold Wood</asp:ListItem>
                                <asp:ListItem Value="Harpenden">Harpenden</asp:ListItem>
                                <asp:ListItem Value="Harrietsham">Harrietsham</asp:ListItem>
                                <asp:ListItem Value="Harringay">Harringay</asp:ListItem>
                                <asp:ListItem Value="Harringay Green Lanes">Harringay Green Lanes</asp:ListItem>
                                <asp:ListItem Value="Harrogate">Harrogate</asp:ListItem>
                                <asp:ListItem Value="Harrow &amp; Wealdstone">Harrow &amp; Wealdstone</asp:ListItem>
                                <asp:ListItem Value="Hartford">Hartford</asp:ListItem>
                                <asp:ListItem Value="Hartlepool">Hartlepool</asp:ListItem>
                                <asp:ListItem Value="Harwich International">Harwich International</asp:ListItem>
                                <asp:ListItem Value="Haslemere">Haslemere</asp:ListItem>
                                <asp:ListItem Value="Hassocks">Hassocks</asp:ListItem>
                                <asp:ListItem Value="Hastings">Hastings</asp:ListItem>
                                <asp:ListItem Value="Hatch End">Hatch End</asp:ListItem>
                                <asp:ListItem Value="Hatfield (Herts)">Hatfield (Herts)</asp:ListItem>
                                <asp:ListItem Value="Hatfield Peverel">Hatfield Peverel</asp:ListItem>
                                <asp:ListItem Value="Havant">Havant</asp:ListItem>
                                <asp:ListItem Value="Haverfordwest">Haverfordwest</asp:ListItem>
                                <asp:ListItem Value="Hawkhead">Hawkhead</asp:ListItem>
                                <asp:ListItem Value="Haydons Road">Haydons Road</asp:ListItem>
                                <asp:ListItem Value="Hayes &amp; Harlington">Hayes &amp; Harlington</asp:ListItem>
                                <asp:ListItem Value="Hayes (Kent)">Hayes (Kent)</asp:ListItem>
                                <asp:ListItem Value="Haymarket">Haymarket</asp:ListItem>
                                <asp:ListItem Value="Haywards Heath">Haywards Heath</asp:ListItem>
                                <asp:ListItem Value="Hazel Grove">Hazel Grove</asp:ListItem>
                                <asp:ListItem Value="Headcorn">Headcorn</asp:ListItem>
                                <asp:ListItem Value="Headingley">Headingley</asp:ListItem>
                                <asp:ListItem Value="Headstone Lane">Headstone Lane</asp:ListItem>
                                <asp:ListItem Value="Heath High Level">Heath High Level</asp:ListItem>
                                <asp:ListItem Value="Heath Low Level">Heath Low Level</asp:ListItem>
                                <asp:ListItem Value="Heathrow Express/Connect (Terminal 4)">Heathrow Express/Connect (Terminal 4)</asp:ListItem>
                                <asp:ListItem Value="Heathrow Express/Connect (Terminal 5)">Heathrow Express/Connect (Terminal 5)</asp:ListItem>
                                <asp:ListItem Value="Heathrow Express/Connect (Terminals 1-3)">Heathrow Express/Connect (Terminals 1-3)</asp:ListItem>
                                <asp:ListItem Value="Heaton Chapel">Heaton Chapel</asp:ListItem>
                                <asp:ListItem Value="Hebden Bridge">Hebden Bridge</asp:ListItem>
                                <asp:ListItem Value="Hedge End">Hedge End</asp:ListItem>
                                <asp:ListItem Value="Hednesford">Hednesford</asp:ListItem>
                                <asp:ListItem Value="Helensburgh Central">Helensburgh Central</asp:ListItem>
                                <asp:ListItem Value="Helsby">Helsby</asp:ListItem>
                                <asp:ListItem Value="Hemel Hempstead">Hemel Hempstead</asp:ListItem>
                                <asp:ListItem Value="Hendon">Hendon</asp:ListItem>
                                <asp:ListItem Value="Henley in Arden">Henley in Arden</asp:ListItem>
                                <asp:ListItem Value="Henley on Thames">Henley on Thames</asp:ListItem>
                                <asp:ListItem Value="Hereford">Hereford</asp:ListItem>
                                <asp:ListItem Value="Herne Bay">Herne Bay</asp:ListItem>
                                <asp:ListItem Value="Herne Hill">Herne Hill</asp:ListItem>
                                <asp:ListItem Value="Hersham">Hersham</asp:ListItem>
                                <asp:ListItem Value="Hertford East">Hertford East</asp:ListItem>
                                <asp:ListItem Value="Hertford North">Hertford North</asp:ListItem>
                                <asp:ListItem Value="Hever">Hever</asp:ListItem>
                                <asp:ListItem Value="Hexham">Hexham</asp:ListItem>
                                <asp:ListItem Value="High Brooms">High Brooms</asp:ListItem>
                                <asp:ListItem Value="High Street (Glasgow)">High Street (Glasgow)</asp:ListItem>
                                <asp:ListItem Value="High Wycombe">High Wycombe</asp:ListItem>
                                <asp:ListItem Value="Higham (Kent)">Higham (Kent)</asp:ListItem>
                                <asp:ListItem Value="Highams Park">Highams Park</asp:ListItem>
                                <asp:ListItem Value="Hildenborough">Hildenborough</asp:ListItem>
                                <asp:ListItem Value="Hillfoot">Hillfoot</asp:ListItem>
                                <asp:ListItem Value="Hillington East">Hillington East</asp:ListItem>
                                <asp:ListItem Value="Hillington West">Hillington West</asp:ListItem>
                                <asp:ListItem Value="Hilsea">Hilsea</asp:ListItem>
                                <asp:ListItem Value="Hinchley Wood">Hinchley Wood</asp:ListItem>
                                <asp:ListItem Value="Hinckley (Leics)">Hinckley (Leics)</asp:ListItem>
                                <asp:ListItem Value="Hinton Admiral">Hinton Admiral</asp:ListItem>
                                <asp:ListItem Value="Hitchin">Hitchin</asp:ListItem>
                                <asp:ListItem Value="Hither Green">Hither Green</asp:ListItem>
                                <asp:ListItem Value="Hockley">Hockley</asp:ListItem>
                                <asp:ListItem Value="Holmwood (Surrey)">Holmwood (Surrey)</asp:ListItem>
                                <asp:ListItem Value="Holyhead">Holyhead</asp:ListItem>
                                <asp:ListItem Value="Homerton">Homerton</asp:ListItem>
                                <asp:ListItem Value="Honiton">Honiton</asp:ListItem>
                                <asp:ListItem Value="Honor Oak Park">Honor Oak Park</asp:ListItem>
                                <asp:ListItem Value="Hook">Hook</asp:ListItem>
                                <asp:ListItem Value="Horley">Horley</asp:ListItem>
                                <asp:ListItem Value="Hornbeam Park">Hornbeam Park</asp:ListItem>
                                <asp:ListItem Value="Hornsey">Hornsey</asp:ListItem>
                                <asp:ListItem Value="Horsforth">Horsforth</asp:ListItem>
                                <asp:ListItem Value="Horsham">Horsham</asp:ListItem>
                                <asp:ListItem Value="Horsley">Horsley</asp:ListItem>
                                <asp:ListItem Value="Hounslow">Hounslow</asp:ListItem>
                                <asp:ListItem Value="Hove">Hove</asp:ListItem>
                                <asp:ListItem Value="Hoxton">Hoxton</asp:ListItem>
                                <asp:ListItem Value="Huddersfield">Huddersfield</asp:ListItem>
                                <asp:ListItem Value="Hull">Hull</asp:ListItem>
                                <asp:ListItem Value="Hungerford">Hungerford</asp:ListItem>
                                <asp:ListItem Value="Huntingdon">Huntingdon</asp:ListItem>
                                <asp:ListItem Value="Huntly">Huntly</asp:ListItem>
                                <asp:ListItem Value="Hurst Green">Hurst Green</asp:ListItem>
                                <asp:ListItem Value="Huyton">Huyton</asp:ListItem>
                                <asp:ListItem Value="Hyndland">Hyndland</asp:ListItem>
                                <asp:ListItem Value="Hythe (Essex)">Hythe (Essex)</asp:ListItem>
                                <asp:ListItem Value="Ifield">Ifield</asp:ListItem>
                                <asp:ListItem Value="Ilford">Ilford</asp:ListItem>
                                <asp:ListItem Value="Ilkley">Ilkley</asp:ListItem>
                                <asp:ListItem Value="Ingatestone">Ingatestone</asp:ListItem>
                                <asp:ListItem Value="Insch">Insch</asp:ListItem>
                                <asp:ListItem Value="Inverkeithing">Inverkeithing</asp:ListItem>
                                <asp:ListItem Value="Inverness">Inverness</asp:ListItem>
                                <asp:ListItem Value="Inverurie">Inverurie</asp:ListItem>
                                <asp:ListItem Value="Ipswich">Ipswich</asp:ListItem>
                                <asp:ListItem Value="Irlam">Irlam</asp:ListItem>
                                <asp:ListItem Value="Irvine">Irvine</asp:ListItem>
                                <asp:ListItem Value="Isleworth">Isleworth</asp:ListItem>
                                <asp:ListItem Value="Islip">Islip</asp:ListItem>
                                <asp:ListItem Value="Iver">Iver</asp:ListItem>
                                <asp:ListItem Value="Jewellery Quarter">Jewellery Quarter</asp:ListItem>
                                <asp:ListItem Value="Johnstone (Strathclyde)">Johnstone (Strathclyde)</asp:ListItem>
                                <asp:ListItem Value="Jordanhill">Jordanhill</asp:ListItem>
                                <asp:ListItem Value="Kearsney (Kent)">Kearsney (Kent)</asp:ListItem>
                                <asp:ListItem Value="Keighley">Keighley</asp:ListItem>
                                <asp:ListItem Value="Keith">Keith</asp:ListItem>
                                <asp:ListItem Value="Kelvedon">Kelvedon</asp:ListItem>
                                <asp:ListItem Value="Kemble">Kemble</asp:ListItem>
                                <asp:ListItem Value="Kempton Park Racecourse">Kempton Park Racecourse</asp:ListItem>
                                <asp:ListItem Value="Kenley">Kenley</asp:ListItem>
                                <asp:ListItem Value="Kensal Green">Kensal Green</asp:ListItem>
                                <asp:ListItem Value="Kensal Rise">Kensal Rise</asp:ListItem>
                                <asp:ListItem Value="Kensington Olympia">Kensington Olympia</asp:ListItem>
                                <asp:ListItem Value="Kent House">Kent House</asp:ListItem>
                                <asp:ListItem Value="Kentish Town West">Kentish Town West</asp:ListItem>
                                <asp:ListItem Value="Kenton">Kenton</asp:ListItem>
                                <asp:ListItem Value="Kettering">Kettering</asp:ListItem>
                                <asp:ListItem Value="Kew Bridge">Kew Bridge</asp:ListItem>
                                <asp:ListItem Value="Kew Gardens">Kew Gardens</asp:ListItem>
                                <asp:ListItem Value="Kidbrooke">Kidbrooke</asp:ListItem>
                                <asp:ListItem Value="Kidderminster">Kidderminster</asp:ListItem>
                                <asp:ListItem Value="Kidsgrove">Kidsgrove</asp:ListItem>
                                <asp:ListItem Value="Kilburn High Road">Kilburn High Road</asp:ListItem>
                                <asp:ListItem Value="Kilmarnock">Kilmarnock</asp:ListItem>
                                <asp:ListItem Value="Kilmaurs">Kilmaurs</asp:ListItem>
                                <asp:ListItem Value="Kilwinning">Kilwinning</asp:ListItem>
                                <asp:ListItem Value="Kinghorn">Kinghorn</asp:ListItem>
                                <asp:ListItem Value="Kings Langley">Kings Langley</asp:ListItem>
                                <asp:ListItem Value="Kings Lynn">Kings Lynn</asp:ListItem>
                                <asp:ListItem Value="Kings Norton">Kings Norton</asp:ListItem>
                                <asp:ListItem Value="Kings Park">Kings Park</asp:ListItem>
                                <asp:ListItem Value="Kings Sutton">Kings Sutton</asp:ListItem>
                                <asp:ListItem Value="Kingston">Kingston</asp:ListItem>
                                <asp:ListItem Value="Kingswood">Kingswood</asp:ListItem>
                                <asp:ListItem Value="Kingussie">Kingussie</asp:ListItem>
                                <asp:ListItem Value="Kirkcaldy">Kirkcaldy</asp:ListItem>
                                <asp:ListItem Value="Kirkham &amp; Wesham">Kirkham &amp; Wesham</asp:ListItem>
                                <asp:ListItem Value="Knaresborough">Knaresborough</asp:ListItem>
                                <asp:ListItem Value="Knebworth">Knebworth</asp:ListItem>
                                <asp:ListItem Value="Knockholt">Knockholt</asp:ListItem>
                                <asp:ListItem Value="Knutsford">Knutsford</asp:ListItem>
                                <asp:ListItem Value="Kyle of Lochalsh">Kyle of Lochalsh</asp:ListItem>
                                <asp:ListItem Value="Ladybank">Ladybank</asp:ListItem>
                                <asp:ListItem Value="Ladywell">Ladywell</asp:ListItem>
                                <asp:ListItem Value="Laindon">Laindon</asp:ListItem>
                                <asp:ListItem Value="Lanark">Lanark</asp:ListItem>
                                <asp:ListItem Value="Lancaster">Lancaster</asp:ListItem>
                                <asp:ListItem Value="Lancing">Lancing</asp:ListItem>
                                <asp:ListItem Value="Landywood">Landywood</asp:ListItem>
                                <asp:ListItem Value="Langley (Berks)">Langley (Berks)</asp:ListItem>
                                <asp:ListItem Value="Langley Green">Langley Green</asp:ListItem>
                                <asp:ListItem Value="Langside">Langside</asp:ListItem>
                                <asp:ListItem Value="Larbert">Larbert</asp:ListItem>
                                <asp:ListItem Value="Largs">Largs</asp:ListItem>
                                <asp:ListItem Value="Larkhall">Larkhall</asp:ListItem>
                                <asp:ListItem Value="Laurencekirk">Laurencekirk</asp:ListItem>
                                <asp:ListItem Value="Lea Green">Lea Green</asp:ListItem>
                                <asp:ListItem Value="Lea Hall">Lea Hall</asp:ListItem>
                                <asp:ListItem Value="Leagrave">Leagrave</asp:ListItem>
                                <asp:ListItem Value="Leamington Spa">Leamington Spa</asp:ListItem>
                                <asp:ListItem Value="Leatherhead">Leatherhead</asp:ListItem>
                                <asp:ListItem Value="Lee (London)">Lee (London)</asp:ListItem>
                                <asp:ListItem Value="Leeds">Leeds</asp:ListItem>
                                <asp:ListItem Value="Leicester">Leicester</asp:ListItem>
                                <asp:ListItem Value="Leigh (Kent)">Leigh (Kent)</asp:ListItem>
                                <asp:ListItem Value="Leigh-on-Sea">Leigh-on-Sea</asp:ListItem>
                                <asp:ListItem Value="Leighton Buzzard">Leighton Buzzard</asp:ListItem>
                                <asp:ListItem Value="Lenham">Lenham</asp:ListItem>
                                <asp:ListItem Value="Lenzie">Lenzie</asp:ListItem>
                                <asp:ListItem Value="Leominster">Leominster</asp:ListItem>
                                <asp:ListItem Value="Letchworth Garden City">Letchworth Garden City</asp:ListItem>
                                <asp:ListItem Value="Leuchars">Leuchars</asp:ListItem>
                                <asp:ListItem Value="Levenshulme">Levenshulme</asp:ListItem>
                                <asp:ListItem Value="Lewes">Lewes</asp:ListItem>
                                <asp:ListItem Value="Lewisham">Lewisham</asp:ListItem>
                                <asp:ListItem Value="Leyland">Leyland</asp:ListItem>
                                <asp:ListItem Value="Leyton Midland Road">Leyton Midland Road</asp:ListItem>
                                <asp:ListItem Value="Leytonstone High Road">Leytonstone High Road</asp:ListItem>
                                <asp:ListItem Value="Lichfield City">Lichfield City</asp:ListItem>
                                <asp:ListItem Value="Lichfield Trent Valley">Lichfield Trent Valley</asp:ListItem>
                                <asp:ListItem Value="Lincoln">Lincoln</asp:ListItem>
                                <asp:ListItem Value="Lingfield">Lingfield</asp:ListItem>
                                <asp:ListItem Value="Linlithgow">Linlithgow</asp:ListItem>
                                <asp:ListItem Value="Liphook">Liphook</asp:ListItem>
                                <asp:ListItem Value="Liskeard">Liskeard</asp:ListItem>
                                <asp:ListItem Value="Liss">Liss</asp:ListItem>
                                <asp:ListItem Value="Lisvane &amp; Thornhill">Lisvane &amp; Thornhill</asp:ListItem>
                                <asp:ListItem Value="Littleborough">Littleborough</asp:ListItem>
                                <asp:ListItem Value="Littlehampton">Littlehampton</asp:ListItem>
                                <asp:ListItem Value="Littlehaven">Littlehaven</asp:ListItem>
                                <asp:ListItem Value="Littleport">Littleport</asp:ListItem>
                                <asp:ListItem Value="Liverpool Lime St">Liverpool Lime St</asp:ListItem>
                                <asp:ListItem Value="Liverpool South Parkway">Liverpool South Parkway</asp:ListItem>
                                <asp:ListItem Value="Livingston North">Livingston North</asp:ListItem>
                                <asp:ListItem Value="Livingston South">Livingston South</asp:ListItem>
                                <asp:ListItem Value="Llandaf">Llandaf</asp:ListItem>
                                <asp:ListItem Value="Llandrindod">Llandrindod</asp:ListItem>
                                <asp:ListItem Value="Llandudno">Llandudno</asp:ListItem>
                                <asp:ListItem Value="Llandudno Junction">Llandudno Junction</asp:ListItem>
                                <asp:ListItem Value="Llanelli">Llanelli</asp:ListItem>
                                <asp:ListItem Value="Llanharan">Llanharan</asp:ListItem>
                                <asp:ListItem Value="Llanhilleth">Llanhilleth</asp:ListItem>
                                <asp:ListItem Value="Llanishen">Llanishen</asp:ListItem>
                                <asp:ListItem Value="Llantwit Major">Llantwit Major</asp:ListItem>
                                <asp:ListItem Value="Lochwinnoch">Lochwinnoch</asp:ListItem>
                                <asp:ListItem Value="Lockerbie">Lockerbie</asp:ListItem>
                                <asp:ListItem Value="London Blackfriars">London Blackfriars</asp:ListItem>
                                <asp:ListItem Value="London Bridge">London Bridge</asp:ListItem>
                                <asp:ListItem Value="London Cannon Street">London Cannon Street</asp:ListItem>
                                <asp:ListItem Value="London Charing Cross">London Charing Cross</asp:ListItem>
                                <asp:ListItem Value="London Euston">London Euston</asp:ListItem>
                                <asp:ListItem Value="London Fenchurch Street">London Fenchurch Street</asp:ListItem>
                                <asp:ListItem Value="London Fields">London Fields</asp:ListItem>
                                <asp:ListItem Value="London Kings Cross">London Kings Cross</asp:ListItem>
                                <asp:ListItem Value="London Liverpool Street">London Liverpool Street</asp:ListItem>
                                <asp:ListItem Value="London Marylebone">London Marylebone</asp:ListItem>
                                <asp:ListItem Value="London Paddington">London Paddington</asp:ListItem>
                                <asp:ListItem Value="London Road (Brighton)">London Road (Brighton)</asp:ListItem>
                                <asp:ListItem Value="London Road (Guildford)">London Road (Guildford)</asp:ListItem>
                                <asp:ListItem Value="London St Pancras">London St Pancras</asp:ListItem>
                                <asp:ListItem Value="London Victoria">London Victoria</asp:ListItem>
                                <asp:ListItem Value="London Waterloo">London Waterloo</asp:ListItem>
                                <asp:ListItem Value="London Waterloo East">London Waterloo East</asp:ListItem>
                                <asp:ListItem Value="Long Buckby">Long Buckby</asp:ListItem>
                                <asp:ListItem Value="Long Eaton">Long Eaton</asp:ListItem>
                                <asp:ListItem Value="Longbridge">Longbridge</asp:ListItem>
                                <asp:ListItem Value="Longfield">Longfield</asp:ListItem>
                                <asp:ListItem Value="Longniddry">Longniddry</asp:ListItem>
                                <asp:ListItem Value="Loughborough">Loughborough</asp:ListItem>
                                <asp:ListItem Value="Loughborough Junction">Loughborough Junction</asp:ListItem>
                                <asp:ListItem Value="Lower Sydenham">Lower Sydenham</asp:ListItem>
                                <asp:ListItem Value="Lowestoft">Lowestoft</asp:ListItem>
                                <asp:ListItem Value="Ludlow">Ludlow</asp:ListItem>
                                <asp:ListItem Value="Luton">Luton</asp:ListItem>
                                <asp:ListItem Value="Luton Airport Parkway">Luton Airport Parkway</asp:ListItem>
                                <asp:ListItem Value="Lydney">Lydney</asp:ListItem>
                                <asp:ListItem Value="Lymington Pier">Lymington Pier</asp:ListItem>
                                <asp:ListItem Value="Lymington Town">Lymington Town</asp:ListItem>
                                <asp:ListItem Value="Macclesfield">Macclesfield</asp:ListItem>
                                <asp:ListItem Value="Machynlleth">Machynlleth</asp:ListItem>
                                <asp:ListItem Value="Maesteg">Maesteg</asp:ListItem>
                                <asp:ListItem Value="Maidenhead">Maidenhead</asp:ListItem>
                                <asp:ListItem Value="Maidstone East">Maidstone East</asp:ListItem>
                                <asp:ListItem Value="Maidstone West">Maidstone West</asp:ListItem>
                                <asp:ListItem Value="Malden Manor">Malden Manor</asp:ListItem>
                                <asp:ListItem Value="Mallaig">Mallaig</asp:ListItem>
                                <asp:ListItem Value="Malton">Malton</asp:ListItem>
                                <asp:ListItem Value="Malvern Link">Malvern Link</asp:ListItem>
                                <asp:ListItem Value="Manchester Airport">Manchester Airport</asp:ListItem>
                                <asp:ListItem Value="Manchester Oxford Road">Manchester Oxford Road</asp:ListItem>
                                <asp:ListItem Value="Manchester Piccadilly">Manchester Piccadilly</asp:ListItem>
                                <asp:ListItem Value="Manchester Victoria">Manchester Victoria</asp:ListItem>
                                <asp:ListItem Value="Manningtree">Manningtree</asp:ListItem>
                                <asp:ListItem Value="Manor Park">Manor Park</asp:ListItem>
                                <asp:ListItem Value="Mansfield">Mansfield</asp:ListItem>
                                <asp:ListItem Value="March">March</asp:ListItem>
                                <asp:ListItem Value="Marden (Kent)">Marden (Kent)</asp:ListItem>
                                <asp:ListItem Value="Margate">Margate</asp:ListItem>
                                <asp:ListItem Value="Market Harborough">Market Harborough</asp:ListItem>
                                <asp:ListItem Value="Markinch">Markinch</asp:ListItem>
                                <asp:ListItem Value="Marks Tey">Marks Tey</asp:ListItem>
                                <asp:ListItem Value="Marple">Marple</asp:ListItem>
                                <asp:ListItem Value="Marston Green">Marston Green</asp:ListItem>
                                <asp:ListItem Value="Martin Mill">Martin Mill</asp:ListItem>
                                <asp:ListItem Value="Martins Heron">Martins Heron</asp:ListItem>
                                <asp:ListItem Value="Maryland">Maryland</asp:ListItem>
                                <asp:ListItem Value="Maryport">Maryport</asp:ListItem>
                                <asp:ListItem Value="Matlock">Matlock</asp:ListItem>
                                <asp:ListItem Value="Matlock Bath">Matlock Bath</asp:ListItem>
                                <asp:ListItem Value="Mauldeth Road">Mauldeth Road</asp:ListItem>
                                <asp:ListItem Value="Maxwell Park">Maxwell Park</asp:ListItem>
                                <asp:ListItem Value="Maze Hill">Maze Hill</asp:ListItem>
                                <asp:ListItem Value="Meadowhall">Meadowhall</asp:ListItem>
                                <asp:ListItem Value="Meldreth">Meldreth</asp:ListItem>
                                <asp:ListItem Value="Melksham">Melksham</asp:ListItem>
                                <asp:ListItem Value="Melton Mowbray">Melton Mowbray</asp:ListItem>
                                <asp:ListItem Value="Menston">Menston</asp:ListItem>
                                <asp:ListItem Value="Meopham">Meopham</asp:ListItem>
                                <asp:ListItem Value="Merstham">Merstham</asp:ListItem>
                                <asp:ListItem Value="Merthyr Tydfil">Merthyr Tydfil</asp:ListItem>
                                <asp:ListItem Value="Metrocentre">Metrocentre</asp:ListItem>
                                <asp:ListItem Value="Micheldever">Micheldever</asp:ListItem>
                                <asp:ListItem Value="Middlesbrough">Middlesbrough</asp:ListItem>
                                <asp:ListItem Value="Milford (Surrey)">Milford (Surrey)</asp:ListItem>
                                <asp:ListItem Value="Milford Haven">Milford Haven</asp:ListItem>
                                <asp:ListItem Value="Mill Hill Broadway">Mill Hill Broadway</asp:ListItem>
                                <asp:ListItem Value="Mills Hill (Manchester)">Mills Hill (Manchester)</asp:ListItem>
                                <asp:ListItem Value="Milngavie">Milngavie</asp:ListItem>
                                <asp:ListItem Value="Milton Keynes Central">Milton Keynes Central</asp:ListItem>
                                <asp:ListItem Value="Mitcham Eastfields">Mitcham Eastfields</asp:ListItem>
                                <asp:ListItem Value="Mitcham Junction">Mitcham Junction</asp:ListItem>
                                <asp:ListItem Value="Monks Risborough">Monks Risborough</asp:ListItem>
                                <asp:ListItem Value="Montrose">Montrose</asp:ListItem>
                                <asp:ListItem Value="Moorthorpe">Moorthorpe</asp:ListItem>
                                <asp:ListItem Value="Morden South">Morden South</asp:ListItem>
                                <asp:ListItem Value="Moreton (Dorset)">Moreton (Dorset)</asp:ListItem>
                                <asp:ListItem Value="Morpeth">Morpeth</asp:ListItem>
                                <asp:ListItem Value="Mortlake">Mortlake</asp:ListItem>
                                <asp:ListItem Value="Mosspark">Mosspark</asp:ListItem>
                                <asp:ListItem Value="Motherwell">Motherwell</asp:ListItem>
                                <asp:ListItem Value="Motspur Park">Motspur Park</asp:ListItem>
                                <asp:ListItem Value="Mottingham">Mottingham</asp:ListItem>
                                <asp:ListItem Value="Moulsecoomb">Moulsecoomb</asp:ListItem>
                                <asp:ListItem Value="Mount Florida">Mount Florida</asp:ListItem>
                                <asp:ListItem Value="Muirend">Muirend</asp:ListItem>
                                <asp:ListItem Value="Musselburgh">Musselburgh</asp:ListItem>
                                <asp:ListItem Value="Mytholmroyd">Mytholmroyd</asp:ListItem>
                                <asp:ListItem Value="Nailsea &amp; Backwell">Nailsea &amp; Backwell</asp:ListItem>
                                <asp:ListItem Value="Nairn">Nairn</asp:ListItem>
                                <asp:ListItem Value="Nantwich">Nantwich</asp:ListItem>
                                <asp:ListItem Value="Narborough">Narborough</asp:ListItem>
                                <asp:ListItem Value="Neath">Neath</asp:ListItem>
                                <asp:ListItem Value="Neilston">Neilston</asp:ListItem>
                                <asp:ListItem Value="Netley">Netley</asp:ListItem>
                                <asp:ListItem Value="New Barnet">New Barnet</asp:ListItem>
                                <asp:ListItem Value="New Beckenham">New Beckenham</asp:ListItem>
                                <asp:ListItem Value="New Cross">New Cross</asp:ListItem>
                                <asp:ListItem Value="New Cross Gate">New Cross Gate</asp:ListItem>
                                <asp:ListItem Value="New Eltham">New Eltham</asp:ListItem>
                                <asp:ListItem Value="New Malden">New Malden</asp:ListItem>
                                <asp:ListItem Value="New Milton">New Milton</asp:ListItem>
                                <asp:ListItem Value="New Pudsey">New Pudsey</asp:ListItem>
                                <asp:ListItem Value="New Southgate">New Southgate</asp:ListItem>
                                <asp:ListItem Value="Newark Castle">Newark Castle</asp:ListItem>
                                <asp:ListItem Value="Newark North Gate">Newark North Gate</asp:ListItem>
                                <asp:ListItem Value="Newbridge">Newbridge</asp:ListItem>
                                <asp:ListItem Value="Newbury">Newbury</asp:ListItem>
                                <asp:ListItem Value="Newcastle">Newcastle</asp:ListItem>
                                <asp:ListItem Value="Newcraighall">Newcraighall</asp:ListItem>
                                <asp:ListItem Value="Newhaven Town">Newhaven Town</asp:ListItem>
                                <asp:ListItem Value="Newington">Newington</asp:ListItem>
                                <asp:ListItem Value="Newmarket">Newmarket</asp:ListItem>
                                <asp:ListItem Value="Newport (Essex)">Newport (Essex)</asp:ListItem>
                                <asp:ListItem Value="Newport (South Wales)">Newport (South Wales)</asp:ListItem>
                                <asp:ListItem Value="Newton (Lanarkshire)">Newton (Lanarkshire)</asp:ListItem>
                                <asp:ListItem Value="Newton Abbot">Newton Abbot</asp:ListItem>
                                <asp:ListItem Value="Newton Le Willows">Newton Le Willows</asp:ListItem>
                                <asp:ListItem Value="Newtongrange">Newtongrange</asp:ListItem>
                                <asp:ListItem Value="Newtown (Powys)">Newtown (Powys)</asp:ListItem>
                                <asp:ListItem Value="Ninian Park">Ninian Park</asp:ListItem>
                                <asp:ListItem Value="Norbiton">Norbiton</asp:ListItem>
                                <asp:ListItem Value="Norbury">Norbury</asp:ListItem>
                                <asp:ListItem Value="Normanton">Normanton</asp:ListItem>
                                <asp:ListItem Value="North Berwick">North Berwick</asp:ListItem>
                                <asp:ListItem Value="North Camp">North Camp</asp:ListItem>
                                <asp:ListItem Value="North Dulwich">North Dulwich</asp:ListItem>
                                <asp:ListItem Value="North Queensferry">North Queensferry</asp:ListItem>
                                <asp:ListItem Value="North Sheen">North Sheen</asp:ListItem>
                                <asp:ListItem Value="North Walsham">North Walsham</asp:ListItem>
                                <asp:ListItem Value="North Wembley">North Wembley</asp:ListItem>
                                <asp:ListItem Value="Northallerton">Northallerton</asp:ListItem>
                                <asp:ListItem Value="Northampton">Northampton</asp:ListItem>
                                <asp:ListItem Value="Northfield">Northfield</asp:ListItem>
                                <asp:ListItem Value="Northfleet">Northfleet</asp:ListItem>
                                <asp:ListItem Value="Northolt Park">Northolt Park</asp:ListItem>
                                <asp:ListItem Value="Northumberland Park">Northumberland Park</asp:ListItem>
                                <asp:ListItem Value="Northwich">Northwich</asp:ListItem>
                                <asp:ListItem Value="Norwich">Norwich</asp:ListItem>
                                <asp:ListItem Value="Norwood Junction">Norwood Junction</asp:ListItem>
                                <asp:ListItem Value="Nottingham">Nottingham</asp:ListItem>
                                <asp:ListItem Value="Nuneaton">Nuneaton</asp:ListItem>
                                <asp:ListItem Value="Nunhead">Nunhead</asp:ListItem>
                                <asp:ListItem Value="Nutbourne">Nutbourne</asp:ListItem>
                                <asp:ListItem Value="Nutfield">Nutfield</asp:ListItem>
                                <asp:ListItem Value="Oakham">Oakham</asp:ListItem>
                                <asp:ListItem Value="Oakleigh Park">Oakleigh Park</asp:ListItem>
                                <asp:ListItem Value="Oban">Oban</asp:ListItem>
                                <asp:ListItem Value="Ockendon">Ockendon</asp:ListItem>
                                <asp:ListItem Value="Ockley">Ockley</asp:ListItem>
                                <asp:ListItem Value="Old Hill">Old Hill</asp:ListItem>
                                <asp:ListItem Value="Olton">Olton</asp:ListItem>
                                <asp:ListItem Value="Orpington">Orpington</asp:ListItem>
                                <asp:ListItem Value="Otford">Otford</asp:ListItem>
                                <asp:ListItem Value="Overton">Overton</asp:ListItem>
                                <asp:ListItem Value="Oxenholme Lake District">Oxenholme Lake District</asp:ListItem>
                                <asp:ListItem Value="Oxford">Oxford</asp:ListItem>
                                <asp:ListItem Value="Oxford Parkway">Oxford Parkway</asp:ListItem>
                                <asp:ListItem Value="Oxshott">Oxshott</asp:ListItem>
                                <asp:ListItem Value="Oxted">Oxted</asp:ListItem>
                                <asp:ListItem Value="Paddock Wood">Paddock Wood</asp:ListItem>
                                <asp:ListItem Value="Paignton">Paignton</asp:ListItem>
                                <asp:ListItem Value="Paisley Canal">Paisley Canal</asp:ListItem>
                                <asp:ListItem Value="Paisley Gilmour Street">Paisley Gilmour Street</asp:ListItem>
                                <asp:ListItem Value="Palmers Green">Palmers Green</asp:ListItem>
                                <asp:ListItem Value="Pangbourne">Pangbourne</asp:ListItem>
                                <asp:ListItem Value="Pannal">Pannal</asp:ListItem>
                                <asp:ListItem Value="Par">Par</asp:ListItem>
                                <asp:ListItem Value="Parkstone (Dorset)">Parkstone (Dorset)</asp:ListItem>
                                <asp:ListItem Value="Partick">Partick</asp:ListItem>
                                <asp:ListItem Value="Patterton">Patterton</asp:ListItem>
                                <asp:ListItem Value="Peckham Rye">Peckham Rye</asp:ListItem>
                                <asp:ListItem Value="Pembrey &amp; Burry Port">Pembrey &amp; Burry Port</asp:ListItem>
                                <asp:ListItem Value="Pembroke Dock">Pembroke Dock</asp:ListItem>
                                <asp:ListItem Value="Penarth">Penarth</asp:ListItem>
                                <asp:ListItem Value="Pencoed">Pencoed</asp:ListItem>
                                <asp:ListItem Value="Pengam">Pengam</asp:ListItem>
                                <asp:ListItem Value="Penge East">Penge East</asp:ListItem>
                                <asp:ListItem Value="Penge West">Penge West</asp:ListItem>
                                <asp:ListItem Value="Penkridge">Penkridge</asp:ListItem>
                                <asp:ListItem Value="Penrith">Penrith</asp:ListItem>
                                <asp:ListItem Value="Penshurst">Penshurst</asp:ListItem>
                                <asp:ListItem Value="Penzance">Penzance</asp:ListItem>
                                <asp:ListItem Value="Perth">Perth</asp:ListItem>
                                <asp:ListItem Value="Peterborough">Peterborough</asp:ListItem>
                                <asp:ListItem Value="Petersfield">Petersfield</asp:ListItem>
                                <asp:ListItem Value="Petts Wood">Petts Wood</asp:ListItem>
                                <asp:ListItem Value="Pevensey &amp; Westham">Pevensey &amp; Westham</asp:ListItem>
                                <asp:ListItem Value="Pewsey">Pewsey</asp:ListItem>
                                <asp:ListItem Value="Pinhoe">Pinhoe</asp:ListItem>
                                <asp:ListItem Value="Pitlochry">Pitlochry</asp:ListItem>
                                <asp:ListItem Value="Pitsea">Pitsea</asp:ListItem>
                                <asp:ListItem Value="Pluckley">Pluckley</asp:ListItem>
                                <asp:ListItem Value="Plumpton">Plumpton</asp:ListItem>
                                <asp:ListItem Value="Plumstead">Plumstead</asp:ListItem>
                                <asp:ListItem Value="Plymouth">Plymouth</asp:ListItem>
                                <asp:ListItem Value="Pokesdown">Pokesdown</asp:ListItem>
                                <asp:ListItem Value="Polegate">Polegate</asp:ListItem>
                                <asp:ListItem Value="Pollokshaws East">Pollokshaws East</asp:ListItem>
                                <asp:ListItem Value="Pollokshaws West">Pollokshaws West</asp:ListItem>
                                <asp:ListItem Value="Pollokshields East">Pollokshields East</asp:ListItem>
                                <asp:ListItem Value="Pollokshields West">Pollokshields West</asp:ListItem>
                                <asp:ListItem Value="Polmont">Polmont</asp:ListItem>
                                <asp:ListItem Value="Ponders End">Ponders End</asp:ListItem>
                                <asp:ListItem Value="Pontyclun">Pontyclun</asp:ListItem>
                                <asp:ListItem Value="Pontypridd">Pontypridd</asp:ListItem>
                                <asp:ListItem Value="Poole">Poole</asp:ListItem>
                                <asp:ListItem Value="Port Glasgow">Port Glasgow</asp:ListItem>
                                <asp:ListItem Value="Port Talbot Parkway">Port Talbot Parkway</asp:ListItem>
                                <asp:ListItem Value="Portchester">Portchester</asp:ListItem>
                                <asp:ListItem Value="Porth">Porth</asp:ListItem>
                                <asp:ListItem Value="Portslade">Portslade</asp:ListItem>
                                <asp:ListItem Value="Portsmouth &amp; Southsea">Portsmouth &amp; Southsea</asp:ListItem>
                                <asp:ListItem Value="Portsmouth Harbour">Portsmouth Harbour</asp:ListItem>
                                <asp:ListItem Value="Potters Bar">Potters Bar</asp:ListItem>
                                <asp:ListItem Value="Poulton Le Fylde">Poulton Le Fylde</asp:ListItem>
                                <asp:ListItem Value="Prestatyn">Prestatyn</asp:ListItem>
                                <asp:ListItem Value="Preston (Lancs)">Preston (Lancs)</asp:ListItem>
                                <asp:ListItem Value="Preston Park">Preston Park</asp:ListItem>
                                <asp:ListItem Value="Prestonpans">Prestonpans</asp:ListItem>
                                <asp:ListItem Value="Prestwick (Strathclyde)">Prestwick (Strathclyde)</asp:ListItem>
                                <asp:ListItem Value="Princes Risborough">Princes Risborough</asp:ListItem>
                                <asp:ListItem Value="Prittlewell">Prittlewell</asp:ListItem>
                                <asp:ListItem Value="Prudhoe">Prudhoe</asp:ListItem>
                                <asp:ListItem Value="Pulborough">Pulborough</asp:ListItem>
                                <asp:ListItem Value="Purfleet">Purfleet</asp:ListItem>
                                <asp:ListItem Value="Purley">Purley</asp:ListItem>
                                <asp:ListItem Value="Purley Oaks">Purley Oaks</asp:ListItem>
                                <asp:ListItem Value="Putney">Putney</asp:ListItem>
                                <asp:ListItem Value="Pye Corner">Pye Corner</asp:ListItem>
                                <asp:ListItem Value="Queenborough">Queenborough</asp:ListItem>
                                <asp:ListItem Value="Queens Park (Glasgow)">Queens Park (Glasgow)</asp:ListItem>
                                <asp:ListItem Value="Queens Park (London)">Queens Park (London)</asp:ListItem>
                                <asp:ListItem Value="Queens Road Peckham">Queens Road Peckham</asp:ListItem>
                                <asp:ListItem Value="Queenstown Road (Battersea)">Queenstown Road (Battersea)</asp:ListItem>
                                <asp:ListItem Value="Radlett">Radlett</asp:ListItem>
                                <asp:ListItem Value="Radyr">Radyr</asp:ListItem>
                                <asp:ListItem Value="Rainham (Essex)">Rainham (Essex)</asp:ListItem>
                                <asp:ListItem Value="Rainham (Kent)">Rainham (Kent)</asp:ListItem>
                                <asp:ListItem Value="Ramsgate">Ramsgate</asp:ListItem>
                                <asp:ListItem Value="Ravensbourne">Ravensbourne</asp:ListItem>
                                <asp:ListItem Value="Rayleigh">Rayleigh</asp:ListItem>
                                <asp:ListItem Value="Raynes Park">Raynes Park</asp:ListItem>
                                <asp:ListItem Value="Reading">Reading</asp:ListItem>
                                <asp:ListItem Value="Reading West">Reading West</asp:ListItem>
                                <asp:ListItem Value="Rectory Road">Rectory Road</asp:ListItem>
                                <asp:ListItem Value="Redcar Central">Redcar Central</asp:ListItem>
                                <asp:ListItem Value="Redcar East">Redcar East</asp:ListItem>
                                <asp:ListItem Value="Redditch">Redditch</asp:ListItem>
                                <asp:ListItem Value="Redhill">Redhill</asp:ListItem>
                                <asp:ListItem Value="Redruth">Redruth</asp:ListItem>
                                <asp:ListItem Value="Reedham (London)">Reedham (London)</asp:ListItem>
                                <asp:ListItem Value="Reigate">Reigate</asp:ListItem>
                                <asp:ListItem Value="Retford">Retford</asp:ListItem>
                                <asp:ListItem Value="Rhiwbina">Rhiwbina</asp:ListItem>
                                <asp:ListItem Value="Rhyl">Rhyl</asp:ListItem>
                                <asp:ListItem Value="Richmond (London)">Richmond (London)</asp:ListItem>
                                <asp:ListItem Value="Riddlesdown">Riddlesdown</asp:ListItem>
                                <asp:ListItem Value="Risca Pontymistr">Risca Pontymistr</asp:ListItem>
                                <asp:ListItem Value="Robertsbridge">Robertsbridge</asp:ListItem>
                                <asp:ListItem Value="Rochdale">Rochdale</asp:ListItem>
                                <asp:ListItem Value="Rochester">Rochester</asp:ListItem>
                                <asp:ListItem Value="Rochford">Rochford</asp:ListItem>
                                <asp:ListItem Value="Rogerstone">Rogerstone</asp:ListItem>
                                <asp:ListItem Value="Romford">Romford</asp:ListItem>
                                <asp:ListItem Value="Romiley">Romiley</asp:ListItem>
                                <asp:ListItem Value="Romsey">Romsey</asp:ListItem>
                                <asp:ListItem Value="Rose Grove">Rose Grove</asp:ListItem>
                                <asp:ListItem Value="Rosyth">Rosyth</asp:ListItem>
                                <asp:ListItem Value="Rotherham Central">Rotherham Central</asp:ListItem>
                                <asp:ListItem Value="Rotherhithe">Rotherhithe</asp:ListItem>
                                <asp:ListItem Value="Rowlands Castle">Rowlands Castle</asp:ListItem>
                                <asp:ListItem Value="Rowley Regis">Rowley Regis</asp:ListItem>
                                <asp:ListItem Value="Roydon Essex">Roydon Essex</asp:ListItem>
                                <asp:ListItem Value="Royston (Herts)">Royston (Herts)</asp:ListItem>
                                <asp:ListItem Value="Rugby">Rugby</asp:ListItem>
                                <asp:ListItem Value="Rugeley (Trent Valley)">Rugeley (Trent Valley)</asp:ListItem>
                                <asp:ListItem Value="Rugeley Town">Rugeley Town</asp:ListItem>
                                <asp:ListItem Value="Runcorn">Runcorn</asp:ListItem>
                                <asp:ListItem Value="Runcorn East">Runcorn East</asp:ListItem>
                                <asp:ListItem Value="Rutherglen">Rutherglen</asp:ListItem>
                                <asp:ListItem Value="Ryde Esplanade">Ryde Esplanade</asp:ListItem>
                                <asp:ListItem Value="Rye (Sussex)">Rye (Sussex)</asp:ListItem>
                                <asp:ListItem Value="Rye House">Rye House</asp:ListItem>
                                <asp:ListItem Value="Salford Crescent">Salford Crescent</asp:ListItem>
                                <asp:ListItem Value="Salfords (Surrey)">Salfords (Surrey)</asp:ListItem>
                                <asp:ListItem Value="Salisbury">Salisbury</asp:ListItem>
                                <asp:ListItem Value="Saltaire">Saltaire</asp:ListItem>
                                <asp:ListItem Value="Saltburn">Saltburn</asp:ListItem>
                                <asp:ListItem Value="Saltcoats">Saltcoats</asp:ListItem>
                                <asp:ListItem Value="Sandal &amp; Agbrigg">Sandal &amp; Agbrigg</asp:ListItem>
                                <asp:ListItem Value="Sanderstead">Sanderstead</asp:ListItem>
                                <asp:ListItem Value="Sandling">Sandling</asp:ListItem>
                                <asp:ListItem Value="Sandwell &amp; Dudley">Sandwell &amp; Dudley</asp:ListItem>
                                <asp:ListItem Value="Sandwich">Sandwich</asp:ListItem>
                                <asp:ListItem Value="Sandy">Sandy</asp:ListItem>
                                <asp:ListItem Value="Saunderton">Saunderton</asp:ListItem>
                                <asp:ListItem Value="Sawbridgeworth">Sawbridgeworth</asp:ListItem>
                                <asp:ListItem Value="Saxmundham">Saxmundham</asp:ListItem>
                                <asp:ListItem Value="Scarborough">Scarborough</asp:ListItem>
                                <asp:ListItem Value="Scotstounhill">Scotstounhill</asp:ListItem>
                                <asp:ListItem Value="Scunthorpe">Scunthorpe</asp:ListItem>
                                <asp:ListItem Value="Seaford Sussex">Seaford Sussex</asp:ListItem>
                                <asp:ListItem Value="Seaham">Seaham</asp:ListItem>
                                <asp:ListItem Value="Seer Green">Seer Green</asp:ListItem>
                                <asp:ListItem Value="Selby">Selby</asp:ListItem>
                                <asp:ListItem Value="Selhurst">Selhurst</asp:ListItem>
                                <asp:ListItem Value="Selly Oak">Selly Oak</asp:ListItem>
                                <asp:ListItem Value="Settle">Settle</asp:ListItem>
                                <asp:ListItem Value="Seven Kings">Seven Kings</asp:ListItem>
                                <asp:ListItem Value="Seven Sisters">Seven Sisters</asp:ListItem>
                                <asp:ListItem Value="Sevenoaks">Sevenoaks</asp:ListItem>
                                <asp:ListItem Value="Severn Tunnel Junction">Severn Tunnel Junction</asp:ListItem>
                                <asp:ListItem Value="Shadwell">Shadwell</asp:ListItem>
                                <asp:ListItem Value="Shanklin">Shanklin</asp:ListItem>
                                <asp:ListItem Value="Shawfair">Shawfair</asp:ListItem>
                                <asp:ListItem Value="Shawford">Shawford</asp:ListItem>
                                <asp:ListItem Value="Shawlands">Shawlands</asp:ListItem>
                                <asp:ListItem Value="Sheerness on Sea">Sheerness on Sea</asp:ListItem>
                                <asp:ListItem Value="Sheffield">Sheffield</asp:ListItem>
                                <asp:ListItem Value="Shelford (Cambs)">Shelford (Cambs)</asp:ListItem>
                                <asp:ListItem Value="Shenfield">Shenfield</asp:ListItem>
                                <asp:ListItem Value="Shepherds Bush">Shepherds Bush</asp:ListItem>
                                <asp:ListItem Value="Shepherds Well">Shepherds Well</asp:ListItem>
                                <asp:ListItem Value="Shepperton">Shepperton</asp:ListItem>
                                <asp:ListItem Value="Shepreth">Shepreth</asp:ListItem>
                                <asp:ListItem Value="Sherborne">Sherborne</asp:ListItem>
                                <asp:ListItem Value="Shettleston">Shettleston</asp:ListItem>
                                <asp:ListItem Value="Shipley (Yorkshire)">Shipley (Yorkshire)</asp:ListItem>
                                <asp:ListItem Value="Shirley (West Midlands)">Shirley (West Midlands)</asp:ListItem>
                                <asp:ListItem Value="Shoeburyness">Shoeburyness</asp:ListItem>
                                <asp:ListItem Value="Sholing">Sholing</asp:ListItem>
                                <asp:ListItem Value="Shoreditch High Street">Shoreditch High Street</asp:ListItem>
                                <asp:ListItem Value="Shoreham by Sea">Shoreham by Sea</asp:ListItem>
                                <asp:ListItem Value="Shortlands">Shortlands</asp:ListItem>
                                <asp:ListItem Value="Shotton">Shotton</asp:ListItem>
                                <asp:ListItem Value="Shotts">Shotts</asp:ListItem>
                                <asp:ListItem Value="Shrewsbury">Shrewsbury</asp:ListItem>
                                <asp:ListItem Value="Sidcup">Sidcup</asp:ListItem>
                                <asp:ListItem Value="Silver Street">Silver Street</asp:ListItem>
                                <asp:ListItem Value="Singer">Singer</asp:ListItem>
                                <asp:ListItem Value="Sittingbourne">Sittingbourne</asp:ListItem>
                                <asp:ListItem Value="Skegness">Skegness</asp:ListItem>
                                <asp:ListItem Value="Skipton">Skipton</asp:ListItem>
                                <asp:ListItem Value="Slade Green">Slade Green</asp:ListItem>
                                <asp:ListItem Value="Slaithwaite">Slaithwaite</asp:ListItem>
                                <asp:ListItem Value="Sleaford">Sleaford</asp:ListItem>
                                <asp:ListItem Value="Slough">Slough</asp:ListItem>
                                <asp:ListItem Value="Smethwick Galton Bridge">Smethwick Galton Bridge</asp:ListItem>
                                <asp:ListItem Value="Smethwick Rolfe Street">Smethwick Rolfe Street</asp:ListItem>
                                <asp:ListItem Value="Smithy Bridge">Smithy Bridge</asp:ListItem>
                                <asp:ListItem Value="Sole Street">Sole Street</asp:ListItem>
                                <asp:ListItem Value="Solihull">Solihull</asp:ListItem>
                                <asp:ListItem Value="South Acton">South Acton</asp:ListItem>
                                <asp:ListItem Value="South Bermondsey">South Bermondsey</asp:ListItem>
                                <asp:ListItem Value="South Croydon">South Croydon</asp:ListItem>
                                <asp:ListItem Value="South Elmsall">South Elmsall</asp:ListItem>
                                <asp:ListItem Value="South Gyle">South Gyle</asp:ListItem>
                                <asp:ListItem Value="South Hampstead">South Hampstead</asp:ListItem>
                                <asp:ListItem Value="South Kenton">South Kenton</asp:ListItem>
                                <asp:ListItem Value="South Merton">South Merton</asp:ListItem>
                                <asp:ListItem Value="South Ruislip">South Ruislip</asp:ListItem>
                                <asp:ListItem Value="South Tottenham">South Tottenham</asp:ListItem>
                                <asp:ListItem Value="South Woodham Ferrers">South Woodham Ferrers</asp:ListItem>
                                <asp:ListItem Value="Southall">Southall</asp:ListItem>
                                <asp:ListItem Value="Southampton Airport Parkway">Southampton Airport Parkway</asp:ListItem>
                                <asp:ListItem Value="Southampton Central">Southampton Central</asp:ListItem>
                                <asp:ListItem Value="Southbourne">Southbourne</asp:ListItem>
                                <asp:ListItem Value="Southbury">Southbury</asp:ListItem>
                                <asp:ListItem Value="Southend Airport">Southend Airport</asp:ListItem>
                                <asp:ListItem Value="Southend Central">Southend Central</asp:ListItem>
                                <asp:ListItem Value="Southend East">Southend East</asp:ListItem>
                                <asp:ListItem Value="Southend Victoria">Southend Victoria</asp:ListItem>
                                <asp:ListItem Value="Southminster">Southminster</asp:ListItem>
                                <asp:ListItem Value="Southwick">Southwick</asp:ListItem>
                                <asp:ListItem Value="Sowerby Bridge">Sowerby Bridge</asp:ListItem>
                                <asp:ListItem Value="Spalding">Spalding</asp:ListItem>
                                <asp:ListItem Value="Spring Road">Spring Road</asp:ListItem>
                                <asp:ListItem Value="Springburn">Springburn</asp:ListItem>
                                <asp:ListItem Value="St Albans Abbey">St Albans Abbey</asp:ListItem>
                                <asp:ListItem Value="St Albans City">St Albans City</asp:ListItem>
                                <asp:ListItem Value="St Austell">St Austell</asp:ListItem>
                                <asp:ListItem Value="St Denys">St Denys</asp:ListItem>
                                <asp:ListItem Value="St Helens Central">St Helens Central</asp:ListItem>
                                <asp:ListItem Value="St Helens Junction">St Helens Junction</asp:ListItem>
                                <asp:ListItem Value="St Helier">St Helier</asp:ListItem>
                                <asp:ListItem Value="St James Street (Walthamstow)">St James Street (Walthamstow)</asp:ListItem>
                                <asp:ListItem Value="St Johns">St Johns</asp:ListItem>
                                <asp:ListItem Value="St Leonards Warrior Square">St Leonards Warrior Square</asp:ListItem>
                                <asp:ListItem Value="St Margarets (Herts)">St Margarets (Herts)</asp:ListItem>
                                <asp:ListItem Value="St Margarets (London)">St Margarets (London)</asp:ListItem>
                                <asp:ListItem Value="St Mary Cray">St Mary Cray</asp:ListItem>
                                <asp:ListItem Value="St Neots">St Neots</asp:ListItem>
                                <asp:ListItem Value="Stafford">Stafford</asp:ListItem>
                                <asp:ListItem Value="Staines">Staines</asp:ListItem>
                                <asp:ListItem Value="Stalybridge">Stalybridge</asp:ListItem>
                                <asp:ListItem Value="Stamford (Lincs)">Stamford (Lincs)</asp:ListItem>
                                <asp:ListItem Value="Stamford Hill">Stamford Hill</asp:ListItem>
                                <asp:ListItem Value="Stanford-le-Hope">Stanford-le-Hope</asp:ListItem>
                                <asp:ListItem Value="Stansted Airport">Stansted Airport</asp:ListItem>
                                <asp:ListItem Value="Stansted Mountfitchet">Stansted Mountfitchet</asp:ListItem>
                                <asp:ListItem Value="Staplehurst">Staplehurst</asp:ListItem>
                                <asp:ListItem Value="Starbeck">Starbeck</asp:ListItem>
                                <asp:ListItem Value="Steeton &amp; Silsden">Steeton &amp; Silsden</asp:ListItem>
                                <asp:ListItem Value="Stepps">Stepps</asp:ListItem>
                                <asp:ListItem Value="Stevenage">Stevenage</asp:ListItem>
                                <asp:ListItem Value="Stewarton">Stewarton</asp:ListItem>
                                <asp:ListItem Value="Stirling">Stirling</asp:ListItem>
                                <asp:ListItem Value="Stockport">Stockport</asp:ListItem>
                                <asp:ListItem Value="Stoke Mandeville">Stoke Mandeville</asp:ListItem>
                                <asp:ListItem Value="Stoke Newington">Stoke Newington</asp:ListItem>
                                <asp:ListItem Value="Stoke on Trent">Stoke on Trent</asp:ListItem>
                                <asp:ListItem Value="Stonebridge Park">Stonebridge Park</asp:ListItem>
                                <asp:ListItem Value="Stonegate">Stonegate</asp:ListItem>
                                <asp:ListItem Value="Stonehaven">Stonehaven</asp:ListItem>
                                <asp:ListItem Value="Stonehouse">Stonehouse</asp:ListItem>
                                <asp:ListItem Value="Stoneleigh">Stoneleigh</asp:ListItem>
                                <asp:ListItem Value="Stourbridge Junction">Stourbridge Junction</asp:ListItem>
                                <asp:ListItem Value="Stourbridge Town">Stourbridge Town</asp:ListItem>
                                <asp:ListItem Value="Stow">Stow</asp:ListItem>
                                <asp:ListItem Value="Stowmarket">Stowmarket</asp:ListItem>
                                <asp:ListItem Value="Stranraer">Stranraer</asp:ListItem>
                                <asp:ListItem Value="Stratford (London)">Stratford (London)</asp:ListItem>
                                <asp:ListItem Value="Stratford International">Stratford International</asp:ListItem>
                                <asp:ListItem Value="Stratford upon Avon">Stratford upon Avon</asp:ListItem>
                                <asp:ListItem Value="Stratford-upon-Avon Parkway">Stratford-upon-Avon Parkway</asp:ListItem>
                                <asp:ListItem Value="Strawberry Hill">Strawberry Hill</asp:ListItem>
                                <asp:ListItem Value="Streatham">Streatham</asp:ListItem>
                                <asp:ListItem Value="Streatham Common">Streatham Common</asp:ListItem>
                                <asp:ListItem Value="Streatham Hill">Streatham Hill</asp:ListItem>
                                <asp:ListItem Value="Strood (Kent)">Strood (Kent)</asp:ListItem>
                                <asp:ListItem Value="Stroud (Glos)">Stroud (Glos)</asp:ListItem>
                                <asp:ListItem Value="Sturry">Sturry</asp:ListItem>
                                <asp:ListItem Value="Sudbury (Suffolk)">Sudbury (Suffolk)</asp:ListItem>
                                <asp:ListItem Value="Sunbury">Sunbury</asp:ListItem>
                                <asp:ListItem Value="Sunderland">Sunderland</asp:ListItem>
                                <asp:ListItem Value="Sundridge Park">Sundridge Park</asp:ListItem>
                                <asp:ListItem Value="Sunningdale">Sunningdale</asp:ListItem>
                                <asp:ListItem Value="Sunnymeads">Sunnymeads</asp:ListItem>
                                <asp:ListItem Value="Surbiton">Surbiton</asp:ListItem>
                                <asp:ListItem Value="Surrey Quays">Surrey Quays</asp:ListItem>
                                <asp:ListItem Value="Sutton (London)">Sutton (London)</asp:ListItem>
                                <asp:ListItem Value="Sutton Coldfield">Sutton Coldfield</asp:ListItem>
                                <asp:ListItem Value="Sutton Common">Sutton Common</asp:ListItem>
                                <asp:ListItem Value="Swanley">Swanley</asp:ListItem>
                                <asp:ListItem Value="Swanscombe">Swanscombe</asp:ListItem>
                                <asp:ListItem Value="Swansea">Swansea</asp:ListItem>
                                <asp:ListItem Value="Swanwick">Swanwick</asp:ListItem>
                                <asp:ListItem Value="Sway">Sway</asp:ListItem>
                                <asp:ListItem Value="Swaythling">Swaythling</asp:ListItem>
                                <asp:ListItem Value="Swindon (Wilts)">Swindon (Wilts)</asp:ListItem>
                                <asp:ListItem Value="Sydenham (London)">Sydenham (London)</asp:ListItem>
                                <asp:ListItem Value="Sydenham Hill">Sydenham Hill</asp:ListItem>
                                <asp:ListItem Value="Syon Lane">Syon Lane</asp:ListItem>
                                <asp:ListItem Value="Syston">Syston</asp:ListItem>
                                <asp:ListItem Value="Tadworth">Tadworth</asp:ListItem>
                                <asp:ListItem Value="Taffs Well">Taffs Well</asp:ListItem>
                                <asp:ListItem Value="Tame Bridge Parkway">Tame Bridge Parkway</asp:ListItem>
                                <asp:ListItem Value="Tamworth">Tamworth</asp:ListItem>
                                <asp:ListItem Value="Taplow">Taplow</asp:ListItem>
                                <asp:ListItem Value="Tattenham Corner">Tattenham Corner</asp:ListItem>
                                <asp:ListItem Value="Taunton">Taunton</asp:ListItem>
                                <asp:ListItem Value="Teddington">Teddington</asp:ListItem>
                                <asp:ListItem Value="Teignmouth">Teignmouth</asp:ListItem>
                                <asp:ListItem Value="Telford">Telford</asp:ListItem>
                                <asp:ListItem Value="Templecombe">Templecombe</asp:ListItem>
                                <asp:ListItem Value="Tenby">Tenby</asp:ListItem>
                                <asp:ListItem Value="Teynham">Teynham</asp:ListItem>
                                <asp:ListItem Value="Thames Ditton">Thames Ditton</asp:ListItem>
                                <asp:ListItem Value="Thatcham">Thatcham</asp:ListItem>
                                <asp:ListItem Value="The Hawthorns">The Hawthorns</asp:ListItem>
                                <asp:ListItem Value="Theale">Theale</asp:ListItem>
                                <asp:ListItem Value="Theobalds Grove">Theobalds Grove</asp:ListItem>
                                <asp:ListItem Value="Thetford">Thetford</asp:ListItem>
                                <asp:ListItem Value="Thirsk">Thirsk</asp:ListItem>
                                <asp:ListItem Value="Thornaby">Thornaby</asp:ListItem>
                                <asp:ListItem Value="Thornliebank">Thornliebank</asp:ListItem>
                                <asp:ListItem Value="Thornton Heath">Thornton Heath</asp:ListItem>
                                <asp:ListItem Value="Thorpe Bay">Thorpe Bay</asp:ListItem>
                                <asp:ListItem Value="Thorpe-le-Soken">Thorpe-le-Soken</asp:ListItem>
                                <asp:ListItem Value="Three Bridges">Three Bridges</asp:ListItem>
                                <asp:ListItem Value="Thurso">Thurso</asp:ListItem>
                                <asp:ListItem Value="Tilbury Town">Tilbury Town</asp:ListItem>
                                <asp:ListItem Value="Tile Hill">Tile Hill</asp:ListItem>
                                <asp:ListItem Value="Tilehurst">Tilehurst</asp:ListItem>
                                <asp:ListItem Value="Tipton">Tipton</asp:ListItem>
                                <asp:ListItem Value="Tisbury">Tisbury</asp:ListItem>
                                <asp:ListItem Value="Tiverton Parkway">Tiverton Parkway</asp:ListItem>
                                <asp:ListItem Value="Todmorden">Todmorden</asp:ListItem>
                                <asp:ListItem Value="Tolworth">Tolworth</asp:ListItem>
                                <asp:ListItem Value="Tonbridge">Tonbridge</asp:ListItem>
                                <asp:ListItem Value="Tooting">Tooting</asp:ListItem>
                                <asp:ListItem Value="Torquay">Torquay</asp:ListItem>
                                <asp:ListItem Value="Totnes">Totnes</asp:ListItem>
                                <asp:ListItem Value="Tottenham Hale">Tottenham Hale</asp:ListItem>
                                <asp:ListItem Value="Totton">Totton</asp:ListItem>
                                <asp:ListItem Value="Trefforest">Trefforest</asp:ListItem>
                                <asp:ListItem Value="Trefforest Estate">Trefforest Estate</asp:ListItem>
                                <asp:ListItem Value="Tring">Tring</asp:ListItem>
                                <asp:ListItem Value="Troon">Troon</asp:ListItem>
                                <asp:ListItem Value="Trowbridge">Trowbridge</asp:ListItem>
                                <asp:ListItem Value="Truro">Truro</asp:ListItem>
                                <asp:ListItem Value="Tulse Hill">Tulse Hill</asp:ListItem>
                                <asp:ListItem Value="Tunbridge Wells">Tunbridge Wells</asp:ListItem>
                                <asp:ListItem Value="Turkey Street">Turkey Street</asp:ListItem>
                                <asp:ListItem Value="Tweedbank">Tweedbank</asp:ListItem>
                                <asp:ListItem Value="Twickenham">Twickenham</asp:ListItem>
                                <asp:ListItem Value="Twyford">Twyford</asp:ListItem>
                                <asp:ListItem Value="Ty Glas">Ty Glas</asp:ListItem>
                                <asp:ListItem Value="Uckfield">Uckfield</asp:ListItem>
                                <asp:ListItem Value="Uddingston">Uddingston</asp:ListItem>
                                <asp:ListItem Value="Ulverston">Ulverston</asp:ListItem>
                                <asp:ListItem Value="University (Birmingham)">University (Birmingham)</asp:ListItem>
                                <asp:ListItem Value="Uphall">Uphall</asp:ListItem>
                                <asp:ListItem Value="Upminster">Upminster</asp:ListItem>
                                <asp:ListItem Value="Upper Halliford">Upper Halliford</asp:ListItem>
                                <asp:ListItem Value="Upper Holloway">Upper Holloway</asp:ListItem>
                                <asp:ListItem Value="Upper Warlingham">Upper Warlingham</asp:ListItem>
                                <asp:ListItem Value="Upwey">Upwey</asp:ListItem>
                                <asp:ListItem Value="Vauxhall">Vauxhall</asp:ListItem>
                                <asp:ListItem Value="Virginia Water">Virginia Water</asp:ListItem>
                                <asp:ListItem Value="Waddon">Waddon</asp:ListItem>
                                <asp:ListItem Value="Wadhurst">Wadhurst</asp:ListItem>
                                <asp:ListItem Value="Wakefield Kirkgate">Wakefield Kirkgate</asp:ListItem>
                                <asp:ListItem Value="Wakefield Westgate">Wakefield Westgate</asp:ListItem>
                                <asp:ListItem Value="Walkden">Walkden</asp:ListItem>
                                <asp:ListItem Value="Wallington">Wallington</asp:ListItem>
                                <asp:ListItem Value="Wallyford">Wallyford</asp:ListItem>
                                <asp:ListItem Value="Walmer">Walmer</asp:ListItem>
                                <asp:ListItem Value="Walsall">Walsall</asp:ListItem>
                                <asp:ListItem Value="Waltham Cross">Waltham Cross</asp:ListItem>
                                <asp:ListItem Value="Walthamston Queens Road">Walthamston Queens Road</asp:ListItem>
                                <asp:ListItem Value="Walthamstow Central">Walthamstow Central</asp:ListItem>
                                <asp:ListItem Value="Walton on Thames">Walton on Thames</asp:ListItem>
                                <asp:ListItem Value="Walton-on-the-Naze">Walton-on-the-Naze</asp:ListItem>
                                <asp:ListItem Value="Wanborough">Wanborough</asp:ListItem>
                                <asp:ListItem Value="Wandsworth Common">Wandsworth Common</asp:ListItem>
                                <asp:ListItem Value="Wandsworth Road">Wandsworth Road</asp:ListItem>
                                <asp:ListItem Value="Wandsworth Town">Wandsworth Town</asp:ListItem>
                                <asp:ListItem Value="Wanstead Park">Wanstead Park</asp:ListItem>
                                <asp:ListItem Value="Wapping">Wapping</asp:ListItem>
                                <asp:ListItem Value="Warblington">Warblington</asp:ListItem>
                                <asp:ListItem Value="Ware (Herts)">Ware (Herts)</asp:ListItem>
                                <asp:ListItem Value="Wareham (Dorset)">Wareham (Dorset)</asp:ListItem>
                                <asp:ListItem Value="Warminster">Warminster</asp:ListItem>
                                <asp:ListItem Value="Warnham">Warnham</asp:ListItem>
                                <asp:ListItem Value="Warrington Bank Quay">Warrington Bank Quay</asp:ListItem>
                                <asp:ListItem Value="Warrington Central">Warrington Central</asp:ListItem>
                                <asp:ListItem Value="Warwick">Warwick</asp:ListItem>
                                <asp:ListItem Value="Warwick Parkway">Warwick Parkway</asp:ListItem>
                                <asp:ListItem Value="Waterbeach">Waterbeach</asp:ListItem>
                                <asp:ListItem Value="Watford High Street">Watford High Street</asp:ListItem>
                                <asp:ListItem Value="Watford Junction">Watford Junction</asp:ListItem>
                                <asp:ListItem Value="Watford North">Watford North</asp:ListItem>
                                <asp:ListItem Value="Watlington">Watlington</asp:ListItem>
                                <asp:ListItem Value="Watton-at-Stone">Watton-at-Stone</asp:ListItem>
                                <asp:ListItem Value="Waun-Gron Park">Waun-Gron Park</asp:ListItem>
                                <asp:ListItem Value="Welham Green">Welham Green</asp:ListItem>
                                <asp:ListItem Value="Welling">Welling</asp:ListItem>
                                <asp:ListItem Value="Wellingborough">Wellingborough</asp:ListItem>
                                <asp:ListItem Value="Wellington (Shropshire)">Wellington (Shropshire)</asp:ListItem>
                                <asp:ListItem Value="Welshpool">Welshpool</asp:ListItem>
                                <asp:ListItem Value="Welwyn Garden City">Welwyn Garden City</asp:ListItem>
                                <asp:ListItem Value="Welwyn North">Welwyn North</asp:ListItem>
                                <asp:ListItem Value="Wem">Wem</asp:ListItem>
                                <asp:ListItem Value="Wembley Central">Wembley Central</asp:ListItem>
                                <asp:ListItem Value="Wembley Stadium">Wembley Stadium</asp:ListItem>
                                <asp:ListItem Value="Wemyss Bay">Wemyss Bay</asp:ListItem>
                                <asp:ListItem Value="Wendover">Wendover</asp:ListItem>
                                <asp:ListItem Value="West Byfleet">West Byfleet</asp:ListItem>
                                <asp:ListItem Value="West Calder">West Calder</asp:ListItem>
                                <asp:ListItem Value="West Croydon">West Croydon</asp:ListItem>
                                <asp:ListItem Value="West Drayton">West Drayton</asp:ListItem>
                                <asp:ListItem Value="West Dulwich">West Dulwich</asp:ListItem>
                                <asp:ListItem Value="West Ealing">West Ealing</asp:ListItem>
                                <asp:ListItem Value="West Hampstead">West Hampstead</asp:ListItem>
                                <asp:ListItem Value="West Hampstead Thameslink">West Hampstead Thameslink</asp:ListItem>
                                <asp:ListItem Value="West Horndon">West Horndon</asp:ListItem>
                                <asp:ListItem Value="West Malling">West Malling</asp:ListItem>
                                <asp:ListItem Value="West Norwood">West Norwood</asp:ListItem>
                                <asp:ListItem Value="West Ruislip">West Ruislip</asp:ListItem>
                                <asp:ListItem Value="West St Leonards">West St Leonards</asp:ListItem>
                                <asp:ListItem Value="West Sutton">West Sutton</asp:ListItem>
                                <asp:ListItem Value="West Wickham">West Wickham</asp:ListItem>
                                <asp:ListItem Value="West Worthing">West Worthing</asp:ListItem>
                                <asp:ListItem Value="Westbury">Westbury</asp:ListItem>
                                <asp:ListItem Value="Westcliff">Westcliff</asp:ListItem>
                                <asp:ListItem Value="Westcombe Park">Westcombe Park</asp:ListItem>
                                <asp:ListItem Value="Westerton">Westerton</asp:ListItem>
                                <asp:ListItem Value="Westgate on Sea">Westgate on Sea</asp:ListItem>
                                <asp:ListItem Value="Westhoughton">Westhoughton</asp:ListItem>
                                <asp:ListItem Value="Weston Super Mare">Weston Super Mare</asp:ListItem>
                                <asp:ListItem Value="Weybridge">Weybridge</asp:ListItem>
                                <asp:ListItem Value="Weymouth">Weymouth</asp:ListItem>
                                <asp:ListItem Value="Whatstandwell">Whatstandwell</asp:ListItem>
                                <asp:ListItem Value="Whimple">Whimple</asp:ListItem>
                                <asp:ListItem Value="Whitchurch (Cardiff)">Whitchurch (Cardiff)</asp:ListItem>
                                <asp:ListItem Value="Whitchurch (Hants)">Whitchurch (Hants)</asp:ListItem>
                                <asp:ListItem Value="Whitchurch (Shropshire)">Whitchurch (Shropshire)</asp:ListItem>
                                <asp:ListItem Value="White Hart Lane">White Hart Lane</asp:ListItem>
                                <asp:ListItem Value="Whitecraigs">Whitecraigs</asp:ListItem>
                                <asp:ListItem Value="Whitehaven">Whitehaven</asp:ListItem>
                                <asp:ListItem Value="Whitlocks End">Whitlocks End</asp:ListItem>
                                <asp:ListItem Value="Whitstable">Whitstable</asp:ListItem>
                                <asp:ListItem Value="Whittlesford Parkway">Whittlesford Parkway</asp:ListItem>
                                <asp:ListItem Value="Whitton (London)">Whitton (London)</asp:ListItem>
                                <asp:ListItem Value="Whyteleafe">Whyteleafe</asp:ListItem>
                                <asp:ListItem Value="Whyteleafe South">Whyteleafe South</asp:ListItem>
                                <asp:ListItem Value="Wick">Wick</asp:ListItem>
                                <asp:ListItem Value="Wickford">Wickford</asp:ListItem>
                                <asp:ListItem Value="Widnes">Widnes</asp:ListItem>
                                <asp:ListItem Value="Widney Manor">Widney Manor</asp:ListItem>
                                <asp:ListItem Value="Wigan North Western">Wigan North Western</asp:ListItem>
                                <asp:ListItem Value="Wigan Wallgate">Wigan Wallgate</asp:ListItem>
                                <asp:ListItem Value="Willesden Junction">Willesden Junction</asp:ListItem>
                                <asp:ListItem Value="Williamwood">Williamwood</asp:ListItem>
                                <asp:ListItem Value="Wilmslow">Wilmslow</asp:ListItem>
                                <asp:ListItem Value="Wimbledon">Wimbledon</asp:ListItem>
                                <asp:ListItem Value="Wimbledon Chase">Wimbledon Chase</asp:ListItem>
                                <asp:ListItem Value="Winchester">Winchester</asp:ListItem>
                                <asp:ListItem Value="Winchfield">Winchfield</asp:ListItem>
                                <asp:ListItem Value="Winchmore Hill">Winchmore Hill</asp:ListItem>
                                <asp:ListItem Value="Windermere">Windermere</asp:ListItem>
                                <asp:ListItem Value="Windsor &amp; Eton Central">Windsor &amp; Eton Central</asp:ListItem>
                                <asp:ListItem Value="Windsor &amp; Eton Riverside">Windsor &amp; Eton Riverside</asp:ListItem>
                                <asp:ListItem Value="Winnersh">Winnersh</asp:ListItem>
                                <asp:ListItem Value="Winnersh Triangle">Winnersh Triangle</asp:ListItem>
                                <asp:ListItem Value="Winsford">Winsford</asp:ListItem>
                                <asp:ListItem Value="Wishaw">Wishaw</asp:ListItem>
                                <asp:ListItem Value="Witham">Witham</asp:ListItem>
                                <asp:ListItem Value="Witley">Witley</asp:ListItem>
                                <asp:ListItem Value="Wivelsfield">Wivelsfield</asp:ListItem>
                                <asp:ListItem Value="Wivenhoe">Wivenhoe</asp:ListItem>
                                <asp:ListItem Value="Woking">Woking</asp:ListItem>
                                <asp:ListItem Value="Wokingham">Wokingham</asp:ListItem>
                                <asp:ListItem Value="Woldingham">Woldingham</asp:ListItem>
                                <asp:ListItem Value="Wolverhampton">Wolverhampton</asp:ListItem>
                                <asp:ListItem Value="Wolverton">Wolverton</asp:ListItem>
                                <asp:ListItem Value="Wombwell">Wombwell</asp:ListItem>
                                <asp:ListItem Value="Wood Street">Wood Street</asp:ListItem>
                                <asp:ListItem Value="Woodbridge">Woodbridge</asp:ListItem>
                                <asp:ListItem Value="Woodgrange Park">Woodgrange Park</asp:ListItem>
                                <asp:ListItem Value="Woodhall">Woodhall</asp:ListItem>
                                <asp:ListItem Value="Woodlesford">Woodlesford</asp:ListItem>
                                <asp:ListItem Value="Woodmansterne">Woodmansterne</asp:ListItem>
                                <asp:ListItem Value="Wool">Wool</asp:ListItem>
                                <asp:ListItem Value="Woolston">Woolston</asp:ListItem>
                                <asp:ListItem Value="Woolwich Arsenal">Woolwich Arsenal</asp:ListItem>
                                <asp:ListItem Value="Woolwich Dockyard">Woolwich Dockyard</asp:ListItem>
                                <asp:ListItem Value="Worcester Foregate Street">Worcester Foregate Street</asp:ListItem>
                                <asp:ListItem Value="Worcester Park">Worcester Park</asp:ListItem>
                                <asp:ListItem Value="Worcester Shrub Hill">Worcester Shrub Hill</asp:ListItem>
                                <asp:ListItem Value="Worksop">Worksop</asp:ListItem>
                                <asp:ListItem Value="Worplesdon">Worplesdon</asp:ListItem>
                                <asp:ListItem Value="Worthing">Worthing</asp:ListItem>
                                <asp:ListItem Value="Wraysbury">Wraysbury</asp:ListItem>
                                <asp:ListItem Value="Wrexham Central">Wrexham Central</asp:ListItem>
                                <asp:ListItem Value="Wrexham General">Wrexham General</asp:ListItem>
                                <asp:ListItem Value="Wye">Wye</asp:ListItem>
                                <asp:ListItem Value="Wylde Green">Wylde Green</asp:ListItem>
                                <asp:ListItem Value="Wythall">Wythall</asp:ListItem>
                                <asp:ListItem Value="Yardley Wood">Yardley Wood</asp:ListItem>
                                <asp:ListItem Value="Yatton">Yatton</asp:ListItem>
                                <asp:ListItem Value="Yeovil Junction">Yeovil Junction</asp:ListItem>
                                <asp:ListItem Value="York">York</asp:ListItem>
                                <asp:ListItem Value="Ystrad Mynach">Ystrad Mynach</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqvalerrorBA10" runat="server" CssClass="starail-Form-required absolute"
                                Display="Static" Text="*" ControlToValidate="ddlStation" InitialValue="0" ErrorMessage="Please select TOD location."
                                ValidationGroup="vgs" />
                        </div>
                    </div>
                </div>
                <div class="starail-Form-row">
                    <asp:Label class="starail-Form-label" ID="lblTODAvailable" runat="server">
                        ToD Availability - Select TOD Location.
                    </asp:Label>
                </div>
                <div class="starail-Form-row">
                    <asp:Label class="starail-Form-label" ID="lblBookingOffice" runat="server">
                        No Booking Office details for this location are currently known.
                    </asp:Label>
                </div>
                <div class="starail-Form-row">
                    <asp:Label class="starail-Form-label" ID="lblTVM" runat="server">
                       No Ticket Vending Details at this location are currently known.
                    </asp:Label>
                </div>
            </div>
            <div class="starail-Section starail-Section--nopadding starail-Section-nextButtonSection textcenter">
                <asp:Button ID="btnSave" runat="server" Text="Next: Booking Details" CssClass="starail-Button"
                    OnClick="btnSave_Click" ValidationGroup="vgs" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="updReservation"
        DisplayAfter="0" DynamicLayout="True">
        <ProgressTemplate>
            <div class="modalBackground progessposition">
            </div>
            <div class="progess-inner2">
                UPDATING RESULTS...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
