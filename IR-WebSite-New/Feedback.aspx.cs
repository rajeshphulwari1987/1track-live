﻿#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Linq;
using Business;
using System.Web.UI.WebControls;
using System.Configuration;
#endregion

public partial class Feedback : Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string siteURL;
    readonly Masters _master = new Masters();
    private Guid _siteId;
    public string script;
    Guid pageID;
    readonly Masters _masterPage = new Masters();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(_siteId);
            BindTopic();
            txtPhn.Attributes.Add("onkeypress", "return keycheck()");
            var pid = _db.tblWebMenus.FirstOrDefault(x => x.Name.ToLower() == "home");
            if (pid != null)
                pageID = pid.ID;
            PageContent(pageID, _siteId);

            #region Seobreadcrumbs
            var pageIDs = (Guid)Page.RouteData.Values["PageId"];
            var dataSeobreadcrumbs = new ManageSeo().Getlinkbody(pageIDs, _siteId);
            if (dataSeobreadcrumbs != null && dataSeobreadcrumbs.IsActive)
                litSeobreadcrumbs.Text = dataSeobreadcrumbs.SourceCode;
            #endregion
        }
    }

    public void PageContent(Guid pageID, Guid siteID)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageID && x.SiteID == siteID);
            if (result != null)
            {
                var url = result.Url;
                var oPage = _masterPage.GetPageDetailsByUrl(url);

                //Banner
                string[] arrListId = oPage.BannerIDs.Split(',');
                var idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _masterPage.GetBannerImgByID(idList);
                int countdata = list.Count();
                int index = new Random().Next(countdata);
                var newlist = list.Skip(index).FirstOrDefault();
                if (newlist != null && newlist.ImgUrl != null)
                    img_Banner.Src = adminSiteUrl + "" + newlist.ImgUrl;
                else
                    img_Banner.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            _master.AddFeedback(new tblFeedback
            {
                ID = Guid.NewGuid(),
                SiteID = _siteId,
                Name = txtName.Text.Trim(),
                Email = txtEmail.Text.Trim(),
                Phone = txtPhn.Text.Trim(),
                TopicID = Guid.Parse(ddlTopic.SelectedValue),
                Description = txtDesp.Text.Trim()
            });
            SendMail();
            ClearData();
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    public void SendMail()
    {
        var site = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
        string siteNm = string.Empty;
        if (site != null)
            siteNm = site.DisplayName;

        string subject = "Feedback: " + ddlTopic.SelectedItem.Text;
        string body = "<html><head><title></title></head><body>" +
                      "<p> " +
                      "From: " + txtName.Text + " <br />" +
                      "Email: " + txtEmail.Text + " <br />" +
                      "Phone: " + txtPhn.Text + " <br />" +
                      "Site Name: " + siteNm + " <br />" +
                      "Topic: " + ddlTopic.SelectedItem.Text + " <br /><br />" +
                      txtDesp.Text + " <br /><br /> " +
                      "Thanks &amp; Regards<br />" + txtName.Text +
                      "<br /></p></body></html>";

        string frmAddress = txtEmail.Text;
        string toAddress = System.Configuration.ConfigurationSettings.AppSettings["ClientMailAddress"];

        var sendEmail = _master.SendFeebackMail(subject, body, frmAddress, toAddress, _siteId);
        if (sendEmail)
            ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Your feedback has been sent successfully.')", true);
    }

    public void BindTopic()
    {
        ddlTopic.DataSource = _master.GetTopicList();
        ddlTopic.DataTextField = "Name";
        ddlTopic.DataValueField = "ID";
        ddlTopic.DataBind();
        ddlTopic.Items.Insert(0, new ListItem("------Select Topic------", "-1"));
    }

    private void ClearData()
    {
        txtName.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtPhn.Text = string.Empty;
        ddlTopic.SelectedValue = "-1";
        txtDesp.Text = string.Empty;
    }
}