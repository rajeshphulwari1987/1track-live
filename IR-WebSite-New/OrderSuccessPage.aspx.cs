﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Xml;
using Business;
using iTextSharp.text;
using iTextSharp.text.pdf;
using OneHubServiceRef;
using System.Configuration;
using System.Web.UI.WebControls;

public partial class OrderSuccessPage : Page
{
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly ManageBooking oBooking = new ManageBooking();
    readonly private ManageOrder _master = new ManageOrder();
    public string currency = "$";
    public string ReservationCode = null;
    public string currencyCode = "USD";
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string siteURL;
    public static Guid currencyID = new Guid();
    private Guid siteId;
    public string script = "<script></script>";
    public string products = "";
    public string WTpnsku = "";
    public string WTtxu = "";
    public string WTtxs = "";
    public string WTtxI = "";
    public string WTtxid = "";
    public string WTtxit = "";
    public string printUrl = "";
    public string pdfName = "";
    public string FileURL = "";
    public bool IsFirst = true;
    public string OrderNo = string.Empty;
    private string htmfile = string.Empty;
    public string SiteAlias = string.Empty;
    public string iframe = string.Empty;
    ManageUser mngUser = new ManageUser();
    private ManagePrintQueue _oPrint = new ManagePrintQueue();
    Guid pageID;
    public bool isEvolviBooking = false;
    public bool isNTVBooking = false;
    public bool isBene = false;
    public bool isTI = false;
    BookingRequestUserControl objBRUC;
    ManageOneHub _ManageOneHub = new ManageOneHub();
    public string EvOtherCharges = "0";

    public List<OrderReceipt> OrderReceiptList = new List<OrderReceipt>();
    public string RUserName = "";
    public string Name = "";
    public string logo = "";
    public string img1 = "";
    public string img2 = "";
    public string img3 = "";
    public string EvolviTandC = "";
    private string HtmlFileSitelog = string.Empty;
    public string SiteHeaderColor = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (Request.QueryString["name"] != null && Request.QueryString["name"] == "rajesh")
            //Session["OrderID"] = 126688;
            var pid = _db.tblWebMenus.FirstOrDefault(x => x.Name.ToLower() == "home");
            if (pid != null)
                pageID = pid.ID;
            PageContent(pageID, siteId);
            Session["IsCheckout"] = null;

            #region Is order paid ?
            if (Session["IsPaymentSuccess"] == null)
                if (Request.Params["req"] != null)
                    Response.Redirect("PaymentProcess?req=" + Request.Params["req"]);
                else
                    Response.Redirect("PaymentProcess");
            else
                Session.Remove("IsPaymentSuccess");
            #endregion
        }

        Session.Remove("OgoneResponse");
        if (Request.Params["req"] != null)
        {
            Session["GTMOrder"] = OrderNo = Request.Params["req"];
        }
        else if (Session["OrderID"] != null)
        {
            Session["GTMOrder"] = OrderNo = Session["OrderID"].ToString();
        }

        var apiTypeList = new ManageBooking().GetAllApiType(Convert.ToInt32(OrderNo));
        if (apiTypeList != null)
        {
            isEvolviBooking = apiTypeList.isEvolvi;
            isNTVBooking = apiTypeList.isNTV;
            isBene = apiTypeList.isBene;
            isTI = apiTypeList.isTI;
        }
        if (isEvolviBooking)
            EvolviTandC = siteURL + "uk-ticket-collection";

        #region If user changed query sitring data then redirct at home
        if (!IsPostBack)
        {
            if (Request.Params["req"] != null)
                ViewState["tempOrder"] = Session["P2POrderID"].ToString();
            else if (Session["OrderID"] != null)
                ViewState["tempOrder"] = Session["OrderID"].ToString();
        }
        if (ViewState["tempOrder"] != null && Request.Params["req"] != null && ViewState["tempOrder"].ToString() != Request.Params["req"])
            Response.Redirect("~/");
        #endregion

        if (!IsPostBack)
        {
            GetCurrencyCode();
            if (Request.Params["req"] != null)
            {
                if (isEvolviBooking)
                {
                    GetEvolviBookingConfirmation();
                    GetEvolviTicketInfo();
                    UpdateP2PStatus();
                }
                PrintTicketInfo();
                var getDeliveryType = oBooking.getDeliveryOption(Convert.ToInt64(Session["P2POrderID"]));
                if (getDeliveryType != null && (getDeliveryType == "Thayls Ticketless" || getDeliveryType == "TrenItaila Printing"))
                    ConfirmP2PBooking();
            }
            else
            {
                Session.Remove("CollectStation");
                SendMail();
            }
            ShowPaymentDetails();

            if (Session["TicketBooking-REPLY"] != null && Request.Params["req"] != null)
            {
                List<PrintResponse> listTicket = Session["TicketBooking-REPLY"] as List<PrintResponse>;
                btnpdf.Visible = listTicket.Any(x => !string.IsNullOrEmpty(x.URL));
            }

            #region Affiliate Tracking
            var affliatetrackResult = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
            if (affliatetrackResult.EnableAffiliateTracking)
            {
                Decimal affNetTotal = (Convert.ToDecimal(lblGrandTotal.Text) + Convert.ToDecimal(lblDiscount.Text));
                string DiscountCode = oBooking.GetDiscountCodeByOrderId(Convert.ToInt32(OrderNo));
                string srcpath = "https://www.emjcd.com/tags/c?containerTagId=7435&AMOUNT=" + affNetTotal + "&CID=1502281&OID=" + lblOrderNumber.Text + "&TYPE=373607&CURRENCY=" + currencyCode + (string.IsNullOrEmpty(DiscountCode) ? string.Empty : "&COUPON=" + DiscountCode) + (Convert.ToDecimal(lblDiscount.Text) < 1 ? string.Empty : "&DISCOUNT=" + lblDiscount.Text);
                iframe = "<iframe  height='1' width='1' frameborder='0' scrolling='no' name='cj_conversion' src='" + srcpath + "'></iframe>";
            }
            #endregion

            #region Default printer
            int OrderId = Convert.ToInt32(OrderNo);
            Guid AdminId = Guid.Parse("9899686a-6ec4-4b32-a813-d9287d7e03e1");
            var data = _oPrint.SiteGetById(siteId);
            var list = _oPrint.GetTPSQueueData(OrderId);
            if (data.EurailPrinter.HasValue || data.BritrailPrinter.HasValue || data.InterrailPrinter.HasValue)
            {
                foreach (var item in list)
                {
                    Guid QueueID = Guid.Empty;
                    if (item.tblCategory.IsPrintQueueEnable && data.EurailPrinter.HasValue)
                    {
                        QueueID = data.EurailPrinter.Value;
                    }
                    else if (item.tblCategory.IsBritRailPass && data.BritrailPrinter.HasValue)
                    {
                        QueueID = data.BritrailPrinter.Value;
                    }
                    else if (item.tblCategory.IsInterRailPass && data.InterrailPrinter.HasValue)
                    {
                        QueueID = data.InterrailPrinter.Value;
                    }

                    if (QueueID != Guid.Empty)
                    {
                        _oPrint.AddUpdatePrintQ(OrderId, item.ID, siteId);
                        _oPrint.AddPrintQItems(new tblPrintQueueItem
                        {
                            ID = Guid.NewGuid(),
                            SiteID = siteId,
                            OrderID = OrderId,
                            CategoryID = item.CategoryID,
                            ProductID = item.ProductID,
                            PassSaleID = item.ID,
                            Status = Guid.Parse("6100b83e-07ed-418b-97e6-e5c6b746da3b"),
                            AdminUserID = AdminId,
                            DateTimeStamp = DateTime.Now,
                            QueueID = QueueID
                        });
                    }
                }
            }
            #endregion

        }
        GetSiteAlias();
        GetDetailsAnalyticTags();
        QubitOperationLoad();
        GetPrintReceipt();

        if (Session["RailPassData"] != null)
            Session["NewRailPassData"] = Session["RailPassData"] as List<getRailPassData>;

        Session.Remove("P2POrderID");
        Session.Remove("IsRegional");
        Session.Remove("SegmentType");
        Session.Remove("holdTodStationList");
        Session.Remove("HoldJourneyDetailsList");
        Session.Remove("HoldOldJourneyResponse");
        Session.Remove("HoldReserVationRequest");
        Session.Remove("OutTrainTimeDateEvolviList");
        Session.Remove("InTrainTimeDateEvolviList");
        Session.Remove("OrderID");
        Session.Remove("TempRailPassData");
        Session.Remove("RailPassData");
    }

    public void GetSiteAlias()
    {
        SiteAlias = oBooking.GetSiteAliasByID(siteId);
    }

    public void GetDetailsAnalyticTags()
    {
        if (!string.IsNullOrEmpty(lblOrderNumber.Text))
        {
            long orderid = Convert.ToInt64(lblOrderNumber.Text);
            ManageAnalyticTag _masterAnalyticTag = new ManageAnalyticTag();
            var data = _masterAnalyticTag.GetTagBySiteId(siteId);
            if (data != null)
            {
                if (Session["GaTaggingProductlist"] != null)
                {
                    var list = Session["GaTaggingProductlist"] as List<ProductPass>;
                    string[] SplitData = data.PurchaseActions.Split(';');
                    Guid IR_ComSiteId = Guid.Parse("5858F611-6726-4227-A417-52CCDEA36B3D");
                    #region New Code Implementation
                    /*PurchaseTransactionTags*/
                    decimal Shipping = _db.tblOrders.FirstOrDefault(x => x.OrderID == orderid).ShippingAmount ?? 0;
                    decimal totalprice = list.AsEnumerable().Sum(x => Convert.ToDecimal(x.Price) * x.Count);

                    string newVar1 = string.Empty;
                    string newVar2 = string.Empty;
                    newVar1 = SplitData[0] + ";";
                    if (SplitData.Count() > 1)
                    {
                        newVar1 += SplitData[1].Replace("addTransaction-id", orderid.ToString());
                        newVar1 = newVar1.Replace("addTransaction-revenue", (totalprice + Shipping).ToString());
                        newVar1 = newVar1.Replace("addTransaction-shipping", Shipping.ToString());
                        newVar1 = newVar1.Replace("addTransaction-affiliation", "");
                        newVar1 = newVar1.Replace("addTransaction-tax", "") + ";";
                    }
                    if (SplitData.Count() > 2)
                        foreach (var item in list)
                        {
                            newVar2 += SplitData[2].Replace("addItem-id", "" + OrderNo);
                            newVar2 = newVar2.Replace("addItem-name", "" + item.Name.Replace("'", "’"));
                            newVar2 = newVar2.Replace("addItem-sku", "" + item.ProductID);
                            newVar2 = newVar2.Replace("addItem-category", "" + item.CategoryName.Replace("'", "’"));
                            newVar2 = newVar2.Replace("addItem-price", "" + item.Price + "");
                            newVar2 = newVar2.Replace("addItem-quantity", "" + item.Count + "");
                            newVar1 += newVar2 + ";";
                            newVar2 = "";
                        }
                    if (SplitData.Count() > 3)
                        newVar1 += SplitData[3] + ";";
                    Session["PurchaseTransactionTags"] = "\n" + newVar1 + "\n";
                    #endregion
                }
            }
        }
    }

    #region Print & confirm P2P ticket

    private void PrintTicketInfo()
    {
        try
        {
            if (Session["BOOKING-REPLY"] == null)
                return;

            var list = Session["BOOKING-REPLY"] as List<BookingResponse>;
            if (list.Any(x => x.PinCode != null))
            {
                if (Request.Params["req"] != null)
                {
                    var chkDeliveryType = oBooking.CheckDilveryOptionType(Convert.ToInt64(Session["P2POrderID"]));
                    if (chkDeliveryType)
                        Printickets();
                    else
                    {
                        TicketBooking();
                        SendMail();
                    }
                }
            }
            else
                PrintTicketItalia();
        }
        catch
        {

        }
    }

    public void Printickets()
    {
        if (Session["BOOKING-REPLY"] != null)
        {
            var bookingList = Session["BOOKING-REPLY"] as List<BookingResponse>;
            if (bookingList == null)
                return;
            var client = new OneHubRailOneHubClient();
            var listResponse = new List<PrintResponse>();
            foreach (var item in bookingList)
            {
                #region Ticket Print
                var objHeader = new OneHubServiceRef.Header
                {
                    onehubusername = "#@dots!squares",
                    onehubpassword = "#@dots!squares",
                    unitofwork = 0,
                    language = Language.nl_BE
                };

                #region TicketBookingRequest
                var objRequest = new TicketBookingRequest { Header = objHeader, ReservationCode = item.ReservationCode };
                _ManageOneHub.TiBeneApiLogin(objRequest, siteId);
                var tktBooking = client.TicketBookingRequest(objRequest);
                oBooking.UpdatePinNumberByReservationCode(item.ReservationCode, tktBooking != null ? (!string.IsNullOrEmpty(tktBooking.PinCode) ? tktBooking.PinCode : item.PinCode) : item.PinCode);
                #endregion

                var printRequest = new TicketPrintRequest
                {
                    Header = objHeader,
                    BillingAddress = item.BillingAddress != null ? new BillingAddress
                    {
                        Address = item.BillingAddress.Address,
                        City = item.BillingAddress.City,
                        Country = item.BillingAddress.Country,
                        CrisNumber = item.BillingAddress.CrisNumber,
                        DeliveryDate = item.BillingAddress.DeliveryDate,
                        Email = item.BillingAddress.Email,
                        FirstName = item.BillingAddress.FirstName,
                        Lastname = item.BillingAddress.Lastname,
                        Phone = item.BillingAddress.Phone,
                        State = item.BillingAddress.State,
                        ZipCode = item.BillingAddress.ZipCode
                    } : null,
                    ReservationCode = item.ReservationCode,
                    ChangeReservationCode = item.ChangeReservationCode,
                    PinCode = tktBooking != null ? (!string.IsNullOrEmpty(tktBooking.PinCode) ? tktBooking.PinCode : item.PinCode) : item.PinCode,
                    TicketType = "PDF_DT",
                    ApiInformation = new ApiInformation
                    {
                        ApiName = GetApiName()
                    }
                };

                _ManageOneHub.TiBeneApiLogin(printRequest, siteId);
                var printResponse = client.TicketPrint(printRequest);
                if (printResponse != null)
                {
                    listResponse.Add(new PrintResponse
                    {
                        ReservationCode = printRequest.ReservationCode,
                        status = String.IsNullOrEmpty(printResponse.PrintIndication) ? "False" : printResponse.PrintIndication,
                        URL = String.IsNullOrEmpty(printResponse.TicketUrl) ? "Ticket print URL not found." : printResponse.TicketUrl
                    });
                }
                #endregion
            }

            var newList = new List<PrintResponse>();
            foreach (var resp in listResponse)
            {
                if (resp != null)
                {
                    printUrl = resp.URL;
                    if (resp.URL.Contains("pdf"))
                        printUrl = BeNePdfService(printUrl, resp.ReservationCode);

                    if (!string.IsNullOrEmpty(printUrl))
                        oBooking.UpdateTicketUrlByReservationCode(resp.ReservationCode, printUrl);

                    newList.Add(new PrintResponse { ReservationCode = resp.ReservationCode, status = resp.status, URL = printUrl });
                }
            }
            Session["TicketBooking-REPLY"] = newList;

            if (listResponse.Count != 0)
                SendMail();
            else
                ShowMessage(2, "Ticket Print not Available");
        }
    }

    public string BeNePdfService(string url, string resCode)
    {
        try
        {
            resCode = resCode + Guid.NewGuid().ToString();
            string outputFile = Server.MapPath("~/PdfService/" + resCode + ".pdf");
            var webClient = new WebClient();
            using (var webStream = webClient.OpenRead(url))
            using (var fileStream = new FileStream(outputFile, FileMode.Create))
            {
                var buffer = new byte[32768];
                int bytesRead;

                while (webStream != null && (bytesRead = webStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    fileStream.Write(buffer, 0, bytesRead);
                }
            }
            string siteURL = _oWebsitePage.GetSiteURLbySiteId(siteId);
            return siteURL + "PdfService/" + resCode + ".pdf";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void PrintTicketItalia()
    {
        try
        {
            var newList = new List<PrintResponse>();
            if (Session["BOOKING-REPLY"] != null)
            {
                var list = Session["BOOKING-REPLY"] as List<BookingResponse>;
                foreach (var item in list)
                    if (item.PdfUrl != null)
                    {
                        string newUrl = "";
                        foreach (var url in item.PdfUrl)
                        {
                            newUrl += url + "#";
                        }
                        oBooking.UpdateTicketUrlByReservationCode(item.ReservationCode, newUrl);
                        newList.Add(new PrintResponse { ReservationCode = item.ReservationCode, status = "PRINT REQUEST SUBMITTED", URL = newUrl });
                    }
            }
            Session["TicketBooking-REPLY"] = newList;
            SendMail();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public bool ConfirmP2PBooking()
    {
        bool result = false;
        if (Session["BOOKING-REPLY"] == null)
            return result;

        var client = new OneHubRailOneHubClient();
        var listbookingReply = Session["BOOKING-REPLY"] as List<BookingResponse>;

        foreach (var item in listbookingReply)
        {
            var objResponse = new ConfirmResponse();
            var objHeader = new OneHubServiceRef.Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = item.UnitOfWork,
                language = Language.nl_BE
            };

            var objRequest = new ConfirmRequest { Header = objHeader, ReservationCode = item.ReservationCode, ApiInformation = new ApiInformation { ApiName = GetApiName() } };
            _ManageOneHub.TiBeneApiLogin(objRequest, siteId);
            objResponse = client.Confirm(objRequest);

            if (objResponse != null)
            {
                string res = objResponse.ConfirmOk;
                result = res == "Ok" || res == "True";
            }

            oBooking.UpdateReservationCode(Convert.ToInt32(Session["P2POrderID"]), item.ReservationCode, result);
            oBooking.UpdatePinNumberByReservationCode(item.ReservationCode, string.IsNullOrEmpty(objResponse.PinCode) ? item.UnitOfWork.ToString() : objResponse.PinCode);
        }
        return result;
    }

    public void TicketBooking()
    {

        try
        {
            var listbookingReply = Session["BOOKING-REPLY"] as List<BookingResponse>;
            var item = listbookingReply.FirstOrDefault();
            var objHeader = new OneHubServiceRef.Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = 0,
                language = Language.nl_BE
            };

            var client = new OneHubRailOneHubClient();
            var objRequest = new TicketBookingRequest { Header = objHeader, ReservationCode = item.ReservationCode };
            _ManageOneHub.TiBeneApiLogin(objRequest, siteId);
            var tktBooking = client.TicketBookingRequest(objRequest);
            oBooking.UpdatePinNumberByReservationCode(item.ReservationCode, tktBooking != null ? tktBooking.PinCode : item.PinCode);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    public void QubitOperationLoad()
    {
        try
        {
            Guid siteId = Guid.Parse(Session["siteId"].ToString());
            var lstQbit = new Masters().GetQubitScriptList(siteId);
            var res = lstQbit.FirstOrDefault();
            if (res != null)
                script = res.Script;

            var lst = new ManageBooking().GetAllCartData(Convert.ToInt64(OrderNo)).ToList();
            if (lst.Count() > 0)
            {
                var lstNew = from a in lst
                             select
                                 new
                                 {
                                     category =
                             (new ManageBooking().getPassDescForQbit(a.PassSaleID, a.ProductType, "Category")),
                                     currency = currencyCode,
                                     name = (new ManageBooking().getPassDescForQbit(a.PassSaleID, a.ProductType, "")),
                                     sku_code = a.ProductType.ToLower().Trim() != "P2P" ? "Rail Pass" : "Rail Ticket",
                                     unit_price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)),
                                     unit_sale_price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType))
                                 };

                var listProduct = new List<ProuductLineItem>();
                string skuCode = "";
                string[] WTtxu1 = new string[lst.Count()];
                string[] WTtxs1 = new string[lst.Count()];
                if (lstNew.Count() > 0)
                {
                    int i = 0;
                    foreach (var litem in lstNew)
                    {
                        skuCode = litem.sku_code;
                        var obj = new ProuductLineItem
                        {
                            product = new ProductItem
                            {
                                category = litem.category,
                                currency = litem.currency,
                                name = litem.name,
                                sku_code = litem.sku_code,
                                unit_price = litem.unit_price.ToString(),
                                unit_sale_price = litem.unit_sale_price.ToString(),
                            },
                            quantity = 1,
                            subtotal = litem.unit_sale_price
                        };
                        listProduct.Add(obj);
                        WTtxu1[i] = "1";
                        WTtxs1[i] = litem.unit_sale_price.ToString();
                        i++;
                    }
                }

                var uPriceDistinct = WTtxs1.Distinct().ToArray();
                string[] uPriceDistictCount = new string[uPriceDistinct.Count()];
                int c = 0;
                foreach (var s in uPriceDistinct)
                {
                    int cin = 0;
                    foreach (string a in WTtxs1)
                    {
                        if (a == s.Trim())
                        {
                            cin++;
                        }
                    }
                    uPriceDistictCount[c] = cin.ToString();
                    c++;
                }

                WTpnsku = String.Format("<meta name=\"WT.pn_sku\" content=\"{0}\" />",
                                        skuCode == "Rail Pass" ? "RailPass" : "RailTicket");

                //<meta name="WT.tx_u" content="1;1" />                                           
                // units, if an order contains multiple products, separate the numbers of units for each product using a semi-colon
                WTtxu = String.Format("<meta name=\"WT.tx_u\" content=\"{0}\" />",
                                      String.Join(";", uPriceDistictCount.ToArray()));

                //<meta name="WT.tx_s" content="151.13;24" /> 
                // Identifies the total cost for each product in the order, separate cost for each product using a semi-colon
                WTtxs = String.Format("<meta name=\"WT.tx_s\" content=\"{0}\" />",
                                      String.Join(";", uPriceDistinct.ToArray()));

                //<meta name="WT.tx_I" content="949" />
                // invoice number
                WTtxI = String.Format("<meta name=\"WT.tx_I\" content=\"{0}\" />", OrderNo);

                //<meta name="WT.tx_id" content="01/15/14" />
                // Date of Order (MM/DD/YYYY)  
                WTtxid = String.Format("<meta name=\"WT.tx_id\" content=\"{0}\" />",
                                       lst.FirstOrDefault().CreatedOn.ToString("MM/dd/yyyy"));

                //<meta name="WT.tx_it" content="19:07:04" /> 
                // Time of Order (HH:MM:SS)
                WTtxit = String.Format("<meta name=\"WT.tx_it\" content=\"{0}\" />",
                                       lst.FirstOrDefault().CreatedOn.ToString("HH:mm:ss"));
                Guid IR_ComSiteId = Guid.Parse("5858F611-6726-4227-A417-52CCDEA36B3D");
                if (siteId != IR_ComSiteId)
                {
                    decimal shipingAMt = lst.FirstOrDefault().ShippingAmount.HasValue ? Convert.ToDecimal(lst.FirstOrDefault().ShippingAmount) : 0;
                    var cart = new BookingCartInfo
                    {
                        currency = currencyCode,
                        Date_of_order = lst.FirstOrDefault().CreatedOn.ToString("MM/dd/yyyy"),
                        line_items = listProduct.ToList(),
                        order_id = OrderNo,
                        shipping_cost = shipingAMt,
                        shipping_method = Session["ShipMethod"] != null ? Session["ShipMethod"].ToString() : "",
                        subtotal = listProduct.Sum(x => x.subtotal),
                        tax = 0,
                        Time_of_order = lst.FirstOrDefault().CreatedOn.ToString("HH:mm:ss"),
                        total = shipingAMt + listProduct.Sum(x => x.subtotal),
                        voucher = "",
                        voucher_discount = 0
                    };

                    var json_serializer = new JavaScriptSerializer();
                    products = " <script type='text/javascript'>\nwindow.universal_variable = {version: '1.1.0'};\n window.universal_variable.transaction = " + json_serializer.Serialize(cart) + ";\n</script>";
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    public void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
            currency = oManageClass.GetCurrency(currencyID);
            currencyCode = oManageClass.GetCurrencyShortCode(currencyID);
        }
    }

    private string GetSegmentNote()
    {
        List<PrintResponse> listTicket = Session["TicketBooking-REPLY"] as List<PrintResponse>;
        if (listTicket.Any(x => string.IsNullOrEmpty(x.URL)))
        {
            return "<tr><td style='font-size: 12px; border: 1px solid red;padding: 5px' valign='top' colspan='2'>" +
                            "<font face='Arial, Helvetica, sans-serif' color='red'>" + "<b>IMPORTANT:</b> This PNR is issued as a ticket on depart and must be printed from one of the self-service " +
                                "ticket machines located at all main Trenitalia stations (which are green, white and red). Once you have printed your ticket you must validate it in one of the validation " +
                                "machines located near to the entrance of each platform (they are green and white with Trenitalia written on the front and usually mounted on a wall), THIS MUST BE DONE BEFORE BOARDING YOUR TRAIN, failure to do so will result in a fine." +
                                "</font></td></tr>";
        }
        return "";
    }

    bool isBritRailPromoPass(long orderid)
    {
        try
        {
            foreach (var result in _db.tblPassSales.Where(x => x.OrderID == orderid).ToList())
            {
                bool isPromo = (_db.tblProducts.Any(x => x.ID == result.ProductID && x.IsPromoPass && x.tblProductCategoriesLookUps.Any(y => y.ProductID == result.ProductID && y.tblCategory.IsBritRailPass)));
                if (isPromo)
                    return true;
            }
            return false;
        }
        catch
        {
            return false;
        }

    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        Document document = new Document(PageSize.A4, 20f, 20f, 20f, 20f);
        try
        {
            string filename = Server.MapPath("~/PdfReceipt/" + lblOrderNumber.Text.Trim() + ".pdf");
            float[] widthsTwo = new float[] { 3.4f, 6f, 6f, 1.6f, 3f, 5f, 3f };

            float[] widthsTwoTwo = new float[] { 5f, 6f, 5f, 2.7f, 6f, 3f };
            float[] widths = new float[] { 4f, 4f, 4f, 4f, };
            iTextSharp.text.Font TitleFont = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font HeaderFont = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font TextFont = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            PdfWriter.GetInstance(document, new FileStream(filename, FileMode.Create));

            #region PdfTable object create
            PdfPTable tableHeader = new PdfPTable(1);
            tableHeader.TotalWidth = 550f;
            tableHeader.LockedWidth = true;
            tableHeader.HorizontalAlignment = 0;

            PdfPTable tableOne = new PdfPTable(4);
            tableOne.DefaultCell.Border = 0;
            tableOne.TotalWidth = 550f;
            tableOne.LockedWidth = true;
            tableOne.SetWidths(widths);
            tableOne.HorizontalAlignment = 0;
            tableOne.SpacingBefore = 10f;
            tableOne.SpacingAfter = 10f;

            PdfPTable tableTwo = new PdfPTable(6);
            tableTwo.DefaultCell.PaddingTop = 5;
            tableTwo.DefaultCell.PaddingBottom = 5;
            tableTwo.TotalWidth = 550f;
            tableTwo.LockedWidth = true;
            tableTwo.SetWidths(widthsTwoTwo);
            tableTwo.HorizontalAlignment = 0;
            tableTwo.SpacingBefore = 10f;
            tableTwo.SpacingAfter = 10f;

            PdfPTable tableTwoTwo = new PdfPTable(6);
            tableTwoTwo.DefaultCell.PaddingTop = 5;
            tableTwoTwo.DefaultCell.PaddingBottom = 5;
            tableTwoTwo.TotalWidth = 550f;
            tableTwoTwo.LockedWidth = true;
            tableTwoTwo.SetWidths(widthsTwoTwo);
            tableTwoTwo.HorizontalAlignment = 0;
            tableTwoTwo.SpacingBefore = 10f;
            tableTwoTwo.SpacingAfter = 10f;

            PdfPTable tableThree = new PdfPTable(4);
            tableThree.TotalWidth = 550f;
            tableThree.LockedWidth = true;
            tableThree.SetWidths(widths);
            tableThree.HorizontalAlignment = 0;
            tableThree.SpacingBefore = 10f;
            tableThree.SpacingAfter = 10f;
            #endregion

            #region First Row For Header
            PdfPCell Header = new PdfPCell(new Phrase("Rail Ticket\n\n", TitleFont));
            Header.Border = iTextSharp.text.Rectangle.NO_BORDER;
            Header.HorizontalAlignment = Element.ALIGN_CENTER;
            tableHeader.AddCell(Header);
            #endregion

            #region Second Row For Order Detail
            PdfPCell OrderNumber = new PdfPCell(new Phrase("Order Number", HeaderFont));
            OrderNumber.PaddingTop = 5;
            OrderNumber.PaddingBottom = 5;
            tableOne.AddCell(OrderNumber);
            PdfPCell TxtOrderNumber = new PdfPCell(new Phrase(lblOrderNumber.Text, TextFont));
            TxtOrderNumber.PaddingTop = 5;
            TxtOrderNumber.PaddingBottom = 5;
            tableOne.AddCell(TxtOrderNumber);

            PdfPCell OrderDate = new PdfPCell(new Phrase("Order Date", HeaderFont));
            OrderDate.PaddingTop = 5;
            OrderDate.PaddingBottom = 5;
            tableOne.AddCell(OrderDate);
            PdfPCell TxtOrderDate = new PdfPCell(new Phrase(lblOrderDate.Text, TextFont));
            TxtOrderDate.PaddingTop = 5;
            TxtOrderDate.PaddingBottom = 5;
            tableOne.AddCell(TxtOrderDate);

            PdfPCell Email = new PdfPCell(new Phrase("Email Address", HeaderFont));
            Email.PaddingTop = 5;
            Email.PaddingBottom = 5;
            tableOne.AddCell(Email);
            PdfPCell TxtEmail = new PdfPCell(new Phrase(lblEmailAddress.Text, TextFont));
            TxtEmail.PaddingTop = 5;
            TxtEmail.PaddingBottom = 5;
            tableOne.AddCell(TxtEmail);

            PdfPCell DeliveryAddress = new PdfPCell(new Phrase("Delivery Address", HeaderFont));
            DeliveryAddress.PaddingTop = 5;
            DeliveryAddress.PaddingBottom = 5;
            tableOne.AddCell(DeliveryAddress);
            PdfPCell TxtDeliveryAddress = new PdfPCell(new Phrase(lblDeliveryAddress.Text.Replace("<br/>", "\n"), TextFont));
            TxtDeliveryAddress.PaddingTop = 5;
            TxtDeliveryAddress.PaddingBottom = 5;
            tableOne.AddCell(TxtDeliveryAddress);
            #endregion

            #region Third Row For Repeater
            if (DivP2PSale.Visible)
            {
                #region Third Row For Header Repeater
                PdfPCell Train = new PdfPCell(new Phrase("Train", HeaderFont));
                Train.PaddingTop = 5;
                Train.PaddingBottom = 5;
                tableTwo.AddCell(Train);
                PdfPCell Departs = new PdfPCell(new Phrase("Departs", HeaderFont));
                Departs.PaddingTop = 5;
                Departs.PaddingBottom = 5;
                tableTwo.AddCell(Departs);
                PdfPCell Arrives = new PdfPCell(new Phrase("Arrives", HeaderFont));
                OrderNumber.PaddingTop = 5;
                Arrives.PaddingBottom = 5;
                tableTwo.AddCell(Arrives);
                PdfPCell Class = new PdfPCell(new Phrase("Class", HeaderFont));
                Class.PaddingTop = 5;
                Class.PaddingBottom = 5;
                tableTwo.AddCell(Class);
                PdfPCell Passenger = new PdfPCell(new Phrase("Passenger Information", HeaderFont));
                Passenger.PaddingTop = 5;
                Passenger.PaddingBottom = 5;
                tableTwo.AddCell(Passenger);
                PdfPCell Price = new PdfPCell(new Phrase("Price", HeaderFont));
                Price.PaddingTop = 5;
                Price.PaddingBottom = 5;
                tableTwo.AddCell(Price);
                #endregion

                #region Fourth Row  For TextData Repeater
                foreach (RepeaterItem item in RptP2PDetails.Items)
                {
                    var pTrain = new Paragraph();
                    pTrain.Font = TextFont;
                    var hdnTrain = item.FindControl("hdnTrain") as HiddenField;
                    pTrain.Add(hdnTrain.Value.Replace("**", "\n"));
                    tableTwo.AddCell(pTrain);

                    var pDeparts = new Paragraph();
                    pDeparts.Font = TextFont;
                    var hdnDeparts = item.FindControl("hdnDeparts") as HiddenField;
                    pDeparts.Add(hdnDeparts.Value.Replace("**", "\n"));
                    tableTwo.AddCell(pDeparts);

                    var pArrives = new Paragraph();
                    pArrives.Font = TextFont;
                    var hdnArrives = item.FindControl("hdnArrives") as HiddenField;
                    pArrives.Add(hdnArrives.Value.Replace("**", "\n"));
                    tableTwo.AddCell(pArrives);

                    var pClass = new Paragraph();
                    pClass.Font = TextFont;
                    var hdnClass = item.FindControl("hdnClass") as HiddenField;
                    pClass.Add(hdnClass.Value);
                    tableTwo.AddCell(pClass);

                    var pPassenger = new Paragraph();
                    pPassenger.Font = TextFont;
                    var hdnPassenger = item.FindControl("hdnPassenger") as HiddenField;
                    pPassenger.Add(hdnPassenger.Value.Replace("**", "\n"));
                    tableTwo.AddCell(pPassenger);

                    var pPrice = new Paragraph();
                    pPrice.Font = TextFont;
                    var hdnPrice = item.FindControl("hdnPrice") as HiddenField;
                    pPrice.Add(currency + " " + hdnPrice.Value);
                    tableTwo.AddCell(pPrice);
                }
                #endregion
            }
            else
            {
                #region Third Row For Header Repeater
                PdfPCell Pass = new PdfPCell(new Phrase("Pass", HeaderFont));
                Pass.PaddingTop = 5;
                Pass.PaddingBottom = 5;
                tableTwoTwo.AddCell(Pass);
                PdfPCell Countries = new PdfPCell(new Phrase("Countries", HeaderFont));
                Countries.PaddingTop = 5;
                Countries.PaddingBottom = 5;
                tableTwoTwo.AddCell(Countries);
                PdfPCell Validity = new PdfPCell(new Phrase("Validity", HeaderFont));
                Validity.PaddingTop = 5;
                Validity.PaddingBottom = 5;
                tableTwoTwo.AddCell(Validity);
                PdfPCell Protected = new PdfPCell(new Phrase("Protected", HeaderFont));
                Protected.PaddingTop = 5;
                Protected.PaddingBottom = 5;
                tableTwoTwo.AddCell(Protected);
                PdfPCell Passenger = new PdfPCell(new Phrase("Passenger Information", HeaderFont));
                Passenger.PaddingTop = 5;
                Passenger.PaddingBottom = 5;
                tableTwoTwo.AddCell(Passenger);
                PdfPCell Price = new PdfPCell(new Phrase("Price", HeaderFont));
                Price.PaddingTop = 5;
                Price.PaddingBottom = 5;
                tableTwoTwo.AddCell(Price);
                #endregion

                #region Fourth Row  For TextData Repeater
                foreach (RepeaterItem item in RptPassDetails.Items)
                {
                    var pPass = new Paragraph();
                    pPass.Font = TextFont;
                    var hdnPass = item.FindControl("hdnPass") as HiddenField;
                    pPass.Add(hdnPass.Value.Replace("**", "\n"));
                    tableTwoTwo.AddCell(pPass);

                    var pCountries = new Paragraph();
                    pCountries.Font = TextFont;
                    var hdnCountries = item.FindControl("hdnCountries") as HiddenField;
                    pCountries.Add(hdnCountries.Value);
                    tableTwoTwo.AddCell(pCountries);

                    var pValidity = new Paragraph();
                    pValidity.Font = TextFont;
                    var hdnValidity = item.FindControl("hdnValidity") as HiddenField;
                    pValidity.Add(hdnValidity.Value);
                    tableTwoTwo.AddCell(pValidity);

                    var pProtected = new Paragraph();
                    pProtected.Font = TextFont;
                    var hdnProtected = item.FindControl("hdnProtected") as HiddenField;
                    pProtected.Add(hdnProtected.Value.Replace("**", "\n"));
                    tableTwoTwo.AddCell(pProtected);

                    var pPassenger = new Paragraph();
                    pPassenger.Font = TextFont;
                    var hdnPassenger = item.FindControl("hdnPassenger") as HiddenField;
                    pPassenger.Add(hdnPassenger.Value.Replace("**", "\n"));
                    tableTwoTwo.AddCell(pPassenger);

                    var pPrice = new Paragraph();
                    pPrice.Font = TextFont;
                    var hdnPrice = item.FindControl("hdnPrice") as HiddenField;
                    pPrice.Add(currency + " " + hdnPrice.Value);
                    tableTwoTwo.AddCell(pPrice);
                }
                #endregion
            }
            #endregion

            #region Fifth Row For Shipping
            PdfPCell Shipping = new PdfPCell(new Phrase("Shipping Amount", HeaderFont));
            Shipping.PaddingTop = 5;
            Shipping.PaddingBottom = 5;
            tableThree.AddCell(Shipping);
            PdfPCell TxtShipping = new PdfPCell(new Phrase(currency + lblShippingAmount.Text, TextFont));
            TxtShipping.PaddingTop = 5;
            TxtShipping.PaddingBottom = 5;
            tableThree.AddCell(TxtShipping);

            PdfPCell Booking = new PdfPCell(new Phrase("Booking Fee", HeaderFont));
            Booking.PaddingTop = 5;
            Booking.PaddingBottom = 5;
            tableThree.AddCell(Booking);
            PdfPCell TxtBooking = new PdfPCell(new Phrase(currency + lblBookingFee.Text, TextFont));
            TxtBooking.PaddingTop = 5;
            TxtBooking.PaddingBottom = 5;
            tableThree.AddCell(TxtBooking);
            #endregion

            #region Sixth One Row For Discount
            PdfPCell Discount = new PdfPCell(new Phrase("Discount Amount", HeaderFont));
            Discount.PaddingTop = 5;
            Discount.PaddingBottom = 5;
            tableThree.AddCell(Discount);
            PdfPCell TxtDiscount = new PdfPCell(new Phrase(currency + lblDiscount.Text, TextFont));
            TxtDiscount.PaddingTop = 5;
            TxtDiscount.PaddingBottom = 5;
            tableThree.AddCell(TxtDiscount);

            if (lblAdminFee.Text == "0.00")
            {
                #region For Blank column
                PdfPCell AdminFeeblankH = new PdfPCell(new Phrase(string.Empty, HeaderFont));
                AdminFeeblankH.PaddingTop = 5;
                AdminFeeblankH.PaddingBottom = 5;
                tableThree.AddCell(AdminFeeblankH);
                PdfPCell AdminFeeblankT = new PdfPCell(new Phrase(string.Empty, TextFont));
                AdminFeeblankT.PaddingTop = 5;
                AdminFeeblankT.PaddingBottom = 5;
                tableThree.AddCell(AdminFeeblankT);
                #endregion
            }
            else
            {
                PdfPCell AdminFee = new PdfPCell(new Phrase("Admin Fee", HeaderFont));
                AdminFee.PaddingTop = 5;
                AdminFee.PaddingBottom = 5;
                tableThree.AddCell(AdminFee);
                PdfPCell TxtAdminFee = new PdfPCell(new Phrase(currency + lblAdminFee.Text, TextFont));
                TxtAdminFee.PaddingTop = 5;
                TxtAdminFee.PaddingBottom = 5;
                tableThree.AddCell(TxtAdminFee);
            }

            PdfPCell NetTotal = new PdfPCell(new Phrase("Net Total", HeaderFont));
            NetTotal.PaddingTop = 5;
            NetTotal.PaddingBottom = 5;
            tableThree.AddCell(NetTotal);
            PdfPCell TxtNetTotal = new PdfPCell(new Phrase(currency + lblNetTotal.Text, TextFont));
            TxtNetTotal.PaddingTop = 5;
            TxtNetTotal.PaddingBottom = 5;
            tableThree.AddCell(TxtNetTotal);
            #endregion

            #region For Blank column
            PdfPCell blankH = new PdfPCell(new Phrase(string.Empty, HeaderFont));
            blankH.PaddingTop = 5;
            blankH.PaddingBottom = 5;
            tableThree.AddCell(blankH);
            PdfPCell blankT = new PdfPCell(new Phrase(string.Empty, TextFont));
            blankT.PaddingTop = 5;
            blankT.PaddingBottom = 5;
            tableThree.AddCell(blankT);
            #endregion

            #region Seventh For Grant Total
            PdfPCell GrantTotal = new PdfPCell(new Phrase("Total Price for " + lbltxttraveller.Text + ": " + currency + lblGrandTotal.Text, HeaderFont));
            GrantTotal.PaddingTop = 5;
            GrantTotal.PaddingBottom = 5;
            GrantTotal.Colspan = 4;
            GrantTotal.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
            tableThree.AddCell(GrantTotal);
            #endregion

            document.Open();
            document.Add(tableHeader);
            document.Add(tableOne);
            if (DivP2PSale.Visible)
                document.Add(tableTwo);
            else
                document.Add(tableTwoTwo);
            document.Add(tableThree);

        }
        finally
        {
            document.Close();
            string path = Server.MapPath("~/PdfReceipt/" + lblOrderNumber.Text.Trim() + ".pdf");
            if (File.Exists(path))
            {
                Response.ContentType = "Application/pdf";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + lblOrderNumber.Text.Trim() + ".pdf");
                Response.TransmitFile(path);
                Response.End();
            }
            else
            {
                ShowMessage(2, "Receipt not generated.");
            }
        }
    }

    public String GetJourney(string ProductType)
    {
        if (Session["TIRegionalBooking"] != null)
            return "";

        if (ProductType == "P2P")
        {
            if (IsFirst)
            {
                IsFirst = false;
                return "OUTBOUND:";
            }
            else
                return "INBOUND:";
        }
        return string.Empty;
    }

    public void ShowPaymentDetails()
    {
        try
        {
            if (!string.IsNullOrEmpty(OrderNo))
            {
                var lst = oBooking.GetAllCartData(Convert.ToInt64(OrderNo)).ToList();
                var lstNew = (from a in lst
                              select new
                              {
                                  PassSaleID = a.PassSaleID,
                                  ProductDesc = a.ProductType != "P2P" ? oBooking.GetProductName(a.PassSaleID) : string.Empty,
                                  Traveller = a.ProductType != "P2P" ? oBooking.GetTravellerName(a.PassSaleID) : string.Empty,
                                  Countries = oBooking.GetPassCountries(a.PassSaleID),
                                  Validity = a.ProductType != "P2P" ? oBooking.GetPassValidity(a.PassSaleID) : string.Empty,
                                  Protected = (a.TicketProtection.HasValue ? (a.TicketProtection.Value > 0 ? "Yes" : "No") : "No"),
                                  TKProtected = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0),
                                  ProtectedAmount = (a.TicketProtection.HasValue ? a.TicketProtection.Value > 0 ? currency + a.TicketProtection.Value : string.Empty : string.Empty),
                                  Trvname = a.ProductType != "P2P" ? a.Trvname : oBooking.GetP2PTravellerName(a.PassSaleID),
                                  Passengerinfo = a.Title + " " + a.FirstName + " " + a.MiddleName + " " + a.LastName.Split('<')[0],
                                  Price = (oBooking.getPrice(a.PassSaleID, a.ProductType)),
                                  Qty = 1,
                                  Arrives = a.ProductType == "P2P" ? oBooking.GetP2PArrives(a.PassSaleID) : string.Empty,
                                  Departs = a.ProductType == "P2P" ? oBooking.GetP2PDeparts(a.PassSaleID) : string.Empty,
                                  ArrivesStation = a.ProductType == "P2P" ? oBooking.GetP2PArrivesStation(a.PassSaleID) : string.Empty,
                                  DepartsStation = a.ProductType == "P2P" ? oBooking.GetP2PDepartsStation(a.PassSaleID) : string.Empty,
                                  Train = a.ProductType == "P2P" ? oBooking.GetP2PTrain(a.PassSaleID) : string.Empty,
                                  Class = a.ProductType == "P2P" ? oBooking.GetP2PClass(a.PassSaleID) : string.Empty,
                                  ServiceName = a.ProductType == "P2P" ? oBooking.GetP2PService(a.PassSaleID) : string.Empty,
                                  OrderIdentity = a.OrderIdentity,
                                  Jolurny = GetJourney(a.ProductType)
                              }).ToList().OrderBy(t => t.OrderIdentity).ToList();

                if (lst.Count > 0)
                {
                    if (Request.Params["req"] == null)
                    {
                        RptPassDetails.DataSource = lstNew;
                        RptPassDetails.DataBind();
                        DivP2PSale.Visible = false;
                    }
                    else
                    {
                        RptP2PDetails.DataSource = lstNew;
                        RptP2PDetails.DataBind();
                        DivPassSale.Visible = false;
                    }
                    lblEmailAddress.Text = lst.FirstOrDefault().EmailAddress;
                    lblOrderDate.Text = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                    lblOrderNumber.Text = lst.FirstOrDefault().OrderID.ToString();
                    lblShippingAmount.Text = (lst.FirstOrDefault().ShippingAmount ?? 0).ToString("F2");
                    lblDeliveryAddress.Text = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName + ", " + lst.FirstOrDefault().Address1 + ", " + (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2) ? lst.FirstOrDefault().Address2 + ", " : string.Empty) + "<br/>" + (!string.IsNullOrEmpty(lst.FirstOrDefault().City) ? lst.FirstOrDefault().City + ", " : string.Empty) + (!string.IsNullOrEmpty(lst.FirstOrDefault().State) ? lst.FirstOrDefault().State + ", " : string.Empty) + lst.FirstOrDefault().DCountry + ", " + lst.FirstOrDefault().Postcode;
                    lblDeliveryAddress.Text = lblDeliveryAddress.Text.Replace("  , , <br/>, ", string.Empty);
                    lblBookingFee.Text = Convert.ToDecimal(lst.FirstOrDefault().BookingFee).ToString("F2");
                    lblDiscount.Text = _master.GetOrderDiscountByOrderId(Convert.ToInt64(OrderNo));
                    lblAdminFee.Text = new ManageAdminFee().GetOrderAdminFeeByOrderID(Convert.ToInt32(OrderNo)).ToString();
                    if (lblAdminFee.Text == "0.00")
                    {
                        hidelblAdminfee.Attributes.Add("style", "display:none");
                        hidetxtAdminfee.Attributes.Add("style", "display:none");
                        lblAdminFee.Text = "0.00";
                    }
                    lblNetTotal.Text = (Convert.ToDecimal(lblAdminFee.Text) + (lstNew.Sum(a => a.Price) + lstNew.Sum(a => a.TKProtected)) + (lst.FirstOrDefault().ShippingAmount ?? 0) + (lst.FirstOrDefault().BookingFee ?? 0) + (!string.IsNullOrEmpty(EvOtherCharges) ? Convert.ToDecimal(EvOtherCharges) : 0)).ToString();
                    lblGrandTotal.Text = (Convert.ToDecimal(lblNetTotal.Text) - Convert.ToDecimal(lblDiscount.Text)).ToString("F2");
                    var sa = lstNew.GroupBy(t => new { t.Trvname }).Select(t => new
                    {
                        Qty = t.Sum(x => Convert.ToInt32(x.Qty)) + "",
                        TravellerName = t.Key.Trvname
                    });
                    foreach (var item in sa)
                        lbltxttraveller.Text = (string.IsNullOrEmpty(lbltxttraveller.Text) ? string.Empty : lbltxttraveller.Text + "+") + item.Qty + " " + item.TravellerName;
                }
            }
            else
            {
                Response.Redirect("~/Home", false);
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    public void PageContent(Guid pageID, Guid siteID)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageID && x.SiteID == siteID);
            if (result != null)
            {
                var url = result.Url;
                var oPage = _masterPage.GetPageDetailsByUrl(url);

                //Banner
                string[] arrListId = oPage.BannerIDs.Split(',');
                var idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _masterPage.GetBannerImgByID(idList);
                int countdata = list.Count();
                int index = new Random().Next(countdata);
                var newlist = list.Skip(index).FirstOrDefault();
                if (newlist != null && newlist.ImgUrl != null)
                    img_Banner.Src = adminSiteUrl + "" + newlist.ImgUrl;
                else
                    img_Banner.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void GetEvolviTicketInfo()
    {
        try
        {
            var evChargesList = oBooking.GetEvolviChargesByOrderId(Convert.ToInt64(OrderNo));
            if (evChargesList != null)
            {
                EvOtherCharges = ((evChargesList.TicketChange + evChargesList.CreditCardCharge) - (evChargesList.Discount)).ToString("F2");
                lblOtherCharges.Text = EvOtherCharges;
                otherchargesText.Visible = false;   // otherchargesText.Visible = true;
                otherchargesValue.Visible = false;  // otherchargesValue.Visible = true;
                if (!string.IsNullOrEmpty(evChargesList.BookingRef) && !string.IsNullOrEmpty(evChargesList.TODRef))
                {
                    div_BookingRef.Visible = true;
                    lblBookingRef.Text = evChargesList.BookingRef;
                    lblTODRef.Text = evChargesList.TODRef;
                }
            }
            else
            {
                otherchargesText.Visible = false;
                otherchargesValue.Visible = false;
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void GetEvolviBookingConfirmation()
    {
        try
        {
            if (Session["BOOKING-REQUEST"] != null)
            {
                var bookingList = Session["BOOKING-REQUEST"] as List<BookingRequest>;
                if (bookingList != null)
                {
                    foreach (var item in bookingList)
                    {
                        if (item.EvolviBookingRequest != null)
                        {
                            var Client = new OneHubRailOneHubClient();
                            RailBookingRequest request = item.EvolviBookingRequest;
                            request.Result = true;
                            _ManageOneHub.ApiLogin(request, siteId);
                            RailBookingResponse response = Client.EvolviRailBooking(request);
                            if (response != null)
                            {
                                if (response.ErrorMessage == null)
                                {
                                    var evBookingInfoList = response.BookingReservationInfos != null ? response.BookingReservationInfos.Select(x => x.TPA_ExtensionsBookingInfoList != null ? x.TPA_ExtensionsBookingInfoList : null).FirstOrDefault() : null;
                                    if (evBookingInfoList != null && !string.IsNullOrEmpty(OrderNo))
                                        oBooking.UpdateEvolviBookingRef(Convert.ToInt64(OrderNo), evBookingInfoList.OrderRef, evBookingInfoList.OrderItemRef, evBookingInfoList.TODRef);
                                }
                                else
                                    Response.Redirect("~/");
                            }
                            else
                                Response.Redirect("~/");
                        }
                    }
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public bool SendMail()
    {
        bool retVal = false;
        try
        {
            bool isDiscountSite = _master.GetOrderDiscountVisibleBySiteId(siteId);
            var smtpClient = new SmtpClient();
            var message = new MailMessage();
            var st = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
            if (st != null)
            {
                string SiteName = st.SiteURL + "Home";
                currency = oManageClass.GetCurrency(st.DefaultCurrencyID.HasValue ? (st.DefaultCurrencyID.Value) : new Guid());
                var orderID = Convert.ToInt32(OrderNo);
                string Subject = "Order Confirmation #" + orderID;
                string dir = HttpContext.Current.Request.PhysicalApplicationPath;

                ManageEmailTemplate objMailTemp = new ManageEmailTemplate();
                htmfile = Server.MapPath("~/MailTemplate/MailTemplate.htm");
                HtmlFileSitelog = string.IsNullOrEmpty(objMailTemp.GetMailTemplateBySiteId(siteId)) ? "IRMailTemplate.htm" : Server.MapPath(objMailTemp.GetMailTemplateBySiteId(siteId));

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(htmfile);
                var list = xmlDoc.SelectNodes("html");
                string body = list[0].InnerXml.ToString();
                string UserName = string.Empty;
                string EmailAddress = string.Empty;
                string DeliveryAddress = string.Empty;
                string BillingAddress = string.Empty;
                string OrderDate = string.Empty;
                string OrderNumber = string.Empty;
                string Total = string.Empty;
                string ShippingAmount = string.Empty;
                string GrandTotal = string.Empty;
                string BookingFee = string.Empty;
                string NetTotal = string.Empty;
                string Discount = string.Empty;
                string AdminFee = string.Empty;
                string OtherCharges = string.Empty;
                string BookingRefNO = string.Empty;
                string PassProtection = "0.00";
                string PassProtectionHtml = "";
                string DiscountHtml = "";
                bool isBeneDeliveryByMail = false;

                var lst = oBooking.GetAllCartData(Convert.ToInt64(OrderNo)).OrderBy(a => a.OrderIdentity).ToList();
                var lst1 = (from a in lst
                            select new
                            {
                                Price = (oBooking.getPrice(a.PassSaleID, a.ProductType)),
                                ProductDesc = (oBooking.getPassDesc(a.PassSaleID, a.ProductType) + "<br>" + a.FirstName + " " + a.MiddleName + " " + a.LastName),
                                TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0)
                            }).ToList();
                if (lst.Count > 0)
                {
                    EmailAddress = lst.FirstOrDefault().EmailAddress;
                    UserName = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName;
                    DeliveryAddress = UserName + "<br>" +
                        lst.FirstOrDefault().Address1 + "<br>" +
                      (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2) ? lst.FirstOrDefault().Address2 + "<br>" : string.Empty) +
                      (!string.IsNullOrEmpty(lst.FirstOrDefault().City) ? lst.FirstOrDefault().City + "<br>" : string.Empty) +
                      (!string.IsNullOrEmpty(lst.FirstOrDefault().State) ? lst.FirstOrDefault().State + "<br>" : string.Empty) +
                      lst.FirstOrDefault().DCountry + "<br>" +
                    lst.FirstOrDefault().Postcode + "<br>" +
                    GetBillingAddressPhoneNo(Convert.ToInt64(OrderNo));

                    OrderDate = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                    OrderNumber = lst.FirstOrDefault().OrderID.ToString();
                    Total = (lst1.Sum(a => a.Price)).ToString();
                    ShippingAmount = lst.FirstOrDefault().ShippingAmount.ToString();
                    BookingFee = lst.FirstOrDefault().BookingFee.ToString();
                    Discount = _master.GetOrderDiscountByOrderId(Convert.ToInt64(orderID));
                    AdminFee = new ManageAdminFee().GetOrderAdminFeeByOrderID(Convert.ToInt32(OrderNo)).ToString();
                    NetTotal = ((lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)) +
                        (lst.FirstOrDefault().ShippingAmount.HasValue ? lst.FirstOrDefault().ShippingAmount.Value : 0) +
                        (lst.FirstOrDefault().BookingFee.HasValue ? lst.FirstOrDefault().BookingFee.Value : 0) +
                        Convert.ToDecimal(AdminFee) + Convert.ToDecimal(EvOtherCharges)).ToString();
                    GrandTotal = (Convert.ToDecimal(NetTotal) - Convert.ToDecimal(Discount)).ToString("F2");

                    var billAddress = oBooking.GetBillingShippingAddress(Convert.ToInt64(orderID));
                    if (billAddress != null)
                    {
                        BillingAddress = (!string.IsNullOrEmpty(billAddress.Address1) ? billAddress.Address1 + "<br>" : string.Empty) +
                            (!string.IsNullOrEmpty(billAddress.Address2) ? billAddress.Address2 + "<br>" : string.Empty) +
                            (!string.IsNullOrEmpty(billAddress.City) ? billAddress.City + "<br>" : string.Empty) +
                            (!string.IsNullOrEmpty(billAddress.State) ? billAddress.State + "<br>" : string.Empty) +
                            (!string.IsNullOrEmpty(billAddress.Postcode) ? billAddress.Postcode + "<br>" : string.Empty) +
                            (!string.IsNullOrEmpty(billAddress.Country) ? billAddress.Country + "<br>" : string.Empty);
                    }
                }
                // to address
                string ToEmail = EmailAddress;
                GetReceiptLoga();
                body = body.Replace("##headerstyle##", SiteHeaderColor);
                body = body.Replace("##sitelogo##", logo);
                body = body.Replace("##OrderNumber##", OrderNumber);
                body = body.Replace("##UserName##", UserName.Trim());
                body = body.Replace("##EmailAddress##", EmailAddress.Trim());
                body = body.Replace("##OrderDate##", OrderDate.Trim());
                body = body.Replace("##Items##", currency + " " + Total.Trim());
                body = body.Replace("##BookingCondition##", st.SiteURL + "Booking-Conditions");

                #region receipt
                RUserName = UserName;
                OrderReceiptList.Add(new OrderReceipt { Id = 1, Name = "Order Id", Value = OrderNumber });
                OrderReceiptList.Add(new OrderReceipt { Id = 2, Name = "Name", Value = UserName });
                OrderReceiptList.Add(new OrderReceipt { Id = 3, Name = "Billing Address", Value = string.IsNullOrEmpty(DeliveryAddress) ? "" : DeliveryAddress.Replace("<br> ", "\n").Replace("<br>", "\n") });
                OrderReceiptList.Add(new OrderReceipt { Id = 4, Name = "Order Summary:", Value = "" });
                OrderReceiptList.Add(new OrderReceipt { Id = 5, Name = "Order Date", Value = OrderDate });
                OrderReceiptList.Add(new OrderReceipt { Id = 220, Name = "Subtotal", Value = currency + " " + Total });
                #endregion

                body = body.Replace("##NettPrice##", "");

                if (isEvolviBooking)
                {
                    OtherCharges = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Other Charges:</td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + EvOtherCharges + "</td></tr>";
                    BookingRefNO = "<tr><td align='left' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong>Booking Ref: </strong></td><td align='left' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'>" + lblBookingRef.Text.Trim() + "</td></tr>";
                    #region receipt
                    OrderReceiptList.Add(new OrderReceipt { Id = 0, Name = "Booking Ref", Value = lblBookingRef.Text });
                    #endregion
                }

                body = body.Replace("##BookingRef##", BookingRefNO);
                body = body.Replace("##Shipping##", currency + " " + ShippingAmount.Trim());
                body = body.Replace("##DeliveryFee##", currency + " " + ShippingAmount.Trim());
                body = body.Replace("##BookingFee##", currency + " " + BookingFee.Trim());
                body = body.Replace("##Total##", currency + " " + GrandTotal.Trim());

                #region receipt
                OrderReceiptList.Add(new OrderReceipt { Id = 221, Name = "Shipping", Value = currency + " " + ShippingAmount });
                OrderReceiptList.Add(new OrderReceipt { Id = 222, Name = "Booking Fee", Value = currency + " " + BookingFee });
                OrderReceiptList.Add(new OrderReceipt { Id = 227, Name = "Order Total", Value = currency + " " + GrandTotal });
                #endregion

                string AdminFeeRow = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Admin Fee: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + AdminFee.Trim() + "</td></tr>";

                if (AdminFee == "0.00")
                    AdminFeeRow = string.Empty;
                if (isDiscountSite)
                {
                    body = body.Replace("##AdminFee##", AdminFeeRow);
                    if (Discount == "0.00")
                        body = body.Replace("##Discount##", "");
                    else
                    {
                        DiscountHtml = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Discount: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + Discount.Trim() + "</td></tr>";
                        body = body.Replace("##Discount##", DiscountHtml);
                    }

                    #region receipt
                    if (AdminFee != "0.00")
                        OrderReceiptList.Add(new OrderReceipt { Id = 224, Name = "Admin Fee", Value = currency + " " + AdminFee });
                    OrderReceiptList.Add(new OrderReceipt { Id = 225, Name = "Net Total", Value = currency + " " + NetTotal });
                    OrderReceiptList.Add(new OrderReceipt { Id = 226, Name = "Discount", Value = currency + " " + Discount });
                    #endregion
                }
                else
                {
                    body = body.Replace("##AdminFee##", AdminFeeRow);
                    body = body.Replace("##Discount##", "");
                }

                var P2PSaleList = new ManageBooking().GetP2PSaleListByOrderID(orderID);
                if (isEvolviBooking)
                    body = body.Replace("##ShippingMethod##", "Ticket on collection");
                else if (isBene)
                {
                    if (P2PSaleList != null && !string.IsNullOrEmpty(P2PSaleList.FirstOrDefault().DeliveryOption))
                    {
                        string shippingdesc = (Session["ShipMethod"] != null ? Session["ShipMethod"].ToString() : "") + (Session["ShipDesc"] != null ? (Server.HtmlDecode(Session["ShipDesc"].ToString())) : "");
                        if (P2PSaleList.Any(x => x.DeliveryOption.ToLower() == "delivery by mail"))
                        {
                            body = body.Replace("##ShippingMethod##", !string.IsNullOrEmpty(shippingdesc) ? shippingdesc : "Delivery by mail");
                            isBeneDeliveryByMail = true;
                        }
                        else
                            body = body.Replace("##ShippingMethod##", P2PSaleList != null ? P2PSaleList.FirstOrDefault().DeliveryOption : "");
                    }
                }
                else
                {
                    string britrailpromoTerms = isBritRailPromoPass(orderID) ? "BritRail Freeday Promo pass <a href='" + st.SiteURL + "BritRailFreedayPromoTerms.aspx' traget='_blank'> Click Here </a>" : "";
                    if (Session["ShipMethod"] != null)
                    {
                        string shippingdesc = (Session["ShipMethod"] != null ? Session["ShipMethod"].ToString() : "") + (Session["ShipDesc"] != null ? (Server.HtmlDecode(Session["ShipDesc"].ToString())) : "");
                        body = body.Replace("##ShippingMethod##", shippingdesc + britrailpromoTerms);
                    }
                    else
                        body = body.Replace("##ShippingMethod##", britrailpromoTerms);
                }

                body = body.Replace("#Blanck#", "&nbsp;");
                var PrintResponselist = Session["TicketBooking-REPLY"] as List<PrintResponse>;
                List<GetAllCartData_Result> lstC = oBooking.GetAllCartData(Convert.ToInt64(OrderNo)).ToList();
                var lstNew = (from a in lstC
                              select new
                              {
                                  a,
                                  Price = (oBooking.getPrice(a.PassSaleID, a.ProductType)),
                                  ProductDesc = a.ProductType == "P2P"
                                  ? (oBooking.getP2PDetailsForEmail(a.PassSaleID, PrintResponselist, (string)(Session["TrainType"]), isEvolviBooking, false, EvolviTandC))
                                  : (oBooking.getPassDetailsForEmail(a.PassSaleID, PrintResponselist, (string)(Session["TrainType"]), EvolviTandC)),
                                  ReceiptProductDetail = (oBooking.GetReceiptPassDescMail(a.PassSaleID, a.ProductType, PrintResponselist, (string)(Session["TrainType"]))),
                                  TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0),
                                  Terms = a.terms
                              }).ToList();
                string strProductDesc = "";
                int i = 1;
                int newid = 5;
                if (lstNew.Count() > 0)
                {
                    lstNew = lstNew.OrderBy(ty => ty.a.OrderIdentity).ToList();
                    if (lstNew.Any(x => x.a.ProductType == "P2P"))
                    {
                        if (isBeneDeliveryByMail)
                        {
                            body = body.Replace("##HeaderText##", "Delivery address");
                            body = body.Replace("##HeaderDeliveryText##", "Your order will be sent to:");
                            body = body.Replace("##DeliveryAddress##", DeliveryAddress.Trim());
                        }
                        else
                        {
                            body = body.Replace("##HeaderText##", "Tickets");
                            body = body.Replace("##HeaderDeliveryText##", "Your ticket collection reference:");
                            body = body.Replace("##DeliveryAddress##", oBooking.getP2PDetailsForEmailTicket(orderID, EvolviTandC, siteURL + "images/pdf-istock.jpg"));
                        }
                    }
                    else
                    {
                        body = body.Replace("##HeaderText##", "Delivery address");
                        body = body.Replace("##HeaderDeliveryText##", "Your order will be sent to:");
                        body = body.Replace("##DeliveryAddress##", DeliveryAddress.Trim());
                    }

                    foreach (var x in lstNew)
                    {
                        if (x.a.ProductType != "P2P")
                        {
                            strProductDesc += "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth' align='center' bgcolor='#eeeeee'><tr><td bgcolor='#ffffff' height='5'></td></tr><tr><td style='padding: 10px;' align='center' valign='top' class='pad'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' class='deviceWidth '>";
                            strProductDesc += x.ProductDesc.Replace("##Price##", currency + " " + x.Price.ToString()).Replace("##Name##", x.a.Title + " " + x.a.FirstName + (!string.IsNullOrEmpty(x.a.MiddleName) ? " " + x.a.MiddleName : "") + " " + x.a.LastName).Replace("##NetAmount##", "");
                            strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Pass start date:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + (x.a.PassStartDate.HasValue ? x.a.PassStartDate.Value.ToString("dd/MMM/yyyy") : string.Empty) + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>";
                            strProductDesc += (Session["CollectStation"] != null ? "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Collect at ticket desk:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + Session["CollectStation"].ToString() + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>" : "");

                            string ElectronicNote = oBooking.GetElectronicNote(x.a.PassSaleID.Value);
                            if (ElectronicNote.Length > 0)
                                strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong></strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #ff0000;font-family: Arial, Helvetica, sans-serif;'>" + ElectronicNote + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>";

                            strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Ticket Protection:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" +
                                (x.TktPrtCharge > 0 ? "Yes" : "No") + "</td><td width='20%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong>" + currency + " " + Convert.ToString(x.TktPrtCharge) + "</strong></td></tr>";
                            strProductDesc += "</table></td></tr></table>";

                            PassProtection = (Convert.ToDecimal(PassProtection) + x.TktPrtCharge).ToString();
                            #region receipt
                            newid = newid + i;
                            OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Pass " + i + ":", Value = "" });
                            newid++;
                            foreach (var w in x.ReceiptProductDetail)
                            {
                                OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = w.Name + i, Value = w.Value + (ElectronicNote.Length > 0 ? "ñ" + ElectronicNote : "") });
                                newid++;
                            }
                            if (x.a.PassStartDate.HasValue)
                            {
                                OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Pass start date", Value = x.a.PassStartDate.Value.ToString("dd/MMM/yyyy") });
                                newid++;
                            } newid++;

                            OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Passenger Name", Value = x.a.Title + " " + x.a.FirstName + " " + x.a.MiddleName + " " + x.a.LastName });
                            newid++;
                            OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Price", Value = currency + " " + x.Price });
                            newid++;
                            if (x.TktPrtCharge > 0)
                            {
                                OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Ticket Protection", Value = currency + " " + x.TktPrtCharge });
                                newid++;
                            }
                            #endregion
                        }
                        else
                        {
                            strProductDesc += "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth' align='center' bgcolor='#eeeeee'><tr><td bgcolor='#ffffff' height='5'></td></tr><tr><td style='padding: 10px;' align='center' valign='top' class='pad'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' class='deviceWidth'>";
                            strProductDesc += x.ProductDesc.Replace("##Price##", currency + " " + x.Price.ToString()).Replace("##NetAmount##", "").Replace("##Name##", x.a.Title + " " + x.a.FirstName + (!string.IsNullOrEmpty(x.a.MiddleName) ? " " + x.a.MiddleName : "") + " " + (!string.IsNullOrEmpty(x.a.LastName) ? x.a.LastName.Split(new[] { "<BR/>" }, StringSplitOptions.None)[0] : ""));
                            strProductDesc += (HttpContext.Current.Session["CollectStation"] != null ? "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Collect at ticket desk:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + HttpContext.Current.Session["CollectStation"] == null ? "" : HttpContext.Current.Session["CollectStation"].ToString() + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>" : "");
                            if (P2PSaleList != null && P2PSaleList.Count > 0 && P2PSaleList.Any(z => z.ApiName.ToUpper() == "ITALIA"))
                            {
                                if (string.IsNullOrEmpty(P2PSaleList.FirstOrDefault(z => z.ID == x.a.PassSaleID).PdfURL))
                                    strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Important:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>This PNR is issued as a ticket on depart and must be printed from one of the self-service ticket machines located at all main Trenitalia stations (which are green, white and red). Once you have printed your ticket you must validate it in one of the validation machines located near to the entrance of each platform (they are green and white with Trenitalia written on the front and usually mounted on a wall), THIS MUST BE DONE BEFORE BOARDING YOUR TRAIN, failure to do so will result in a fine.</td></tr>";
                            }
                            PassProtection = (Convert.ToDecimal(PassProtection) + x.TktPrtCharge).ToString();
                            if (x.Terms != null)
                                strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Ticket Terms:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + HttpContext.Current.Server.HtmlDecode(x.Terms) + "</td></tr>";
                            if (isEvolviBooking)
                            {
                                var P2PData = _db.tblP2PSale.FirstOrDefault(Q => Q.ID == x.a.PassSaleID.Value);
                                if (P2PData != null)
                                {
                                    if (!string.IsNullOrEmpty(P2PData.EvSleeper))
                                    {
                                        strProductDesc += "<tr><td width='22%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Reservation Details:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>";
                                        foreach (var item in P2PData.EvSleeper.Split('@'))
                                        {
                                            string ss = item.Replace("Seat Allocation:", "").Replace("Berth Allocation:", "");
                                            strProductDesc += "<div>" + HttpContext.Current.Server.HtmlDecode(ss) + "</div>";
                                        }
                                        strProductDesc += "</td></tr>";
                                    }
                                }
                            }
                            strProductDesc += "</table></td></tr></table>";

                            #region receipt
                            newid = newid + i;
                            OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Pass " + i + ":", Value = "" });
                            newid++;
                            foreach (var w in x.ReceiptProductDetail)
                            {
                                OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = w.Name, Value = w.Value });
                                newid++;
                            }
                            OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Passenger Name", Value = x.a.Title + " " + x.a.FirstName + " " + x.a.MiddleName + " " + (x.a.LastName.Split('<')[0] + (x.a.PassSaleID.HasValue ? "\nPassenger: " + (oBooking.GetPassengerDetails(x.a.PassSaleID.Value)) : "")) });
                            newid++;
                            OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Price", Value = currency + " " + x.Price });
                            newid++;
                            if (x.TktPrtCharge > 0)
                            {
                                OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Ticket Protection", Value = currency + " " + x.TktPrtCharge });
                                newid++;
                            }
                            #endregion
                        }
                        i++;
                    }
                }

                PassProtectionHtml = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Pass Protection: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + PassProtection.Trim() + "</td></tr>";
                body = body.Replace("##PassProtection##", PassProtection == "0.00" ? "" : PassProtectionHtml);
                body = body.Replace("##OrderDetails##", strProductDesc);
                body = body.Replace("##BillingAddress##", BillingAddress);
                if (Request.Params["req"] != null)
                {
                    var getDeliveryType = oBooking.getDeliveryOption(Convert.ToInt64(Session["P2POrderID"]));
                    if (getDeliveryType != null && getDeliveryType == "Print at Home")
                    {
                        const string printAthome = "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth '><tr><td style='padding: 10px; font-size: 12px; color: #333; font-family: Arial, Helvetica, sans-serif;'colspan='2'>Please ensure you have a printed copy of your PDF booking confirmation as train operators across europe will not always accept the booking from your tablet or mobile screen.</td></tr></table>";
                        body = body.Replace("##PrintAtHome##", printAthome);
                        Session["showPrintAtHome"] = "1";
                    }
                    else
                        body = body.Replace("##PrintAtHome##", "");
                }
                else
                    body = body.Replace("##PrintAtHome##", "");

                body = body.Replace("../images/", SiteName + "images/");

                /*Send Email to Bcc email address*/
                string BccEmail = _oWebsitePage.GetBccEmail(siteId);
                /*Get smtp details*/
                var result = _masterPage.GetEmailSettingDetail(siteId);
                if (result != null && lstNew.Count() > 0)
                {
                    var fromAddres = new MailAddress(result.Email, result.Email);
                    smtpClient.Host = result.SmtpHost;
                    smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                    smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                    message.From = fromAddres;
                    message.To.Add(ToEmail);
                    if (!string.IsNullOrEmpty(BccEmail))
                        message.Bcc.Add(BccEmail);
                    message.Bcc.Add(ConfigurationManager.AppSettings["BCCMailAddress"]);
                    message.Subject = Subject;
                    message.IsBodyHtml = true;
                    message.Body = body;
                    smtpClient.Send(message);
                    retVal = true;
                }
                Session["ShipMethod"] = string.Empty;
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, "Failed to send E-mail confimation!");
        }
        return retVal;
    }

    public void GetPrintReceipt()
    {
        Document document = new Document(PageSize.A4, 20f, 20f, 20f, 20f);
        try
        {
            GetReceiptLoga();
            string filename = Server.MapPath("~/PdfReceipt/" + lblOrderNumber.Text.Trim() + ".pdf");
            float[] widths = new float[] { 3f, 5f, 4f };
            iTextSharp.text.Font TitleFont = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font HeaderFont = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font TextFont = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font TextRed = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, BaseColor.RED);
            iTextSharp.text.Font TextBlue = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.UNDERLINE, BaseColor.BLUE);
            PdfWriter.GetInstance(document, new FileStream(filename, FileMode.Create));

            #region Pdf Table Creates
            PdfPTable tableOne = new PdfPTable(3);
            tableOne.TotalWidth = 550f;
            tableOne.LockedWidth = true;
            tableOne.SetWidths(widths);
            tableOne.HorizontalAlignment = 0;
            #endregion

            iTextSharp.text.Image RTlogo = iTextSharp.text.Image.GetInstance(logo);
            PdfPCell SiteLogo = new PdfPCell(RTlogo);
            SiteLogo.PaddingTop = 5;
            SiteLogo.PaddingBottom = 5;
            SiteLogo.Colspan = 3;
            SiteLogo.Border = Rectangle.NO_BORDER;
            tableOne.AddCell(SiteLogo);

            PdfPCell headingText1 = new PdfPCell(new Phrase("Thank you for your order", HeaderFont));
            headingText1.Border = Rectangle.NO_BORDER;
            tableOne.AddCell(headingText1);

            PdfPCell headingText2 = new PdfPCell(new Phrase(RUserName, TextFont));
            headingText2.Border = Rectangle.NO_BORDER;
            headingText2.Colspan = 2;
            tableOne.AddCell(headingText2);

            PdfPCell Note1 = new PdfPCell(new Phrase("Note", HeaderFont));
            Note1.Border = Rectangle.NO_BORDER;
            tableOne.AddCell(Note1);

            PdfPCell Note2 = new PdfPCell(new Phrase("This is not your travel document", TextRed));
            Note2.Border = Rectangle.NO_BORDER;
            Note2.Colspan = 2;
            tableOne.AddCell(Note2);

            bool IsFirst = true;
            bool IsFirstTOD = true;
            foreach (var x in OrderReceiptList.OrderBy(x => x.Id))
            {
                if (x.Name.Contains("Order Summary") || x.Name.Contains("Pass") && string.IsNullOrEmpty(x.Value))
                {
                    x.Name = x.Name.Contains("Pass") ? "" : x.Name;
                    PdfPCell CellName = new PdfPCell(new Phrase(x.Name, HeaderFont));
                    CellName.PaddingTop = 15;
                    CellName.PaddingBottom = 7;
                    CellName.Colspan = 2;
                    CellName.Border = Rectangle.NO_BORDER;
                    tableOne.AddCell(CellName);
                }
                else if (x.Name.Contains("Subtotal"))
                {
                    var CellName = new PdfPCell(new Phrase(x.Name, HeaderFont));
                    CellName.Border = Rectangle.NO_BORDER;
                    CellName.PaddingTop = 15;
                    tableOne.AddCell(CellName);

                    var CellValue = new PdfPCell(new Phrase(x.Value, TextFont));
                    CellValue.Border = Rectangle.NO_BORDER;
                    CellValue.PaddingTop = 15;
                    tableOne.AddCell(CellValue);
                }
                else
                {
                    var CellName = new PdfPCell(new Phrase(x.Name, HeaderFont));
                    CellName.Border = Rectangle.NO_BORDER;
                    tableOne.AddCell(CellName);


                    string[] splitstring = x.Value.Split('ñ');
                    var CellValue = new PdfPCell(new Phrase(x.Value, TextFont));
                    CellValue.Border = Rectangle.NO_BORDER;
                    if (x.Name == "TOD Ref" && IsFirstTOD)
                    {
                        IsFirstTOD = false;
                        var link = new Chunk("How to collect your ticket", TextBlue).SetAnchor(EvolviTandC);
                        Paragraph SParagraph = new Paragraph();
                        SParagraph.SetLeading(3, 1);
                        SParagraph.Font.Size = 10;
                        SParagraph.Add(x.Value + "\n");
                        SParagraph.Add(link);
                        CellValue.MinimumHeight = 34F;
                        CellValue.AddElement(SParagraph);
                    }
                    if (x.Name.Contains("Journey ") && splitstring.Length > 1)
                    {
                        var link = new Chunk(splitstring[1], TextRed);
                        Paragraph SParagraph = new Paragraph();
                        SParagraph.SetLeading(3, 1);
                        SParagraph.Font.Size = 10;
                        SParagraph.Add(splitstring[0] + "\n");
                        SParagraph.Add(link);
                        CellValue.MinimumHeight = 34F;
                        CellValue.AddElement(SParagraph);
                    }
                    tableOne.AddCell(CellValue);
                }
                if (IsFirst)
                {
                    IsFirst = false;
                    iTextSharp.text.Image RSimg1 = iTextSharp.text.Image.GetInstance(img1);
                    RSimg1.ScaleAbsolute(130f, 130f);
                    RSimg1.SpacingAfter = 10;
                    iTextSharp.text.Image RSimg2 = iTextSharp.text.Image.GetInstance(img2);
                    RSimg2.ScaleAbsolute(130f, 130f);
                    RSimg2.SpacingAfter = 10;
                    iTextSharp.text.Image RSimg3 = iTextSharp.text.Image.GetInstance(img3);
                    RSimg3.ScaleAbsolute(130f, 130f);
                    RSimg3.SpacingAfter = 10;
                    PdfPCell Simg = new PdfPCell();
                    Simg.HorizontalAlignment = 1;
                    Simg.AddElement(RSimg1);
                    Simg.AddElement(RSimg2);
                    Simg.AddElement(RSimg3);
                    Simg.Rowspan = OrderReceiptList.Count + 2;
                    Simg.Border = Rectangle.NO_BORDER;
                    tableOne.AddCell(Simg);
                }
            }

            PdfPCell CellName1 = new PdfPCell(new Phrase("Here to help...", HeaderFont));
            CellName1.PaddingTop = 15;
            CellName1.PaddingBottom = 7;
            CellName1.Colspan = 2;
            CellName1.Border = Rectangle.NO_BORDER;
            tableOne.AddCell(CellName1);

            string txt1 = "If you have any queries about your order please do not hesitate to contact us.\n\nThank you for shopping with us and happy travels!";
            PdfPCell CellName2 = new PdfPCell(new Phrase(txt1, TextFont));
            CellName2.Colspan = 2;
            CellName2.Border = Rectangle.NO_BORDER;
            tableOne.AddCell(CellName2);

            string txt2 = "\n\n\nSee internationalrail.com for delivery options and full terms and conditions.\nOur Privacy Policy and Terms and Conditions are also available on our website";
            PdfPCell CellName3 = new PdfPCell(new Phrase(txt2, TextFont));
            CellName3.PaddingTop = 15;
            CellName3.PaddingBottom = 7;
            CellName3.Colspan = 3;
            CellName3.Border = Rectangle.NO_BORDER;
            CellName3.HorizontalAlignment = 1;
            tableOne.AddCell(CellName3);

            string txt3 = "(c) International Rail Ltd is a company registered in England & Wales.\nRegistered number: 3060803";
            PdfPCell CellName4 = new PdfPCell(new Phrase(txt3, HeaderFont));
            CellName4.Colspan = 3;
            CellName4.Border = Rectangle.NO_BORDER;
            CellName4.HorizontalAlignment = 1;
            tableOne.AddCell(CellName4);

            var CellNamex = new PdfPCell(new Phrase(document.PageSize.Height + "; ", HeaderFont));
            CellNamex.Border = Rectangle.NO_BORDER;
            CellNamex.MinimumHeight = document.PageSize.Height - (tableOne.TotalHeight + 50f);
            CellName2.Colspan = 2;
            tableOne.AddCell(CellNamex);

            document.Open();
            document.Add(tableOne);
            anchor_pdf.HRef = siteURL + "PdfReceipt/" + lblOrderNumber.Text.Trim() + ".pdf";
        }
        catch (Exception ex)
        {
            ShowMessage(2, "Receipt not generated.");
        }
        finally
        {
            document.Close();
            document.Dispose();
        }
    }

    public void GetReceiptLoga()
    {
        if (HtmlFileSitelog.ToLower().Contains("irmailtemplate"))
            Name = "IR";
        else if (HtmlFileSitelog.ToLower().Contains("travelcut"))
            Name = "TC";
        else if (HtmlFileSitelog.ToLower().Contains("stamail"))
            Name = "STA";
        else if (HtmlFileSitelog.ToLower().Contains("MeritMail"))
            Name = "MM";

        switch (Name)
        {
            case "TC":
                logo = "https://www.internationalrail.com/images/travelcutslogo.png";
                img1 = "https://www.internationalrail.com/images/image1-top_blue.jpg";
                img2 = "https://www.internationalrail.com/images/image2-middle_blue.jpg";
                img3 = "https://www.internationalrail.com/images/image3-bottom_blue.jpg";
                SiteHeaderColor = "#0f396d";
                break;
            case "STA":
                logo = "https://www.internationalrail.com/images/mainLogo.png";
                img1 = "https://www.internationalrail.com/images/image1-top_blue.jpg";
                img2 = "https://www.internationalrail.com/images/image2-middle_blue.jpg";
                img3 = "https://www.internationalrail.com/images/image3-bottom_blue.jpg";
                SiteHeaderColor = "#0c6ab8";
                break;
            case "MM":
                logo = "https://www.internationalrail.com/images/meritlogo.png";
                img1 = "https://www.internationalrail.com/images/image1-top_blue.jpg";
                img2 = "https://www.internationalrail.com/images/image2-middle_blue.jpg";
                img3 = "https://www.internationalrail.com/images/image3-bottom_blue.jpg";
                SiteHeaderColor = "#0c6ab8";
                break;
            default:
                logo = "https://www.internationalrail.com/images/logo.png";
                img1 = "https://www.internationalrail.com/images/image1-top_red.jpg";
                img2 = "https://www.internationalrail.com/images/image2-middle_red.jpg";
                img3 = "https://www.internationalrail.com/images/image3-bottom_red.jpg";
                SiteHeaderColor = "#941e34";
                break;
        }
    }

    public void UpdateP2PStatus()
    {
        try
        {
            if (!string.IsNullOrEmpty(OrderNo))
            {
                var lst = oBooking.GetAllCartData(Convert.ToInt64(OrderNo)).ToList();
                if (lst != null && lst.Count > 0)
                    foreach (var item in lst)
                    {
                        bool sts = item.PassSaleID.HasValue ? oBooking.UpdateP2PSaleStatus(item.PassSaleID.Value) : false;
                    }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public string GetApiName()
    {
        if (isTI)
            return "TrainItalia";
        else if (isBene)
            return "BeNe";
        else if (isNTVBooking)
            return "Ntv";
        else if (isEvolviBooking)
            return "Evolvi";
        else
            return "";
    }

    public string GetBillingAddressPhoneNo(long OrderID)
    {
        var result = _db.tblOrderBillingAddresses.FirstOrDefault(x => x.OrderID == OrderID);
        if (result != null)
        {
            return !string.IsNullOrEmpty(result.Phone) ? result.Phone : "";
        }
        return "";
    }
}

public class BookingCartInfo
{
    // A unique identifier for the order
    public string order_id { get; set; }
    // The standard letter code in capitals for the currency type in which the order is being paid, eg: EUR, USD, GBP
    public string currency { get; set; }
    // A valid number with the total cost of the basket including any known tax per item, but not including shipping or discounts
    public Decimal subtotal { get; set; }
    // A boolean true or false to indicate whether subtotal includes tax
    public bool subtotal_include_tax { get; set; }
    // A valid number with the total amount of potential tax included in the order
    public Decimal tax { get; set; }
    // A valid number with the total amount of potential shipping costs included in the order
    public Decimal shipping_cost { get; set; }
    // Optional. Describes the shipping method
    public string shipping_method { get; set; }
    // A valid number with the total cost of the basket including any known tax, shipping and discounts
    public Decimal total { get; set; }
    //Product list
    public List<ProuductLineItem> line_items { get; set; }
    // If voucher used
    public string voucher { get; set; }
    // A valid number with the total amount of discount due to the voucher entered
    public Decimal voucher_discount { get; set; }
    // Date of Order (MM/DD/YYYY)
    public string Date_of_order { get; set; }
    // Time of Order (HH:MM:SS)
    public string Time_of_order { get; set; }
}

public class ProuductLineItem
{
    public ProductItem product { get; set; }
    //quantity:1
    public int quantity { get; set; }
    //subtotal:5000.00
    public Decimal subtotal { get; set; }
}

public class ProductItem
{
    //sku_code: "SKU",
    public string sku_code { get; set; }

    //name: "PRODUCT-NAME",
    public string name { get; set; }

    //category: "PRODUCT-CATEGORY",
    public string category { get; set; }
    // currency: "GBP",
    public string currency { get; set; }

    //unit_price:151.53
    public string unit_price { get; set; }

    //unit_sale_price:151.53
    public string unit_sale_price { get; set; }
}