﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="P2PBookingCart.aspx.cs" Inherits="P2PBookingCart"
    UICulture="en" Culture="en-GB" %>

<%@ Register TagPrefix="uc" TagName="ucTicketDelivery" Src="~/UserControls/ucTicketDelivery.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%=ChkOutActionsTags%>
    <%=ChkOutSetActionsTags%>
    <style type="text/css">
        .starail-BookingDetails-form {
            overflow: visible;
            width: 100%;
            float: left;
        }

        #MainContent_pnlbindshippping {
            width: 931px;
        }

        .inputContainer-1 {
            width: 30% !important;
        }

        .inputContainer-2 {
            width: 30% !important;
            padding-right: 1rem;
        }

        .inputContainer-3 {
            width: 40% !important;
        }

        #pnlQuckLoad {
            width: 600px;
            top: 2% !important;
        }

            #pnlQuckLoad .starail-YourBooking {
                padding: 2.14286rem 1.14286rem 0.42857rem 1.14286rem;
            }

        #MainContent_div_TermCondition {
            height: 500px;
            overflow-x: auto;
        }

        .chkwidth {
            width: 1%;
        }

        .p-full-text {
            text-align: justify;
            margin: 0px;
        }

        .starail-BookingDetails-title {
            display: inline-block;
        }

        .timertopcls {
            display: inline-block;
            float: right;
            margin-top: 4px;
            padding: 4px 6px;
            border-radius: 7px;
            font-size: 12px;
            top: 49% !important;
            right: 15.5% !important;
            position: fixed;
            z-index: 100001;
            background: #fff;
            text-align: center;
            margin-right: -120px;
            box-shadow: 2px 4px 4px #d4d4d4;
            text-shadow: 1px 2px 3px #d4d4d4;
        }

            .timertopcls a {
                font-weight: bold;
                text-decoration: underline;
            }

        @media screen and (min-device-width: 360px) and (max-device-width: 768px) {
            .starail-BookingDetails-form {
                overflow: hidden !important;
                width: auto;
                float: none;
            }

            #MainContent_pnlbindshippping {
                width: auto;
            }
        }

        @media (max-width: 767px) {
            #MainContent_div_DeliverByDate_Bene label {
                width: 315px;
            }

            #pnlQuckLoad {
                width: 90%;
                top: 6% !important;
            }

            #MainContent_div_TermCondition {
                height: 200px;
            }

            .starail-BookingDetails-title {
                display: block;
                margin-bottom: 10px;
            }

            .timertopcls {
                display: block;
                margin: 0 15px 10px 15px;
                float: left;
                right: -4% !important;
            }
        }

        #MainContent_div_DeliverByDate_Bene label {
            width: 619px;
        }

        @media only screen and (max-width: 640px) {
            .starail-BookingDetails-form {
                width: 95%;
            }

            #MainContent_ucTicketDelivery_rdoBkkoingList > label {
                width: 80% !important;
            }

            #MainContent_div_DeliverByDate_Bene label {
                width: 100%;
            }
        }

        @media only screen and (max-width: 480px) {
            .inputContainer-1 {
                width: 100% !important;
                padding-top: 2%;
            }

            .inputContainer-2 {
                width: 100% !important;
                padding-right: 0%;
                padding-top: 2%;
            }

            .inputContainer-3 {
                width: 100% !important;
                padding-top: 2%;
            }

            .paddingtop {
                padding-bottom: 2%;
            }
        }
    </style>
    <script type="text/javascript">
        function departureDate() {
            $("#MainContent_txtDateOfDepature").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                maxDate: '+6m'
            });
        }
        $(document).ready(function () {
            $("#radioChkShp").attr("checked", "checked");
            $(".bookingcheckboxSpan").remove();
            $(".bookingcheckbox").find('label').before("<span class='bookingcheckboxSpan'><i class='starail-Icon-tick'></i></span>");
            $("#MainContent_txtDateOfDepature").bind("contextmenu cut copy paste", function (e) {
                return false;
            });

            $('.calculateTotal').click(function () {
                getdata();
            });
            getdata();
            departureDate();
            GetDeliveryMethod();
            popupsection();
        });

        function Focusonerrorcontrol() {
            $(window).on('click', function () {
                var Toperror = $(".starail-Form-error").eq(0).offset();
                if (Toperror != undefined) {
                    $('body,html').animate({ scrollTop: Toperror.top }, 1000);
                    $(window).off('click');
                }
                else if ('<%=isEvolviBooking %>' == 'True' && !$('#evTermCondition').is(':checked')) {
                    $(window).off('click');
                    alert('To continue, you must accept our Terms and Conditions.');
                    return false;
                }
            });
    }

    function popupsection() {
        $(document).ready(function () {
            $('.ticket-Restrictions').click(function () {
                $('#pnlQuckLoad_background,#pnlQuckLoad').show();
            });
            $('#btnCloseWin').click(function () {
                $('#pnlQuckLoad_background,#pnlQuckLoad').hide();
            });
        });
    }
    /*shipping details*/
    function shipingchk() {
        popupsection();
        departureDate();
        getdata();
        if ($("#MainContent_chkShippingfill").is(':checked')) {
            $("#MainContent_pnlbindshippping").show();
            ValidatorEnable($('[id*=reqvalerrorSA0]')[0], true);
            ValidatorEnable($('[id*=reqvalerrorSA1]')[0], true);
            ValidatorEnable($('[id*=reqvalerrorSA2]')[0], true);

            ValidatorEnable($('[id*=reqvalerrorSA7]')[0], true);
            ValidatorEnable($('[id*=reqvalerrorSA8]')[0], true);
            ValidatorEnable($('[id*=reqvalerrorSA9]')[0], true);
            ValidatorEnable($('[id*=reqvalerrorSA10]')[0], true);
            if ($("#MainContent_ddlshpMr").val() == 0) {
                $("#MainContent_ddlshpMr").prop("selectedIndex", $("#MainContent_ddlMr").prop("selectedIndex"));
            }
            if ($("#MainContent_txtshpfname").val() == '') {
                $("#MainContent_txtshpfname").val($("#txtFirst").val());
            }
            if ($("#MainContent_txtshpLast").val() == '') {
                $("#MainContent_txtshpLast").val($("#MainContent_txtLast").val());
            }
            if ($("#MainContent_txtshpEmail").val() == '') {
                $("#MainContent_txtshpEmail").val($("#MainContent_txtEmail").val());
            }
            //                if ($("#MainContent_txtshpConfirmEmail").val() == '') {
            //                    $("#MainContent_txtshpConfirmEmail").val($("#MainContent_txtConfirmEmail").val());
            //                }
            if ($("#MainContent_txtShpPhone").val() == '') {
                $("#MainContent_txtShpPhone").val($("#MainContent_txtBillPhone").val());
            }
            if ($("#MainContent_txtshpAdd").val() == '') {
                $("#MainContent_txtshpAdd").val($("#MainContent_txtAdd").val());
            }
            if ($("#MainContent_txtshpAdd2").val() == '') {
                $("#MainContent_txtshpAdd2").val($("#MainContent_txtAdd2").val());
            }
            if ($("#MainContent_txtshpCity").val() == '') {
                $("#MainContent_txtshpCity").val($("#MainContent_txtCity").val());
            }
            if ($("#MainContent_txtshpState").val() == '') {
                $("#MainContent_txtshpState").val($("#MainContent_txtState").val());
            }
            if ($("#MainContent_txtshpZip").val() == '') {
                $("#MainContent_txtshpZip").val($("#MainContent_txtZip").val());
            }
            if ($("#MainContent_ddlshpCountry").val() == '0') {
                $("#MainContent_ddlshpCountry").prop("selectedIndex", $("#MainContent_ddlCountry").prop("selectedIndex"));
            }
        }
        else {
            $("#MainContent_pnlbindshippping").hide();
            ValidatorEnable($('[id*=reqvalerrorSA0]')[0], false);
            ValidatorEnable($('[id*=reqvalerrorSA1]')[0], false);
            ValidatorEnable($('[id*=reqvalerrorSA2]')[0], false);
            ValidatorEnable($('[id*=reqvalerrorSA7]')[0], false);
            ValidatorEnable($('[id*=reqvalerrorSA8]')[0], false);
            ValidatorEnable($('[id*=reqvalerrorSA9]')[0], false);
            ValidatorEnable($('[id*=reqvalerrorSA10]')[0], false);
        }
        forloadingdeliveryoption();
    }
    function getdata() {
        $(document).ready(function () {
            initeachRequest();
        });
    }
    function initeachRequest() {
        var countPrice = 0.00;
        $('.calculateTotal').each(function () {
            var tkamount = parseFloat($(this).parent().find('.hdntkprs').val());
            var price = parseFloat($(this).parent().find('.hdnprs').val());
            if (!$(this).find('input').prop("checked")) {
                tkamount = 0;
            }
            countPrice = countPrice + (tkamount + price);
        });
        $(".shipping").each(function () {
            if ($(this).is(":checked")) {
                var str = $(this).val().split('^');
                var price = str[0];
                var smethod = str[1];
                var sdesc = str[2];
                sdesc = $('<div/>').html(sdesc).text();
                $("#MainContent_hdnShippingCost").attr('value', price.toString());
                $("#MainContent_hdnShipMethod").attr('value', smethod.toString());
                $("#MainContent_hdnShipDesc").attr('value', sdesc.toString());
            }
        });
        if (!($('#MainContent_chkShippingfill').is(':checked')) && ($("#MainContent_ucTicketDelivery_rdoBkkoingList input[type='radio']:checked").val() == "DH")) {
            $("#MainContent_hdnShippingCost").val('0.00');
        }
        else if ($('#MainContent_pnlShipping').attr('id') === undefined || $('#MainContent_pnlShipping').attr('display') == 'none') {
            $("#MainContent_hdnShippingCost").val('0.00');
        }

        var shipingamount = parseFloat($("#MainContent_hdnShippingCost").val());
        var bookingFee = parseFloat($.trim($("#lblBookingFee").text()));
        $("#lblTotal").text((countPrice + bookingFee).toFixed(2));
        countPrice = bookingFee + countPrice + shipingamount;
        var Adminfeeval = 0;
        if ($("#hdnAdminFee").val().match(/sum/i)) {
            Adminfeeval = parseFloat($("#hdnAdminFee").val().replace("sum", ''));
        }
        else if ($("#hdnAdminFee").val().match(/mult/i)) {
            Adminfeeval = parseFloat($("#hdnAdminFee").val().replace("mult", '')) * countPrice;
        }
        $("#hdnAdminFeeAmount").val(Adminfeeval.toFixed(2));
        $("#MainContent_lblAdminFee").text(Adminfeeval.toFixed(2));
        $("#MainContent_lblTotalCostWITHBooking").text(countPrice.toFixed(2));
        $("#MainContent_lblTotalCost").text((Adminfeeval + countPrice).toFixed(2));
    }
    function shippingclick(obj) {
        getdata();
    }
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function GoTicketSelectionPage() {
        window.history.back();
    }

    function checklength(id) {
        if ($("#" + id).val().length < 2) {
            $("#" + id).val('');
            alert('Please enter atleast 2 character of name');
            return false;
        }
    }
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnBookingFee" runat="server" Value="0.00" />
            <asp:HiddenField ID="hdnShipMethod" runat="server" Value="" />
            <asp:HiddenField ID="hdnShipDesc" runat="server" Value="" />
            <asp:HiddenField ID="hdnIsStaSite" runat="server" Value="False" />
            <asp:HiddenField ID="hdnIsTI" runat="server" Value="False" ClientIDMode="Static" />
            <div class="starail-Grid starail-Grid--mobileFull">
                <div class="starail-Grid-col starail-Grid-col--nopadding">
                    <div class="starail-ProgressBar starail-ProgressBar-stage--stage2 starail-u-hideMobile starail-ProgressBar-stage--stage1">
                        <div class="starail-ProgressBar-line">
                            <div class="starail-ProgressBar-progress">
                            </div>
                        </div>
                        <div class="starail-ProgressBar-stage starail-ProgressBar-stage1">
                            <div class="starail-ProgressBar-circle">
                            </div>
                            <p class="starail-ProgressBar-label">
                                Ticket Selection
                            </p>
                            <div class="starail-ProgressBar-subStage">
                            </div>
                        </div>
                        <div class="starail-ProgressBar-stage starail-ProgressBar-stage2">
                            <div class="starail-ProgressBar-circle">
                            </div>
                            <p class="starail-ProgressBar-label">
                                Booking Details
                            </p>
                            <div class="starail-ProgressBar-subStage">
                            </div>
                        </div>
                        <div class="starail-ProgressBar-stage starail-ProgressBar-stage3">
                            <div class="starail-ProgressBar-circle">
                            </div>
                            <p class="starail-ProgressBar-label">
                                Checkout
                            </p>
                        </div>
                    </div>
                    <div class="starail-Section starail-Section--nopadding">
                        <div class="starail-TrainOptions-mobileNav starail-u-hideDesktop">
                            <div>
                                <a class="starail-Button starail-Button--blue" style="font-size: 15px;" onclick="GoTicketSelectionPage();">
                                    <i class="starail-Icon-chevron-left"></i>Train Selection </a><a class="starail-Button starail-Button--blue starail-checkout-trigger"
                                        style="font-size: 15px;" id="btnCheckoutTop" runat="server" onclick="GoPaymentProcessPage();">Checkout <i class="starail-Icon-chevron-right"></i></a>
                                <!-- will submit form -->
                            </div>
                        </div>
                        <h1 class="starail-BookingDetails-title">Booking Details</h1>
                        <div class="timertopcls" style='display: none;'>
                            <div class='timercls'></div>
                            <a href='<%=siteURL %>' class="timerredirectcls" style='display: none;'>Click here</a>
                        </div>
                        <div class="starail-Form-row">
                            <asp:Panel ID="pnlErrSuccess" runat="server">
                                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                                    <asp:Label ID="lblSuccessMsg" runat="server" />
                                </div>
                                <div id="DivError" runat="server" class="error" style="display: none;">
                                    <asp:Label ID="lblErrorMsg" runat="server" />
                                </div>
                            </asp:Panel>
                        </div>
                        <!-- Tickets -->
                        <div class="starail-YourBooking">
                            <h2>Your Booking (ticket booking details page only)</h2>
                            <asp:Repeater ID="rptTrain" runat="server" OnItemDataBound="rptTrain_ItemDataBound">
                                <HeaderTemplate>
                                    <div class="starail-YourBooking-table">
                                        <div class="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--header starail-u-hideMobile">
                                            <div>
                                                Train
                                            </div>
                                            <div>
                                                Departs
                                            </div>
                                            <div>
                                                Arrives
                                            </div>
                                            <div class="starail-YourBooking-col-center">
                                                Traveller
                                            </div>
                                            <div class="starail-YourBooking-col-center" id="divHeaderTckProt" runat="server">
                                                Ticket Protection
                                            </div>
                                            <div class="starail-YourBooking-col-price">
                                                Price
                                            </div>
                                        </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="starail-YourBooking-ticketDetails">
                                        <div class="starail-YourBooking-mobileTrigger starail-u-hideDesktop">
                                            <a href="#" class=""><span id="Span1" class="starail-YourBooking-light" runat="server" visible='<%#!isRegionalBooking %>'>
                                                <%# Container.ItemIndex == 0 ? "Outbound:" : "Inbound:"%>
                                            </span>
                                                <%#Eval("P2PPassDetail.TrainNo")%>
                                                <i class="starail-Icon-chevron-up"></i></a>
                                        </div>
                                        <div class="starail-u-hideMobile">
                                            <p id="P1" class="starail-YourBooking-title" runat="server" visible="<%#!isRegionalBooking %>">
                                                <%# Container.ItemIndex == 0 ? "Outbound:" : "Inbound:"%>
                                            </p>
                                            <p>
                                                <%#Eval("P2PPassDetail.TrainNo")%>
                                            </p>
                                        </div>
                                        <div class="starail-YourBooking-col">
                                            <p class="starail-u-hideDesktop">
                                                Departs:
                                            </p>
                                            <p>
                                                <%#Eval("P2PPassDetail.DepartureTime")%>
                                                |
                                                <%#Eval("P2PPassDetail.DateTimeDepature")%>
                                            </p>
                                            <p>
                                                <%#Eval("P2PPassDetail.From")%>
                                            </p>
                                        </div>
                                        <div class="starail-YourBooking-col">
                                            <p class="starail-u-hideDesktop">
                                                Arrives:
                                            </p>
                                            <p>
                                                <%#Eval("P2PPassDetail.ArrivalTime")%>
                                                |
                                                <%#Eval("P2PPassDetail.DateTimeArrival")%>
                                            </p>
                                            <p>
                                                <%#Eval("P2PPassDetail.To")%>
                                            </p>
                                        </div>
                                        <div class="starail-YourBooking-col-center">
                                            <p>
                                                <%#Eval("Traveller")%>
                                            </p>
                                        </div>
                                        <div class="starail-YourBooking-col-center" id="divTckProt" runat="server">
                                            <p>
                                                <a data-lightbox-id="starail-ticket-info" class="js-lightboxOpen" href="#"><i class="starail-Icon-shield"></i></a>
                                                <asp:Label runat="server" ID="currsyb" Text='<%#currency%>'></asp:Label>
                                                <asp:Label ID="lbltpPrice" runat="server" Text='<%#Eval("TicketProtectionPrice") %>' />
                                                <input type="hidden" class="hdntkprs" value='<%#Eval("TicketProtectionPrice") %>' />
                                                <input type="hidden" class="hdnprs" value='<%#Eval("Price")%>' />
                                                <asp:CheckBox ID="chkTicketProtection" CssClass="starail-Form-fancyCheckbox calculateTotal bookingcheckbox"
                                                    ForeColor="White" runat="server" Checked='<%#Eval("TicketProtection")%>' Text="." />
                                            </p>
                                        </div>
                                        <div class="starail-YourBooking-col-price" style="text-align: right;">
                                            <p>
                                                <%=currency%>
                                                <asp:Label ID="lblPrice" CssClass="calculatePrice" runat="server" Text='<%#Eval("Price")%>' />
                                                <asp:Label ID="lblPassSaleID" runat="server" Text='<%#Eval("ProductID")%>' Style="display: none;" />
                                                <asp:Label ID="lblHidPriceValue" runat="server" Text='<%#Eval("Price")%>' Style="display: none;" />
                                            </p>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </div>
                                    <div class="starail-YourBooking-table">
                                        <div class="starail-YourBooking-ticketDetails booking-feep2p">
                                            <div class="starail-YourBooking-col">
                                                Booking Fee
                                            </div>
                                            <div class="starail-YourBooking-col-price" style="text-align: right;">
                                                <%=currency%><asp:Label ID="lblBookingFee" runat="server" ClientIDMode="Static" />
                                            </div>
                                        </div>
                                    </div>
                                    <p class="starail-YourBooking-totalPrice">
                                        Sub Total Price : <span class="starail-YourBooking-totalPrice-amount">
                                            <%=currency%><asp:Label ID="lblTotal" ClientIDMode="Static" runat="server"></asp:Label>
                                        </span>
                                    </p>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <!-- / Tickets -->
                        <div class="starail-BookingDetails-form" id="DivItaliaPassengers" runat="server"
                            visible="false">
                            <h2>Traveller Details</h2>
                            <asp:DataList ID="dtlTrenItaliaPassngerDetails" runat="server" RepeatColumns="1"
                                RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <ItemTemplate>
                                    <div class="starail-Form-row" style="margin-bottom: 0px">
                                        <label for="starail-firstname" class="starail-Form-label">
                                            <asp:Literal runat="server" ID="ltrPassengerType" Text='<%#Eval("PassangerType")%>'></asp:Literal>
                                            <span class="starail-Form-required">*</span></label>
                                        <div class="starail-Form-inputContainer">
                                            <div class="starail-Form-inputContainer-col paddingtop">
                                                <asp:TextBox ID="txtFirstName" runat="server" MaxLength="15" CssClass="starail-Form-input" />
                                                <asp:RequiredFieldValidator ID="reqvalerrorFirstName" runat="server" ErrorMessage="Please enter First Name."
                                                    CssClass="starail-Form-required  absolute" Display="Static" Text="*" ControlToValidate="txtFirstName"
                                                    ValidationGroup="vgs1" />
                                                <asp:FilteredTextBoxExtender ID="ftr1" runat="server" TargetControlID="txtFirstName"
                                                    ValidChars="-qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                                                <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtFirstName"
                                                    WatermarkText="First Name" WatermarkCssClass="watermark" />
                                            </div>
                                            <div class="starail-Form-inputContainer-col">
                                                <asp:TextBox ID="txtLastName" runat="server" MaxLength="25" CssClass="starail-Form-input" />
                                                <asp:RequiredFieldValidator ID="reqvalerrorLastName" runat="server" ErrorMessage="Please enter Last Name."
                                                    CssClass="starail-Form-required  absolute" Display="Static" Text="*" ControlToValidate="txtLastName"
                                                    ValidationGroup="vgs1" />
                                                <asp:FilteredTextBoxExtender ID="ftex" runat="server" TargetControlID="txtLastName"
                                                    ValidChars="-qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                                                <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtLastName"
                                                    WatermarkText="Last Name" WatermarkCssClass="watermark" />
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                        <div class="starail-BookingDetails-form">
                            <h2>Billing Address</h2>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-billing-firstname">
                                    Full name <span class="starail-Form-required">*</span></label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col starail-Form-inputContainer-col--ddInput">
                                        <asp:DropDownList ID="ddlMr" runat="server" class="starail-Form-select">
                                            <asp:ListItem Text="Title" Value="0" />
                                            <asp:ListItem Text="Dr." Value="1" />
                                            <asp:ListItem Text="Mr." Value="2" />
                                            <asp:ListItem Text="Miss" Value="3" />
                                            <asp:ListItem Text="Mrs." Value="4" />
                                            <asp:ListItem Text="Ms" Value="5" />
                                        </asp:DropDownList>
                                        <asp:TextBox ID="txtFirst" runat="server" CssClass="starail-Form-input" ClientIDMode="Static" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorBA0" runat="server" ControlToValidate="txtFirst"
                                            ErrorMessage="Please enter frist name." CssClass="starail-Form-required absolute"
                                            Display="Static" Text="*" />
                                        <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtFirst"
                                            WatermarkText="First name" WatermarkCssClass="watermark" />
                                    </div>
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtLast" runat="server" CssClass="starail-Form-input" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorBA1" runat="server" CssClass="absolute starail-Form-required"
                                            ErrorMessage="Please enter last name." Display="Static" Text="*" ControlToValidate="txtLast" />
                                        <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtLast"
                                            WatermarkText="Last name" WatermarkCssClass="watermark" />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-billing-email">
                                    Email address <span class="starail-Form-required">*</span></label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="starail-Form-input" Style='text-transform: lowercase' />
                                        <asp:RequiredFieldValidator ID="reqvalerrorBA2" runat="server" ControlToValidate="txtEmail"
                                            ErrorMessage="Please enter email." CssClass="starail-Form-required absolute"
                                            Display="Static" Text="*" />
                                        <asp:RegularExpressionValidator ID="reqcustvalerrorBA3" runat="server" CssClass="starail-Form-required absolute"
                                            ErrorMessage="Please enter valid email." ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            Display="Static" Text="*" ValidationGroup="vgs1" />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row" style="display: none;">
                                <label class="starail-Form-label" for="starail-billing-email">
                                    Confirm Email address
                                </label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtConfirmEmail" runat="server" CssClass="starail-Form-input" Style='text-transform: lowercase' />
                                        <%-- <asp:RegularExpressionValidator ID="reqcustvalerrorBA5" runat="server" ControlToValidate="txtConfirmEmail"
                                            ErrorMessage="Please enter valid confirm email." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            CssClass="starail-Form-required absolute" Display="Static" Text="*" ValidationGroup="vgs1" />--%>
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-billing-address1">
                                    Phone Number <span class="starail-Form-required">*</span></label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtBillPhone" runat="server" CssClass="starail-Form-input" MaxLength="20"
                                            onkeypress="return isNumberKey(event)" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorBA7" runat="server" ControlToValidate="txtBillPhone"
                                            ErrorMessage="Please enter phone number." CssClass="starail-Form-required absolute"
                                            Display="Static" Text="*" />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-billing-address2">
                                    Address Line 1 Or Company Name<span class="starail-Form-required">*</span></label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtAdd" runat="server" CssClass="starail-Form-input" MaxLength="50" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorBA8" runat="server" ControlToValidate="txtAdd"
                                            ErrorMessage="Please enter address." CssClass="starail-Form-required absolute"
                                            Display="Static" Text="*" />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-billing-address2">
                                    Address Line 2</label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtAdd2" runat="server" CssClass="starail-Form-input" MaxLength="50" />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-billing-town">
                                    Town / City <span class="starail-Form-required">*</span>
                                </label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtCity" runat="server" CssClass="starail-Form-input" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorBA11" runat="server" ControlToValidate="txtCity"
                                            ErrorMessage="Please enter city." CssClass="starail-Form-required absolute" Display="Static"
                                            Text="*" />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-billing-state">
                                    County / State</label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtState" runat="server" CssClass="starail-Form-input" />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-billing-postcode">
                                    Postal / Zip code <span class="starail-Form-required">*</span></label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtZip" runat="server" CssClass="starail-Form-input" MaxLength="7" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorBA9" runat="server" ControlToValidate="txtZip"
                                            ErrorMessage="Please enter postal/zip code." CssClass="starail-Form-required absolute"
                                            Display="Static" Text="*" />
                                        <asp:Label ID="lblpmsg" runat="server" Visible="false" CssClass="starail-Form-required"
                                            Text="Please enter maximum 7 character postal/zip code." />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-billing-country">
                                    Country <span class="starail-Form-required">*</span></label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:DropDownList ID="ddlCountry" runat="server" class="starail-Form-select" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorBA10" runat="server" CssClass="starail-Form-required absolute"
                                            Display="Static" Text="*" ControlToValidate="ddlCountry" InitialValue="0" ErrorMessage="Please choose country." />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row" id="div_chkShippingfill" runat="server">
                                <div class="starail-Form-spacer starail-u-hideMobile">
                                    &nbsp;
                                </div>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <label class="starail-BookingDetails-form-fancyCheckbox">
                                            <span class="starail-Form-fancyCheckbox">
                                                <input type="checkbox" class="starail-sendtoaddress" id="chkShippingfill" runat="server"
                                                    onclick="shipingchk()" name="starail-sendtoaddress" />
                                                Send my tickets to a different address <span><i class="starail-Icon-tick"></i></span>
                                            </span>
                                        </label>
                                        <asp:Label runat="server" ID="lblmsg" />
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <p>
                                Your personal details are processed in accordance with the privacy policy of InternationalRail.
                            </p>
                        </div>
                        <asp:Panel ID="pnlbindshippping" runat="server" Style="display: none;">
                            <div class="starail-BookingDetails-form">
                                <h2>Delivery Address</h2>
                                <div class="starail-Form-row">
                                    <label class="starail-Form-label" for="starail-delivery-firstname">
                                        Full name <span class="starail-Form-required">*</span></label>
                                    <div class="starail-Form-inputContainer">
                                        <div class="starail-Form-inputContainer-col starail-Form-inputContainer-col--ddInput">
                                            <asp:DropDownList ID="ddlshpMr" runat="server" class="starail-Form-select">
                                                <asp:ListItem Text="Title" Value="0" />
                                                <asp:ListItem Text="Dr." Value="1" />
                                                <asp:ListItem Text="Mr." Value="2" />
                                                <asp:ListItem Text="Miss" Value="3" />
                                                <asp:ListItem Text="Mrs." Value="4" />
                                                <asp:ListItem Text="Ms" Value="5" />
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtshpfname" runat="server" CssClass="starail-Form-input" />
                                            <asp:RequiredFieldValidator ID="reqvalerrorSA0" runat="server" ErrorMessage="Please enter delivery frist name."
                                                ControlToValidate="txtshpfname" CssClass="starail-Form-required absolute" Display="Static"
                                                Text="*" />
                                            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtshpfname"
                                                WatermarkText="First name" WatermarkCssClass="watermark" />
                                        </div>
                                        <div class="starail-Form-inputContainer-col">
                                            <asp:TextBox ID="txtshpLast" runat="server" CssClass="starail-Form-input" />
                                            <asp:RequiredFieldValidator ID="reqvalerrorSA1" runat="server" ControlToValidate="txtshpLast"
                                                ErrorMessage="Please enter delivery last name." CssClass="starail-Form-required absolute"
                                                Display="Static" Text="*" />
                                            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtshpLast"
                                                WatermarkText="Last name" WatermarkCssClass="watermark" />
                                        </div>
                                    </div>
                                </div>
                                <div class="starail-Form-row">
                                    <label class="starail-Form-label" for="starail-billing-email">
                                        Email address <span class="starail-Form-required">*</span></label>
                                    <div class="starail-Form-inputContainer">
                                        <div class="starail-Form-inputContainer-col">
                                            <asp:TextBox ID="txtshpEmail" runat="server" CssClass="starail-Form-input" Style='text-transform: lowercase' />
                                            <asp:RequiredFieldValidator ID="reqvalerrorSA2" runat="server" ControlToValidate="txtshpEmail"
                                                ErrorMessage="Please enter delivery email." CssClass="absolute starail-Form-required"
                                                Display="Static" Text="*" />
                                            <asp:RegularExpressionValidator ID="reqcustvalerrorSA3" runat="server" ErrorMessage="Please enter valid delivery email."
                                                ControlToValidate="txtshpEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                CssClass="absolute starail-Form-required" Display="Static" Text="*" ValidationGroup="vgs1" />
                                        </div>
                                    </div>
                                </div>
                                <div class="starail-Form-row" style="display: none;">
                                    <label class="starail-Form-label" for="starail-billing-email">
                                        Confirm Email address
                                    </label>
                                    <div class="starail-Form-inputContainer">
                                        <div class="starail-Form-inputContainer-col">
                                            <asp:TextBox ID="txtshpConfirmEmail" runat="server" CssClass="starail-Form-input"
                                                Style='text-transform: lowercase' />
                                            <%--  <asp:RegularExpressionValidator ID="reqcustvalerrorSA5" runat="server" ControlToValidate="txtshpConfirmEmail"
                                                ErrorMessage="Please enter valid delivery confirm email." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                CssClass="absolute starail-Form-required" Display="Static" Text="*" ValidationGroup="vgs1" />--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="starail-Form-row">
                                    <label class="starail-Form-label" for="starail-delivery-address1">
                                        Phone Number <span class="starail-Form-required">*</span></label>
                                    <div class="starail-Form-inputContainer">
                                        <div class="starail-Form-inputContainer-col">
                                            <asp:TextBox ID="txtShpPhone" runat="server" CssClass="starail-Form-input" MaxLength="20"
                                                onkeypress="return isNumberKey(event)" />
                                            <asp:RequiredFieldValidator ID="reqvalerrorSA7" runat="server" ControlToValidate="txtShpPhone"
                                                ErrorMessage="Please enter delivery phone number." CssClass="starail-Form-required absolute"
                                                Display="Static" Text="*" />
                                        </div>
                                    </div>
                                </div>
                                <div class="starail-Form-row">
                                    <label class="starail-Form-label" for="starail-delivery-address1">
                                        Address Line 1 Or Company Name <span class="starail-Form-required">*</span></label>
                                    <div class="starail-Form-inputContainer">
                                        <div class="starail-Form-inputContainer-col">
                                            <asp:TextBox ID="txtshpAdd" runat="server" CssClass="starail-Form-input" />
                                            <asp:RequiredFieldValidator ID="reqvalerrorSA8" runat="server" CssClass="absolute starail-Form-required"
                                                ErrorMessage="Please enter delivery address." Display="Static" Text="*" ControlToValidate="txtshpAdd" />
                                        </div>
                                    </div>
                                </div>
                                <div class="starail-Form-row">
                                    <label class="starail-Form-label" for="starail-delivery-address2">
                                        Address line 2</label>
                                    <div class="starail-Form-inputContainer">
                                        <div class="starail-Form-inputContainer-col">
                                            <asp:TextBox ID="txtshpAdd2" runat="server" CssClass="starail-Form-input" />
                                        </div>
                                    </div>
                                </div>
                                <div class="starail-Form-row">
                                    <label class="starail-Form-label" for="starail-delivery-town">
                                        Town / City
                                    </label>
                                    <div class="starail-Form-inputContainer">
                                        <div class="starail-Form-inputContainer-col">
                                            <asp:TextBox ID="txtshpCity" runat="server" CssClass="starail-Form-input" />
                                        </div>
                                    </div>
                                </div>
                                <div class="starail-Form-row">
                                    <label class="starail-Form-label" for="starail-delivery-state">
                                        County / State</label>
                                    <div class="starail-Form-inputContainer">
                                        <div class="starail-Form-inputContainer-col">
                                            <asp:TextBox ID="txtshpState" runat="server" CssClass="starail-Form-input" />
                                        </div>
                                    </div>
                                </div>
                                <div class="starail-Form-row">
                                    <label class="starail-Form-label" for="starail-delivery-postcode">
                                        Postal / Zip code <span class="starail-Form-required">*</span></label>
                                    <div class="starail-Form-inputContainer">
                                        <div class="starail-Form-inputContainer-col">
                                            <asp:TextBox ID="txtshpZip" runat="server" MaxLength="7" CssClass="starail-Form-input" />
                                            <asp:RequiredFieldValidator ID="reqvalerrorSA9" runat="server" CssClass="absolute starail-Form-required"
                                                ErrorMessage="Please enter delivery postal/zip code." Display="Static" Text="*"
                                                ControlToValidate="txtshpZip" />
                                        </div>
                                    </div>
                                </div>
                                <div class="starail-Form-row">
                                    <label class="starail-Form-label" for="starail-delivery-country">
                                        Country <span class="starail-Form-required">*</span></label>
                                    <div class="starail-Form-inputContainer">
                                        <div class="starail-Form-inputContainer-col">
                                            <asp:DropDownList ID="ddlshpCountry" runat="server" class="starail-Form-select" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlshpCountry_SelectedIndexChanged" />
                                            <asp:RequiredFieldValidator ID="reqvalerrorSA10" runat="server" ControlToValidate="ddlshpCountry"
                                                ErrorMessage="Please choose delivery country." CssClass="absolute starail-Form-required"
                                                Display="Static" Text="*" InitialValue="0" />
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <p>
                                    <asp:Label runat="server" ID="lblshpmsg" />
                                </p>
                            </div>
                        </asp:Panel>
                        <uc:ucTicketDelivery ID="ucTicketDelivery" runat="server" />
                        <div class="starail-BookingDetails-form" id="div_DeliverByDate_Bene" runat="server">
                            <h2>DELIVER BY DATE</h2>
                            <div class="starail-Form-row">
                                <label for="starail-firstname" class="starail-Form-label">
                                    Up to what date you are able to receive mail at your given delivery address before
                                    you leave for your trip?:
                                </label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-datePicker booking-detail-p2p">
                                        <asp:TextBox ID="txtDateOfDepature" runat="server" class="starail-Form-input" Text="DD/MM/YYYY" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorDPdATE" runat="server" ErrorMessage="Please select deliver by date."
                                            CssClass="absolute starail-Form-required" Display="Static" Text="*" ControlToValidate="txtDateOfDepature"
                                            InitialValue="DD/MM/YYYY" />
                                        <i class="starail-Icon-datepicker"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="starail-BookingDetails-form" id="div_DeliverByDate_TI" runat="server"
                            visible="false">
                            <h2>TICKET DELIVERY</h2>
                            <p style="width: 100%">
                                Please note that all Italian (Trenitalia) rail tickets are issued instantly as a
                                PDF at the end of the booking process, there is no delivery by mail option.
                            </p>
                        </div>
                        <%-- Start Evolvi --%>
                        <div class="starail-BookingDetails-form" id="div_EvolviTermCondition" runat="server"
                            style="display: none">
                            <h2>Terms and Conditions</h2>
                            <p style="width: 100%">
                                <asp:CheckBox ID="evTermCondition" runat="server" ClientIDMode="Static" />
                                I accept the <a class="ticket-Restrictions" style="text-decoration: underline;">Ticket
                                    Restrictions and Terms</a> together with the <a target="_blank" style="text-decoration: underline;"
                                        href='<%=siteURL %>Documents/NRCOC.pdf'>National Conditions of Carriage</a>
                                and I agree to purchase the items in this order.
                            </p>
                            <%-- <div class="evterm-condition" id="div_TermCondition" runat="server">
                            </div>--%>
                            <br />
                        </div>
                        <%-- End Evolvi --%>
                        <asp:Panel ID="pnlShipping" runat="server">
                            <div class="starail-BookingDetails-form">
                                <h2>Shipping Option</h2>
                                <asp:Repeater ID="rptShippings" runat="server">
                                    <ItemTemplate>
                                        <div class="starail-Form-row">
                                            <label>
                                                <span class="starail-Form-fancyRadioGroup">
                                                    <input type="radio" id="radioChkShp" class="shipping" value='<%#Eval("Price")+"^"+Eval("ShippingName")+"^"+ Server.HtmlDecode(Eval("description").ToString())%>'
                                                        name="shipping" onclick="shippingclick(this)" />
                                                    <%#Eval("ShippingName") %>
                                                    <%=currency%>
                                                    <%#Eval("Price")%>
                                                    <span><i></i></span></span>
                                            </label>
                                            <div class="starail-Form-inputContainer paddingleft">
                                                <%#Eval("description")%>
                                            </div>
                                        </div>
                                        <hr />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </asp:Panel>
                        <div class="starail-BookingDetails-form" runat="server" id="OrderDiscount">
                            <h2>TOTAL PRICE</h2>
                            <div class="starail-Form-row">
                                <div class="starail-Form-inputContainer-col" style="width: 100%">
                                    <p class="starail-YourBooking-totalPrice">
                                        Total Price: <span class="starail-YourBooking-totalPrice-amount">
                                            <%=currency%>
                                            <asp:Label ID="lblTotalCostWITHBooking" runat="server" /></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <asp:Panel ID="pnladminfee" runat="server">
                            <div class="starail-BookingDetails-form">
                                <h2>Admin Fee</h2>
                                <div class="starail-Form-row">
                                    <label>
                                        <%=currency%>
                                        <asp:Label ID="lblAdminFee" runat="server" Text="0.00" Style="margin-right: 8px;"></asp:Label>
                                        <asp:HiddenField ID="hdnAdminFeeAmount" runat="server" ClientIDMode="Static" />
                                        <asp:HiddenField ID="hdnAdminFee" runat="server" ClientIDMode="Static" />
                                    </label>
                                </div>
                            </div>
                        </asp:Panel>
                        <div class="starail-BookingDetails-form" runat="server" id="Div1">
                            <h2>GRAND TOTAL PRICE</h2>
                            <div class="starail-Form-row">
                                <div class="starail-Form-inputContainer-col" style="width: 100%">
                                    <p class="starail-YourBooking-totalPrice">
                                        Total Price: <span class="starail-YourBooking-totalPrice-amount">
                                            <%=currency%>
                                            <asp:Label ID="lblTotalCost" runat="server" /></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="starail-BookingDetails-form">
                            <div class="starail-Form-row p-desc-mad" id="div_Mandatory" runat="server" visible="false">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 chkwidth">
                                    <asp:CheckBox ID="chkMandatory" runat="server" />
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 chkwidth" style="padding: 0px;">
                                    <span class="starail-Form-required">*</span>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                    <p class="p-full-text" id="p_MandatoryText" runat="server"></p>
                                </div>
                            </div>
                            <div class="starail-Form-row p-desc-not-mad" id="div_NotMandatory" runat="server" visible="false">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 chkwidth">
                                    <asp:CheckBox ID="chkNotMandatory" runat="server" />
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 chkwidth" style="padding: 0px;">
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                    <p class="p-full-text" id="p_NotMandatoryText" runat="server"></p>
                                </div>
                            </div>
                        </div>

                        <div class="starail-BookingDetails-submit">
                            <asp:Button ID="btnCheckout" ClientIDMode="Static" runat="server" Text="Proceed to Checkout"
                                OnClientClick="shipingchk();Focusonerrorcontrol();" CssClass="starail-Button starail-Button--fullMobile  starail-Button--rightSubmit IR-GaCode"
                                Style="margin: 0px" OnClick="btnCheckout_Click" ValidationGroup="vgs1" />
                            <asp:ValidationSummary ID="vs1" runat="server" ShowMessageBox="true" ShowSummary="false"
                                DisplayMode="List" ValidationGroup="vgs1" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="starail-Lightbox" id="starail-ticket-info">
                <div class="starail-Lightbox-content">
                    <div class="starail-Lightbox-closeContainer">
                        <a href="#" class="starail-Lightbox-close js-lightboxClose"><i class="starail-Icon starail-Icon-close"></i></a>
                    </div>
                    <div class="starail-Lightbox-content-inner">
                        <div class="starail-Lightbox-text">
                            <h3 class="starail-Lightbox-textTitle">Ticket Protection</h3>
                            <div id="divpopupdata" runat="server">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="display: none;">
                <asp:HyperLink ID="HyperLink2" runat="server" />
            </div>
            <div id="pnlQuckLoad_background" class="modalBackground" style="display: none; position: fixed; left: 0px; top: 0px; z-index: 10000; width: 100%; height: 100%;"></div>
            <div id="pnlQuckLoad" style="display: none; position: fixed; z-index: 100001; left: 28%;">
                <div class="starail-popup">
                    <div class="starail-YourBooking">
                        <h2>Terms and Conditions</h2>
                        <div class="evterm-condition" id="div_TermCondition" runat="server">
                        </div>
                        <div style="text-align: center">
                            <a id="btnCloseWin" href="javascript:void(0)" class="cancelbutton">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hdnShippingCost" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDeliveryMethodValue" runat="server" />
    <asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DisplayAfter="0" DynamicLayout="True">
        <ProgressTemplate>
            <div class="modalBackground progessposition">
            </div>
            <div class="progess-inner2">
                UPDATING RESULTS...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <script type="text/javascript">
        $(document).ready(function () {
            showhideterms();
        });
        loadevent();
        forloadingdeliveryoption();

        function loadevent() {
            $(".loyradioSpan").remove();
            $(".ppbkspanradio").find('label').before("<span class='loyradioSpan' style='float:left;margin-left:10px;margin-right:10px;'><i class='radioSpancenter'></i></span>");
        }

        function GetDeliveryMethod() {
            $("#MainContent_ucTicketDelivery_rdoBkkoingList input").on('change', function () {
                forloadingdeliveryoption();
            });
        }

        function forloadingdeliveryoption() {
            var selected = $("#MainContent_ucTicketDelivery_rdoBkkoingList input[type='radio']:checked");
            if (selected.length > 0) {
                $("#MainContent_hdnDeliveryMethodValue").val(selected.val());
                if (selected.val() == "DH" && !$("#MainContent_chkShippingfill").is(":checked")) {
                    $("#MainContent_pnlShipping").hide();
                    $("#MainContent_hdnShippingCost").val('0.00');
                }
                else if (selected.val() == "TL" && !$("#MainContent_chkShippingfill").is(":checked")) {
                    $("#MainContent_pnlShipping").hide();
                    $("#MainContent_hdnShippingCost").val('0.00');
                }
                else {
                    $("#MainContent_pnlShipping").show();
                    getdata();
                }

                if (selected.val() == "TA") {
                    $("#MainContent_pnlbindshippping").hide();
                    $("#div_chkShippingfill").hide();
                    ValidatorEnable($('[id*=reqvalerrorSA0]')[0], false);
                    ValidatorEnable($('[id*=reqvalerrorSA1]')[0], false);
                    ValidatorEnable($('[id*=reqvalerrorSA2]')[0], false);
                    ValidatorEnable($('[id*=reqvalerrorSA7]')[0], false);
                    ValidatorEnable($('[id*=reqvalerrorSA8]')[0], false);
                    ValidatorEnable($('[id*=reqvalerrorSA9]')[0], false);
                    ValidatorEnable($('[id*=reqvalerrorSA10]')[0], false);
                }
                else {
                    $("#div_chkShippingfill").show();
                }
            }
        }

        function fillagentdata() {
            if ($("#MainContent_chkShippingfill").is(':checked')) {
                $("#MainContent_pnlbindshippping").show();
                ValidatorEnable($('[id*=reqvalerrorSA0]')[0], true);
                ValidatorEnable($('[id*=reqvalerrorSA1]')[0], true);
                ValidatorEnable($('[id*=reqvalerrorSA2]')[0], true);

                ValidatorEnable($('[id*=reqvalerrorSA7]')[0], true);
                ValidatorEnable($('[id*=reqvalerrorSA8]')[0], true);
                ValidatorEnable($('[id*=reqvalerrorSA9]')[0], true);
                ValidatorEnable($('[id*=reqvalerrorSA10]')[0], true);
            }
            else {
                $("#MainContent_pnlbindshippping").hide();
                ValidatorEnable($('[id*=reqvalerrorSA0]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA1]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA2]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA7]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA8]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA9]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA10]')[0], false);
            }
        }

        function showhideterms() {
            if ('<%=IsStaSite %>' == 'False') {
                var mandatoryText = '<%=MandatoryTextFirst %>';
                if (mandatoryText.length > 130) {
                    $('[id*=p_MandatoryText]').text(mandatoryText.substr(0, mandatoryText.lastIndexOf(' ', 127)) + '...');
                    mandatoryText = mandatoryText.substr(0, mandatoryText.lastIndexOf(' ', 127)) + '...';
                }
                var notMandatoryText = '<%=MandatoryTextFirstSecond %>';
                if (notMandatoryText.length > 130) {
                    $('[id*=p_NotMandatoryText]').text(notMandatoryText.substr(0, notMandatoryText.lastIndexOf(' ', 127)) + '...');
                    notMandatoryText = notMandatoryText.substr(0, notMandatoryText.lastIndexOf(' ', 127)) + '...';
                }

                $('.p-desc-mad').mouseover(function () {
                    $(this).find('.p-full-text').text('<%=MandatoryTextFirst %>');
                }).mouseout(function () {

                    $(this).find('.p-full-text').text(mandatoryText);
                });
                ;
                $('.p-desc-not-mad').mouseover(function () {
                    $(this).find('.p-full-text').text('<%=MandatoryTextFirstSecond %>');
                }).mouseout(function () {
                    $(this).find('.p-full-text').text(notMandatoryText);
                });
            }
        }

        function checkMandatory() {
            alert('Please accept the privacy statement, by clicking the tick box at the bottom of the page.');
        }
    </script>
</asp:Content>
