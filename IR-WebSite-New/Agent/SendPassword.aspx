﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SendPassword.aspx.cs" Inherits="Agent_SendPassword"
    MasterPageFile="~/STAPublic.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%=script%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnId" runat="server"></asp:HiddenField>
    <div class="starail-Login-wrapper login_interail">
        <div class="starail-Box starail-Login">
            <div class="starail-Box-content starail-Box-content--small">
                <h2 class="starail-Login-title starail-Login-title--wide">
                    Generate New Password</h2>
                <asp:Label ID="lblSuccessMsg" runat="server" CssClass="valdreq" ForeColor="#FF3300" />
                <div class="starail-Form-row starail-Form-row--hasIcon">
                    <label for="emailAddress" class="starail-Form-label starail-u-visuallyHidden">
                        Email</label>
                    <asp:TextBox ID="txtEmail" CssClass="starail-Form-input" runat="server" placeholder="Email" />
                    <asp:RequiredFieldValidator ID="reqvalerror2" ControlToValidate="txtEmail" runat="server"
                        ValidationGroup="FLoginForm" Display="Dynamic" />
                    <asp:RegularExpressionValidator ID="reqcustvalerrorBA3" runat="server" Display="Dynamic"
                        ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ValidationGroup="FLoginForm" />
                    <i class="starail-Icon-email"></i><span class="starail-Form-errorText">Please enter
                        a valid email address</span>
                </div>
                <div class="starail-Form-row starail-Login-forgottenBtn">
                    <asp:Button ID="BtnSubmit" ValidationGroup="FLoginForm" Text="Generate Password"
                        CssClass="starail-Button starail-Button--full starail-Button--login" OnClick="BtnSubmit_Click"
                        runat="server" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
