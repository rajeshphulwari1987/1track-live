﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Xml;
using Business;
using iTextSharp.text;
using iTextSharp.text.pdf;
using OneHubServiceRef;
using System.Configuration;
using System.Web.UI.WebControls;

public partial class Agentpayment : Page
{
    Guid siteId;
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly ManageBooking oBooking = new ManageBooking();
    readonly private ManageOrder _master = new ManageOrder();
    public string currency = "$";
    public string ReservationCode = null;
    public string currencyCode = "USD";
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string siteURL;
    public static Guid currencyID = new Guid();
    public string script = "<script></script>";
    public string products = "";
    public string WTpnsku = "";
    public string WTtxu = "";
    public string WTtxs = "";
    public string WTtxI = "";
    public string WTtxid = "";
    public string WTtxit = "";
    public string printUrl = "";
    public string pdfName = "";
    public string FileURL = "";
    public bool IsFirst = true;
    public string OrderNo = string.Empty;
    private string htmfile = string.Empty;
    public string PurchaseActionsTags = string.Empty;
    public string PurchaseSetActionsTags = string.Empty;
    public string SiteAlias = string.Empty;
    ManageUser mngUser = new ManageUser();
    Guid pageID;

    public List<OrderReceipt> OrderReceiptList = new List<OrderReceipt>();
    public string RUserName = "";
    public string Name = "";
    public string logo = "";
    public string img1 = "";
    public string img2 = "";
    public string img3 = "";
    ManageOneHub _ManageOneHub = new ManageOneHub();
    public string EvOtherCharges = "0";
    public bool isEvolviBooking = false;
    public bool isNTVBooking = false;
    public bool isBene = false;
    public bool isTI = false;
    public string EvolviTandC = "";
    private string HtmlFileSitelog = string.Empty;
    public string SiteHeaderColor = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request["amt"] == null || Request["oid"] == null)
            {
                Response.Redirect("~/agent/");
            }
            ShowMessage(0, null);
            if (!IsPostBack)
            {
                QubitOperationLoad();
                if (Session["AgentUserID"] == null)
                {
                    Response.Redirect("~/agent/");
                }
                else
                    hdnAgentId.Value = Session["AgentUserID"].ToString();
                GetCurrencyCode();
                var pid = _db.tblWebMenus.FirstOrDefault(x => x.Name.ToLower() == "home");
                if (pid != null)
                    pageID = pid.ID;
                if (oManageClass.IsTiApi(Convert.ToInt64(Request.Params["oid"])))
                    hdnIsTI.Value = "True";
            }

            Session.Remove("OgoneResponse");
            long order = Convert.ToInt64(Request.Params["oid"]);
            bool ISP2POrder = _db.tblOrders.Any(x => x.OrderID == order && x.TrvType == "P2P");
            if (ISP2POrder)
            {
                OrderNo = Session["P2POrderID"].ToString();
            }
            else if (Session["OrderID"] != null)
            {
                OrderNo = Session["OrderID"].ToString();
            }

            var apiTypeList = new ManageBooking().GetAllApiType(Convert.ToInt32(OrderNo));
            if (apiTypeList != null)
            {
                isEvolviBooking = apiTypeList.isEvolvi;
                isNTVBooking = apiTypeList.isNTV;
                isBene = apiTypeList.isBene;
                isTI = apiTypeList.isTI;
            }
            if (isEvolviBooking)
                EvolviTandC = siteURL + "uk-ticket-collection";

            #region If user changed query sitring data then redirct at home
            if (!IsPostBack)
            {
                if (Session["P2POrderID"] != null && ISP2POrder)
                    ViewState["tempOrder"] = Session["P2POrderID"].ToString();
                else if (Session["OrderID"] != null)
                    ViewState["tempOrder"] = Session["OrderID"].ToString();
            }
            if (ViewState["tempOrder"] != null && ViewState["tempOrder"].ToString() != Request.Params["oid"])
                Response.Redirect("~/");
            #endregion

            if (!IsPostBack)
            {
                ShowPaymentDetails(ISP2POrder);
            }
            GetSiteAlias();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void btnpaycompliteorder_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request["oid"] != null)
            {
                var objOrder = new ManageOrder();
                bool res = objOrder.IsExistAgentRefrenceNo(siteId, txtfolderno.Text.Trim());
                if (res)
                    ShowMessage(2, "STA reference number is alerady used for another devision/site");
                else
                {
                    Int32 oid = Convert.ToInt32(Request["oid"].ToString());
                    if (_db.tblOrders.Any(x => x.OrderID == oid && x.TrvType == "P2P"))
                    {
                        if (isEvolviBooking)
                        {
                            GetEvolviBookingConfirmation();
                            GetEvolviTicketInfo();
                            UpdateP2PStatus();
                        }
                        PrintTicketInfo();
                        var getDeliveryType = oBooking.getDeliveryOption(Convert.ToInt64(Session["P2POrderID"]));
                        if (getDeliveryType != null && (getDeliveryType == "Thayls Ticketless" || getDeliveryType == "TrenItaila Printing"))
                            ConfirmP2PBooking();
                    }
                    else
                    {
                        Session.Remove("CollectStation");
                        SendMail();
                    }
                    GetPrintReceipt();
                    objOrder.UpdateAgentRefrenceByOrderNo(Convert.ToInt32(Request["oid"].ToString()), txtfolderno.Text.Trim(), txtNote.InnerText);
                    Response.Redirect("~/agent/AgentPaymentConfirm.aspx?stRef=" + txtfolderno.Text.Trim() + "&oid=" + Request["oid"]);
                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void GetCurrencyCode()
    {
        try
        {
            if (Session["siteId"] != null)
            {
                siteId = Guid.Parse(Session["siteId"].ToString());
                currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
                currency = oManageClass.GetCurrency(currencyID);
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                //btnNext.Visible = false;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    private string GetSegmentNote()
    {
        List<PrintResponse> listTicket = Session["TicketBooking-REPLY"] as List<PrintResponse>;
        if (listTicket.Any(x => string.IsNullOrEmpty(x.URL)))
        {
            return "<tr><td style='font-size: 12px; border: 1px solid red;padding: 5px' valign='top' colspan='2'>" +
                            "<font face='Arial, Helvetica, sans-serif' color='red'>" + "<b>IMPORTANT:</b> This PNR is issued as a ticket on depart and must be printed from one of the self-service " +
                                "ticket machines located at all main Trenitalia stations (which are green, white and red). Once you have printed your ticket you must validate it in one of the validation " +
                                "machines located near to the entrance of each platform (they are green and white with Trenitalia written on the front and usually mounted on a wall), THIS MUST BE DONE BEFORE BOARDING YOUR TRAIN, failure to do so will result in a fine." +
                                "</font></td></tr>";

        }
        return "";
    }

    bool isBritRailPromoPass(long orderid)
    {
        try
        {
            foreach (var result in _db.tblPassSales.Where(x => x.OrderID == orderid).ToList())
            {
                bool isPromo = (_db.tblProducts.Any(x => x.ID == result.ProductID && x.IsPromoPass && x.tblProductCategoriesLookUps.Any(y => y.ProductID == result.ProductID && y.tblCategory.IsBritRailPass)));
                if (isPromo)
                    return true;
            }
            return false;
        }
        catch
        {
            return false;
        }

    }

    public string GetCCEmailID()
    {
        try
        {
            string ccEmail = string.Empty;
            if (Session["AgentUserID"] != null)
            {
                var agentId = Guid.Parse(Session["AgentUserID"].ToString());
                var result = _masterPage.GetAgentOfficeEmailID(agentId).FirstOrDefault();
                if (result != null)
                {
                    if (result.chkEmail && result.Email != string.Empty)
                        ccEmail = result.Email + ",";
                    if (result.chkSecondaryEmail && result.SecondaryEmail != string.Empty)
                        ccEmail = ccEmail + result.SecondaryEmail;
                }
            }
            return ccEmail.TrimEnd(',');
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void GetSiteAlias()
    {
        SiteAlias = oBooking.GetSiteAliasByID(siteId);
    }

    public void QubitOperationLoad()
    {
        try
        {
            List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
            var res = lstQbit.FirstOrDefault();
            if (res != null)
                script = res.Script;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public String GetJourney(string ProductType)
    {
        if (Session["TIRegionalBooking"] != null)
            return "";

        if (ProductType == "P2P")
        {
            if (IsFirst)
            {
                IsFirst = false;
                return "OUTBOUND:";
            }
            else
                return "INBOUND:";
        }
        return string.Empty;
    }

    public void ShowPaymentDetails(bool ISP2POrder)
    {
        try
        {
            if (!string.IsNullOrEmpty(OrderNo))
            {
                var lst = oBooking.GetAllCartData(Convert.ToInt64(OrderNo)).ToList();
                var lstNew = (from a in lst
                              select new
                              {
                                  PassSaleID = a.PassSaleID,
                                  ProductDesc = a.ProductType != "P2P" ? oBooking.GetProductName(a.PassSaleID) : string.Empty,
                                  Traveller = a.ProductType != "P2P" ? oBooking.GetTravellerName(a.PassSaleID) : string.Empty,
                                  Countries = oBooking.GetPassCountries(a.PassSaleID),
                                  //string.IsNullOrEmpty(oBooking.GetPassCountries(a.PassSaleID)) ? oBooking.GetCountryByIDs(a.Country) : oBooking.GetPassCountries(a.PassSaleID),
                                  Validity = a.ProductType != "P2P" ? oBooking.GetPassValidity(a.PassSaleID) : string.Empty,
                                  Protected = (a.TicketProtection.HasValue ? (a.TicketProtection.Value > 0 ? "Yes" : "No") : "No"),
                                  TKProtected = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0),
                                  ProtectedAmount = (a.TicketProtection.HasValue ? a.TicketProtection.Value > 0 ? currency + a.TicketProtection.Value : string.Empty : string.Empty),
                                  Trvname = a.ProductType != "P2P" ? a.Trvname : oBooking.GetP2PTravellerName(a.PassSaleID),
                                  Passengerinfo = a.Title + " " + a.FirstName + " " + a.MiddleName + " " + a.LastName.Split('<')[0],
                                  Price = (oBooking.getPrice(a.PassSaleID, a.ProductType)),
                                  Qty = 1,
                                  Arrives = a.ProductType == "P2P" ? oBooking.GetP2PArrives(a.PassSaleID) : string.Empty,
                                  Departs = a.ProductType == "P2P" ? oBooking.GetP2PDeparts(a.PassSaleID) : string.Empty,
                                  ArrivesStation = a.ProductType == "P2P" ? oBooking.GetP2PArrivesStation(a.PassSaleID) : string.Empty,
                                  DepartsStation = a.ProductType == "P2P" ? oBooking.GetP2PDepartsStation(a.PassSaleID) : string.Empty,
                                  Train = a.ProductType == "P2P" ? oBooking.GetP2PTrain(a.PassSaleID) : string.Empty,
                                  Class = a.ProductType == "P2P" ? oBooking.GetP2PClass(a.PassSaleID) : string.Empty,
                                  ServiceName = a.ProductType == "P2P" ? oBooking.GetP2PService(a.PassSaleID) : string.Empty,
                                  OrderIdentity = a.OrderIdentity,
                                  Jolurny = GetJourney(a.ProductType)
                              }).ToList().OrderBy(t => t.OrderIdentity).ToList();

                if (lst.Count > 0)
                {
                    if (!ISP2POrder)
                    {
                        RptPassDetails.DataSource = lstNew;
                        RptPassDetails.DataBind();
                        DivP2PSale.Visible = false;
                    }
                    else
                    {
                        RptP2PDetails.DataSource = lstNew;
                        RptP2PDetails.DataBind();
                        DivPassSale.Visible = false;
                    }
                    lblEmailAddress.Text = lst.FirstOrDefault().EmailAddress;
                    lblOrderDate.Text = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                    lblOrderNumber.Text = lst.FirstOrDefault().OrderID.ToString();
                    lblShippingAmount.Text = (lst.FirstOrDefault().ShippingAmount ?? 0).ToString("F2");
                    lblDeliveryAddress.Text = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName + ", " + lst.FirstOrDefault().Address1 + ", " + (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2) ? lst.FirstOrDefault().Address2 + ", " : string.Empty) + "<br/>" + (!string.IsNullOrEmpty(lst.FirstOrDefault().City) ? lst.FirstOrDefault().City + ", " : string.Empty) + (!string.IsNullOrEmpty(lst.FirstOrDefault().State) ? lst.FirstOrDefault().State + ", " : string.Empty) + lst.FirstOrDefault().DCountry + ", " + lst.FirstOrDefault().Postcode;
                    lblDeliveryAddress.Text = lblDeliveryAddress.Text.Replace("  , , <br/>, ", string.Empty);
                    lblBookingFee.Text = Convert.ToDecimal(lst.FirstOrDefault().BookingFee).ToString("F2");
                    lblDiscount.Text = _master.GetOrderDiscountByOrderId(Convert.ToInt64(OrderNo));
                    lblAdminFee.Text = new ManageAdminFee().GetOrderAdminFeeByOrderID(Convert.ToInt32(OrderNo)).ToString();
                    if (lblAdminFee.Text == "0.00")
                    {
                        hidelblAdminfee.Attributes.Add("style", "display:none");
                        lblAdminFee.Text = "0.00";
                    }
                    lblNetTotal.Text = (Convert.ToDecimal(lblAdminFee.Text) + (lstNew.Sum(a => a.Price) + lstNew.Sum(a => a.TKProtected)) + (lst.FirstOrDefault().ShippingAmount ?? 0) + (lst.FirstOrDefault().BookingFee ?? 0) + (!string.IsNullOrEmpty(EvOtherCharges) ? Convert.ToDecimal(EvOtherCharges) : 0)).ToString();
                    lblGrandTotal.Text = currency + "" + ((Convert.ToDecimal(lblNetTotal.Text) - Convert.ToDecimal(lblDiscount.Text)).ToString("F2"));
                    btnpaycompliteorder.Text = "Confirm payment of " + currency + "" + ((Convert.ToDecimal(lblNetTotal.Text) - Convert.ToDecimal(lblDiscount.Text)).ToString("F2"));
                    var sa = lstNew.GroupBy(t => new { t.Trvname }).Select(t => new
                    {
                        Qty = t.Sum(x => Convert.ToInt32(x.Qty)) + "",
                        TravellerName = t.Key.Trvname
                    });
                }
            }
            else
            {
                Response.Redirect("~/Home", false);
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    #region Print & confirm P2P ticket

    private void PrintTicketInfo()
    {
        try
        {
            if (Session["BOOKING-REPLY"] == null)
                return;

            var list = Session["BOOKING-REPLY"] as List<BookingResponse>;
            if (list.Any(x => x.PinCode != null))
            {
                if (Session["P2POrderID"] != null)
                {
                    var chkDeliveryType = oBooking.CheckDilveryOptionType(Convert.ToInt64(Session["P2POrderID"]));
                    if (chkDeliveryType)
                        Printickets();
                    else
                    {
                        TicketBooking();
                        SendMail();
                    }
                }
            }
            else
                PrintTicketItalia();
        }
        catch { }

    }

    public void Printickets()
    {
        if (Session["BOOKING-REPLY"] != null)
        {
            var bookingList = Session["BOOKING-REPLY"] as List<BookingResponse>;
            if (bookingList == null)
                return;
            var client = new OneHubRailOneHubClient();
            var listResponse = new List<PrintResponse>();
            foreach (var item in bookingList)
            {
                #region Ticket Print
                var objHeader = new OneHubServiceRef.Header
                {
                    onehubusername = "#@dots!squares",
                    onehubpassword = "#@dots!squares",
                    unitofwork = 0,
                    language = Language.nl_BE
                };

                #region TicketBookingRequest
                var objRequest = new TicketBookingRequest { Header = objHeader, ReservationCode = item.ReservationCode };
                _ManageOneHub.TiBeneApiLogin(objRequest, siteId);
                var tktBooking = client.TicketBookingRequest(objRequest);
                oBooking.UpdatePinNumberByReservationCode(item.ReservationCode, tktBooking != null ? (!string.IsNullOrEmpty(tktBooking.PinCode) ? tktBooking.PinCode : item.PinCode) : item.PinCode);
                #endregion

                var printRequest = new TicketPrintRequest
                {
                    Header = objHeader,
                    BillingAddress = item.BillingAddress != null ? new BillingAddress
                    {
                        Address = item.BillingAddress.Address,
                        City = item.BillingAddress.City,
                        Country = item.BillingAddress.Country,
                        CrisNumber = item.BillingAddress.CrisNumber,
                        DeliveryDate = item.BillingAddress.DeliveryDate,
                        Email = item.BillingAddress.Email,
                        FirstName = item.BillingAddress.FirstName,
                        Lastname = item.BillingAddress.Lastname,
                        Phone = item.BillingAddress.Phone,
                        State = item.BillingAddress.State,
                        ZipCode = item.BillingAddress.ZipCode
                    } : null,
                    ReservationCode = item.ReservationCode,
                    ChangeReservationCode = item.ChangeReservationCode,
                    PinCode = tktBooking != null ? (!string.IsNullOrEmpty(tktBooking.PinCode) ? tktBooking.PinCode : item.PinCode) : item.PinCode,
                    TicketType = "PDF_DT",
                    ApiInformation = new ApiInformation
                    {
                        ApiName = GetApiName()
                    }
                };

                _ManageOneHub.TiBeneApiLogin(printRequest, siteId);
                var printResponse = client.TicketPrint(printRequest);
                if (printResponse != null)
                {
                    listResponse.Add(new PrintResponse
                    {
                        ReservationCode = printRequest.ReservationCode,
                        status = String.IsNullOrEmpty(printResponse.PrintIndication) ? "False" : printResponse.PrintIndication,
                        URL = String.IsNullOrEmpty(printResponse.TicketUrl) ? "Ticket print URL not found." : printResponse.TicketUrl
                    });
                }
                #endregion
            }

            var newList = new List<PrintResponse>();
            foreach (var resp in listResponse)
            {
                if (resp != null)
                {
                    printUrl = resp.URL;
                    if (resp.URL.Contains("pdf"))
                        printUrl = BeNePdfService(printUrl, resp.ReservationCode);

                    if (!string.IsNullOrEmpty(printUrl))
                        oBooking.UpdateTicketUrlByReservationCode(resp.ReservationCode, printUrl);

                    newList.Add(new PrintResponse { ReservationCode = resp.ReservationCode, status = resp.status, URL = printUrl });
                }
            }
            Session["TicketBooking-REPLY"] = newList;

            if (listResponse.Count != 0)
                SendMail();
            else
                ShowMessage(2, "Ticket Print not Available");
        }
    }

    public string BeNePdfService(string url, string resCode)
    {
        try
        {
            resCode = resCode + Guid.NewGuid().ToString();
            string outputFile = Server.MapPath("~/PdfService/" + resCode + ".pdf");
            var webClient = new WebClient();
            using (var webStream = webClient.OpenRead(url))
            using (var fileStream = new FileStream(outputFile, FileMode.Create))
            {
                var buffer = new byte[32768];
                int bytesRead;

                while (webStream != null && (bytesRead = webStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    fileStream.Write(buffer, 0, bytesRead);
                }
            }
            string siteURL = _oWebsitePage.GetSiteURLbySiteId(siteId);
            return siteURL + "PdfService/" + resCode + ".pdf";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void PrintTicketItalia()
    {
        try
        {
            var newList = new List<PrintResponse>();
            if (Session["BOOKING-REPLY"] != null)
            {
                var list = Session["BOOKING-REPLY"] as List<BookingResponse>;
                foreach (var item in list)
                    if (item.PdfUrl != null)
                    {
                        string newUrl = "";
                        foreach (var url in item.PdfUrl)
                        {
                            newUrl += url + "#";
                        }
                        oBooking.UpdateTicketUrlByReservationCode(item.ReservationCode, newUrl);
                        newList.Add(new PrintResponse { ReservationCode = item.ReservationCode, status = "PRINT REQUEST SUBMITTED", URL = newUrl });
                    }
            }
            Session["TicketBooking-REPLY"] = newList;
            SendMail();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public bool ConfirmP2PBooking()
    {
        bool result = false;
        if (Session["BOOKING-REPLY"] == null)
            return result;

        var client = new OneHubRailOneHubClient();
        var listbookingReply = Session["BOOKING-REPLY"] as List<BookingResponse>;

        foreach (var item in listbookingReply)
        {
            var objResponse = new ConfirmResponse();
            var objHeader = new OneHubServiceRef.Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = item.UnitOfWork,
                language = Language.nl_BE,
            };

            var objRequest = new ConfirmRequest { Header = objHeader, ReservationCode = item.ReservationCode, ApiInformation = new ApiInformation { ApiName = GetApiName() } };
            _ManageOneHub.TiBeneApiLogin(objRequest, siteId);
            objResponse = client.Confirm(objRequest);

            if (objResponse != null)
            {
                string res = objResponse.ConfirmOk;
                result = res == "Ok" || res == "True";
            }

            oBooking.UpdateReservationCode(Convert.ToInt32(Session["P2POrderID"]), item.ReservationCode, result);
            oBooking.UpdatePinNumberByReservationCode(item.ReservationCode, string.IsNullOrEmpty(objResponse.PinCode) ? item.UnitOfWork.ToString() : objResponse.PinCode);
        }
        return result;
    }

    public void TicketBooking()
    {

        try
        {
            var listbookingReply = Session["BOOKING-REPLY"] as List<BookingResponse>;
            var item = listbookingReply.FirstOrDefault();
            var objHeader = new OneHubServiceRef.Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = 0,
            };

            var client = new OneHubRailOneHubClient();
            var objRequest = new TicketBookingRequest { Header = objHeader, ReservationCode = item.ReservationCode };
            _ManageOneHub.TiBeneApiLogin(objRequest, siteId);
            var tktBooking = client.TicketBookingRequest(objRequest);
            oBooking.UpdatePinNumberByReservationCode(item.ReservationCode, tktBooking != null ? tktBooking.PinCode : item.PinCode);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    public void GetEvolviTicketInfo()
    {
        try
        {
            var evChargesList = oBooking.GetEvolviChargesByOrderId(Convert.ToInt64(OrderNo));
            if (evChargesList != null)
            {
                EvOtherCharges = ((evChargesList.TicketChange + evChargesList.CreditCardCharge) - (evChargesList.Discount)).ToString("F2");
                lblOtherCharges.Text = EvOtherCharges;
                otherchargesText.Visible = true;
                if (!string.IsNullOrEmpty(evChargesList.BookingRef) && !string.IsNullOrEmpty(evChargesList.TODRef))
                {
                    div_BookingRef.Visible = true;
                    lblBookingRef.Text = evChargesList.BookingRef;
                    lblTODRef.Text = evChargesList.TODRef;
                }
            }
            else
                otherchargesText.Visible = false;
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void GetEvolviBookingConfirmation()
    {
        try
        {
            if (Session["BOOKING-REQUEST"] != null)
            {
                var bookingList = Session["BOOKING-REQUEST"] as List<BookingRequest>;
                if (bookingList != null)
                {
                    foreach (var item in bookingList)
                    {
                        if (item.EvolviBookingRequest != null)
                        {
                            var Client = new OneHubRailOneHubClient();
                            RailBookingRequest request = item.EvolviBookingRequest;
                            request.Result = true;
                            _ManageOneHub.ApiLogin(request, siteId);
                            RailBookingResponse response = Client.EvolviRailBooking(request);
                            if (response != null)
                            {
                                if (response.ErrorMessage == null)
                                {
                                    var evBookingInfoList = response.BookingReservationInfos != null ? response.BookingReservationInfos.Select(x => x.TPA_ExtensionsBookingInfoList != null ? x.TPA_ExtensionsBookingInfoList : null).FirstOrDefault() : null;
                                    if (evBookingInfoList != null && !string.IsNullOrEmpty(OrderNo))
                                        oBooking.UpdateEvolviBookingRef(Convert.ToInt64(OrderNo), evBookingInfoList.OrderRef, evBookingInfoList.OrderItemRef, evBookingInfoList.TODRef);
                                }
                                else
                                    Response.Redirect("~/agent/");
                            }
                            else
                                Response.Redirect("~/agent/");
                        }
                    }
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void UpdateP2PStatus()
    {
        try
        {
            if (!string.IsNullOrEmpty(OrderNo))
            {
                var lst = oBooking.GetAllCartData(Convert.ToInt64(OrderNo)).ToList();
                if (lst != null && lst.Count > 0)
                    foreach (var item in lst)
                    {
                        bool sts = item.PassSaleID.HasValue ? oBooking.UpdateP2PSaleStatus(item.PassSaleID.Value) : false;
                    }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public bool SendMail()
    {
        bool retVal = false;
        try
        {
            var orderID = Convert.ToInt32(OrderNo);
            bool ISP2POrder = _db.tblOrders.Any(x => x.OrderID == orderID && x.TrvType == "P2P");
            bool isDiscountSite = _master.GetOrderDiscountVisibleBySiteId(siteId);
            var smtpClient = new SmtpClient();
            var message = new MailMessage();
            var st = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
            if (st != null)
            {
                string SiteName = st.SiteURL + "Home";
                currency = oManageClass.GetCurrency(st.DefaultCurrencyID.HasValue ? (st.DefaultCurrencyID.Value) : new Guid());
                string Subject = "Order Confirmation #" + orderID;
                string dir = HttpContext.Current.Request.PhysicalApplicationPath;

                ManageEmailTemplate objMailTemp = new ManageEmailTemplate();
                htmfile = Server.MapPath("~/MailTemplate/MailTemplate.htm");
                HtmlFileSitelog = string.IsNullOrEmpty(objMailTemp.GetMailTemplateBySiteId(siteId)) ? "IRMailTemplate.htm" : Server.MapPath(objMailTemp.GetMailTemplateBySiteId(siteId));

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(htmfile);
                var list = xmlDoc.SelectNodes("html");
                string body = list[0].InnerXml.ToString();
                string UserName = string.Empty;
                string EmailAddress = string.Empty;
                string DeliveryAddress = string.Empty;
                string BillingAddress = string.Empty;
                string OrderDate = string.Empty;
                string OrderNumber = string.Empty;
                string Total = string.Empty;
                string ShippingAmount = string.Empty;
                string GrandTotal = string.Empty;
                string BookingFee = string.Empty;
                string NetTotal = string.Empty;
                string Discount = string.Empty;
                string AdminFee = string.Empty;
                string OtherCharges = string.Empty;
                string BookingRefNO = string.Empty;
                string PassProtection = "0.00";
                string PassProtectionHtml = "";
                string DiscountHtml = "";
                string PassNetTotal = "0.00";
                string PassNetTotalHtml = string.Empty;
                bool isBeneDeliveryByMail = false;

                var lst = oBooking.GetAllCartData(Convert.ToInt64(OrderNo)).OrderBy(a => a.OrderIdentity).ToList();
                var lst1 = (from a in lst
                            select new
                            {
                                Price = (oBooking.getPrice(a.PassSaleID, a.ProductType)),
                                ProductDesc = (oBooking.getPassDesc(a.PassSaleID, a.ProductType) + "<br>" + a.FirstName + " " + a.MiddleName + " " + a.LastName),
                                TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0)
                            }).ToList();
                if (lst.Count > 0)
                {
                    EmailAddress = lst.FirstOrDefault().EmailAddress;
                    UserName = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName;
                    DeliveryAddress = UserName + "<br>" +
                        lst.FirstOrDefault().Address1 + "<br>" +
                      (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2) ? lst.FirstOrDefault().Address2 + "<br>" : string.Empty) +
                      (!string.IsNullOrEmpty(lst.FirstOrDefault().City) ? lst.FirstOrDefault().City + "<br>" : string.Empty) +
                      (!string.IsNullOrEmpty(lst.FirstOrDefault().State) ? lst.FirstOrDefault().State + "<br>" : string.Empty) +
                      lst.FirstOrDefault().DCountry + "<br>" +
                    lst.FirstOrDefault().Postcode + "<br>" +
                     GetBillingAddressPhoneNo(Convert.ToInt64(OrderNo));

                    OrderDate = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                    OrderNumber = lst.FirstOrDefault().OrderID.ToString();
                    Total = (lst1.Sum(a => a.Price)).ToString();
                    ShippingAmount = lst.FirstOrDefault().ShippingAmount.ToString();
                    BookingFee = lst.FirstOrDefault().BookingFee.ToString();
                    Discount = _master.GetOrderDiscountByOrderId(Convert.ToInt64(orderID));
                    AdminFee = new ManageAdminFee().GetOrderAdminFeeByOrderID(Convert.ToInt32(OrderNo)).ToString();
                    NetTotal = ((lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)) +
                        (lst.FirstOrDefault().ShippingAmount.HasValue ? lst.FirstOrDefault().ShippingAmount.Value : 0) +
                        (lst.FirstOrDefault().BookingFee.HasValue ? lst.FirstOrDefault().BookingFee.Value : 0) +
                        Convert.ToDecimal(AdminFee) + Convert.ToDecimal(EvOtherCharges)).ToString();
                    PassNetTotal = (lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)).ToString();
                    GrandTotal = (Convert.ToDecimal(NetTotal) - Convert.ToDecimal(Discount)).ToString("F2");

                    var billAddress = oBooking.GetBillingShippingAddress(Convert.ToInt64(orderID));
                    if (billAddress != null)
                    {
                        BillingAddress = (!string.IsNullOrEmpty(billAddress.Address1) ? billAddress.Address1 + "<br>" : string.Empty) +
                            (!string.IsNullOrEmpty(billAddress.Address2) ? billAddress.Address2 + "<br>" : string.Empty) +
                            (!string.IsNullOrEmpty(billAddress.City) ? billAddress.City + "<br>" : string.Empty) +
                            (!string.IsNullOrEmpty(billAddress.State) ? billAddress.State + "<br>" : string.Empty) +
                            (!string.IsNullOrEmpty(billAddress.Postcode) ? billAddress.Postcode + "<br>" : string.Empty) +
                            (!string.IsNullOrEmpty(billAddress.Country) ? billAddress.Country + "<br>" : string.Empty);
                    }
                }
                // to address
                string ToEmail = EmailAddress;
                GetReceiptLoga();
                body = body.Replace("##headerstyle##", SiteHeaderColor);
                body = body.Replace("##sitelogo##", logo);
                body = body.Replace("##OrderNumber##", OrderNumber);
                body = body.Replace("##UserName##", UserName.Trim());
                body = body.Replace("##EmailAddress##", EmailAddress.Trim());
                body = body.Replace("##OrderDate##", OrderDate.Trim());
                body = body.Replace("##Items##", currency + " " + Total.Trim());
                body = body.Replace("##BookingCondition##", st.SiteURL + "Booking-Conditions");
                ////*NET Price*/

                #region receipt
                RUserName = UserName;
                OrderReceiptList.Add(new OrderReceipt { Id = 1, Name = "Order Id", Value = OrderNumber });
                OrderReceiptList.Add(new OrderReceipt { Id = 2, Name = "Name", Value = UserName });
                OrderReceiptList.Add(new OrderReceipt { Id = 3, Name = "Billing Address", Value = string.IsNullOrEmpty(DeliveryAddress) ? "" : DeliveryAddress.Replace("<br> ", "\n").Replace("<br>", "\n") });
                OrderReceiptList.Add(new OrderReceipt { Id = 4, Name = "Order Summary:", Value = "" });
                OrderReceiptList.Add(new OrderReceipt { Id = 5, Name = "Order Date", Value = OrderDate });
                OrderReceiptList.Add(new OrderReceipt { Id = 220, Name = "Subtotal", Value = currency + " " + Total });
                #endregion


                var prdPass = _master.GetPassSale(orderID);
                decimal tckPCharge = prdPass.Sum(ty => ty.TicketProtection);
                var p2p = _master.GetP2PTckP(orderID);
                decimal p2PTckPCharge = p2p.Sum(tp => tp.TicketProtection);
                var nettPrice = "";
                if (ISP2POrder)
                    nettPrice = currency + " " + (Convert.ToDecimal(Total) - Convert.ToDecimal(p2PTckPCharge)).ToString();

                #region receipt
                OrderReceiptList.Add(new OrderReceipt { Id = 1, Name = "Nett Price", Value = nettPrice });
                #endregion

                //body = body.Replace("##NettPrice##", "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Net Price:</td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + nettPrice + "</td></tr>");


                if (isEvolviBooking)
                {
                    OtherCharges = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Other Charges:</td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + EvOtherCharges + "</td></tr>";
                    BookingRefNO = "<tr><td align='left' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong>Booking Ref: </strong></td><td align='left' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'>" + lblBookingRef.Text.Trim() + "</td></tr>";
                    #region receipt
                    OrderReceiptList.Add(new OrderReceipt { Id = 0, Name = "Booking Ref", Value = lblBookingRef.Text });
                    #endregion
                }

                body = body.Replace("##BookingRef##", BookingRefNO);
                body = body.Replace("##Shipping##", currency + " " + ShippingAmount.Trim());
                body = body.Replace("##DeliveryFee##", currency + " " + ShippingAmount.Trim());
                body = body.Replace("##BookingFee##", currency + " " + BookingFee.Trim());
                body = body.Replace("##Total##", currency + " " + GrandTotal.Trim());

                #region receipt
                OrderReceiptList.Add(new OrderReceipt { Id = 221, Name = "Shipping", Value = currency + " " + ShippingAmount });
                OrderReceiptList.Add(new OrderReceipt { Id = 222, Name = "Booking Fee", Value = currency + " " + BookingFee });
                OrderReceiptList.Add(new OrderReceipt { Id = 227, Name = "Order Total", Value = currency + " " + GrandTotal });
                #endregion

                string AdminFeeRow = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Admin Fee: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + AdminFee.Trim() + "</td></tr>";

                if (AdminFee == "0.00")
                    AdminFeeRow = string.Empty;
                if (isDiscountSite)
                {
                    body = body.Replace("##AdminFee##", AdminFeeRow);
                    PassNetTotalHtml = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Net Total: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + PassNetTotal.Trim() + "</td></tr>";
                    if (Discount == "0.00")
                        body = body.Replace("##Discount##", "");
                    else
                    {
                        DiscountHtml = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Discount: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + Discount.Trim() + "</td></tr>";
                        body = body.Replace("##Discount##", DiscountHtml);
                    }

                    #region receipt
                    if (AdminFee != "0.00")
                        OrderReceiptList.Add(new OrderReceipt { Id = 224, Name = "Admin Fee", Value = currency + " " + AdminFee });
                    OrderReceiptList.Add(new OrderReceipt { Id = 225, Name = "Net Total", Value = currency + " " + NetTotal });
                    OrderReceiptList.Add(new OrderReceipt { Id = 226, Name = "Discount", Value = currency + " " + Discount });
                    #endregion
                }
                else
                {
                    body = body.Replace("##AdminFee##", AdminFeeRow);
                    body = body.Replace("##Discount##", "");
                }

                var P2PSaleList = new ManageBooking().GetP2PSaleListByOrderID(orderID);
                if (isEvolviBooking)
                    body = body.Replace("##ShippingMethod##", "Ticket on collection");
                else if (isBene)
                {
                    if (P2PSaleList != null && !string.IsNullOrEmpty(P2PSaleList.FirstOrDefault().DeliveryOption))
                    {
                        string shippingdesc = (Session["ShipMethod"] != null ? Session["ShipMethod"].ToString() : "") + (Session["ShipDesc"] != null ? (Server.HtmlDecode(Session["ShipDesc"].ToString())) : "");
                        if (P2PSaleList.Any(x => x.DeliveryOption.ToLower() == "delivery by mail"))
                        {
                            body = body.Replace("##ShippingMethod##", !string.IsNullOrEmpty(shippingdesc) ? shippingdesc : "Delivery by mail");
                            isBeneDeliveryByMail = true;
                        }
                        else
                            body = body.Replace("##ShippingMethod##", P2PSaleList != null ? P2PSaleList.FirstOrDefault().DeliveryOption : "");
                    }
                }
                else
                {
                    string britrailpromoTerms = isBritRailPromoPass(orderID) ? "BritRail Freeday Promo pass <a href='" + st.SiteURL + "BritRailFreedayPromoTerms.aspx' traget='_blank'> Click Here </a>" : "";
                    if (Session["ShipMethod"] != null)
                    {
                        string shippingdesc = (Session["ShipMethod"] != null ? Session["ShipMethod"].ToString() : "") + (Session["ShipDesc"] != null ? (Server.HtmlDecode(Session["ShipDesc"].ToString())) : "");
                        body = body.Replace("##ShippingMethod##", shippingdesc + britrailpromoTerms);
                    }
                    else
                        body = body.Replace("##ShippingMethod##", britrailpromoTerms);
                }

                body = body.Replace("#Blanck#", "&nbsp;");
                var PrintResponselist = Session["TicketBooking-REPLY"] as List<PrintResponse>;
                List<GetAllCartData_Result> lstC = oBooking.GetAllCartData(Convert.ToInt64(OrderNo)).ToList();
                var lstNew = (from a in lstC
                              select new
                              {
                                  a,
                                  Price = (oBooking.getPrice(a.PassSaleID, a.ProductType)),
                                  ProductDesc = a.ProductType == "P2P"
                                  ? (oBooking.getP2PDetailsForEmail(a.PassSaleID, PrintResponselist, (string)(Session["TrainType"]), isEvolviBooking, true, EvolviTandC))
                                  : (oBooking.getPassDetailsForEmail(a.PassSaleID, PrintResponselist, (string)(Session["TrainType"]), EvolviTandC)),
                                  ReceiptProductDetail = (oBooking.GetReceiptPassDescMail(a.PassSaleID, a.ProductType, PrintResponselist, (string)(Session["TrainType"]))),
                                  TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0),
                                  Terms = a.terms
                              }).ToList();
                string strProductDesc = "";
                int i = 1;
                int newid = 5;
                if (lstNew.Count() > 0)
                {
                    lstNew = lstNew.OrderBy(ty => ty.a.OrderIdentity).ToList();
                    if (lstNew.Any(x => x.a.ProductType == "P2P"))
                    {
                        if (isBeneDeliveryByMail)
                        {
                            body = body.Replace("##HeaderText##", "Delivery address");
                            body = body.Replace("##HeaderDeliveryText##", "Your order will be sent to:");
                            body = body.Replace("##DeliveryAddress##", DeliveryAddress.Trim());
                        }
                        else
                        {
                            body = body.Replace("##HeaderText##", "Tickets");
                            body = body.Replace("##HeaderDeliveryText##", "Your ticket collection reference:");
                            body = body.Replace("##DeliveryAddress##", oBooking.getP2PDetailsForEmailTicket(orderID, EvolviTandC, siteURL + "images/pdf-istock.jpg"));
                        }
                    }
                    else
                    {
                        body = body.Replace("##HeaderText##", "Delivery address");
                        body = body.Replace("##HeaderDeliveryText##", "Your order will be sent to:");
                        body = body.Replace("##DeliveryAddress##", DeliveryAddress.Trim());
                    }

                    foreach (var x in lstNew)
                    {
                        if (x.a.ProductType != "P2P")
                        {
                            nettPrice = currency + " " + (_master.GetEurailAndNonEurailPrice(x.a.PassSaleID.Value)).ToString("F");
                            strProductDesc += "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth' align='center' bgcolor='#eeeeee'><tr><td bgcolor='#ffffff' height='5'></td></tr><tr><td style='padding: 10px;' align='center' valign='top' class='pad'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' class='deviceWidth '>";
                            strProductDesc += x.ProductDesc.Replace("##Price##", currency + " " + x.Price.ToString()).Replace("##Name##", x.a.Title + " " + x.a.FirstName + (!string.IsNullOrEmpty(x.a.MiddleName) ? " " + x.a.MiddleName : "") + " " + x.a.LastName).Replace("##NetAmount##", "Net Amount :" + nettPrice);

                            strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Pass start date:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + (x.a.PassStartDate.HasValue ? x.a.PassStartDate.Value.ToString("dd/MMM/yyyy") : string.Empty) + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>";
                            strProductDesc += (Session["CollectStation"] != null ? "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Collect at ticket desk:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + Session["CollectStation"].ToString() + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>" : "");

                            string ElectronicNote = oBooking.GetElectronicNote(x.a.PassSaleID.Value);
                            if (ElectronicNote.Length > 0)
                                strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong></strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #ff0000;font-family: Arial, Helvetica, sans-serif;'>" + ElectronicNote + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>";

                            strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Ticket Protection:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" +
                                (x.TktPrtCharge > 0 ? "Yes" : "No") + "</td><td width='20%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong>" + currency + " " + Convert.ToString(x.TktPrtCharge) + "</strong></td></tr>";
                            strProductDesc += "</table></td></tr></table>";

                            PassProtection = (Convert.ToDecimal(PassProtection) + x.TktPrtCharge).ToString();

                            #region receipt
                            newid = newid + i;
                            OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Pass " + i + ":", Value = "" });
                            newid++;
                            foreach (var w in x.ReceiptProductDetail)
                            {
                                OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = w.Name + i, Value = w.Value + (ElectronicNote.Length > 0 ? "ñ" + ElectronicNote : "") });
                                newid++;
                            }
                            if (x.a.PassStartDate.HasValue)
                            {
                                OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Pass start date", Value = x.a.PassStartDate.Value.ToString("dd/MMM/yyyy") });
                                newid++;
                            } newid++;

                            OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Passenger Name", Value = x.a.Title + " " + x.a.FirstName + " " + x.a.MiddleName + " " + x.a.LastName });
                            newid++;
                            OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Price", Value = currency + " " + x.Price });
                            newid++;
                            if (x.TktPrtCharge > 0)
                            {
                                OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Ticket Protection", Value = currency + " " + x.TktPrtCharge });
                                newid++;
                            }
                            #endregion
                        }
                        else
                        {
                            strProductDesc += "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth' align='center' bgcolor='#eeeeee'><tr><td bgcolor='#ffffff' height='5'></td></tr><tr><td style='padding: 10px;' align='center' valign='top' class='pad'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' class='deviceWidth '>";
                            strProductDesc += x.ProductDesc.Replace("##Price##", currency + " " + x.Price.ToString()).Replace("##NetAmount##", "Net Amount :" + nettPrice).Replace("##Name##", x.a.Title + " " + x.a.FirstName + (!string.IsNullOrEmpty(x.a.MiddleName) ? " " + x.a.MiddleName : "") + " " + (!string.IsNullOrEmpty(x.a.LastName) ? x.a.LastName.Split(new[] { "<BR/>" }, StringSplitOptions.None)[0] : ""));
                            strProductDesc += (HttpContext.Current.Session["CollectStation"] != null ? "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Collect at ticket desk:</strong></td><td width='50%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + HttpContext.Current.Session["CollectStation"] == null ? "" : HttpContext.Current.Session["CollectStation"].ToString() + "</td><td width='20%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>" : "");
                            if (P2PSaleList != null && P2PSaleList.Count > 0 && P2PSaleList.Any(z => z.ApiName.ToUpper() == "ITALIA"))
                            {
                                if (string.IsNullOrEmpty(P2PSaleList.FirstOrDefault(z => z.ID == x.a.PassSaleID).PdfURL))
                                    strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Important:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>This PNR is issued as a ticket on depart and must be printed from one of the self-service ticket machines located at all main Trenitalia stations (which are green, white and red). Once you have printed your ticket you must validate it in one of the validation machines located near to the entrance of each platform (they are green and white with Trenitalia written on the front and usually mounted on a wall), THIS MUST BE DONE BEFORE BOARDING YOUR TRAIN, failure to do so will result in a fine.</td></tr>";
                            }
                            PassProtection = (Convert.ToDecimal(PassProtection) + x.TktPrtCharge).ToString();
                            if (x.Terms != null)
                                strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Ticket Terms:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + HttpContext.Current.Server.HtmlDecode(x.Terms) + "</td></tr>";
                            if (isEvolviBooking)
                            {
                                var P2PData = _db.tblP2PSale.FirstOrDefault(Q => Q.ID == x.a.PassSaleID.Value);
                                if (P2PData != null)
                                {
                                    if (!string.IsNullOrEmpty(P2PData.EvSleeper))
                                    {
                                        strProductDesc += "<tr><td width='22%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Reservation Details:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>";
                                        foreach (var item in P2PData.EvSleeper.Split('@'))
                                        {
                                            string ss = item.Replace("Seat Allocation:", "").Replace("Berth Allocation:", "");
                                            strProductDesc += "<div>" + HttpContext.Current.Server.HtmlDecode(ss) + "</div>";
                                        }
                                        strProductDesc += "</td></tr>";
                                    }
                                }
                            }
                            strProductDesc += "</table></td></tr></table>";

                            #region receipt
                            newid = newid + i;
                            OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Pass " + i + ":", Value = "" });
                            newid++;
                            foreach (var w in x.ReceiptProductDetail)
                            {
                                OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = w.Name, Value = w.Value });
                                newid++;
                            }
                            OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Passenger Name", Value = x.a.Title + " " + x.a.FirstName + " " + x.a.MiddleName + " " + (x.a.LastName.Split('<')[0] + (x.a.PassSaleID.HasValue ? "\nPassenger: " + (oBooking.GetPassengerDetails(x.a.PassSaleID.Value)) : "")) });
                            newid++;
                            OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Price", Value = currency + " " + x.Price });
                            newid++;
                            if (x.TktPrtCharge > 0)
                            {
                                OrderReceiptList.Add(new OrderReceipt { Id = newid, Name = "Ticket Protection", Value = currency + " " + x.TktPrtCharge });
                                newid++;
                            }
                            #endregion
                        }
                        i++;
                    }
                }

                PassProtectionHtml = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Pass Protection: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + PassProtection.Trim() + "</td></tr>";
                body = body.Replace("##PassProtection##", PassProtection == "0.00" ? "" : PassProtectionHtml);
                body = body.Replace("##OrderDetails##", strProductDesc);
                body = body.Replace("##BillingAddress##", BillingAddress);
                if (ISP2POrder)
                {
                    var getDeliveryType = oBooking.getDeliveryOption(Convert.ToInt64(Session["P2POrderID"]));
                    if (getDeliveryType != null && getDeliveryType == "Print at Home")
                    {
                        const string printAthome = "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth'><tr><td style='padding: 10px; font-size: 12px; color: #333; font-family: Arial, Helvetica, sans-serif;'colspan='2'>Please ensure you have a printed copy of your PDF booking confirmation as train operators across europe will not always accept the booking from your tablet or mobile screen.</td></tr></table>";
                        body = body.Replace("##PrintAtHome##", printAthome);
                        Session["showPrintAtHome"] = "1";
                    }
                    else
                        body = body.Replace("##PrintAtHome##", "");
                }
                else
                    body = body.Replace("##PrintAtHome##", "");

                body = body.Replace("../images/", SiteName + "images/");

                /*Send Email to Bcc email address*/
                string BccEmail = _oWebsitePage.GetBccEmail(siteId);
                /*Get smtp details*/
                var result = _masterPage.GetEmailSettingDetail(siteId);
                if (result != null && lstNew.Count() > 0)
                {
                    var fromAddres = new MailAddress(result.Email, result.Email);
                    smtpClient.Host = result.SmtpHost;
                    smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                    smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                    message.From = fromAddres;
                    message.To.Add(ToEmail);
                    if (!string.IsNullOrEmpty(BccEmail))
                        message.Bcc.Add(BccEmail);
                    message.Bcc.Add(ConfigurationManager.AppSettings["BCCMailAddress"]);
                    message.Subject = Subject;
                    message.IsBodyHtml = true;
                    message.Body = body;
                    smtpClient.Send(message);
                    retVal = true;
                }
                Session["ShipMethod"] = string.Empty;
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, "Failed to send E-mail confimation!");
        }
        return retVal;
    }

    public void GetPrintReceipt()
    {
        Document document = new Document(PageSize.A4, 20f, 20f, 20f, 20f);
        try
        {
            GetReceiptLoga();
            string filename = Server.MapPath("~/PdfReceipt/" + lblOrderNumber.Text.Trim() + ".pdf");
            float[] widths = new float[] { 3f, 5f, 4f };
            iTextSharp.text.Font TitleFont = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font HeaderFont = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font TextFont = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font TextRed = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, BaseColor.RED);
            iTextSharp.text.Font TextBlue = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.UNDERLINE, BaseColor.BLUE);
            PdfWriter.GetInstance(document, new FileStream(filename, FileMode.Create));

            #region Pdf Table Creates
            PdfPTable tableOne = new PdfPTable(3);
            tableOne.TotalWidth = 550f;
            tableOne.LockedWidth = true;
            tableOne.SetWidths(widths);
            tableOne.HorizontalAlignment = 0;
            #endregion

            iTextSharp.text.Image RTlogo = iTextSharp.text.Image.GetInstance(logo);
            PdfPCell SiteLogo = new PdfPCell(RTlogo);
            SiteLogo.PaddingTop = 5;
            SiteLogo.PaddingBottom = 5;
            SiteLogo.Colspan = 3;
            SiteLogo.Border = Rectangle.NO_BORDER;
            tableOne.AddCell(SiteLogo);

            PdfPCell headingText1 = new PdfPCell(new Phrase("Thank you for your order", HeaderFont));
            headingText1.Border = Rectangle.NO_BORDER;
            tableOne.AddCell(headingText1);

            PdfPCell headingText2 = new PdfPCell(new Phrase(RUserName, TextFont));
            headingText2.Border = Rectangle.NO_BORDER;
            headingText2.Colspan = 2;
            tableOne.AddCell(headingText2);

            PdfPCell Note1 = new PdfPCell(new Phrase("Note", HeaderFont));
            Note1.Border = Rectangle.NO_BORDER;
            tableOne.AddCell(Note1);

            PdfPCell Note2 = new PdfPCell(new Phrase("This is not your travel document", TextRed));
            Note2.Border = Rectangle.NO_BORDER;
            Note2.Colspan = 2;
            tableOne.AddCell(Note2);

            bool IsFirst = true;
            bool IsFirstTOD = true;
            foreach (var x in OrderReceiptList.OrderBy(x => x.Id))
            {
                if (x.Name.Contains("Order Summary") || x.Name.Contains("Pass") && string.IsNullOrEmpty(x.Value))
                {
                    x.Name = x.Name.Contains("Pass") ? "" : x.Name;
                    PdfPCell CellName = new PdfPCell(new Phrase(x.Name, HeaderFont));
                    CellName.PaddingTop = 15;
                    CellName.PaddingBottom = 7;
                    CellName.Colspan = 2;
                    CellName.Border = Rectangle.NO_BORDER;
                    tableOne.AddCell(CellName);
                }
                else if (x.Name.Contains("Subtotal"))
                {
                    var CellName = new PdfPCell(new Phrase(x.Name, HeaderFont));
                    CellName.Border = Rectangle.NO_BORDER;
                    CellName.PaddingTop = 15;
                    tableOne.AddCell(CellName);

                    var CellValue = new PdfPCell(new Phrase(x.Value, TextFont));
                    CellValue.Border = Rectangle.NO_BORDER;
                    CellValue.PaddingTop = 15;
                    tableOne.AddCell(CellValue);
                }
                else
                {
                    var CellName = new PdfPCell(new Phrase(x.Name, HeaderFont));
                    CellName.Border = Rectangle.NO_BORDER;
                    tableOne.AddCell(CellName);


                    var CellValue = new PdfPCell(new Phrase(x.Value, TextFont));
                    CellValue.Border = Rectangle.NO_BORDER;
                    if (x.Name == "TOD Ref" && IsFirstTOD)
                    {
                        IsFirstTOD = false;
                        var link = new Chunk("How to collect your ticket", TextBlue).SetAnchor(EvolviTandC);
                        Paragraph SParagraph = new Paragraph();
                        SParagraph.SetLeading(3, 1);
                        SParagraph.Font.Size = 10;
                        SParagraph.Add(x.Value + "\n");
                        SParagraph.Add(link);
                        CellValue.MinimumHeight = 34F;
                        CellValue.AddElement(SParagraph);
                    }
                    tableOne.AddCell(CellValue);
                }
                if (IsFirst)
                {
                    IsFirst = false;
                    iTextSharp.text.Image RSimg1 = iTextSharp.text.Image.GetInstance(img1);
                    RSimg1.ScaleAbsolute(130f, 130f);
                    RSimg1.SpacingAfter = 10;
                    iTextSharp.text.Image RSimg2 = iTextSharp.text.Image.GetInstance(img2);
                    RSimg2.ScaleAbsolute(130f, 130f);
                    RSimg2.SpacingAfter = 10;
                    iTextSharp.text.Image RSimg3 = iTextSharp.text.Image.GetInstance(img3);
                    RSimg3.ScaleAbsolute(130f, 130f);
                    RSimg3.SpacingAfter = 10;
                    PdfPCell Simg = new PdfPCell();
                    Simg.HorizontalAlignment = 1;
                    Simg.AddElement(RSimg1);
                    Simg.AddElement(RSimg2);
                    Simg.AddElement(RSimg3);
                    Simg.Rowspan = OrderReceiptList.Count + 2;
                    Simg.Border = Rectangle.NO_BORDER;
                    tableOne.AddCell(Simg);
                }
            }

            PdfPCell CellName1 = new PdfPCell(new Phrase("Here to help...", HeaderFont));
            CellName1.PaddingTop = 15;
            CellName1.PaddingBottom = 7;
            CellName1.Colspan = 2;
            CellName1.Border = Rectangle.NO_BORDER;
            tableOne.AddCell(CellName1);

            string txt1 = "If you have any queries about your order please do not hesitate to contact us.\n\nThank you for shopping with us and happy travels!";
            PdfPCell CellName2 = new PdfPCell(new Phrase(txt1, TextFont));
            CellName2.Colspan = 2;
            CellName2.Border = Rectangle.NO_BORDER;
            tableOne.AddCell(CellName2);

            string txt2 = "\n\n\nSee internationalrail.com for delivery options and full terms and conditions.\nOur Privacy Policy and Terms and Conditions are also available on our website";
            PdfPCell CellName3 = new PdfPCell(new Phrase(txt2, TextFont));
            CellName3.PaddingTop = 15;
            CellName3.PaddingBottom = 7;
            CellName3.Colspan = 3;
            CellName3.Border = Rectangle.NO_BORDER;
            CellName3.HorizontalAlignment = 1;
            tableOne.AddCell(CellName3);

            string txt3 = "(c) International Rail Ltd is a company registered in England & Wales.\nRegistered number: 3060803";
            PdfPCell CellName4 = new PdfPCell(new Phrase(txt3, HeaderFont));
            CellName4.Colspan = 3;
            CellName4.Border = Rectangle.NO_BORDER;
            CellName4.HorizontalAlignment = 1;
            tableOne.AddCell(CellName4);

            var CellNamex = new PdfPCell(new Phrase(document.PageSize.Height + "; ", HeaderFont));
            CellNamex.Border = Rectangle.NO_BORDER;
            CellNamex.MinimumHeight = document.PageSize.Height - (tableOne.TotalHeight + 50f);
            CellName2.Colspan = 2;
            tableOne.AddCell(CellNamex);

            document.Open();
            document.Add(tableOne);
        }
        catch (Exception ex)
        {
            ShowMessage(2, "Receipt not generated.");
        }
        finally
        {
            document.Close();
            document.Dispose();
        }
    }

    public void GetReceiptLoga()
    {
        if (HtmlFileSitelog.ToLower().Contains("irmailtemplate"))
            Name = "IR";
        else if (HtmlFileSitelog.ToLower().Contains("travelcut"))
            Name = "TC";
        else if (HtmlFileSitelog.ToLower().Contains("stamail"))
            Name = "STA";
        else if (HtmlFileSitelog.ToLower().Contains("MeritMail"))
            Name = "MM";

        switch (Name)
        {
            case "TC":
                logo = "https://www.internationalrail.com/images/travelcutslogo.png";
                img1 = "https://www.internationalrail.com/images/image1-top_blue.jpg";
                img2 = "https://www.internationalrail.com/images/image2-middle_blue.jpg";
                img3 = "https://www.internationalrail.com/images/image3-bottom_blue.jpg";
                SiteHeaderColor = "#0f396d";
                break;
            case "STA":
                logo = "https://www.internationalrail.com/images/mainLogo.png";
                img1 = "https://www.internationalrail.com/images/image1-top_blue.jpg";
                img2 = "https://www.internationalrail.com/images/image2-middle_blue.jpg";
                img3 = "https://www.internationalrail.com/images/image3-bottom_blue.jpg";
                SiteHeaderColor = "#0c6ab8";
                break;
            case "MM":
                logo = "https://www.internationalrail.com/images/meritlogo.png";
                img1 = "https://www.internationalrail.com/images/image1-top_blue.jpg";
                img2 = "https://www.internationalrail.com/images/image2-middle_blue.jpg";
                img3 = "https://www.internationalrail.com/images/image3-bottom_blue.jpg";
                SiteHeaderColor = "#0c6ab8";
                break;
            default:
                logo = "https://www.internationalrail.com/images/logo.png";
                img1 = "https://www.internationalrail.com/images/image1-top_red.jpg";
                img2 = "https://www.internationalrail.com/images/image2-middle_red.jpg";
                img3 = "https://www.internationalrail.com/images/image3-bottom_red.jpg";
                SiteHeaderColor = "#941e34";
                break;
        }
    }

    public string GetApiName()
    {
        if (isTI)
            return "TrainItalia";
        else if (isBene)
            return "BeNe";
        else if (isNTVBooking)
            return "Ntv";
        else if (isEvolviBooking)
            return "Evolvi";
        else
            return "";
    }

    public string GetBillingAddressPhoneNo(long OrderID)
    {
        var result = _db.tblOrderBillingAddresses.FirstOrDefault(x => x.OrderID == OrderID);
        if (result != null)
        {
            return !string.IsNullOrEmpty(result.Phone) ? result.Phone : "";
        }
        return "";
    }
}
