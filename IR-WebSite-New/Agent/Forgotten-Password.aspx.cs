﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Business;
using System.Configuration;

public partial class Agent_Forgotten_Password : System.Web.UI.Page
{
    readonly ManageUser _ManageUser = new ManageUser();
    readonly Masters _master = new Masters();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string SiteAdminUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string siteURL, SiteLogoUrl = string.Empty;
    private Guid _siteId;
    public string script = "<script></script>";
    public string FaviconIcon = string.Empty;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        try
        {
            if (Session["siteId"] != null)
            {
                _siteId = Guid.Parse(Session["siteId"].ToString());
                siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            }
        }
        catch (Exception ex) { throw ex; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            QubitOperationLoad();
        }
        catch (Exception ex) { throw ex; }
    }

    protected void btnFSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (!String.IsNullOrEmpty(txtEmail.Text) && !String.IsNullOrEmpty(txtUser.Text))
            {
                var UserExist = _ManageUser.CheckEmail(txtUser.Text, txtEmail.Text);
                if (UserExist != null)
                {
                    string Subject = "Reset Password";

                    string Body = "<html><head><title></title></head><body><p> Dear " + UserExist.UserName + " , <br /> <br /> Your password is reset on International Rail <br />" +
                              "<br />	Your Agent login details are as below:<br />	<br />	User Name : " + UserExist.UserName + "<br />	Password : " + UserExist.Password + "<br /><br />	Thanks" +
                              "<br />International Rail</p></body></html>";

                    var SendEmail = ManageUser.SendMailForPswRest(Subject, Body, txtEmail.Text, _siteId);
                    if (SendEmail)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>alert('Email Sent on Registered Email Id');</script>", false);
                        txtUser.Text = string.Empty;
                        txtEmail.Text = string.Empty;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>alert('Email is not Registered with System');</script>", false);
                }
            }
        }
        catch (Exception ex) { throw ex; }
    }

    public void QubitOperationLoad()
    {
        try
        {
            List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
            var res = lstQbit.FirstOrDefault();
            if (res != null)
                script = res.Script;
        }
        catch (Exception ex) { throw ex; }
    }
}