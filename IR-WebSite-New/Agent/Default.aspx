﻿<%@ Page Title="" Language="C#" MasterPageFile="~/STAPublic.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Agent_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%=script%>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" >
    </asp:ScriptManager>
    <div class="starail-Login-wrapper login_interail"> 
        <div class="starail-Box starail-Login">
            <div class="starail-Box-content starail-Box-content--small"> 
                <h2 class="starail-Login-title">
                    Log in to book and print tickets</h2>
                <div class="starail-Form-row starail-Form-row--hasIcon">
                    <label for="emailAddress" class="starail-Form-label starail-u-visuallyHidden">
                        Email</label>
                    <asp:TextBox ID="txtUsername" CssClass="required starail-Form-input" runat="server"
                        placeholder="User Name"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqvalerror1" ControlToValidate="txtUsername" runat="server"
                        ValidationGroup="loginform" Display="Dynamic"></asp:RequiredFieldValidator>
                    <i class="starail-Icon-email"></i><span class="starail-Form-errorText"></span>
                </div>
                <div class="starail-Form-row starail-Form-row--hasIcon">
                    <label for="password" class="starail-Form-label starail-u-visuallyHidden">
                        Password</label>
                    <asp:TextBox ID="txtPassword" runat="server" CssClass="required starail-Form-input"
                        TextMode="Password" placeholder="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqvalerror2" ControlToValidate="txtPassword" runat="server"
                        ValidationGroup="loginform" Display="Dynamic"></asp:RequiredFieldValidator>
                    <i class="starail-Icon-password"></i><span class="starail-Form-note starail-Form-note--block">
                        (Leave blank in you don't have one)</span> <span class="starail-Form-errorText">
                    </span>
                </div>
                <p class="starail-Login-forgottenPw">
                    <a href="Forgotten-Password.aspx">Forgotten your password?</a>
                </p>
                <div class="starail-Form-row starail-Login-remember">
                    <label class="starail-BookingDetails-form-fancyCheckbox">
                        <span class="starail-Form-fancyCheckbox">
                            <asp:CheckBox ID="chkIsAffliateLogin" runat="server" />
                            Is affiliate login ? <span><i class="starail-Icon-tick"></i></span></span>
                    </label>
                </div>
                <div class="starail-Form-row starail-Login-remember">
                    <label class="starail-BookingDetails-form-fancyCheckbox">
                        <span class="starail-Form-fancyCheckbox">
                            <asp:CheckBox ID="chkRememberMe" runat="server" />
                            Remember me <span><i class="starail-Icon-tick"></i></span></span>
                    </label>
                </div>
                <div class="starail-Form-row">
                    <asp:Button ID="BtnSubmit" Text="Log In" OnClick="BtnSubmit_Click" 
                        CssClass="starail-Button starail-Button--full starail-Button--login" runat="server"
                        ValidationGroup="loginform" />
                    <asp:Label ID="lblSuccessMsg" runat="server" CssClass="valdreq" ForeColor="#FF3300" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
