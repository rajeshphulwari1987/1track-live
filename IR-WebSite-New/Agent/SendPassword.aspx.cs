﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Business;
using System.Configuration;

public partial class Agent_SendPassword : System.Web.UI.Page
{
    readonly ManageUser _ManageUser = new ManageUser();
    readonly Masters _master = new Masters();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string SiteAdminUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string siteURL;
    private Guid _siteId;
    public string FaviconIcon = string.Empty;
    public string script = "<script></script>";
    public string SiteLogoUrl = string.Empty;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        try
        {
            if (Session["siteId"] != null)
            {
                _siteId = Guid.Parse(Session["siteId"].ToString());
                siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            }
        }
        catch (Exception ex) { throw ex; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            lblSuccessMsg.Text = string.Empty;
            if (!IsPostBack)
            {
                if (Session["emailaddressuser"] != null)
                {
                    txtEmail.Enabled = false;
                    hdnId.Value = Convert.ToString(Session["emailaddressuser"]);
                    var list = _ManageUser.GetAdminUserDetails(Guid.Parse(hdnId.Value));
                    if (list != null && list.IsActive == true)
                        txtEmail.Text = list.EmailAddress;
                }
                Session.Remove("emailaddressuser");
                QubitOperationLoad();
            }
        }
        catch (Exception ex) { throw ex; }
    }

    public void QubitOperationLoad()
    {
        try
        {
            var lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
            var res = lstQbit.FirstOrDefault();
            if (res != null)
                script = res.Script;
        }
        catch (Exception ex) { throw ex; }
    }

    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (!String.IsNullOrEmpty(txtEmail.Text))
            {
                var id = Guid.Parse(hdnId.Value);
                var admin = _db.tblAdminUsers.FirstOrDefault(x => x.ID == id);
                string Subject = "Generate Password Link";
                if (admin != null)
                {
                    string siteName = _master.GetSiteNameByID(_siteId);
                    string Body = "<html><head><title></title></head><body> <p><br>Hi, <br><br>Thanks for requesting the creation of a password for your username on the 1track " + siteName + " website.<br><br>Your username is: <b> " + admin.UserName + "</b><br><br>Please click on the link below (or copy and paste into you browser) to generate your new password:<br><br><a href='" + siteURL + "Agent/ResetPassword.aspx?reqId=" + hdnId.Value + "'>" + siteURL + "Agent/ResetPassword.aspx?reqId=" + hdnId.Value + "</a><br><br>Thanks<br><br>International Rail</p><br><br></body></html>";
                    var SendEmail = ManageUser.SendMail(Subject, Body, txtEmail.Text, _siteId);
                    if (SendEmail)
                    {
                        txtEmail.Text = string.Empty;
                        lblSuccessMsg.Text = "Email sent on registred email.";
                    }
                }
            }
            else
                lblSuccessMsg.Text = "Email is not Registered with System";
        }
        catch (Exception ex) { throw ex; }
    }
}