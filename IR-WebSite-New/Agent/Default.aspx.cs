﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Business;
using System.Configuration;

public partial class Agent_Default : System.Web.UI.Page
{
    readonly ManageUser _ManageUser = new ManageUser();
    readonly Masters _master = new Masters();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string SiteAdminUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string siteURL, SiteLogoUrl = string.Empty;
    private Guid _siteId;
    public string script = "<script></script>";
    public string FaviconIcon = string.Empty;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        try
        {
            if (Session["siteId"] != null)
            {
                _siteId = Guid.Parse(Session["siteId"].ToString());
                siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            }
            else
                Response.Redirect("../");
        }
        catch (Exception ex) { throw ex; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            lblSuccessMsg.Text = string.Empty;
            if (!IsPostBack)
            {
                if (Request.QueryString["UserId"] != null)
                    AgenLogin();
                if (AgentuserInfo.UserID != Guid.Empty)
                {
                    removeAgentSession();
                    Response.Redirect(siteURL);
                }
                SetPassword();
            }
            QubitOperationLoad();
        }
        catch (Exception ex) { throw ex; }
    }

    public void removeAgentSession()
    {
        Session.Remove("RailPassData");
        Session.Remove("DetailRailPass");
        Session.Remove("AgentUsername");
        Session.Remove("AgentRoleId");
        Session.Remove("AgentUserID");
        Session.Remove("AgentSiteID");
        Session.Remove("IsAffliate");
        Session.Remove("IsPaymentGatway");
        Session.Remove("PopupDisplayed");
        Session.Remove("OrderID");
        Session.Remove("P2POrderID");
    }

    void AffiliateLogin()
    {
        try
        {
            tblAffiliateUser AffUserExist = _ManageUser.AffliateLogin(txtUsername.Text, txtPassword.Text, _siteId);
            if (AffUserExist == null)
                lblSuccessMsg.Text = "Invalid username or password.";
            else
            {
                AgentuserInfo.UserID = AffUserExist.ID;
                AgentuserInfo.IsAffliate = true;
                AgentuserInfo.Username = AffUserExist.UserName;
                //-- Agent cookies work for the affilate user as well
                if (chkRememberMe.Checked)
                {
                    HttpCookie cookie = new HttpCookie("Agentlogin");
                    Response.Cookies.Add(cookie);
                    string pass = Common.Encrypt(txtPassword.Text.Trim(), true);
                    cookie.Values.Add("AgentUserName", txtUsername.Text);
                    cookie.Values.Add("AgentPassword", pass);
                    cookie.Values.Add("AgentRemember", chkRememberMe.Checked.ToString());
                    cookie.Expires = DateTime.Now.AddHours(2);
                }
                else
                {
                    HttpContext.Current.Response.Cookies.Remove("Agentlogin");
                    Response.Cookies["Agentlogin"].Expires = DateTime.Now;
                }
                Response.Redirect(siteURL);
            }
        }
        catch (Exception ex) { throw ex; }
    }

    public void AgenLogin()
    {
        try
        {
            tblAdminUser UserExist = null;
            if (Request.QueryString["UserId"] != null)
            {
                UserExist = _ManageUser.AgentLoginById(Guid.Parse(Request.QueryString["UserId"]));
            }
            else if (!String.IsNullOrEmpty(txtUsername.Text) && !String.IsNullOrEmpty(txtPassword.Text))
            {
                UserExist = _ManageUser.AgentLogin(txtUsername.Text, txtPassword.Text, _siteId);
            }
            if (UserExist != null)
            {
                //Login Agent User Information
                AgentuserInfo.RoleId = UserExist.RoleID;
                AgentuserInfo.UserID = UserExist.ID;
                AgentuserInfo.Username = UserExist.UserName;
                //Remember  Me 
                if (chkRememberMe.Checked)
                {
                    HttpCookie cookie = new HttpCookie("Agentlogin");
                    Response.Cookies.Add(cookie);
                    string pass = Common.Encrypt(txtPassword.Text.Trim(), true);
                    cookie.Values.Add("AgentUserName", txtUsername.Text);
                    cookie.Values.Add("AgentPassword", pass);
                    cookie.Values.Add("AgentRemember", chkRememberMe.Checked.ToString());
                    cookie.Expires = DateTime.Now.AddHours(2);
                }
                else
                {
                    HttpContext.Current.Response.Cookies.Remove("Agentlogin");
                    Response.Cookies["Agentlogin"].Expires = DateTime.Now;
                }

                if (Request.QueryString["UserId"] != null)
                    Response.Redirect(siteURL + "Printing/PrintingOrders");
                else if (Session["redirectpage"] != null)
                    Response.Redirect(siteURL + "PaymentProcess" + Session["redirectpage"]);
                else if (Session["deeplinkurl"] != null)
                {
                    if (Session["deeplinkurl"].ToString().Contains("404.aspx"))
                        Response.Redirect(siteURL);
                    else
                        Response.Redirect(Session["deeplinkurl"].ToString());
                }
                else
                    Response.Redirect(siteURL + "home");
            }
            else if (Request.QueryString["UserId"] != null || !String.IsNullOrEmpty(txtUsername.Text))
            {
                var list = _ManageUser.AgentDetailsByUserName(txtUsername.Text);
                if (list != null && string.IsNullOrEmpty(list.Password))
                {
                    Session.Add("emailaddressuser", list.ID);
                    Response.Redirect("SendPassword.aspx");
                }
                lblSuccessMsg.Text = "Invalid username or password.";
            }
        }
        catch (Exception ex)
        {
            if (Request.QueryString["UserId"] != null)
                Response.Redirect(siteURL + "Printing/PrintingOrders");
            else if (Session["redirectpage"] != null)
                Response.Redirect(siteURL + "PaymentProcess" + Session["redirectpage"]);
            else if (Session["emailaddressuser"] != null)
                Response.Redirect("SendPassword.aspx");
            else if (Session["deeplinkurl"] != null)
            {
                if (Session["deeplinkurl"].ToString().Contains("404.aspx"))
                    Response.Redirect(siteURL);
                else
                    Response.Redirect(Session["deeplinkurl"].ToString());
            }
            else
                Response.Redirect(siteURL, true);
        }
    }

    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (!String.IsNullOrEmpty(txtUsername.Text) && !chkIsAffliateLogin.Checked)
                AgenLogin();
            else if (!String.IsNullOrEmpty(txtUsername.Text))
                AffiliateLogin();
        }
        catch (Exception ex) { throw ex; }
    }

    void SetPassword()
    {
        try
        {
            if (Request.Cookies["Agentlogin"] != null)
            {
                HttpCookie getCookie = Request.Cookies.Get("Agentlogin");
                if (getCookie != null && getCookie["AgentRemember"] != null && getCookie["AgentRemember"].ToLower() == "true")
                {
                    string pass = Common.Decrypt(getCookie.Values["AgentPassword"].Trim(), true);
                    txtUsername.Text = getCookie.Values["AgentUserName"].Trim();
                    txtPassword.Attributes.Add("value", pass);
                    chkRememberMe.Checked = true;
                }
            }
        }
        catch (Exception ex) { throw ex; }
    }

    public void QubitOperationLoad()
    {
        try
        {
            List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
            var res = lstQbit.FirstOrDefault();
            if (res != null)
                script = res.Script;
        }
        catch (Exception ex) { throw ex; }
    }
}