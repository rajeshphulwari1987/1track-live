﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Agentpayment.aspx.cs" Inherits="Agentpayment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#MainContent_btnpaycompliteorder").click(function () {
                var inp = $("#txtfolderno").val();
                if (!$("#MainContent_chkTerm").is(":checked") && jQuery.trim(inp).length > 0) {
                    alert("To continue, you must accept our Terms");
                    return false;
                }
                else {
                    return true;
                }
            });
        });
    </script>
    <style type="text/css">
        .ButtonMargin {
            margin: 0px !important;
        }

        .LabelMargin {
            margin: 10px;
        }


        .timertopcls {
            display: inline-block;
            float: right;
            margin-top: 4px;
            padding: 4px 6px;
            border-radius: 7px;
            font-size: 12px;
            margin-bottom: 10px;
            top: 49% !important;
            right: 15.5% !important;
            position: fixed;
            z-index: 100001;
            background: #fff;
            text-align: center;
            margin-right: -120px;
            box-shadow: 2px 4px 4px #d4d4d4;
            text-shadow: 1px 2px 3px #d4d4d4;
        }

            .timertopcls a {
                font-weight: bold;
                text-decoration: underline;
            }

        @media (max-width: 767px) {

            .timertopcls {
                display: block;
                margin: 0 15px 10px 15px;
                float: left;
                right: -4% !important;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="sm1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnAgentId" runat="server" />
    <asp:HiddenField ID="hdnIsTI" runat="server" Value="False" ClientIDMode="Static" />
    <div class="starail-Grid starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-ProgressBar starail-ProgressBar-stage--stage3 starail-u-hideMobile">
                <div class="starail-ProgressBar-line">
                    <div class="starail-ProgressBar-progress">
                    </div>
                </div>
                <div class="starail-ProgressBar-stage starail-ProgressBar-stage1">
                    <div class="starail-ProgressBar-circle">
                    </div>
                    <p class="starail-ProgressBar-label">
                        Ticket Selection
                    </p>
                    <div class="starail-ProgressBar-subStage">
                    </div>
                </div>
                <div class="starail-ProgressBar-stage starail-ProgressBar-stage2">
                    <div class="starail-ProgressBar-circle">
                    </div>
                    <p class="starail-ProgressBar-label">
                        Booking Details
                    </p>
                    <div class="starail-ProgressBar-subStage">
                    </div>
                </div>
                <div class="starail-ProgressBar-stage starail-ProgressBar-stage3">
                    <div class="starail-ProgressBar-circle">
                    </div>
                    <p class="starail-ProgressBar-label">
                        Checkout
                    </p>
                </div>
            </div>
            <div class="starail-TrainOptions-mobileNav starail-u-hideDesktop">
                <div class="">
                    <a href="/booking-details.html" class="starail-Button starail-Button--blue"><i class="starail-Icon-chevron-left"></i>Details</a> <a href="#" class="starail-Button starail-Button--blue starail-done-trigger is-disabled">Done! <i class="starail-Icon-chevron-right"></i></a>
                    <!-- triggers payment confirm form -->
                    <!-- remove is-disabled class to enable button when form complete-->
                </div>
            </div>
            <div class="starail-Form--paymentConfirmation">
                <div class="starail-BookingDetails-titleAndButton">
                    <h1 class="starail-BookingDetails-title">Enter your Reference details and confirm payment</h1>
                    <asp:Panel ID="pnlErrSuccess" runat="server">
                        <div id="DivSuccess" runat="server" class="success" style="display: none;">
                            <asp:Label ID="lblSuccessMsg" runat="server" />
                        </div>
                        <div id="DivError" runat="server" class="error" style="display: none;">
                            <asp:Label ID="lblErrorMsg" runat="server" />
                        </div>
                    </asp:Panel>
                </div>
                <div class="starail-BookingDetails-titleAndButton timertopcls" style='display: none;'>
                    <div class='timercls'></div>
                    <a href='<%=siteURL %>' class="timerredirectcls" style='display: none;'>Click here</a>
                </div>
                <div style="clear: both"></div>
                <div class="starail-BookingDetails-form starail-BookingDetails-form--payAccount">
                    <h2>Pay on Account</h2>
                    <div class="starail-Form-row">
                        <label for="starail-ref" class="starail-Form-label">
                            Your ref / folder number</label>
                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                            <div>
                                <asp:TextBox runat="server" ID="txtfolderno" ClientIDMode="Static" MaxLength="45"
                                    CssClass="starail-Form-input"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqvalerror1" ControlToValidate="txtfolderno" runat="server"
                                    ValidationGroup="Agent" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtfolderno"
                                    WatermarkText="Reference Number" WatermarkCssClass="watermark" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                        <label for="starail-ref" class="starail-Form-label">
                            Note</label>
                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                            <div>
                                <textarea runat="server" id="txtNote" title="Enter Note." class="starail-Form-textarea"
                                    rows="2" placeholder="Note"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row starail-u-marginNone">
                        <div class="starail-Form-spacer starail-u-hideMobile">
                            &nbsp;
                        </div>
                        <div class="starail-Form-inputContainer">
                            <div class="starail-Form-inputContainer-col">
                                <label class="starail-BookingDetails-form-fancyCheckbox">
                                    <span class="starail-Form-fancyCheckbox">
                                        <asp:CheckBox ID="chkTerm" runat="server" />
                                        By entering the ref / folder number and clicking 'Pay now' you're approving the
                                        purchase of the products in your order, and will be invoiced for them. Tick this
                                        box to proceed. <span><i class="starail-Icon-tick"></i></span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <!--.starail-BookingDetails-form -->
                <!-- Passes -->
                <div class="starail-YourBooking starail-YourBooking--confirmation">
                    <h2>Your Journey</h2>
                    <div class="starail-YourBooking-table">
                        <div class="starail-YourBooking-ticketDetails">
                            <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                                Order Number:
                            </div>
                            <div class="starail-YourBooking-col--mobileFullWidth">
                                <asp:Label runat="server" ID="lblOrderNumber"></asp:Label>
                            </div>
                            <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                                Order Date:
                            </div>
                            <div class="starail-YourBooking-col--mobileFullWidth">
                                <asp:Label runat="server" ID="lblOrderDate"></asp:Label>
                            </div>
                        </div>
                        <div class="starail-YourBooking-ticketDetails">
                            <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                                Email Address:
                            </div>
                            <div class="starail-YourBooking-col--mobileFullWidth">
                                <asp:Label runat="server" ID="lblEmailAddress"></asp:Label>
                            </div>
                            <div class="verticaltop starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                                Delivery Address:
                            </div>
                            <div class="verticaltop starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                                <asp:Label runat="server" ID="lblDeliveryAddress"></asp:Label>
                            </div>
                        </div>
                        <div class="starail-YourBooking-ticketDetails" id="div_BookingRef" runat="server"
                            visible="false">
                            <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                                Booking Ref:
                            </div>
                            <div class="starail-YourBooking-col--mobileFullWidth">
                                <asp:Label runat="server" ID="lblBookingRef"></asp:Label>
                            </div>
                            <div class="verticaltop starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                                TOD Ref:
                            </div>
                            <div class="verticaltop starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                                <asp:Label runat="server" ID="lblTODRef"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="starail-u-hideMobile">
                        <br />
                    </div>
                    <div class="starail-YourBooking-table" runat="server" id="DivPassSale">
                        <div class="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--header starail-u-hideMobile">
                            <div>
                                Pass
                            </div>
                            <div>
                                Countries
                            </div>
                            <div>
                                Validity
                            </div>
                            <div class="starail-YourBooking-col-center">
                                Protected
                            </div>
                            <div class="starail-YourBooking-col">
                                Passenger information
                            </div>
                            <div class="starail-YourBooking-col-price">
                                Price
                            </div>
                        </div>
                        <asp:Repeater ID="RptPassDetails" runat="server">
                            <ItemTemplate>
                                <div class="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--closed">
                                    <div class="starail-YourBooking-mobileTrigger starail-u-hideDesktop">
                                        <a class="" href="#">
                                            <%#Eval("ProductDesc")%>:
                                            <br />
                                            <span class="starail-YourBooking-light">
                                                <%#Eval("Traveller")%></span> <i class="starail-Icon-chevron-up"></i></a>
                                    </div>
                                    <div class="starail-u-hideMobile">
                                        <p class="starail-YourBooking-title">
                                            <%#Eval("ProductDesc")%>
                                        </p>
                                        <p>
                                            <%#Eval("Traveller")%>
                                        </p>
                                        <asp:HiddenField runat="server" ID="hdnPass" Value='<%#Eval("ProductDesc")+"**"+Eval("Traveller")%>' />
                                    </div>
                                    <div class="starail-YourBooking-col  starail-YourBooking-col--mobileFullWidth">
                                        <p class="starail-u-hideDesktop">
                                            Countries
                                        </p>
                                        <p>
                                            <%#Eval("Countries")%>
                                        </p>
                                        <asp:HiddenField runat="server" ID="hdnCountries" Value='<%#Eval("Countries")%>' />
                                    </div>
                                    <div class="starail-YourBooking-col  starail-YourBooking-col--mobileFullWidth">
                                        <p class="starail-u-hideDesktop">
                                            Validity:
                                        </p>
                                        <p>
                                            <%#Eval("Validity")%>
                                        </p>
                                        <asp:HiddenField runat="server" ID="hdnValidity" Value='<%#Eval("Validity")%>' />
                                    </div>
                                    <div class="starail-YourBooking-col starail-YourBooking-col-center">
                                        <p>
                                            <span class="starail-u-hideDesktop">Protected: </span>
                                            <%#Eval("Protected").ToString() == "Yes" ? "<i class='starail-Icon-shield'></i>": ""%>
                                            <%#Eval("ProtectedAmount")%>
                                            <span class="starail-u-visuallyHiddenDesktop">
                                                <%#Eval("Protected")%></span>
                                        </p>
                                        <asp:HiddenField runat="server" ID="hdnProtected" Value='<%#Eval("ProtectedAmount")+"**"+Eval("Protected")%>' />
                                    </div>
                                    <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                                        <p>
                                            <span class="starail-u-hideDesktop">Passenger information: </span><span class="starail-u-hideMobile">Passenger (<%#Eval("Trvname")%>):
                                                <br />
                                            </span>
                                            <%#Eval("Passengerinfo")%><span class="starail-u-hideDesktopInline">(1
                                                <%#Eval("Trvname")%>)</span>
                                        </p>
                                        <asp:HiddenField runat="server" ID="hdnPassenger" Value='<%#"Passenger 1 ("+Eval("Trvname")+"):**"+Eval("Passengerinfo")%>' />
                                    </div>
                                    <div class="starail-YourBooking-col-price">
                                        <p>
                                            <%=currency%><%#Eval("Price")%>
                                        </p>
                                        <asp:HiddenField runat="server" ID="hdnPrice" Value='<%#Eval("Price")%>' />
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="starail-YourBooking-table" runat="server" id="DivP2PSale">
                        <div class="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--header starail-u-hideMobile">
                            <div>
                                Train
                            </div>
                            <div>
                                Departs
                            </div>
                            <div>
                                Arrives
                            </div>
                            <div class="starail-YourBooking-col-center">
                                Class
                            </div>
                            <div class="">
                                Passenger information
                            </div>
                            <div class="starail-YourBooking-col-price">
                                Price
                            </div>
                        </div>
                        <asp:Repeater ID="RptP2PDetails" runat="server">
                            <ItemTemplate>
                                <div class="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--closed">
                                    <div class="starail-YourBooking-mobileTrigger starail-u-hideDesktop">
                                        <a class="" href="#"><span class="starail-YourBooking-light">
                                            <%#Eval("Jolurny")%>
                                        </span>
                                            <%# isEvolviBooking.ToString() == "False" ? Eval("Train") : string.Empty%>
                                            <i class="starail-Icon-chevron-up"></i></a>
                                    </div>
                                    <div class="starail-u-hideMobile">
                                        <p class="starail-YourBooking-title">
                                            <%#Eval("Jolurny")%>
                                        </p>
                                        <p>
                                            <%# isEvolviBooking.ToString() == "False" ? Eval("Train") : string.Empty%>
                                        </p>
                                        <asp:HiddenField runat="server" ID="hdnTrain" Value='<%# isEvolviBooking.ToString() == "False"?Eval("Jolurny")+"**"+Eval("Train"):Eval("Jolurny")%>' />
                                    </div>
                                    <div class="starail-YourBooking-col">
                                        <p class="starail-u-hideDesktop">
                                            Departs:
                                        </p>
                                        <p>
                                            <%#Eval("Departs")%>
                                        </p>
                                        <p>
                                            <%#Eval("DepartsStation")%>
                                        </p>
                                        <asp:HiddenField runat="server" ID="hdnDeparts" Value='<%#Eval("Departs")+"**"+Eval("DepartsStation")%>' />
                                    </div>
                                    <div class="starail-YourBooking-col">
                                        <p class="starail-u-hideDesktop">
                                            Arrives:
                                        </p>
                                        <p>
                                            <%#Eval("Arrives")%>
                                        </p>
                                        <p>
                                            <%#Eval("ArrivesStation")%>
                                        </p>
                                        <asp:HiddenField runat="server" ID="hdnArrives" Value='<%#Eval("Arrives")+"**"+Eval("ArrivesStation")%>' />
                                    </div>
                                    <div class="starail-YourBooking-col-center">
                                        <p>
                                            <span class="starail-u-hideDesktop">Class: </span>
                                            <%#Eval("Class")%>
                                        </p>
                                        <asp:HiddenField runat="server" ID="hdnClass" Value='<%#Eval("Class")%>' />
                                    </div>
                                    <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                                        <p>
                                            <span class="starail-u-hideDesktop">Passenger information: </span><span class="starail-u-hideMobile">Passenger 1 (<%#Eval("Trvname")%>):</span>
                                            <br />
                                            <%#Eval("Passengerinfo")%>
                                            <span class="starail-u-hideDesktopInline">(1
                                                <%#Eval("Trvname")%>)</span>
                                        </p>
                                        <asp:HiddenField runat="server" ID="hdnPassenger" Value='<%#"Passenger 1 ("+Eval("Trvname")+"):**"+Eval("Passengerinfo")%>' />
                                    </div>
                                    <div class="starail-YourBooking-col-price">
                                        <p>
                                            <%=currency%><%#Eval("Price")%>
                                        </p>
                                        <asp:HiddenField runat="server" ID="hdnPrice" Value='<%#Eval("Price")%>' />
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="starail-u-hideMobile">
                        <br />
                    </div>
                    <div class="starail-YourBooking-extraRow">
                        <p class="starail-YourBooking-extraRow-title">
                            Shipping Amount:
                        </p>
                        <p class="starail-YourBooking-extraRow-price">
                            <%=currency%><asp:Label runat="server" ID="lblShippingAmount"></asp:Label>
                        </p>
                    </div>
                    <div class="starail-YourBooking-extraRow">
                        <p class="starail-YourBooking-extraRow-title">
                            Booking Fee:
                        </p>
                        <p class="starail-YourBooking-extraRow-price">
                            <%=currency%><asp:Label runat="server" ID="lblBookingFee"></asp:Label>
                        </p>
                    </div>
                    <div class="starail-YourBooking-extraRow" id="OrderDiscount1" runat="server">
                        <p class="starail-YourBooking-extraRow-title">
                            Discount Amount:
                        </p>
                        <p class="starail-YourBooking-extraRow-price">
                            <%=currency%><asp:Label runat="server" ID="lblDiscount"></asp:Label>
                        </p>
                    </div>
                    <div class="starail-YourBooking-extraRow" runat="server" id="hidelblAdminfee">
                        <p class="starail-YourBooking-extraRow-title">
                            Admin Fee:
                        </p>
                        <p class="starail-YourBooking-extraRow-price">
                            <%=currency%><asp:Label runat="server" ID="lblAdminFee"></asp:Label>
                        </p>
                    </div>
                    <div class="starail-YourBooking-extraRow" runat="server" id="otherchargesText">
                        <p class="starail-YourBooking-extraRow-title">
                            Other Charges:
                        </p>
                        <p class="starail-YourBooking-extraRow-price">
                            <%=currency%><asp:Label runat="server" ID="lblOtherCharges"></asp:Label>
                        </p>
                    </div>
                    <div class="starail-YourBooking-extraRow">
                        <p class="starail-YourBooking-extraRow-title">
                            Net Total:
                        </p>
                        <p class="starail-YourBooking-extraRow-price">
                            <%=currency%><asp:Label runat="server" ID="lblNetTotal"></asp:Label>
                        </p>
                    </div>
                    <p class="starail-YourBooking-totalPrice">
                        Total Price:
                        <asp:Label CssClass="starail-YourBooking-totalPrice-amount LabelMargin" ID="lblGrandTotal"
                            runat="server"></asp:Label>
                    </p>
                </div>
                <!-- / Passes -->
                <div class="starail-BookingDetails-submit starail-u-cf">
                    <asp:Button ID="btnpaycompliteorder" runat="server" Text="Confirm payment of" class="starail-Button starail-Button--fullMobile  starail-Button--rightSubmit ButtonMargin"
                        ValidationGroup="Agent" OnClick="btnpaycompliteorder_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
