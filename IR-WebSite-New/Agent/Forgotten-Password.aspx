﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Forgotten-Password.aspx.cs"
    MasterPageFile="~/STAPublic.master" Inherits="Agent_Forgotten_Password" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%=script%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="starail-Login-wrapper login_interail">
        <div class="starail-Box starail-Login">
            <div class="starail-Box-content starail-Box-content--small">
                <h2 class="starail-Login-title starail-Login-title--wide">
                    Enter your email address and username and we'll send you a password reminder</h2>
                <div class="starail-Form-row starail-Form-row--hasIcon">
                    <label for="emailAddress" class="starail-Form-label starail-u-visuallyHidden">
                        Email</label>
                    <asp:TextBox ID="txtEmail" CssClass="starail-Form-input" runat="server" placeholder="Email" />
                    <asp:RequiredFieldValidator ID="reqvalerror2" ControlToValidate="txtEmail" runat="server"
                        ValidationGroup="FLoginForm" Display="Dynamic" />
                    <asp:RegularExpressionValidator ID="reqcustvalerrorBA3" runat="server" Display="Dynamic"
                        ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ValidationGroup="FLoginForm" />
                    <i class="starail-Icon-email"></i><span class="starail-Form-errorText">Please enter
                        a valid email address</span>
                </div>
                <div class="starail-Form-row starail-Form-row--hasIcon">
                    <label for="username" class="starail-Form-label starail-u-visuallyHidden">
                        Username</label>
                    <asp:TextBox ID="txtUser" CssClass="starail-Form-input" runat="server" placeholder="Username" />
                    <asp:RequiredFieldValidator ID="reqvalerror1" ControlToValidate="txtUser" runat="server"
                        ValidationGroup="FLoginForm" Display="Dynamic" />
                    <i class="starail-Icon-username"></i><span class="starail-Form-errorText"></span>
                </div>
                <div class="starail-Form-row starail-Login-forgottenBtn">
                    <asp:Button ID="btnFSubmit" ValidationGroup="FLoginForm" Text="Request a new password"
                        CssClass="starail-Button starail-Button--full starail-Button--login" OnClick="btnFSubmit_Click"
                        runat="server" />
                </div>
                <p class="starail-Login-rememberedPw">
                    <a href="Default.aspx">Remembered your password? Log in.</a>
                </p>
            </div>
        </div>
    </div>
</asp:Content>
