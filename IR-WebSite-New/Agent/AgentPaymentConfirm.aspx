﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="AgentPaymentConfirm.aspx.cs" Inherits="Agent_AgentPaymentConfirm" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        window.universal_variable = 
        {           
            version: "1.1.0"
        }; 
        window.universal_variable.transaction =<%=products %>;
    </script>
    <%=script%>
    <style type="text/css">
        .PrintQueueNew
        {
            margin: 0 2.857rem 0.943rem;
        }
        .starail-Grid
        {
            padding: 0px !important;
        }
        .buttonwidth
        {
            width: 28%;
        }
        @media only screen and (max-width: 639px)
        {
            .buttonwidth
            {
                width: 100%;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <iframe height="1" width="1" frameborder="0" scrolling="no" runat="server" id="Trackiframe"
        name="cj_conversion"></iframe>
    <asp:HiddenField ID="hdnNetPrice" runat="server" />
    <asp:ScriptManager ID="scm1" runat="server">
    </asp:ScriptManager>
    <div class="starail-Grid starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Banner">
                <div class="starail-Banner-content">
                    <h1 class="starail-Banner-title starail-Banner-primaryTitle">
                        Thanks!</h1>
                </div>
                <img alt="Banner Alt" class="starail-Banner-image" id="img_Banner" runat="server" />
            </div>
        </div>
    </div>
    <div class="starail-PrintQueue PrintQueueNew" id="DivStock" runat="server">
        <div class="starail-PrintQueue-label">
            <label for="starail-printqueue" class="">
                Add pass(es) to a print queue:</label>
        </div>
        <div class="starail-PrintQueue-inputs">
            <div class="starail-PrintQueue-select">
                <asp:DropDownList ID="ddlPrntQ" runat="server" CssClass="starail-Form-select" />
                <asp:RequiredFieldValidator ID="reqvalerror1" runat="server" ControlToValidate="ddlPrntQ"
                    ValidationGroup="vp" InitialValue="-1" />
            </div>
            <asp:Button ID="btnSend" runat="server" CssClass="starail-Button starail-PrintQueue-btn buttonwidth"
                Text="Add to Queue" ValidationGroup="vp" OnClick="btnSend_Click" />
            <span id="Span_PassesText" visible="false" runat="server" style="color: #bd3c3c;">Pass(es)
                sent</span>
        </div>
    </div>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="tblOrderInfo">
        <div class="starail-OrderInfo-wrapper">
            <a class="starail-Button starail-OrderInfo-btn" onclick="DownloadTicket();" id="Anchor_Pdf"
                runat="server" visible="false"><i class="starail-Icon-ticket"></i>Download PDF Ticket</a>
            <div class="starail-OrderInfo starail-OrderInfo--confirmation">
                <h2 class="starail-OrderInfo-title">
                    Order Information</h2>
                <div class="starail-OrderInfo-section starail-OrderInfo-section--section1">
                    <div class="starail-OrderInfo-rowTitle">
                        Pay to:</div>
                    <div class="starail-OrderInfo-rowContent">
                        One Track Booking system</div>
                    <div class="starail-OrderInfo-rowTitle">
                        Email address:</div>
                    <div class="starail-OrderInfo-rowContent">
                        <asp:Label ID="lblEmailAddress" runat="server" Text="" /></div>
                    <div class="starail-OrderInfo-rowTitle">
                        Delivery address:</div>
                    <div class="starail-OrderInfo-rowContent">
                        <asp:Label ID="lblDeliveryAddress" runat="server" Text="" /></div>
                </div>
                <div class="starail-OrderInfo-section starail-OrderInfo-section--section2">
                    <div class="starail-OrderInfo-rowTitle">
                        Order date:</div>
                    <div class="starail-OrderInfo-rowContent">
                        <asp:Label ID="lblOrderDate" runat="server" Text="" /></div>
                    <div class="starail-OrderInfo-rowTitle">
                        Order no:</div>
                    <div class="starail-OrderInfo-rowContent">
                        <asp:Label ID="lblOrderNumber" runat="server" Text="" /></div>
                    <div class="starail-OrderInfo-rowTitle">
                        Reference / folder no:</div>
                    <div class="starail-OrderInfo-rowContent">
                        <asp:Label ID="lblStaRefNo" runat="server" Text="" /></div>
                    <asp:Repeater ID="rptOrderInfo" runat="server" OnItemDataBound="rptOrderInfo_ItemDataBound">
                        <ItemTemplate>
                            <div class="starail-OrderInfo-rowTitle">
                                Journey
                                <%# Container.ItemIndex + 1 %>:
                            </div>
                            <div class="starail-OrderInfo-rowContent Journeyrow">
                                <%#Eval("ProductDesc")%>
                            </div>
                            <div class="starail-OrderInfo-rowTitle">
                                Name:
                            </div>
                            <div class="starail-OrderInfo-rowContent">
                                <%#Eval("Title")%>
                                <%#Eval("Name")%>
                            </div>
                            <div class="starail-OrderInfo-rowTitle" id="trNettPrice" runat="server">
                                <asp:Label ID="lblPrice" runat="server" />
                            </div>
                            <div class="starail-OrderInfo-rowContent" id="trNettPrice1" runat="server">
                                <%=currency %>
                                <%#Eval("Price").ToString()%>
                            </div>
                            <div class="starail-OrderInfo-rowTitle" id="trNettPricePass" runat="server">
                                Nett Price:
                            </div>
                            <div class="starail-OrderInfo-rowContent" id="trNettPricePass1" runat="server">
                                <%=currency %>
                                <%#Eval("NetPrice").ToString()%>
                            </div>
                            <div class="starail-OrderInfo-rowTitle">
                                Ticket Protection:
                            </div>
                            <div class="starail-OrderInfo-rowContent">
                                <%#(Eval("TktPrtCharge")) != "" ? "" + currency +" "+ Eval("TktPrtCharge")+"" : ""%>
                            </div>
                            <div class="starail-OrderInfo-rowTitle">
                                Commission Fee:
                            </div>
                            <div class="starail-OrderInfo-rowContent">
                                <%=currency %>
                                <%#(Eval("CommissionFee")) != null ? Eval("CommissionFee").ToString() : "0:00"%>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div class="starail-OrderInfo-rowTitle">
                        Gross price:</div>
                    <div class="starail-OrderInfo-rowContent">
                        <%=currency %>
                        <asp:Label ID="lblGrossPrice" runat="server" Text="" /></div>
                    <div class="starail-OrderInfo-rowTitle">
                        Shipping:</div>
                    <div class="starail-OrderInfo-rowContent">
                        <%=currency %>
                        <asp:Label ID="lblShippingAmount" runat="server" Text="" /></div>
                    <div class="starail-OrderInfo-rowTitle">
                        Admin Fee:</div>
                    <div class="starail-OrderInfo-rowContent">
                        <%=currency %>
                        <asp:Label ID="lblAdminFee" runat="server" Text="" /></div>
                    <div class="starail-OrderInfo-rowTitle">
                        Booking fee:</div>
                    <div class="starail-OrderInfo-rowContent">
                        <%=currency %>
                        <asp:Label ID="lblBookingFee" runat="server" Text="" /></div>
                    <div class="starail-OrderInfo-rowTitle">
                        Discount:</div>
                    <div class="starail-OrderInfo-rowContent">
                        <%=currency %>
                        <asp:Label ID="lblDiscount" runat="server" Text="" /></div>
                </div>
                <p id="trTrenItalia" runat="server" visible="false" style="padding-top: 20px;">
                    <b>IMPORTANT:</b> This PNR is issued as a ticket on depart and must be printed from
                    one of the self-service ticket machines located at all main Trenitalia stations
                    (which are green, white and red). Once you have printed your ticket you must validate
                    it in one of the validation machines located near to the entrance of each platform
                    (they are green and white with Trenitalia written on the front and usually mounted
                    on a wall), THIS MUST BE DONE BEFORE BOARDING YOUR TRAIN, failure to do so will
                    result in a fine.
                </p>
                <p class="starail-OrderInfo-totalPrice">
                    Total Price: <span class="starail-OrderInfo-totalPrice-amount">
                        <%=currency %>
                        <asp:Label ID="lblGrandTotal" runat="server" Text="" /></span></p>
                <asp:Button ID="btnPrintTicket" OnClick="btnPrintTicket_Click" runat="server" Text="Print Ticket"
                    CssClass="starail-Button starail-Button--fullMobile  starail-Button--rightSubmit"
                    Style="display: none;" ClientIDMode="Static" />
                <a href="#" id="Anchor_Pdfbottom" runat="server" class="starail-YourBooking-receipt"
                    target="_blank"><i class="starail-Icon-receipt"></i>Download Your Receipt</a>
            </div>
            <!-- / Tickets -->
        </div>
        <div class="starail-Grid starail-Grid--mobileFull">
            <div class="starail-Grid-col
            starail-Grid-col--nopadding">
                <a class="starail-Button starail-Button--blue starail-Button--continueShopping" href="../">
                    Continue Shopping</a>
            </div>
        </div>
    </asp:Panel>
    <script type="text/javascript">
        function DownloadTicket() {
            $("#btnPrintTicket").trigger('click');
        }
    </script>
</asp:Content>
