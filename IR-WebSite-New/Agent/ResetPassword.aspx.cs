﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Business;
using System.Configuration;

public partial class Agent_ResetPassword : System.Web.UI.Page
{
    readonly ManageUser _ManageUser = new ManageUser();
    readonly Masters _master = new Masters();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string SiteAdminUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string siteURL;
    private Guid _siteId;
    public string FaviconIcon = string.Empty;
    public string script = "<script></script>";
    public string SiteLogoUrl = string.Empty;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        try
        {
            if (Session["siteId"] != null)
            {
                _siteId = Guid.Parse(Session["siteId"].ToString());
                siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            }
        }
        catch (Exception ex) { throw ex; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            lblSuccessMsg.Text = string.Empty;
            QubitOperationLoad();
        }
        catch (Exception ex) { throw ex; }
    }

    public void QubitOperationLoad()
    {
        var lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request.Params["reqId"] != null)
            {
                var id = Guid.Parse(Request.Params["reqId"]);
                var admin = _db.tblAdminUsers.FirstOrDefault(x => x.ID == id);
                if (admin != null)
                {
                    if (!string.IsNullOrEmpty(admin.Forename) && txtnewpass.Text.ToLower().Contains(admin.Forename))
                        lblErrorMsg.Text = "Password should not contain first name.";
                    else if (!string.IsNullOrEmpty(admin.Surname) && txtnewpass.Text.ToLower().Contains(admin.Surname))
                        lblErrorMsg.Text = "Password should not contain last name.";
                    else
                    {
                        var result = _ManageUser.ChangeAgentPassword(id, txtconfpass.Text, txtFirstName.Text, txtLastName.Text);
                        if (result) Response.Redirect("Login");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }
}