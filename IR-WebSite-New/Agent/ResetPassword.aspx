﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResetPassword.aspx.cs" Inherits="Agent_ResetPassword"
    MasterPageFile="~/STAPublic.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%=script%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="starail-Login-wrapper login_interail">
        <div class="starail-Box starail-Login">
            <div class="starail-Box-content starail-Box-content--small">
                <h2 class="starail-Login-title starail-Login-title--wide">
                    Generate New Password</h2>
                <asp:Label ID="lblSuccessMsg" runat="server" CssClass="valdreq" ForeColor="#FF3300" />
                <asp:Label ID="lblErrorMsg" runat="server" CssClass="valdreq" ForeColor="red" />
                <div class="starail-Form-row starail-Form-row--hasIcon">
                    <label for="username" class="starail-Form-label starail-u-visuallyHidden">
                        First Name:</label>
                    <asp:TextBox ID="txtFirstName" CssClass="starail-Form-input" runat="server" MaxLength="50"
                        placeholder="First Name" />
                    <asp:RequiredFieldValidator ID="reqvalerror1" ControlToValidate="txtFirstName" runat="server"
                        ValidationGroup="FLoginForm" Display="Dynamic" />
                    <i class="starail-Icon-username"></i><span class="starail-Form-errorText"></span>
                </div>
                <div class="starail-Form-row starail-Form-row--hasIcon">
                    <label for="username" class="starail-Form-label starail-u-visuallyHidden">
                        Last Name:</label>
                    <asp:TextBox ID="txtLastName" runat="server" CssClass="starail-Form-input" MaxLength="50"
                        placeholder="Last Name" />
                    <asp:RequiredFieldValidator ID="reqvalerror2" ControlToValidate="txtLastName" runat="server"
                        ValidationGroup="FLoginForm" Display="Dynamic" />
                    <i class="starail-Icon-username"></i><span class="starail-Form-errorText"></span>
                </div>
                <div class="starail-Form-row starail-Form-row--hasIcon">
                    <label for="username" class="starail-Form-label starail-u-visuallyHidden">
                        New Password:</label>
                    <asp:TextBox ID="txtnewpass" CssClass="starail-Form-input" runat="server" TextMode="Password"
                        placeholder="New Password" MaxLength="20"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqvalerror3" ControlToValidate="txtnewpass" runat="server"
                        ValidationGroup="FLoginForm" Display="Dynamic" />
                    <asp:RegularExpressionValidator ID="reqcustvalerrorBA2" runat="server" ControlToValidate="txtnewpass"
                        Display="Dynamic" ValidationExpression="(?=^.{8,20}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                        ValidationGroup="FLoginForm" />
                    <i class="starail-Icon-password"></i><span class="starail-Form-errorText"></span>
                </div>
                <div class="starail-Form-row starail-Form-row--hasIcon">
                    <label for="username" class="starail-Form-label starail-u-visuallyHidden">
                        Confirm Password:</label>
                    <asp:TextBox ID="txtconfpass" runat="server" CssClass="starail-Form-input" TextMode="Password"
                        placeholder="Confirm Password" MaxLength="20"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqvalerror4" ControlToValidate="txtconfpass" runat="server"
                        ValidationGroup="FLoginForm" Display="Dynamic" />
                    <asp:RegularExpressionValidator ID="reqcustvalerrorBA3" runat="server" ControlToValidate="txtconfpass"
                        Display="Dynamic" ValidationExpression="(?=^.{8,20}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                        ValidationGroup="FLoginForm" />
                    <i class="starail-Icon-password"></i><span class="starail-Form-errorText"></span>
                </div>
                <div class="starail-Form-row starail-Form-row--hasIcon">
                    <b>Hint:</b>
                    <ul>
                        <li>1. Password should contain minimum 8 characters. </li>
                        <li>2. Password should have atleast 1 Upper or Lower case character, Numeric & Special
                            character. </li>
                        <li>3. Password should Not based on personal info. </li>
                    </ul>
                </div>
                <div class="starail-Form-row starail-Login-forgottenBtn">
                    <asp:Button ID="BtnSubmit" ValidationGroup="FLoginForm" Text="Submit" CssClass="starail-Button starail-Button--full starail-Button--login"
                        OnClick="BtnSubmit_Click" runat="server" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
