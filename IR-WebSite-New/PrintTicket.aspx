﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="PrintTicket.aspx.cs" Inherits="PrintTicket" %>

<%@ Register TagPrefix="uc" TagName="ucRightContent" Src="UserControls/ucRightContent.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        $("#lnkUrl").click(function () {
            $("#lnkUrl").next().find();
        });
    </script>
    <%=script %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
    <div class="starail-Grid starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Banner">
                <div class="starail-Banner-content">
                    <h1 class="starail-Banner-title starail-Banner-primaryTitle">
                        Enjoy Your Trip!</h1>
                </div>
                <img alt="Banner Alt" class="starail-Banner-image dots-header" id="img_Banner" runat="server" />
            </div>
        </div>
    </div>
    <div class="starail-Grid">
        <div class="starail-Grid-col">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="starail-YourBooking starail-YourBooking--confirmation">
                <h2>
                    Your ticket(s)</h2>
                <div class="starail-CalloutBox starail-CalloutBox--mobileFull" style="background: #fff">
                    <asp:Literal Text="Please click on the button below to download your PDF ticket(s)."
                        runat="server" ID="ltrTicketMsg" Visible="true"></asp:Literal>
                    <p style="margin: 10px; text-align: center;">
                        <asp:Repeater ID="rptBeNePrint" runat="server" OnItemDataBound="rptBeNePrint_ItemDataBound">
                            <ItemTemplate>
                                <a href='<%#Eval("URL")%>' target="_blank" id="lnkUrl" runat="server">
                                    <asp:Image ID="Image1" CssClass="scale-with-grid" alt="" border="0" runat="server"
                                        ImageUrl="images/download-ticket.png" /></a><br /><br />
                            </ItemTemplate>
                        </asp:Repeater>
                    </p>
                    Thanks for you order - have a great trip!<br />
                    <br />
                    If we can help you with anything else please do not hesitate to contact us.<br />
                    <br />
                    <br />
                    <br />
                    <i style="font-size: 13px">You'll need Adobe Reader to view your ticket, if you don't
                        already have it click below to download it.</i>
                    <p style="text-align: center;">
                        <a href="http://www.adobe.com/go/getreader" target="_blank" style="text-decoration: none;">
                            <img id="Image2" class="scale-with-grid" alt="" border="0" src="images/get_adobe_reader.gif" />
                        </a>
                    </p>
                </div>
            </div>
            <!-- / Tickets -->
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnExec" Value="0" />
</asp:Content>
