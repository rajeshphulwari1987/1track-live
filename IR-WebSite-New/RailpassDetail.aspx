﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RailpassDetail.aspx.cs" MasterPageFile="~/Site.master"
    Inherits="RailpassDetail" Culture="en-GB" %>

<%@ Register TagPrefix="uc" TagName="ucCountryLevel" Src="~/UserControls/ucCountryPass.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%=ActionsTags%>
    <%=SetActionsTags%>
    <%=IRScriptingTagJs%>
    <style type="text/css">
        .data-list-class-grid-td-secound
        {
            width: 34% !important;
        }
        .data-list-class-grid-td-first
        {
            width: 32% !important;
        }
        
        .grddatabind tr th
        {
            width: 31% !important;
            text-align: center !important;
        }
        @media (max-width: 767px)
        {
            .data-list-class-grid-td-first, .data-list-class-grid-td-secound
            {
                width: 100% !important;
            }
        }
 .starail-Tabs-notification.is-showing {
            z-index: 9;
        }
    </style>
    <style>
        a
        {
            color: #0645ad;
        }
        a:visited
        {
            color: #0b0080;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("ul.innertab li:first").addClass("starail-Tabs-tab--active").show();
            $("#GV1").css("display", "block");
            $("#MainContent_UpdatePanel1 div:first").css("display", "block");
        });
        function innertabActive() {
            $('#innertab').find('.linkbutton').trigger('click');
        }
        function getcurrentLinkPosition() {
            $(".innertab li").find('a').each(function () {
                var classmatch = $(this).attr('class').replace(/ /g, '');
                if (classmatch == 'linkButtonactive' || classmatch == 'linkButtonpie_first-childactive' || classmatch == 'linkButtonactivepie_first-child') {
                    $(this).trigger('click');
                }
            });
        }
        function AlertMessage(data) {
            alert(data);
        }
        function showmsg() {
            alert('Please select at least one Travel Validity.');
        }
        function GoRailPassPage() {
            window.history.back();
        }
        function GoBookingCartPage() {
            $("#btnBookNow").trigger('click');
        }
    </script>
    <%=script%>
    <style>
        .scrollbox .starail-Accordion-body
        {
            overflow-y: auto !important;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:HiddenField ID="hdnIsBritral" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnIstwinpass" runat="server" ClientIDMode="Static" />
    <asp:ScriptManager ID="scrpMng" runat="server" ScriptMode="Release" EnableCdn="true">
        <CompositeScript>
            <Scripts>
                <asp:ScriptReference Name="MicrosoftAjax.js" />
                <asp:ScriptReference Name="MicrosoftAjaxWebForms.js" />
            </Scripts>
        </CompositeScript>
    </asp:ScriptManager>
    <br />
    <div class="Breadcrumb">
        <asp:Literal runat="server" ID="litSeobreadcrumbs"></asp:Literal></div>
    <div class="starail-Grid starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-ProgressBar starail-ProgressBar-stage--stage1 starail-u-hideMobile">
                <div class="starail-ProgressBar-line">
                    <div class="starail-ProgressBar-progress">
                    </div>
                </div>
                <div class="starail-ProgressBar-stage starail-ProgressBar-stage1">
                    <div class="starail-ProgressBar-circle">
                    </div>
                    <p class="starail-ProgressBar-label">
                        Pass Selection</p>
                    <div class="starail-ProgressBar-subStage">
                    </div>
                </div>
                <div class="starail-ProgressBar-stage starail-ProgressBar-stage2">
                    <div class="starail-ProgressBar-circle">
                    </div>
                    <p class="starail-ProgressBar-label">
                        Booking Details</p>
                    <div class="starail-ProgressBar-subStage">
                    </div>
                </div>
                <div class="starail-ProgressBar-stage starail-ProgressBar-stage3">
                    <div class="starail-ProgressBar-circle">
                    </div>
                    <p class="starail-ProgressBar-label">
                        Checkout</p>
                </div>
            </div>
            <div class="starail-Section starail-Section--nopadding">
                <div class="starail-TrainOptions-mobileNav starail-u-hideDesktop">
                    <div class="">
                        <a class="starail-Button starail-Button--blue" style="font-size: 15px;" onclick="GoRailPassPage();">
                            <i class="starail-Icon-chevron-left"></i>Train Selection</a> <a class="starail-Button starail-TrainOptions-mobileNav-rtnJourney starail-rtnJourney-trigger starail-Button--blue is-disabled"
                                style="font-size: 15px;" onclick="GoBookingCartPage();">Checkout <i class="starail-Icon-chevron-right">
                                </i></a>
                    </div>
                </div>
                <h1 class="starail-Type-PageHeader">
                    <div runat="server" id="getproductname">
                    </div>
                </h1>
                <h3 class="starail-Banner-title freeday " style="text-align: center; color: #fb4f14;
                    text-transform: none;">
                    <div runat="server" id="divspecialOfferText">
                    </div>
                </h3>
                <a href='<%=siteURL %>' class="starail-u-hideDesktop starail-Button--mobileBackButton">
                    <i class="starail-Icon-chevron-left"></i>Choose another pass</a>
                <section class="starail-CalloutBox" id="div_GreatPassDescription" runat="server">
                </section>
                <a href='<%=siteURL %>' class="starail-Button starail-Button--blue starail-Button--fullCallout starail-u-hideMobile">
                    <i class="starail-Icon-chevron-left"></i>Choose another pass</a>
            </div>
            <div runat="server" id="divucCountryLevel" visible="false" class="starail-Section starail-Section--nopadding">
                <div class="starail-Tabs ">
                    <uc:ucCountryLevel ID="ucCountryLevel" runat="server" />
                </div>
            </div>
            <div id="dvOtherCountry" runat="server">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="starail-Section starail-Section--nopadding">
                            <div class="starail-Tabs ">
                                <ul id="innertab" class="innertab starail-Tabs-tabs starail-Tabs-passSelectionTabs">
                                    <asp:HiddenField ID="hdnGlobelTrName" runat="server" Value='' />
                                    <asp:Repeater ID="rptTraveller" runat="server">
                                        <ItemTemplate>
                                            <li class="starail-Tabs-tab ">
                                                <asp:LinkButton ID="lnkTraveller" INumber='<%# Container.ItemIndex + 1 %>' CssClass="linkButton js-starail-Tabs-trigger"
                                                    runat="server" CommandName="PriceList" CommandArgument='<%#Eval("ID")%>' Text='<%#Eval("Name")+"<br/> ("+Eval("Age")+" YEARS )"%>'
                                                    TxtName='<%#Eval("Name")%>' OnClientClick="ChangeLinkPosition(this); return false;"
                                                    ToolTip='<%#Eval("Name").ToString().Contains("Saver") ? "Travelling together? See below point 3, Eligibility for Saving" :string.Empty%>'>    
                                                </asp:LinkButton>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <li class="starail-Tabs-tab starail-Tabs-tab--passSelection starail-u-hideMobile">
                                        <asp:Button ID="btnsubmits" runat="server" class="starail-Button IR-GaCode" Width="205px"
                                            Text="Next: Booking Details" OnClick="btnBookNow_Click" OnClientClick="return checkSaverSelect();">
                                        </asp:Button>
                                    </li>
                                </ul>
                                <div class="starail-Tabs-wrapper starail-Tabs-wrapper--nopadding starail-Tabs-wrapper--dropShadowDesktop">
                                    <div class="starail-Grid starail-Grid--mobileFull">
                                        <asp:Repeater ID="rptTravellerGrid" runat="server" OnItemDataBound="rptTravellerGrid_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="country-block-inner starail-Tabs-content js-notificationCountSection"
                                                    style="display: none;" id="GV<%# Container.ItemIndex + 1 %>">
                                                    <asp:HiddenField ID="hdnT" ClientIDMode="Static" Value='<%#Eval("Name")%>' runat="server">
                                                    </asp:HiddenField>
                                                    <asp:Label ID="lblID" runat="server" Text='<%#Eval("ID")%>' Visible="false"></asp:Label>
                                                    <div class="starail-PassChoiceTable js-accordon leftpaddingrailpass">
                                                        <asp:GridView ID="grdPrice" runat="server" AutoGenerateColumns="true" OnRowDataBound="grdPrice_RowDataBound"
                                                            CssClass="grddatabind starail-PassChoiceTable js-accordon" Width="100%">
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <div class="starail-PassChoiceTable-footer">
                                        <div class="starail-Grid starail-Grid--mobileFull leftpaddingrailpass">
                                            <div class="starail-PassChoiceTable-protect" runat="server" clientidmode="Static" style="display:none;" id="TPmessageDiv">
                                                <h3>
                                                    <i class="starail-Icon-shield"></i>Protect your pass</h3>
                                                <p>
                                                    Make sure you’re covered in case it gets lost or stolen for just
                                                    <%=currency%><%=TicketProtection %>
                                                    per pass! Tick the ‘Pass protection’ box when you check out.</p>
                                            </div>
                                            <div class="starail-PassChoiceTable-totalContainer">
                                                <div id="bindLst" style="display: none;">
                                                </div>
                                                <%-- <div id="divTotal"  class="starail-PassChoiceTable-description" style="float: right;margin-right: 34px;"> </div>--%>
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td id="divText" class="starail-PassChoiceTable-description">
                                                            </td>
                                                            <td id="divTotal" class="starail-PassChoiceTable-total">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="starail-Section starail-Section--nopadding starail-Section-nextButtonSection textcenter">
                            <asp:Button ID="btnBookNow" runat="server" class="starail-Button IR-GaCode" Text="Next: Booking Details"
                                Width="205px" OnClientClick="return checkSaverSelect();" OnClick="btnBookNow_Click">
                            </asp:Button>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="starail-Section starail-Section--nopadding">
                <div class="starail-Tabs ">
                    <ul id="divBlock" class="starail-Accordion js-accordon">
                        <li id="divDesc" class="starail-Accordion-item js-accordonItem  is-open">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    1. Description
                                </h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent" id="divDescription" runat="server">
                                </div>
                            </div>
                        </li>
                        <li id="divCmsCon" class="starail-Accordion-item js-accordonItem">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    2. Eligibility
                                </h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent" id="divEligibility" runat="server">
                                </div>
                            </div>
                        </li>
                        <li id="divEligforSaver" class="starail-Accordion-item js-accordonItem">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    3. Eligibility for Saver
                                </h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent" id="divEligibilityforSaver" runat="server">
                                </div>
                            </div>
                        </li>
                        <li id="divCmsBook" class="starail-Accordion-item js-accordonItem">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    4. Discount</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent" id="divDiscount" runat="server">
                                </div>
                            </div>
                        </li>
                        <li id="divCmsValid" class="starail-Accordion-item js-accordonItem">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    5. Validity</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent" id="divValidity" runat="server">
                                </div>
                            </div>
                        </li>
                        <li id="divCmsFare" class="starail-Accordion-item js-accordonItem">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    6. Terms and Conditions</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent" id="divTerm" runat="server">
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="starail-Section starail-Section--backToTop">
                        <a href="#" class="starail-backToTop"><i class="starail-Icon-arrow-up"></i>Back to top</a>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hdnCurrencySign" runat="server" />
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DisplayAfter="0" DynamicLayout="True">
        <ProgressTemplate>
            <div class="modalBackground progessposition">
            </div>
            <div class="progess-inner2">
                UPDATING RESULTS...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <script type="text/javascript">
        var ArryAdultSaver = new Array();
        $(document).ready(function () {
            callabc();
            loadpass();
        });
        function loadpass() {
            var currSign = $("#MainContent_hdnCurrencySign").val();
            var Gtotal = 0;
            var travlertotal = '';
            var Amt = 0;
            var tbldtl = "<table class='bindlsttbl'><tr><th>Pass</th><th>Validity</th><th>Quantity</th><th>Price</th><th></th></tr>";
            $(".linkButton").each(function () {
                var travlercount = 0;
                var Travelername = '';
                var PassName = $("#MainContent_getproductname").text();
                var strPass = $(this).attr("txtname");
                var linobj = $(this);
                var isnotzero = 0;
                $("#GV" + $(this).attr("inumber")).find("select").each(function () {
                    var i = parseInt($(this).val());
                    var PAmt = parseFloat($(this).parent().find(".ltrName").text());
                    var subT = 0;
                    if (i > 0) {
                        isnotzero = 1;
                        travlercount = travlercount + i;
                        Travelername = $(this).parent().parent().parent().parent().parent().parent().parent().parent().find("#hdnT").val();
                        var SelId = $(this).attr('id');
                        var validity = $(this).parent().parent().parent().find("td:first").text();
                        subT = parseFloat(i) * PAmt;
                        Amt = Amt + subT;
                        tbldtl = tbldtl + "<tr><td><b>" + PassName + "</b></br>" + strPass + "</td><td>" + validity + "</td><td>" + i + "</td><td>" + PAmt + "</td><td><img src='images/btn-cross.png' class='icon-close scale-with-grid' border='0' id='" + SelId + "' onclick='bindLstDeleteItem(this)'/></td></tr>";
                        Gtotal = parseFloat(Amt);
                    }
                    if (isnotzero == 0)
                        linobj.find('.starail-Tabs-notification').remove();
                    else {
                        linobj.find('.starail-Tabs-notification').remove();
                        linobj.append("<i class='starail-Tabs-notification js-notification is-showing'>" + travlercount + "</i>");
                    }
                });
                if (travlercount != 0)
                    travlertotal = (travlertotal.length > 0 ? travlertotal + " + " : travlertotal) + travlercount + " " + Travelername;
            });
            tbldtl = tbldtl + "</table>";
            var Blanktable = "<table class='bindlsttbl'><tr><th>Pass</th><th>Validity</th><th>Quantity</th><th>Price</th><th></th></tr></table>";
            if (tbldtl == Blanktable) {
                $(".datadiv2").hide();
            }
            else {
                $(".datadiv2").show();
            }
            $('#bindLst').html(tbldtl);
            $('#divText').html("Total price for " + travlertotal);
            $('#divTotal').html(currSign + Gtotal.toFixed(2));
        }
        /* Check min and max select of saver in class */
        function checkSaverSelect() {
            var CountSaverMax = 0;
            var resultflag = false;
            var SaverName = "";
            var SaverPassesArray = [];
            $(".linkButton").each(function () {
                var strPass = $(this).attr("txtname");
                var res = strPass.match(/saver/i);
                if (res) {
                    SaverName = $(this).html().split('<')[0] + " and " + SaverName;
                    $("#GV" + $(this).attr("inumber")).find("select").each(function () {
                        var i = parseInt($(this).val());
                        if (i > 0) {
                            var datavalc = $.trim($(this).attr('id'));
                            var SplitAc = datavalc.split('ddlID');
                            var SplitBc = SplitAc[1].split('_');
                            var strClassIDNumber = SplitBc[0];
                            SaverPassesArray.push($.trim($(this).parent().parent().parent().find("td:first").text()) + strClassIDNumber + "-" + i);
                        }
                    });
                }
            });
            var i = 0;
            var j = SaverPassesArray.length;
            if (j == 0) {
                resultflag = true;
            }
            else if (j == 1) {
                var SaverName1 = SaverPassesArray[0].split('-');
                var Fnum1 = parseInt($.trim(SaverName1[1]));
                if ((Fnum1 >= 2 && Fnum1 <= 5) || Fnum1 == 0) {
                    resultflag = true;
                }
                else {
                    resultflag = false;
                }
            }
            else {
                for (i = 0; i < j; i++) {
                    var k = 0;
                    var sameOther = false;
                    for (k = 0; k < j; k++) {
                        if (i != k) {
                            var strFN = SaverPassesArray[i].split('-');
                            var strSN = SaverPassesArray[k].split('-');
                            if (strFN[0] == strSN[0]) {
                                sameOther = true;
                                var num1 = parseInt($.trim(strFN[1]));
                                var num2 = parseInt($.trim(strSN[1]));
                                var total = num1 + num2;
                                if ((total >= 2 && total <= 5) || total == 0) {
                                    resultflag = true;
                                }
                                else {
                                    resultflag = false;
                                    break;
                                }
                            }
                        }
                    }
                    if (sameOther == false) {
                        var SaverName2 = SaverPassesArray[i].split('-');
                        var Fnum2 = parseInt($.trim(SaverName2[1]));
                        if (!((Fnum2 >= 2 && Fnum2 <= 5) || Fnum2 == 0)) {
                            resultflag = false;
                            break;
                        }
                        else {
                            resultflag = true;
                        }
                    }
                }
            }
            if (resultflag == true) {
                return true;
            }
            else {
                alert('You can select minimum 2 or maximum 5 number of people for ' + SaverName + ' ticket on same travelling class.');
                return false;
            }
        }
        /* Total and saver condition Rajesh */
        function callabc(e) {
            var FreeChildCount = 2; //Default
            var IfSenior = false;
            var IsBritrail = $("#hdnIsBritral").val() == "True";
            var array = new Array();
            var arrayAdult = new Array();
            var idx = 0;
            var strShow = "";
            var str = '';
            var Gtotal = 0;
            var currSign = $("#MainContent_hdnCurrencySign").val();
            var CountSaverMax = 0;
            var SaverName = "";
            var selID = e;
            var flag = false;
            if (selID != null) {
                var data = $(e).attr("id");
                var SplitAA = data.split('ddlID');
                var SplitBB = SplitAA[1].split('_');
                var strClassIDNumber = SplitBB[0];
                var selIDSiblingFirstColumn = $("#" + $.trim(selID.id.toString())).parent().parent().parent().find("td:first").text() + strClassIDNumber;
                /*default*/
                $(".linkButton").each(function () {
                    var strPass = $(this).attr("txtname");
                    var resAdult = strPass.match(/Adult/i);
                    var res = strPass.match(/saver/i);
                    if (IsBritrail) {
                        FreeChildCount = 1;
                        IfSenior = (strPass.match(/Senior/i));
                        if (resAdult)
                            resAdult = !strPass.match(/saver/i);
                    }
                    if (res) {
                        SaverName = $(this).text() + " and " + SaverName;
                        $("#GV" + $(this).attr("inumber").toString()).find("select").each(function () {
                            var i = parseInt($(this).val());
                            if (i > 0) {
                                var dataval = $.trim($(this).attr('id'));
                                var SplitA = dataval.split('ddlID');
                                var SplitB = SplitA[1].split('_');
                                var strClassIDNumber1 = SplitB[0];
                                var casd = $(this).parent().parent().parent().find("td:first").text() + strClassIDNumber1;
                                if ($.trim(casd) == $.trim(selIDSiblingFirstColumn)) {
                                    CountSaverMax = CountSaverMax + i;
                                }
                            }
                        });
                    }
                    /* ##Start## Only for free child 1(Eurail). adult,adult saver 2(Britrail). adult,Senior */
                    if (resAdult || IfSenior) {
                        $("#GV" + $(this).attr("inumber")).find("select").each(function () {
                            var i = parseInt($(this).val());
                            var dataval = $.trim($(this).attr('id'));
                            var SplitA = dataval.split('ddlID');
                            var SplitB = SplitA[1].split('_');
                            var strClassIDNumber1 = SplitB[0];
                            var casd = $(this).parent().parent().parent().find("td:first").text() + strClassIDNumber1;
                            if ($.trim(casd) == $.trim(selIDSiblingFirstColumn)) {
                                var GRPPass = ($("#hdnIstwinpass").val() === 'True');
                                if (strPass.length == 5 && GRPPass && (i > 0) && (i % 2 == 1)) {
                                    alert("You must select a minimum number of 2 adults for a twin pass in the same class and validity. Twin Passes can only be sold in multiples of 2.");
                                    $(this).val('0');
                                }
                                var countselval = 0;
                                for (var G = 0; G < ArryAdultSaver.length; G++) {
                                    var sptarr = ArryAdultSaver[G].split('ñ');
                                    if (sptarr[0] == casd) {
                                        var spltid = $(this).attr('id').split("_");
                                        var idsadult = '_' + spltid[4] + '_' + spltid[5];
                                        $("[id*=" + idsadult + "]").each(function () {
                                            var spltid = $(this).attr('id').split("_");
                                            var hdrtxt = $("#MainContent_rptTraveller_lnkTraveller_" + spltid[3]).text();
                                            resAdult = hdrtxt.match(/Adult/i);
                                            if (IsBritrail) {
                                                IfSenior = (hdrtxt.match(/Senior/i));
                                                if (resAdult)
                                                    resAdult = !hdrtxt.match(/saver/i);
                                            }
                                            if ((resAdult || IfSenior) && $(this).val() != '') {
                                                countselval += parseInt($(this).val());
                                            }
                                        });
                                        ArryAdultSaver.splice(G, 1);
                                    }
                                }
                                ArryAdultSaver[ArryAdultSaver.length] = casd + 'ñ' + parseInt(countselval);
                            }
                        });
                        /*
                        for (var ko = 0; ko < ArryAdultSaver.length; ko++) {
                        console.log(ArryAdultSaver[ko]);
                        }
                        console.log("||||||||||||||||******************************************||||||||||||||||");
                        */
                        /*********/
                        var isfamilypass = $(".starail-Tabs-tab--active").find('a').text().match(/Family/i);
                        if (isfamilypass)
                            FreeChildCount = 8;
                        $.each(($('select[child=child]')), function () {
                            var isvalid = true;
                            var i = parseInt($(this).val());
                            if (i > 0) {
                                var dataval = $.trim($(this).attr('id'));
                                var SplitA = dataval.split('ddlID');
                                var SplitB = SplitA[1].split('_');
                                var strClassIDNumber1 = SplitB[0];
                                var tdtxt = $(this).parent().parent().parent().find("td:first").text() + strClassIDNumber1;
                                if (ArryAdultSaver.length > 0) {
                                    for (var G = 0; G < ArryAdultSaver.length; G++) {
                                        var arrmatch = ArryAdultSaver[G].split('ñ');
                                        if (arrmatch[0] == tdtxt) {
                                            var adultsaverno = parseInt(arrmatch[1]) * FreeChildCount;
                                            if (i > adultsaverno) {
                                                $(this).val('0');
                                                var GRPPass = ($("#hdnIstwinpass").val() === 'True');
                                                if (GRPPass) {
                                                    alert("A maximum of 2 free child passes are allowed per adult, for additional children please purchase a normal German Rail Pass i.e. non-twin pass, in the Youth category.");
                                                }
                                                else {
                                                    if (IsBritrail)
                                                        alert('With each Adult, or Senior pass (not Adult Saver, Child or Youth) you are allowed x1 Child for free pass.');
                                                    else if (isfamilypass)
                                                        alert('To be eligible for the Family Card you need 1 adult pass of the same validity for every 8 child passes.');
                                                    else
                                                        alert("A maximum of 2 child passes is allowed per adult, for additional children please purchase a Youth pass.");
                                                }
                                            }
                                            isvalid = false;
                                        }
                                    }
                                    if (isvalid) {
                                        $(this).val('0');
                                        childalert(IsBritrail);
                                    }
                                }
                                else {
                                    $(this).val('0');
                                    childalert(IsBritrail);
                                }
                            }
                        });
                        /*********/
                    }
                    /*##End##*/
                });
                var string = $(selID).parent().parent().parent().parent().parent().parent().parent().parent().find("#hdnT").val();
                var result = string.match(/saver/i);
                if (result) {
                    if (CountSaverMax > 5) {
                        $(selID).val('0');
                        alert('Maximum number of people for ' + SaverName + ' ticket on same travelling class.');
                        flag = true;
                    } else {
                        flag = true;
                    }
                } else {
                    flag = true;
                }
                if (flag) {
                    loadpass();
                }
            }
        }

        function childalert(IsBritrail) {
            if (IsBritrail)
                alert('Child passes can only be purchased with Adult or Senior passes with same travel validity and class.');
            else
                alert("Child passes can only be purchased with Adult or Adult Saver passes with same travel validity and class.");
        }

        function bindLstDeleteItem(obj) {
            var result = confirm("Are you sour you want to delete this item?");
            if (result) {
                var id = $(obj).attr('id');
                $("#" + id).prop('selectedIndex', 0);
                $("#" + id).trigger('change');
            }
        }

        function ChangeLinkPosition(obj) {
            $("#hdnlinkbtn").val($(obj).attr('id'));
            $(obj).parent('li').addClass("starail-Tabs-tab--active");
            $(obj).parents().siblings().removeClass('starail-Tabs-tab--active');
            var item = $(obj).attr('inumber');
            $("#GV" + item).css("display", "block");
            $("#GV" + item).siblings('.country-block-inner').css("display", "none");
        }
        function collChangeLinkPosition() {
            $("#" + $("#hdnlinkbtn").val()).trigger('click');
        }
        function init() {
            var accoridonHeaders = $('.js-accordonHeader');
            var accoridonButtons = $('.js-accordonButton');
            accoridonHeaders.on('click', function (e) {
                e.preventDefault();
                //Keep actions within each accordion
                var parentAccordion = $(this).parents('.js-accordon');
                var parentAccordionItem = $(this).parent();

                if (parentAccordionItem.hasClass('is-open')) {
                    parentAccordionItem.removeClass('is-open');
                    return false;
                }
                parentAccordion.find('.js-accordonItem').removeClass('is-open');
                parentAccordionItem.addClass('is-open');
                $.event.trigger({
                    type: 'accordionOpened'
                });
            });
        } 
    </script>
    <asp:HiddenField runat="server" ID="hdnlinkbtn" ClientIDMode="Static" Value="MainContent_rptTraveller_lnkTraveller_0" />
</asp:Content>
