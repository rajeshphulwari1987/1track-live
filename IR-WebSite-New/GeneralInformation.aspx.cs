﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business;
using System.Web.UI;

public partial class GeneralInformation : System.Web.UI.Page
{
    public string adminSiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string siteURL;
    private Guid siteId;
    public string script;
    private readonly db_1TrackEntities db = new db_1TrackEntities();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(siteId);
            BindGeneralInformation();
        }
    }

    public void BindGeneralInformation()
    {
        var result = db.tblGeneralInformation_.Where(x => x.SiteId == siteId).ToList();
        if (result != null && result.Count > 0)
        {
            ContentHead.InnerHtml = result.FirstOrDefault().Keywords;
            ContentText.InnerHtml = Server.HtmlDecode(result.FirstOrDefault().Description);
        }
    }

}