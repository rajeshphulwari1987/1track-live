﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Business;
using OneHubServiceRef;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Globalization;
using System.Threading;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Xml;
using Newtonsoft.Json;

[System.Web.Script.Services.ScriptService]
public partial class BookingCart : Page
{
    #region Global Variables
    public string AdminSiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    readonly Masters _masterPage = new Masters();
    private Common _Common = new Common();
    readonly ManageAdminFee ManageAdmfee = new ManageAdminFee();
    private db_1TrackEntities _db = new db_1TrackEntities();
    readonly ManageUser _ManageUser = new ManageUser();
    private ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    ManageTrainDetails _master = new ManageTrainDetails();
    ManageBooking _masterBooking = new ManageBooking();
    public static string currency = "$";
    public static Guid currencyID = new Guid();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
    Guid pageID, siteId;
    public string script = "<script></script>";
    public string siteURL;
    Boolean isShippingAllow = false;
    public long OrderID = 0;
    public string ChkOutActionsTags = string.Empty;
    public string ChkOutSetActionsTags = string.Empty;
    public string RemoveProductTags = string.Empty;
    public bool IsAgent = false;
    public bool IsStaSite = false;
    public string MandatoryTextFirst = "", MandatoryTextFirstSecond = "";
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            siteId = Guid.Parse(Session["siteId"].ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        GetCurrencyCode();
        if (Session["OrderID"] != null)
            OrderID = Convert.ToInt64(Session["OrderID"]);
        if (!IsPostBack)
        {
            if (Session["RemoveProductTags"] != null)
                RemoveProductTags = Session["RemoveProductTags"].ToString();
            Session.Remove("RemoveProductTags");
            BindPageMandatoryFields();
            PageLoadEvent();
            QubitOperationLoad();
            GetDetailsAnalyticTags();
            GetAdminFeeData();
            FillAgentAddress(OrderID);
            if (AgentuserInfo.UserID != Guid.Empty)
            {
                var Agentlist = _ManageUser.AgentDetailsById(AgentuserInfo.UserID);
                var AgentNameAndEmail = _ManageUser.AgentNameEmailById(AgentuserInfo.UserID);
                if (Agentlist != null && AgentNameAndEmail != null)
                {
                    ddlMr.SelectedValue = Convert.ToInt32(AgentNameAndEmail.Salutation).ToString();
                    txtFirst.Text = AgentNameAndEmail.Forename;
                    txtLast.Text = AgentNameAndEmail.Surname;
                    txtEmail.Text = AgentNameAndEmail.EmailAddress;
                    ddlCountry.SelectedValue = Convert.ToString(Agentlist.Country);
                    txtCity.Text = Agentlist.Town;
                    txtAdd.Text = Agentlist.Address1;
                    txtAdd2.Text = Agentlist.Address2;
                    txtZip.Text = Agentlist.Postcode;
                    txtBillPhone.Text = Agentlist.Telephone;
                }
            }
            else if (USERuserInfo.ID != Guid.Empty)
            {
                var UserData = _db.tblUserLogins.FirstOrDefault(x => x.ID == USERuserInfo.ID);
                if (UserData != null)
                {
                    txtFirst.Text = UserData.FirstName;
                    txtLast.Text = UserData.LastName;
                    txtEmail.Text = UserData.Email;
                    txtEmail.Attributes.Add("disabled", "disabled");
                    txtEmail.Attributes.Add("style", "text-transform: lowercase;background: #ccc;");
                    ddlCountry.SelectedValue = UserData.Country.ToString();
                    txtCity.Text = UserData.City;
                    txtState.Text = UserData.State;
                    txtAdd.Text = UserData.Address;
                    txtAdd2.Text = UserData.Address2;
                    txtZip.Text = UserData.PostCode;
                    txtBillPhone.Text = UserData.Phone;
                }
            }
            ShowHideTermsMandatoryText(siteId);
        }
        FillShippingData();
    }

    public void GetAdminFeeData()
    {
        try
        {
            hdnAdminFee.Value = ManageAdmfee.GetAdminFeeBySiteID(siteId);
            if (string.IsNullOrEmpty(hdnAdminFee.Value))
                pnladminfee.Attributes.Add("style", "display:none");
            else
                pnladminfee.Attributes.Add("style", "display:block");
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void GetDetailsAnalyticTags()
    {
        ManageAnalyticTag _masterAnalyticTag = new ManageAnalyticTag();
        var data = _masterAnalyticTag.GetTagBySiteId(siteId);
        if (data != null)
        {
            if (Session["GaTaggingProductlist"] != null)
            {
                var list = (Session["GaTaggingProductlist"] as List<ProductPass>);
                string obj = "";
                #region /*ChkOutActions*/
                string[] SplitData = data.ChkOutActions.Split(';');
                string ChkOut = "";
                foreach (var item in list)
                {
                    ChkOut = SplitData[0].Replace("<Product Code>", "" + item.ProductCode + "");
                    ChkOut = ChkOut.Replace("<Product Name>", "" + item.Name.Replace("'", "’") + "");
                    ChkOut = ChkOut.Replace("<Product Category>", "" + item.CategoryName.Replace("'", "’") + "");
                    ChkOut = ChkOut.Replace("<Price>", "" + item.Price + "");
                    ChkOut = ChkOut.Replace("<Quantity>", "" + item.Count + "");
                    obj += ChkOut + ";\n";
                    ChkOut = "";
                }
                if (SplitData.Count() > 1)
                    foreach (var item in list)
                    {
                        ChkOut = SplitData[1].Replace("<Product Code>", "" + item.ProductCode + "");
                        ChkOut = ChkOut.Replace("<Product Name>", "" + item.Name.Replace("'", "’") + "");
                        ChkOut = ChkOut.Replace("<Product Category>", "" + item.CategoryName.Replace("'", "’") + "");
                        ChkOut = ChkOut.Replace("<Price>", "" + item.Price + "");
                        ChkOut = ChkOut.Replace("<Quantity>", "" + item.Count + "");
                        obj += ChkOut + ";\n";
                        ChkOut = "";
                    }
                ChkOutActionsTags = "\n<script type='text/javascript'>\n/*ChkOutActions*/\n" + obj + "\n</script>\n";
                ChkOutSetActionsTags = "\n<script type='text/javascript'>\n/*ChkOutSetActions*/\n" + data.ChkOutSetActions.Replace("<Step Number>", "'1'") + "\n</script>\n";
                #endregion
            }
        }
    }

    public void BindPageMandatoryFields()
    {
        try
        {
            if (Page.RouteData.Values["PageId"] == null)
                return;

            pageID = (Guid)Page.RouteData.Values["PageId"];
            var list = _masterPage.GetMandatoryVal(siteId, pageID);

            foreach (var item in list)
            {
                if (item.ControlField == "rfFirst")
                    reqvalerrorBA0.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfLast")
                    reqvalerrorBA1.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfEmail")
                    reqvalerrorBA2.ValidationGroup = item.ValGrp;


                if (item.ControlField == "rfPhone")
                    reqvalerrorBA7.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfAdd")
                    reqvalerrorBA8.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfZip")
                    reqvalerrorBA9.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfCountry")
                    reqvalerrorBA10.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfDateOfDepature")
                    reqvalerrorDPdATE.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfshipFirstName")
                    reqvalerrorSA0.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfshipLastName")
                    reqvalerrorSA1.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfshipEmail")
                    reqvalerrorSA2.ValidationGroup = item.ValGrp;


                if (item.ControlField == "rfshpPhone")
                    reqvalerrorSA7.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfshpAdd")
                    reqvalerrorSA8.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfshpZip")
                    reqvalerrorSA9.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfshpCountry")
                    reqvalerrorSA10.ValidationGroup = item.ValGrp;


                reqvalerrorBA8.ValidationGroup = item.ValGrp;   //Billing address1
                reqvalerrorBA11.ValidationGroup = item.ValGrp;  //Billing city

            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void QubitOperationLoad()
    {
        if (Session["siteId"] != null)
        {
            var lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
            if (lstQbit != null && lstQbit.Count() > 0)
            {
                var res = lstQbit.FirstOrDefault();
                if (res != null)
                    script = res.Script;
            }
        }
    }

    void PageLoadEvent()
    {
        ddlCountry.DataSource = _master.GetCountryDetail();
        ddlCountry.DataValueField = "CountryID";
        ddlCountry.DataTextField = "CountryName";
        ddlCountry.DataBind();
        //ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

        if (_masterPage.GetPassShippingAllCountryList(siteId).ToList().Count > 0)
            ddlshpCountry.DataSource = _masterPage.GetPassShippingAllCountryList(siteId).ToList();
        else
            ddlshpCountry.DataSource = _master.GetCountryDetail();

        ddlshpCountry.DataValueField = "CountryID";
        ddlshpCountry.DataTextField = "CountryName";
        ddlshpCountry.DataBind();
        ddlshpCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

        if (Session["siteId"] != null && OrderID != 0)
            _masterBooking.UpdateSiteToOrder(OrderID, siteId);

        if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
        {
            var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
            string countryID = cookie.Values["_cuntryId"];
            ddlCountry.SelectedValue = countryID;
            ddlshpCountry.SelectedValue = countryID;
        }
        PrepouplateSeniorAndAdult();
    }

    void PrepouplateSeniorAndAdult()
    {
        if (Session["RailPassData"] != null)
        {
            var list = Session["RailPassData"] as List<getRailPassData>;
            decimal TotalSalePrice = list.Sum(t => Convert.ToDecimal(t.Price));
            decimal CPBookingFee = list.Sum(t => t.BookingFee);
            decimal SBookingFee = 0;
            hdntotalSaleamount.Value = TotalSalePrice.ToString("F2");
            var lst = _masterBooking.GetBooingFees(TotalSalePrice, Guid.Parse(Session["siteId"].ToString()));
            if (lst.Any())
                SBookingFee = lst.FirstOrDefault().BookingFee;

            if ((SBookingFee + CPBookingFee) > 0)
                hdnBookingFee.Value = lblBookingFee.Text = (SBookingFee + CPBookingFee).ToString("F2");
            else
            {
                lblBookingFee.Text = hdnBookingFee.Value = "0.00";
                pnlbooking.Attributes.Add("style", "display:none");
            }
            decimal totalAmount = TotalSalePrice + Convert.ToDecimal(lblBookingFee.Text);
            lblTotalCost.Text = totalAmount.ToString("F2");
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    public decimal getsitetickprotection()
    {
        try
        {
            var list = FrontEndManagePass.GetTicketProtectionPrice(siteId);
            if (list != null)
            {
                return Convert.ToDecimal(FrontEndManagePass.GetPriceAfterConversion(list.Amount, siteId, list.CurrencyID, currencyID).ToString("F"));
            }
            else
                return 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
            currency = oManageClass.GetCurrency(currencyID);
        }
    }

    protected void btnCheckout_Click(object sender, EventArgs e)
    {
        try
        {
            if (!chkMandatory.Checked)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "checkMandatory", "shipingchk();callvalerror();Focusonerrorcontrol();checkMandatory();showhideterms();", true);
                return;
            }

            UpdateAgentAddress(OrderID);
            bool dchk = UcPassDetail.btnChkOut_Click();
            if (!dchk)
            {
                string addpos = string.Empty;
                if (Session["addpos"] != null)
                {
                    addpos = Session["addpos"].ToString();
                    Session.Remove("addpos");
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "ckval_11", "shipingchk();callvalerror();Focusonerrorcontrol();showhideterms();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "validDOBpos", "validDOBpos('" + addpos + "');", true);
                return;
            }

            if (Session["OrderID"] != null)
                OrderID = Convert.ToInt64(Session["OrderID"]);

            #region user login for public site
            if (AgentuserInfo.UserID == Guid.Empty && USERuserInfo.ID == Guid.Empty)
            {
                bool isRegistered = false;
                tblUserLogin oreg = new tblUserLogin();
                var usesdetail = _ManageUser.GetUserByEmailAndSiteID(txtEmail.Text, siteId);
                if (usesdetail != null)
                {
                    oreg.ID = usesdetail.ID;
                    oreg.FirstName = usesdetail.FirstName;
                    oreg.LastName = usesdetail.LastName;
                    oreg.Email = usesdetail.Email;
                    oreg.SiteId = usesdetail.SiteId;
                    oreg.Password = usesdetail.Password;
                    isRegistered = true;
                }
                else
                {
                    oreg.ID = Guid.NewGuid();
                    oreg.FirstName = txtFirst.Text;
                    oreg.LastName = txtLast.Text;
                    oreg.Email = txtEmail.Text;
                    oreg.Password = _Common.CreateRandomPassword(10);
                    oreg.Address = txtAdd.Text;
                    oreg.Phone = txtBillPhone.Text;
                    oreg.City = txtCity.Text;
                    oreg.PostCode = txtZip.Text;
                    oreg.Country = Guid.Parse(ddlCountry.SelectedValue);
                    oreg.SiteId = siteId;
                    oreg.IsActive = true;
                    _ManageUser.SaveUser(oreg);
                }
                USERuserInfo.UserEmail = oreg.Email;
                USERuserInfo.ID = oreg.ID;
                USERuserInfo.Username = oreg.FirstName + (string.IsNullOrEmpty(oreg.LastName) ? "" : " " + oreg.LastName);
                USERuserInfo.SiteID = oreg.SiteId;
                if (OrderID != 0)
                    _masterBooking.SetorderUserid(OrderID, USERuserInfo.ID);

                #region email sent user
                //var siteName = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
                //string htmfile = Server.MapPath("~/MailTemplate/CRMEmailTemplate.htm");
                //var xmlDoc = new XmlDocument();
                //xmlDoc.Load(htmfile);
                //var list = xmlDoc.SelectNodes("html");
                //string body = list[0].InnerXml.ToString();
                //if (siteName.IsSTA.HasValue ? siteName.IsSTA.Value : false)
                //    body = body.Replace("##headercolor##", "#0c6ab8");
                //else
                //    body = body.Replace("##headercolor##", "#941e34");
                //body = body.Replace("##siteimagepath##", LoadSiteLogo());
                //body = body.Replace("##Heading##", (isRegistered ? "You have already registered at " : "User account has been successfully registered at ") + (siteName != null ? siteName.DisplayName : "Internationalrail"));
                //body = body.Replace("##UserName##", oreg.Email);
                //body = body.Replace("##Password##", oreg.Password);
                //body = body.Replace("##Description##", "Please login your account by clicking this link :- <a href='" + siteURL + "User/Login'>Click here</a>");
                //body = body.Replace("#Blank#", "&nbsp;");

                //string Subject = "User Registration";
                //var emailSetting = _masterPage.GetEmailSettingDetail(siteId);
                //if (emailSetting != null)
                //    _masterPage.SendContactUsMail(siteId, Subject, body, emailSetting.Email, txtEmail.Text);
                #endregion
            }
            #endregion

            Session["ShipMethod"] = hdnShipMethod.Value;
            Session["ShipDesc"] = hdnShipDesc.Value;

            int postcodelen = txtZip.Text.Replace(" ", "").Length;
            lblpmsg.Visible = postcodelen > 7;
            if (postcodelen > 7)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "ckval_11", "shipingchk();callvalerror();showhideterms();", true);
                return;
            }
            AddPassBookingInLocalDB();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public string LoadSiteLogo()
    {
        var data = _db.tblSites.FirstOrDefault(x => x.ID == siteId && x.IsActive == true);
        if (data != null)
        {
            if (!string.IsNullOrEmpty(data.LogoPath))
                return AdminSiteUrl + data.LogoPath;
            else
                return siteURL + "assets/img/branding/logo.png";
        }
        return "https://www.internationalrail.com/images/logo.png";
    }

    void AddPassBookingInLocalDB()
    {
        ManageAdmfee.UpdateOrderAdminFeeByOrderID(OrderID, Convert.ToDecimal(String.IsNullOrEmpty(hdnAdminFeeAmount.Value) ? "0" : hdnAdminFeeAmount.Value));
        var objBillingAddress = new tblOrderBillingAddress();
        objBillingAddress.ID = Guid.NewGuid();
        objBillingAddress.OrderID = OrderID;
        objBillingAddress.Title = ddlMr.SelectedItem.Text;
        objBillingAddress.FirstName = txtFirst.Text;
        objBillingAddress.LastName = txtLast.Text;
        objBillingAddress.Phone = txtBillPhone.Text;
        objBillingAddress.Address1 = txtAdd.Text;
        objBillingAddress.Address2 = txtAdd2.Text;
        objBillingAddress.EmailAddress = txtEmail.Text;
        objBillingAddress.City = txtCity.Text;
        objBillingAddress.State = txtState.Text;
        objBillingAddress.Country = ddlCountry.SelectedItem.Text;
        objBillingAddress.Postcode = txtZip.Text;

        if (!chkShippingfill.Checked)
        {
            objBillingAddress.TitleShpg = ddlMr.SelectedItem.Text;
            objBillingAddress.FirstNameShpg = txtFirst.Text;
            objBillingAddress.LastNameShpg = txtLast.Text;
            objBillingAddress.EmailAddressShpg = txtEmail.Text;
            objBillingAddress.PhoneShpg = txtBillPhone.Text;
            objBillingAddress.Address1Shpg = txtAdd.Text;
            objBillingAddress.Address2Shpg = txtAdd2.Text;
            objBillingAddress.CityShpg = txtCity.Text;
            objBillingAddress.StateShpg = txtState.Text;
            objBillingAddress.CountryShpg = ddlCountry.SelectedItem.Text;
            objBillingAddress.PostcodeShpg = txtZip.Text;
        }
        else
        {
            objBillingAddress.TitleShpg = ddlshpMr.SelectedItem.Text;
            objBillingAddress.FirstNameShpg = txtshpfname.Text;
            objBillingAddress.LastNameShpg = txtshpLast.Text;
            objBillingAddress.EmailAddressShpg = txtshpEmail.Text;
            objBillingAddress.PhoneShpg = txtShpPhone.Text;
            objBillingAddress.Address1Shpg = txtshpAdd.Text;
            objBillingAddress.Address2Shpg = txtshpAdd2.Text;
            objBillingAddress.CityShpg = txtshpCity.Text;
            objBillingAddress.StateShpg = txtshpState.Text;
            objBillingAddress.CountryShpg = ddlshpCountry.SelectedItem.Text;
            objBillingAddress.PostcodeShpg = txtshpZip.Text;
        }
        if (txtDateOfDepature.Text != "DD/MM/YYYY")
            _masterBooking.UpdateDepatureDate(OrderID, Convert.ToDateTime(txtDateOfDepature.Text));
        _masterBooking.AddOrderBillingAddress(objBillingAddress);
        if (!string.IsNullOrEmpty(hdnShipMethod.Value))
            _masterBooking.FillShippingDefaultData(OrderID, hdnShipMethod.Value);
        _masterBooking.UpdateOrderBookingFee(Convert.ToDecimal(lblBookingFee.Text), OrderID);
        Response.Redirect("~/PaymentProcess", false);
    }

    public void UpdateAgentAddress(long OrderID)
    {
        try
        {
            Session["ManageAgentData"] = null;
            if (Session["AgentUserID"] != null)
            {
                ManageAgentData _ManageAgentData = new ManageAgentData();
                _ManageAgentData.OrderId = OrderID;
                List<AgentAddress> _AgentAddress = new List<AgentAddress>();

                _AgentAddress.Add(new AgentAddress
                {
                    Title = ddlMr.SelectedValue,
                    FName = txtFirst.Text,
                    LName = txtLast.Text,
                    EmailAddress = txtEmail.Text,
                    Phone = txtBillPhone.Text,
                    Address1 = txtAdd.Text,
                    Address2 = txtAdd2.Text,
                    City = txtCity.Text,
                    State = txtState.Text,
                    ZipCode = txtZip.Text,
                    Country = ddlCountry.SelectedValue
                });

                if (chkShippingfill.Checked)
                {
                    _AgentAddress.Add(new AgentAddress
                    {
                        Title = ddlshpMr.SelectedValue,
                        FName = txtshpfname.Text,
                        LName = txtshpLast.Text,
                        EmailAddress = txtshpEmail.Text,
                        Phone = txtShpPhone.Text,
                        Address1 = txtshpAdd.Text,
                        Address2 = txtshpAdd2.Text,
                        City = txtshpCity.Text,
                        State = txtshpState.Text,
                        ZipCode = txtshpZip.Text,
                        Country = ddlshpCountry.SelectedValue
                    });
                }
                _ManageAgentData.AgentAddress = _AgentAddress;
                Session["ManageAgentData"] = _ManageAgentData;
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void FillAgentAddress(long OrderID)
    {
        try
        {
            if (Session["ManageAgentData"] != null && Session["AgentUserID"] != null)
            {
                ManageAgentData ManageAgentDataList = Session["ManageAgentData"] as ManageAgentData;
                var _billingAddress = ManageAgentDataList.AgentAddress.FirstOrDefault();
                ddlMr.SelectedValue = _billingAddress.Title;
                txtFirst.Text = _billingAddress.FName;
                txtLast.Text = _billingAddress.LName;
                txtEmail.Text = _billingAddress.EmailAddress;
                txtBillPhone.Text = _billingAddress.Phone;
                txtAdd.Text = _billingAddress.Address1;
                txtAdd2.Text = _billingAddress.Address2;
                txtCity.Text = _billingAddress.City;
                txtState.Text = _billingAddress.State;
                txtZip.Text = _billingAddress.ZipCode;
                ddlCountry.SelectedValue = _billingAddress.Country;
                if (ManageAgentDataList.AgentAddress.Count == 2)
                {
                    var _shippingAddress = ManageAgentDataList.AgentAddress.Skip(1).FirstOrDefault();
                    ddlshpMr.SelectedValue = _shippingAddress.Title;
                    txtshpfname.Text = _shippingAddress.FName;
                    txtshpLast.Text = _shippingAddress.LName;
                    txtshpEmail.Text = _shippingAddress.EmailAddress;
                    txtShpPhone.Text = _shippingAddress.Phone;
                    txtshpAdd.Text = _shippingAddress.Address1;
                    txtshpAdd2.Text = _shippingAddress.Address2;
                    txtshpCity.Text = _shippingAddress.City;
                    txtshpState.Text = _shippingAddress.State;
                    txtshpZip.Text = _shippingAddress.ZipCode;
                    ddlshpCountry.SelectedValue = _shippingAddress.Country;
                    chkShippingfill.Checked = true;
                }
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "FillAgentAddress", "fillagentdata();showhideterms();", true);
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    [WebMethod(EnableSession = true)]
    public static string AjaxFillShippingData(bool HaveShipping, string ShipCountry, string Country)
    {
        try
        {
            Guid SiteId = Guid.Empty;
            if (HttpContext.Current.Session["siteId"] != null)
                SiteId = Guid.Parse(HttpContext.Current.Session["siteId"].ToString());

            bool isShippingAllow = false;
            var list = new List<Shippingjson>();
            var _masterBooking = new ManageBooking();
            var db = new db_1TrackEntities();

            if (HttpContext.Current.Session["RailPassData"] != null)
            {
                List<getRailPassData> listPass = HttpContext.Current.Session["RailPassData"] as List<getRailPassData>;
                foreach (var item in listPass)
                {
                    Guid id = Guid.Parse(item.PrdtId);
                    isShippingAllow = db.tblProducts.Any(x => x.IsShippingAplicable && x.ID == id);
                    if (isShippingAllow)
                        break;
                }
            }
            if (isShippingAllow)
            {
                var countryid = HaveShipping ? Guid.Parse(ShipCountry) : Guid.Parse(Country);
                var lstShip = _masterBooking.getAllPassShippingDetail(SiteId, countryid);
                if (lstShip.Any())
                    list = lstShip.Select(x => new Shippingjson { Price = x.Price, ShippingName = x.ShippingName, Description = x.Description }).ToList();
                else
                {
                    var lstDefaultShip = _masterBooking.getDefaultShippingDetail(SiteId);
                    if (lstDefaultShip.Any())
                        list = lstDefaultShip.Select(x => new Shippingjson { Price = x.Price, ShippingName = x.ShippingName, Description = x.Description }).ToList();
                    else
                    {
                        list = null;
                    }
                }
            }
            return JsonConvert.SerializeObject(list);
        }
        catch (Exception ex)
        {
            return "no";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string AjaxFillBookingFeeOnDepartureDate(string DeptDate, string Price)
    {
        try
        {
            if (HttpContext.Current.Session["siteId"] != null && DeptDate != "DD/MM/YYYY")
            {
                Guid SiteId = Guid.Parse(HttpContext.Current.Session["siteId"].ToString());
                var deptDate = Convert.ToDateTime(DeptDate);
                var currentDate = DateTime.Now;
                deptDate.AddHours(currentDate.Hour);
                deptDate.AddMinutes(currentDate.Minute);
                double day = (deptDate - currentDate).TotalDays;
                var db = new db_1TrackEntities();
                var Data = db.tblBookingFeeRules.FirstOrDefault(x => x.HasAmount == false && x.tblBookingFeeSiteLookUps.FirstOrDefault().SiteID == SiteId);
                if (Data != null)
                {
                    if (day > Data.StartDate && day < Data.EndDate)
                        return Price;
                    else
                        return Data.BookingFee.ToString();
                }
            }
            return Price;
        }
        catch (Exception ex)
        {
            return "no";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string AjaxCheckCountry(Guid productid, Guid countryid)
    {
        UserControls_passdetalis obj = new UserControls_passdetalis();
        bool result = obj.getvalidcountryid(productid, countryid);
        if (!result)
        {
            return "Country not valid for this pass.";
        }
        else
            return "yes";
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            FillShippingData();
            ScriptManager.RegisterStartupScript(this, GetType(), "shipingchk", "shipingchk();callvalerror();getdata();lastshipping('first');showhideterms();", true);
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void ddlshpCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            FillShippingData();
            ScriptManager.RegisterStartupScript(this, GetType(), "datashipingchk", "shipingchk();callvalerror();getdata();lastshipping('first');showhideterms();", true);
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void chkShippingfill_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            FillShippingData();
            ScriptManager.RegisterStartupScript(this, GetType(), "lastshipping", "shipingchk();callvalerror();getdata();lastshipping('first');showhideterms();", true);
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void FillShippingData()
    {
        if (Session["RailPassData"] != null)
        {
            List<getRailPassData> listPass = Session["RailPassData"] as List<getRailPassData>;
            foreach (var item in listPass)
            {
                Guid id = Guid.Parse(item.PrdtId);
                isShippingAllow = _db.tblProducts.Any(x => x.IsShippingAplicable && x.ID == id);
                if (isShippingAllow)
                    break;
            }
        }
        pnlShipping.Attributes.Add("style", isShippingAllow ? "display:block" : "display:none");
        if (isShippingAllow)
        {
            Guid countryid = Guid.Empty;
            if (ddlshpCountry.SelectedValue != "0" && chkShippingfill.Checked)
                countryid = Guid.Parse(ddlshpCountry.SelectedValue);
            else if (ddlCountry.SelectedValue != "0")
                countryid = Guid.Parse(ddlCountry.SelectedValue);
            var lstShip = _masterBooking.getAllPassShippingDetail(siteId, countryid);
            if (lstShip.Any())
            {
                rptShippings.DataSource = lstShip;
            }
            else
            {
                var lstDefaultShip = _masterBooking.getDefaultShippingDetail(siteId);
                if (lstDefaultShip.Any())
                {
                    rptShippings.DataSource = lstDefaultShip;
                }
                else
                {
                    hdnShippingCost.Value = "0.00";
                    rptShippings.DataSource = null;
                    pnlShipping.Attributes.Add("style", "display:none");
                }
            }
            rptShippings.DataBind();
        }
        else
        {
            hdnShippingCost.Value = "0.00";
            rptShippings.DataSource = null;
            rptShippings.DataBind();
        }
    }

    public void ShowHideTermsMandatoryText(Guid SiteId)
    {
        try
        {
            var data = _oWebsitePage.GetSiteDatabySiteId(SiteId);
            if (data != null)
            {
                IsStaSite = data.IsSTA.HasValue ? (data.IsSTA.Value == true ? true : false) : false;
                div_Mandatory.Visible = data.IsMandatoryTerm;
                div_NotMandatory.Visible = data.IsMandatoryTermSecond;
                if (SiteId == Guid.Parse("107F17AC-580C-4D94-A7F7-E858ADADAB4F") || SiteId == Guid.Parse("79139CF1-8243-453E-849B-C3F7EB6004D6"))  //for STAUK Agent and Public
                    p_MandatoryText.InnerHtml = "This booking via STA Travel will be fulfilled by our partners International Rail. Any personal information will only be used to administer your account/order for the products and services you are purchasing.<br/><br/>For purchases from 1st July 2018 onwards, I have read and accept the information given on my rights under the \"Package Travel and Linked Travel Arrangements Regulations 2018\".<br/>If you book another product (accommodation, transport or other tourist service) with us this will not be a package, as defined by the Package Travel Directive <a target='_blank' style='text-decoration: underline;' href='http://www.statravel.co.uk/booking-terms-and-conditions-post-july-1-2018-non-package.htm'>found here</a>.<br/><br/>By ticking this box you are agreeing to all the above points relating to your rights.";
                else
                    p_MandatoryText.InnerHtml = MandatoryTextFirst = !string.IsNullOrEmpty(data.MandatoryTermText) ? data.MandatoryTermText : "";
                p_NotMandatoryText.InnerHtml = MandatoryTextFirstSecond = !string.IsNullOrEmpty(data.MandatoryTermTextSecond) ? data.MandatoryTermTextSecond : "";
            }
        }
        catch (Exception ex) { }
    }
}

public class Shippingjson
{
    public decimal Price { get; set; }
    public string ShippingName { get; set; }
    public string Description { get; set; }
}