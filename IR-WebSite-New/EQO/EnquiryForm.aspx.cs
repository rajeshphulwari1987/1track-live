﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Text;
using System.Xml;
using System.Globalization;
using System.Web.Services;
using Newtonsoft.Json;
using System.Net.Mail;
using System.IO;
using System.Data;
using System.Configuration;

public partial class EQO_EnquiryForm : System.Web.UI.Page
{

    private Masters _objmaster = new Masters();
    private ManageEQO _ManageEnquiry = new ManageEQO();
    private ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string AdminSiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    private string htmfile = string.Empty;
    public static string unavailableDates1 = "";
    private Guid _siteId;
    public string siteURL;
    public static string routeInfo = HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (txtEmail.Text != string.Empty)
            {
                hdnEmail.Value = txtEmail.Text;
            }

            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "LoadCal1", "LoadCal1();", true);

            var data = (dynamic)null;
            if (HttpContext.Current.Session["holdCustInfo"] == null)
            {
                EQO_EnquiryForm frm1 = new EQO_EnquiryForm();
                data = frm1._ManageEnquiry.GetUserEmail();
            }

            if (!Page.IsPostBack)
            {
                for (int j = 10; j >= 0; j--)
                {
                    ddlAdult.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                    ddlChild.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                    ddlYouth.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                    ddlSenior.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                    ddlAdult.SelectedValue = "1";
                }

                var siteDDates = new ManageHolidays().GetAllHolydaysBySite(_siteId);
                unavailableDates1 = "[";
                if (siteDDates.Any())
                {
                    foreach (var it in siteDDates)
                    {
                        unavailableDates1 += "\"" + Convert.ToDateTime(it.DateofHoliday).ToString("dd-MM-yyyy") + "\"" + ",";
                    }
                    unavailableDates1 = unavailableDates1.Substring(0, unavailableDates1.Length - 1);
                }
                unavailableDates1 += "]";
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            bool isnewcust = false;
            var data = _ManageEnquiry.GetUserEmailbyEmailid(txtEmail.Text);
            if (data == null)
                isnewcust = true;
            var list = _ManageEnquiry.Getsitebyid(_siteId);

            tblUserLogin oreg = new tblUserLogin()
            {
                ID = Guid.NewGuid(),
                FirstName = txtFirstName.Text,
                LastName = txtLastName.Text,
                Email = txtEmail.Text,
                Password = "123456",
                Phone = txtPhone.Text,
                Country = Guid.Parse(Convert.ToString(list.DefaultCountryID)),
                SiteId = _siteId,
                IsActive = true,
                IsEQOUser = true,
                IsContactUsUser = false,
                CreatedOn = DateTime.Now,
            };
            var user = _ManageEnquiry.SaveEQOcontactusUser(oreg);

            if (isnewcust)
            {
                //insert into activity log
                InsertActivityLog(routeInfo, HttpContext.Current.Request.UrlReferrer.ToString(), null, oreg.FirstName + " (Customer) has been created", "Customer");
                SendContactEmail(_siteId, user, oreg);
            }

            Guid lookupid = SaveEQOEnquiryLookup(oreg.ID);

            DateTime? nullvalue = null;

            DateTime? DepartDate = (txtDepartureDate.Text != string.Empty && txtDepartureDate.Text != "DD/MM/YYYY") ? Convert.ToDateTime(txtDepartureDate.Text) : nullvalue;
            string DepartTime = (txtDepartureDate.Text != string.Empty && txtDepartureDate.Text != "DD/MM/YYYY") ? ddldepTime.SelectedValue : null;
            DateTime? ReturnDate = (txtReturnDate.Text != string.Empty && txtReturnDate.Text != "DD/MM/YYYY") ? Convert.ToDateTime(txtReturnDate.Text) : nullvalue;
            string ReturnTime = (txtReturnDate.Text != string.Empty && txtReturnDate.Text != "DD/MM/YYYY") ? ddlReturnTime.SelectedValue : null;

            Guid p2penquiryid = new Guid();

            if (lookupid != null && lookupid != new Guid() && rdP2P.Checked)
                p2penquiryid = SaveEQOP2PEnquiry(lookupid, txtFrom.Text, txtTo.Text, DepartDate, DepartTime);

            if (rdBookingType.SelectedValue == "1")
                p2penquiryid = SaveEQOP2PEnquiry(lookupid, txtTo.Text, txtFrom.Text, ReturnDate, ReturnTime, true);

            if (p2penquiryid != new Guid() || lookupid != new Guid())
                SendMailForEnquiry(user, _siteId, lookupid);

            ClearData();
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public Guid SaveEQOEnquiryLookup(Guid id)
    {
        Guid? catid = Guid.Empty;

        tblEQOEnquiry look = new tblEQOEnquiry()
        {
            Id = Guid.NewGuid(),
            UserId = id,
            QueryMessage = txtdesc.Text,
            StatusId = 1,
            IsActive = true,
            CreatedBy = null,
            CreatedOn = DateTime.Now,
            IsDeleted = false,
            SiteID = _siteId,
            IsP2P = rdP2P.Checked ? true : false,
            IsFrontRequest = true,
            IsNew = true
        };
        string enq = _ManageEnquiry.SaveeqoEnquiryLookUp(look);

        string[] enq1 = enq.Split('_');
        InsertActivityLog(routeInfo, HttpContext.Current.Request.UrlReferrer.ToString(), null, enq1[1] + " (Enquiry) has been created", "Enquiry");
        return Guid.Parse(enq1[0]);
    }

    public Guid SaveEQOP2PEnquiry(Guid lookupid, string from, string to, DateTime? departdate, string departtime, bool IsReturn = false)
    {
        DateTime? nullvalue = null;
        tblEQOP2PEnquiry oreg1 = new tblEQOP2PEnquiry()
        {
            Id = Guid.NewGuid(),
            EQOLookupId = lookupid,
            From = from,
            To = to,
            DepartDate = Convert.ToString(departdate) != string.Empty ? Convert.ToDateTime(departdate) : nullvalue,
            DepartTime = Convert.ToString(departtime) != "00:00" ? departtime : null,
            IsFlexible = chkIsflexible.Checked,
            Adult = Convert.ToInt32(ddlAdult.SelectedValue),
            Children = Convert.ToInt32(ddlChild.SelectedValue),
            Youth = Convert.ToInt32(ddlYouth.SelectedValue),
            Senior = Convert.ToInt32(ddlSenior.SelectedValue),
            IsReturn = IsReturn
        };
        var enq = _ManageEnquiry.SaveEQOP2PEnquiry(oreg1);
        return enq;
    }

    public void SendContactEmail(Guid siteid, Guid user, tblUserLogin oreg)
    {
        try
        {
            if (user != new Guid())
            {
                var sitedata = _ManageEnquiry.Getsitebyid(_siteId);
                htmfile = Server.MapPath("~/MailTemplate/CRMEmailTemplate.htm");
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(htmfile);
                var list = xmlDoc.SelectNodes("html");
                string body = list[0].InnerXml.ToString();
                if (sitedata.IsSTA.HasValue ? sitedata.IsSTA.Value : false)
                    body = body.Replace("##headercolor##", "#0c6ab8");
                else
                    body = body.Replace("##headercolor##", "#941e34");
                body = body.Replace("##siteimagepath##", LoadSiteLogo());
                body = body.Replace("##Heading##", "User account has been successfully registered at " + (sitedata != null ? sitedata.DisplayName : "Internationalrail"));
                body = body.Replace("##UserName##", oreg.Email);
                body = body.Replace("##Password##", oreg.Password);
                body = body.Replace("##Description##", "Please login your account by clicking this link :- <a href='" + siteURL + "User/Login'>Click here</a>");
                body = body.Replace("#Blank#", "&nbsp;");

                string Subject = "User Registration";
                var emailSetting = _objmaster.GetEmailSettingDetail(_siteId);
                if (emailSetting != null)
                {
                    var sendEmail = _objmaster.SendContactUsMail(_siteId, Subject, body, emailSetting.Email, "dipu.bharti@dotsquares.com");//txtEmail.Text
                }
            }
            else
                ShowMessage(2, "We've been unable to register an account with this email address as it appears you’re lucky enough to already have one! If you can't remember your password, you can reset you it here. <a href='ForgotPassword'> Forgotten password.</a>");
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            ShowMessage(2, ex.Message);
        }
    }

    public void SendMailForEnquiry(Guid userid, Guid siteid, Guid lookupid)
    {
        try
        {
            var QMrec = _ManageEnquiry.Getenquirybyid(lookupid);
            var Crec = _ManageEnquiry.GetUserbyid(userid);
            var Srec = _ManageEnquiry.Getsitebyid(_siteId);
            var EmailType = _ManageEnquiry.GetEmailByCode("Enquiry");
            if (EmailType != null)
            {
                var GetEmailTemplate = _ManageEnquiry.GetTemplateById(EmailType.Id);
                if (GetEmailTemplate != null)
                {
                    string emailHTML = GetEmailTemplate.EmailContent;
                    string sendingEmailAddress = string.Empty;
                    if (QMrec != null && Srec != null && Crec != null)
                    {
                        emailHTML = emailHTML.Replace("{CustomerName}", Convert.ToString(Crec.FirstName) + " " + Convert.ToString(Crec.LastName));
                        emailHTML = emailHTML.Replace("{EnquiryID}", Convert.ToString(QMrec.EnquiryID));
                        emailHTML = emailHTML.Replace("{QueryMessage}", Convert.ToString(QMrec.QueryMessage));
                        List<Attachment> attlist = new List<Attachment>();
                        DataTable dt = Upload_Files(lookupid, QMrec.Id, Crec.ID, emailHTML, GetEmailTemplate.Subject, GetEmailTemplate.Subject);
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow row in dt.Rows)
                                {
                                    string FileName = row[0].ToString();
                                    string Downloadfilename = row[2].ToString();
                                    Attachment attachment = new Attachment(Server.MapPath("CopyFiles\\") + Downloadfilename);
                                    attlist.Add(attachment);
                                }
                            }
                        }
                        sendingEmailAddress = Crec.Email;
                        var emailSetting = _objmaster.GetEmailSettingDetail(_siteId);
                        bool issent = _objmaster.SendEmailMessage(_siteId, "1Track EQO", emailSetting.Email, sendingEmailAddress, "", "", "Enquiry", emailHTML, attlist.ToArray(), "Enquiry", "Create", Convert.ToString(QMrec.Id));
                        if (issent)
                        {
                            ShowMessage(1, "Thank you for your interest in International Rail. Your comment/query has been sent successfully. This information will enable us to route your request to the appropriate person. You should receive a response soon.");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {

            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            ShowMessage(2, ex.Message);
        }
    }

    public void InsertINTOMailTables(Guid EnquiryID, Guid Touserid, String emailHTML, string subject, string msgheader, DataTable dt)
    {
        tblEQOMail model = new tblEQOMail();
        model.FromId = Guid.Parse("98D39398-D622-4FD0-80A1-B725FC05BC0F");
        model.ToId = Touserid;
        model.EQOEnquiryId = EnquiryID;
        model.Subject = subject;
        model.MessageHeader = msgheader;
        model.Message = emailHTML;
        model.MailDate = DateTime.Now;
        model.IsDeleted = false;
        model.IsForwarded = false;
        model.IsRead = false;
        model.IsReceived = false;
        model.IsReplied = false;
        model.IsSend = false;
        var objResult = _ManageEnquiry.Create(model);

        //insert into tblEQOMailTo
        tblEQOMailTo model1 = new tblEQOMailTo();
        model1.Bcc = "";
        model1.Ccc = "";
        model1.ToId = Touserid;

        if (objResult > 0)
        {
            model1.MailId = objResult;
            var objResult1 = _ManageEnquiry.CreateMailTo(model1);
        }
        //insert into attatchments
        if (dt.Rows.Count > 0)
        {
            tblEQOMailsAttachment model2 = null;
            foreach (DataRow row in dt.Rows)
            {
                model2 = new tblEQOMailsAttachment();
                model2.MailId = model1.MailId;
                model2.AttachfileName = row[0].ToString();
                model2.DocPath = siteURL + Convert.ToString(row[1]);
                var objResult2 = _ManageEnquiry.CreateMailAttatchments(model2);
            }
        }
    }

    private void ClearData()
    {
        txtFirstName.Text = string.Empty;
        txtLastName.Text = string.Empty;
        txtPhone.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtdesc.Text = string.Empty;
        txtFirstName.Text = txtLastName.Text = txtPhone.Text = txtdesc.Text = txtEmail.Text = txtFrom.Text = txtTo.Text = "";
        txtDepartureDate.Text = txtReturnDate.Text = "";
    }

    public string LoadSiteLogo()
    {
        var data = _ManageEnquiry.Getsitebyid(_siteId);
        if (data != null)
        {
            if (!string.IsNullOrEmpty(data.LogoPath))
                return AdminSiteUrl + data.LogoPath;
            else
                return siteURL + "assets/img/branding/logo.png";
        }
        return "https://www.internationalrail.com/images/logo.png";
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.InnerHtml = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.InnerHtml = string.Empty;
                lblSuccessMsg.Text = message;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "somekey", "autoHide();", true);
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.InnerHtml = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected DataTable Upload_Files(Guid lookupid, Guid EnquiryID, Guid Touserid, String emailHTML, string subject, string msgheader)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("FileName", typeof(string));
        dt.Columns.Add("DocPath", typeof(string));
        dt.Columns.Add("downloaddocname", typeof(string));

        if (fileUpload.HasFile)
        {
            HttpFileCollection hfc = Request.Files;

            if (hfc.Count <= 10)    // 10 FILES RESTRICTION.
            {
                for (int i = 0; i <= hfc.Count - 1; i++)
                {
                    HttpPostedFile hpf = hfc[i];
                    if (hpf.ContentLength > 0)
                    {
                        if (!File.Exists(Server.MapPath("CopyFiles\\") +
                            Path.GetFileName(hpf.FileName)))
                        {

                            // SAVE THE FILE IN A FOLDER.
                            string[] arr = hpf.FileName.Split('.');
                            string downloaddocname = arr[0] + "_" + Guid.NewGuid() + "." + arr[1];
                            downloaddocname = downloaddocname.Replace(" ", "");
                            hpf.SaveAs(Server.MapPath("CopyFiles\\") + Path.GetFileName(downloaddocname));

                            //Save the file tblEQOEnquiryDoc
                            string docpath = "eqo/CopyFiles/" + Path.GetFileName(downloaddocname);
                            string extension = System.IO.Path.GetExtension(downloaddocname);

                            //save eqo enquiry doc
                            SaveEQOEnquiryDoc(lookupid, Path.GetFileName(hpf.FileName), docpath, downloaddocname);

                            dt.Rows.Add(Path.GetFileName(hpf.FileName), docpath, downloaddocname);
                        }
                    }
                }
            }
        }
        InsertINTOMailTables(EnquiryID, Touserid, emailHTML, subject, msgheader, dt);
        return dt;
    }

    public Guid SaveEQOEnquiryDoc(Guid lookupid, string filename, string docpath, string downloaddocname)
    {
        tblEQOEnquiryDoc look = new tblEQOEnquiryDoc()
        {
            Id = Guid.NewGuid(),
            EQOLookupId = lookupid,
            DocName = filename,
            DocPath = docpath,
            DownloadDocName = downloaddocname

        };
        Guid enq = _ManageEnquiry.SaveeqoEnquiryDoc(look);
        return enq;
    }


    private void InsertActivityLog(string routeInfo, string url, Guid? userID, string activityLog, string logType)
    {
     
        tblEQOActivityLog log = new tblEQOActivityLog()
        {
            UserID = userID,
            AccessArea = routeInfo,
            AccessPath = url,
            DateAndTime = HttpContext.Current.Timestamp,
            IP = new ManageBooking().GetIpAddress(),
            IsSuccess = HttpContext.Current.Error == null ? true : false,
            ExceptionId = HttpContext.Current.Error == null ? Guid.Empty : Guid.NewGuid(),
            ActivityLog = activityLog,
            LogType = logType,
            Browser = HttpContext.Current.Request.Browser.Browser.ToString() + " " + HttpContext.Current.Request.Browser.Version.ToString()
        };
        if (!activityLog.ToLower().Contains("error"))
        {
            if (activityLog.Contains("created") || activityLog.Contains("updated") || activityLog.Contains("deleted") || activityLog != string.Empty)
            {
                long activitylog = _ManageEnquiry.SaveEQOActivityLog(log);
            }
        }
    }
}

public class Emailfilter
{
    public Guid ID { get; set; }
    public string Email { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Phone { get; set; }
    public string DisplayName { get; set; }
}