﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="EnquiryForm.aspx.cs" Inherits="EQO_EnquiryForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../assets/searchabletext/Jquery.searchabletext.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery.MultiFile.js"></script>
    <style type="text/css">
        .customsubtext
        {
            font-size: 11px;
            color: #006ecc;
            padding-left: 10px;
        }
        #divFile p
        {
            font: 13px tahoma, arial;
        }
        #divFile h3
        {
            font: 16px arial, tahoma;
            font-weight: bold;
        }
        #_bindProductDivData
        {
            position: absolute;
            border: 1px solid #ccc;
            z-index: 999;
            padding-top: 5px;
            width: 95%;
        }
        #Pass div.starail-Form-inputContainer-col
        {
            position: relative;
        }
        
        #_bindEmailDivData
        {
            position: absolute;
            border: 1px solid #ccc;
            z-index: 999;
            padding-top: 5px;
            width: 95%;
        }
        #_bindEmailDivData div
        {
            font-size: 14px;
        }
        #_bindEmailDivData div:first-letter
        {
            text-transform: uppercase;
        }
        #_bindDivData
        {
            width: 57%;
        }
        .rdn-first
        {
            padding-right: 2%;
        }
        .starail-Form-inputContainer--inputGrid .starail-Form-inputContainer-col
        {
            padding: 0 !important;
        }
        #MainContent_chkIsflexible + label
        {
            padding-left: 5px;
        }
        .switchRadioGroup td:first
        {
            width: 50%;
        }
        .switchRadioGroup td:last
        {
            width: 50%;
        }
        #MainContent_rdBookingType
        {
            width: 45%;
        }
        #MainContent_rdBookingType td
        {
            width: 50%;
        }
        .email-text
        {
            height: 36px;
            width: 77%;
        }
        .email-text .starail-Form-inputContainer
        {
            overflow: visible;
        }
        .email-text .starail-Form-inputContainer-col
        {
            position: relative;
        }
    </style>
    <script type="text/javascript">
        var unavailableDates = '<%=unavailableDates1 %>';
        function nationalDays(date) {
            dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
            if ($.inArray(dmy, unavailableDates) > -1) {
                return [false, "", "Unavailable"];
            }
            return [true, ""];
        }

        function DatePickerCall() {
            $("#MainContent_txtDepartureDate, #MainContent_txtReturnDate").bind("contextmenu cut copy paste", function (e) {
                return false;
            });
            $("#MainContent_txtDepartureDate, #MainContent_txtReturnDate").keypress(function (event) { event.preventDefault(); });
            LoadCal1();
        }

        function LoadCal1() {
            $("#MainContent_txtDepartureDate").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                maxDate: '+2y',
            });

            $("#MainContent_txtReturnDate").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                maxDate: '+2y',
            });

            $(".imgCalSendBox").click(function () {
                $("#MainContent_txtDepartureDate").datepicker('show');
            });

            $(".imgCalSendBox1").click(function () {
                if ($('#MainContent_rdBookingType_1').is(':checked')) {
                    $("#MainContent_txtReturnDate").datepicker('show');
                }
            });

            if ($('#MainContent_ucTrainSearch_rdBookingType_1').is(':checked')) {
                $("#txtReturnDate").datepicker('enable');
            }
            else {
                $("#txtReturnDate").datepicker('disable');
            }
            $("#MainContent_txtDepartureDate, #MainContent_txtReturnDate").keypress(function (event) { event.preventDefault(); });

        }

        function calenableT() {
            LoadCal1();
            $("#MainContent_txtReturnDate").datepicker('enable').val($("#MainContent_txtDepartureDate").val());
        }
        function caldisableT() {
            LoadCal1();
            $("#MainContent_txtReturnDate").datepicker('disable').val("DD/MM/YYYY");
        }
    </script>
    <script type="text/javascript">
        $(document).click(function (event) {
            $('#_bindEmailDivData').remove();
        });
        function _Bindthisvalue_x(e) {
            var idtxtbox = $('#_bindDivData').prev("input").attr('id');
            $("#" + idtxtbox + "").val($(e).find('span').text());
            if (idtxtbox == 'txtTo') {
                $('#spantxtFrom').text($(e).find('div').text());
                localStorage.setItem("spantxtFrom", $(e).find('div').text());
                $("#hdntxtToRailName").val($(e).find('p').text());
            }
            else {
                $('#spantxtTo').text($(e).find('div').text());
                localStorage.setItem("spantxtTo", $(e).find('div').text());
                $("#hdntxtFromRailName").val($(e).find('p').text());
            }
            $('#_bindDivData').remove();
        }

        function selectpopup(e) {
            if (count != 40 && count != 38 && count != 13 && count != 37 && count != 39 && count != 9) {
                var $this = $(e);
                var data = $this.val();
                var station = '';
                var hostName = window.location.host;
                var url = "http://" + hostName;

                if (window.location.toString().indexOf("https:") >= 0)
                    url = "https://" + hostName;

                if ($("#txtFrom").val() == '' && $("#txtTo").val() == '') {
                    $('#spantxtFrom').text('');
                    $('#spantxtTo').text('');
                }

                var filter = $("#span" + $this.attr('id') + "").text();
                if (filter == "" && $this.val() != "")
                    filter = $("#hdnFilter").val();
                $("#hdnFilter").val(filter);

                if ($this.attr('id') == 'txtTo') {
                    station = $("#txtFrom").val();
                }
                else {
                    station = $("#txtTo").val();
                }

                if (hostName == "localhost") {
                    url = url + "/interrailnew";
                }
                var hostUrl = url + "/StationList.asmx/getStationsXList";

                data = data.replace(/[']/g, "♥");
                var $div = $("<div id='_bindDivData'/>");
                $.ajax({
                    type: "POST",
                    url: hostUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{'prefixText':'" + data + "','filter':'" + filter + "','station':'" + station + "'}",
                    success: function (msg) {
                        $('#_bindDivData').remove();
                        var lentxt = data.length;
                        $.each(msg.d, function (key, value) {
                            var splitdata = value.split('ñ');
                            var lenfull = splitdata[0].length;;
                            var txtupper = splitdata[0].substring(0, lentxt);
                            var txtlover = splitdata[0].substring(lentxt, lenfull);

                            $this.after($div.append("<div class='popupselect' onclick='_Bindthisvalue_x(this)'><b class='_prifixText'>" + txtupper + "</b>" + txtlover + "<span style='display:none'>" + splitdata[0] + "</span><div style='display:none'>" + splitdata[1] + "</div><p style='display:none'>" + splitdata[2] + "</p></div>"));
                        });

                        $(".popupselect:eq(0)").attr('style', 'background-color: #ccc !important');
                    },
                    error: function () {
                        //                    alert("Wait...");
                    }
                });
            }
        }

        function Getkeydown(event, obj) {
            var $id = $(obj);
            var maxno = 0;
            count = event.keyCode;
            $(".popupselect").each(function () { maxno++; });
            if (count == 13 || count == 9) {
                $(".popupselect").each(function () {
                    if ($(this).attr('style') != undefined) {
                        $(this).trigger('onclick');
                        $id.val($.trim($(this).find('span').text()));
                    }
                });
                $('#_bindDivData').remove();
                countKey = 1;
                conditionone = 0;
                conditiontwo = 0;
            }
            else if (count == 40 && maxno > 1) {
                conditionone = 1;
                if (countKey == maxno)
                    countKey = 0;
                if (conditiontwo == 1) {
                    countKey++;
                    conditiontwo = 0;
                }
                $(".popupselect").removeAttr('style');
                $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
                countKey++;
            }
            else if (count == 38 && maxno > 1) {
                conditiontwo = 1;
                if (countKey == 0)
                    countKey = maxno;
                if (conditionone == 1) {
                    countKey--;
                    conditionone = 0;
                }
                countKey--;
                $(".popupselect").removeAttr('style');
                $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
            }
            else {
                countKey = 1;
                conditionone = 0;
                conditiontwo = 0;
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            showHidePanel();
            showHideDateTime();
            $('.rdn-list').find(':radio').change(function () {
                showHidePanel();
            });
            $('[id*=rdBookingType]').find(':radio').change(function () {
                showHideDateTime();
            });
            $('.validPhone').keypress(function (event) {
                var keycode = event.which;
                if (!(keycode >= 48 && keycode <= 57 || keycode == 8 || keycode == 0 || keycode == 32 || keycode == 40 || keycode == 41 || keycode == 43)) {
                    event.preventDefault();
                }
            });
        });

        function showHidePanel() {
            if ($('[id*=rdPass]').is(':checked')) {
                $('[id*=divP2P]').hide();
                $('[id*=Pass]').show();
                ValidatorEnable($('[id*=reqvalerror1]')[0], false);
                ValidatorEnable($('[id*=reqvalerror2]')[0], false);
                ValidatorEnable($('[id*=reqvalerror11]')[0], false);
                ValidatorEnable($('[id*=reqvalerror3]')[0], false);
            }
            else if ($('[id*=rdP2P]').is(':checked')) {
                $('[id*=rdPass]').show();
                $('[id*=divP2P]').show();
                ValidatorEnable($('[id*=reqvalerror1]')[0], true);
                ($('[id*=reqvalerror2]')[0], true);
                ValidatorEnable($('[id*=reqvalerror11]')[0], true);
            }
        }
        function showHideDateTime() {
            if ($('[id*=rdBookingType_0]').is(':checked')) {
                $('[id*=txtReturnDate]').attr('disabled', true);
                $('[id*=ddlReturnTime]').attr('disabled', true);
                $('.return-datetime').hide();
                ValidatorEnable($('[id*=reqvalerror3]')[0], false);
            }
            else if ($('[id*=rdBookingType_1]').is(':checked')) {
                $('[id*=txtReturnDate]').attr('disabled', false);
                $('[id*=ddlReturnTime]').attr('disabled', false);
                $('.return-datetime').show();
                ValidatorEnable($('[id*=reqvalerror3]')[0], true);
            }
        }
        function autoHide() {
            setTimeout(function () {
                $('#MainContent_DivSuccess').fadeOut('fast');
            }, 5000);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <input id="sessionInput" type="hidden" value='<%= Session["holdCustInfo"] %>' />
    <div class="starail-BookingDetails-form">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" />
            </div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <div id="lblErrorMsg" runat="server" class="starail-Form-required">
                </div>
            </div>
        </asp:Panel>
        <div class="starail-Form-row">
            <h2>
                Enquiry Form</h2>
        </div>
      
        <div class="starail-Form-row email-text">
            <label class="starail-Form-label" for="starail-passportno">
                Email <span class="starail-Form-required">*</span></label>
            <asp:HiddenField ID="hdnuserid" runat="server" />
            <asp:HiddenField ID="hdnfn" runat="server" />
            <asp:HiddenField ID="hdnln" runat="server" />
            <asp:HiddenField ID="hdnph" runat="server" />
            <div class="starail-Form-inputContainer">
                <div class="starail-Form-inputContainer-col">
                    <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" MaxLength="200" autocomplete="off"
                        CssClass="starail-Form-input"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqvalerrorEmail" runat="server" ControlToValidate="txtEmail"
                        CssClass="absolute" ErrorMessage="Please enter email." ValidationGroup="reg"
                        Display="Dynamic" Text="*" />
                    <asp:RegularExpressionValidator ID="reqcustvalerrorBA3" runat="server" CssClass="starail-Form-required absolute"
                        ErrorMessage="Please enter a valid email." ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        Display="Static" Text="*" ValidationGroup="reg" />
                </div>
            </div>
        </div>
        <div class="starail-Form-row">
            <label class="starail-Form-label" for="starail-passportno">
                First Name <span class="starail-Form-required">*</span></label>
            <div class="starail-Form-inputContainer">
                <div class="starail-Form-inputContainer-col">
                    <asp:TextBox ID="txtFirstName" runat="server" placeholder="First Name" MaxLength="200"
                        CssClass="starail-Form-input"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqvalerrorFirstName" CssClass="absolute" Display="Dynamic"
                        runat="server" ControlToValidate="txtFirstName" ValidationGroup="reg"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="starail-Form-row">
            <label class="starail-Form-label" for="starail-passportno">
                Last Name
            </label>
            <div class="starail-Form-inputContainer">
                <div class="starail-Form-inputContainer-col">
                    <asp:TextBox ID="txtLastName" runat="server" placeholder="Last Name" MaxLength="200"
                        CssClass="starail-Form-input"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="starail-Form-row">
            <label class="starail-Form-label" for="starail-passportno">
                Phone Number
            </label>
            <div class="starail-Form-inputContainer">
                <div class="starail-Form-inputContainer-col">
                    <asp:TextBox ID="txtPhone" runat="server" placeholder="Phone Number" MaxLength="20"
                        CssClass="starail-Form-input validPhone"></asp:TextBox>
                </div>
            </div>
        </div>
      
        <div class="starail-Form-row">
            <label for="" class="starail-Form-label">
                Choose Pass Type
            </label>
            <div class="starail-Form-inputContainer rdn-list">
                <asp:RadioButton ID="rdPass" runat="server" Text="Pass" GroupName="Software" Checked="true"
                    CssClass="rdn-first" />
                <asp:RadioButton ID="rdP2P" runat="server" CssClass="rdn-second" Text="P2P" GroupName="Software" />
            </div>
        </div>
        <div id="Pass" style="display: none">
            <%-- <div class="starail-Form-row">
                <label class="starail-Form-label" for="starail-email">
                    Select Category</label>
                <div class="starail-Form-inputContainer">
                    <div class="starail-Form-inputContainer-col">
                    
                         <asp:DropDownList ID="ddlCategory" runat="server" CssClass="starail-Form-input">
                                 </asp:DropDownList>
                    </div>
                </div>
            </div>--%>
        </div>
        <div class="starail-Form-row">
        </div>
        <div id="divP2P" runat="server" style="display: none;">
            <div class="starail-Form-row">
                <label class="starail-Form-label" for="starail-email">
                    From<span class="starail-Form-required">*</span></label>
                <div class="starail-Form-inputContainer  train-result-journyFromTo">
                    <div class="starail-Form-inputContainer-col">
                        <asp:TextBox ID="txtFrom" runat="server" Text="" ClientIDMode="Static" onkeyup="selectpopup(this);"
                            placeholder="From" autocomplete="off" onkeydown="Getkeydown(event,this)" class="starail-Form-input" />
                        <span id="spantxtFrom" style="display: none"></span>
                        <asp:HiddenField ID="hdnFrm" runat="server" />
                        <asp:HiddenField ID="hdnFilter" runat="server" ClientIDMode="Static" />
                        <asp:RequiredFieldValidator ID="reqvalerror1" runat="server" ValidationGroup="reg"
                            Display="Dynamic" ControlToValidate="txtFrom" />
                    </div>
                </div>
            </div>
            <div class="starail-Form-row">
            </div>
            <div class="starail-Form-row">
                <label class="starail-Form-label" for="starail-email">
                    To<span class="starail-Form-required">*</span></label>
                <div class="starail-Form-inputContainer train-result-journyFromTo">
                    <div class="starail-Form-inputContainer-col">
                        <asp:TextBox ID="txtTo" runat="server" onkeyup="selectpopup(this);" onkeydown="Getkeydown(event,this)"
                            autocomplete="off" placeholder="To" ClientIDMode="Static" class="starail-Form-input" />
                        <span id="spantxtTo" style="display: none"></span>
                        <asp:RequiredFieldValidator ID="reqvalerror2" runat="server" ValidationGroup="reg"
                            Display="Dynamic" ControlToValidate="txtTo" />
                    </div>
                </div>
            </div>
            <div class="starail-Form-row">
            </div>
            <div class="starail-Form-row">
                <label for="" class="starail-Form-label starail-u-invisible starail-u-visuallyHiddenMobile journeylbl">
                    Journey Type</label>
                <div class="starail-u-cf starail-Form-switchRadioGroup">
                    <asp:RadioButtonList ID="rdBookingType" runat="server" RepeatDirection="Horizontal"
                        CssClass="switchRadioGroup">
                        <asp:ListItem Value="0" Selected="True">One-way</asp:ListItem>
                        <asp:ListItem Value="1">Return</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="starail-Form-row">
            </div>
            <div class="starail-Form-row">
                <label class="starail-Form-label" for="starail-email">
                    Leaving<span class="starail-Form-required">*</span></label>
                <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                    <div class="starail-Form-datePicker">
                        <asp:TextBox ID="txtDepartureDate" runat="server" class="starail-Form-input" Text="DD/MM/YYYY"
                            autocomplete="off" onfocus="DatePickerCall()" />
                        <asp:RequiredFieldValidator ID="reqvalerror11" runat="server" ValidationGroup="reg"
                            InitialValue="DD/MM/YYYY" Display="Dynamic" ControlToValidate="txtDepartureDate" />
                        <i class="starail-Icon-datepicker"></i>
                    </div>
                    <asp:DropDownList ID="ddldepTime" runat="server" CssClass="starail-Form-select starail-Form-inputContainer--time starail-Form-inputContainer--last">
                        <asp:ListItem>00:00</asp:ListItem>
                        <asp:ListItem>01:00</asp:ListItem>
                        <asp:ListItem>02:00</asp:ListItem>
                        <asp:ListItem>03:00</asp:ListItem>
                        <asp:ListItem>04:00</asp:ListItem>
                        <asp:ListItem>05:00</asp:ListItem>
                        <asp:ListItem>06:00</asp:ListItem>
                        <asp:ListItem>07:00</asp:ListItem>
                        <asp:ListItem>08:00</asp:ListItem>
                        <asp:ListItem Selected="True">09:00</asp:ListItem>
                        <asp:ListItem>10:00</asp:ListItem>
                        <asp:ListItem>11:00</asp:ListItem>
                        <asp:ListItem>12:00</asp:ListItem>
                        <asp:ListItem>13:00</asp:ListItem>
                        <asp:ListItem>14:00</asp:ListItem>
                        <asp:ListItem>15:00</asp:ListItem>
                        <asp:ListItem>16:00</asp:ListItem>
                        <asp:ListItem>17:00</asp:ListItem>
                        <asp:ListItem>18:00</asp:ListItem>
                        <asp:ListItem>19:00</asp:ListItem>
                        <asp:ListItem>20:00</asp:ListItem>
                        <asp:ListItem>21:00</asp:ListItem>
                        <asp:ListItem>22:00</asp:ListItem>
                        <asp:ListItem>23:00</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="starail-Form-row return-datetime">
                <label for="returning" class="starail-Form-label">
                    Returning
                </label>
                <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                    <div class="starail-Form-datePicker loyreturntxt">
                        <asp:TextBox ID="txtReturnDate" runat="server" class="starail-Form-input" Text="DD/MM/YYYY"
                            autocomplete="off" Enabled="False" />
                        <asp:RequiredFieldValidator ID="reqvalerror3" runat="server" ValidationGroup="reg"
                            InitialValue="DD/MM/YYYY" Enabled="false" Display="Dynamic" ControlToValidate="txtReturnDate" />
                        <i class="starail-Icon-datepicker"></i>
                    </div>
                    <asp:DropDownList ID="ddlReturnTime" runat="server" CssClass="loyreturnselect" Enabled="false">
                        <asp:ListItem>00:00</asp:ListItem>
                        <asp:ListItem>01:00</asp:ListItem>
                        <asp:ListItem>02:00</asp:ListItem>
                        <asp:ListItem>03:00</asp:ListItem>
                        <asp:ListItem>04:00</asp:ListItem>
                        <asp:ListItem>05:00</asp:ListItem>
                        <asp:ListItem>06:00</asp:ListItem>
                        <asp:ListItem>07:00</asp:ListItem>
                        <asp:ListItem>08:00</asp:ListItem>
                        <asp:ListItem Selected="True">09:00</asp:ListItem>
                        <asp:ListItem>10:00</asp:ListItem>
                        <asp:ListItem>11:00</asp:ListItem>
                        <asp:ListItem>12:00</asp:ListItem>
                        <asp:ListItem>13:00</asp:ListItem>
                        <asp:ListItem>14:00</asp:ListItem>
                        <asp:ListItem>15:00</asp:ListItem>
                        <asp:ListItem>16:00</asp:ListItem>
                        <asp:ListItem>17:00</asp:ListItem>
                        <asp:ListItem>18:00</asp:ListItem>
                        <asp:ListItem>19:00</asp:ListItem>
                        <asp:ListItem>20:00</asp:ListItem>
                        <asp:ListItem>21:00</asp:ListItem>
                        <asp:ListItem>22:00</asp:ListItem>
                        <asp:ListItem>23:00</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="starail-Form-row">
            </div>
            <div class="starail-Form-row">
                <label for="returning" class="starail-Form-label">
                    Flexible
                </label>
                <div class="starail-Form-inputContainer">
                    <asp:CheckBox ID="chkIsflexible" runat="server" Text="Is Flexible" Width="120px" />
                </div>
            </div>
        </div>
        <div class="starail-Form-row starail-u-cf starail-SearchTickets-quantity">
            <label for="" class="starail-Form-label">
                Who's going?</label>
            <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                <div>
                </div>
                <div class="starail-Form-inputContainer-col">
                    <asp:DropDownList ID="ddlAdult" runat="server" CssClass="starail-Form-select starail-Form-select--narrow">
                    </asp:DropDownList>
                    <label for="starail-adult">
                        Adults (26-65 at time of travel)</label>
                </div>
                <div class="starail-Form-inputContainer-col train-result-col2">
                    <asp:DropDownList ID="ddlChild" runat="server" CssClass="starail-Form-select starail-Form-select--narrow">
                    </asp:DropDownList>
                    <label for="starail-children">
                        Children (under 17 at time of travel)</label>
                </div>
                <div class="starail-Form-inputContainer-col">
                    <asp:DropDownList ID="ddlYouth" runat="server" CssClass="starail-Form-select starail-Form-select--narrow">
                    </asp:DropDownList>
                    <label for="starail-adult">
                        Youths (17-25 at time of travel)</label>
                </div>
                <div class="starail-Form-inputContainer-col train-result-col2">
                    <asp:DropDownList ID="ddlSenior" runat="server" CssClass="starail-Form-select starail-Form-select--narrow">
                    </asp:DropDownList>
                    <label for="starail-adult">
                        Seniors (over 66 at time of travel)</label>
                </div>
            </div>
        </div>
        <div class="starail-Form-row">
            <label class="starail-Form-label" for="starail-passportno">
                Message <span class="starail-Form-required">*</span></label>
            <div class="starail-Form-inputContainer">
                <div class="starail-Form-inputContainer-col">
                    <asp:TextBox ID="txtdesc" runat="server" placeholder="Message" TextMode="MultiLine"
                        CssClass="starail-Form-input" Rows="10" Columns="6"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqvalerrorMessage" runat="server" ControlToValidate="txtdesc"
                        CssClass="absolute" ErrorMessage="Please enter message." ValidationGroup="reg"
                        Display="Dynamic" Text="*" />
                    <asp:HiddenField ID="hdnEmail" runat="server" />
                </div>
            </div>
        </div>
        <div class="starail-Form-row">
            <label class="starail-Form-label" for="starail-passportno">
                Documents</label>
            <div class="starail-Form-inputContainer">
                <div class="starail-Form-inputContainer-col">
                    <div id="divFile">
                        <p>
                            <asp:FileUpload ID="fileUpload" class="multi" multiple="true" runat="server"  /></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="starail-Form-row">
            <label class="starail-Form-label" for="starail-country">
            </label>
            <div class="starail-Form-inputContainer">
                <div class="starail-Form-inputContainer-col">
                    <asp:Button ID="btnSubmit" runat="server" Text="Save Enquiry" ValidationGroup="reg"
                        OnClick="btnSubmit_Click" CssClass="starail-Button starail-Form-button starail-Form-button--primary" />
                </div>
            </div>
        </div>
        <div class="starail-Form-row">
            <asp:Label runat="server" ID="lblmsg" />
        </div>
        <ul class="starail-Checklist">
            <li><span class="starail-Form-required">*</span>Please complete all mandatory fields.</li>
        </ul>
    </div>
</asp:Content>
