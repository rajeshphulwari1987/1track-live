﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using OneHubServiceRef;
using Business;
using System.Configuration;
using System.Web;

public partial class LoyaltyCardInfo : Page
{
    public int minDate = 0;
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    BookingRequestUserControl objBRUC;
    public static string unavailableDates1 = "";
    private Guid siteId;
    public string siteURL;
    public string script;
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    ManageOneHub _ManageOneHub = new ManageOneHub();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            ShowHaveRailPass(siteId);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "LoadCal", "LoadCal();", true);
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            var siteDDates = new ManageHolidays().GetAllHolydaysBySite(siteId);
            unavailableDates1 = "[";
            if (siteDDates.Count() > 0)
            {
                foreach (var it in siteDDates)
                {
                    unavailableDates1 += "\"" + Convert.ToDateTime(it.DateofHoliday).ToString("dd-MM-yyyy") + "\"" + ",";
                }
                unavailableDates1 = unavailableDates1.Substring(0, unavailableDates1.Length - 1);
            }
            unavailableDates1 += "]";
        }

        if (!IsPostBack)
        {
            #region Seobreadcrumbs
            var pageID = (Guid)Page.RouteData.Values["PageId"];
            var dataSeobreadcrumbs = new ManageSeo().Getlinkbody(pageID, siteId);
            if (dataSeobreadcrumbs != null && dataSeobreadcrumbs.IsActive)
                litSeobreadcrumbs.Text = dataSeobreadcrumbs.SourceCode;
            #endregion

            bool isUseSite = (bool)_oWebsitePage.IsUsSite(siteId);
            //divSearch.Visible = (bool)_oWebsitePage.IsVisibleP2PWidget(siteId);
            minDate = _oWebsitePage.GetBookingDayLimitBySiteId(siteId);
            rdBookingType.Items[1].Text = isUseSite ? "Round Trip" : "Return";
            script = new Masters().GetQubitScriptBySId(siteId);
            for (int j = 10; j >= 0; j--)
            {
                ddlAdult.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlChild.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlYouth.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlSenior.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlAdult.SelectedValue = "1";
            }
            // Vesa: Bug project/2/issue/500
            // FillPageInfo();

            if (Page.RouteData.Values["PageId"] != null)
            {
                var pageId = (Guid)Page.RouteData.Values["PageId"];
                PageContent(pageId, siteId);
            }
        }
    }

    void ShowHaveRailPass(Guid siteID)
    {
        var railPass = _oWebsitePage.HavRailPass(siteID);
        //divRailPass.Visible = railPass;
    }

    public void FillPageInfo()
    {
        if (Session["BookingUCRerq"] != null)
        {
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            txtFrom.Text = objBRUC.FromDetail;
            txtTo.Text = objBRUC.ToDetail;
            txtDepartureDate.Text = objBRUC.depdt.ToString("dd/MMM/yyyy");
            ddldepTime.SelectedValue = objBRUC.depTime.ToString("HH:mm");
            rdBookingType.SelectedValue = objBRUC.Journeytype;
            if (objBRUC.ReturnDate != string.Empty)
            {
                txtReturnDate.Enabled = true;
                //reqLoyReturnDate.Enabled = true;
                ddlReturnTime.Enabled = true;
                txtReturnDate.Text = objBRUC.ReturnDate;
                ddlReturnTime.SelectedValue = objBRUC.ReturnTime;
                //reqLoyReturnDate.Enabled = true;
            }
            else
            {
                txtReturnDate.Enabled = false;
                ddlReturnTime.Enabled = false;
                txtReturnDate.Text = "";
                ddlReturnTime.SelectedValue = objBRUC.ReturnTime;
                //reqLoyReturnDate.Enabled = false;
            }
            ddlAdult.SelectedValue = objBRUC.Adults.ToString();
            ddlChild.SelectedValue = objBRUC.Boys.ToString();
            ddlClass.SelectedValue = objBRUC.ClassValue.ToString();
            ddlTransfer.SelectedValue = objBRUC.Transfare.ToString();
            chkLoyalty.Checked = objBRUC.Loyalty;
            chkIhaveRailPass.Checked = objBRUC.isIhaveRailPass;
            hdntxtFromRailName.Value = hdntxtToRailName.Value = !string.IsNullOrEmpty(objBRUC.OneHubServiceName) ? objBRUC.OneHubServiceName.ToUpper() : "";
            chkLoyalty_CheckedChanged(null, null);
        }
    }

    public void BindLoyaltyList()
    {
        var list = new List<Passanger>();
        int cnt = Convert.ToInt32(ddlAdult.SelectedItem.Text.Trim());
        for (int i = 0; i < cnt; i++)
        {
            list.Add(new Passanger
            {
                PassangerType = "Adult"
            });
        }

        int cntc = Convert.ToInt32(ddlChild.SelectedItem.Text.Trim());
        for (int i = 0; i < cntc; i++)
        {
            list.Add(new Passanger
            {
                PassangerType = "Child"
            });
        }

        int cnty = Convert.ToInt32(ddlYouth.SelectedItem.Text.Trim());
        for (int i = 0; i < cnty; i++)
        {
            list.Add(new Passanger
            {
                PassangerType = "Youth"
            });
        }

        int cnts = Convert.ToInt32(ddlSenior.SelectedItem.Text.Trim());
        for (int i = 0; i < cnts; i++)
        {
            list.Add(new Passanger
            {
                PassangerType = "Senior"
            });
        }
        dtlLoayalty.DataSource = list;
        dtlLoayalty.DataBind();
    }

    public List<Loyaltycard> GetLoyaltycardDetails()
    {
        try
        {
            var list = new List<Loyaltycard>();
            foreach (DataListItem item in dtlLoayalty.Items)
            {
                var txtloyCardNoEuro = (TextBox)item.FindControl("txtloyCardNoEur");
                var txtloyCardNoThy = (TextBox)item.FindControl("txtloyCardNoThy");

                //--Add Eurostar Train Loyalty Card Detalis
                string lEuroCode = txtloyCardNoEuro.Text.Trim();
                if (!String.IsNullOrEmpty(lEuroCode))
                    list.Add(new Loyaltycard { cardnumber = lEuroCode, carrier = new Carrier { code = "EUR" } });

                //--Add Thalys Train Loyalty Card Detalis
                string lThyCode = txtloyCardNoThy.Text.Trim();
                if (!String.IsNullOrEmpty(lThyCode))
                    list.Add(new Loyaltycard { cardnumber = lThyCode, carrier = new Carrier { code = "THA" } });
            }

            #region Validate Card Numbers

            var chkduplicate = list.GroupBy(x => x.cardnumber).Select(g => new { Value = g.Key, Count = g.Count() }).OrderByDescending(x => x.Count);
            if (chkduplicate.Any(x => x.Count > 1))
            {
                list = null;
                ShowMessage(2, "Similar (Thalys or Eurostar) card number is not allowed for another traveller.");
            }
            else if (list.Any(x => x.cardnumber.Length < 16))
            {
                list = null;
                ShowMessage(2, "Invalid card number.");
            }
            else if (list.Where(z => z.carrier.code == "EUR").Any(x => x.cardnumber.Substring(0, 6) != "308381"))
            {
                list = null;
                ShowMessage(2, "The number of your frequent traveller programme (Thalys The Card or Eurostar Frequent Traveller) is not correct. Please check the number of your frequent traveller card");
            }

            else if (list.Where(z => z.carrier.code == "THA").Any(x => x.cardnumber.Substring(0, 6) != "308406"))
            {
                list = null;
                ShowMessage(2, "The number of your frequent traveller programme (Thalys The Card or Eurostar Frequent Traveller) is not correct. Please check the number of your frequent traveller card");
            }

            #endregion

            return list;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
            return null;
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected void chkLoyalty_CheckedChanged(object sender, EventArgs e)
    {
        if (chkLoyalty.Checked)
        {
            BindLoyaltyList();
            FtpCardsDescriptionContent.Visible = true;
            //chkIhaveRailPass.Checked = false;
        }
        else
        {
            dtlLoayalty.DataSource = null;
            dtlLoayalty.DataBind();
            FtpCardsDescriptionContent.Visible = false;
        }
    }

    protected void rdBookingType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rdBookingType.SelectedValue == "0")
        {
            txtReturnDate.Enabled = false;
            ddlReturnTime.Enabled = false;
            reqvalerror7.Enabled = false;
            ReturningDiv.Attributes.Add("style", "display:none;");
            ScriptManager.RegisterStartupScript(Page, GetType(), "cal", "caldisable();ChangeChildAgeForEvolvi();coalapsibleh4();", true);
        }
        else
        {
            txtReturnDate.Enabled = true;
            ddlReturnTime.Enabled = true;
            reqvalerror7.Enabled = true;
            ReturningDiv.Attributes.Add("style", "display:block;");
            ScriptManager.RegisterStartupScript(Page, GetType(), "cal", "calenable();ChangeChildAgeForEvolvi();coalapsibleh4();", true);
        }
    }

    protected void btnCheckout_Click(object sender, EventArgs e)
    {
        try
        {
            #region If P2P Widget is not allowed for this site then searching will not happing
            bool IsVisibleP2PWidget = (bool)_oWebsitePage.IsVisibleP2PWidget(siteId);
            if (!IsVisibleP2PWidget)
                return;
            #endregion
            Search();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void Search()
    {
        try
        {
            BookingRequestUserControl objBruc = new BookingRequestUserControl();
            ManageBooking objBooking = new ManageBooking();

            objBruc.FromDetail = txtFrom.Text.Trim();
            objBruc.ToDetail = txtTo.Text.Trim();
            objBruc.depdt = DateTime.ParseExact(txtDepartureDate.Text, "dd/MMM/yyyy", null);
            objBruc.depTime = Convert.ToDateTime(ddldepTime.SelectedValue);

            GetStationDetailsByStationName objStationDeptDetail = objBooking.GetStationDetailsByStationName(txtFrom.Text.Trim());
            objBruc.depstCode = objStationDeptDetail != null ? objStationDeptDetail.StationCode : string.Empty;
            objBruc.depRCode = objStationDeptDetail != null ? objStationDeptDetail.RailwayCode : "0";
            ViewState["DeptrailName"] = objStationDeptDetail != null ? objStationDeptDetail.RailName : "";

            GetStationDetailsByStationName objStationArrDetail = objBooking.GetStationDetailsByStationName(txtTo.Text.Trim());
            objBruc.arrstCode = objStationArrDetail != null ? objStationArrDetail.StationCode : string.Empty;
            objBruc.arrRCode = objStationArrDetail != null ? objStationArrDetail.RailwayCode : "0";
            ViewState["ArvtrailName"] = objStationArrDetail != null ? objStationArrDetail.RailName : "";

            List<EvolviRailCardRequestInfo> objRailCardList = new List<EvolviRailCardRequestInfo>();
            if (objStationDeptDetail != null)
            {
                if (objStationDeptDetail.RailName.Trim() == "BENE")
                    objBruc.OneHubServiceName = "BeNe";
                else if (objStationDeptDetail.RailName.Trim() == "NTV")
                    objBruc.OneHubServiceName = "NTV";
                else if (objStationDeptDetail.RailName.Trim() == "ITALIA")
                    objBruc.OneHubServiceName = "Trenitalia";
                else if (objStationDeptDetail.RailName.Trim() == "EVOLVI")
                {
                    objBruc.OneHubServiceName = "Evolvi";
                    objBruc.SequenceNo = System.Web.Configuration.WebConfigurationManager.AppSettings["SequenceNo"].ToString();
                    objBruc.MaxResponse = System.Web.Configuration.WebConfigurationManager.AppSettings["MaxResponse"].ToString();
                    objBruc.ISOCountry = new ManageBooking().GetCountryISOCode(siteId);
                    objBruc.ISOCurrency = new ManageBooking().GetCurrencyISOCode(siteId);
                    objBruc.isRailCard = false;
                    objBruc.EvolviRailCardRequestInfoList = objRailCardList;
                }
            }

            objBruc.ClassValue = Convert.ToInt32(ddlClass.SelectedValue);
            objBruc.Adults = Convert.ToInt32(ddlAdult.SelectedValue);
            objBruc.Boys = Convert.ToInt32(ddlChild.SelectedValue);
            objBruc.Seniors = Convert.ToInt32(ddlSenior.SelectedValue);
            objBruc.Youths = Convert.ToInt32(ddlYouth.SelectedValue);
            objBruc.Transfare = Convert.ToInt32(ddlTransfer.SelectedValue);
            objBruc.ReturnDate = String.IsNullOrEmpty(txtReturnDate.Text) || txtReturnDate.Text.Trim() == "DD/MM/YYYY" ? string.Empty : txtReturnDate.Text;
            objBruc.ReturnTime = ddlReturnTime.SelectedValue;
            objBruc.Loyalty = chkLoyalty.Checked;
            objBruc.isIhaveRailPass = chkIhaveRailPass.Checked;
            objBruc.Journeytype = rdBookingType.SelectedValue;

            if (rdBookingType.SelectedValue == "1")
                objBruc.IsReturnJurney = true;

            if (chkLoyalty.Checked == true)
                objBruc.lstLoyalty = GetLoyaltycardDetails();

            Session["BookingUCRerq"] = objBruc;
            var currDate = DateTime.Now;

            if (objBruc.OneHubServiceName == "BeNe" || objBruc.OneHubServiceName == "Trenitalia")
            {
                int daysLimit = _ManageOneHub.GetEurostarBookingDays(objBruc);
                if (daysLimit == 0)
                    daysLimit = new ManageBooking().getOneHubServiceDayCount(objBruc.OneHubServiceName);

                var maxDate = currDate.AddDays(daysLimit - 1);
                if (objBruc.depdt > maxDate && objStationDeptDetail != null)
                {
                    Session["ErrorMessage"] = "ErrorMaxDate";
                    Session["TrainSearch"] = null;
                    if (ViewState["DeptrailName"] != null && ViewState["ArvtrailName"] != null)
                    {
                        if (ViewState["DeptrailName"].ToString() == "BENE" && ViewState["ArvtrailName"].ToString() == "BENE")
                            Response.Redirect("TrainResults.aspx?req=" + "BE");
                        else if (ViewState["DeptrailName"].ToString() == "ITALIA" && ViewState["ArvtrailName"].ToString() == "ITALIA")
                            Response.Redirect("TrainResults.aspx?req=" + "TI");
                        else if (ViewState["DeptrailName"].ToString() == "NTV" && ViewState["ArvtrailName"].ToString() == "NTV")
                            Response.Redirect("TrainResults.aspx?req=" + "NTV");
                        else if (ViewState["DeptrailName"].ToString() == "EVOLVI" && ViewState["ArvtrailName"].ToString() == "EVOLVI")
                            Response.Redirect("TrainResults.aspx?req=" + "EV");
                    }
                }
            }

            var client = new OneHubRailOneHubClient();
            TrainInformationRequest request = _ManageOneHub.TrainInformation(objBruc, 1, txtDepartureDate.Text, ddldepTime.SelectedValue, txtReturnDate.Text, ddlReturnTime.SelectedValue);

            if (objBruc.OneHubServiceName == "Evolvi")
            {
                request.SequenceNo = System.Web.Configuration.WebConfigurationManager.AppSettings["SequenceNo"].ToString();
                request.MaxResponse = System.Web.Configuration.WebConfigurationManager.AppSettings["MaxResponse"].ToString();
                request.ISOCountry = new ManageBooking().GetCountryISOCode(siteId);
                request.ISOCurrency = new ManageBooking().GetCurrencyISOCode(siteId);
                request.EvolviRailCardRequestInfo = objRailCardList.ToArray();
            }

            if (Convert.ToInt32(rdBookingType.SelectedValue) > 0 && objBruc.OneHubServiceName == "Trenitalia")
                request.IsReturnJourney = false;
            else if (Convert.ToInt32(rdBookingType.SelectedValue) > 0)
                request.IsReturnJourney = true;

            _ManageOneHub.ApiLogin(request, siteId);
            TrainInformationResponse pInfoSolutionsResponse = client.TrainInformation(request);
            if (pInfoSolutionsResponse != null && pInfoSolutionsResponse.TrainInformationList != null)
            {
                //--TreniItalia Search return request                
                if (objBruc.ReturnDate != string.Empty && objBruc.OneHubServiceName == "Trenitalia")
                {
                    List<TrainInfoSegment> list = pInfoSolutionsResponse.TrainInformationList.SelectMany(x => x.TrainInfoSegment.Select(y => y)).ToList();
                    request = _ManageOneHub.TrainInformation(objBruc, 2, txtDepartureDate.Text, ddldepTime.SelectedValue, txtReturnDate.Text, ddlReturnTime.SelectedValue);
                    request.IsReturnJourney = true;
                    _ManageOneHub.ApiLogin(request, siteId);
                    TrainInformationResponse pInfoSolutionsResponseReturn = client.TrainInformation(request);
                    List<TrainInfoSegment> listReturn = pInfoSolutionsResponseReturn.TrainInformationList.SelectMany(x => x.TrainInfoSegment.Select(y => y)).ToList();
                    list.AddRange(listReturn);
                    if (pInfoSolutionsResponseReturn.TrainInformationList != null)
                    {
                        List<TrainInformation> listResp = pInfoSolutionsResponse.TrainInformationList.ToList();
                        List<TrainInformation> listRespReturn = pInfoSolutionsResponseReturn.TrainInformationList.ToList();
                        List<TrainInformation> resultList = listResp.Concat(listRespReturn).ToList();
                        pInfoSolutionsResponse.TrainInformationList = resultList.ToArray();
                    }
                }
            }
            Session["TrainSearch"] = pInfoSolutionsResponse;
            Session["HoldJourneyDetailsList"] = null;
            Session["HoldReserVationRequest"] = null;
            string stri = "BE";
            if (ViewState["DeptrailName"] != null && ViewState["ArvtrailName"] != null)
            {
                if (ViewState["DeptrailName"].ToString() == "BENE" && ViewState["ArvtrailName"].ToString() == "BENE")
                    stri = "BE";
                else if (ViewState["DeptrailName"].ToString() == "ITALIA" && ViewState["ArvtrailName"].ToString() == "ITALIA")
                    stri = "TI";
                else if (ViewState["DeptrailName"].ToString() == "NTV" && ViewState["ArvtrailName"].ToString() == "NTV")
                    stri = "NTV";
                else if (ViewState["DeptrailName"].ToString() == "EVOLVI" && ViewState["ArvtrailName"].ToString() == "EVOLVI")
                    stri = "EV";
            }

            if (chkLoyalty.Checked == true)
                Response.Redirect("TrainResults.aspx?req=" + stri);
            else
                Response.Redirect("TrainResults.aspx?req=" + stri);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void PageContent(Guid pageId, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageId && x.SiteID == siteId);
            if (result != null)
            {
                var url = result.Url;
                tblPage oPage = _masterPage.GetPageDetailsByUrl(url);
                h2_Heading.InnerHtml = oPage.RailTicketInnerHeading;
                h4_Heading.InnerHtml = oPage.RailTicketSubInnerLower;
                p_Content.InnerHtml = oPage.PageContent;

                if (!string.IsNullOrEmpty(oPage.BannerIDs))
                {
                    string[] arrListId = oPage.BannerIDs.Split(',');
                    List<int> idList =
                        (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                    var list = _masterPage.GetBannerImgByID(idList);

                    int countdata = list.Count();
                    int index = new Random().Next(countdata);
                    var newlist = list.Skip(index).FirstOrDefault();
                    if (newlist != null && newlist.ImgUrl != null)
                        imgInnerBanner.ImageUrl = adminSiteUrl + "" + newlist.ImgUrl;
                    else
                        imgInnerBanner.ImageUrl = "images/img_inner-banner.jpg";
                }
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }
}