﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Business;
using OneHubServiceRef;
using System.Web.UI.HtmlControls;

public partial class RegionalTrainsUc : System.Web.UI.UserControl
{
    #region Global Variable
    public string pricePoup = "";
    public string currency;
    public Guid currencyID = new Guid();
    Guid siteId;
    readonly Masters _objMaster = new Masters();
    ManageBooking _masterBooking = new ManageBooking();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    private readonly ManageTrainDetails _master = new ManageTrainDetails();

    List<P2PReservationIDInfo> lstP2PIdInfo = new List<P2PReservationIDInfo>();
    #endregion

    int count = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnsiteURL.Value = new ManageFrontWebsitePage().GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            hdnInbountOrOutBound.Value = Session["InbountOrOutBound"] != null ? Session["InbountOrOutBound"].ToString() : "";
            GetCurrencyCode();
            FillDetials();
        }
    }

    public void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
            currency = oManageClass.GetCurrency(currencyID);
            hdnCurrID.Value = currency;
        }

        ddlCountry.DataSource = _master.GetCountryDetail();
        ddlCountry.DataValueField = "CountryID";
        ddlCountry.DataTextField = "CountryName";
        ddlCountry.DataBind();

    }

    void FillDetials()
    {
        if (Session["PriceInfoResponse"] == null)
            return;
        RegionalTrainPrice listTrainInfo = Session["PriceInfoResponse"] as RegionalTrainPrice;
        rptTrainResult.DataSource = listTrainInfo != null ? listTrainInfo.TrainPriceInfoList.Where(x => !string.IsNullOrEmpty(x.TrainNumber)).ToList() : null;
        rptTrainResult.DataBind();
    }

    protected void rptTrainResult_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            RegionalTrainPrice listTrainInfo = Session["PriceInfoResponse"] as RegionalTrainPrice;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hdnTrainNo = e.Item.FindControl("hdnTrainNo") as HiddenField;
                HiddenField hdnSelectedInfo = e.Item.FindControl("hdnSelectedInfo") as HiddenField;
                if (listTrainInfo.TrainPriceInfoList == null)
                    return;

                TrainPriceInfo[] priceInfo = listTrainInfo.TrainPriceInfoList.Where(x => !string.IsNullOrEmpty(x.TrainNumber)).ToArray();
                List<TrainPrice> list = priceInfo.Where(x => x.TrainNumber == hdnTrainNo.Value).SelectMany(x => x.TrainPriceList).ToList();

                HtmlGenericControl divTrn = e.Item.FindControl("DivTr") as HtmlGenericControl;
                DropDownList ddlPrice = e.Item.FindControl("ddlFromPrice") as DropDownList;

                string classDiv = "";
                if (list.Count() > 0 && list.Count > 0)
                {
                    string trainNo = hdnTrainNo.Value;
                    List<string> offer = list.Select(x => x.TravelType).Distinct().ToList();
                    List<string> serviceName = list.OrderByDescending(x => Convert.ToDouble(x.MaxPrice)).Select(x => x.ServiceName).Distinct().ToList();

                    foreach (var sItem in serviceName)
                    {
                        classDiv += "<div class='starail-JourneyBlock-class'><p>" + sItem + "</p>";
                        string priceDiv = "";
                        foreach (var item in offer)
                        {
                            Guid id = Guid.NewGuid();

                            var firstOrDefault = list.FirstOrDefault(x => x.TravelType == item && x.ServiceName == sItem);
                            if (firstOrDefault != null)
                            {
                                var srcCurId = FrontEndManagePass.GetCurrencyID("EUT");
                                if (BusinessOneHub.IsNumeric(firstOrDefault.MinPrice))
                                {
                                    string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(firstOrDefault.MinPrice), siteId, srcCurId, currencyID).ToString("F");

                                    string text = sItem + "  " + item + "  " + currency + GetRoundPrice(price);
                                    string value = listTrainInfo.journeysolutioncode.Replace(",", "-") + "," + trainNo + "," + listTrainInfo.TripType + "," +
                                         firstOrDefault.ServiceCode + "," + firstOrDefault.ServiceTypeCode + "," +
                                         firstOrDefault.Offer.OfferCode + "," +
                                         firstOrDefault.Offer.OfferTypeCode + "," +
                                         firstOrDefault.Offer.OfferSubgroupCode + "," +
                                         firstOrDefault.Offer.Agreement.Trim() + "," +
                                         firstOrDefault.Class + "," + id;

                                    ddlPrice.Items.Add(new ListItem(text, value));
                                    priceDiv += "<div class='starail-JourneyBlock-radioRow'><label> <span class='starail-Form-fancyRadioGroup'>";
                                    priceDiv += "<input type='radio'  checked='true' id='rdo" + count + "' name='" + trainNo + "' value='" + value + "' />";
                                    priceDiv += item + "  " + currency + GetRoundPrice(price);
                                    priceDiv += "<span><i></i></span></span> &nbsp;</label><a href='#' class='js-lightboxOpen' data-lightbox-id='starail-ticket-info" + id + "'><i class='starail-Icon-question'></i></a> </div> ";

                                    pricePoup += "<div class='starail-Lightbox'id='starail-ticket-info" + id + "'><div class='starail-Lightbox-content'><div class='starail-Lightbox-closeContainer'><a href='#' class='starail-Lightbox-close js-lightboxClose'><i class='starail-Icon starail-Icon-close'></i></a>";
                                    pricePoup += "</div>  <div class='starail-Lightbox-content-inner'> <div class='starail-Lightbox-text'> <h3 class='starail-Lightbox-textTitle'>   Ticket Info</h3>";
                                    pricePoup += "<span  class='tresult'><h4>Class &amp; Seats  </h4>" + Server.HtmlDecode(firstOrDefault.FareDescription) + "</span>";
                                    pricePoup += "</div></div></div></div>";
                                    hdnSelectedInfo.Value = value;

                                }
                            }
                        }
                        classDiv += priceDiv;
                        classDiv += "</div>";
                    }
                }
                divTrn.InnerHtml = classDiv;
                count++;
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    string GetRoundPrice(string price)
    {
        //Please round up the prices to nearest above 0.50
        //If he price is 100.23, mark it as 100.50, 
        //If the price is 100.65 mark it as 102.00

        string[] strPrice = price.ToString().Split('.');

        if (strPrice[1] != null && Convert.ToDecimal(strPrice[1]) > 50)
            return (Convert.ToDecimal(strPrice[0]) + 1).ToString() + ".00";
        else if (strPrice[1] != null && Convert.ToDecimal(strPrice[1]) > 0 && Convert.ToDecimal(strPrice[1]) <= 50)
            return strPrice[0].Trim() + ".50";
        else
            return strPrice[0].Trim() + ".00";
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        try
        {
            Session["TIRegionalBooking"] = true;
            string AffiliateCode = string.Empty;
            var AgentID = Guid.Empty;
            var UserID = Guid.Empty;

            if (Session["AffCode"] != null)
                AffiliateCode = Session["AffCode"].ToString();
            if (AgentuserInfo.UserID != Guid.Empty && AgentuserInfo.IsAffliate)
                AffiliateCode = _masterBooking.GetAffiliateCode(AgentuserInfo.UserID);
            else if (AgentuserInfo.UserID != Guid.Empty && !AgentuserInfo.IsAffliate)
                AgentID = AgentuserInfo.UserID;
            else if (USERuserInfo.ID != Guid.Empty)
                UserID = USERuserInfo.ID;

            GetCurrencyCode();
            Session["P2POrderID"] = new ManageBooking().CreateOrder(AffiliateCode, AgentID, UserID, siteId, "P2P", "0", "TITR");
            Session["ProductType"] = "P2P";

            List<SelectedOffer> listOffer = GetTrainSegmentDetails();
            BookingRequest(listOffer);
            Session["P2PIdInfo"] = lstP2PIdInfo;
            Response.Redirect("~/P2PBookingCart.aspx?req=IT", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    List<SelectedOffer> GetTrainSegmentDetails()
    {
        List<SelectedOffer> listOffer = new List<SelectedOffer>();
        foreach (RepeaterItem itOffer in rptTrainResult.Items)
        {
            HiddenField hdnSelectedInfo = itOffer.FindControl("hdnSelectedInfo") as HiddenField;
            if (hdnSelectedInfo != null && !string.IsNullOrEmpty(hdnSelectedInfo.Value))
            {
                string[] strOffers = hdnSelectedInfo.Value.Trim().Split(',');
                SelectedOffer objSelectedOffer = new SelectedOffer
                {
                    TrainNumber = strOffers[1],
                    Service = new Service
                    {
                        ServiceCode = Convert.ToInt32(strOffers[3].Trim()),
                        ServiceTypeCode = Convert.ToInt32(strOffers[4].Trim())
                    },
                    Offers = new Offer
                    {
                        OfferCode = Convert.ToInt32(strOffers[5].Trim()),
                        OfferSubgroupCode = Convert.ToInt32(strOffers[7].Trim()),
                        OfferTypeCode = Convert.ToInt32(strOffers[6].Trim()),
                        Agreement = strOffers[8]
                    },
                    Class = strOffers[9],
                };
                listOffer.Add(objSelectedOffer);
            }
        }
        return listOffer;
    }

    void BookingRequest(List<SelectedOffer> listOffer)
    {
        Session["BOOKING-REQUEST"] = null;

        try
        {
            if (Session["PriceInfoResponse"] == null || Session["BookingUCRerq"] == null || Session["TrainSearch"] == null)
                return;

            RegionalTrainPrice listTrainInfo = Session["PriceInfoResponse"] as RegionalTrainPrice;
            TrainInformationResponse response = Session["TrainSearch"] as TrainInformationResponse;
            TrainInfoSegment[] trainSeglist = response.TrainInformationList.FirstOrDefault(x => x.JourneySolutionCode == listTrainInfo.journeysolutioncode.Replace(",", "-")).TrainInfoSegment;
            #region Add P2P bookin in DB
            BookingRequestUserControl objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            List<BookingRequest> listBookingRequest = new List<BookingRequest>();

            foreach (var trainInfo in listTrainInfo.TrainPriceInfoList)
            {
                var offerDB = listOffer.FirstOrDefault(x => x.TrainNumber == trainInfo.TrainNumber);
                TrainPrice price = trainInfo.TrainPriceList.FirstOrDefault(x => x.Offer.OfferCode == offerDB.Offers.OfferCode && x.Offer.OfferSubgroupCode == offerDB.Offers.OfferSubgroupCode && x.Offer.OfferTypeCode == offerDB.Offers.OfferTypeCode && x.Class == offerDB.Class);
                if (price != null)
                    AddP2PBooking(trainInfo, objBRUC, price);
            }
            #endregion

            string[] strTrain = new string[] { "RE", "RV" };
            bool IsRegional = trainSeglist.Any(x => strTrain.Contains(x.TrainDescr.Trim()));
            Session["IsRegional"] = IsRegional;

            TrainInfoSegment[] nonRegionalTrainSegment = trainSeglist.Where(x => !string.IsNullOrEmpty(x.TrainNumber)).Select(x => x).ToArray();
            if (nonRegionalTrainSegment.Count() == 0)
                nonRegionalTrainSegment = trainSeglist;

            #region Booking Request
            Owner[] owners = new Owner[1];
            owners[0] = new Owner
            {
                Lastname = txtLastname.Text,
                FirstName = txtFirstname.Text,
                DocumentNumber = "",
                DocumentType = DocumentType.None,
                OwnerType = objBRUC.Adults > 0 || objBRUC.Seniors > 0 || objBRUC.Youths > 0 ? OwnerType.Adult : OwnerType.Boy
            };

            List<PurchasingForServiceOwner> _PurchasingForServiceOwnerList = new List<PurchasingForServiceOwner>();
            if (nonRegionalTrainSegment != null && nonRegionalTrainSegment.Length > 0)
            {
                foreach (var item in nonRegionalTrainSegment.ToList())
                {
                    _PurchasingForServiceOwnerList.Add(new PurchasingForServiceOwner
                    {
                        NumAdults = objBRUC.Adults,
                        NumBoys = objBRUC.Boys,
                        NumSeniors = objBRUC.Seniors,
                        NumYouths = objBRUC.Youths,
                        Owners = owners,
                        SeparatedTicketsInUse = true,
                        ServicePreferences = listOffer != null && listOffer.Count > 0 ? new ServicePreference[]
                            {
                               new ServicePreference
                               {
                                    Bed = true,
                                    Service = listOffer.FirstOrDefault(y => y.TrainNumber == item.TrainNumber) != null ? listOffer.FirstOrDefault(y => y.TrainNumber == item.TrainNumber).Service : null,
                                    TrainNumber = item.TrainNumber
                               }
                            } : null,
                        ServiceSetting = new ServiceSetting
                        {
                            transportadultpassengersnumber = IsRegional ? objBRUC.Adults + objBRUC.Youths + objBRUC.Seniors : 0,
                            transportboypassengersnumber = IsRegional ? objBRUC.Boys : 0,
                            traveltype = listTrainInfo.TripType
                        },
                        SolutionRate = new SolutionRate
                        {
                            JourneySolutionCode = listTrainInfo.journeysolutioncode.Replace("-", ","),
                            Offer = listOffer.FirstOrDefault(y => y.TrainNumber == item.TrainNumber).Offers,
                        }
                    });
                }
            }

            var request = new PurchasingForServiceOwnerRequest
            {
                Header = new Header
                {
                    onehubusername = "#@dots!squares",
                    onehubpassword = "#@dots!squares",
                    unitofwork = 0,
                    language = Language.nl_BE,
                },
                IssuanceMode = IsRegional ? IssuanceMode.D : IssuanceMode.S,
                PurchaseOption = false,
                PurchasingForServiceOwnerGroup = _PurchasingForServiceOwnerList.ToArray()
            };

            var bkrequest = new BookingRequest();
            var id = Guid.NewGuid();
            bkrequest.PurchasingForServiceOwnerRequest = request;
            bkrequest.DepartureStationName = listTrainInfo.TrainPriceInfoList.FirstOrDefault().DepartureStationName;
            bkrequest.Id = id;
            listBookingRequest.Add(bkrequest);
            if (Session["BOOKING-REQUEST"] != null)
            {
                var oldList = Session["BOOKING-REQUEST"] as List<BookingRequest>;
                if (oldList != null) listBookingRequest.AddRange(oldList);
            }
            Session["BOOKING-REQUEST"] = listBookingRequest;
            #endregion
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void AddP2PBooking(TrainPriceInfo trainInfo, BookingRequestUserControl objBRUC, TrainPrice prices)
    {
        try
        {
            var srcCurId = FrontEndManagePass.GetCurrencyID("EUT");
            Guid id = Guid.NewGuid();

            string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(prices.MaxPrice), siteId, srcCurId, currencyID).ToString("F");
            long POrderID = Convert.ToInt64(Session["P2POrderID"]);
            tblP2PSale objP2P = new tblP2PSale();
            objP2P.From = trainInfo.DepartureStationName;
            objP2P.DateTimeDepature = Convert.ToDateTime(trainInfo.DepartureDate);
            objP2P.To = trainInfo.ArrivalStationName;
            objP2P.DateTimeArrival = Convert.ToDateTime(trainInfo.ArrivalDate);
            objP2P.TrainNo = trainInfo.TrainNumber;
            objP2P.Passenger = objBRUC != null ? objBRUC.Adults + " Adults, " + objBRUC.Boys + " Child," + objBRUC.Youths + " Youth," + objBRUC.Seniors + " Senior" : "";
            objP2P.SeviceName = trainInfo.TrainPriceList.FirstOrDefault() != null ? trainInfo.TrainPriceList.FirstOrDefault().ServiceName : "";
            objP2P.DepartureTime = trainInfo.DepartureTime;
            objP2P.ArrivalTime = trainInfo.ArrivalTime;
            objP2P.NetPrice = Convert.ToDecimal(GetRoundPrice(price));
            objP2P.FareName = prices.TravelType;
            objP2P.Price = Convert.ToDecimal(GetRoundPrice(price));
            objP2P.Class = prices.Class;
            objP2P.CurrencyId = currencyID;
            objP2P.Terms = prices.FareDescription;

            objP2P.Adult = objBRUC.Adults.ToString();
            objP2P.Children = objBRUC.Boys.ToString();
            objP2P.Youth = objBRUC.Youths.ToString();
            objP2P.Senior = objBRUC.Seniors.ToString();

            objP2P.ApiPrice = BusinessOneHub.IsNumeric(prices.MaxPrice) ? Convert.ToDecimal(prices.MaxPrice) : 0;
            objP2P.ApiName = "ITALIA";
            //site currency//
            objP2P.Site_USD = _masterBooking.GetCurrencyMultiplier("SUSD", Guid.Empty, POrderID);
            objP2P.Site_SEU = _masterBooking.GetCurrencyMultiplier("SSEU", Guid.Empty, POrderID);
            objP2P.Site_SBD = _masterBooking.GetCurrencyMultiplier("SSBD", Guid.Empty, POrderID);
            objP2P.Site_GBP = _masterBooking.GetCurrencyMultiplier("SGBP", Guid.Empty, POrderID);
            objP2P.Site_EUR = _masterBooking.GetCurrencyMultiplier("SEUR", Guid.Empty, POrderID);
            objP2P.Site_INR = _masterBooking.GetCurrencyMultiplier("SINR", Guid.Empty, POrderID);
            objP2P.Site_SEK = _masterBooking.GetCurrencyMultiplier("SSEK", Guid.Empty, POrderID);
            objP2P.Site_NZD = _masterBooking.GetCurrencyMultiplier("SNZD", Guid.Empty, POrderID);
            objP2P.Site_CAD = _masterBooking.GetCurrencyMultiplier("SCAD", Guid.Empty, POrderID);
            objP2P.Site_JPY = _masterBooking.GetCurrencyMultiplier("SJPY", Guid.Empty, POrderID);
            objP2P.Site_AUD = _masterBooking.GetCurrencyMultiplier("SAUD", Guid.Empty, POrderID);
            objP2P.Site_CHF = _masterBooking.GetCurrencyMultiplier("SCHF", Guid.Empty, POrderID);
            objP2P.Site_EUB = _masterBooking.GetCurrencyMultiplier("SEUB", Guid.Empty, POrderID);
            objP2P.Site_EUT = _masterBooking.GetCurrencyMultiplier("SEUT", Guid.Empty, POrderID);
            objP2P.Site_GBB = _masterBooking.GetCurrencyMultiplier("SGBB", Guid.Empty, POrderID);
            objP2P.Site_THB = _masterBooking.GetCurrencyMultiplier("STHB", Guid.Empty, POrderID);
            objP2P.Site_SGD = _masterBooking.GetCurrencyMultiplier("SSGD", Guid.Empty, POrderID);

            objP2P.EUR_EUT = _masterBooking.GetCurrencyMultiplier("E_EUT", Guid.Empty, POrderID);
            objP2P.EUR_EUB = _masterBooking.GetCurrencyMultiplier("E_EUB", Guid.Empty, POrderID);

            Guid P2PID;
            Guid LookupID = new ManageBooking().AddP2PBookingOrder(objP2P, Convert.ToInt64(Session["P2POrderID"]), out P2PID);
            tblP2PSale objP2PRow = new ManageBooking().getP2PSingleRowData(P2PID);

            P2PReservationIDInfo objP2PIdInfo = new P2PReservationIDInfo();
            objP2PIdInfo.ID = P2PID;
            objP2PIdInfo.P2PID = objP2PRow.P2PId;
            objP2PIdInfo.JourneyType = "";
            lstP2PIdInfo.Add(objP2PIdInfo);

            Session["P2PID"] = P2PID;
            tblOrderTraveller objTraveller = new tblOrderTraveller();
            objTraveller.ID = Guid.NewGuid();
            objTraveller.Title = ddlTitle.SelectedItem.Text;
            objTraveller.FirstName = txtFirstname.Text;
            objTraveller.LastName = txtLastname.Text;
            objTraveller.Country = (ddlCountry.SelectedValue != "0" ? Guid.Parse(ddlCountry.SelectedValue) : Guid.Empty);
            Session["objTravellerID"] = objTraveller.ID;

            new ManageBooking().AddTraveller(objTraveller, LookupID);
        }
        catch (Exception ec)
        {
            throw ec;
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                hdnDisplaySendJourney.Value = "true";
                //btnNext.Visible = false;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}

public class SelectedOffer
{
    public string TrainNumber { get; set; }
    public Offer Offers { get; set; }
    public Service Service { get; set; }
    public string Class { get; set; }
}
