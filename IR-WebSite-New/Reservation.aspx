﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="Reservation.aspx.cs" Inherits="Reservation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .berth-Form-error
        {
            background-color: #ffedee !important;
            border-color: #e6462e !important;
        }
        .grddatabind
        {
            padding: 0px !important;
            width: 100%;
        }
        .grddatabind tr td
        {
            line-height: 0px !important;
        }
        .grddatabind tr
        {
            line-height: 0px !important;
            padding-left: 5px;
        }
        .additional label
        {
            line-height: 30px !important;
            padding-left: 5px;
        }
        .grddataparent
        {
            padding: 5px 5px 10px 5px;
        }
        .grddataparent .starail-Form-row
        {
            border-bottom: solid 1px #CCCCC8;
            padding-bottom: 8px;
            margin-bottom: 20px;
            margin-top: 30px;
        }
        .journey-header
        {
            background: #fff;
            padding: 17px 5px 1px 5px;
            width: 100%;
        }
        .grddatabind td
        {
            padding-right: 10px;
        }
        .grddatabind td.first, .grddatabind td.second
        {
            padding-top: 12px;
        }
        .berth-text
        {
            width: 100%;
            display: -webkit-inline-box;
        }
        
        
        .berth-text .berth-first
        {
            width: 19%;
            margin-left: 1%;
            margin-right: 1%;
        }
        .berth-text .berth-second
        {
            width: 19%;
            margin-left: 1%;
            margin-right: 1%;
        }
        .berth-text .berth-third
        {
            width: 19%;
            margin-left: 1%;
            margin-right: 1%;
        }
        .berth-text .berth-fourth
        {
            width: 13%;
            margin-left: 1%;
            margin-right: 1%;
        }
        .berth-text .berth-fifth
        {
            width: 13%;
            margin-left: 1%;
            margin-right: 1%;
        }
        .berth-text .berth-sixth
        {
            width: 10%;
            margin-left: 1%;
            margin-right: 1%;
        }
        
        
        .berth-value
        {
            position: relative;
            width: 100%;
            display: -webkit-inline-box;
            margin-top: 1%;
        }
        .berth-value .berth-last-first
        {
            width: 19%;
            margin-left: 1%;
            margin-right: 1%;
        }
        .berth-value .berth-last-second
        {
            width: 19%;
            margin-left: 1%;
            margin-right: 1%;
        }
        .berth-value .berth-last-third
        {
            width: 19%;
            margin-left: 1%;
            margin-right: 1%;
        }
        .berth-value .berth-last-fourth
        {
            width: 13%;
            margin-left: 1%;
            margin-right: 1%;
        }
        .berth-value .berth-last-fifth
        {
            width: 13%;
            margin-left: 1%;
            margin-right: 1%;
        }
        .berth-value .berth-last-sixth
        {
            width: 10%;
            margin-left: 1%;
            margin-right: 1%;
        }
        
        .berth-text .berth-first-name
        {
            width: 23%;
            margin-left: 1%;
            margin-right: 1%;
        }
        .berth-value .berth-last-name
        {
            width: 23%;
            margin-left: 1%;
            margin-right: 1%;
        }
        .berth-value select, input[type="text"]
        {
            text-align: left;
            transition: border 300ms ease-in-out;
            border-radius: 1px;
            height: 35px;
            font-size: 0.9rem;
            border: 1px solid #d5d7d8 !important;
            color: #222;
            background-color: #FFF;
            padding: .41429rem !important;
        }
        .berth-value .berth-last-name select, input[type="text"]
        {
            width: 100%;
        }
        .berth-traveller-details
        {
            width: 100% !important;
        }
        
        .newdivsection
        {
            width: 31% !important;
            display: inline-block;
        }
        
        .sh-block
        {
            display: block !important;
            position: relative;
        }
        
        .hd-block, .del-hd-block, .requiredvalue
        {
            display: none !important;
        }
        
        @media (max-width:639px)
        {
            .full-mobile.grddatabind tbody tr td
            {
                display: block;
            }
            .full-mobile.grddatabind tbody tr
            {
                display: block;
            }
            .full-mobile.grddatabind tbody
            {
                display: block;
            }
        
        }
        @media (max-width:479px)
        {
            .grddatabind td.first, .grddatabind td.second
            {
                width: 100%;
                padding-right: 0;
            }
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.additioninfo').attr('style', 'display:none;');
            callAllFunctions();
        });

        function callAllFunctions() {
            $("#rptOutBerthErrorMessage").text("");
            $("#rptInBerthErrorMessage").text("");
            selectRadio();
            showHideAdditionalInfo();
            changeEvent();
            manageSoloBerth('div_Out_Berth');
            manageSoloBerth('div_In_Berth');
            OtherSettings();
        }

        function OtherSettings() {
            if ($('[id*=rptOutBound_hdnIsSleeper]').val() == "True" || $('[id*=rptInBound_hdnIsSleeper]').val() == "True") {
                $('[id*=div_Berth_Traveller]').show();
                EnableTravellerValidation();
            }
            else {
                $('[id*=div_Berth_Traveller]').hide();
                DisableTravellerValidation();
            }

            if ($('.newtable').find($('[id*=rdnSeat]')).is(':checked') || $('.newtable').find($('[id*=rdnSpecificSeat]')).is(':checked')) {
                $('[id*=div_AdditionalPreference]').show();
            }
            else {
                $('[id*=div_AdditionalPreference]').hide();
            }
        }

        function changeEvent() {
            $('[id*=rptOutBound_div_Reservation1]').find('input[type="radio"]').change(function () {
                selectRadio();
                showHideAdditionalInfo();
                OtherSettings();
            });
            $('[id*=rptInBound_div_Reservation1]').find('input[type="radio"]').change(function () {
                selectRadio();
                showHideAdditionalInfo();
                OtherSettings();
            });
        }

        function showHideAdditionalInfo() {
            $('.newtable').each(function () {
                if ($(this).find('[id*=rdnSeat]').is(':checked') || $(this).find('[id*=rdnSpecificSeat]').is(':checked')) {
                    $('.additioninfo').attr('style', 'display:inline-block;');
                    return false;
                }
                else
                    $('.additioninfo').attr('style', 'display:none;');
            });
        }

        function selectRadio() {
            var berthPrice = 0;
            var hasoutboundberth = false;
            var hasinboundberth = false;
            $('[id*=rptOutBound_div_Reservation1]').each(function () {
                if ($(this).find($('[id*=rdnNoSeat]')).is(':checked')) {
                    $(this).find('.first').attr("style", "display:none;");
                    $(this).find('.second').attr("style", "display:none;");
                    $(this).find('[id*=reqvalerror0]').removeClass("requiredvalue").addClass("del-hd-block");
                    $(this).find('[id*=reqvalerror1]').removeClass("requiredvalue").addClass("del-hd-block");
                    $(this).find('[id*=reqvalerror2]').removeClass("requiredvalue").addClass("del-hd-block");
                    if (!hasoutboundberth)
                        $('[id*=div_Out_Berth]').hide();
                }
                else if ($(this).find($('[id*=rdnSeat]')).is(':checked')) {
                    $(this).find('.first').attr("style", "display:inline-block;");
                    $(this).find('.second').attr("style", "display:none;");
                    $(this).find('[id*=reqvalerror0]').removeClass("requiredvalue").addClass("del-hd-block");
                    $(this).find('[id*=reqvalerror1]').removeClass("requiredvalue").addClass("del-hd-block");
                    $(this).find('[id*=reqvalerror2]').removeClass("requiredvalue").addClass("del-hd-block");
                    if (!hasoutboundberth)
                        $('[id*=div_Out_Berth]').hide();
                }
                else if ($(this).find($('[id*=rdnSpecificSeat]')).is(':checked')) {
                    $(this).find('.first').attr("style", "display:none;");
                    $(this).find('.second').attr("style", "display:inline-block;");
                    $(this).find('[id*=reqvalerror0]').removeClass("del-hd-block").addClass("requiredvalue");
                    $(this).find('[id*=reqvalerror1]').removeClass("del-hd-block").addClass("requiredvalue");
                    $(this).find('[id*=reqvalerror2]').removeClass("del-hd-block").addClass("requiredvalue");
                    if (!hasoutboundberth)
                        $('[id*=div_Out_Berth]').hide();
                }
                else if ($(this).find($('[id*=rdnBerth]')).is(':checked') || $(this).find($('[id*=rdnBerthExtra]')).is(':checked')) {
                    $(this).find('.first').attr("style", "display:none;");
                    $(this).find('.second').attr("style", "display:none;");
                    $(this).find('[id*=reqvalerror0]').removeClass("requiredvalue").addClass("del-hd-block");
                    $(this).find('[id*=reqvalerror1]').removeClass("requiredvalue").addClass("del-hd-block");
                    $(this).find('[id*=reqvalerror2]').removeClass("requiredvalue").addClass("del-hd-block");
                    $('[id*=div_Out_Berth]').find('.berth-value').each(function (berthKey, berthValue) {
                        berthPrice = (parseFloat(berthPrice) + parseFloat($(berthValue).find('[id*=hdnBerthCost]').val())).toFixed(2);
                    });
                    $('[id*=div_Out_Berth]').show();
                    hasoutboundberth = true;
                }
            });

            $('[id*=rptInBound_div_Reservation1]').each(function () {
                if ($(this).find($('[id*=rdnNoSeat]')).is(':checked')) {
                    $(this).find('.first').attr("style", "display:none;");
                    $(this).find('.second').attr("style", "display:none;");
                    $(this).find('[id*=reqvalerror0]').removeClass("requiredvalue").addClass("del-hd-block");
                    $(this).find('[id*=reqvalerror1]').removeClass("requiredvalue").addClass("del-hd-block");
                    $(this).find('[id*=reqvalerror2]').removeClass("requiredvalue").addClass("del-hd-block");
                    if (!hasinboundberth)
                        $('[id*=div_In_Berth]').hide();
                }
                else if ($(this).find($('[id*=rdnSeat]')).is(':checked')) {
                    $(this).find('.first').attr("style", "display:inline-block;");
                    $(this).find('.second').attr("style", "display:none;");
                    $(this).find('[id*=reqvalerror0]').removeClass("requiredvalue").addClass("del-hd-block");
                    $(this).find('[id*=reqvalerror1]').removeClass("requiredvalue").addClass("del-hd-block");
                    $(this).find('[id*=reqvalerror2]').removeClass("requiredvalue").addClass("del-hd-block");
                    if (!hasinboundberth)
                        $('[id*=div_In_Berth]').hide();
                }
                else if ($(this).find($('[id*=rdnSpecificSeat]')).is(':checked')) {
                    $(this).find('.first').attr("style", "display:none;");
                    $(this).find('.second').attr("style", "display:inline-block;");
                    $(this).find('[id*=reqvalerror0]').removeClass("del-hd-block").addClass("requiredvalue");
                    $(this).find('[id*=reqvalerror1]').removeClass("del-hd-block").addClass("requiredvalue");
                    $(this).find('[id*=reqvalerror2]').removeClass("del-hd-block").addClass("requiredvalue");
                    if (!hasinboundberth)
                        $('[id*=div_In_Berth]').hide();
                }
                else if ($(this).find($('[id*=rdnBerth]')).is(':checked') || $(this).find($('[id*=rdnBerthExtra]')).is(':checked')) {
                    $(this).find('.first').attr("style", "display:none;");
                    $(this).find('.second').attr("style", "display:none;");
                    $(this).find('[id*=reqvalerror0]').removeClass("requiredvalue").addClass("del-hd-block");
                    $(this).find('[id*=reqvalerror1]').removeClass("requiredvalue").addClass("del-hd-block");
                    $(this).find('[id*=reqvalerror2]').removeClass("requiredvalue").addClass("del-hd-block");
                    $('[id*=div_In_Berth]').find('.berth-value').each(function (berthKey, berthValue) {
                        berthPrice = (parseFloat(berthPrice) + parseFloat($(berthValue).find('[id*=hdnBerthCost]').val())).toFixed(2);
                    });
                    $('[id*=div_In_Berth]').show();
                    hasinboundberth = true;
                }
            });
            $('#MainContent_lblTotal').text('<%=currency %>' + ' ' + (parseFloat($('#MainContent_hdnTotal').val()) + parseFloat(berthPrice)).toFixed(2));
        }

        function EnableTravellerValidation() {
            $('[id*=reqvalerror5]').removeClass("del-hd-block").addClass("requiredvalue");
            $('[id*=reqvalerror6]').removeClass("del-hd-block").addClass("requiredvalue");
            $('[id*=reqvalerror7]').removeClass("del-hd-block").addClass("requiredvalue");
            $('[id*=reqvalerror8]').removeClass("del-hd-block").addClass("requiredvalue");
        }

        function DisableTravellerValidation() {
            $('[id*=reqvalerror5]').removeClass("requiredvalue").addClass("del-hd-block");
            $('[id*=reqvalerror6]').removeClass("requiredvalue").addClass("del-hd-block");
            $('[id*=reqvalerror7]').removeClass("requiredvalue").addClass("del-hd-block");
            $('[id*=reqvalerror8]').removeClass("requiredvalue").addClass("del-hd-block");
        }

        function validValidation() {
            var result = true;
            $("#MainContent_updReservation").find(".requiredvalue").each(function (key, value) {
                if ($(value).prev()[0].tagName == "SELECT" && $(value).prev().val() == "0") {
                    $(value).prev().addClass("berth-Form-error");
                    result = false;
                }
                else if ($(value).prev()[0].tagName == "INPUT" && $(value).prev().val() == "") {
                    $(value).prev().addClass("berth-Form-error");
                    result = false;
                }
                else {
                    $(value).prev().removeClass("berth-Form-error");
                }
            });

            if ($("#MainContent_div_Out_Berth").attr("style").match(/block/i)) {
                if (!($("#rptOutBerthErrorMessage").text().replace(/ /g, '') == "") && !($("#rptOutBerthErrorMessage").text() == "Traveller valid")) {
                    result = false;
                    console.log("out bound");
                }
            }
            if ($("#MainContent_div_In_Berth").attr("style").match(/block/i)) {
                if (!($("#rptInBerthErrorMessage").text().replace(/ /g, '') == "") && !($("#rptInBerthErrorMessage").text() == "Traveller valid")) {
                    result = false;
                    console.log("in bound");
                }
            }
            return result;
        }
    </script>
    <script type="text/javascript">
        function addoutboundrow(obj) {
            var div = $(obj).parent().parent().find(".hd-block:first");
            var olda = $(div).addClass("sh-block").removeClass("hd-block");
            $(div).remove();
            $(obj).parent().before(olda);
            $(obj).parent().parent().find("[id*=hdnisDelete]").val('no');
            if ($(obj).parent().parent().find(".hd-block:first").attr("class") == undefined) {
                /*console.log($(obj).parent().parent().attr('id'));*/
                if ($(obj).parent().parent().attr('id') == "MainContent_div_Out_Berth")
                    $("[id*=addoutbound]").hide();
                else
                    $("[id*=addinbound]").hide();
            }

            if ($(obj).parent().parent().attr('id') == "MainContent_div_Out_Berth")
                $("#rptOutBerthErrorMessage").css("color", "red").text("Traveller not valid");
            else if ($("#MainContent_div_In_Berth").attr("style").match(/block/i))
                $("#rptInBerthErrorMessage").css("color", "red").text("Traveller not valid");
        }
        function removethisrow(obj) {
            $(obj).parent().removeClass("sh-block").addClass("hd-block");
            $(obj).parent().find("[id*=hdnisDelete]").val('yes');
            if ($(obj).parent().parent().attr('id') == "MainContent_div_Out_Berth")
                $("[id*=addoutbound]").show();
            else
                $("[id*=addinbound]").show();

            var ddlNoOfBerth = $(obj).parent().find('[id*=ddlNoOfBerth]');
            var ddlBerth1 = $(obj).parent().find('[id*=ddlBerth1]');
            var ddlBerth2 = $(obj).parent().find('[id*=ddlBerth2]');
            var ddlCabinLocation = $(obj).parent().find('[id*=ddlCabinLocation]');
            var ddlPreferences = $(obj).parent().find('[id*=ddlPreferences]');
            var lblBerthCost = $(obj).parent().find('[id*=lblBerthCost]');
            var hdnBerthCost = $(obj).parent().find('[id*=hdnBerthCost]');

            $(ddlNoOfBerth).val('0');
            $(ddlBerth1).val('0');
            $(ddlBerth2).val('0');
            $(ddlCabinLocation).val('0');
            $(ddlPreferences).val('0');
            $(lblBerthCost).text("0.00");
            $(hdnBerthCost).val("0.00");
            Berthselectionchange($(obj).parent().parent().attr("id").replace("MainContent_", ""));
        }
        function bearthresetoption(obj) {
            $(obj).empty();
        }
        function bearthnoofberth(obj, condition) {
            if (condition == 1) {
                $(obj).append($('<option>', { value: 'One berth in two berth cabin', text: 'One berth in two berth cabin' }, '</option>'));
            }
            else if (condition == 2) {
                $(obj).append($('<option>', { value: 'Solo occupancy cabin', text: 'Solo occupancy cabin' }, '</option>'));
            }
            else if (condition == 11) {
                $(obj).append($('<option>', { value: '0', text: 'Select ...' }, '</option>'));
                $(obj).append($('<option>', { value: 'One berth in two berth cabin', text: 'One berth in two berth cabin' }, '</option>'));
                $(obj).append($('<option>', { value: 'Two berths in two berth cabin', text: 'Two berths in two berth cabin' }, '</option>'));
            }
            else if (condition == 12) {
                $(obj).append($('<option>', { value: '0', text: 'Select ...' }, '</option>'));
                $(obj).append($('<option>', { value: 'Solo occupancy cabin', text: 'Solo occupancy cabin' }, '</option>'));
                $(obj).append($('<option>', { value: 'Two berths in two berth cabin', text: 'Two berths in two berth cabin' }, '</option>'));
            }
        }
        function bearth1combin(obj, condition) {
            $(obj).append($('<option>', { value: '0', text: 'Select ...' }, '</option>'));
            if (condition == 1) {
                $(obj).append($('<option>', { value: 'Male Adult', text: 'Male Adult' }, '</option>'));
                $(obj).append($('<option>', { value: 'Female Adult', text: 'Female Adult' }, '</option>'));
            }
            else if (condition == 2) {
                $(obj).append($('<option>', { value: 'Adult', text: 'Adult' }, '</option>'));
                $(obj).append($('<option>', { value: 'Child', text: 'Child' }, '</option>'));
            }
        }
        function bearth2combin(obj) {
            $(obj).append($('<option>', { value: '0', text: 'Select ...' }, '</option>'));
            $(obj).append($('<option>', { value: 'Adult', text: 'Adult' }, '</option>'));
            $(obj).append($('<option>', { value: 'Adult and child sharing berth', text: 'Adult and child sharing berth' }, '</option>'));
            $(obj).append($('<option>', { value: 'Child', text: 'Child' }, '</option>'));
            $(obj).append($('<option>', { value: 'Two children sharing berth', text: 'Two children sharing berth' }, '</option>'));
        }
        function bearthlocations(obj) {
            $(obj).append($('<option>', { value: '0', text: 'No Preference' }, '</option>'));
            $(obj).append($('<option>', { value: 'Centre of Carriage', text: 'Centre of Carriage' }, '</option>'));
            $(obj).append($('<option>', { value: 'End of Carriage', text: 'End of Carriage' }, '</option>'));
        }
        function preference(obj, condition) {
            $(obj).append($('<option>', { value: '0', text: 'No Preference' }, '</option>'));
            if (condition == 1) {
                $(obj).append($('<option>', { value: 'Lower Berth', text: 'Lower Berth' }, '</option>'));
                $(obj).append($('<option>', { value: 'Upper Berth', text: 'Upper Berth' }, '</option>'));
            }
            else if (condition == 2)
                $(obj).append($('<option>', { value: 'Connecting Door', text: 'Connecting Door' }, '</option>'));
        }
        function hideshowAddButton() {
            $('#MainContent_div_Out_Berth').find('.berth-value').each(function (key, value) {
                if ($(this).hasClass('hd-block'))
                    $("[id*=addoutbound]").show();
            });
            $('#MainContent_div_In_Berth').find('.berth-value').each(function (key, value) {
                if ($(this).hasClass('hd-block'))
                    $("[id*=addinbound]").show();
            });
        }
        function manageSoloBerth(berthType) {
            var isSolo = $('[id*="' + berthType + '"]').find('.berth-value').find('[id*=hdnisSolo]').val() == "True";
            var isMF = $('[id*="' + berthType + '"]').find('.berth-value').find('[id*=hdnisMaleFemale]').val() == "True";
            var isTogr = $('[id*="' + berthType + '"]').find('.berth-value').find('[id*=hdnisTogr]').val() == "True";
            var ATotalAdult = parseInt($('[id*="' + berthType + '"]').find('.berth-value:first').find('[id*=hdnNoOfAdult]').val());
            var ATotalChild = parseInt($('[id*="' + berthType + '"]').find('.berth-value:first').find('[id*=hdnNoOfChild]').val());
            if (!$.isNumeric(ATotalAdult))
                ATotalAdult = 0;
            if (!$.isNumeric(ATotalChild))
                ATotalChild = 0;

            /*console.log("berthType= " + berthType + ",  ATotalAdult= " + ATotalAdult + ", ATotalChild= " + ATotalChild);*/

            if (!isTogr && (isSolo || isMF)) {
                $("[id*=addoutbound]").hide();
                $("[id*=addinbound]").hide();

                $('[id*="' + berthType + '"]').find('.berth-value').each(function (key, value) {
                    var ddlNoOfBerth = $(value).find('[id*=ddlNoOfBerth]');
                    var ddlBerth1 = $(value).find('[id*=ddlBerth1]');
                    var ddlBerth2 = $(value).find('[id*=ddlBerth2]');
                    var ddlCabinLocation = $(value).find('[id*=ddlCabinLocation]');
                    var ddlPreferences = $(value).find('[id*=ddlPreferences]');

                    bearthresetoption(ddlNoOfBerth);
                    bearthresetoption(ddlBerth1);
                    bearthresetoption(ddlBerth2);
                    bearthresetoption(ddlCabinLocation);
                    bearthresetoption(ddlPreferences);
                    bearthlocations(ddlCabinLocation);
                    if (isSolo) {
                        bearthnoofberth(ddlNoOfBerth, 2);
                        bearth1combin(ddlBerth1, 2);
                        preference(ddlPreferences, 2);
                        if (ATotalAdult > 0) {
                            $(ddlBerth1).val("Adult");
                            ATotalAdult -= 1;
                        }
                        else if (ATotalChild > 0) {
                            $(ddlBerth1).val("Child");
                            ATotalChild -= 1;
                        }
                    }
                    else {
                        bearthnoofberth(ddlNoOfBerth, 1);
                        bearth1combin(ddlBerth1, 1);
                        preference(ddlPreferences, 1);
                    }
                    $(ddlBerth2).prepend($('<option>', { value: '0', text: 'Not Available' }, '</option>'));
                    ddlBerth2.attr('disabled', 'disabled');
                });
                if ($('[id*="' + berthType + '"]').find('.berth-value:first').find('[id*=hdnNoOfChild]').val() != undefined)
                    Berthselectionchange(berthType);
            }
            else {
                $('[id*="' + berthType + '"]').find('.berth-value').each(function (key, value) {
                    var ddlNoOfBerth = $(value).find('[id*=ddlNoOfBerth]');
                    var ddlBerth1 = $(value).find('[id*=ddlBerth1]');
                    var ddlBerth2 = $(value).find('[id*=ddlBerth2]');
                    var ddlCabinLocation = $(value).find('[id*=ddlCabinLocation]');
                    var ddlPreferences = $(value).find('[id*=ddlPreferences]');
                    bearthresetoption(ddlNoOfBerth);
                    bearthresetoption(ddlBerth1);
                    bearthresetoption(ddlBerth2);
                    bearthresetoption(ddlCabinLocation);
                    bearthresetoption(ddlPreferences);
                    bearthlocations(ddlCabinLocation);
                    if (isSolo) {
                        bearthnoofberth(ddlNoOfBerth, 12);
                        bearth2combin(ddlBerth1);
                        bearth2combin(ddlBerth2);
                        preference(ddlPreferences, 2);
                    }
                    else {
                        bearthnoofberth(ddlNoOfBerth, 11);
                        bearth2combin(ddlBerth1);
                        bearth2combin(ddlBerth2);
                        preference(ddlPreferences, 2);
                    }

                    if (ATotalAdult == 0 && ATotalChild == 0) {
                        $(this).removeClass("sh-block").addClass("hd-block");
                        $(value).find("[id*=hdnisDelete]").val('yes');
                        $(ddlBerth1).prepend($('<option>', { value: '0', text: 'Not Available' }, '</option>'));
                        $(ddlBerth2).prepend($('<option>', { value: '0', text: 'Not Available' }, '</option>'));
                        $(ddlCabinLocation).prepend($('<option>', { value: '0', text: 'No Preference' }, '</option>'));
                        $(ddlPreferences).prepend($('<option>', { value: '0', text: 'No Preference' }, '</option>'));
                        $(ddlNoOfBerth).val('0');
                    }
                    else if (ATotalChild == ATotalAdult && ATotalChild != 0) {
                        $(ddlNoOfBerth).val("Two berths in two berth cabin");
                        if (ATotalChild == 1) {
                            $(ddlBerth1).val("Child");
                            $(ddlBerth2).val("Adult");
                            ATotalChild -= 1;
                            ATotalAdult -= 1;
                        }
                        else {
                            $(ddlBerth1).val("Adult and child sharing berth");
                            $(ddlBerth2).val("Adult and child sharing berth");
                            ATotalChild -= 2;
                            ATotalAdult -= 2;
                        }
                    }
                    else if (ATotalChild > ATotalAdult && ATotalAdult != 0) {
                        $(ddlNoOfBerth).val("Two berths in two berth cabin");
                        if (ATotalChild == 2) {
                            $(ddlBerth1).val("Adult and child sharing berth");
                            $(ddlBerth2).val("Child");
                            ATotalChild -= 2;
                            ATotalAdult -= 1;
                        }
                        if (ATotalChild == 3) {
                            $(ddlBerth1).val("Two children sharing berth");
                            $(ddlBerth2).val("Adult and child sharing berth");
                            ATotalChild -= 3;
                            ATotalAdult -= 1;
                        }
                        if (ATotalChild >= 4) {
                            $(ddlBerth1).val("Two children sharing berth");
                            $(ddlBerth2).val("Two children sharing berth");
                            ATotalChild -= 4;
                        }
                    }
                    else if (ATotalChild < ATotalAdult && ATotalChild != 0) {
                        $(ddlNoOfBerth).val("Two berths in two berth cabin");
                        if (ATotalChild == 1) {
                            $(ddlBerth1).val("Adult and child sharing berth");
                            $(ddlBerth2).val("Adult");
                            ATotalChild -= 1;
                            ATotalAdult -= 2;
                        }
                        else {
                            $(ddlBerth1).val("Adult and child sharing berth");
                            $(ddlBerth2).val("Adult and child sharing berth");
                            ATotalChild -= 2;
                            ATotalAdult -= 2;
                        }
                    }
                    else if (ATotalAdult > 0 && ATotalChild == 0) {
                        if (ATotalAdult == 1) {
                            bearthresetoption(ddlNoOfBerth);
                            bearthresetoption(ddlBerth1);
                            bearthresetoption(ddlBerth2);
                            bearthresetoption(ddlPreferences);
                            if (isSolo) {
                                bearthnoofberth(ddlNoOfBerth, 12); // 2
                                $(ddlNoOfBerth).val("Solo occupancy cabin"); //TODO
                                bearth1combin(ddlBerth1, 2);
                                preference(ddlPreferences, 2);
                                $(ddlBerth1).val("Adult");
                            }
                            else {
                                bearthnoofberth(ddlNoOfBerth, 11); //1
                                $(ddlNoOfBerth).val("One berth in two berth cabin"); //TODO
                                bearth1combin(ddlBerth1, 1);
                                preference(ddlPreferences, 1);
                            }
                            $(ddlBerth2).prepend($('<option>', { value: '0', text: 'Not Available' }, '</option>'));
                            ddlBerth2.attr('disabled', 'disabled');
                            ATotalAdult -= 1;
                        }
                        else {
                            $(ddlNoOfBerth).val("Two berths in two berth cabin");
                            $(ddlBerth1).val("Adult");
                            $(ddlBerth2).val("Adult");
                            ATotalAdult -= 2;
                        }
                    }
                });
                if ($('[id*="' + berthType + '"]').find('.berth-value:first').find('[id*=hdnNoOfChild]').val() != undefined)
                    Berthselectionchange(berthType);
            } hideshowAddButton();
        }

        var TotalAdult = 0;
        var TotalChild = 0;
        var TotalChildSelected = 0;
        var TotalAdultSelected = 0;
        function Berthselectionchange(obj) {

            TotalAdultSelected = 0;
            TotalChildSelected = 0;
            TotalAdult = $('[id*="' + obj + '"]').find('.berth-value').find('[id*=hdnNoOfAdult]').val();
            TotalChild = $('[id*="' + obj + '"]').find('.berth-value').find('[id*=hdnNoOfChild]').val();

            $('[id*="' + obj + '"]').find('.berth-value').each(function (key, value) {
                var labelprice = 0;

                var ddlNoOfBerth = $(value).find('[id*=ddlNoOfBerth]');
                var ddlBerth1 = $(value).find('[id*=ddlBerth1]');
                var ddlBerth2 = $(value).find('[id*=ddlBerth2]');
                var lblBerthCost = $(value).find('[id*=lblBerthCost]');
                var hdnBerthCost = $(value).find('[id*=hdnBerthCost]');
                var AdultPrice = parseFloat($(value).find('[id*=hdnAdultPrice]').val());
                var ChildPrice = parseFloat($(value).find('[id*=hdnChildPrice]').val());
                var AdultOrChildPrice = parseFloat($(value).find('[id*=hdnAdultOrChildSharingPrice]').val());
                var TwoChildPrice = parseFloat($(value).find('[id*=hdnTwoChildSharingPrice]').val());
                var SoloAdultOrChildPrice = parseFloat($(value).find('[id*=hdnsoloAdultOrChildPrice]').val());
                var OneBerthAdultMF = parseFloat($(value).find('[id*=hdnOneBerthAdultMF]').val());

                var bearth1 = $(ddlBerth1).val();
                var bearth2 = $(ddlBerth2).val();

                var numberofberth = $(ddlNoOfBerth).val();
                if (numberofberth == "Two berths in two berth cabin") {
                    for (var i = 1; i <= 2; i++) {
                        var selectbearth = "";
                        if (i == 1)
                            selectbearth = bearth1;
                        else
                            selectbearth = bearth2;

                        if (selectbearth == "Two children sharing berth") {
                            labelprice += TwoChildPrice;
                            TotalChildSelected += 2;
                        }
                        if (selectbearth == "Child") {
                            labelprice += ChildPrice;
                            TotalChildSelected += 1;
                        }
                        if (selectbearth == "Adult") {
                            labelprice += AdultPrice;
                            TotalAdultSelected += 1;
                        }
                        if (selectbearth == "Adult and child sharing berth") {
                            labelprice += AdultOrChildPrice;
                            TotalChildSelected += 1;
                            TotalAdultSelected += 1;
                        }
                        if (bearth2 == 0 || bearth1 == 0)
                            TotalChildSelected = TotalAdultSelected = 0;
                    }
                    lblBerthCost.text(labelprice.toFixed(2));
                    hdnBerthCost.val(labelprice.toFixed(2));
                }
                else if (numberofberth == "One berth in two berth cabin") {
                    if (bearth1 != '0') {
                        labelprice += OneBerthAdultMF;
                        lblBerthCost.text(labelprice.toFixed(2));
                        hdnBerthCost.val(labelprice.toFixed(2));
                        TotalAdultSelected += 1;
                    }
                    if (bearth1 == 0)
                        TotalChildSelected = TotalAdultSelected = 0;
                }
                else if (numberofberth == "Solo occupancy cabin") {
                    if (bearth1 != '0') {
                        if (bearth1 == "Adult") {
                            labelprice += SoloAdultOrChildPrice;
                            TotalAdultSelected += 1;
                        }
                        else if (bearth1 == "Child") {
                            labelprice += SoloAdultOrChildPrice;
                            TotalChildSelected += 1;
                        }
                        lblBerthCost.text(labelprice.toFixed(2));
                        hdnBerthCost.val(labelprice.toFixed(2));
                    }
                    if (bearth1 == 0)
                        TotalChildSelected = TotalAdultSelected = 0;
                }
                else if ($(this).attr("class").match(/sh-block/i)) {
                    TotalChildSelected = TotalAdultSelected = 0;
                }
            });

            /*console.log("total: A::[" + TotalAdultSelected + "] [" + TotalAdult + "]   ::    total: C::[" + TotalChildSelected + "] [" + TotalChild + "]  ");*/

            if (TotalChild == TotalChildSelected && TotalAdult == TotalAdultSelected) {
                if (obj.match(/div_Out_Berth/i)) {
                    $("#rptOutBerthErrorMessage").css("color", "green").text("Traveller valid");
                }
                else if (obj.match(/div_In_Berth/i)) {
                    $("#rptInBerthErrorMessage").css("color", "green").text("Traveller valid");
                }
            }
            else {
                if (obj.match(/div_Out_Berth/i)) {
                    $("#rptOutBerthErrorMessage").css("color", "red").text("Traveller not valid");
                }
                else if (obj.match(/div_In_Berth/i)) {
                    $("#rptInBerthErrorMessage").css("color", "red").text("Traveller not valid");
                }
            }
            selectRadio();

        }

        function changeberth(obj) {
            var value = $(obj).parent().parent();
            var isSolo = $(value).find('[id*=hdnisSolo]').val() == "True"
            var ddlNoOfBerth = $(value).find('[id*=ddlNoOfBerth]');
            var ddlBerth1 = $(value).find('[id*=ddlBerth1]');
            var ddlBerth2 = $(value).find('[id*=ddlBerth2]');
            var ddlCabinLocation = $(value).find('[id*=ddlCabinLocation]');
            var ddlPreferences = $(value).find('[id*=ddlPreferences]');
            var lblBerthCost = $(value).find('[id*=lblBerthCost]');
            var hdnBerthCost = $(value).find('[id*=hdnBerthCost]');
            var numberofberth = $(ddlNoOfBerth).val();
            if (numberofberth == 'Solo occupancy cabin') {
                bearthresetoption(ddlBerth1);
                bearthresetoption(ddlBerth2);
                bearthresetoption(ddlPreferences);
                bearthresetoption(ddlCabinLocation);
                bearth1combin(ddlBerth1, 2);
                preference(ddlPreferences, 2);
                bearthlocations(ddlCabinLocation);
                $(ddlBerth2).append($('<option>', { value: '0', text: 'No Preference' }, '</option>'));
                $(ddlBerth2).attr("disabled", "disabled");
            }
            else if (numberofberth == 'One berth in two berth cabin') {
                bearthresetoption(ddlBerth1);
                bearthresetoption(ddlBerth2);
                bearthresetoption(ddlPreferences);
                bearthresetoption(ddlCabinLocation);
                bearth1combin(ddlBerth1, 1);
                preference(ddlPreferences, 1);
                bearthlocations(ddlCabinLocation);
                $(ddlBerth2).append($('<option>', { value: '0', text: 'No Preference' }, '</option>'));
                $(ddlBerth2).attr("disabled", "disabled");
            }
            else if (numberofberth == "Two berths in two berth cabin") {
                $(ddlBerth2).removeAttr("disabled", "disabled");
                bearthresetoption(ddlBerth1);
                bearthresetoption(ddlBerth2);
                bearthresetoption(ddlPreferences);
                bearthresetoption(ddlCabinLocation);
                bearthlocations(ddlCabinLocation);
                if (isSolo) {
                    bearth2combin(ddlBerth1);
                    bearth2combin(ddlBerth2);
                    preference(ddlPreferences, 2);
                }
                else {
                    bearth2combin(ddlBerth1);
                    bearth2combin(ddlBerth2);
                    preference(ddlPreferences, 1);
                }
            }
            else {
                $(ddlNoOfBerth).val('0');
                $(ddlBerth1).val('0');
                $(ddlBerth2).val('0');
                $(ddlCabinLocation).val('0');
                $(ddlPreferences).val('0');
                $(lblBerthCost).text("0.00");
                $(hdnBerthCost).val("0.00");
            }
            Berthselectionchange($(obj).parent().parent().parent().attr("id").replace("MainContent_", ""));
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updReservation" runat="server">
        <ContentTemplate>
            <br />
            <%--OutBound Inbound Berth --%>
            <div class="starail-BookingDetails-form">
                <div class="starail-Form-row">
                    <asp:Panel ID="pnlErrSuccess" runat="server">
                        <div id="DivSuccess" runat="server" class="success" style="display: none;">
                            <asp:Label ID="lblSuccessMsg" runat="server" />
                        </div>
                        <div id="DivError" runat="server" class="error" style="display: none;">
                            <asp:Label ID="lblErrorMsg" runat="server" />
                        </div>
                    </asp:Panel>
                </div>
                <h2>
                    Reservation Preferences</h2>
                <p>
                    Reserved your seat for segment trains.</p>
                <asp:HiddenField ID="hdnCurrID" runat="server" Value="" />
                <asp:HiddenField ID="hdnNoOfPassenger" runat="server" Value="0" />
                <%--OutBound --%>
                <div class="grddataparent">
                    <asp:Repeater ID="rptOutBound" runat="server" OnItemDataBound="rptOutBound_ItemDataBound">
                        <HeaderTemplate>
                            <div class="journey-header">
                                <h3>
                                    Outbound</h3>
                            </div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="starail-Form-row">
                                <b>Leg
                                    <%# Eval("Index")%></b> -
                                <%#Eval("DepartureStation")%>
                                to
                                <%#Eval("ArrivalStation")%>, departing at
                                <%# DateTime.ParseExact(Eval("DepartureDateTime").ToString(), "dd-MM-yyyy HH:mm", null).ToString("HH:mm")%>
                                on
                                <%# DateTime.ParseExact(Eval("DepartureDate").ToString(), "dd-MM-yyyy", null).ToString("dd MMM yyyy")%>
                                <%# Eval("TravellBy")%>
                            </div>
                            <asp:HiddenField ID="hdnOutboundsettercode" runat="server" Value='<%#Eval("fareSetterCode")%>' />
                            <asp:HiddenField ID="hdnIndex" runat="server" Value='<%#Eval("Index")%>' />
                            <asp:HiddenField ID="hdnMatchLegId" runat="server" Value='<%#Eval("MatchLegID")%>' />
                            <asp:HiddenField ID="hdnReservationStatus" runat="server" Value='<%#Eval("ReservationStatus") %>' />
                            <asp:HiddenField ID="hdnReservable" runat="server" Value='<%#Eval("Reservable")%>' />
                            <asp:HiddenField ID="hdnLegId" runat="server" Value='<%#Eval("LegId") %>' />
                            <asp:HiddenField ID="hdnDeptDateTime" runat="server" Value='<%#Eval("DepartureDateTime") %>' />
                            <asp:HiddenField ID="hdnAccommodationType" runat="server" Value='<%#Eval("AccommodationType") %>' />
                            <asp:HiddenField ID="hdnIsSleeper" runat="server" Value='<%#Eval("IsSleeper") %>' />
                            <asp:HiddenField ID="hdnisBerthSleeper" runat="server" Value='<%#Eval("isBerthSleeper") %>' />
                            <asp:HiddenField ID="hdnisSeatSleeper" runat="server" Value='<%#Eval("isSeatSleeper") %>' />
                            <table class="grddatabind newtable" id="div_Reservation1" runat="server">
                                <tbody>
                                    <tr>
                                        <td scope="row" colspan="3">
                                            <asp:RadioButton ID="rdnNoSeat" runat="server" GroupName="seat" Text="No Seat" />
                                            <asp:RadioButton ID="rdnSeat" runat="server" GroupName="seat" Text="Seat" />
                                            <asp:RadioButton ID="rdnSpecificSeat" runat="server" GroupName="seat" Text="Specific Seat" />
                                            <asp:RadioButton ID="rdnBerth" runat="server" GroupName="seat" Text="Berth" />
                                            <asp:RadioButton ID="rdnBerthExtra" runat="server" GroupName="seat" Text="Berth at extra cost" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="first">
                                            Direction
                                            <asp:DropDownList ID="ddlDirection" runat="server">
                                                <asp:ListItem Value="Any" Text="Any"></asp:ListItem>
                                                <asp:ListItem Value="Airline" Text="Forward"></asp:ListItem>
                                                <asp:ListItem Value="Back" Text="Back"></asp:ListItem>
                                                <asp:ListItem Value="Facing" Text="Facing"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="first">
                                            Location
                                            <asp:DropDownList ID="ddlLocation" runat="server">
                                                <asp:ListItem Value="Any" Text="Any"></asp:ListItem>
                                                <asp:ListItem Value="Quiet" Text="Quiet"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="first">
                                            Position
                                            <asp:DropDownList ID="ddlPosition" runat="server">
                                                <asp:ListItem Value="Any" Text="Any"></asp:ListItem>
                                                <asp:ListItem Value="Aisle" Text="Aisle"></asp:ListItem>
                                                <asp:ListItem Value="Middle" Text="Middle"></asp:ListItem>
                                                <asp:ListItem Value="Window" Text="Window"></asp:ListItem>
                                                <asp:ListItem Value="Individual" Text="Individual"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="second">
                                            Coach
                                            <asp:DropDownList ID="ddlCoach" runat="server">
                                            </asp:DropDownList>
                                            <span id="reqvalerror0" class="del-hd-block"></span>
                                        </td>
                                        <td class="second">
                                            Seat
                                            <asp:DropDownList ID="ddlSeat" runat="server">
                                            </asp:DropDownList>
                                            <span id="reqvalerror1" class="del-hd-block"></span>
                                        </td>
                                        <td class="second">
                                            Direction
                                            <asp:DropDownList ID="ddlDirections" runat="server">
                                                <asp:ListItem Value="0" Text="Select"></asp:ListItem>
                                                <asp:ListItem Value="Airline" Text="Forward"></asp:ListItem>
                                                <asp:ListItem Value="Back" Text="Back"></asp:ListItem>
                                                <asp:ListItem Value="Facing" Text="Facing"></asp:ListItem>
                                                <asp:ListItem Value="Unspecified" Text="Unspecified"></asp:ListItem>
                                            </asp:DropDownList>
                                            <span id="reqvalerror2" class="del-hd-block"></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="grddatabind" id="div_Reservation2" runat="server">
                                <tbody>
                                    <tr>
                                        <td scope="row">
                                            <%#Eval("Reservable")%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <%--InBound --%>
                <div class="grddataparent">
                    <div id="div_Inbound" runat="server">
                        <asp:Repeater ID="rptInBound" runat="server" OnItemDataBound="rptInBound_ItemDataBound">
                            <HeaderTemplate>
                                <div class="journey-header">
                                    <h3>
                                        Inbound</h3>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="starail-Form-row">
                                    <b>Leg
                                        <%# Eval("Index")%></b> -
                                    <%#Eval("DepartureStation")%>
                                    to
                                    <%#Eval("ArrivalStation")%>, departing at
                                    <%# DateTime.ParseExact(Eval("DepartureDateTime").ToString(), "dd-MM-yyyy HH:mm", null).ToString("HH:mm")%>
                                    on
                                    <%# DateTime.ParseExact(Eval("DepartureDate").ToString(), "dd-MM-yyyy", null).ToString("dd MMM yyyy")%>
                                    <%# Eval("TravellBy")%></div>
                                <asp:HiddenField ID="hdnInboundsettercode" runat="server" Value='<%#Eval("fareSetterCode")%>' />
                                <asp:HiddenField ID="hdnIndex" runat="server" Value='<%#Eval("Index")%>' />
                                <asp:HiddenField ID="hdnMatchLegId" runat="server" Value='<%#Eval("MatchLegID")%>' />
                                <asp:HiddenField ID="hdnReservationStatus" runat="server" Value='<%#Eval("ReservationStatus") %>' />
                                <asp:HiddenField ID="hdnReservable" runat="server" Value='<%#Eval("Reservable")%>' />
                                <asp:HiddenField ID="hdnLegId" runat="server" Value='<%#Eval("LegId") %>' />
                                <asp:HiddenField ID="hdnDeptDateTime" runat="server" Value='<%#Eval("DepartureDateTime") %>' />
                                <asp:HiddenField ID="hdnAccommodationType" runat="server" Value='<%#Eval("AccommodationType") %>' />
                                <asp:HiddenField ID="hdnIsSleeper" runat="server" Value='<%#Eval("IsSleeper") %>' />
                                <asp:HiddenField ID="hdnisBerthSleeper" runat="server" Value='<%#Eval("isBerthSleeper") %>' />
                                <asp:HiddenField ID="hdnisSeatSleeper" runat="server" Value='<%#Eval("isSeatSleeper") %>' />
                                <table class="grddatabind newtable" id="div_Reservation1" runat="server">
                                    <tbody>
                                        <tr>
                                            <td scope="row" colspan="3">
                                                <asp:RadioButton ID="rdnNoSeat" runat="server" GroupName="seat" Text="No Seat" />
                                                <asp:RadioButton ID="rdnSeat" runat="server" GroupName="seat" Text="Seat" />
                                                <asp:RadioButton ID="rdnSpecificSeat" runat="server" GroupName="seat" Text="Specific Seat" />
                                                <asp:RadioButton ID="rdnBerth" runat="server" GroupName="seat" Text="Berth" />
                                                <asp:RadioButton ID="rdnBerthExtra" runat="server" GroupName="seat" Text="Berth at extra cost" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="first">
                                                Direction
                                                <asp:DropDownList ID="ddlDirection" runat="server">
                                                    <asp:ListItem Value="Any" Text="Any"></asp:ListItem>
                                                    <asp:ListItem Value="Airline" Text="Forward"></asp:ListItem>
                                                    <asp:ListItem Value="Back" Text="Back"></asp:ListItem>
                                                    <asp:ListItem Value="Facing" Text="Facing"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td class="first">
                                                Location
                                                <asp:DropDownList ID="ddlLocation" runat="server">
                                                    <asp:ListItem Value="Any" Text="Any"></asp:ListItem>
                                                    <asp:ListItem Value="Quiet" Text="Quiet"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td class="first">
                                                Position
                                                <asp:DropDownList ID="ddlPosition" runat="server">
                                                    <asp:ListItem Value="Any" Text="Any"></asp:ListItem>
                                                    <asp:ListItem Value="Aisle" Text="Aisle"></asp:ListItem>
                                                    <asp:ListItem Value="Middle" Text="Middle"></asp:ListItem>
                                                    <asp:ListItem Value="Window" Text="Window"></asp:ListItem>
                                                    <asp:ListItem Value="Individual" Text="Individual"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td class="second">
                                                Coach
                                                <asp:DropDownList ID="ddlCoach" runat="server">
                                                    <asp:ListItem>Select</asp:ListItem>
                                                </asp:DropDownList>
                                                <span id="reqvalerror0" class="del-hd-block"></span>
                                            </td>
                                            <td class="second">
                                                Seat
                                                <asp:DropDownList ID="ddlSeat" runat="server">
                                                    <asp:ListItem>Select</asp:ListItem>
                                                </asp:DropDownList>
                                                <span id="reqvalerror1" class="del-hd-block"></span>
                                            </td>
                                            <td class="second">
                                                Direction
                                                <asp:DropDownList ID="ddlDirections" runat="server">
                                                    <asp:ListItem Value="0" Text="Select"></asp:ListItem>
                                                    <asp:ListItem Value="Airline" Text="Forward"></asp:ListItem>
                                                    <asp:ListItem Value="Back" Text="Back"></asp:ListItem>
                                                    <asp:ListItem Value="Facing" Text="Facing"></asp:ListItem>
                                                    <asp:ListItem Value="Unspecified" Text="Unspecified"></asp:ListItem>
                                                </asp:DropDownList>
                                                <span id="reqvalerror2" class="del-hd-block"></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="grddatabind" id="div_Reservation2" runat="server">
                                    <tbody>
                                        <tr>
                                            <td scope="row">
                                                <%#Eval("Reservable")%>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
            <%--Additional Preferences --%>
            <div class="starail-BookingDetails-form" id="div_AdditionalPreference" runat="server">
                <h2>
                    Additional Preferences</h2>
                <asp:CheckBoxList ID="chkAdditionalInfo" runat="server" CssClass="grddatabind additional full-mobile"
                    RepeatDirection="Horizontal" RepeatColumns="4">
                    <asp:ListItem Text="Video Facility" Value="VIDO"></asp:ListItem>
                    <asp:ListItem Text="Table Seats" Value="TABL"></asp:ListItem>
                    <asp:ListItem Text="Power Socket" Value="POWE"></asp:ListItem>
                    <asp:ListItem Text="Internet Connection" Value="INET"></asp:ListItem>
                    <asp:ListItem Text="Near Luggage Rack" Value="LUGG"></asp:ListItem>
                    <asp:ListItem Text="Near Toilet" Value="NRWC"></asp:ListItem>
                    <asp:ListItem Text="Near Shop or Buffet" Value="CLSP"></asp:ListItem>
                </asp:CheckBoxList>
                <div style="padding: 5px">
                    <p>
                        Please note that the requested seating options are not guaranteed, and that requesting
                        “Quiet” or multiple seat options can reduce the chances of being able to reserve
                        seats. If the reservations made are not suitable, please modify your options and
                        request seats again.
                    </p>
                    <p>
                        If your booking includes a late night or early morning cross city Underground or
                        Metro journey, please check the opening and closing times for this service.</p>
                </div>
            </div>
            <%--OutBound Berth --%>
            <div class="starail-BookingDetails-form manage-berth" id="div_Out_Berth" runat="server"
                style="display: none;">
                <asp:Repeater ID="rptOutBerth" runat="server">
                    <HeaderTemplate>
                        <h2>
                            OUTBOUND</h2>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="berth-value">
                            <asp:Label runat="server" ID="Label1" Text='<%#Eval("no")%>' CssClass="del-hd-block"></asp:Label>
                            <a style="position: absolute; right: -10px; top: -10px;" class="<%#Eval("isfirst").ToString()=="True"?"" : "del-hd-block" %>"
                                onclick="removethisrow(this)">
                                <img src="https://www.internationalrail.com/images/btn-cross.png" />
                            </a>
                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#Eval("no")%>' />
                            <asp:HiddenField ID="hdnisDelete" runat="server" Value='no' />
                            <asp:HiddenField ID="hdnisSolo" runat="server" Value='<%#Eval("isSolo") %>' />
                            <asp:HiddenField ID="hdnisMaleFemale" runat="server" Value='<%#Eval("isMaleFemale") %>' />
                            <asp:HiddenField ID="hdnisTogr" runat="server" Value='<%#Eval("isTogr") %>' />
                            <asp:HiddenField ID="hdnNoOfAdult" runat="server" Value='<%#Eval("NoOfAdult") %>' />
                            <asp:HiddenField ID="hdnNoOfChild" runat="server" Value='<%#Eval("NoOfChild") %>' />
                            <asp:HiddenField ID="hdnAdultPrice" runat="server" Value='<%#Eval("AdultPrice") %>' />
                            <asp:HiddenField ID="hdnChildPrice" runat="server" Value='<%#Eval("ChildPrice") %>' />
                            <asp:HiddenField ID="hdnAdultOrChildSharingPrice" runat="server" Value='<%#Eval("AdultOrChildSharingPrice") %>' />
                            <asp:HiddenField ID="hdnTwoChildSharingPrice" runat="server" Value='<%#Eval("TwoChildSharingPrice") %>' />
                            <asp:HiddenField ID="hdnsoloAdultOrChildPrice" runat="server" Value='<%#Eval("soloAdultOrChildPrice") %>' />
                            <asp:HiddenField ID="hdnOneBerthAdultMF" runat="server" Value='<%#Eval("OneBerthAdultMF") %>' />
                            <div class="berth-last-first newdivsection">
                                <label>
                                    Number Of Berths</label>
                                <asp:DropDownList ID="ddlNoOfBerth" runat="server" CssClass="grddatabind additional full-mobile"
                                    onchange="changeberth(this)">
                                    <asp:ListItem>Select ...</asp:ListItem>
                                    <asp:ListItem>One berth in two berth cabin</asp:ListItem>
                                    <asp:ListItem>Two berths in two berth cabin</asp:ListItem>
                                    <asp:ListItem>Solo occupancy cabin</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="berth-last-second newdivsection">
                                <label>
                                    Berth 1</label>
                                <asp:DropDownList ID="ddlBerth1" runat="server" CssClass="grddatabind additional full-mobile"
                                    onchange="Berthselectionchange('div_Out_Berth')">
                                    <asp:ListItem>Select ...</asp:ListItem>
                                    <asp:ListItem>Male Adult</asp:ListItem>
                                    <asp:ListItem>Female Adult</asp:ListItem>
                                    <asp:ListItem>Adult</asp:ListItem>
                                    <asp:ListItem>Child</asp:ListItem>
                                    <asp:ListItem>Two children sharing berth</asp:ListItem>
                                    <asp:ListItem>Adult and child sharing berth</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="berth-last-third newdivsection">
                                <label>
                                    Berth 2</label>
                                <asp:DropDownList ID="ddlBerth2" runat="server" CssClass="grddatabind additional full-mobile"
                                    onchange="Berthselectionchange('div_Out_Berth')">
                                    <asp:ListItem>Select ...</asp:ListItem>
                                    <asp:ListItem>Not Available</asp:ListItem>
                                    <asp:ListItem>Male Adult</asp:ListItem>
                                    <asp:ListItem>Female Adult</asp:ListItem>
                                    <asp:ListItem>Adult</asp:ListItem>
                                    <asp:ListItem>Child</asp:ListItem>
                                    <asp:ListItem>Two children sharing berth</asp:ListItem>
                                    <asp:ListItem>Adult and child sharing berth</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="berth-last-fourth newdivsection">
                                <label>
                                    Cabin Location</label>
                                <asp:DropDownList ID="ddlCabinLocation" runat="server" CssClass="grddatabind additional full-mobile">
                                    <asp:ListItem>No Preference</asp:ListItem>
                                    <asp:ListItem>Centre of Carriage</asp:ListItem>
                                    <asp:ListItem>End of Carriage</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="berth-last-fifth newdivsection">
                                <label>
                                    Preference</label>
                                <asp:DropDownList ID="ddlPreferences" runat="server" CssClass="grddatabind additional full-mobile">
                                    <asp:ListItem Value="No Preference">No Preference</asp:ListItem>
                                    <asp:ListItem Value="Connecting Door">Connecting Door</asp:ListItem>
                                    <asp:ListItem Value="Upper Berth">Upper Berth</asp:ListItem>
                                    <asp:ListItem Value="Lower Berth">Lower Berth</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="berth-last-sixth newdivsection" style="text-align: right;">
                                <%=currency%>
                                <asp:Label ID="lblBerthCost" runat="server" Text='0.00'></asp:Label>
                                <asp:HiddenField ID="hdnBerthCost" runat="server" Value="0.00" />
                            </div>
                            <hr />
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        <div style="text-align: right; font-size: 15px; font-weight: bold;">
                            <a id="addoutbound" onclick="addoutboundrow(this)">Add +</a></div>
                        <div id="rptOutBerthErrorMessage" style="text-align: right;">
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <%--InBound Berth --%>
            <div class="starail-BookingDetails-form manage-berth" id="div_In_Berth" runat="server"
                style="display: none;">
                <asp:Repeater ID="rptInBerth" runat="server">
                    <HeaderTemplate>
                        <h2>
                            INBOUND</h2>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="berth-value">
                            <asp:Label runat="server" ID="Label1" Text='<%#Eval("no")%>' CssClass="del-hd-block"></asp:Label>
                            <a style="position: absolute; right: -10px; top: -10px;" class="<%#Eval("isfirst").ToString()=="True"?"" : "del-hd-block" %>"
                                onclick="removethisrow(this)">
                                <img src="https://www.internationalrail.com/images/btn-cross.png" />
                            </a>
                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#Eval("no")%>' />
                            <asp:HiddenField ID="hdnisDelete" runat="server" Value='no' />
                            <asp:HiddenField ID="hdnisSolo" runat="server" Value='<%#Eval("isSolo") %>' />
                            <asp:HiddenField ID="hdnisMaleFemale" runat="server" Value='<%#Eval("isMaleFemale") %>' />
                            <asp:HiddenField ID="hdnisTogr" runat="server" Value='<%#Eval("isTogr") %>' />
                            <asp:HiddenField ID="hdnNoOfAdult" runat="server" Value='<%#Eval("NoOfAdult") %>' />
                            <asp:HiddenField ID="hdnNoOfChild" runat="server" Value='<%#Eval("NoOfChild") %>' />
                            <asp:HiddenField ID="hdnAdultPrice" runat="server" Value='<%#Eval("AdultPrice") %>' />
                            <asp:HiddenField ID="hdnChildPrice" runat="server" Value='<%#Eval("ChildPrice") %>' />
                            <asp:HiddenField ID="hdnAdultOrChildSharingPrice" runat="server" Value='<%#Eval("AdultOrChildSharingPrice") %>' />
                            <asp:HiddenField ID="hdnTwoChildSharingPrice" runat="server" Value='<%#Eval("TwoChildSharingPrice") %>' />
                            <asp:HiddenField ID="hdnsoloAdultOrChildPrice" runat="server" Value='<%#Eval("soloAdultOrChildPrice") %>' />
                            <asp:HiddenField ID="hdnOneBerthAdultMF" runat="server" Value='<%#Eval("OneBerthAdultMF") %>' />
                            <div class="berth-last-first newdivsection">
                                <label>
                                    Number Of Berths</label>
                                <asp:DropDownList ID="ddlNoOfBerth" runat="server" CssClass="grddatabind additional full-mobile"
                                    onchange="changeberth(this)">
                                    <asp:ListItem>Select ...</asp:ListItem>
                                    <asp:ListItem>One berth in two berth cabin</asp:ListItem>
                                    <asp:ListItem>Two berths in two berth cabin</asp:ListItem>
                                    <asp:ListItem>Solo occupancy cabin</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="berth-last-second newdivsection">
                                <label>
                                    Berth 1</label>
                                <asp:DropDownList ID="ddlBerth1" runat="server" CssClass="grddatabind additional full-mobile"
                                    onchange="Berthselectionchange('div_In_Berth')">
                                    <asp:ListItem>Select ...</asp:ListItem>
                                    <asp:ListItem>Male Adult</asp:ListItem>
                                    <asp:ListItem>Female Adult</asp:ListItem>
                                    <asp:ListItem>Adult</asp:ListItem>
                                    <asp:ListItem>Child</asp:ListItem>
                                    <asp:ListItem>Two children sharing berth</asp:ListItem>
                                    <asp:ListItem>Adult and child sharing berth</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="berth-last-third newdivsection">
                                <label>
                                    Berth 2</label>
                                <asp:DropDownList ID="ddlBerth2" runat="server" CssClass="grddatabind additional full-mobile"
                                    onchange="Berthselectionchange('div_In_Berth')">
                                    <asp:ListItem>Select ...</asp:ListItem>
                                    <asp:ListItem>Not Available</asp:ListItem>
                                    <asp:ListItem>Male Adult</asp:ListItem>
                                    <asp:ListItem>Female Adult</asp:ListItem>
                                    <asp:ListItem>Adult</asp:ListItem>
                                    <asp:ListItem>Child</asp:ListItem>
                                    <asp:ListItem>Two children sharing berth</asp:ListItem>
                                    <asp:ListItem>Adult and child sharing berth</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="berth-last-fourth newdivsection">
                                <label>
                                    Cabin Location</label>
                                <asp:DropDownList ID="ddlCabinLocation" runat="server" CssClass="grddatabind additional full-mobile">
                                    <asp:ListItem>No Preference</asp:ListItem>
                                    <asp:ListItem>Centre of Carriage</asp:ListItem>
                                    <asp:ListItem>End of Carriage</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="berth-last-fifth newdivsection">
                                <label>
                                    Preference</label>
                                <asp:DropDownList ID="ddlPreferences" runat="server" CssClass="grddatabind additional full-mobile">
                                    <asp:ListItem Value="No Preference">No Preference</asp:ListItem>
                                    <asp:ListItem Value="Connecting Door">Connecting Door</asp:ListItem>
                                    <asp:ListItem Value="Upper Berth">Upper Berth</asp:ListItem>
                                    <asp:ListItem Value="Lower Berth">Lower Berth</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="berth-last-sixth newdivsection" style="text-align: right;">
                                <%=currency%>
                                <asp:Label ID="lblBerthCost" runat="server" Text='0.00'></asp:Label>
                                <asp:HiddenField ID="hdnBerthCost" runat="server" Value="0.00" />
                            </div>
                            <hr />
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        <%#Eval("hideAddbtn")%>
                        <div style="text-align: right; font-size: 15px; font-weight: bold;">
                            <a id="addinbound" onclick="addoutboundrow(this)">Add +</a></div>
                        <div id="rptInBerthErrorMessage" style="text-align: right;">
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <%--Birth Traveller --%>
            <div class="starail-BookingDetails-form" id="div_Berth_Traveller" runat="server"
                style="display: none;">
                <h2>
                    Traveller Contact Details</h2>
                <p class="berth-traveller-details">
                    Please enter the name and phone number of the lead traveller. This information will
                    be used by the Train Operating Company should they need to contact the lead traveller
                    regarding this sleeper reservation.</p>
                <div class="berth-text">
                    <div class="berth-first-name">
                        Title
                    </div>
                    <div class="berth-first-name">
                        First Name
                    </div>
                    <div class="berth-first-name">
                        Last Name
                    </div>
                    <div class="berth-first-name">
                        Phone Number
                    </div>
                </div>
                <div class="berth-value">
                    <div class="berth-last-name">
                        <asp:DropDownList ID="ddlTitle" runat="server" CssClass="grddatabind additional full-mobile">
                            <asp:ListItem Value="0">Title</asp:ListItem>
                            <asp:ListItem Value="1">Dr.</asp:ListItem>
                            <asp:ListItem Value="2">Mr.</asp:ListItem>
                            <asp:ListItem Value="3">Miss</asp:ListItem>
                            <asp:ListItem Value="4">Mrs.</asp:ListItem>
                            <asp:ListItem Value="5">Ms.</asp:ListItem>
                        </asp:DropDownList>
                        <span id="reqvalerror5" class="del-hd-block"></span>
                    </div>
                    <div class="berth-last-name">
                        <asp:TextBox ID="txtFirstName" runat="server">
                        </asp:TextBox>
                        <span id="reqvalerror6" class="del-hd-block"></span>
                    </div>
                    <div class="berth-last-name">
                        <asp:TextBox ID="txtLastName" runat="server">
                        </asp:TextBox>
                        <span id="reqvalerror7" class="del-hd-block"></span>
                    </div>
                    <div class="berth-last-name">
                        <asp:TextBox ID="txtPhoneNo" runat="server">
                        </asp:TextBox>
                        <span id="reqvalerror8" class="del-hd-block"></span>
                    </div>
                </div>
            </div>
            <div class="starail-BookingDetails-form">
                <h2>
                    TOTAL PRICE</h2>
                <div class="starail-Form-row">
                    <div class="starail-Form-inputContainer-col" style="width: 100%">
                        <p class="starail-YourBooking-totalPrice">
                            Total Price: <span class="starail-YourBooking-totalPrice-amount">
                                <asp:HiddenField ID="hdnTotal" runat="server" Value="0" />
                                <asp:Label ID="lblTotal" runat="server" Text="" CssClass="spanprice"></asp:Label></span></p>
                    </div>
                </div>
            </div>
            <div class="starail-Section starail-Section--nopadding starail-Section-nextButtonSection textcenter">
                <asp:Button ID="btnChange" runat="server" Text="Back" CssClass="starail-Button" OnClick="btnChange_Click" />
                <asp:Button ID="btnSave" runat="server" Text="Next: Booking Details" CssClass="starail-Button"
                    OnClientClick="return validValidation()" OnClick="btnSave_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="updReservation"
        DisplayAfter="0" DynamicLayout="True">
        <ProgressTemplate>
            <div class="modalBackground progessposition">
            </div>
            <div class="progess-inner2">
                UPDATING RESULTS...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
