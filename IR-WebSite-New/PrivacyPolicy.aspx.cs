﻿using System;
using Business;
using System.Linq;
using System.Web;
using System.Configuration;

public partial class PrivacyPolicy : System.Web.UI.Page
{
    readonly ManageBooking _master = new ManageBooking();
    private Guid _siteId;
    Guid pageID;
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string siteURL;
    public string script;
    public string PageTitle = "";
    public string PageDescription = "";
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(_siteId);
            if (Page.RouteData.Values["PageId"] != null)
            {
                pageID = (Guid)Page.RouteData.Values["PageId"];
                BindPrivacyPolicy(_siteId);
            }
            var pid = _db.tblWebMenus.FirstOrDefault(x => x.Name.ToLower() == "home");
            if (pid != null)
                PageContent(pid.ID, _siteId);

            #region Seobreadcrumbs
            var pageIDs = (Guid)Page.RouteData.Values["PageId"];
            var dataSeobreadcrumbs = new ManageSeo().Getlinkbody(pageIDs, _siteId);
            if (dataSeobreadcrumbs != null && dataSeobreadcrumbs.IsActive)
                litSeobreadcrumbs.Text = dataSeobreadcrumbs.SourceCode;
            #endregion
        }
    }
    public void PageContent(Guid pageID, Guid siteID)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageID && x.SiteID == siteID);
            if (result != null)
            {
                var url = result.Url;
                var oPage = _masterPage.GetPageDetailsByUrl(url);

                //Banner
                string[] arrListId = oPage.BannerIDs.Split(',');
                var idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _masterPage.GetBannerImgByID(idList);
                int countdata = list.Count();
                int index = new Random().Next(countdata);
                var newlist = list.Skip(index).FirstOrDefault();
                if (newlist != null && newlist.ImgUrl != null)
                    img_Banner.Src = adminSiteUrl + "" + newlist.ImgUrl;
                else
                    img_Banner.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void BindPrivacyPolicy(Guid siteId)
    {
        var result = _master.GetPrivacyBySiteid(siteId).Where(x => x.IsActive == true);
        if (result != null)
        {
            PageTitle = result.Select(x => x.Title).FirstOrDefault();
            PageDescription = result.Select(x => x.Description).FirstOrDefault();
        }
        rptPriv.DataSource = result;
        rptPriv.DataBind();
    }
}